<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_Block_Adminhtml_Product_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    protected $_store = null;
    protected $_customData = array();

    public function __construct()
    {
        parent::__construct();
        $this->setId('giftcardGrid');
        $this->setDefaultSort('status');
        $this->setDefaultDir('ASC');
    }

    public function getStore()
    {
        if ($this->_store == null) {
            $storeId = (int)$this->getRequest()->getParam('store', 0);
            $this->_store = Mage::app()->getStore($storeId);
        }
        return $this->_store;
    }

    protected function _prepareCollection()
    {
        $store = $this->getStore();

        $collection = Mage::getResourceModel('aw_giftcard2/product_collection')
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('sku')
            ->addAttributeToSelect('thumbnail')
            ->addAttributeToSelect(AW_Giftcard2_Model_Product_Type_Giftcard::ATTRIBUTE_CODE_TYPE)
            ->addAttributeToFilter('type_id', AW_Giftcard2_Model_Product_Type_Giftcard::TYPE_CODE)
        ;

        if ($store->getId()) {
            $adminStore = Mage_Core_Model_App::ADMIN_STORE_ID;
            $collection->addStoreFilter($store);
            $collection->joinAttribute(
                'name',
                'catalog_product/name',
                'entity_id',
                null,
                'inner',
                $adminStore
            );
            $collection->joinAttribute(
                'custom_name',
                'catalog_product/name',
                'entity_id',
                null,
                'inner',
                $store->getId()
            );
            $collection->joinAttribute(
                'status',
                'catalog_product/status',
                'entity_id',
                null,
                'inner',
                $store->getId()
            );
            $collection->joinAttribute(
                'visibility',
                'catalog_product/visibility',
                'entity_id',
                null,
                'inner',
                $store->getId()
            );
        }
        else {
            $collection->joinAttribute('status', 'catalog_product/status', 'entity_id', null, 'inner');
            $collection->joinAttribute('visibility', 'catalog_product/visibility', 'entity_id', null, 'inner');
        }

        $collection->addGiftcardData($store);

        $this->setCollection($collection);

        parent::_prepareCollection();
        $this->getCollection()->addWebsiteNamesToResult();

        return $this;
    }

    protected function _addColumnFilterToCollection($column)
    {
        if ($this->getCollection()) {
            if ($column->getId() == 'websites') {
                $this->getCollection()->joinField('websites',
                    'catalog/product_website',
                    'website_id',
                    'product_id=entity_id',
                    null,
                    'left');
            }
        }
        return parent::_addColumnFilterToCollection($column);
    }

    protected function _setCollectionOrder($column)
    {
        parent::_setCollectionOrder($column);
        $collection = $this->getCollection();
        if ($collection) {
            $collection->setOrder('entity_id', Varien_Data_Collection::SORT_ORDER_DESC);
        }
        return $this;
    }

    protected function _prepareColumns()
    {
        $this->addColumn('status',
            array(
                'header'=> $this->__('Status'),
                'width' => '70px',
                'index' => 'status',
                'type'  => 'options',
                'options' => Mage::getSingleton('catalog/product_status')->getOptionArray(),
            )
        );

        $this->addColumn('visibility',
            array(
                'header'=> $this->__('Visibility'),
                'width' => '70px',
                'index' => 'visibility',
                'type'  => 'options',
                'options' => Mage::getModel('catalog/product_visibility')->getOptionArray(),
            )
        );

        $this->addColumn('image',
            array(
                'header' => $this->__('Thumbnail'),
                'align' => 'center',
                'filter' => false,
                'index' => 'image',
                'width'     => '50',
                'renderer' => 'aw_giftcard2/adminhtml_widget_grid_column_renderer_thumbnail'
            )
        );

        $this->addColumn('name',
            array(
                'header'=> $this->__('Name'),
                'index' => 'name',
            )
        );

        $this->addColumn('sku',
            array(
                'header'=> $this->__('SKU'),
                'width' => '80px',
                'index' => 'sku',
            )
        );

        $this->addColumn('type',
            array(
                'header'=> $this->__('Type'),
                'width' => '60px',
                'index' => AW_Giftcard2_Model_Product_Type_Giftcard::ATTRIBUTE_CODE_TYPE,
                'type'  => 'options',
                'options' => Mage::getSingleton('aw_giftcard2/source_entity_attribute_giftcard_type')->toArray()
            )
        );

        $this->addColumn('purchased_qty',
            array(
                'header'=> $this->__('Purchased Qty'),
                'width' => '100px',
                'type'  => 'number',
                'index' => 'purchased_qty',
            )
        );

        $this->addColumn('used_qty',
            array(
                'header'=> $this->__('Used Qty'),
                'width' => '100px',
                'type'  => 'number',
                'index' => 'used_qty',
            )
        );

        $this->addColumn('purchased_amount',
            array(
                'header'=> $this->__('Purchased Amount'),
                'type'  => 'price',
                'currency_code' => $this->getStore()->getBaseCurrency()->getCode(),
                'index' => 'purchased_amount',
            )
        );

        $this->addColumn('used_amount',
            array(
                'header'=> $this->__('Used Amount'),
                'type'  => 'price',
                'currency_code' => $this->getStore()->getBaseCurrency()->getCode(),
                'index' => 'used_amount',
            )
        );

        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('websites',
                array(
                    'header'=> $this->__('Websites'),
                    'width' => '100px',
                    'sortable'  => false,
                    'index'     => 'websites',
                    'type'      => 'options',
                    'options'   => Mage::getModel('core/website')->getCollection()->toOptionHash(),
                ));
        }

        $this->addColumn('email_templates',
            array(
                'header'=> $this->__('Email Template(s)'),
                'width' => '200px',
                'index' => 'email_templates',
                'filter_index' => 'email_templates',
                'sortable'  => false,
                'type'  => 'options',
                'options' => Mage::getModel('aw_giftcard2/source_entity_attribute_giftcard_email_template')->toArray(),
                'renderer'      => 'aw_giftcard2/adminhtml_widget_grid_column_renderer_template'
            )
        );

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('giftcard');

        $this->getMassactionBlock()->addItem('delete', array(
            'label'=> $this->__('Delete'),
            'url'  => $this->getUrl('*/*/massDelete', array('_current'=>true)),
            'confirm' => $this->__('Are you sure?')
        ));
        $this->getMassactionBlock()->addItem('enable', array(
            'label'=> $this->__('Enable'),
            'url'  => $this->getUrl('*/*/massEnable', array('_current'=>true)),
            'confirm' => $this->__('Are you sure?')
        ));
        $this->getMassactionBlock()->addItem('disable', array(
            'label' => $this->__('Disable'),
            'url' => $this->getUrl('*/*/massDisable'),
            'confirm' => $this->__('Are you sure?')
        ));

    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getEntityId()));
    }
}