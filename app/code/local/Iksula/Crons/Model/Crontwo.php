<?php

class Iksula_Crons_Model_Crontwo extends Mage_Core_Model_Abstract
{
    /**
     * This function is called from crontwoController to check and update pending and canceled Orders
     *
     * @return processed orders
     */
    public function Hitapi($paymentMethod = 'payu')
    {
        Mage::log('Cron two executing',null,'crontwo.log');
        $flag = Mage::getStoreConfig('crons_section/crons_group/crontwo');
        // check if cron is enabled from backend
        if ($flag == 1) {
            $fromDate            = new DateTime();
            $fromDate            = $fromDate->format("Y-m-d H:i:s");
            $updateCroneStatuses = Mage::getModel('crons/crons')->getCollection()->getFirstItem();
            $updateCroneStatuses->setData('crontwo_status', 'Running : Started at - ' . $fromDate)->save();
            $orders = $this->getOrderCollection($paymentMethod);
            // echo count($orders);exit;
            $this->segregateOrders($orders);
        } else {
            $fromDate            = new DateTime();
            $fromDate            = $fromDate->format("Y-m-d H:i:s");
            $updateCroneStatuses = Mage::getModel('crons/crons')->getCollection()->getFirstItem();
            $updateCroneStatuses->setData('crontwo_status', 'Disabled from backend : Tried to run at - ' . $fromDate)->save();
        }

    }

    /**
     * Function returns order collection which will be processed further
     *
     * @return array of sales_order objects(orders)
     */

    private function getOrderCollection($paymentMethod)
    {
        $filterDateFrom = '2014-12-30 00:00:00';
        //$filterDateTo = new DateTime();
        // $filterDateTo->modify("-120 minutes");
        //$filterDateTo = $filterDateTo->format("Y-m-d H:i:s");
        $filterDateTo = date('Y-m-d h:i:s',strtotime('-15 min'));

        // List of all payment methods availeble in payu
        $orderObj = Mage::getModel('sales/order');
        $orders   = $orderObj->getCollection()->join(array('payment'=>'sales/order_payment'),'main_table.entity_id=parent_id','method');
        if($paymentMethod == 'payu'){
            $orders->addFieldToFilter('method',array('in'=>array('secureebs_standard','payuemi_standard','payudebit_standard','payuMoney_standard','payunet_standard')))->addFieldToFilter('status', array('in' => array('pending','canceledpending')))->addFieldToFilter('created_at', array('from' => $filterDateFrom, 'to' => $filterDateTo))->setPageSize(30)->setCurPage(1);
        }
        // elseif($paymentMethod == 'innoviti'){
        //     $orders->addFieldToFilter('method',array('in'=>array('gateway')))->addFieldToFilter('status', array('in' => array('pending','canceledpending')))->addFieldToFilter('created_at', array('from' => $filterDateFrom, 'to' => $filterDateTo))->setPageSize(30)->setCurPage(1);
        // }
        return $orders;

    }

    /**
     * Function returns order collection which will be processed further
     * @param array of orders to process further
     */

    public function segregateOrders($orders)
    {

        $payuMethods   = array(
            'secureebs_standard',
            'payuairtel_standard',
            'payudebit_standard',
            'payuMoney_standard',
            'payunet_standard'
        );
        $transactionId = '';
        foreach ($orders as $orderData) {
            try {
                if ($orderData->getPayment()) {
                    if($orderData->getPayment()->getMethodInstance()) {
                        $method = $orderData->getPayment()->getMethodInstance()->getCode();
                        if ($method == "gateway") {
                            // $this->fetchInnovitiDataFromApi($orderData);
                        } elseif (in_array($method, $payuMethods)) {
                            // get all order Ids and append them with pipe separated so that we can send only one request to payu and get statuses for all of them. This will reduce execution time
                            $transactionId = $transactionId . $orderData->getRealOrderId() . '|';
                        }
                    }
                }
            } catch(Exception $e) {
                //
            }
        }
        // check if payu orders are actually there
        if ($transactionId != "") {
            $this->fetchPayuDataFromApi($transactionId);
        }

        $fromDate            = new DateTime();
        $fromDate            = $fromDate->format("Y-m-d H:i:s");
        $updateCroneStatuses = Mage::getModel('crons/crons')->getCollection()->getFirstItem();
        $updateCroneStatuses->setData('crontwo_status', 'Complete : Execution Finished at - ' . $fromDate)->save();
    }

    /**
     * Function returns order information for Innoviti order from the Innoviti API
     *
     * @param data of order to be checked in Innoviti API
     * @return ----
     */

    // private function fetchInnovitiDataFromApi($orderData)
    // {

    //     $status = Mage::getModel('crons/innovityapi')->getApiResponse($orderData);
    //     $orderIdvalue = $orderData->getRealOrderId();
    //     $loadedOrder = Mage::getModel('sales/order')->loadByIncrementId($orderIdvalue);

    //     // If captured at Innoviti, change order status from 'canceledpending' to 'awaitingverification'
    //     // Else, change order status from 'canceledpending' to 'canceled'
    //     if ($status == '00') {
    //         $this->makeAwaitingVerification($loadedOrder, '');
    //     } else {
    //         $this->makeCanceled($loadedOrder, '');
    //     }
    // }

    /**
     * Function returns order information for PayU orders from the PayU API
     *
     * @param list of pipe separated order IDs
     * @return ----
     */

    private function fetchPayuDataFromApi($transactionId)
    {

        $statuses = Mage::getModel('crons/payuapi')->getApiResponse($transactionId);
        $order_response = $statuses['transaction_details']; //get order data array
        foreach ($order_response as $orderid => $orderData) {
            $payustatus  = $orderData['unmappedstatus']; //get payment status
            $loadedOrder = Mage::getModel('sales/order')->loadByIncrementId($orderid);

            // If captured at PayU, change order status from 'canceledpending' to 'awaitingverification'
            // Else, change order status from 'canceledpending' to 'canceled'
            if ($payustatus == 'captured') {
                $authcode = $orderData['mihpayid'];
                $this->makeAwaitingVerification($loadedOrder, $authcode);
            } else {
                $this->makeCanceled($loadedOrder, $authcode);
            }
        }
    }

    /**
     * Function takes the order and changes its status to awaitingconfirmation
     *
     * @param orderData,authcode
     * @return ----
     */

    private function makeAwaitingVerification($loadedOrder, $authcode = '')
    {

        try {
            Mage::log($loadedOrder->getIncrementId().' in awaiting verification',null,'crontwo.log');
            $loadedOrder->setState(Mage_Sales_Model_Order::STATE_PROCESSING,'awaitingverification','Updated By Cron:2')->save();
            Mage::helper('orderitemsstatestatus')->changeItemStatebyOrderState('awaitingverification' , $loadedOrder);
            
            /* PO Data not saved using Observer */
                $poData = Mage::getModel('omniapis/storepo')->getCollection()->addFieldToFilter('order_id',array('eq'=>$loadedOrder->getId()))->getData();
                foreach ($poData as $pod) {
                    Mage::getModel('omniapis/storepo')->load($pod['id'])->setStatus('awaitingverification')->save();
                }
            /* PO Data not saved using Observer */

            if ($authcode != '') {
                $loadedOrder->setAuthcode($authcode);
            }
            $loadedOrder->sendNewOrderEmail();
            $loadedOrder->save();

        }
        catch (Exception $e) {
            mail("twinkal.g@iksula.com,satyendra.mishra@iksula.com", "Payu Issue OrderId #" . $loadedOrder->getIncrementId(), 'order was not updated');
        }

    }

    /**
     * Function takes the order and changes its status to canceledpending
     *
     * @param orderData,authcode
     * @return ----
     */

    private function makeCanceled($loadedOrder, $authcode = '')
    {
        try {
            Mage::log($loadedOrder->getIncrementId().' in cancel',null,'crontwo.log');
            $loadedOrder->setState(Mage_Sales_Model_Order::STATE_CANCELED,'canceled','Updated by Cron:2')->save();
            Mage::helper('orderitemsstatestatus')->changeItemStatebyOrderState('canceled' , $loadedOrder);
            
            /* PO Data not saved using Observer */
                $poData = Mage::getModel('omniapis/storepo')->getCollection()->addFieldToFilter('order_id',array('eq'=>$loadedOrder->getId()))->getData();
                foreach ($poData as $pod) {
                    Mage::getModel('omniapis/storepo')->load($pod['id'])->setStatus('canceledpending')->save();
                }
            /* PO Data not saved using Observer */

            if ($authcode != '') {
                $loadedOrder->setAuthcode($authcode);
            }
            $loadedOrder->save();

        }
        catch (Exception $e) {
            mail("twinkal.g@iksula.com,satyendra.mishra@iksula.com", "Payu Issue OrderId #" . $loadedOrder->getIncrementId(), 'order was not updated');
        }

    }

}
