<?php


class Aster_Purchase_Block_Adminhtml_Report_Purchase extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

		// $this->_controller = "adminhtml_report_purchase";
		// $this->_blockGroup = "purchase";
		// $this->_headerText = Mage::helper("purchase")->__("Purchase Manager");
		// $this->_addButtonLabel = Mage::helper("purchase")->__("Add New Item");
		// parent::__construct();
		// $this->_removeButton('add');


		$this->_blockGroup = 'purchase';
		$this->_controller = 'adminhtml_report_purchase';
		$this->_headerText = Mage::helper('purchase')->__('Complex Report');
		$this->setTemplate('report/grid/container.phtml');
		parent::__construct();
		$this->_removeButton('add');
		$this->addButton('filter_form_submit', array(
			'label'     => Mage::helper('purchase')->__('Show Report'),
			'onclick'   => 'filterFormSubmit()'
			));
	}

	public function getFilterUrl()
    {
        $this->getRequest()->setParam('filter', null);
        return $this->getUrl('*/*/complex', array('_current' => true));
    }

}