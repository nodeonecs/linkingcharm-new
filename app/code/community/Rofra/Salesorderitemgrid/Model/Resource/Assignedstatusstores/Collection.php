<?php
/**
 * Iksula_Storemanager extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category       Iksula
 * @package        Iksula_Storemanager
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Store collection resource model
 *
 * @category    Iksula
 * @package     Iksula_Storemanager
 * @author      Ultimate Module Creator
 */
class Rofra_Salesorderitemgrid_Model_Resource_Assignedstatusstores_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /*protected $_joinedFields = array();*/


    protected function _construct()
    {
        parent::_construct();
        $this->_init('salesorderitemgrid/assigned_status');
        //$this->_map['fields']['store'] = 'store_table.store_id';
    }


}
