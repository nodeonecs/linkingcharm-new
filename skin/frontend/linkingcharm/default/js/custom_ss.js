
  jQuery(function($){
    $('a[data-modal-id]').live('click', (function(e) {
    jQuery.fancybox.close();
    var appendthis =  ("<div class='modal-overlay js-modal-close'></div>");
      var modalBox = $(this).attr('data-modal-id');
      e.preventDefault();
      var flag = 0;
      if(modalBox == 'popup') {
        var modaltype = $(this).attr('data-modal-type');
      // call function to get simple product from configurable
        if(modaltype == 'configurable') {
          flag = simpleToconfigurable();
        }
      }
  
      if(flag == 0) {
          var modalId = $(this).attr('data-modal-id');
          var modaltype = $(this).attr('data-modal-type');
          var modalSku = $(this).attr('data-modal-sku');
          var modalUrl = $(this).attr('data-modal-url');
          var modalPage = $(this).attr('data-modal-page');
          var modalItem = $(this).attr('data-modal-item');
          setpopuphtml(modalSku,modaltype,modalUrl,modalPage,modalItem,modalId);

        $(".main-container").append(appendthis);
        $(".modal-overlay").fadeTo("fast", 0.7);
        $(".js-modalbox").fadeIn("fast");
        $('#'+modalBox).fadeIn($(this).data());
        // $('body').css('overflow', 'hidden');
      }
    })); 


  jQuery(".close-div").live('click',(function() {

    jQuery('#attribute_code_data').val('');
    jQuery('#attribute_value_data').val('');
    jQuery('#simple_product_id').val('');
    
    jQuery(".modal-box, .modal-overlay, .checkout_storepopup" ).fadeOut("fast",function() {
      jQuery(".modal-overlay").remove();
    });
    // $('body').css('overflow', 'auto');
  }));

  jQuery(".checkout-modal-close").live('click',(function() {
    // jQuery(".modal-box, .modal-overlay").fadeOut(500, function() {
    //   jQuery(".modal-overlay").remove();
    // });
    jQuery("#checkout-pickup-popup").removeClass('checkout_storepopup');
    jQuery("#checkout-pickup-popup").empty();
    jQuery( ".header-cart-link" ).trigger( "click" );
    
  }));


  $(window).resize(function() {
    $(".modal-box").css({
      top: ($(window).height() - $(".modal-box").outerHeight()) / 2,
      left: ($(window).width() - $(".modal-box").outerWidth()) / 2
    });
  });
  $(window).resize();
  });


function setpopuphtml(product_sku,product_type,url,page,item_id,modalid) {
    jQuery.ajax({
        url : url, 
        type : 'post',
        data : { 'product_sku': product_sku,'product_type':product_type,'page_type':page,'item_id':item_id},
        success : function(data) {
            if(page == 'product_page') {
              jQuery('#popup').html(data);
            }else{
              jQuery('#'+modalid).html(data);
              jQuery('#'+modalid).fadeIn();
              jQuery("#checkout-pickup-popup").append(jQuery("#"+modalid).html());
              jQuery("#checkout-pickup-popup").addClass('checkout_storepopup');
              // jQuery.fancybox.close();
              jQuery("#"+modalid).empty();
            }
        }
    });
}

function pinStore(product_sku,product_type,url,page_type,item_id) {
    var store_pincode = document.getElementById("store_pincode").value;
    if(store_pincode == '' || jQuery('#store_pincode').val().length < 6) {
      jQuery('#store_result').html('Please Enter 6 digit Pincode');
      return false;
    }
    var configurable_sku = '';
    if(page_type == 'product_page' || page_type == 'custom_page') {
      if(product_type == 'configurable') {
          var configurable_sku = product_sku;
          var product_sku = jQuery('#simple_product_id').val();
      }else {
          // do nothing
      }
    }else {
        // do nothing
    }

    jQuery.ajax({
        url : url, 
        type : 'post',
        data : { 'product_sku': product_sku, 'store_pincode':store_pincode,'product_type':product_type,'page_type':page_type,'item_id':item_id,'configurable_sku':configurable_sku},
        success : function(data) {
            jQuery('#store_result').html(data);            
            jQuery(".store_result_data").mCustomScrollbar();
        }
    });
}

function myStore(product_sku,product_type,url,page_type,item_id) {
    var store_code = {};
    var store_code = document.getElementById("store_city").value;
    
    var configurable_sku = '';
    if(page_type == 'product_page' || page_type == 'custom_page') {
      if(product_type == 'configurable') {
          var configurable_sku = product_sku;
          var product_sku = jQuery('#simple_product_id').val();
      }else {
          // do nothing
      }
     }else {
        // do nothing
     }

    jQuery.ajax({
        url : url,
        type : 'post',
        data : { 'product_sku': product_sku, 'store_code':store_code,'product_type':product_type,'page_type':page_type,'item_id':item_id,'configurable_sku':configurable_sku},
        success : function(data) {
            jQuery('#store_result').html(data);

            jQuery(".store_result_data").mCustomScrollbar();

        }
    });
}

// jQuery(document).ready(function() {
//     $(document).ajaxComplete(function() {
//         $(".pickupbox").mCustomScrollbar();
//     });
// });




 