<?php


class Amasty_Rma_Block_Adminhtml_Request_Grid_Renderer_Storename
extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract{


public function render(Varien_Object $row)
    {
        $request_created_by = $row->getRequestCreatedBy();
        if($request_created_by){
        	$adminModel = Mage::getModel('admin/user')->load($request_created_by)->getData();
        	return $adminModel['firstname'] . " , " . $adminModel['lastname'];
        }else{
        	return "Customer";
        }
    }


}
