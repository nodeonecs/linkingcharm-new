<?php

class KP_Otp_IndexController extends Mage_Core_Controller_Front_Action {


  public function sendOtpToCustomerAction(){

    // code to send otp sms to customer
    try{ 
     $mobileNumber = $this->getRequest()->getParam('mobile');
      if($mobileNumber){
        Mage::getSingleton('core/session')->setIsOtpVarified(false);        
        Mage::getSingleton('core/session')->unsOtpNumber();
        // code to generate rand number 
        $otpNumber = rand(100000,999999);

        Mage::getSingleton('core/session')->setOtpNumber($otpNumber);
        Mage::log("Otp>>".$otpNumber);
        // curl code to send sms to customer 
        // Textlocal account details
        
        $username = Mage::getStoreConfig('opt_general/credential/username');
        $password = Mage::getStoreConfig('opt_general/credential/password');
        $apiUrl = Mage::getStoreConfig('opt_general/credential/api_url');
        $sender = urlencode(Mage::getStoreConfig('opt_general/credential/sender'));
        $default_text = Mage::getStoreConfig('opt_general/credential/default_text');
        $email_id = Mage::getStoreConfig('opt_general/credential/email_id');
        $validTime = date('H:i:s', strtotime('+5 minutes', strtotime(date('H:i:s'))));
        $message = Mage::helper('otp')->__($default_text,$otpNumber,$validTime);
        
        // Prepare data for POST request
        // $data = array('uname' => $username, 'pass' => $password, 'dest' => $mobileNumber, "send" => $sender, "msg" => $message);
        
        // Gets the current store's id
        $storeId = Mage::app()->getStore()->getStoreId();
        Mage::helper('smsapp')->sensSMS("OTP is $otpNumber",$mobileNumber,$storeId);
        $response['response'] = true;
        $response['message'] = 'OTP sent to '.$mobileNumber;
        // Send the POST request with cURL
        // $ch = curl_init($apiUrl);
        // curl_setopt($ch, CURLOPT_POST, true);
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // $curlResponse = curl_exec($ch);
        // $status = trim(substr($curlResponse, 0,5));
        // switch ($status) {
        //   case '0x200':
        //   case '0x201':
        //   case '0x202':
        //   case '0x203':
        //   case '0x205':
        //   case '0x206':
        //   case '0x207':
        //   case '0x208':
        //   case '0x209':
        //   case '0x210':
        //   case '0x211':
        //   case '0x212':
                      
        //     # code...
        //   // Code to send an email to merchant
        //     $mail = Mage::getModel('core/email');
        //     $mail->setToName('Tresmode');
        //     $mail->setToEmail($email_id);
        //     $mail->setBody("Issue while sendig OTP message to customer. Error message is ".substr($response, 5));
        //     $mail->setSubject('OTP not getting send');
        //     $mail->setFromEmail('care@tresmode.com');
        //     $mail->setFromName("OTP Issue");
        //     $mail->setType('html');

        //     try {
        //     $mail->send();           
        //     }
        //     catch (Exception $e) {
        //      Mage::log("Issue while sending mail from OTP module");
        //     }
            
        //     $response['response'] = false;
        //     $response['response_server'] = $curlResponse;
        //     $response['message'] = "There is an issue while sending message, please try again later ";
        //     break;
        //   case '0x213':
        //     $response['response'] = false;
        //     $response['response_server'] = $curlResponse;
        //     $response['message'] = "Invalid mobile number, please try again later ";  
        //     break;
        //   default:
        //     $response['response'] = true;
        //     $response['response_server'] = $curlResponse;
        //     $response['message'] = "OTP has been sent to your mobile number ".$mobileNumber;
        //     break;
        // }
        
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
       } 
    }catch(Exception $ex){
      $response['response'] = false;
      $response['message'] = $ex->getMessage();
      $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));

    }

  }


  public function confirmOtpAction(){

    try{
      $session = Mage::getSingleton("core/session", array("name"=>"frontend"));
      $actualOtp = $session->getOtpNumber();
      $userInputOtp = trim($this->getRequest()->getParam('otp'));

      //$cdate = date("Y-m-d H:i:s");
      $_isVarified = Mage::getSingleton('core/session')->getIsOtpVarified(); 
      $mobile = Mage::getSingleton('checkout/session')->getQuote()
                                      ->getBillingAddress()
                                      ->getData('telephone');
      $response = array();
      $validNumber = Mage::getSingleton('core/session')->getVarifiedMobile();
          // further checking whether session timeout is over 
          /*$validTime = Mage::getSingleton('core/session')->getValidTime();
          $validNumber = Mage::getSingleton('core/session')->getVarifiedMobile();
          if($cdate > $validTime || $validNumber != $mobile){
           
               Mage::getSingleton('core/session')->unsIsOtpVarified();
               Mage::getSingleton('core/session')->unsOtpNumber();
                $response['response'] = false;
                $response['message'] = 'OTP provided is not valid, please try again'; 
                
            }else*/ if( !empty($userInputOtp) && $actualOtp == $userInputOtp){
            
                    Mage::getSingleton('core/session')->setIsOtpVarified(true);
                    Mage::getSingleton('core/session')->setVarifiedMobile($mobile);       
                    //$newtimestamp = date('Y-m-d H:i:s', strtotime("+5 min"));
                    //Mage::getSingleton('core/session')->setValidTime($newtimestamp);  
                    $response['response'] = true;
                    $response['message'] = 'OTP verified successfully';
                    $response['mobile'] = $mobile;
                    //$response['valid_timestamp'] = $newtimestamp;
            }else{
           
                  $response['response'] = false;
                  $response['message'] = 'OTP provided is not valid, please try again';
                  Mage::getSingleton('core/session')->setIsOtpVarified(false);  
            }
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
     
    }catch(Exception $ex){
      $response['response'] = false;
      $response['message'] = $ex->getMessage();
      $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
    }

  }

  public function unsetvarAction()
  {
    Mage::getSingleton('core/session')->unsOtpNumber();
  }


}
