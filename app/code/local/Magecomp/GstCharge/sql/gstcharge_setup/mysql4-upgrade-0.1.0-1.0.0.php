<?php
/* @var $installer Mage_Sales_Model_Mysql4_Setup */
$installer = $this;

$installer->startSetup();

$installer->addAttribute('quote_address', 'sgst_charge', array('type' => 'decimal'));
$installer->addAttribute('quote_address', 'base_sgst_charge', array('type' => 'decimal'));

$installer->addAttribute('order', 'sgst_charge', array('type' => 'decimal'));
$installer->addAttribute('order', 'base_sgst_charge', array('type' => 'decimal'));

$installer->addAttribute('invoice', 'sgst_charge', array('type' => 'decimal'));
$installer->addAttribute('invoice', 'base_sgst_charge', array('type' => 'decimal'));

$installer->addAttribute('creditmemo', 'sgst_charge', array('type' => 'decimal'));
$installer->addAttribute('creditmemo', 'base_sgst_charge', array('type' => 'decimal'));

$installer->addAttribute('quote_address', 'igst_charge', array('type' => 'decimal'));
$installer->addAttribute('quote_address', 'base_igst_charge', array('type' => 'decimal'));

$installer->addAttribute('order', 'igst_charge', array('type' => 'decimal'));
$installer->addAttribute('order', 'base_igst_charge', array('type' => 'decimal'));

$installer->addAttribute('invoice', 'igst_charge', array('type' => 'decimal'));
$installer->addAttribute('invoice', 'base_igst_charge', array('type' => 'decimal'));

$installer->addAttribute('creditmemo', 'igst_charge', array('type' => 'decimal'));
$installer->addAttribute('creditmemo', 'base_igst_charge', array('type' => 'decimal'));

$installer->endSetup();