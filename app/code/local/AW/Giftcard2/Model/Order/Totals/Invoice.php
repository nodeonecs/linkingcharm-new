<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_Model_Order_Totals_Invoice extends Mage_Sales_Model_Order_Invoice_Total_Abstract
{
    public function collect(Mage_Sales_Model_Order_Invoice $invoice)
    {
        $_result = parent::collect($invoice);

        $baseGrandTotal = $invoice->getBaseGrandTotal();
        $grandTotal = $invoice->getGrandTotal();

        $baseTotalGiftCardAmount = 0;
        $totalGiftCardAmount = 0;
        $invoiceGiftCards = array();

        if (!$invoice->getId()) {
            $orderGiftCards = Mage::helper('aw_giftcard2/giftcard')->getQuoteGiftCards($invoice->getOrder()->getQuoteId());
            foreach ($orderGiftCards as $orderGiftCard) {
                $baseGiftCardAmount = $orderGiftCard->getBaseGiftcardAmount();
                $giftCardAmount = $orderGiftCard->getGiftcardAmount();

                $orderId = $invoice->getOrderId();

                /** @var AW_Giftcard2_Model_Giftcard_Invoice $giftCardInvoice */
                foreach ($this->_getInvoicedGiftCardCodes($orderId, $orderGiftCard->getGiftcardId()) as $giftCardInvoice) {
                    $baseGiftCardAmount -= $giftCardInvoice->getBaseGiftcardAmount();
                    $giftCardAmount -= $giftCardInvoice->getGiftcardAmount();
                }

                if ($baseGiftCardAmount >= $baseGrandTotal) {
                    $baseGiftCardUsedAmount = $baseGrandTotal;
                } else {
                    $baseGiftCardUsedAmount = $baseGiftCardAmount;
                }
                $baseGrandTotal -= $baseGiftCardUsedAmount;

                if ($giftCardAmount >= $grandTotal) {
                    $giftCardUsedAmount = $grandTotal;
                } else {
                    $giftCardUsedAmount = $giftCardAmount;
                }
                $grandTotal -= $giftCardUsedAmount;

                $baseGiftCardAmount = round($baseGiftCardUsedAmount, 4);
                $giftCardAmount = round($giftCardUsedAmount, 4);

                $baseTotalGiftCardAmount += $baseGiftCardAmount;
                $totalGiftCardAmount += $giftCardAmount;

                if ($baseGiftCardAmount > 0) {
                    $_invoiceGiftCardCard = new Varien_Object($orderGiftCard->getData());
                    $_invoiceGiftCardCard
                        ->setBaseGiftcardAmount($baseGiftCardAmount)
                        ->setGiftcardAmount($giftCardAmount);
                    array_push($invoiceGiftCards, $_invoiceGiftCardCard);
                }
            }
        }
        if ($invoice->getId() && $invoice->getAwGiftCardsUsed()) {
            foreach ($invoice->getAwGiftCardsUsed() as $invoiceGiftCard) {
                $baseTotalGiftCardAmount += $invoiceGiftCard->getBaseGiftcardAmount();
                $totalGiftCardAmount += $invoiceGiftCard->getGiftcardAmount();
            }
        }
        $invoice
            ->setAwGiftCardsUsed($invoiceGiftCards)
            ->setBaseAwGCAmount($baseTotalGiftCardAmount)
            ->setAwGCAmount($totalGiftCardAmount)
            ->setBaseGrandTotal($invoice->getBaseGrandTotal() - $baseTotalGiftCardAmount)
            ->setGrandTotal($invoice->getGrandTotal() - $totalGiftCardAmount)
        ;
        return $_result;
    }

    /**
     * Retrieves already used giftcard's amounts in previous invoices for selected order
     *
     * @param Mage_Sales_Model_Order_Invoice $invoice
     * @param $giftCardId
     * @return AW_Giftcard2_Model_Resource_Giftcard_Invoice_Collection
     */
    protected function _getInvoicedGiftCardCodes($orderId, $giftCardId)
    {
        /** @var AW_Giftcard2_Model_Resource_Giftcard_Invoice_Collection $collection */
        $collection = Mage::getModel('aw_giftcard2/giftcard_invoice')->getCollection()
            ->setFilterByOrderId($orderId)
            ->setFilterByGiftCardId($giftCardId)
        ;
        return $collection;
    }
}