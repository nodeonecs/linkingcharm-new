<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_Helper_Config extends Mage_Core_Helper_Abstract
{
    const GENERAL_ENABLED                 = 'aw_giftcard2/general/enabled';
    const GENERAL_EXPIRE                  = 'aw_giftcard2/general/expire';
    const GENERAL_GIFTCARD_SCOPE          = 'aw_giftcard2/general/scope';

    const COUPON_CODE_LENGTH              = 'aw_giftcard2/coupon/code_length';
    const COUPON_CODE_FORMAT              = 'aw_giftcard2/coupon/code_format';
    const COUPON_CODE_PREFIX              = 'aw_giftcard2/coupon/code_prefix';
    const COUPON_CODE_SUFFIX              = 'aw_giftcard2/coupon/code_suffix';
    const COUPON_CODE_SEPARATOR           = 'aw_giftcard2/coupon/code_separator';
    const COUPON_CODE_DASH                = 'aw_giftcard2/coupon/code_dash';

    const NOTIFICATION_EMAIL_SENDER       = 'aw_giftcard2/email/sender';

    const ADMIN_CODE_IDENTIFIER           = 'aw_gc2_code';

    /**
     * @param null | Mage_Core_Model_Store $store
     *
     * @return bool
     */
    public static function isEnabled($store = null)
    {
        return (bool)Mage::getStoreConfig(self::GENERAL_ENABLED, $store) && (bool)Mage::helper('core')->isModuleOutputEnabled('AW_Giftcard2');
    }

    /**
     * @param null | Mage_Core_Model_Website $website
     *
     * @return int
     */
    public function getExpireValue($website = null)
    {
        if (null === $website) {
            $website = Mage::app()->getWebsite();
        }
        return $website->getConfig(self::GENERAL_EXPIRE);
    }

    /**
     * @return int
     */
    public function getGiftcardScope()
    {
        return (int)Mage::getStoreConfig(self::GENERAL_GIFTCARD_SCOPE);
    }

    /**
     * @param null | Mage_Core_Model_Store $store
     *
     * @return string
     */
    public function getEmailSender($store = null)
    {
        return Mage::getStoreConfig(self::NOTIFICATION_EMAIL_SENDER, $store);
    }

    /**
     * @param null | Mage_Core_Model_Store $store
     *
     * @return string
     */
    public function getEmailSenderName($store = null)
    {
        $sender = $this->getEmailSender($store);
        return Mage::getStoreConfig("trans_email/ident_{$sender}/name");
    }

    /**
     * @param null | Mage_Core_Model_Store $store
     *
     * @return string
     */
    public function getEmailSenderEmail($store = null)
    {
        $sender = $this->getEmailSender($store);
        return Mage::getStoreConfig("trans_email/ident_{$sender}/email");
    }

    public static function getAdminCodeIdentifier()
    {
        return self::ADMIN_CODE_IDENTIFIER;
    }

    /**
     * @param null | Mage_Core_Model_Website $website
     *
     * @return int
     */
    public function getCouponCodeLength($website = null)
    {
        if (null === $website) {
            $website = Mage::app()->getWebsite();
        }
        return $website->getConfig(self::COUPON_CODE_LENGTH);
    }

    /**
     * @param null | Mage_Core_Model_Website $website
     *
     * @return string
     */
    public function getCouponCodeFormat($website = null)
    {
        if (null === $website) {
            $website = Mage::app()->getWebsite();
        }
        return $website->getConfig(self::COUPON_CODE_FORMAT);
    }

    /**
     * @param null | Mage_Core_Model_Website $website
     *
     * @return string
     */
    public function getCouponCodePrefix($website = null)
    {
        if (null === $website) {
            $website = Mage::app()->getWebsite();
        }
        return $website->getConfig(self::COUPON_CODE_PREFIX);
    }

    /**
     * @param null | Mage_Core_Model_Website $website
     *
     * @return string
     */
    public function getCouponCodeSuffix($website = null)
    {
        if (null === $website) {
            $website = Mage::app()->getWebsite();
        }
        return $website->getConfig(self::COUPON_CODE_SUFFIX);
    }

    /**
     * @param null | Mage_Core_Model_Website $website
     *
     * @return string
     */
    public function getCouponCodeSeparator($website = null)
    {
        if (null === $website) {
            $website = Mage::app()->getWebsite();
        }
        return $website->getConfig(self::COUPON_CODE_SEPARATOR);
    }

    /**
     * @param null | Mage_Core_Model_Website $website
     *
     * @return int
     */
    public function getCouponCodeDash($website = null)
    {
        if (null === $website) {
            $website = Mage::app()->getWebsite();
        }
        return $website->getConfig(self::COUPON_CODE_DASH);
    }
}