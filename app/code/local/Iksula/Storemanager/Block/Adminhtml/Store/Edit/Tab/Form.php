<?php
/**
 * Iksula_Storemanager extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category       Iksula
 * @package        Iksula_Storemanager
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Store edit form tab
 *
 * @category    Iksula
 * @package     Iksula_Storemanager
 * @author      Ultimate Module Creator
 */
class Iksula_Storemanager_Block_Adminhtml_Store_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * prepare the form
     *
     * @access protected
     * @return Iksula_Storemanager_Block_Adminhtml_Store_Edit_Tab_Form
     * @author Ultimate Module Creator
     */



    public function getCustomRoleObj(){ // Returns the Current Login Permission for the amasty advanced Permission.

        $iLoginuserid = Mage::getSingleton('admin/session')->getUser()->getUserId();
        //$iRole_id = Mage::getSingleton('amrolepermissions/rule')->getRoleId();

        $LoginUserRole = Mage::getModel('admin/user')->load($iLoginuserid)->getRole()->getRoleId();
        //var_dump($role_data);
        $iRole_Collectionsid = Mage::getSingleton('amrolepermissions/rule')
                                                ->getCollection()
                                                ->addFieldToFilter('role_id' , array('eq' => $LoginUserRole));



                 foreach($iRole_Collectionsid as $Role){
                    return $Role;
                 }

    } // End of the function .



    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('store_');
        $form->setFieldNameSuffix('store');
        $this->setForm($form);

        $actionNameRegister = Mage::registry('action-name');

        if(isset($actionNameRegister)){

            $actionname = Mage::registry('action-name');

        }else{

            $actionname = Mage::app()->getRequest()->getActionName();

        }

        //echo $actionname ;



          //echo $actionname ;


        $fieldset = $form->addFieldset(
            'store_form',
            array('legend' => Mage::helper('iksula_storemanager')->__('Store'))
        );
         if($actionname == 'edit'){


            $fieldset->addField(
            'website_id',
            'select',
            array(
                'label'  => Mage::helper('iksula_storemanager')->__('Website'),
                'name'   => 'website_id',
                'required'  => true,
                'disabled' => true,
                //'class' => 'validate-one-required',
                'values' => Mage::getModel('iksula_storemanager/store_option_websites')->getOptionArray(),
            )
        );

            /*$fieldset->addType('label', 'Iksula_Customstoreinventory_Block_Adminhtml_Storeinventory_Edit_Renderer_Websitelabel');
             $fieldset->addField('website_id', 'label', array(
                'label'     => Mage::helper('iksula_storemanager')->__('Website'),
            ));*/

            $fieldset->addField(
            'store_code',
            'text',
            array(
                'label'  => Mage::helper('iksula_storemanager')->__('Store Code'),
                'name'   => 'store_code',
                'required'  => true,
                'class' => 'required-entry',
                 'disabled' => true,

            )
        );


             $fieldset->addField(
            'store_alias',
            'text',
                array(
                    'label'  => Mage::helper('iksula_storemanager')->__('Store Alias  (will be a username for login)'),
                    'name'   => 'store_alias',
                    'required'  => true,
                    'class' => 'required-entry',
                    'disabled' => true,
                )
            );



        }elseif($actionname == 'new'){

            //echo 'I am new';


             //$customroleObj = $this->getCustomRoleObj(); // Current Login object.
             //echo $customroleObj->getScopeWebsites();
            //exit;

             //if($customroleObj->getCustomRole() == 'W_HO'){ // Current login is Head Office .

                $fieldset->addField(
                    'website_id',
                    'select',
                    array(
                        'label'  => Mage::helper('iksula_storemanager')->__('Website'),
                        'name'   => 'website_id',

                        //'class' => 'validate-one-required',
                        'values' => Mage::getModel('iksula_storemanager/store_option_websites')->getOptionArray(),
                    )
                );

             /*}elseif($customroleObj->getCustomRole() == 'S_A'){ // Current login is Super Admin

                $fieldset->addField(
                    'website_id',
                    'select',
                    array(
                        'label'  => Mage::helper('iksula_storemanager')->__('Website'),
                        'name'   => 'website_id',
                        'required'  => true,
                        //'class' => 'validate-one-required',
                        'values' => Mage::getModel('iksula_storemanager/store_option_websites')->getOptionArray(),
                    )
                );

             }*/
             $fieldset->addField(
            'store_code',
            'text',
                array(
                    'label'  => Mage::helper('iksula_storemanager')->__('Store Code'),
                    'name'   => 'store_code',
                    'required'  => true,
                    'class' => 'required-entry',
                )
            );

              $fieldset->addField(
            'store_alias',
            'text',
                array(
                    'label'  => Mage::helper('iksula_storemanager')->__('Store Alias  (will be a username for login)'),
                    'name'   => 'store_alias',
                    'required'  => true,
                    'class' => 'required-entry',
                    'onchange' => 'getUsername(this)',
                )
            );
        }


        $fieldset->addField(
            'name',
            'text',
            array(
                'label' => Mage::helper('iksula_storemanager')->__('Store name'),
                'name'  => 'name',
                'required'  => true,
                'class' => 'required-entry',

           )
        );




        /*$state = $fieldset->addField(
            'store_state',
            'select',
            array(
                'label' => Mage::helper('iksula_storemanager')->__('Store state'),
                'name'  => 'store_state',
                'required'  => true,
                'class' => 'required-entry',
                'values'    => Mage::getModel('modulename/modulename')->getstate('IN'),

           )
        );


        $fieldset->addField(
            'store_city',
            'select',
            array(
                'label' => Mage::helper('iksula_storemanager')->__('Store city'),
                'name'  => 'store_city',
                'required'  => true,
                'class' => 'required-entry',

           )
        );*/


        if($actionname == 'new'){

            $country = $fieldset->addField('store_country', 'select', array(
                'name'  => 'store_country',
                'label'     => 'Country',
                'required'  => true,
                'values'    => Mage::getModel('adminhtml/system_config_source_country')->toOptionArray(),
                'onchange' => 'getstate(this)',
            ));

            $fieldset->addField(
                'store_state',
                'select',
                array(
                    'label' => Mage::helper('iksula_storemanager')->__('Store state'),
                    'name'  => 'store_state',
                    'required'  => true,
                    'class' => 'required-entry',
                    //'values'    => Mage::getModel('adminhtml/system_config_source_country')->toOptionArray(),
                    'onchange' => 'getCity(this)',
               )
            );


            $fieldset->addField(
            'store_city',
            'select',
            array(
                'label' => Mage::helper('iksula_storemanager')->__('Store city'),
                'name'  => 'store_city',
                'required'  => true,
                'class' => 'required-entry',

           )
        );


            $country->setAfterElementHtml("
    <script>


    function getUsername(username){

         var username_val = username.value;

         var reloadurl = '". $this->getUrl('adminhtml/storemanager_store/checkusername') . "username/' + username_val;

        new Ajax.Request(reloadurl, {
            method: 'get',
            onComplete: function(usernameresult) {
                if(usernameresult.responseText == '1'){
                alert('Username already exists');
                $('store_store_alias').clear();
                return false;
                }
            }
        });
    }

function getstate(selectElement){
//alert(selectElement.value);
var reloadurl = '". $this->getUrl('adminhtml/storemanager_store/state') . "country/' + selectElement.value;
//console.log(reloadurl);

new Ajax.Request(reloadurl, {
method: 'get',
onComplete: function(stateform) {
    //alert(stateform.responseText);
jQuery('#store_store_state').html(stateform.responseText);
}
});
}

function getCity(selectElement){
//alert(selectElement.value);
var reloadurl = '". $this->getUrl('adminhtml/storemanager_store/city') . "state/' + selectElement.value;
console.log(reloadurl);

new Ajax.Request(reloadurl, {
method: 'get',
onComplete: function(stateform) {
//    console.log(stateform.responseText);
jQuery('#store_store_city').html(stateform.responseText);
}
});
}
</script>");

            /*
* Add Ajax to the Country select box html output
*/

        }elseif($actionname == 'edit'){


            $fieldset->addType('label', 'Iksula_Storemanager_Block_Adminhtml_Renderer_Countrylabel');
             $fieldset->addField('store_country', 'label', array(
                'label'     => Mage::helper('iksula_storemanager')->__('Store Country'),
            ));

             $fieldset->addType('label', 'Iksula_Storemanager_Block_Adminhtml_Renderer_Statelabel');
             $fieldset->addField('store_state', 'label', array(
                'label'     => Mage::helper('iksula_storemanager')->__('Store State'),
            ));

             $fieldset->addType('label', 'Iksula_Storemanager_Block_Adminhtml_Renderer_Citylabel');
             $fieldset->addField('store_city', 'label', array(
                'label'     => Mage::helper('iksula_storemanager')->__('Store City'),
            ));



        }


        $fieldset->addField(
            'store_pincode',
            'text',
            array(
                'label' => Mage::helper('iksula_storemanager')->__('Store pincode'),
                'name'  => 'store_pincode',
                'required'  => true,
                'class' => 'required-entry',
                'maxlength' => 8

           )
        );


        $fieldset->addField(
            'store_longitude',
            'text',
            array(
                'label' => Mage::helper('iksula_storemanager')->__('Store longitude'),
                'name'  => 'store_longitude',
                'required'  => true,
                'class' => 'required-entry',

           )
        );


        $fieldset->addField(
            'store_latitude',
            'text',
            array(
                'label' => Mage::helper('iksula_storemanager')->__('Store latitude'),
                'name'  => 'store_latitude',
                'required'  => true,
                'class' => 'required-entry',

           )
        );

        if($actionname == 'edit'){

             $fieldset->addField(
                'store_definition',
                'text',
                array(
                    'label'  => Mage::helper('iksula_storemanager')->__('Store Definition'),
                    'name'   => 'store_definition',
                    'readonly' => true,
                )
            );

        }elseif($actionname == 'new'){

             $fieldset->addField(
                'store_definition',
                'select',
                array(
                    'label'  => Mage::helper('iksula_storemanager')->__('Store Definition'),
                    'name'   => 'store_definition',
                    'readonly' => false,
                    //'class' => 'validate-one-required',
                    'values' => array(
                        array(
                            'value' => 'store',
                            'label' => Mage::helper('iksula_storemanager')->__('Store'),
                        ),
                        array(
                            'value' => 'hub',
                            'label' => Mage::helper('iksula_storemanager')->__('Hub'),
                        ),
                        array(
                            'value' => 'warehouse',
                            'label' => Mage::helper('iksula_storemanager')->__('Warehouse'),
                        ),
                    ),
                )
            );
        }

         $fieldset->addType('label', 'Iksula_Storemanager_Block_Adminhtml_Renderer_Storeassigned');
             $fieldset->addField('store_assigned', 'label', array(
                'label'     => Mage::helper('iksula_storemanager')->__('Store Assigned'),
            ));


        $fieldset->addField(
            'store_typeid',
            'select',
            array(
                'label' => Mage::helper('iksula_storemanager')->__('Store Type'),
                'name'  => 'store_typeid',
                'required'  => true,
                'class' => 'validate-select',
                'values' => Mage::getModel('iksula_storemanager/store_option_storetype')->getOptionArray(),

           )
        );

        $fieldset->addField(
            'store_address',
            'textarea',
            array(
                'label' => Mage::helper('iksula_storemanager')->__('Store address'),
                'name'  => 'store_address',
                'required'  => true,
                'class' => 'required-entry',

           )
        );


        $fieldset->addField(
            'store_zone',
            'text',
            array(
                'label' => Mage::helper('iksula_storemanager')->__('Store zone'),
                'name'  => 'store_zone',
                'required'  => true,
                'class' => 'required-entry',

           )
        );

        $fieldset->addField(
            'store_vat',
            'text',
            array(
                'label' => Mage::helper('iksula_storemanager')->__('Store VAT'),
                'name'  => 'store_vat',
                'required'  => true,
                'class' => 'required-entry',

           )
        );


        $fieldset->addField(
            'store_tin',
            'text',
            array(
                'label' => Mage::helper('iksula_storemanager')->__('Store TIN'),
                'name'  => 'store_tin',
                'required'  => true,
                'class' => 'required-entry',

           )
        );

        $fieldset->addField(
            'store_cst',
            'text',
            array(
                'label' => Mage::helper('iksula_storemanager')->__('Store CST'),
                'name'  => 'store_cst',
                'required'  => true,
                'class' => 'required-entry',

           )
        );


        $fieldset->addField(
            'store_pan',
            'text',
            array(
                'label' => Mage::helper('iksula_storemanager')->__('Store PAN'),
                'name'  => 'store_pan',
                'required'  => true,
                'class' => 'required-entry',

           )
        );


        $fieldset->addField(
            'store_cin',
            'text',
            array(
                'label' => Mage::helper('iksula_storemanager')->__('Store CIN'),
                'name'  => 'store_cin',
                'required'  => true,
                'class' => 'required-entry',

           )
        );

        $fieldset->addField(
            'store_phone',
            'text',
            array(
                'label' => Mage::helper('iksula_storemanager')->__('Store phone for SMS updates'),
                'name'  => 'store_phone',
                'required'  => true,
                'class' => 'required-entry',

           )
        );

        $fieldset->addField(
            'store_email',
            'text',
            array(
                'label' => Mage::helper('iksula_storemanager')->__('Store email for Email updates'),
                'name'  => 'store_email',
                'required'  => true,
                'class' => 'required-entry',

           )
        );






        $field = $fieldset->addField(
            'active_status',
            'select',
            array(
                'label'  => Mage::helper('iksula_storemanager')->__('Status'),
                'name'   => 'active_status',
                'values' => array(
                    array(
                        'value' => 1,
                        'label' => Mage::helper('iksula_storemanager')->__('Active'),
                    ),
                    array(
                        'value' => 0,
                        'label' => Mage::helper('iksula_storemanager')->__('Inactive'),
                    ),
                ),
            )
        );




        $field->setAfterElementHtml('

             <script>
            var $j = jQuery.noConflict();
    $j(document).ready(function(){

        $j("#store_store_assigned > option").show();  // Show a Store Assigned Options when load

                var website_id = $j("#store_website_id").val();  // Get the website id selected.
             $j("#store_store_assigned > option[cust_website_id !="+website_id+"]").hide(); // Hide the respective stores according to the website id.

        $j("#store_website_id").on("change", function(){

            $j("#store_store_assigned > option").show(); //

            var website_id = $j(this).val();

             $j("#store_store_assigned > option[cust_website_id !="+website_id+"]").hide();

        });

         /***************Show the Content according to the Store Definition selected.(Load of the Page) ***********/
        if($j("#store_store_definition").val() == "store"){
            $j("#store_store_assigned").closest("tr").hide();
            $j("#store_store_assigned").removeClass("required-entry");
            $j.each([ 5 , 6 ], function( index, value ) {

                $j("#store_store_typeid > option[value="+value+"]").hide();
            });
        }else if($j("#store_store_definition").val() == "warehouse"){
                $j("#store_store_assigned").closest("tr").hide();
                $j("#store_store_assigned").removeClass("required-entry");

                $j.each([ 1 , 2 , 3 , 4  , 5 ], function( index, value ) {

                    $j("#store_store_typeid > option[value="+value+"]").hide();
                });
            $j("#store_store_assigned").closest("tr").show();
            $j("#store_store_assigned").addClass("required-entry");
        }else if($j("#store_store_definition").val() == "hub"){

            $j.each([ 1 , 2 , 3 , 4 , 6  ], function( index, value ) {

                $j("#store_store_typeid > option[value="+value+"]").hide();
            });
            $j("#store_store_assigned").closest("tr").show();
            $j("#store_store_assigned").addClass("required-entry");


        }


        /***************Show the Content according to the Store Definition selected.(Change of the selection) ***********/

        $j("#store_store_definition").change(function(){
            var store_def = $j("#store_store_definition").val();

            var store_typeval = " ";
//            console.log("#store_store_typeid option:eq(0)");
            //$j("#store_store_typeid:eq(0)").prop("selected", true);
            $j("#store_store_typeid option:eq(0)").attr("selected","selected");
            if(store_def == "store"){
                $j("#store_store_assigned").closest("tr").hide();
                $j("#store_store_assigned").removeClass("required-entry");
                $j("#store_store_typeid > option").show();
                $j.each([ 5 , 6 ], function( index, value ) {

                    $j("#store_store_typeid > option[value="+value+"]").hide();
                });
            }else if(store_def == "warehouse"){

                $j("#store_store_assigned").closest("tr").hide();
                $j("#store_store_assigned").removeClass("required-entry");
                $j("#store_store_typeid > option").show();
                $j.each([ 1 , 2 , 3 , 4  , 5 ], function( index, value ) {

                    $j("#store_store_typeid > option[value="+value+"]").hide();
                });
            }else if(store_def == "hub"){
                $j("#store_store_typeid > option").show();
                $j.each([ 1 , 2 , 3 , 4 , 6  ], function( index, value ) {

                    $j("#store_store_typeid > option[value="+value+"]").hide();
                });
                $j("#store_store_assigned").closest("tr").show();
                $j("#store_store_assigned").addClass("required-entry");
            }
        });
    });
</script>');

        if (Mage::app()->isSingleStoreMode()) {
            $fieldset->addField(
                'store_id',
                'hidden',
                array(
                    'name'      => 'stores[]',
                    'value'     => Mage::app()->getStore(true)->getId()
                )
            );
            Mage::registry('current_store')->setStoreId(Mage::app()->getStore(true)->getId());
        }

        $formValues = Mage::registry('current_store')->getDefaultValues();
        if (!is_array($formValues)) {
            $formValues = array();
        }
        if (Mage::getSingleton('adminhtml/session')->getStoreData()) {
            $formValues = array_merge($formValues, Mage::getSingleton('adminhtml/session')->getStoreData());
            Mage::getSingleton('adminhtml/session')->setStoreData(null);
        } elseif (Mage::registry('current_store')) {
            $formValues = array_merge($formValues, Mage::registry('current_store')->getData());
        }
        $form->setValues($formValues);
        return parent::_prepareForm();
    }
}
