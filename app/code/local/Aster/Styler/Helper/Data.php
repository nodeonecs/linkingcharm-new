<?php
class Aster_Styler_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function getProductTheme($option_id,$type)
	{
		$data = array();
		$collection = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect('*');
		$collection->addAttributeToFilter(array(array('attribute'=>'product_type','eq'=>$option_id)));

		foreach ($collection as $product) {
			$data[$product->getData('product_theme')] = $product->getResource()->getAttribute('product_theme')->getFrontend()->getValue($product);
			// $data['product_theme'][$product->getData('product_theme')] = $product->getResource()->getAttribute('product_theme')->getFrontend()->getValue($product);
			// $data['product_finish'][$product->getData('product_finish')] = $product->getResource()->getAttribute('product_finish')->getFrontend()->getValue($product);
			// $data['color'][$product->getData('color')] = $product->getResource()->getAttribute('color')->getFrontend()->getValue($product);
			// $data['max_price'][] = $product->getData('price');
			// $data['min_price'][] = $product->getData('price');
			// $data['price'][0] = min($data['min_price']);
			// $data['price'][1] = max($data['max_price']);
		}
		
 		return $data;
	}

	public function getProductFinish($option_id,$type)
	{
		$data = array();
		$collection = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect('*');
		$collection->addAttributeToFilter(array(array('attribute'=>'product_type','eq'=>$option_id)));

		foreach ($collection as $product) {
			$data[$product->getData('product_finish')] = $product->getResource()->getAttribute('product_finish')->getFrontend()->getValue($product);
		}
		
 		return $data;
	}

	public function getProductColors($option_id,$type)
	{
		$data = array();
		$collection = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect('*');
		$collection->addAttributeToFilter(array(array('attribute'=>'product_type','eq'=>$option_id)));

		foreach ($collection as $product) {
			$data[$product->getData('color')] = $product->getResource()->getAttribute('color')->getFrontend()->getValue($product);
		}
		
 		return $data;
	}


	public function getConfigOptions($configurableProduct,$sku,$qty)
	{

		if($configurableProduct->getId())
		{
			$_allChildren = $configurableProduct->getTypeInstance()->getUsedProductCollection()->addAttributeToSelect('sku');
			foreach ($_allChildren as $_child) 
			{
				$stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($_child['entity_id']);
				if($stockItem->getData('is_in_stock')==1)
				{
					$firstChildId = $_child->getId();
					break;
				}
			}


			$productAttributeOptions = $configurableProduct->getTypeInstance(true)->getConfigurableAttributesAsArray($configurableProduct);
			$options = array();

			foreach ($productAttributeOptions as $productAttribute) 
			{
				$allValues = array_column($productAttribute['values'], 'value_index');
				$_childProduct = Mage::getModel('catalog/product')->load($firstChildId);
				if($_childProduct->getId()){
					$currentProductValue = $_childProduct->getData($productAttribute['attribute_code']);
					if (in_array($currentProductValue, $allValues)) {
						$options[$productAttribute['attribute_id']] = $currentProductValue;
					}
				}
			}
			$params = array(
				'product_id' => $configurableProduct->getId(),
				'qty' => 1,
				'super_attribute' => $options
			);

			return $params;
		}
	}

	public function getConfigOptionsByProduct($configurableProduct)
	{
		$childOptionId = array();
		if($configurableProduct->getTypeId() == 'configurable' && $configurableProduct->getId())
		{
			$_allChildren = $configurableProduct->getTypeInstance()->getUsedProductCollection();
			foreach ($_allChildren as $_child) 
			{
				$stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($_child['entity_id']);
				if($stockItem->getData('is_in_stock')==1)
				{
					$childOptionId[] = $_child->getId();
				}
			}

			$productAttributeOptions = $configurableProduct->getTypeInstance(true)->getConfigurableAttributesAsArray($configurableProduct);
			$options = array();

			foreach ($productAttributeOptions as $productAttribute) 
			{
				$allValues = array_column($productAttribute['values'], 'value_index');
				foreach ($childOptionId as $firstChildId) {
					$_childProduct = Mage::getModel('catalog/product')->load($firstChildId);
					if($_childProduct->getId()){
						$currentProductValue = $_childProduct->getData($productAttribute['attribute_code']);
						if (in_array($currentProductValue, $allValues)) {
							// $options[$firstChildId][$productAttribute['attribute_id']] = $currentProductValue;
							$options[$firstChildId] = $_childProduct->getResource()->getAttribute('size')->getFrontend()->getValue($_childProduct);
						}
					}
				}
				
			}
		}

		return $options;
	}

	public function getConfigNewOptions($configurableProductId,$simpleProductId)
	{
		$configurableProduct = Mage::getModel('catalog/product')->load($configurableProductId);

		if($configurableProduct->getId() && $configurableProduct->getTypeId()=='configurable')
		{
			$_allChildren = $configurableProduct->getTypeInstance()->getUsedProductCollection()->addAttributeToSelect('sku');
			
			foreach ($_allChildren as $_child) 
			{
				$stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($_child['entity_id']);
				if($stockItem->getData('is_in_stock')==1)
				{
					$childOptionId[] = $_child->getId();
				}
			}


			$productAttributeOptions = $configurableProduct->getTypeInstance(true)->getConfigurableAttributesAsArray($configurableProduct);
			$options = array();

			foreach ($productAttributeOptions as $productAttribute) 
			{
				$allValues = array_column($productAttribute['values'], 'value_index');
				
				$_childProduct = Mage::getModel('catalog/product')->load($simpleProductId);
				if($_childProduct->getId()){
					$currentProductValue = $_childProduct->getData($productAttribute['attribute_code']);
					if (in_array($currentProductValue, $allValues)) {
						$options[$productAttribute['attribute_id']] = $currentProductValue;
					}
				}

				if(empty($simpleProductId)){
					foreach ($childOptionId as $firstChildId) {
					$_childProduct = Mage::getModel('catalog/product')->load($firstChildId);
						if($_childProduct->getId()){
							$currentProductValue = $_childProduct->getData($productAttribute['attribute_code']);
							if (in_array($currentProductValue, $allValues)) {
								$options[$productAttribute['attribute_id']] = $currentProductValue;
							}
						}
					}
				}

			}
			$params = array(
				'product_id' => $configurableProduct->getId(),
				'qty' => 1,
				'super_attribute' => $options
			);

			return $params;
		}else{
			return $params = array(
				'product_id' => $configurableProduct->getId(),
				'qty' => 1,
			);

		}
	}

	public function addToCartCustom($cart,$value)
	{
		try
		{			
			$_product = Mage::getModel('catalog/product')->load($value['product_id']);
			$_name = $_product->getName();
			$buyRequest = Mage::helper('styler')->getConfigNewOptions($value['product_id'],$value['size']);

			$cart->addProduct($_product, $buyRequest);

			$quoteMessageQty = $cart->getQuote()->getMessages();

			if (!$cart->getQuote()->getHasError() || $quoteMessageQty['qty'])
			{
				$cart->save();
				$message = $this->__('%s was added to your shopping cart.', Mage::helper('core')->htmlEscape($_product->getName()));
              // Mage::getSingleton('checkout/session')->addSuccess($message);
				$cartItemsCount = Mage::helper('checkout/cart')->getSummaryCount();
				$response['count'] = $cartItemsCount;
				$response['status'] = 'SUCCESS';
				$response['message'] = $message;
			}

		}
		catch (Mage_Core_Exception $e) 
		{      
			$cartItemsCount = Mage::helper('checkout/cart')->getSummaryCount();
			$response['count'] = $cartItemsCount;
			$response['status'] = 'ERROR';
			$response['message'] = $_name .' '.$e->getMessage();
			$jsonData[] = $response;
		}catch (Exception $e)
		{
			$cartItemsCount = Mage::helper('checkout/cart')->getSummaryCount();
			$response['count'] = $cartItemsCount;
			$response['status'] = 'ERROR';
			$response['message'] = $this->__('Cannot add the item to shopping cart.');
			$jsonData[] = $response;
        // Mage::logException($e);
		} 
		return $response;    
	}

	public function getHomeProducts()
	{
		$storeId = Mage::app()->getStore()->getStoreId();
		
		$categoryId = Mage::getStoreConfig('linkingcharm_config/general_config/home_product', $storeId);

		$category = Mage::getModel('catalog/category')->load($categoryId);

		$productCollection = $category->getProductCollection()->addAttributeToSelect('*');
		$productCollection->addUrlRewrite();
		$productCollection->getSelect()->limit(10);
		
        $response['category_name'] = $category->getName();
        $response['category_products'] = $productCollection;
		return $response;

	}
}
	 