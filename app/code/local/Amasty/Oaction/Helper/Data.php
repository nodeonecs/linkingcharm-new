<?php
/**
* @author Amasty Team
* @copyright Copyright (c) 2010-2011 Amasty (http://www.amasty.com)
* @package Amasty_Oaction
*/
class Amasty_Oaction_Helper_Data extends Mage_Core_Helper_Abstract
{

	public function updatePOStatus($event, $orderDetails){   // helper
		//array('order' => $orderObj, 'state' => $aOrderStatusStateChange['state'], 'status' => $aOrderStatusStateChange['status'])
        if($event == 'sales_order_status_after'){
        	$orderModel = $orderDetails['order'];
        	$status = $orderDetails['status'];
        	$orderId = $orderModel->getEntityId();
        	foreach ($orderModel->getAllItems() as $key => $subOrder) {
        		$itemId = $subOrder->getItemId();
        		$poData = Mage::getModel('omniapis/storepo')->getCollection()->addFieldToFilter('order_id',array('eq'=>$orderId))->addFieldToFilter('sub_orderid',array('eq'=>$itemId))->getData();
        		foreach ($poData as $pod) {
        			Mage::getModel('omniapis/storepo')->load($pod['id'])->setStatus($status)->save();
        		}
        	}
        }
    }
}