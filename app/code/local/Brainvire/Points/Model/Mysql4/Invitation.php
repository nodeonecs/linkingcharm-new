<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   Brainvire
 * @package    Brainvire_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * @license    
 */


class Brainvire_Points_Model_Mysql4_Invitation extends Mage_Core_Model_Mysql4_Abstract {

    public function _construct() {
        $this->_init('points/invitation', 'invitation_id');
        $this->_read = $this->_getReadAdapter();
    }

    
    public function loadByEmailAndStatus($invitatioin, $emailAddress, $status) {

        $select = $this->_read->select()
                ->from($this->getTable('points/invitation'))
                ->where("email=? ", $emailAddress)
                ->where("status=? ", $status);

        if ($data = $this->_read->fetchRow($select)) {
            $invitatioin->addData($data);
        }
        $this->_afterLoad($invitatioin);
        return $this;
    }

    
    public function loadByEmailAndStore($invitatioin, $emailAddress, $storeId) {

        $select = $this->_read->select()
                ->from($this->getTable('points/invitation'))
                ->where("email=? ", $emailAddress)
                ->where("store_id=? ", $storeId);

        if ($data = $this->_read->fetchRow($select)) {
            $invitatioin->addData($data);
        }
        $this->_afterLoad($invitatioin);
        return $this;
    }

    
    public function loadByEmail($invitatioin, $emailAddress) {
        $select = $this->_read->select()
                ->from($this->getTable('points/invitation'))
                ->where('email=?', $emailAddress);

        if ($data = $this->_read->fetchRow($select)) {
            $invitatioin->addData($data);
        }
        $this->_afterLoad($invitatioin);
        return $this;
    }

    
    public function loadByProtectionCode($invitatioin, $protectionCode) {
        $select = $this->_read->select()
                ->from($this->getTable('points/invitation'))
                ->where('protection_code=?', $protectionCode);

        if ($data = $this->_read->fetchRow($select)) {
            $invitatioin->addData($data);
        }
        $this->_afterLoad($invitatioin);
        return $this;
    }

    
    public function loadByCustomerAndEmail($invitatioin, $customer, $emailAddress) {

        $select = $this->_read->select()
                ->from($this->getTable('points/invitation'))
                ->where("email=? ", $emailAddress)
                ->where("customer_id=?", $customer->getId());

        if ($data = $this->_read->fetchRow($select)) {
            $invitatioin->addData($data);
        }
        $this->_afterLoad($invitatioin);
        return $this;
    }

    
    public function loadByReferralId($invitatioin, $referralId) {

        $select = $this->_read->select()
                ->from($this->getTable('points/invitation'))
                ->where("referral_id=?", $referralId);

        if ($data = $this->_read->fetchRow($select)) {
            $invitatioin->addData($data);
        }
        $this->_afterLoad($invitatioin);
        return $this;
    }

}
