<?php

class Rofra_Salesorderitemgrid_Block_Adminhtml_Sales_Order_Totals extends Mage_Adminhtml_Block_Sales_Order_Totals
{
    public function getSalesOrderInfo() {
        $orderId = $this->getRequest()->getParam('order_id');
        $order      = "";
        if(!empty($orderId)){
            $order = Mage::getModel('sales/order')->load($orderId);
        }
        return $order;
    }
    public function getTotalItemsInOrder() {
    	$orderId = $this->getRequest()->getParam('order_id');
        $orderObj = Mage::getModel('sales/order')->load($orderId);

        $countItems = 0;

        foreach($orderObj->getAllItems() as $items){
            if(strtolower($items->getProductType()) == 'simple'){

                $countItems ++;
            }

        }
		return $countItems;
    }
}
