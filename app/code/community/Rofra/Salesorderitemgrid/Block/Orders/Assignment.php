<?php

Class Rofra_Salesorderitemgrid_Block_Orders_Assignment extends Mage_Core_Block_Template{


/*	public $allocationstate = array('complete_allocation');
	public $unallocationstate = array('pending_allocation');*/

	public $allocationstatus = array('store_allocated');
	public $unallocationstatus = array('store_unallocated' , 'store_rejected' /*, 'cod_verified'*/ , 'processing');



	public function getOrderItemsCollections(){  // Returns the Order Items Object .

		$rowItemsId = Mage::app()->getRequest()->getParam('itemid');

		$aOrderItemCollection = Mage::getModel('sales/order_item')->load($rowItemsId);

		return $aOrderItemCollection;
	}

	public function getNonSellableStores(){ // Returns the Non Sellable store by the store_type table is_sellable fields .

		$aNotSellableStoreTypeId = Mage::getModel('storetype/storetype')->getCollection()
			->addFieldToFilter('is_sellable' , array('eq' => '0'))->addFieldToSelect(array('id'));
			foreach($aNotSellableStoreTypeId as $storetype){
				$aNotallowedStoreTypeId [] = $storetype->getId();
			}

			$sNotallowedStoreTypeId = implode("','" ,$aNotallowedStoreTypeId);

			return $sNotallowedStoreTypeId;


	}


	public function getStoresNotHubAssignedToHUb(){  // Get all stores Listing which is assigned to the any Hub

		$aStoresNotAssignedToHub = Mage::getModel('iksula_storemanager/storehubrelation')->getCollection()->addFieldToFilter('is_hub' , array('eq' => '0'))->addFieldToFilter('hub_id' , array('eq' => '0'))->addFieldToSelect('store_id');

			foreach($aStoresNotAssignedToHub as $stores => $storevalue){

				$aStoreNotAssignedHubValue [] = $storevalue->getStoreId();
			}

			$sStoreNotAssignedHubValue = implode("','" , $aStoreNotAssignedHubValue);

			return $sStoreNotAssignedHubValue;

	}



	public function getStoresList($OrderItemsObj){  //Returns the stores List if the Order items is not allocated , If allocated (returns the store name for that Order Items) .

		$store_id = $OrderItemsObj->getData('store_id');
		$website_id = $this->getWebsiteId($store_id);
		$storemodel = Mage::getModel('core/store_group');


		if(in_array($OrderItemsObj->getItemStatus() , $this->allocationstatus)){
			$assignedStoreCode = $OrderItemsObj->getData('store_code_1');
			$StoreName = $storemodel->load($assignedStoreCode)->getName();
			$aStores[$assignedStoreCode] = $StoreName;

		}else{

			$sNotallowedStoreTypeId = $this->getNonSellableStores();

			$aStoresNotAssignedToHub = Mage::getModel('iksula_storemanager/storehubrelation')->getCollection()->addFieldToFilter('is_hub' , array('eq' => '0'))->addFieldToFilter('hub_id' , array('eq' => '0'))->addFieldToSelect('store_id');

			foreach($aStoresNotAssignedToHub as $stores => $storevalue){

				$aStoreNotAssignedHubValue [] = $storevalue->getStoreId();
			}

				$sStoreNotAssignedHubValue = $this->getStoresNotHubAssignedToHUb();

				 /*$storecollectionlist = $storemodel->getCollection()->getSelect()->join(
				    'storeinventory',
				    'main_table.store_code = storeinventory.store_code',
				    array('main_table.group_id' , 'main_table.name' , 'storeinventory.inventory' )
				)->where("storeinventory.inventory != '0' and storeinventory.sku = '".$OrderItemsObj->getSku()."' and
				storeinventory.inventory >=".$OrderItemsObj->getQtyOrdered()."
				and main_table.website_id = ".$website_id."
				and main_table.store_typeid not in ('".$sNotallowedStoreTypeId."')
				and main_table.group_id not in ('".$sStoreNotAssignedHubValue."')
				 and main_table.default_store_id != 1 and main_table.active_status = 1");*/

				$storecollectionlist = $storemodel->getCollection()->getSelect()->join(
				    'storeinventory',
				    'main_table.store_code = storeinventory.store_code',
				    array('main_table.group_id' , 'main_table.name' , 'storeinventory.inventory' )
				)->where("storeinventory.inventory != '0' and storeinventory.sku = '".$OrderItemsObj->getSku()."' and
				storeinventory.inventory >=".$OrderItemsObj->getQtyOrdered()."
				and main_table.website_id = ".$website_id."
				and main_table.store_typeid not in ('".$sNotallowedStoreTypeId."')				
				and main_table.default_store_id != 1 and main_table.active_status = 1");
				//exit;


			$oStoresCollectionList = Mage::getSingleton('core/resource')->getConnection('core_read')->fetchAll($storecollectionlist);

			$aStores["-"] = "Please select your stores|0";
			foreach($oStoresCollectionList as $Stores){

				$aStores[$Stores['group_id']] = $Stores['name']."|".$Stores['inventory'];
			}

		}

		$aStores = array_unique($aStores);
		return $aStores;
	}



	public function getWebsiteId($store_id){ // Gives the Website_id by the store_id field present in the sales_flat_order_items.



		$website_id = Mage::getModel('core/store')->load($store_id)->getWebsiteId();

		return $website_id;

	}

	public function getWebsiteName($website_id){ // Returns the Website Name by the Website Id .

		$websiteName = Mage::app()->getWebsite($website_id)->getName();

		return $websiteName;


	}



	public function getTop5Stores($ItemsOrderobj){ /// Gives the Top 5 stores List which is connected to the Order Items by city , state , metro and larger quantity.

		$OrderId = $ItemsOrderobj->getOrderId();
		 $store_id = $ItemsOrderobj->getStoreId();
		 $website_id = $this->getWebsiteId($store_id);

		 if(!$store_id){
		 	echo 'Store Id is not found';
		 	exit;
		 }

		 if(!$website_id){
		 	echo 'Website Id is not found';
		 	exit;
		 }
		//exit;


		$metrocities = Mage::getStoreConfig('iksula_customstoreinventory/storeinventory/priority_metro_cities');

		$aMetroCitiesPriority = explode(',' , $metrocities);

		$sNotallowedStoreTypeId = $this->getNonSellableStores();

		$addressObj = Mage::getModel('sales/order_address')->load($OrderId , 'parent_id');


			$city = $addressObj->getCity();

			/*echo 'test';
			exit;*/
			$state = $addressObj->getRegion();
			if(isset($city))
				$city_id = Mage::getModel('romcity/romcity')->load($city ,'cityname')->getCityId();


			if(isset($state))
			$state_id = Mage::getModel('directory/region')->load($state ,'default_name')->getRegionId();

				$sStoreNotAssignedHubValue = $this->getStoresNotHubAssignedToHUb();

				 /*$StoreTopSkuCollections = Mage::getModel(
		    	'core/store_group')->getCollection()->getSelect()->join(
				    'storeinventory',
				    'main_table.store_code = storeinventory.store_code',
				    array('storeinventory.entity_id','main_table.group_id' , 'main_table.name' , 'storeinventory.inventory' )
				)->where("(storeinventory.inventory != '0'
				 and storeinventory.sku = '".$ItemsOrderobj->getSku()."'
				  and storeinventory.inventory >=".$ItemsOrderobj->getQtyOrdered()."
				  and main_table.website_id = ".$website_id."   
				  and main_table.store_typeid not in ('".$sNotallowedStoreTypeId."') 
				  and main_table.group_id not in ('".$sStoreNotAssignedHubValue."')
				 and main_table.default_store_id != 1 and main_table.active_status = 1)");*/

				$StoreTopSkuCollections = Mage::getModel(
		    	'core/store_group')->getCollection()->getSelect()->join(
				    'storeinventory',
				    'main_table.store_code = storeinventory.store_code',
				    array('storeinventory.entity_id','main_table.group_id' , 'main_table.name' , 'storeinventory.inventory' )
				)->where("(storeinventory.inventory != '0'
				 and storeinventory.sku = '".$ItemsOrderobj->getSku()."'
				  and storeinventory.inventory >=".$ItemsOrderobj->getQtyOrdered()."
				  and main_table.website_id = ".$website_id."   
				  and main_table.store_typeid not in ('".$sNotallowedStoreTypeId."') 				  
				 and main_table.default_store_id != 1 and main_table.active_status = 1)");

				//exit;




		    $StoreTopSkuCollections = Mage::getSingleton('core/resource')->getConnection('core_read')->fetchAll($StoreTopSkuCollections);
		    /*echo 'ksksk';
					exit;*/
		    //echo count($StoreTopSkuCollections);

		    foreach($StoreTopSkuCollections as $Stores){
		    	$aStoresSkuIds[] = $Stores['entity_id'];
				//$aStores[$Stores['group_id']] = $Stores['name']."|".$Stores['inventory'];
			}

			$sStoresSkuIds = implode(',' , $aStoresSkuIds);
			$aMainTop5Stores = array();

			if(!empty($aStoresSkuIds)){

				$aStoresStates= array();
				$aStoresCity= array();

				if(isset($city_id))
				$aStoresCity = $this->getCityStateFilter($sStoresSkuIds , $city_id , 'city');

				if(isset($state_id))
				$aStoresStates = $this->getCityStateFilter($sStoresSkuIds , $state_id , 'state');




				$aMainTop5Stores = array_merge($aStoresCity,$aStoresStates);
				$aMainTop5Stores = array_map("unserialize", array_unique(array_map("serialize", $aMainTop5Stores)));

				$aMainTop5Stores = array_slice($aMainTop5Stores , 0 , 5);
			}

			return $aMainTop5Stores;
	}


	public function getCityStateFilter($aStoresIds , $CityOrStateId , $defined){

		if($defined == 'city'){
			$storecityorstate = 'store_city';
		}else{
			$storecityorstate = 'store_state';
		}


			$StoreTopCityOrStateCollections = Mage::getModel('core/store_group')->getCollection()->getSelect()->join(
					    'storeinventory',
					    'main_table.store_code = storeinventory.store_code',
					    array('main_table.group_id' , 'main_table.name' , 'storeinventory.inventory' , 'main_table.store_city' , 'main_table.store_state' )
					)->where("storeinventory.entity_id in (".$aStoresIds.")
								 and main_table.".$storecityorstate." = ".$CityOrStateId);




					$StoreTopCityOrStateCollections = Mage::getSingleton('core/resource')->getConnection('core_read')->fetchAll($StoreTopCityOrStateCollections);

					$aStoresCityOrStates = array();
			    foreach($StoreTopCityOrStateCollections as $Stores){

					$aStoresCityOrStates[] = array($Stores['group_id'] => $Stores['store_city']."|".$Stores['store_state']."|".$Stores['name']."|".$Stores['inventory']);
				}


				return $aStoresCityOrStates;
	}

}
