<?php
require_once "Mage/Cms/controllers/IndexController.php";
class Localcms_Myroute_IndexController extends Mage_Cms_IndexController
{
    public function noRouteAction($coreRoute = null)
    {
    	if(Mage::getStoreConfig('myroute_section/myroute_group/myroute_field') == 1)
		{
			// START: Closed product 302 redirect - When products are taken offline they should 302 to the category page.  
			// echo $path=$this->getRequest()->getPathInfo(); die();
			$path=$this->getRequest()->getPathInfo();
			if (strpos($path,'catalog/product/view/id/') !== false) {
				
				$productid=str_replace('catalog/product/view/id/','',$path);
				if ($productid>0)
				{
					$product = Mage::getModel('catalog/product')->load($productid);
					
					$allIds = $product->getCategoryIds();
					
					foreach($allIds as $categoryId) {
						$category = Mage::getModel('catalog/category')->load($categoryId);
						echo $category->getUrl();
						header("Location: ".$category->getUrl(),TRUE,302);
						exit;	
					}
				}
			}
			else{
				header("Location: ".Mage::getBaseUrl(),TRUE,302);
				exit;
			}
			// END: Closed product 302 redirect - When products are taken offline they should 302 to the category page. 
			
	        $this->getResponse()->setHeader('HTTP/1.1','404 Not Found');
	        $this->getResponse()->setHeader('Status','404 File not found');
	
	        $pageId = Mage::getStoreConfig(Mage_Cms_Helper_Page::XML_PATH_NO_ROUTE_PAGE);
	        if (!Mage::helper('cms/page')->renderPage($this, $pageId)) {
	            $this->_forward('defaultNoRoute');
	        }
	    }
		else
		{
			parent::noRouteAction();
		}
    }
}
?>