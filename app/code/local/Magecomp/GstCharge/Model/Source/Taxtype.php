<?php
class Magecomp_GstCharge_Model_Source_Taxtype 
{
	public function toOptionArray()
    {
		$helper = Mage::helper('gstcharge');
		
        return array(
            array('value' => 1, 'label'=>$helper->__('Excluding Tax')),
            array('value' => 0, 'label'=>$helper->__('Including Tax')),
		);
    }
}
