<?php

class Rofra_Salesorderitemgrid_Block_Sales_Order_View extends Mage_Adminhtml_Block_Sales_Order_View {


	 public function  __construct() {

        parent::__construct();
        $order_id = $this->getOrder()->getId();
        $url = Mage::helper("adminhtml")->getUrl("adminhtml/order_items/pocancelcall", array("order_id" => $order_id));
        $printUrl = Mage::helper("adminhtml")->getUrl("adminhtml/order_items/poprintcall", array("order_id" => $order_id));

        $this->_addButton('po_cancelid', array(
            'label'     => Mage::helper('sales')->__('PO Cancel'),
            'onclick'   => 'setLocation(\''.$url.'\')',
            'class'     => 'go'
        ), 0, 100, 'header', 'header');

        $this->_addButton('po_print', array(
            'label'     => Mage::helper('sales')->__('Print PO'),
            'onclick'   => 'setLocation(\''.$printUrl.'\')',
            'class'     => 'go'
        ), 0, 100, 'header', 'header');
    }



}
