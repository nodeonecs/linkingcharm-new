<?php
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$setup->startSetup();

$setup->run("
	ALTER TABLE sales_flat_order_item ADD COLUMN pickup_start_date varchar(255) default NULL;
	ALTER TABLE sales_flat_order_item ADD COLUMN pickup_end_date varchar(255) default NULL;
");

$setup->endSetup();
