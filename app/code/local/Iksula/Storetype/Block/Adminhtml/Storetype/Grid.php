<?php

class Iksula_Storetype_Block_Adminhtml_Storetype_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("storetypeGrid");
				$this->setDefaultSort("id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("storetype/storetype")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("id", array(
				"header" => Mage::helper("storetype")->__("ID"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "number",
				"index" => "id",
				));

				$this->addColumn("name", array(
				"header" => Mage::helper("storetype")->__("Store type name"),
				"index" => "name",
				));


				$this->addColumn("name", array(
				"header" => Mage::helper("storetype")->__("Store type name"),
				"index" => "name",
				));



				//$yesnoOptions = array('0' => 'No','1' => 'Yes');

				   $this->addColumn('is_sellable', array(
				            'header'    => Mage::helper('storetype')->__('Is Sellable'),
				            'index'     => 'is_sellable',
				  			'type'      => 'options',
				  			'options'   => array('0' => 'No','1' => 'Yes'),
				        ));




				   $this->addColumn('fulfilment', array(
				            'header'    => Mage::helper('storetype')->__('Fulfilment'),
				            'index'     => 'fulfilment',
				  			'type'      => 'options',
				  			'options'   => array('0' => 'No','1' => 'Yes'),
				        ));


				   $this->addColumn('returns', array(
				            'header'    => Mage::helper('storetype')->__('Returns'),
				            'index'     => 'returns',
				  			'type'      => 'options',
				  			'options'   => array('0' => 'No','1' => 'Yes'),
				        ));
				/*$this->addColumn("status", array(
				"header" => Mage::helper("storetype")->__("Store Type Status"),
				"index" => "status",
				));*/
			$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
			$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}



		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('id');
			$this->getMassactionBlock()->setFormFieldName('ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_storetype', array(
					 'label'=> Mage::helper('storetype')->__('Remove Storetype'),
					 'url'  => $this->getUrl('*/adminhtml_storetype/massRemove'),
					 'confirm' => Mage::helper('storetype')->__('Are you sure?')
				));
			return $this;
		}


}
