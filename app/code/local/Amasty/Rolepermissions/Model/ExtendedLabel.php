<?php
class Amasty_Rolepermissions_Model_ExtendedLabel extends Varien_Data_Form_Element_Abstract{


	public function __construct($attributes=array())
    {
        parent::__construct($attributes);
        $this->setType('label');
    }

    public function getElementHtml()
    {
        $html = $this->getBold() ? '<strong>' : '';
        $html.= $this->getEscapedValue();
        $html.= $this->getBold() ? '</strong>' : '';
        $html.= $this->getAfterElementHtml();
        return $html;
    }

    public function getLabelHtml($idSuffix = ''){



        if (!is_null($this->getLabel())) {
            $html = '
            <script type="text/javascript">
				jQuery(document).ready(function(){
					var  = [];
					 jQuery("select[id ^= scope_storeviews] :selected").each(function(i, selected){
						var store_obj = jQuery(selected).closest("optgroup");
							test_val[i] = { "store_views_label":jQuery(selected).text() ,"store_label":store_obj.attr("label"), "website_label" : jQuery("optgroup").first().attr("label")};
					});

				});

            </script>

				'."\n";
        }
        else {
            $html = '';
        }
        return $html;
    }


}
