<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_AjaxController extends Mage_Core_Controller_Front_Action
{
    /*public function preDispatch()
    {
        if (!$this->_validateFormKey()) {
            $this->norouteAction();
            $this->setFlag('',self::FLAG_NO_DISPATCH,true);
            return $this;
        }
        return parent::preDispatch();
    }*/

    public function previewAction()
    {
        $data = $this->getRequest()->getPost();
        $storeId = Mage::app()->getStore()->getId();

        $response = new Varien_Object();
        $response->setSuccess(true);
        try {
            $content = Mage::getModel('aw_giftcard2/email_template')->getPreview($data, $storeId);
            $response->setContent($content);
        } catch (Exception $e) {
            $response->setSuccess(false);
            $response->setContent($e->getMessage());
        }
        $this->getResponse()->setBody($response->toJson());
        return;
    }

}