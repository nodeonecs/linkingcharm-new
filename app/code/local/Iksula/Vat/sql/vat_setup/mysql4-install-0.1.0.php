<?php
$installer = $this;
$installer->startSetup();

$installer->addAttribute("quote_address", "vattax", array("type"=>"decimal"));
$installer->addAttribute("quote_address", "base_vattax", array("type"=>"decimal"));
$installer->addAttribute("order", "vattax", array("type"=>"decimal"));
$installer->addAttribute("order", "base_vattax", array("type"=>"decimal"));
$installer->addAttribute("invoice", "vattax", array("type"=>"decimal"));
$installer->addAttribute("invoice", "base_vattax", array("type"=>"decimal"));
$installer->endSetup();

