<?php
class Magecomp_GstCharge_Model_Source_State 
{
	public function toOptionArray()
    {
		$helper = Mage::helper('gstcharge');
		
		$country_code = 'IN';
		
		if ($country_code != '') 
		{
			$statearray_val = Mage::getModel('directory/region')->getResourceCollection() ->addCountryFilter($country_code)->load();
			foreach ($statearray_val as $_stateVal) 
			{
				$state_data []= array('value' => $_stateVal->getRegionId(), 'label'=>$helper->__($_stateVal->getName()));
			}
		}
		
        return $state_data;
    }
}
