<?php
startSetup();
 
/* Get the customer entity type Id */
$entity = $this->getEntityTypeId('customer');
 
/* If the attribute exists */
if(!$this->attributeExists($entity, 'mobile')) 
{
    /* delete it */
    $this->removeAttribute($entity, 'mobile');
}
 
/* create the new attribute */
$this->addAttribute($entity, 'mobile', array(
        'type' => 'text',               /* input type */
        'label' => 'Mobile',    /* Label for the user to read */
        'input' => 'text',              /* input type */
        'visible' => TRUE,              /* users can see it */
        'required' => FALSE,            /* is it required, self-explanatory */
        'default_value' => 'default',   /* default value */
        'adminhtml_only' => '1'         /* use in admin html only */
));
 
/* save the setup */
$this->endSetup();
	 