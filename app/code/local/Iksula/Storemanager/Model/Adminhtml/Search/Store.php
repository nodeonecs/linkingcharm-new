<?php
/**
 * Iksula_Storemanager extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Iksula
 * @package        Iksula_Storemanager
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Admin search model
 *
 * @category    Iksula
 * @package     Iksula_Storemanager
 * @author      Ultimate Module Creator
 */
class Iksula_Storemanager_Model_Adminhtml_Search_Store extends Varien_Object
{
    /**
     * Load search results
     *
     * @access public
     * @return Iksula_Storemanager_Model_Adminhtml_Search_Store
     * @author Ultimate Module Creator
     */
    public function load()
    {
        $arr = array();
        if (!$this->hasStart() || !$this->hasLimit() || !$this->hasQuery()) {
            $this->setResults($arr);
            return $this;
        }
        $collection = Mage::getResourceModel('iksula_storemanager/store_collection')
            ->addFieldToFilter('store_type_id', array('like' => $this->getQuery().'%'))
            ->setCurPage($this->getStart())
            ->setPageSize($this->getLimit())
            ->load();
        foreach ($collection->getItems() as $store) {
            $arr[] = array(
                'id'          => 'store/1/'.$store->getId(),
                'type'        => Mage::helper('iksula_storemanager')->__('Store'),
                'name'        => $store->getStoreTypeId(),
                'description' => $store->getStoreTypeId(),
                'url' => Mage::helper('adminhtml')->getUrl(
                    '*/storemanager_store/edit',
                    array('id'=>$store->getId())
                ),
            );
        }
        $this->setResults($arr);
        return $this;
    }
}
