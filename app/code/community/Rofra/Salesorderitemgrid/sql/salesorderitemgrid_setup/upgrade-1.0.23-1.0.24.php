<?php
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$setup->startSetup();

$setup->run("
CREATE TABLE IF NOT EXISTS autoallocate_status(
  			id int not null auto_increment, item_id varchar(100) , current_allocated_stores varchar(100), current_accepted_stores varchar(100) , rejected_stores varchar(100) , primary key(id)
        )
");

$setup->endSetup();
