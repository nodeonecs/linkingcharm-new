

<?php


class Iksula_Storemanager_Block_Adminhtml_Store_Edit_Renderer_Statuslabel
extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract{


public function render(Varien_Object $row)
    {
    	$aStatusLabel = array('0' => 'Disabled' , '1' => 'Enabled');
        $rowItemId = $row->getId();
        $StoreStatusId = Mage::getModel('core/store_group')->load($rowItemId)->getStoreStatus();

        $sStoreStatusLabel = $aStatusLabel[$StoreStatusId];




        return $sStoreStatusLabel;
    }


}
