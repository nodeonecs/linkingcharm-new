<?php
class KP_Otp_Helper_Data extends Mage_Core_Helper_Abstract
{

  public function getIsModuleEnable(){
    return Mage::getStoreConfig('opt_general/credential/otp_enable');

  }

  public function getOtpApiUrl(){
    return Mage::getStoreConfig('opt_general/credential/api_url');

  }

  public function getOtpApiPassword(){
    return Mage::getStoreConfig('opt_general/credential/password');

  } 

  public function getOtpApiUsername(){
    return Mage::getStoreConfig('opt_general/credential/username');

  }

   public function getOtpApiDefaultText(){
    return Mage::getStoreConqfig('opt_general/credential/default_text');

  }


}
	 