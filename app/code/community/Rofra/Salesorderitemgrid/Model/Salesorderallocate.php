<?php

Class Rofra_Salesorderitemgrid_Model_Salesorderallocate extends Mage_Core_Model_Abstract{



/***********************************
			@function :- Allocate the Order items to stores in manual mode.

***************************************/

	public function saveAllocateData($orderitemid , $stores , $website_id = "1"){

		$StoreidCollections = Mage::getModel('core/store')->getCollection()
												->addFieldToFilter('group_id' , array('eq' => $stores));



		foreach($StoreidCollections as $storeidobj){

			$storeid = $storeidobj->getStoreId();
			break;
		}

		$OrdersData = Mage::getModel('sales/order_item')->load($orderitemid)->getData();
		$OrdersQty = $OrdersData['qty_ordered'];
		$OrdersProductSku = $OrdersData['sku'];
		$OrdersId = $OrdersData['order_id'];
		$websiteStoreId = $OrdersData['store_id'];

		$checkoutmethod = $OrdersData['checkout_method'];

		if(strtolower($checkoutmethod) == 'pickable'){
			$item_status = 'store_pickup_allocated';
			$verifyemailcheck = true;
		}elseif(strtolower($checkoutmethod) == 'deliverable'){
			$item_status = 'store_allocated';
			$verifyemailcheck = false;
		}
		/*$dataallocated = array('store_code' => $stores , 'item_status' => 'store_allocated'
			,'store_id' => $storeid , 'rejected_reason' => 'NULL', 'ITEMID'=>$orderitemid,'SKU'=>$OrdersProductSku,'QTY'=>round($OrdersQty));*/

			$dataallocated = array('store_code_1' => $stores , 'item_status' => $item_status
			,'store_1' => $storeid , 'rejected_reason' => 'NULL','store_id'=> $websiteStoreId,
			 'ITEMID'=>$orderitemid,'SKU'=>$OrdersProductSku,'QTY'=>round($OrdersQty));

		$OrderItemsStateStatus = json_decode(Mage::helper('orderitemsstatestatus')
			->getRelatedItemStatusandState('status' , $item_status,$dataallocated) , true);

		$dataallocated = array('store_code_1' => $stores , 'item_state' => $OrderItemsStateStatus['state'] , 'item_status' => $item_status ,'store_1' => $storeid , 'rejected_reason' => 'NULL');

		$ModelSaveAllocate = Mage::getModel('sales/order_item')->load($orderitemid)->addData($dataallocated);


		try{
			$ModelSaveAllocate->setId($orderitemid)->save();

			Mage::helper('orderitemsstatestatus')->changeOrderStatusState($OrdersId /*, array('state' => $OrderItemsStateStatus['order_state'] , 'status' =>  $OrderItemsStateStatus['order_status'])*/);
			return "Order is allocated";


		}catch(Exception $e){
			return $e->getMessage();
		}
	}

	/*****************************************


	Inventory Check for the Manual Allocation (Check if the stores has a inventory for that sku ).

	/********************************************/


	function getInventoryCheck($orderitemsId){
		$OrderItemsObj = Mage::getModel('sales/order_item')->load($orderitemsId);
		$store_id = $OrderItemsObj->getStoreId();
		$sku = $OrderItemsObj->getSku();
		$qtyOrdered = round($OrderItemsObj->getQtyOrdered());
		$Core_store = Mage::getModel('core/store')->load($store_id);
		$group_id = $Core_store->getGroupId();
		$Core_store_group = Mage::getModel('core/store_group')->load($group_id);
		$store_code = $Core_store_group->getStoreCode();

		 $InventoryCheckCollections = Mage::getModel('storeinventory/storeinventory')->getCollection()->addFieldToFilter('store_code' , array('eq' => $store_code))->addFieldToFilter('sku' , array('eq' => $sku))->addFieldToFilter('inventory' , array('gteq' => $qtyOrdered))->getSelect();


		 if(count($InventoryCheckCollections) >= 1){

		 	return true;
		 }

		 return false;


	}


/************************



 			@function :- Inventory Check for the autoallocation (Check if the Store has a inventory for that specific sku).

 ************************/
	function getInventoryCheckforautoallocation($orderitemsId , $storeId){
		$OrderItemsObj = Mage::getModel('sales/order_item')->load($orderitemsId);
		$sku = $OrderItemsObj->getSku();
		$qtyOrdered = round($OrderItemsObj->getQtyOrdered());
		/*$Core_store = Mage::getModel('core/store')->load($storeviewId);
		$group_id = $Core_store->getGroupId();*/
		$Core_store_group = Mage::getModel('core/store_group')->load($storeId);
		$store_code = $Core_store_group->getStoreCode();

			if(!$store_code){
				die('Store Code is empty');
			}

			if(!$sku){
				die('Sku is empty');
			}

		 $InventoryCheckCollections = Mage::getModel('storeinventory/storeinventory')->getCollection()->addFieldToFilter('store_code' , array('eq' => $store_code))->addFieldToFilter('sku' , array('eq' => $sku))->addFieldToFilter('inventory' , array('gteq' => $qtyOrdered));



		 /*echo $InventoryCheckCollections->getSelect();
		 exit;*/
		 if(count($InventoryCheckCollections) >= 1){

		 	return true;
		 }

		 return false;


	}


	/*****************************************************


	   @function :- Current Status for the stores for manual allocation


	 /**********************************************************/

	function getStoresActiveStatus($orderitemsId){

		$OrderItemsObj = Mage::getModel('sales/order_item')->load($orderitemsId);
		$store_id = $OrderItemsObj->getStoreId();
		$Core_store = Mage::getModel('core/store')->load($store_id);
		$group_id = $Core_store->getGroupId();
		$Core_store_group = Mage::getModel('core/store_group')->load($group_id);
		$store_status = $Core_store_group->getActiveStatus();



		if($store_status == '0'){

			return false;
		}else{
			return true;
		}



	}


	/***************************************************


		@function :- Current Status of Stores by autoallocation


	/*********************************************************/

	function getStoresActiveStatusforautoallocation($storeviewId){
		$Core_store = Mage::getModel('core/store')->load($storeviewId);
		$group_id = $Core_store->getGroupId();
		$Core_store_group = Mage::getModel('core/store_group')->load($group_id);
		$store_status = $Core_store_group->getActiveStatus();

		if($store_status == '0'){

			return false;
		}else{
			return true;
		}
	}


	/***********************************

			@function :- Unallocate the Order from the stores for manual mode .


	**********************************************/


	// public function saveUnallocateData($orderitemid , $stores , $website_id){
	// 	// $MainOrderId = Mage::getModel('sales/order_item')->load($orderitemid)->getOrderId();
	// 	// $OrderStoreId = Mage::getModel('sales/order')->load($MainOrderId)->getStoreId();

	// 	$DefaultGroupId = Mage::getModel('core/website')->load($website_id , 'website_id')->getDefaultGroupId();
	// 	$iDefaultStoreViewId = Mage::getModel('core/store_group')->load($DefaultGroupId , 'group_id')->getDefaultStoreId();
	// 	$OrdersData = Mage::getModel('sales/order_item')->load($orderitemid)->getData();
	// 	$OrdersQty = $OrdersData['qty_ordered'];
	// 	$OrdersProductSku = $OrdersData['sku'];
	// 	$OrdersId = $OrdersData['order_id'];
	// 	$dataunalloacted = array('store_code' => $stores , /*'store_id' => $storeid ,*/ 'rejected_reason' => 'NULL', 'ITEMID'=>$orderitemid,'SKU'=>$OrdersProductSku,'QTY'=>round($OrdersQty));

	// 	$OrderItemsStateStatus = json_decode(Mage::helper('orderitemsstatestatus')->getRelatedItemStatusandState('status' , 'store_unallocated', $dataunalloacted) , true);

	// 	$dataunalloacted = array('store_code' => '0' , 'item_state' => $OrderItemsStateStatus['state'] , 'store_id' => $iDefaultStoreViewId , 'item_status' => 'store_unallocated' , 'rejected_reason' => 'NULL');

	// 	$ModelSaveUnallocate = Mage::getModel('sales/order_item')->load($orderitemid)->addData($dataunalloacted);

	// 	try{
	// 		$ModelSaveUnallocate->setId($orderitemid)->save();

	// 				Mage::helper('orderitemsstatestatus')->changeOrderStatusState($OrdersId /*, array('state' => $OrderItemsStateStatus['order_state'] , 'status' =>  $OrderItemsStateStatus['order_status'])*/);

	// 		return "Order is unallocated";

	// 	}catch(Exception $e){
	// 		return $e->getMessage();
	// 	}
	// }



	public function saveUnallocateData($orderitemid , $stores , $website_id){
		// $MainOrderId = Mage::getModel('sales/order_item')->load($orderitemid)->getOrderId();
		// $OrderStoreId = Mage::getModel('sales/order')->load($MainOrderId)->getStoreId();
		$DefaultGroupId = Mage::getModel('core/website')->load($website_id , 'website_id')->getDefaultGroupId();
		$iDefaultStoreViewId = Mage::getModel('core/store_group')->load($DefaultGroupId , 'group_id')->getDefaultStoreId();
		$OrdersData = Mage::getModel('sales/order_item')->load($orderitemid)->getData();
		$OrdersQty = $OrdersData['qty_ordered'];
		$OrdersProductSku = $OrdersData['sku'];
		$OrdersId = $OrdersData['order_id'];
		$websiteStoreId = $OrdersData['store_id'];
		$store_1 = $OrdersData['store_1'];

		$dataunalloacted = array('store_code_1' => $stores ,'store_id' => $websiteStoreId ,'store_1' => $store_1, 'rejected_reason' => 'NULL', 'ITEMID'=>$orderitemid,'SKU'=>$OrdersProductSku,'QTY'=>round($OrdersQty));

		$OrderItemsStateStatus = json_decode(Mage::helper('orderitemsstatestatus')->getRelatedItemStatusandState('status' , 'store_unallocated', $dataunalloacted) , true);

		$dataunalloacted = array('store_code_1' => 'NULL' , 'store_code_2' => 'NULL' , 'store_code_3' => 'NULL' , 'store_code_4' => 'NULL' , 'store_code_5' => 'NULL' , 'store_1' => 'NULL' , 'store_2' => 'NULL' , 'store_3' => 'NULL' , 'store_4' => 'NULL' , 'store_5' => 'NULL' , 'item_state' => $OrderItemsStateStatus['state'] , 'store_id' => $iDefaultStoreViewId , 'item_status' => 'store_unallocated' , 'rejected_reason' => 'NULL');

		$ModelSaveUnallocate = Mage::getModel('sales/order_item')->load($orderitemid)->addData($dataunalloacted);

		try{
			$ModelSaveUnallocate->setId($orderitemid)->save();

					Mage::helper('orderitemsstatestatus')->changeOrderStatusState($OrdersId /*, array('state' => $OrderItemsStateStatus['order_state'] , 'status' =>  $OrderItemsStateStatus['order_status'])*/);

			return "Order is unallocated";

		}catch(Exception $e){
			return $e->getMessage();
		}
	}


	/**********************


	@function :- Save the Rejected Reason for the rejection by the stores

	****************************************/

	public function saverejectedreason($storerejectedreason , $orderitemid){

		// $MainOrderId = Mage::getModel('sales/order_item')->load($orderitemid)->getOrderId();
		// $OrderStoreId = Mage::getModel('sales/order')->load($MainOrderId)->getStoreId();

		//$StateCode = $this->getRelatedItemStatusandState('status' , 'store_rejected');
		$OrdersData = Mage::getModel('sales/order_item')->load($orderitemid)->getData();
		$OrdersQty = $OrdersData['qty_ordered'];
		$OrdersProductSku = $OrdersData['sku'];
		$OrdersId = $OrdersData['order_id'];
		$websiteStoreId = $OrdersData['store_id'];
		$store_1 = $OrdersData['store_1'];

		$reasonData = array('ITEMID'=>$orderitemid,'store_id' => $websiteStoreId,'SKU'=>$OrdersProductSku,'QTY'=>round($OrdersQty), 'REJECTEDREASON'=>$storerejectedreason);

		$OrderItemsStateStatus = json_decode(Mage::helper('orderitemsstatestatus')->getRelatedItemStatusandState('status' , 'store_rejected', $reasonData) , true);


		//$data = array( 'assigned_status' => '3' , 'rejected_reason' => 'NULL' /*, 'is_rejected' => '0'*/);
		$datareject = array( 'item_state' => $OrderItemsStateStatus['state'] , 'item_status' => 'store_rejected' , 'rejected_reason' => $storerejectedreason);

		try{
			$ModelRejectedReason = Mage::getModel('sales/order_item')->load($orderitemid)->addData($datareject);
			$ModelRejectedReason->setId($orderitemid)->save();
			Mage::helper('orderitemsstatestatus')->changeOrderStatusState($OrdersId /*, array('state' => $OrderItemsStateStatus['order_state'] , 'status' =>  $OrderItemsStateStatus['order_status'])*/);


			return array('result'=> '1' , 'message' => 'Reason is added');
		}catch(Exception $e){
			return array('result'=> '0' , 'message' => $e->getMessage());

		}
	}

	public function saverejectedStatusautoallocated($storerejectedreason , $orderitemid){
		
		// $MainOrderId = Mage::getModel('sales/order_item')->load($orderitemid)->getOrderId();
		$OrdersData = Mage::getModel('sales/order_item')->load($orderitemid)->getData();
		$store_view_id = $OrdersData['store_1'];
		$OrdersQty = $OrdersData['qty_ordered'];
		$OrdersProductSku = $OrdersData['sku'];
		$OrdersId = $OrdersData['order_id'];
		$checkoutmethod = $OrdersData['checkout_method'];
		$websiteStoreId = $OrdersData['store_id'];
		$store_code_1 = $OrdersData['store_code_1'];

		if(strtolower($checkoutmethod) == 'pickable'){
			$item_status = 'store_pickup_rejected';
		}elseif(strtolower($checkoutmethod) == 'deliverable'){
			$item_status = 'store_rejected';
		}

		$reasonData = array('ITEMID'=>$orderitemid,'store_id' => $websiteStoreId,'store_1'=>$store_view_id, 'store_code_1'=>$store_code_1,'SKU'=>$OrdersProductSku,'QTY'=>round($OrdersQty), 'REJECTEDREASON'=>$storerejectedreason,'autoallocation' => true, 'item_status'=>$item_status);


		$StoreId  = Mage::getModel('core/store')->load($store_view_id)->getGroupId();
		$storeIdCode = Mage::getModel('core/store_group')->load($StoreId)->getStoreCode();


		$Collections = Mage::getModel('storeinventory/storeinventory')->getCollection()->addFieldToFilter('sku' , array('eq' => $OrdersProductSku))->addFieldToFilter('store_code' , array('eq' => $storeIdCode));

		if($Collections ){

			foreach($Collections as $storecollections){

					$updatedInventory = (int)$storecollections->getInventory() + $OrdersQty;
					$inventoryData = array('inventory' => $updatedInventory);
					$ModelStoreInventory = Mage::getModel('storeinventory/storeinventory')->load($storecollections->getEntityId())->addData($inventoryData);
					$ModelStoreInventory->setId($storecollections->getEntityId())->save();

			}

		}

		$OrderItemsStateStatus = json_decode(Mage::helper('orderitemsstatestatus')->getRelatedItemStatusandState('status' , $item_status, $reasonData) , true);
		/*echo 'Done';
		exit;*/

		$datareject = array( 'item_state' => $OrderItemsStateStatus['state'] , 'item_status' => $item_status , 'rejected_reason' => $storerejectedreason , 'store_1' => 'NULL' , 'store_code_1' => 'NULL' , 'accepted_store' => NULL);

		try{
			$ModelRejectedReason = Mage::getModel('sales/order_item')->load($orderitemid)->addData($datareject);
			$ModelRejectedReason->setId($orderitemid)->save();
			Mage::helper('orderitemsstatestatus')->changeOrderStatusState($OrdersId);

			$AllocateItems = Mage::getModel('salesorderitemgrid/autoallocatestatus')->load($orderitemid , 'item_id');
      		$allocateid = $AllocateItems->getId();
      		$sRejectedStores = $AllocateItems->getRejectedStores();

		      if($sRejectedStores != 'NULL' && $sRejectedStores !== NULL){
		        $aRejectedStores =  explode(',' , $sRejectedStores);
		      }
		      else{
		        $aRejectedStores = array();
		      }


		      $aRejectedStores [] = $store_view_id;
		      $aRejectedStores = array_unique($aRejectedStores);


		      $sRejected_stores = implode(',' , $aRejectedStores);
		      $datareject = array('rejected_stores' => $sRejected_stores);

		      $ModelRejectedStores = Mage::getModel('salesorderitemgrid/autoallocatestatus')->load($allocateid)->addData($datareject);
				$ModelRejectedStores->setId($allocateid)->save();


			return array('result'=> '1' , 'message' => 'Reason is added');
		}catch(Exception $e){
			return array('result'=> '0' , 'message' => $e->getMessage());

		}
	}


	/*****************************


		@function :- Accepted by stores for the Order items for manual mode


		****************************************/

	public function saveAcceptedOrder($orderitemid){

		//$StateCode = $this->getRelatedItemStatusandState('status' , 'store_accepted');
		$OrdersData = Mage::getModel('sales/order_item')->load($orderitemid)->getData();
		$OrdersQty = $OrdersData['qty_ordered'];
		$OrdersProductSku = $OrdersData['sku'];
		$OrdersId = $OrdersData['order_id'];
		$store_view_id = $OrdersData['store_1'];
		$websiteStoreId = $OrdersData['store_id'];
		$store_code_1 = $OrdersData['store_code_1'];
		$dataAccepted = array('store_id' => $websiteStoreId,'store_1'=>$store_view_id, 'store_code_1'=>$store_code_1,'ITEMID'=>$orderitemid,
			'SKU'=>$OrdersProductSku,'QTY'=>round($OrdersQty),'item_status'=> 'store_accepted');

		$OrderItemsStateStatus = json_decode(Mage::helper('orderitemsstatestatus')->getRelatedItemStatusandState('status' , 'store_accepted',$dataAccepted) , true);
		//$data = array( 'assigned_status' => '3' , 'rejected_reason' => 'NULL' /*, 'is_rejected' => '0'*/);
		$data = array( 'item_state' => $OrderItemsStateStatus['state'] , 'item_status' => 'store_accepted' , 'rejected_reason' => 'NULL');
		$ModelAcceptedStores = Mage::getModel('sales/order_item')->load($orderitemid)->addData($data);

		try{
			$ModelAcceptedStores->setId($orderitemid)->save();
			Mage::helper('orderitemsstatestatus')->changeOrderStatusState($OrdersId /*, array('state' => $OrderItemsStateStatus['order_state'] , 'status' =>  $OrderItemsStateStatus['order_status'])*/);
			return array('result'=> '1' , 'message' => 'Order is accepted');
		}catch(Exception $e){
			return array('result'=> '0' , 'message' => $e->getMessage());

		}


	}


	/***********************************************

		@function :-  Accepted by stores for the order items for autoallocation mode


		*******************************************/

	public function saveAutoallocateAcceptedOrder($orderitemid , $storeviewid){

		$OrdersData = Mage::getModel('sales/order_item')->load($orderitemid)->getData();
		$OrdersQty = $OrdersData['qty_ordered'];
		$OrdersProductSku = $OrdersData['sku'];
		$OrdersId = $OrdersData['order_id'];
		$checkoutmethod = $OrdersData['checkout_method'];
		$websiteStoreId = $OrdersData['store_id'];

		$StoreId  = Mage::getModel('core/store')->load($storeviewid)->getGroupId();
		$storeIdCode = Mage::getModel('core/store_group')->load($StoreId)->getStoreCode();

		if(strtolower($checkoutmethod) == 'pickable'){
			$item_status = 'store_pickup_accepted';
			$result = Mage::helper('salesorderitemgrid')->getPickupDate();
			$pickup_start_date = $result['start_date'];
			$pickup_end_date = $result['end_date'];
			$verifyemailcheck = true;
		}elseif(strtolower($checkoutmethod) == 'deliverable'){
			$item_status = 'store_accepted';
			$pickup_start_date = NULL;
			$pickup_end_date = NULL;
			$verifyemailcheck = false;
		}

		$dataAccepted = array('ITEMID'=>$orderitemid,'SKU'=>$OrdersProductSku,
			'QTY'=>round($OrdersQty),'store_id'=>$websiteStoreId,'store_1' => $storeviewid
			,'store_code_1' => $StoreId,'autoallocation'=>true,'item_status'=>$item_status 
			, 'pickup_start_date' => $pickup_start_date, 'pickup_end_date' => $pickup_end_date);		
		
		$Collections = Mage::getModel('storeinventory/storeinventory')->getCollection()
		->addFieldToFilter('sku' , array('eq' => $OrdersProductSku))->addFieldToFilter('store_code' , array('eq' => $storeIdCode));
		if($Collections){

			foreach($Collections as $storecollections){
					$updatedInventory = (int)$storecollections->getInventory() - $OrdersQty;
					$inventoryData = array('inventory' => $updatedInventory);
					$ModelStoreInventory = Mage::getModel('storeinventory/storeinventory')->load($storecollections->getEntityId())->addData($inventoryData);
					$ModelStoreInventory->setId($storecollections->getEntityId())->save();
			}

		}

		$orderitemsObj = Mage::getModel('sales/order_item')->load($orderitemid);

		$OrderItemsStateStatus = json_decode(Mage::helper('orderitemsstatestatus')->getRelatedItemStatusandState('status' , $item_status, $dataAccepted) , true);
		/*echo 'done';
		exit;*/

		$data = array( 'item_state' => $OrderItemsStateStatus['state'] , 'item_status' => $item_status , 'rejected_reason' => 'NULL' , 'store_1' => $storeviewid , 'store_2' => 'NULL' , 'store_3' => 'NULL' , 'store_4' => 'NULL' , 'store_5' => 'NULL' , 'store_code_1' => $StoreId , 'store_code_2' => 'NULL' , 'store_code_3' => 'NULL' , 'store_code_4' => 'NULL' , 'store_code_5' => 'NULL' , 'accepted_store' => $storeviewid , 'pickup_start_date' => $pickup_start_date, 'pickup_end_date' => $pickup_end_date );
		$ModelAcceptedStores = Mage::getModel('sales/order_item')->load($orderitemid)->addData($data);
		try{
			$ModelAcceptedStores->setId($orderitemid)->save();
			Mage::helper('orderitemsstatestatus')->changeOrderStatusState($OrdersId , $verifyemailcheck );
			return array('result'=> '1' , 'message' => 'Order is accepted');
		}catch(Exception $e){
			return array('result'=> '0' , 'message' => $e->getMessage());
		}

	}


	/******************************************************


		@function :- Transfer to hub after accept by stores by manual mode.

	*******************************************************/


	public function savetransfertohub($hub_id , $orderitemid ){
		$OrdersData = Mage::getModel('sales/order_item')->load($orderitemid)->getData();
		$OrdersQty = $OrdersData['qty_ordered'];
		$OrdersProductSku = $OrdersData['sku'];
		$OrdersId = $OrdersData['order_id'];
		$store_view_id = $OrdersData['store_1'];
		$websiteStoreId = $OrdersData['store_id'];
		$store_code_1 = $OrdersData['store_code_1'];
		$dataTransferToHub = array('store_code_hub' => $hub_id ,'store_1'=>$store_view_id, 'store_id'=>$websiteStoreId, 'store_code_1'=>$store_code_1, 'ITEMID'=>$orderitemid,'SKU'=>$OrdersProductSku,'QTY'=>round($OrdersQty));


		//$StateCode = $this->getRelatedItemStatusandState('status' , 'out_for_hub');
		$StateCode = json_decode(Mage::helper('orderitemsstatestatus')->getRelatedItemStatusandState('status' , 'out_for_hub', $dataTransferToHub) , true);
		$data = array( 'is_transfertohub' => '1' , 'hub_id' => $hub_id , 'item_state' => $StateCode['state'] , 'item_status' => 'out_for_hub');
		$ModelAcceptedStores = Mage::getModel('sales/order_item')->load($orderitemid)->addData($data);

		try{
			$ModelAcceptedStores->setId($orderitemid)->save();
			return array('result'=> '1' , 'message' => 'Transfered to hub');
		}catch(Exception $e){
			return array('result'=> '0' , 'message' => $e->getMessage());

		}



	}


	/************************************************


		@function :- Action By hub (Accept and Reject)  for the Manual mode


	/*********************************************************/


	public function actionbyhub($orderitemid , $action ){
		$OrdersData = Mage::getModel('sales/order_item')->load($orderitemid)->getData();
		$OrdersQty = $OrdersData['qty_ordered'];
		$OrdersProductSku = $OrdersData['sku'];
		$OrdersId = $OrdersData['order_id'];
		$store_view_id = $OrdersData['store_1'];
		$websiteStoreId = $OrdersData['store_id'];
		$store_code_1 = $OrdersData['store_code_1'];
		$dataTransferToHub = array('store_code_hub' => $hub_id ,'store_1'=>$store_view_id, 'store_id'=>$websiteStoreId, 'store_code_1'=>$store_code_1, 'ITEMID'=>$orderitemid,'SKU'=>$OrdersProductSku,'QTY'=>round($OrdersQty));

		if($action){

			if($action == 'accept'){

				$message = 'Hub has accepted the order';

				$StateCode = json_decode(Mage::helper('orderitemsstatestatus')->getRelatedItemStatusandState('status' , 'received_at_hub', $dataTransferToHub) , true);
				$data = array( 'is_transfertohub' => '1' , 'hub_id' => $hub_id , 'item_state' => $StateCode['state'] , 'item_status' => 'received_at_hub');
			}elseif($action == 'reject'){
				$message = 'Hub has rejected the order';

				$StateCode = json_decode(Mage::helper('orderitemsstatestatus')->getRelatedItemStatusandState('status' , 'rejected_by_hub', $dataTransferToHub) , true);
				$data = array( 'is_transfertohub' => '1' , 'hub_id' => $hub_id , 'item_state' => $StateCode['state'] , 'item_status' => 'rejected_by_hub');

			}

		}



		$ModelHubs = Mage::getModel('sales/order_item')->load($orderitemid)->addData($data);

		try{
			$ModelHubs->setId($orderitemid)->save();
			return array('result'=> '1' , 'message' => $message);
		}catch(Exception $e){
			return array('result'=> '0' , 'message' => $e->getMessage());

		}



	}

	public function getAcceptedCheckforOrder($orderitemid , $storeviewid){

		$orderitemobj = Mage::getModel('sales/order_item')->load($orderitemid);

		$orderstores = Mage::getModel('sales/order_item')->getCollection()
									->addFieldToFilter('item_id' , array('eq' => $orderitemid))
									->addFieldToFilter(
													array('store_1','store_2' , 'store_3' , 'store_4' , 'store_5'),
													array($storeviewid,$storeviewid , $storeviewid, $storeviewid , $storeviewid)
													);

		if(count($orderstores) == 0){

			return true;
		}else{

			return false;
		}
	}

}
