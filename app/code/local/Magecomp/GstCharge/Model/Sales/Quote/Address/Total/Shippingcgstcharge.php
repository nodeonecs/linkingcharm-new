<?php

class Magecomp_GstCharge_Model_Sales_Quote_Address_Total_Shippingcgstcharge extends Mage_Sales_Model_Quote_Address_Total_Abstract
{
	const GSTCHARGE_SHIPPINGSETTINGS_ENABLED   =  'gstcharge/shippingsettings/enabled';
	const GSTCHARGE_SHIPPINGSETTINGS_TAXTYPE   =  'gstcharge/shippingsettings/taxtype';
	const GSTCHARGE_OPTIONS_STATE              =  'gstcharge/options/state';
	
	public function __construct()
    {
        $this->setCode('shipping_cgst');
    }
	
	public function collect(Mage_Sales_Model_Quote_Address $address) 
	{
		$address->setPercentShippingCgstCharge(0);
		$address->setShippingCgstCharge(0);
		if(Mage::getStoreConfig(self::GSTCHARGE_SHIPPINGSETTINGS_ENABLED)):
			$cart = Mage::getSingleton('checkout/session')->getQuote();
			$countryId = $address->getCountryId();
			$customerRegionId = $address->getRegionId();
			$systemRegionId = Mage::getStoreConfig(self::GSTCHARGE_OPTIONS_STATE);
			
			$maxGstPercent = $gstPercent = $shippingGst = 0;
			foreach ($cart->getAllVisibleItems() as $item) 
			{
				if($countryId == 'IN' && $customerRegionId==$systemRegionId)
				{
					$gstPercent = $item->getCgstPercent();
				}
				if ($gstPercent > $maxGstPercent)
					$maxGstPercent = $gstPercent;
			}
			$shippingGst = $address->getShippingAmount() * ($maxGstPercent/100);
			$address->setPercentShippingCgstCharge($maxGstPercent);
			$address->setShippingCgstCharge($shippingGst/2);
			
			
			if(Mage::getStoreConfig(self::GSTCHARGE_SHIPPINGSETTINGS_TAXTYPE))
			{
				$address->setGrandTotal($address->getGrandTotal() + $address->getShippingCgstCharge());
				$address->setBaseGrandTotal($address->getBaseGrandTotal() + $address->getShippingCgstCharge());
			}
			else
			{
				$address->setGrandTotal($address->getGrandTotal());
				$address->setBaseGrandTotal($address->getBaseGrandTotal());
			}
		endif;
		return $this;
		
	}
	
	public function fetch(Mage_Sales_Model_Quote_Address $address)
    {
        $cart = Mage::getSingleton('checkout/session')->getQuote();
		$amount = $address->getShippingCgstCharge();
		if ($amount != 0 && $address->getAddressType() == 'shipping') 
		{
			$title = Mage::helper('gstcharge')->__('Shipping CGST');
			$address->addTotal(array(
				'code' => $this->getCode(),
				'title' => $title,
				'value' => $amount
			));
		
		}
		
		return $this;
    }
}
