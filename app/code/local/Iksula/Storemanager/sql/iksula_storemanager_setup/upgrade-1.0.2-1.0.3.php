<?php
/**
 * Iksula_Storemanager extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Iksula
 * @package        Iksula_Storemanager
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Storemanager module install script
 *
 * @category    Iksula
 * @package     Iksula_Storemanager
 * @author      Ultimate Module Creator
 */

$installer = $this;
$installer->startSetup();

$installer->run(" 
DROP TABLE IF EXISTS {$this->getTable('iksula_storemanager/store')};
CREATE TABLE IF NOT EXISTS {$this->getTable('iksula_storemanager/store')} (
  entity_id int(11) NOT NULL auto_increment,
  store_type_id varchar(100) NOT NULL,
  status varchar(100) NOT NULL,
  url_key varchar(100) NOT NULL,
  in_rss varchar(100) NOT NULL,
  meta_title varchar(100) NOT NULL,
  meta_keywords varchar(100) NOT NULL,
  meta_description varchar(100) NOT NULL,
  allow_comment varchar(100) NOT NULL,
  updated_at varchar(100) NOT NULL,
  created_at varchar(100) NOT NULL,  
  PRIMARY KEY (entity_id)
 ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;"
);
//$this->run("DROP TABLE IF EXISTS {iksula_storemanager/store_store}");


$installer->run("
 
DROP TABLE IF EXISTS {$this->getTable('iksula_storemanager/store_store')};
CREATE TABLE IF NOT EXISTS {$this->getTable('iksula_storemanager/store_store')} (
  store_id int(11) unsigned NOT NULL auto_increment,  
  PRIMARY KEY (store_id)
 ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");


$installer->run("
 
 DROP TABLE IF EXISTS {$this->getTable('iksula_storemanager/store_comment')};
CREATE TABLE IF NOT EXISTS {$this->getTable('iksula_storemanager/store_comment')} (
  comment_id int(11) unsigned NOT NULL auto_increment,  
  store_id int(11) unsigned NOT NULL ,
  title int(11) unsigned NOT NULL ,  
  comment int(11) unsigned NOT NULL ,  
  status int(11) unsigned NOT NULL ,  
  customer_id int(11) unsigned NOT NULL , 
  name int(11) unsigned NOT NULL , 
  email int(11) unsigned NOT NULL , 
  updated_at int(11) unsigned NOT NULL , 
  created_at int(11) unsigned NOT NULL ,  
  PRIMARY KEY (comment_id)
 ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");



$installer->run(" 
 DROP TABLE IF EXISTS {$this->getTable('iksula_storemanager/store_comment_store')};
CREATE TABLE IF NOT EXISTS {$this->getTable('iksula_storemanager/store_comment_store')} (
    entity_id int(11) NOT NULL auto_increment, 
  comment_id int(11) unsigned NOT NULL ,  
  store_id int(11) unsigned NOT NULL, 
  PRIMARY KEY (entity_id)   
 ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;");



$installer->endSetup();
