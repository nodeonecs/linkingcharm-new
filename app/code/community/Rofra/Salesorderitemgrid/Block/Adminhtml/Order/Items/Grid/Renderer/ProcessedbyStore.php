<?php


class Rofra_Salesorderitemgrid_Block_Adminhtml_Order_Items_Grid_Renderer_Processedbystore
extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract{


public function render(Varien_Object $row)
    {
        $orderId = $row->getId();
         $oOrderItemsObject = Mage::getModel('sales/order_item')->load($orderId);
         $sOrderItemsType = $oOrderItemsObject->getProductType();
         $iParentItemId = $oOrderItemsObject->getParentItemId();
        if(strtolower($sOrderItemsType) == 'simple'){

            if($iParentItemId){

                $orderId = $iParentItemId;

            }

        }

        $order = Mage::getModel('sales/order')->loadByIncrementId($row->getIncrementalId());

        $orderItems = $order->getItemsCollection();
        $invoiceItems = Mage::getModel('sales/order_invoice')->getCollection()
                        ->addAttributeToSelect('entity_id');

        $invoiceItems->getSelect()->join(
            array('invoice_item' =>'sales_flat_invoice_item'),
             'main_table.entity_id= invoice_item.parent_id',
            array('invoice_item.order_item_id'));

        $invoiceItems->addFieldToFilter('invoice_item.order_item_id', array('eq'=>$orderId));
        //echo $invoiceItems->getSelect();
        /*$html =  $shippedItems->getSelect();
        return $html;*/
        //exit;
        /*echo '<pre>';
        print_r($invoiceItems->getData());
        print_r($invoiceItems->getFirstItem()->getId());*/

        //$html = (string)$shippedItems->getSelect();
        $html = '';
        if(count($invoiceItems) > 0){
            //return 'test';
            $invoice_id = $invoiceItems->getFirstItem()->getId();
            $html = "";

            //$hlp = Mage::helper('amrolepermissions');
            $sStoreViewId = Mage::getSingleton('sales/order_invoice')->load($invoice_id , 'entity_id')->getStoreScopeview();
            if($sStoreViewId){
                $StoreGroup_id = Mage::getSingleton('core/store')->load($sStoreViewId , 'store_id')->getGroupId();
                $sStoreName = Mage::getSingleton('core/store_group')->load($StoreGroup_id , 'group_id')->getName();
                $html = $sStoreName;
            }

            
        }



        return $html;
    }


}
