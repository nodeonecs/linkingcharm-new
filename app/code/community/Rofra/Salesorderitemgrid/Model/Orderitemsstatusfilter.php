<?php

Class Rofra_Salesorderitemgrid_Model_Orderitemsstatusfilter extends Mage_Core_Model_Abstract{




	public function getStatusOptionsEventDriven(){

		$StatusCollections = Mage::getModel('orderitemsstatestatus/orderitemsstatestatus')->getCollection()->addFieldToFilter('type' , array('eq' => '2'))->addFieldToFilter('event_driven' , array('eq' => '1'));

		foreach($StatusCollections as $key => $value){

			$aStatusArray [$value->getCode()] = $value->getTitle();

		}

		return $aStatusArray;

	}


	public function getStatusOptions(){

		$StatusCollections = Mage::getModel('orderitemsstatestatus/orderitemsstatestatus')->getCollection()->addFieldToFilter('type' , array('eq' => '2'));

		foreach($StatusCollections as $key => $value){

			$aStatusArray [$value->getCode()] = $value->getTitle();

		}

		return $aStatusArray;

	}

	public function getStateOptions(){


		$StatusCollections = Mage::getModel('orderitemsstatestatus/orderitemsstatestatus')->getCollection()->addFieldToFilter('type' , array('eq' => '1'));

		foreach($StatusCollections as $key => $value){

			$aStateArray [$value->getCode()] = $value->getTitle();

		}

		return $aStateArray;

	}
}
