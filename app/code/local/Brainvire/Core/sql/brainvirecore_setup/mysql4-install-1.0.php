<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   Brainvire
 * @package    Brainvire_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * @license    
 */


$installer = $this;



$installer->startSetup();

$installer->run("
 CREATE TABLE IF NOT EXISTS {$this->getTable('brainvirecore/logger')} (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(255)  NOT NULL,
  `content` mediumtext  NOT NULL,
  `module` varchar(255)  NOT NULL,
  `object` varchar(255)  NOT NULL,
  `severity` varchar(255) NOT NULL,
  `visibility` tinyint(1) NOT NULL default '1',
  `custom_field_1` varchar(255) NOT NULL,
  `custom_field_2` varchar(255) NOT NULL,
  `custom_field_3` varchar(255) NOT NULL,
  `file` varchar(255) NOT NULL,
  `line` int(11) NOT NULL,
  `date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `code` varchar(16) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `module` (`module`),
  KEY `severity` (`severity`),
  KEY `visibility` (`visibility`),
  KEY `custom_field_1` (`custom_field_1`),
  KEY `custom_field_2` (`custom_field_2`),
  KEY `custom_field_3` (`custom_field_3`),
  KEY `date` (`date`),
  KEY `code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
$installer->endSetup();