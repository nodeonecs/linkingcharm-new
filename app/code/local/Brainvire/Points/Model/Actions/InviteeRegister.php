<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   Brainvire
 * @package    Brainvire_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * @license    
 */


class Brainvire_Points_Model_Actions_InviteeRegister extends Brainvire_Points_Model_Actions_Abstract {

    protected $_action = 'invitee_registered';
    protected $_comment = 'Reward for registration of invited user %s';

    protected function _applyLimitations($amount) {
        $pointLimitForAction = Mage::helper('points/config')->getLimitPointsOfInvitationForDay();

        $collection = Mage::getModel('points/transaction')
                ->getCollection()
                ->addFieldToFilter('summary_id', $this->getSummary()->getId())
                ->addFieldToFilter('action', $this->getAction())
                ->limitByDay(Mage::getModel('core/date')->gmtTimestamp());

        
        $summ = 0;
        foreach ($collection as $transaction) {
            $summ += $transaction->getBalanceChange();
        }

        return parent::_applyLimitations($this->_calculateNewAmount($summ, $amount, $pointLimitForAction));
    }

    public function getCommentHtml($area = self::ADMIN) {
        return Mage::helper('points')->__($this->_transaction->getComment());
    }

    public function getComment() {
        if (isset($this->_commentParams['referral_name'])) {
            return Mage::helper('points')->__($this->_comment, $this->_commentParams['referral_name']);
        }
        return $this->_comment;
    }

}

?>
