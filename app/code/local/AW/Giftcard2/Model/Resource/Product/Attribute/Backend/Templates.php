<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_Model_Resource_Product_Attribute_Backend_Templates
    extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init('aw_giftcard2/product_entity_templates', 'value_id');
    }

    /**
     * @param int $entityId
     * @return array
     */
    public function loadTemplatesData($entityId)
    {
        $adapter = $this->_getReadAdapter();
        $columns = array(
            'value_id' => $this->getIdFieldName(),
            'template' => 'value',
            'image' => 'image',
            'store_id' => 'store_id',
        );
        $select = $adapter->select()
                        ->from($this->getMainTable(), $columns)
                        ->where('entity_id=?', $entityId)
                        ->order(array('store_id ASC'))
        ;
        return $adapter->fetchAll($select);
    }

    /**
     * @param Varien_Object $templateObject
     * @return int
     */
    public function deleteTemplatesData(Varien_Object $templateObject)
    {
        $adapter = $this->_getWriteAdapter();
        $conditions = array (
            $adapter->quoteInto('entity_id = ?', $templateObject->getEntityId()),
            $adapter->quoteInto('value = ?', $templateObject->getTemplate()),
        );
        if ($templateObject->getStoreId()) {
            $conditions[] = $adapter->quoteInto('store_id = ?', $templateObject->getStoreId());
        }
        $where = implode(' AND ', $conditions);
        return $adapter->delete($this->getMainTable(), $where);
    }

    /**
     * @param Varien_Object $templateObject
     * @return $this
     */
    public function saveTemplatesData(Varien_Object $templateObject)
    {
        $adapter = $this->_getWriteAdapter();
        $data = $this->_prepareDataForTable($templateObject, $this->getMainTable());

        if (!empty($data[$this->getIdFieldName()])) {
            $where = $adapter->quoteInto($this->getIdFieldName() . ' = ?', $data[$this->getIdFieldName()]);
            unset($data[$this->getIdFieldName()]);
            $adapter->update($this->getMainTable(), $data, $where);
        } else {
            $adapter->insert($this->getMainTable(), $data);
        }
        return $this;
    }

    /**
     * @param int $entityId
     * @param int $templateCode
     * @return string|null
     */
    public function loadTemplateImage($entityId, $templateCode)
    {
        $adapter = $this->_getReadAdapter();
        $columns = array(
            'image' => 'image'
        );
        $select = $adapter->select()
            ->from($this->getMainTable(), $columns)
            ->where('entity_id=?', $entityId)
            ->where('value=?', $templateCode)
            ->order(array('store_id ASC'))
        ;
        return $adapter->fetchOne($select);
    }

}