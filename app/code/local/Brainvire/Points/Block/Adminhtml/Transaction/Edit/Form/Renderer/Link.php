<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   BV
 * @package    BV_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */


class Brainvire_Points_Block_Adminhtml_Transaction_Edit_Form_Renderer_Link extends Mage_Adminhtml_Block_Template implements Varien_Data_Form_Element_Renderer_Interface {

    protected function _construct() {
        $this->setTemplate('brainvire_points/transaction/form/renderer/link.phtml');
    }

    public function render(Varien_Data_Form_Element_Abstract $element) {
        $this->setElement($element);
        return $this->toHtml();
    }

}
