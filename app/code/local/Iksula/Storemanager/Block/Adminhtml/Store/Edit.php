<?php
/**
 * Iksula_Storemanager extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category       Iksula
 * @package        Iksula_Storemanager
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Store admin edit form
 *
 * @category    Iksula
 * @package     Iksula_Storemanager
 * @author      Ultimate Module Creator
 */
class Iksula_Storemanager_Block_Adminhtml_Store_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->_blockGroup = 'iksula_storemanager';
        $this->_controller = 'adminhtml_store';
        $this->_updateButton(
            'save',
            'label',
            Mage::helper('iksula_storemanager')->__('Save Store')
        );
        $this->_updateButton(
            'delete',
            'label',
            Mage::helper('iksula_storemanager')->__('Delete Store')
        );
        $this->_addButton(
            'saveandcontinue',
            array(
                'label'   => Mage::helper('iksula_storemanager')->__('Save And Continue Edit'),
                'onclick' => 'saveAndContinueEdit()',
                'class'   => 'save',
            ),
            -100
        );
        $this->_formScripts[] = "
            function saveAndContinueEdit() {
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
        $this->_removeButton('delete'); // Remove the Delete Button from the Edit Page of the Store Manager.
    }

    /**
     * get the edit form header
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getHeaderText()
    {
        if (Mage::registry('current_store') && Mage::registry('current_store')->getId()) {
            return Mage::helper('iksula_storemanager')->__(
                "Edit Store '%s'",
                $this->escapeHtml(Mage::registry('current_store')->getGroupId())
            );
        } else {
            return Mage::helper('iksula_storemanager')->__('Add Store');
        }
    }
}
