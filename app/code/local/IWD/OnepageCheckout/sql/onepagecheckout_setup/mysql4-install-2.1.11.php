<?php
$installer = $this;
$installer->startSetup();
$sql=<<<SQLTEXT
create table pincodes(
id int not null auto_increment, 
postcode varchar(255),
region_id int(11),
city varchar(255),
region varchar(255),
primary key(id));

SQLTEXT;

$installer->run($sql);

$installer->endSetup();