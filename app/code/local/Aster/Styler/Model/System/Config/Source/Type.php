<?php
class Aster_Styler_Model_System_Config_Source_Type
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $optionCollection = array();

        $attribute = Mage::getModel('eav/config')->getAttribute( Mage_Catalog_Model_Product::ENTITY ,'product_type');
        $options = $attribute->getSource()->getAllOptions();
        $i = 0;
        foreach ($options as $option) {
            if(!empty($option['label'])){
                $optionCollection[$i]['value'] = $option['value'];
                $optionCollection[$i]['label'] = Mage::helper('adminhtml')->__($option['label']);
                $i++;
            }            
        }

        return $optionCollection;
    }

}
