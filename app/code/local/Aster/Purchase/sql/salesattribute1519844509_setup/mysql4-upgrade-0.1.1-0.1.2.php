<?php
$installer = $this;
$installer->startSetup();

$installer->addAttribute("invoice_item", "hsn_code", array("type"=>"varchar"));
$installer->addAttribute("invoice_item", "base_category", array("type"=>"varchar"));
$installer->addAttribute("invoice_item", "cost_price", array("type"=>"varchar"));

$installer->addAttribute("creditmemo_item", "hsn_code", array("type"=>"varchar"));
$installer->addAttribute("creditmemo_item", "base_category", array("type"=>"varchar"));
$installer->addAttribute("creditmemo_item", "cost_price", array("type"=>"varchar"));

$installer->addAttribute("shipment_item", "hsn_code", array("type"=>"varchar"));
$installer->addAttribute("shipment_item", "base_category", array("type"=>"varchar"));
$installer->addAttribute("shipment_item", "cost_price", array("type"=>"varchar"));

$installer->endSetup();
	 