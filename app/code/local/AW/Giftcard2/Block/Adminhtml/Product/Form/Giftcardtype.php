<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_Block_Adminhtml_Product_Form_GiftcardType extends Varien_Data_Form_Element_Select
{
    public function getAfterElementHtml()
    {
        $html = parent::getAfterElementHtml();
        return $html."  <script type=\"text/javascript\">
                        var typeOptions = {
                            giftcardTypeSelector: '".$this->getHtmlId()."',
                            templatesSelector: '.aw-giftcard2-templates',
                            templateValuesSelector: 'aw-giftcard2-templates_container',
                        }
                        $(typeOptions.giftcardTypeSelector).observe('change', function () {
                            checkGiftcardType(typeOptions);
                        });
                        document.observe('dom:loaded', function () {
                            checkGiftcardType(typeOptions);
                        });

                        checkGiftcardType = function(typeOptions) {
                             if (
                                $(typeOptions.giftcardTypeSelector).value == " . AW_Giftcard2_Model_Source_Entity_Attribute_Giftcard_Type::PHYSICAL_VALUE . "
                             ) {
                                $$(typeOptions.templatesSelector).first().up('tr').hide();
                                $(typeOptions.templateValuesSelector).innerHTML = '';
                             } else {
                                $$(typeOptions.templatesSelector).first().up('tr').show();
                             }
                             if ($('weight') != undefined) {
                                var weightEl = $('weight');
                                var weightLabelReqEl = weightEl.up('tr').down('td').down('label > span');

                                if (
                                     $(typeOptions.giftcardTypeSelector).value == " . AW_Giftcard2_Model_Source_Entity_Attribute_Giftcard_Type::VIRTUAL_VALUE . "
                                ) {
                                    while(weight.hasClassName('required-entry')) {
                                        weight.removeClassName('required-entry');
                                    }
                                    if (weightLabelReqEl != undefined) {
                                        weightLabelReqEl.hide();
                                    }
                                } else {
                                        weight.addClassName('required-entry');
                                        if (weightLabelReqEl != undefined) {
                                            weightLabelReqEl.show();
                                        }
                                }
                             }
                        }
        				</script>";
    }
}