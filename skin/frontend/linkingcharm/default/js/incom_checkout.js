// Global Variables
var steps = ["login-step", "address-step", "payment-step"];
var current_step;
var next_step;
var hide_flag = 0;
var index = 0;
var show_shipping_address_info_step = "review-step";
var shipping_address = '';
var chosen_address_option = 0;
var validation_status = [0, 0, 0];
var validate_steps = true;
var bypass_current_validation = false;
var step_button_id;
var paymethodone;
var id_of_selected_pay;
var payformdd;
var selectpaydd;
var customer_zip;
var emailidcheckout;
var fname;
var lname;
var street1;
var street2;
var postcode;
var telephone;
var city;
//var all_customer_data = {};
var validate_email = true;
var validate_mobile = true;
var delete_id;
var update_id;
var shipping_html = '';
var phone;
var all_zip_codes;
var zip_codes_received = false;

jQuery(document).ready(function($) {

  	var cod_opt = '<label for="p_method_cashondelivery" class="cod_opt">COD </label>';
  	$("#paydt_cashondelivery label").after(cod_opt);

  	/* Select box for Payment methos */

  	var active_payment_method = $(".checkout_parent .sp-methods dt.added label").text();
  	$(".payment_method_tab span.ttitle").text(active_payment_method);

  	$(".payment_method_tab").live('click',function(){
  		$(".payment_tabs").stop(true,true).toggleClass('visible');
  		$(".menu_toogle_arrow").stop(true,true).toggleClass("menu_toogle_arrow_up")
  	})

  	$(".payment_tabs dt label").live('click', function(){
  		var content_text = $(this).text();
  		$(".payment_method_tab span.ttitle").text(content_text);
  		$(".payment_tabs").stop(true,true).removeClass('visible');
  		$(".menu_toogle_arrow").stop(true,true).removeClass("menu_toogle_arrow_up")
  	})

  	if($('body').hasClass('mobileapp-index-index')){
  		$('a.logo').removeAttr("href");
  	}
  	/* Select box for Payment methos */

  	function register_user(){

  		emailidcheckout = $('#email').val();
  		fname = $('#billing\\:firstname').val();
  		lname = $('#billing\\:lastname').val();
  		mob   = $('#billing\\:telephone').val();
  		$('#checkout_billing_email').val(emailidcheckout);

  		$.ajax({
			url: baseurl + 'onepagecheckout/index/registerUserCheckout/',
			type:'post',
			data:{email:emailidcheckout, fname:fname, lname:lname, mob:mob,current_step:current_step},
			/*success: function(guest_data){
				$.ajax({
					type:'post',
					success: function(data){
						var div = $(data).find("#email_Checkout");
						var email = div.val();
						var size = parseInt(all_customer_data.length);
						all_customer_data[size] = [];
						all_customer_data[size]['email'] = email;
					}
				});
			}*/
		});
		return;
  	}



	$(".payment_tabs_area dd").first().show();

		paymethodone = $('.added').attr('id');
		payformdd 	 = $('.dd_added').attr('id');
		$('.paymethod').live('change',function(){
  			//change of payment method
  			$('#' + paymethodone).removeClass('added');
  			id_of_selected_pay = $(this).attr('id');
  			$('#' + id_of_selected_pay).addClass('added');
  			paymethodone = $(this).attr('id');

  			//for payment hide and show
  			selectpaydd = id_of_selected_pay.substring(6);
  				$('#' + payformdd).removeClass('dd_added');
  				$('#ddpay_' + selectpaydd).addClass('dd_added');
  				payformdd = 'ddpay_' + selectpaydd;
		});



	function validate_this_step() {
		if(validate_steps == false) {
			return true;
		}

		/*if(current_step == 'address-step') {

			telephone = $('#billing\\:telephone').val();
			if(telephone.length < 10 || telephone.length > 10){
				var validation_advice = '<div class="validation-advice validation-advice-mobile-no">Please enter a valid mobile number</div>';
				if(!$('.validation-advice-mobile-no').length){
					$('#billing\\:telephone').after(validation_advice);
				}
				return false;
			}
			else{
				$('.validation-advice-mobile-no').remove();
			}
		}*/

		if(validation_status[index] == 1) {
			return true;
		}
		return false;
	}

	function show_next_step(jump_to_last) {
		$('.step#'+current_step).hide();
		index = $.inArray(current_step, steps);
		index++;
		$('.step#'+steps[index]).show();
		$('.step_bubble#'+steps[index]).removeClass('not_clickable');
		current_step = steps[index];
		if(jump_to_last) {

			next_step();
		}

		if(current_step == 'payment-step') {
			$('.checkout_discount_block').show();
			$('.hide_final_amount').show();

		}
		else{
			// $('.checkout_discount_block').hide();
			$('.hide_final_amount').hide();
		}
	}

	// This function will check if the email id is already registered then prevent login.
	// Currently this not used as we have enabled guest checkout.
	/*function validate_this_email(){

		// alert("test");
		emailidcheckout = $('#email').val();
		//all_customer_data
		$.each(all_customer_data,function(key,value){

			if($.trim(value['email']) == $.trim(emailidcheckout))
			{
				validate_email = false;
				$('.email_error').addClass('validation-advice');
				$('#haspass1').click();
				$('.email_error').css('display','block');
				return false;
			}
			else{
				validate_email = true;
				$('.email_error').css('display','none');
				return true;
			}
		});
	}*/

	function next_step(event) {
		//------------------For guest users----------------
		emailuser = $('#email').val();
  		$('#checkout_billing_email').val(emailuser);

        var validate = validate_this_step();

		if(mobile_match == true && validate_email == true && validate == true || bypass_current_validation == true) {
			/*if(current_step == 'address-step') {
				show_shipping_detail();
			}*/
			//Below two ajax calls is for tracking user on checkout page
			if(current_step == 'login-step') {
				step = 1;
				$.ajax({
					url: baseurl + 'onepagecheckout/Track/getTrackDetailsStepTwo/',
					type:'post',
					data:{checkoutstep:step},
					dataType: 'json',
					success: function(data){
						$('.steps').html('steps <b> 2 of 3</b>');
						//console.log(data);
					}
				});
			}

			cemailidcheckout = $('#email').val();
	  		cfname = $('#billing\\:firstname').val();
	  		clname = $('#billing\\:lastname').val();
	  		cmob   = $('#billing\\:telephone').val();

			if(current_step == 'address-step') {
				step = 2;
				$.ajax({
					url: baseurl + 'onepagecheckout/Track/getTrackDetailsStepThree/',
					type:'post',
					data:{checkoutstep:step, cemailidcheckout:cemailidcheckout, cfname:cfname, clname:clname, cmob:cmob},
					dataType: 'json',
					success: function(data){
						$('.steps').html('steps <b> 3 of 3</b>');
						//console.log(data);
					}
				});
			}

			if($(this) && $(this).hasClass('jump_to_last')) {
				show_next_step(true);
			} else {
				show_next_step(false);
			}
			$('.step, .step_bubble').removeClass('active');
			$('.step_bubble#'+current_step).addClass('active');
			$('.change_state').removeClass('active');
			$('.step_bubble#'+current_step+' .change_state').addClass('active');
			bypass_current_validation = false;
		}
		 $("html, body").animate({ scrollTop: 0 }, "slow");
	}

	function navigate_to_step() {

		var step_id = $(this).attr('id');
		
		this_index = $.inArray(step_id, steps);
		if(this_index < index && (!$(this).hasClass('not_clickable'))) {
			
			$('.step#'+current_step).hide();
			current_step = step_id;
			index = this_index;
			if(current_step == 'login-step')
				$('.steps').html('steps <b> 1 of 3</b>');
			else if(current_step == 'address-step')
				$('.steps').html('steps <b> 2 of 3</b>');
			else if(current_step == 'payment-step')
				$('.steps').html('steps <b> 3 of 3</b>');
			var i = 0;
			for(i = index + 1; i < steps.length; i++) {
				$('.step_bubble#'+steps[i]).addClass('not_clickable');
			}
			$('.step, .step_bubble').removeClass('active');
			$('.step_bubble#'+current_step).addClass('active');
			$('.change_state').removeClass('active');
			$('.step_bubble#'+current_step+' .change_state').addClass('active');
			//$('.step_img').addClass('active');
			$('.step#'+steps[index]).show();

			if(steps[index]!='payment-step'){
				$(".checkout_discount_block").hide();
				$('.hide_final_amount').hide();
			}else{
				$(".checkout_discount_block").show();
				$('.hide_final_amount').show();
			}

			validation_status[index] = 0;
		}
	}

	function callback_function(){

		if(current_step == 'payment-step') {
			$('.checkout_discount_block').show();
			$('.hide_final_amount').show();
		}
		else{
			// $('.checkout_discount_block').hide();
			$('.hide_final_amount').hide();
		}
	}

	/*function ajax_update_cart(update_id){
		//alert(update_id);
		$('.checkout_loader').show();
		$('#checkout-review').hide();
		var form_key = "<?php echo Mage::getSingleton('core/session')->getFormKey();?>";
		$.ajax({
			url: baseurl + 'checkout/cart/updatePost',
			type:'post',
			data:{update_cart_action:update_id,form_key:form_key},
			success: function(del_html){
				 $('#checkout-review').show();
				 checkout.update({
		            'review': 1
		        });
				$('.checkout_loader').hide();
				//all_customer_data = jQuery.parseJSON(all_customer_data);
			},
			error: function(del_html){
		       $('.checkout_loader').hide();
		    }
		});
	}*/

	function initialize() {
		// Ajax call to get all customer data
		/*$.ajax({
			url: baseurl + 'onepagecheckout/index/getAllCustomers/',
			type:'post',
			dataType: 'json',
			success: function(cust_data){
				all_customer_data = cust_data;
			}
		});*/

		// $.ajax({
	 //        url: basejsonurl,
	 //        success: function(data){
	 //        	all_zip_codes = JSON.parse(data);
	 //        	zip_codes_received = true;
	 //        },
	 //        error: function(){
	 //        	zip_codes_received = false;
	 //        }
	 //    });

		$('.step#address-step').hide();
		$('.step#payment-step').hide();

		if($('.step_bubble.active').length > 0) {
			current_step = $('.step_bubble.active').attr('id');
			index = steps.indexOf(current_step);
		} else {
			current_step = steps[0];
			$('.step_bubble#'+current_step).addClass('active');
		}
		$('.step#'+current_step).show();
	}

	initialize();

	// Below line will allow guest checkout
	//$('.not_login_continue').live('click',validate_this_email);
	$('.continue').live('click', next_step);
	//$(document).on('click','.continue', next_step);
	$('.step_bubble').click(navigate_to_step);
	// Register users from checkout.
	$('.checkout_not_login').click(function(){
		register_user();
	});

	// $('.checkout_discount_block').hide();
	$('.hide_final_amount').hide();

	$('.step_shipping_save').live('click',function(){

		// checkout.update({
  //           'review': 1
  //       });

        if(current_step == 'address-step'){
			// $('.checkout_discount_block').hide();
			$('.hide_final_amount').hide();
		}
	});

	$('#haspass1').click(function(){
		$('.showformlog').show();
		$('.check_login_button').hide();
		$('.email_error').css('display','none');
		$('#pass').focus();
	});

	$('#haspass').click(function(){
		$('.showformlog').hide();
		$('.check_login_button').show();
	});

	$('.step_1').live('click',function (event){

	});


	/* Enter Key Events */
	$(window).keypress(function(event){
		if (event.which == 10 || event.which == 13) {
			if(event.target.id != "billing:street1" && event.target.id != "shipping:street1") {
				event.preventDefault();
			}
			if(event.target.id == "emailid") {
				$('#login-step-continue').click();
			}
			if(event.target.id == "email" || event.target.id == "pass") {
				$('#send2').click();
			}
		}
	});

	/*$('.cancel_arrow').live('click',function(){
		delete_id = this.id;
		$('.checkout_loader').show();
		$('#checkout-review').hide();
		$.ajax({
			url: baseurl + 'checkout/cart/delete',
			type:'post',
			data:{id:delete_id},
			success: function(del_html){
				 $('#checkout-review').show();
				 checkout.update({
		            'review': 1
		        });
				$('.checkout_loader').hide();
				//all_customer_data = jQuery.parseJSON(all_customer_data);
			},
			error: function(del_html){
		       $('.checkout_loader').hide();
		    }
		});
	});*/


	$('.checkout_success_button').on('click',function(){
	jQuery('.checkout_success_loader').show();

	    // jQuery.ajax({

	    //     type: "post",
	    //     data: validation_status,
	    //     success: function(data){
	        	var COD = $('#ddpay_phoenix_cashondelivery.dd_added').html();
	        	if(validation_status == '0,1,0' || COD == null){
	        		jQuery('.checkout_success_loader').hide();

	            }
	            else{
	            	jQuery('.checkout_success_loader').show();
	            }
				// else{
				// 	// jQuery('.checkout_success_loader').hide();
				// 	if(COD == null){
				// 		jQuery('.checkout_success_loader').show();
				// 	}
				// }
				// if(COD == null){
				// 	jQuery('.checkout_success_loader').show();
				// }
	    //     },
	    //     error:function(){

	    //     }
	    // });
	});

	// $('.payatstorecheckout_success_button').on('click',function(){
	// jQuery('.payatstore_checkout_success_loader').show();
	//     // jQuery.ajax({

	//     //     type: "post",
	//     //     data: validation_status,
	//     //     success: function(data){
	//         	// var COD = $('#ddpay_phoenix_cashondelivery.dd_added').html();
	//         	if(validation_status == '0,1,0'){ // 
	//         		jQuery('.payatstore_checkout_success_loader').hide();

	//             }
	//             else{
	//             	jQuery('.payatstore_checkout_success_loader').show();
	//             }
	// 			// else{
	// 			// 	// jQuery('.checkout_success_loader').hide();
	// 			// 	if(COD == null){
	// 			// 		jQuery('.checkout_success_loader').show();
	// 			// 	}
	// 			// }
	// 			// if(COD == null){
	// 			// 	jQuery('.checkout_success_loader').show();
	// 			// }
	//     //     },
	//     //     error:function(){

	//     //     }
	//     // });
	// });

	// $('.cashpaymentcheckout_success_button').on('click',function(){
	// jQuery('.cashpayment_checkout_success_loader').show();
	//     // jQuery.ajax({

	//     //     type: "post",
	//     //     data: validation_status,
	//     //     success: function(data){
	//         	// var COD = $('#ddpay_phoenix_cashondelivery.dd_added').html();
	//         	if(validation_status == '0,1,0'){	// || COD == null
	//         		jQuery('.cashpayment_checkout_success_loader').hide();
	//             }
	//             else{
	//             	jQuery('.cashpayment_checkout_success_loader').show();
	//             }
	// 			// else{
	// 			// 	// jQuery('.checkout_success_loader').hide();
	// 			// 	if(COD == null){
	// 			// 		jQuery('.checkout_success_loader').show();
	// 			// 	}
	// 			// }
	// 			// if(COD == null){
	// 			// 	jQuery('.checkout_success_loader').show();
	// 			// }
	//     //     },
	//     //     error:function(){

	//     //     }
	//     // });
	// });

});
