<?php

Class Iksula_Ajax_LazyController extends Mage_Core_Controller_Front_Action {

	/*
	 *
	 * Returns Footer HTML in ajax response
	 *
	 */
	public function footerAction() {
		if($this->getRequest()->getParam('content') == 'footer'){
			$layout         = $this->getLayout();
			$block_header   = $layout->createBlock('page/html_footer','footer')->setTemplate('page/html/footer.phtml');
			$wrapper  = $layout->createBlock('page/html_wrapper')->setTemplate('page/html/wrapper.phtml')->setTranslate('label')->setLabel('Page Footer')->setElementClass('bottom-container');
			$block_header->setChild('bottomContainer',$wrapper);
			$storeSwitcher   = $layout->createBlock('page/switch','store_switcher')->setTemplate('page/switch/stores.phtml');
			$block_header->setChild('store_switcher',$storeSwitcher);
			$this->getResponse()->setBody(Mage::helper('core')->jsonEncode($block_header->toHtml()));
	        return;
		}
	}

}
