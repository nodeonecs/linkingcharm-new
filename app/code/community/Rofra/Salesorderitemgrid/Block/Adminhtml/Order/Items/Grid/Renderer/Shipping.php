<?php
class Rofra_Salesorderitemgrid_Block_Adminhtml_Order_Items_Grid_Renderer_Shipping
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $value =  $row->getData($this->getColumn()->getIndex());     
        
        //$rowItemId = $row->getId();  

        $orderid = $row->getData('order_id');      
      
        $order = Mage::getModel('sales/order')->load($orderid);

        echo $order->getShippingAmount();

    }
}
?>