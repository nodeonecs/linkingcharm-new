<?php

class Rofra_Salesorderitemgrid_Block_Adminhtml_Sales_Order_View extends Mage_Adminhtml_Block_Widget_Form_Container
{

    public function __construct()
    {//need to remove
        $this->_objectId    = 'order_id';
        $this->_controller  = 'sales_order';
        $this->_mode        = 'view';

        parent::__construct();

        $this->_removeButton('delete');
        $this->_removeButton('reset');
        $this->_removeButton('save');
        $this->setId('sales_order_view');
        /*$InvoiceallowedItemStatus = array('store_accepted' , 'invoiced' , 'shipped');
        $ShipmentallowedItemStatus = array('store_accepted' , 'invoiced' , 'shipped');*/
        $sAllowedItemStatus = array('store_allocated' , 'store_rejected' , 'invoiced' , 'shipped' , 'store_accepted' , 'processing' , 'store_pickup_accepted');


        $suborder_id = Mage::app()->getRequest()->getParam('sub_order_id'); // Get sub order item id by url .

        if(trim($suborder_id) == ""){  // Sub Order id is blank die with the message.
            die('Sub Order item Id not found');
        }

        $OrderItemsObj = Mage::getModel('sales/order_item')->load($suborder_id);
        
        $order_item_id = $OrderItemsObj->getData('item_id');
        $product_type = $OrderItemsObj->getProductType(); // Product Type of the Order Item id in url
        

        $ParentItemid = $OrderItemsObj->getParentItemId();
        $pickupstatus = $OrderItemsObj->getPickupStatus();

        if(strtolower($productType) == 'configurable'){                    
                $getChildItemId = Mage::getModel('sales/order_item')->load($suborder_id , 'parent_item_id')->getItemId();
                $getChildObject = Mage::getSingleton('sales/order_item')->load($getChildItemId); 
                $ItemsStatus = $getChildObject->getItemStatus();
                $checkoutmethod = $getChildObject->getCheckoutMethod();                   
                // $pickupstatus = $getChildObject->getPickupStatus();
        }elseif(strtolower($product_type) == 'simple'){
                $ItemsStatus = $OrderItemsObj->getItemStatus();
                $checkoutmethod = $OrderItemsObj->getCheckoutMethod();
                $pickupstatus = $OrderItemsObj->getPickupStatus();
        }

        
        


            if($product_type == 'configurable'){ // If Product Type is configurable 

                $parent_id = $suborder_id; // Get Parent Id 
                $child_id = Mage::getModel('sales/order_item')->load($parent_id , 'parent_item_id')->getItemId();               // Get the child item id.


            }else{ // If the product is not configurable product but only simple .
                
                $parent_id = $suborder_id;
                $child_id = $suborder_id;

            }

            $OrderItemsObj = Mage::getModel('sales/order_item')->load($parent_id);      // Get Parent Id Object.
            $ItemsStatus = Mage::getModel('sales/order_item')->load($child_id)->getItemStatus();  // Get item status by the child id .
            $checkoutmethod = Mage::getModel('sales/order_item')->load($child_id)->getCheckoutMethod(); // Get the child checkout method for the check (deliverable or pickable)

            $QtyShipped = $OrderItemsObj->getQtyShipped();  // Get Shipped Counts for selected item id                     
            $QtyInvoiced = $OrderItemsObj->getQtyInvoiced(); // Get Invoiced Counts for selected item id                                            
            $QtyOrdered = $OrderItemsObj->getQtyOrdered(); // Get Ordered Counts for selected item id                     
            
            $order = $this->getOrder();
        

        if($ItemsStatus != 'canceled' && $order->canShip() && $order->canInvoice()){

            $this->_addButton('cancelItem', array(
                'label'     => Mage::helper('sales')->__('Cancel Order Item'),
                'onclick'   => 'sendOtpSMSForOrderItemCancalation(' . $suborder_id . ',' . $order->getId() .')',
                'class'     => 'go'
            ));
        }
       
        if($checkoutmethod == 'pickable' && $pickupstatus == '') {
            $this->_addButton('pickupItem', array(
                'label'     => Mage::helper('sales')->__('Pickup Completed'),
                'onclick'   => 'pickupstatuscompleted(' . $order_item_id . ',' . $order->getId() .')',
                'class'     => 'go'
            ));
        }

        $this->_formScripts[] = " function sendOtpSMSForOrderItemCancalation(order_item_id,order){ 
            if(order_item_id && order){
                new Ajax.Request('". Mage::helper('adminhtml')->getUrl('adminhtml/order_items/sendOtpBeforeCancel')."', {
                      method: 'post',
                      parameters: {order_item_id,order},
                      onSuccess: successFuncOtp,
                      onFailure:  failureFuncOtp
                      })
                }
            } ";

            $this->_formScripts[] = " function pickupstatuscompleted(order_item_id,order){ 
            if(order_item_id && order){
                new Ajax.Request('". Mage::helper('adminhtml')->getUrl('adminhtml/order_items/pickupstatuscompleted')."', {
                      method: 'post',
                      parameters: {order_item_id,order},
                      onSuccess: window.location.reload(),
                      onFailure:  window.location.reload()
                      })
                }
            } ";
            ////successFuncPickupStatus,
        
        $this->_formScripts[] = " function verifyOtpSMSForOrderItemCancalation(order,order_item_id,otp){ 
                if(order_item_id && order){
                    window.location = '".Mage::helper('adminhtml')->getUrl('adminhtml/order_items/verifyOtpandCancelOrderItem')."order/'+order+'/order_item_id/'+order_item_id+'/otp/'+otp;
                } 
            }";
         $this->_formScripts[] = ' function successFuncOtp(response){ 
            output = JSON.parse(response.responseText);
            if(output.response){
                var otpEntered = prompt("Enter OTP recieved by customer", "");
                if(otpEntered == output.otp){
                    verifyOtpSMSForOrderItemCancalation(output.order,output.order_item_id,output.otp);
                }else{
                    alert("Please enter valid OTP");
                }
            }else{
                alert("Error Sending OTP");
            }
        } ';
        $this->_formScripts[] = " function failureFuncOtp(response){ alert('try again'); } ";
          
        /*************************   Ship Button ***********************/

        if ($this->_isAllowedAction('ship') && ($QtyShipped < $QtyOrdered) && $order->canShip() && in_array($ItemsStatus , $sAllowedItemStatus) && $checkoutmethod != 'pickable') {
            $this->_addButton('order_ship', array(
                'label'     => Mage::helper('sales')->__('Ship'),
                'onclick'   => 'setLocation(\'' . $this->getShipUrl() . '\')',
                'class'     => 'go'
            ));
        }
        /********************************************************************************/        
        
        /*************************************Invoice Button ******************************************/
        
        if ($this->_isAllowedAction('invoice') && $order->canInvoice() && ($QtyInvoiced < $QtyOrdered) && in_array($ItemsStatus , $sAllowedItemStatus)) {
            $this->_addButton('order_invoice', array(
                'label'     => Mage::helper('sales')->__('Invoice'),
                'onclick'   => 'setLocation(\'' . $this->getInvoiceUrl() . '\')',
                'class'     => 'go'
            ));
        }
        
        if($QtyOrdered == $QtyShipped){
            $notExistInReturnList = Mage::getModel('amrma/item')->getCollection()->addFieldToFilter('sales_item_id',array('eq'=>$suborder_id))->getSize();
            if(!$notExistInReturnList){
                $createReturnUrl = Mage::helper("adminhtml")->getUrl("adminhtml/amrma_request/new", array("order_id" => $order->getIncrementId(),"sub_order_id" => $suborder_id));
                $this->_addButton('returnitem',array(
                    'label'     => Mage::helper('sales')->__('Return this item'),
                    'onclick'   => 'setLocation(\'' . $createReturnUrl . '\')',
                    'class'     => 'go'
                ));
            }
        }

    }



    /**
     * Retrieve order model object
     *
     * @return Mage_Sales_Model_Order
     */
    public function getOrder()
    {
        return Mage::registry('sales_order');
    }

    /**
     * Retrieve Order Identifier
     *
     * @return int
     */
    public function getOrderId()
    {
        return $this->getOrder()->getId();
    }

    public function getHeaderText()
    {
        if ($_extOrderId = $this->getOrder()->getExtOrderId()) {
            $_extOrderId = '[' . $_extOrderId . '] ';
        } else {
            $_extOrderId = '';
        }
        return Mage::helper('sales')->__('Order # %s %s | %s', $this->getOrder()->getRealOrderId(), $_extOrderId, $this->formatDate($this->getOrder()->getCreatedAtDate(), 'medium', true));
    }

    public function getUrl($params='', $params2=array())
    {
        $params2['order_id'] = $this->getOrderId();
        return parent::getUrl($params, $params2);
    }

    public function getCommentUrl()
    {
        return $this->getUrl('*/*/comment');
    }

    protected function _isAllowedAction($action)
    {
        return Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/' . $action);
    }

    /**
     * Return back url for view grid
     *
     * @return string
     */
    public function getBackUrl()
    {
        if ($this->getOrder()->getBackUrl()) {
            return $this->getOrder()->getBackUrl();
        }

        return $this->getUrl('*/*/');
    }

    public function getShipUrl()
    {
        $sub_order_id =  $this->getRequest()->getParam('sub_order_id');
        return $this->getUrl('*/*/newshipment', array('sub_order_id' => $sub_order_id));
    }

    public function getInvoiceUrl()
    {
        $sub_order_id =  $this->getRequest()->getParam('sub_order_id');
        return $this->getUrl('*/*/newinvoice', array('sub_order_id' => $sub_order_id));
    }
}
