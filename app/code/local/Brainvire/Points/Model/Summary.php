<?php
/**
* Brainvire Infotech Pvt. Ltd
  * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */


class Brainvire_Points_Model_Summary extends Mage_Core_Model_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init('points/summary');
    }

    public function loadByCustomer($customer) {
        $this->getResource()->loadByCustomer($this, $customer);
        if (!$this->getId() && $customer->getId()) {
            $this
                    ->setCustomerId($customer->getId())
                    ->save();
        }
        return $this;
    }

    public function loadByCustomerID($customerID) {
        $this->getResource()->loadByCustomerID($this, $customerID);
        if (!$this->getId()) {
            $this
                    ->setCustomerId($customerID)
                    ->save();
        }
        return $this;
    }

    public function getCustomer() {
        return Mage::getModel('customer/customer')->load($this->getCustomerId());
    }

}
