<?php

require_once 'Mage/Wishlist/controllers/IndexController.php';
class Iksula_Rewrites_Wishlist_IndexController extends Mage_Wishlist_IndexController
{
	protected function _addItemToWishList()
    {
        $wishlist = $this->_getWishlist();
        if (!$wishlist) {
            return $this->norouteAction();
        }

        $session = Mage::getSingleton('customer/session');

        $productId = (int)$this->getRequest()->getParam('product');
        if (!$productId) {
            $this->_redirect('*/');
            return;
        }

        $product = Mage::getModel('catalog/product')->load($productId);
        if (!$product->getId() || !$product->isVisibleInCatalog()) {
            $session->addError($this->__('Cannot specify product.'));
            $this->_redirect('*/');
            return;
        }

       

        try {

            $requestParams = $this->getRequest()->getParams();
            $customer = Mage::getSingleton('customer/session')->getCustomer();
            $productId = $requestParams['product'];
            $baseUrl = Mage::getBaseUrl();

             
            $_in_wishlist = false;

            if($customer->getId())
            {
                $wishlist = Mage::getModel('wishlist/wishlist')->loadByCustomer($customer, true);
                $wishListItemCollection = $wishlist->getItemCollection();

                if($wishListItemCollection->getData()){

                    foreach ($wishListItemCollection as $item){

                        if($productId == $item->getProductId()){

                            $_in_wishlist = true;

                            if ($session->getBeforeWishlistRequest()) {
                                $requestParams = $session->getBeforeWishlistRequest();
                                $session->unsBeforeWishlistRequest();
                            }

                            $errotMsg = $this->__('%1$s is already in wishlist. Click <a href="%2$s">here</a> to continue shopping.',
                            $product->getName(), Mage::helper('core')->escapeUrl($baseUrl));
                            $session->addError($errotMsg);
                           
                        }
                    }
                    if($_in_wishlist == false){
                        Mage::log('6',null,'an.log');

                        $buyRequest = new Varien_Object($requestParams);
                        $result = $wishlist->addNewItem($product, $buyRequest);
                        if (is_string($result)) {
                            Mage::throwException($result);
                        }
                        $wishlist->save();
                        Mage::log('7',null,'an.log');
                        Mage::dispatchEvent(
                            'wishlist_add_product',
                            array(
                                'wishlist' => $wishlist,
                                'product' => $product,
                                'item' => $result
                            )
                        );

                        $message = $this->__('%1$s has been added to your wishlist. Click <a href="%2$s">here</a> to continue shopping.',
                        $product->getName(), Mage::helper('core')->escapeUrl($baseUrl));
                        $session->addSuccess($message);

                    }
                }
                else{
                    if ($session->getBeforeWishlistRequest()) {
                        $requestParams = $session->getBeforeWishlistRequest();
                        $session->unsBeforeWishlistRequest();
                    }
                    if($_in_wishlist == false){
                        $buyRequest = new Varien_Object($requestParams);
                        $result = $wishlist->addNewItem($product, $buyRequest);
                        if (is_string($result)) {
                            Mage::throwException($result);
                        }
                        $wishlist->save();

                        Mage::dispatchEvent(
                            'wishlist_add_product',
                            array(
                                'wishlist' => $wishlist,
                                'product' => $product,
                                'item' => $result
                            )
                        );
                    }
                    Mage::helper('wishlist')->calculate();

                    $message = $this->__('%1$s has been added to your wishlist. Click <a href="%2$s">here</a> to continue shopping.',
                        $product->getName(), Mage::helper('core')->escapeUrl($baseUrl));
                    $session->addSuccess($message);
                }
                
            }
        } catch (Mage_Core_Exception $e) {
            $session->addError($this->__('An error occurred while adding item to wishlist: %s', $e->getMessage()));
        }
        catch (Exception $e) {
            $session->addError($this->__('An error occurred while adding item to wishlist.'));
        }

        $this->_redirect('*', array('wishlist_id' => $wishlist->getId()));
        // $session->clear();
    }

}