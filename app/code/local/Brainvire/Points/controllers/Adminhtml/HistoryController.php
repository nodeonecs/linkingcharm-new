<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   BV
 * @package    BV_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */


class Brainvire_Points_Adminhtml_HistoryController extends Mage_Adminhtml_Controller_Action {

    public function transactionHistoryGridAction() {

        $customerId = $this->getRequest()->getParam('id', 0);
        $historyTable = $this->getLayout()
                ->createBlock('points/adminhtml_customer_edit_tabs_rewardPoints_history_grid', '', array('customer_id' => $customerId));
        $this->getResponse()->setBody($historyTable->toHtml());
    }

}

?>
