<?php
/* @var $installer Mage_Sales_Model_Mysql4_Setup */
$installer = $this;

$installer->startSetup();

$installer->updateAttribute('catalog_product', 'gst_source', array(
    'frontend_label'  => 'Tax Rate(in Percentage)',
	'note'   => 'If the selling is performed under a state, GST amount will be divided equally into CGST and SGST. If it is performed intra state, the whole tax rate will be applied to IGST.'
));


$installer->updateAttribute('catalog_category', 'gst_cat_source', array(
    'frontend_label'  => 'Tax Rate(in Percentage)',
	'note'   => 'If the selling is performed under a state, GST amount will be divided equally into CGST and SGST. If it is performed intra state, the whole tax rate will be applied to IGST.'
));



$installer->addAttribute('catalog_product', 'gst_source_after_minprice', array(
    'type'                       => 'varchar',
    'label'                      => 'Tax Rate if Product Price Below Minimum Set Price',
    'input'                      => 'select',
    'required'                   => false,
    'sort_order'                 => 8,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_WEBSITE,
    'searchable'        => false,
	'filterable'        => false,
	'source' => 'gstcharge/source_gstoption',
	'note'   => 'If the product prices are below the set minimum price, tax rate set here will be applied.'
));

$installer->addAttribute('catalog_product', 'gst_source_minprice', array(
    'type'                       => 'decimal',
    'label'                      => 'Minimum Product Price to Apply Tax Rate',
    'input'                      => 'text',
    'required'                   => false,
    'sort_order'                 => 7,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_WEBSITE,
    'searchable'        => false,
	'filterable'        => false,
));



$installer->addAttribute('catalog_category', 'gst_cat_source_minprice', array(
    'group'         => 'General Information',
	'type'                       => 'decimal',
    'label'                      => 'Minimum Product Price to Apply Tax Rate',
    'input'                      => 'text',
    'required'                   => false,
    'sort_order'                 => 91,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_WEBSITE,
    'searchable'        => false,
	'filterable'        => false,
));

$installer->addAttribute('catalog_category', 'gst_cat_source_after_minprice', array(
    'group'         => 'General Information',
    'type'          => 'varchar',
    'label'         => 'Tax Rate if Product Price Below Minimum Set Price',
    'input'         => 'select',
    'required'      => false,
    'sort_order'    => 92,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_WEBSITE,
    'source'        => 'gstcharge/source_gstoption',
	'note'   => 'If the product prices are below the set minimum price, tax rate set here will be applied.'
));



$installer->endSetup();