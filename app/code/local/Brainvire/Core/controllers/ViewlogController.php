<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   Brainvire
 * @package    Brainvire_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * @license    
 */


class Brainvire_Core_ViewlogController extends Mage_Adminhtml_Controller_action {

    public function indexAction() {
        $this
                ->loadLayout()
                ->_addContent($this->getLayout()->createBlock('brainvirecore/adminhtml_log'))
                ->renderLayout();
    }

    public function clearAction() {
        try {
            Mage::getResourceSingleton('brainvirecore/logger')->truncateAll();
            Mage::getSingleton('adminhtml/session')->addSuccess("Log successfully cleared");
            $this->_redirect('*/*');
        } catch (Mage_Core_Exception $E) {
            Mage::getSingleton('adminhtml/session')->addError($E->getMessage());
            $this->_redirectReferer();
        }
        return $this;
    }

}