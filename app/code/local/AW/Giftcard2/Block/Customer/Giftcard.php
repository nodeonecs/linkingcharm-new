<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_Block_Customer_Giftcard extends Mage_Core_Block_Template
{
    public function getFormUrl()
    {
        return $this->getUrl(
            '*/*/check',
            array(
                '_secure' => Mage::app()->getStore(true)->isCurrentlySecure()
            )
        );
    }

    public function canShowGiftcardInfo()
    {
        if (Mage::getSingleton('customer/session')->getAwGiftCardCheckData()) {
            return true;
        }
        return false;
    }

    public function getGiftCardStatusInfo()
    {
        if (!$this->_statusInfo && Mage::getSingleton('customer/session')->getAwGiftCardCheckData()) {
            $availability = Mage::getModel('aw_giftcard2/source_giftcard_availability');
            $isUsed = Mage::getModel('aw_giftcard2/source_giftcard_used');
            $giftCardData = Mage::getSingleton('customer/session')->getAwGiftCardCheckData();

            $_statusInfo = new Varien_Object();
            $_statusInfo
                ->setCode($giftCardData['code'])
                ->setAvailability($availability->getOptionText($giftCardData['availability']))
                ->setIsUsed($isUsed->getOptionText($giftCardData['is_used']))
                ->setBalance($giftCardData['balance'])
            ;

            if (isset($giftCardData['expire_at'])) {
                $_statusInfo->setExpireAt($giftCardData['expire_at']);
            }
            $this->_statusInfo = $_statusInfo;
            Mage::getSingleton('customer/session')->unsAwGiftCardCheckData();;
        }
        return $this->_statusInfo;
    }

    public function getCode()
    {
        return $this->getGiftCardStatusInfo()->getCode();
    }

    public function getAvailability()
    {
        return $this->getGiftCardStatusInfo()->getAvailability();
    }

    public function getBalance()
    {
        return $this->getGiftCardStatusInfo()->getBalance();
    }

    public function getExpireAt()
    {
        return $this->getGiftCardStatusInfo()->getExpireAt();
    }

    public function getIsUsed()
    {
        return $this->getGiftCardStatusInfo()->getIsUsed();
    }
}