<?php
$installer = $this;

$installer->startSetup();

$installer->run("
ALTER TABLE amasty_amrolepermissions_rule
    ADD head_office tinyint(1) default 0 ;
");

$installer->endSetup();
