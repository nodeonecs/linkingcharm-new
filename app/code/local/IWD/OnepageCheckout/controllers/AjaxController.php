<?php
require_once 'Mage/Checkout/controllers/OnepageController.php';
class IWD_OnepageCheckout_AjaxController extends Mage_Checkout_OnepageController
{
protected function _getCart()
    {
        return Mage::getSingleton('checkout/cart');
    }
    
    protected function _getQuote()
    {
        return $this->_getCart()->getQuote();
    }

    public function cartupdateAction()
    {
        $a=$this->getRequest()->getParam('productid');
        $b=$this->getRequest()->getParam('update');
        $quote = $this->getQuote();
        $product = $quote->getItemById($a);
        if($b=="inc" && $a ){
          
            $qty = intval($product->getQty()+1);
            $maximumQty = intval(Mage::getModel('catalog/product')->load($product->getProductId())->getStockItem()->getMaxSaleQty());
            
            if($qty > $maximumQty){
            
                $result['error'] = $this->__('Product Has Reached To Maximum Allowed Qty: %s', $maximumQty);
            $this->getResponse()->setBody(Zend_Json::encode($result));
                return;
            }
            
            $product->setQty($qty);
            $this->_getQuote()->getShippingAddress()->setCollectShippingRates(true);
            
            $quote->collectTotals()
                    ->save();
                    $result['success'] = $this->__('Increased');
                    //$result['cart_qty'] = $qty;
                $this->getResponse()->setBody(Zend_Json::encode($result));
                return;
                    
        }
        else if ($b=="dec" && $a){
     
            $qty = intval($product->getQty()-1);
            $minimumQty = intval(Mage::getModel('catalog/product')->load($product->getProductId())->getStockItem()->getMinSaleQty());
                        
            if($qty < $minimumQty){
                
                $result['error'] = $this->__('Product Has Reached To Minimal Allowed Qty: %s', $minimumQty);
                $this->getResponse()->setBody(Zend_Json::encode($result));
                return;
            }
                    
            if($qty > 0){
                    $product->setQty($qty);
            }else{
                $quote->removeItem($a);
               }
            $this->_getQuote()->getShippingAddress()->setCollectShippingRates(true);
                    
            $quote->collectTotals()
                ->save();
                  $result['success'] = $this->__('Decreased');
                //$result['cart_qty'] = $qty;
                $this->getResponse()->setBody(Zend_Json::encode($result));
                return;
        }
    }

    private function getQuote()
    {
        return $this->getCheckout()->getQuote();
    }

    protected function getCheckout()
    {
        return $this->getOnepage()->getCheckout();
    }
}