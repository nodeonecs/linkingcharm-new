<?php
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$setup->startSetup();

$setup->run("	
	ALTER TABLE sales_flat_quote_item ALTER checkout_method SET DEFAULT 'deliverable';
	ALTER TABLE sales_flat_order_item ALTER checkout_method SET DEFAULT 'deliverable';	
");

$setup->endSetup();
