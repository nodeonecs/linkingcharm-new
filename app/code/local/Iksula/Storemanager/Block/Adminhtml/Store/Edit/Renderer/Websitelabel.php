<?php


class Iksula_Storemanager_Block_Adminhtml_Store_Edit_Renderer_Websitelabel
extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract{


public function render(Varien_Object $row)
    {
        $rowItemId = $row->getId();
        $StoreWebsiteId = Mage::getModel('core/store_group')->load($rowItemId)->getWebsiteId();

        $sWebsiteName = Mage::getModel('core/website')->load($StoreWebsiteId)->getName();


        return $sWebsiteName;
    }


}
