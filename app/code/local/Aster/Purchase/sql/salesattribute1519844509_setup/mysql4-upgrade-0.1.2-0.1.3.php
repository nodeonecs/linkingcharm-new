<?php
$installer = $this;
$installer->startSetup();

$installer->addAttribute("order_item", "hsncode", array("type"=>"varchar"));

$installer->addAttribute("invoice_item", "hsncode", array("type"=>"varchar"));

$installer->addAttribute("creditmemo_item", "hsncode", array("type"=>"varchar"));

$installer->addAttribute("shipment_item", "hsncode", array("type"=>"varchar"));

$installer->endSetup();
