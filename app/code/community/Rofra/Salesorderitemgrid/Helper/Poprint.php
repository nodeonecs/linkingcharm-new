<?php
class Rofra_Salesorderitemgrid_Helper_Poprint extends Mage_Core_Helper_Abstract{

public function createPOHTML($poData, $order_id, $orderDate){

		$websiteName = Mage::app()->getWebsite()->getName();
		$poOrderNo = 'po-'.$websiteName.'-'.$order_id;
		$BuyerCompanyNameddress = Mage::getStoreConfig('incom/completedata/buyer_address');
		$BuyerCompanyName = Mage::getStoreConfig('incom/completedata/buyer_name');
		$deliveryCompanyAddress = Mage::getStoreConfig('incom/completedata/delivery_company_address');
		$deliveryCompanyName = Mage::getStoreConfig('incom/completedata/delivery_company_name');
		$websiteCompanyName = Mage::getStoreConfig('incom/completedata/website_name');
		$websiteCompanyAddress = Mage::getStoreConfig('incom/completedata/website_address');
		$buyerVat = Mage::getStoreConfig('incom/completedata/buyer_vat');
		$buyerCst = Mage::getStoreConfig('incom/completedata/buyer_cst');
		$buyerCin = Mage::getStoreConfig('incom/completedata/buyer_cin');
		$buyerTin = Mage::getStoreConfig('incom/completedata/buyer_tin');
		$buyerEmail = Mage::getStoreConfig('incom/completedata/buyer_email');
		$buyerPhone = Mage::getStoreConfig('incom/completedata/buyer_contact');

		$vendorCode = Mage::getStoreConfig('incom/completedata/vendor_code'); 
		$vendorVat = Mage::getStoreConfig('incom/completedata/vendor_vat');
		$vendorCst = Mage::getStoreConfig('incom/completedata/vendor_cst');
 		$vendorCin = Mage::getStoreConfig('incom/completedata/vendor_cin');

 		$warehousePhone = Mage::getStoreConfig('incom/completedata/warehouse_contact');
		$warehouseEmail = Mage::getStoreConfig('incom/completedata/warehouse_email');


		$HTML = '<table cellpadding="0" cellspacing="0" width="100%" border="1" style="margin: 0px; padding: 0px; font-family: arial; font-size: 30px; border: 1px solid #000; margin:0 0 20px 0">
			        <tr>
			            <td width="100%" colspan="2" style="text-align: center; font-weight: bold; padding-top: 10px; border-bottom: 1px solid #000; padding: 15px 5px"><p>'.$BuyerCompanyName.'</p> </td>
			        </tr>
			        <tr>
			            <td valign="top" style="width: 60%; box-sizing: border-box; padding: 0px; border-bottom: 1px solid #000">
			                <table cellpadding="4" cellspacing="4" width="100%" style="margin:0px; padding: 0px; border: 1px solid #000;">
			                    <tr>
			                        <td style="width: 60%; border-right: 1px solid #000; border-bottom: 1px solid #000; padding: 10px">

			                        To,<br/> '.$websiteCompanyName.'<br/> '.$websiteCompanyAddress.'
			                        </td>
			                        <td style="width: 40%; border-bottom: 1px solid #000">
			                            <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px; padding: 0px;">
			                                <tr>
			                                    <td style="border-bottom: 1px solid #000 ">PURCHASE ORDER<br/> '.$poOrderNo.'</td>
			                                </tr>
			                                <tr>
			                                    <td style="border-bottom: 1px solid #000 ">NO: *'.$order_id.'* <br/>DATED: '.$orderDate.'</td> 
			                                </tr>
			                                <tr>
			                                    <td >Please quote P.O. Number on all correspondence, Invoice, challan etc</td>
			                                </tr>
			                            </table>
			                        </td>
			                    </tr>
			                    <tr>
			                        <td style="width: 60%; border-right: 1px solid #000">Vendor Code: '.$vendorCode.'<br/> Vendor VAT: '.$vendorVat.'<br/> Vendor CST: '.$vendorCst.'<br/> Vendor CIN : '.
			                        $vendorCin.'
			                        </td>
			                        <td style="width: 40%;">Payment Terms:<br/> P.O Expiry Date:<br/> Against Form:<br/> Buyer VAT: '.$buyerVat.'<br/> Buyer CST: '.$buyerCst.'<br/> Buyer CIN: '.$buyerCin.'
			                        </td>
			                    </tr>
			                </table>    
			            </td>
			            <td width="40%" style="border-left: 1px solid #000; border-bottom: 1px solid #000">
			                <table cellpadding="4" cellspacing="0" width="100%" style="margin: 0px; padding: 0px; border: 1px solid #000000">
			                    <tr>
			                        <td width="50%" style="padding: 2px; box-sizing: border-box; border-bottom: 1px solid #000; border-right: 1px solid #000">
			                            SendOriginal Tax Invoice to :<br/>'.$BuyerCompanyName.'<br/>'.$BuyerCompanyNameddress.'<br/>TinNo. '.$buyerTin.'
			                        </td>
			                        <td width="50%" style="padding: 2px; box-sizing: border-box; border-bottom: 1px solid #000">Regarding thisorder<br/>Contact:-<br/>Phone: '.$buyerPhone.'<br/>Email ID: '.$buyerEmail.'
			                        </td>
			                    </tr>
			                    <tr>
			                        <td width="50%" style="padding: 2px; box-sizing: border-box; border-right: 1px solid #000">
			                            Delivery at :<br/>'.$deliveryCompanyName.'<br/>'.$deliveryCompanyAddress.'
			                        </td>
			                        <td width="50%" style="padding: 2px; box-sizing: border-box">
			                            Ware House:<br/>Contact No.: '.$warehousePhone .'<br/>Email ID: '.$warehouseEmail.'
			                        </td>
			                    </tr>
			                </table>
			            </td>       
			        </tr>
			        <tr>
			            <td width="100%" colspan="2" style="font-weight: bold; padding-top: 10px; border-bottom: 1px solid #000;">Delivery Mode:</td>
			        </tr>
			        <tr>
			            <td width="100%" colspan="2" style="font-weight: bold; padding-top: 0px; border-bottom: 1px solid #000;">
			                <table cellpadding="4" cellspacing="0" width="100%" style="margin: 0px; ">
			                    <tr>
			                        <td style="width: 20%; border-right: 1px solid #000">VAT/CST Inclusive</td>
			                        <td style="width: 20%; border-right: 1px solid #000">Excise Duty:<br/>    Not applicable</td>
			                        <td style="width: 20%; border-right: 1px solid #000">Freight Charges:<br/>    Inclusive</td>
			                        <td style="width: 20%; border-right: 1px solid #000"></td>
			                        <td style="width: 20%;">Form:<br/>    As Applicable</td>
			                    </tr>
			                </table>    
			            </td>
			        </tr>
			        <tr>
			            <td width="100%" colspan="2" style="font-weight: bold; padding-top: 0px; ">
			                <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px; ">
			                    <tr>
			                        <td style="width: 33%; border-right: 1px solid #000">Payment Terms:<br/>As per agreement</td>
			                        <td style="width: 33%; border-right: 1px solid #000">
			                            <table cellpadding="4" cellspacing="0" width="100%" style="margin: 0px; ">
			                                <tr><td>Packing And Forwarding:<br/>Proper packing by you</td></tr>
			                                <tr>
			                                    <td>
			                                        <table cellpadding="4" cellspacing="0" width="100%" style="margin: 0px; border: 1px solid #000 ">
			                                            <tr>
			                                                <td height="100px" style="width: 33%; border-right: 1px solid #000;">Packing And Forwarding:</td>
			                                                <td height="100px" style="width: 33%; border-right: 1px solid #000;">Packing And Forwarding:</td>
			                                                <td height="100px" style="width: 33%;">Packing And Forwarding:</td>
			                                            </tr>
			                                        </table>
			                                    </td>
			                                </tr>
			                            </table>
			                        </td>
			                        <td style="width: 33%;">
			                            <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px; ">
			                                <tr><td>Packing And Forwarding:<br/>    Proper packing by you</td></tr>
			                                <tr><td>For INCOM SERVICES PVT LTD<br/>( AUTHORISED SIGNATORY )</td></tr>
			                            </table>
			                        </td>
			                    </tr>
			                </table>
			            </td>
			        </tr>
			</table> <p>&nbsp;</p>';
	        $HTML .= '<table cellpadding="0" cellspacing="0" width="100%" border="1" style="margin: 0px; padding: 0px; font-family: arial; font-size: 30px; border: 1px solid #000; margin:0 0 20px 0">
	        <tr style="text-align: center; font-weight: bold; padding-top: 10px; border-bottom: 1px solid #000; padding: 15px 5px">
	        	<td>PO Code</td>
	        	<td>Order Id</td>
	        	<td>Sub Order Id</td>
	        	<td>SKU</td>
	        	<td>QTY</td>
	        	<td>Unit Price</td>
	        	<td>Total Price</td>
	        	<td>PO created By</td>
	        	<td>Payment Method</td>
	        	<td>Invoice Id</td>
	        </tr>';
	        foreach ($poData as $pod) {	
	            $HTML.= '<tr style="text-align: center; font-weight: bold; padding-top: 10px; border-bottom: 1px solid #000; padding: 15px 5px">';
	            $HTML.= "<td>".$pod['pocode']."</td><td>".$pod['order_id']."</td><td>".$pod['sub_orderid']."</td><td>".$pod['sku']."</td><td>".$pod['qty']."</td><td>".$pod['unit_price']."</td><td>".$pod['total']."</td><td>".$pod['po_created_by']."</td><td>".$pod['payment_method']."</td><td>".$pod['invoice_id']."</td>";
	            $HTML.= '</tr>';
	        }
	        $HTML.= "</table>";
	        return $HTML;
		}
}

?>