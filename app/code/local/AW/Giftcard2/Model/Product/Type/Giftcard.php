<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_Model_Product_Type_Giftcard extends Mage_Catalog_Model_Product_Type_Abstract
{
    /**
     * Gift Card product type code
     */
    const TYPE_CODE = 'aw_giftcard2';

    /**
     * Gift Card product attributes codes
     */
    const ATTRIBUTE_CODE_TYPE               = 'aw_gc2_type';
    const ATTRIBUTE_CODE_DESCRIPTION        = 'aw_gc2_description';
    const ATTRIBUTE_CODE_EXPIRE             = 'aw_gc2_expire';
    const ATTRIBUTE_CODE_ALLOW_MESSAGE      = 'aw_gc2_allow_message';
    const ATTRIBUTE_CODE_EMAIL_TEMPLATES    = 'aw_gc2_email_templates';
    const ATTRIBUTE_CODE_AMOUNTS            = 'aw_gc2_amounts';
    const ATTRIBUTE_CODE_ALLOW_OPEN_AMOUNT  = 'aw_gc2_allow_open_amount';
    const ATTRIBUTE_CODE_OPEN_AMOUNT_MIN    = 'aw_gc2_open_amount_min';
    const ATTRIBUTE_CODE_OPEN_AMOUNT_MAX    = 'aw_gc2_open_amount_max';

    /**
     * Gift Card Product option code to process buy request to quote item and order item
     */
    const CUSTOM_OPTIONS_CODE                       = 'aw_gc2_options';

    /**
     * Buy request attributes codes
     */
    const BUY_REQUEST_ATTR_CODE_AMOUNT              = 'aw_gc2_amount';
    const BUY_REQUEST_ATTR_CODE_CUSTOM_AMOUNT       = 'aw_gc2_custom_amount';
    const BUY_REQUEST_ATTR_CODE_SENDER_NAME         = 'aw_gc2_sender_name';
    const BUY_REQUEST_ATTR_CODE_SENDER_EMAIL        = 'aw_gc2_sender_email';
    const BUY_REQUEST_ATTR_CODE_RECIPIENT_NAME      = 'aw_gc2_recipient_name';
    const BUY_REQUEST_ATTR_CODE_RECIPIENT_EMAIL     = 'aw_gc2_recipient_email';
    const BUY_REQUEST_ATTR_CODE_EMAIL_TEMPLATE      = 'aw_gc2_template';
    const BUY_REQUEST_ATTR_CODE_EMAIL_TEMPLATE_NAME = 'aw_gc2_template_name';
    const BUY_REQUEST_ATTR_CODE_HEADLINE            = 'aw_gc2_headline';
    const BUY_REQUEST_ATTR_CODE_MESSAGE             = 'aw_gc2_message';

    /**
     * Additional order item options
     */
    const ORDER_ITEM_CODE_EMAIL_SENT            = 'aw_gc2_email_sent';
    const ORDER_ITEM_CODE_CREATED_CODES         = 'aw_gc2_created_codes';

    /**
     * Gift Card Product option codes to process buy request
     *
     * @var array
     */
    protected $_buyRequestOptionCodes = array(
        self::BUY_REQUEST_ATTR_CODE_AMOUNT,
        self::BUY_REQUEST_ATTR_CODE_CUSTOM_AMOUNT,
        self::BUY_REQUEST_ATTR_CODE_EMAIL_TEMPLATE,
        self::BUY_REQUEST_ATTR_CODE_SENDER_NAME,
        self::BUY_REQUEST_ATTR_CODE_SENDER_EMAIL,
        self::BUY_REQUEST_ATTR_CODE_RECIPIENT_NAME,
        self::BUY_REQUEST_ATTR_CODE_RECIPIENT_EMAIL,
        self::BUY_REQUEST_ATTR_CODE_HEADLINE,
        self::BUY_REQUEST_ATTR_CODE_MESSAGE
    );

    /**
     * Gift Card Product option codes to process buy request
     *
     * @var array
     */
    /*protected $_quoteToOrderOptionCodes = array(
        self::BUY_REQUEST_ATTR_CODE_AMOUNT,
        self::BUY_REQUEST_ATTR_CODE_EMAIL_TEMPLATE,
        self::BUY_REQUEST_ATTR_CODE_EMAIL_TEMPLATE_NAME,
        self::BUY_REQUEST_ATTR_CODE_SENDER_NAME,
        self::BUY_REQUEST_ATTR_CODE_SENDER_EMAIL,
        self::BUY_REQUEST_ATTR_CODE_RECIPIENT_NAME,
        self::BUY_REQUEST_ATTR_CODE_RECIPIENT_EMAIL,
        self::BUY_REQUEST_ATTR_CODE_HEADLINE,
        self::BUY_REQUEST_ATTR_CODE_MESSAGE
    );*/

    protected $_canUseQtyDecimals  = false;
    protected $_canConfigure = true;

    public function getCustomOptionsCode()
    {
        return self::CUSTOM_OPTIONS_CODE;
    }

    public function getTypeValue($product = null)
    {
        $product = $this->getProduct($product);
        if ($this->_isNeedToLoadProduct($product)) {
            $product = Mage::getModel('catalog/product')->load($product->getId());
        }
        $typeValue = $product->getData(self::ATTRIBUTE_CODE_TYPE);
        return $typeValue;
    }

    /**
     * Returns selected Amount value
     *
     * @param Mage_Catalog_Model_Product $product
     * @return null || AmountValue
     */
    public function getAmountOptionValue($product)
    {
        $options = $this->getCustomOptions($product);
        if (isset($options[self::BUY_REQUEST_ATTR_CODE_AMOUNT])) {
            return $options[self::BUY_REQUEST_ATTR_CODE_AMOUNT];
        }
        return null;
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @return bool
     */
    public function isVirtualGCType($product = null)
    {
        return (
            $this->getTypeValue($product) ==
                AW_Giftcard2_Model_Source_Entity_Attribute_Giftcard_Type::VIRTUAL_VALUE
        );
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @return bool
     */
    public function isPhysicalGCType($product = null)
    {
        return (
            $this->getTypeValue($product) ==
                AW_Giftcard2_Model_Source_Entity_Attribute_Giftcard_Type::PHYSICAL_VALUE
        );
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @return bool
     */
    public function isCombinedGCType($product = null)
    {
        return (
            $this->getTypeValue($product) ==
                AW_Giftcard2_Model_Source_Entity_Attribute_Giftcard_Type::COMBINED_VALUE
        );
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @return bool
     */
    public function isVirtual($product = null)
    {
        return $this->isVirtualGCType($product);
    }

    /**
     * Check if product available for sale
     *
     * @param Mage_Catalog_Model_Product $product
     * @return bool
     */
    public function isSalable($product = null)
    {
        if ($this->isAllowOpenAmount($product) || count($this->getAmountOptions($product)) > 0) {
            return parent::isSalable($product);
        }
        return false;
    }

    /**
     * Giftcard always has required options
     *
     * @param Mage_Catalog_Model_Product $product
     * @return bool
     */
    public function hasRequiredOptions($product = null)
    {
        return true;
    }

    /**
     * Returns hasOptions only if product has custom options (compatibility with AW Mobile 3)
     *
     * @param Mage_Catalog_Model_Product $product
     * @return bool
     */
    public function hasOptions($product = null)
    {
        if (
            $this->getProduct($product)->getOptions()
            || Mage::helper('aw_giftcard2')->isMobileVersionEnabled()
        ) {
            return true;
        }
        return false;
    }

    /**
     * Sets hasOptions for all gift card products (compatibility with AW Mobile 3)
     *
     * @param Mage_Catalog_Model_Product $product
     * @return $this
     */
    public function beforeSave($product = null)
    {
        parent::beforeSave($product);
        $this->getProduct($product)->setTypeHasOptions(true);
        return $this;
    }

    /**
     * Retrieves amounts of given Gift Card product
     *
     * @param Mage_Catalog_Model_Product $product
     * @return array
     */
    public function getAmounts(Mage_Catalog_Model_Product $product)
    {
        $amounts = array();
        $websiteId = Mage::getModel('core/store')->load($product->getStoreId())->getWebsiteId();
        if (!$product->getData(self::ATTRIBUTE_CODE_AMOUNTS)) {
            $attribute = $product->getResource()->getAttribute(self::ATTRIBUTE_CODE_AMOUNTS);
            if ($attribute) {
                $attribute->getBackend()->afterLoad($product);
            }
        }
        $_amounts = $product->getData(self::ATTRIBUTE_CODE_AMOUNTS);
        if (is_array($_amounts)) {
            foreach ($_amounts as $data) {
                if (in_array($data['website_id'], array($websiteId, 0))) {
                    if (!in_array($data['price'], $amounts)) {
                        $amounts[] = $data['price'];
                    }
                }
            }
        }
        return $amounts;
    }

    /**
     * Get amount options
     *
     * @param Mage_Catalog_Model_Product $product
     * @return array
     */
    public function getAmountOptions(Mage_Catalog_Model_Product $product)
    {
        $amountOptions = $this->getAmounts($product);
        sort($amountOptions);
        return $amountOptions;
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @return bool
     */
    public function isAllowOpenAmount(Mage_Catalog_Model_Product $product)
    {
        return (bool)$product->getData(self::ATTRIBUTE_CODE_ALLOW_OPEN_AMOUNT);
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @return mixed
     */
    public function getOpenAmountMin(Mage_Catalog_Model_Product $product)
    {
        return $product->getData(self::ATTRIBUTE_CODE_OPEN_AMOUNT_MIN);
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @return mixed
     */
    public function getOpenAmountMax(Mage_Catalog_Model_Product $product)
    {
        return $product->getData(self::ATTRIBUTE_CODE_OPEN_AMOUNT_MAX);
    }

    public function getGiftCardDescription(Mage_Catalog_Model_Product $product)
    {
        return $product->getData(self::ATTRIBUTE_CODE_DESCRIPTION);
    }

    /**
     * Get email templates options
     *
     * @param Mage_Catalog_Model_Product $product
     * @return array
     */
    public function getTemplateOptions(Mage_Catalog_Model_Product $product)
    {
        $templateOptions = array();
        $storeId = $product->getStoreId();
        foreach ($product->getData(self::ATTRIBUTE_CODE_EMAIL_TEMPLATES) as $data) {
            if (in_array($data['store_id'], array($storeId, 0))) {
                $_tmpArr = array(
                    'template'      => $data['template'],
                    'image'         => $data['image'],
                );
                if (!in_array($_tmpArr, $templateOptions)) {
                    $templateOptions[] = $_tmpArr;

                }
            }
        }
        return $templateOptions;
    }

    /** Check if message is allowed for $product
     *
     * @return bool
     */
    public function isAllowMessage(Mage_Catalog_Model_Product $product)
    {
        return (bool)$product->getData(self::ATTRIBUTE_CODE_ALLOW_MESSAGE);
    }

    /**
     * Prepare selected options for product
     *
     * @param  Mage_Catalog_Model_Product $product
     * @param  Varien_Object $buyRequest
     * @return array
     */
    public function processBuyRequest($product, $buyRequest)
    {
        $options = array();
        foreach ($this->_buyRequestOptionCodes as $code) {
            if ($buyRequest->hasData($code)) {
                $options[$code] = $buyRequest->getData($code);
            }
        }

        return $options;
    }

    /**
     * Prepare for Cart override (Magento 1.4.x compatibility)
     *
     * @param Varien_Object $buyRequest
     * @param Mage_Catalog_Model_Product $product
     * @return array|string
     */
    public function prepareForCart(Varien_Object $buyRequest, $product = null)
    {
        if (!method_exists(get_parent_class($this), 'prepareForCartAdvanced')) {
            $result = parent::prepareForCart($buyRequest, $product);
            if (is_string($result)) {
                return $result;
            }
            $this->_prepareProduct($buyRequest, $product, null);
            return $result;
        }
        return parent::prepareForCart($buyRequest, $product);
    }


    /**
     * Prepare for Cart override (Magento 1.5.x+)
     *
     * @param Varien_Object $buyRequest
     * @param Mage_Catalog_Model_Product $product
     * @param string $processMode
     * @return array|string
     */
    protected function _prepareProduct(Varien_Object $buyRequest, $product, $processMode)
    {
        $result = array();
        if (method_exists(get_parent_class($this), 'prepareForCartAdvanced')) {
            $result = parent::_prepareProduct($buyRequest, $product, $processMode);
            if (is_string($result)) {
                return $result;
            }
        }
        try {
            $this->_validateBuyRequest($buyRequest, $product, $processMode);
            $amount = $this->_getAmount($buyRequest, $product);
            $this->_validateAmount($buyRequest, $product, $processMode, $amount);
        } catch (Mage_Core_Exception $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            Mage::logException($e);
            return Mage::helper('aw_giftcard2')->__('An error has occurred while adding product to cart.');
        }

        $this->_addGiftcardOptionsFromBuyRequest($product, $buyRequest, $amount);

        return $result;
    }

    /**
     * Validate BuyRequest params
     *
     * @param Varien_Object $buyRequest
     * @param Mage_Catalog_Model_Product $product
     * @param string $processMode
     * @throws Mage_Core_Exception
     */
    protected function _validateBuyRequest(Varien_Object $buyRequest, $product, $processMode)
    {
        if ($this->_isStrictProcessMode($processMode)) {
            if ($this->_isCustomAmount($buyRequest, $product)) {
                if ($buyRequest->getData(self::BUY_REQUEST_ATTR_CODE_CUSTOM_AMOUNT) <= 0) {
                    Mage::throwException(
                        Mage::helper('aw_giftcard2')->__('Please specify Gift Card amount.')
                    );
                }
            }
            if (!$buyRequest->getData(self::BUY_REQUEST_ATTR_CODE_RECIPIENT_NAME)) {
                Mage::throwException(
                    Mage::helper('aw_giftcard2')->__('Please specify recipient name.')
                );
            }
            if (!$buyRequest->getData(self::BUY_REQUEST_ATTR_CODE_SENDER_NAME)) {
                Mage::throwException(
                    Mage::helper('aw_giftcard2')->__('Please specify sender name.')
                );
            }
            if (!$this->isPhysicalGCType($product)) {
                if (!$buyRequest->getData(self::BUY_REQUEST_ATTR_CODE_RECIPIENT_EMAIL)) {
                    Mage::throwException(
                        Mage::helper('aw_giftcard2')->__('Please specify recipient email.')
                    );
                }
                if (!$buyRequest->getData(self::BUY_REQUEST_ATTR_CODE_SENDER_EMAIL)) {
                    Mage::throwException(
                        Mage::helper('aw_giftcard2')->__('Please specify sender email.')
                    );
                }

                if (!$buyRequest->getData(self::BUY_REQUEST_ATTR_CODE_EMAIL_TEMPLATE)) {
                    $noTemplateError = true;
                } else {
                    $templateId = $buyRequest->getData(self::BUY_REQUEST_ATTR_CODE_EMAIL_TEMPLATE);
                    $productTemplates = $this->getTemplateOptions($product);
                    $noTemplateError = true;
                    foreach ($productTemplates as $templateArr) {
                        if ($templateArr['template'] == $templateId) {
                            $noTemplateError = false;
                            break;
                        }
                    }
                }
                if ($noTemplateError) {
                    Mage::throwException(
                        Mage::helper('aw_giftcard2')->__('Please specify a design.')
                    );
                }
            }
        }
    }

    /**
     * Checks if custom amount is selected in $buyRequest
     *
     * @param Varien_Object $buyRequest
     * @param Mage_Catalog_Model_Product $product
     * @return bool
     */
    protected function _isCustomAmount(Varien_Object $buyRequest, $product)
    {
        return (
            $buyRequest->getData(self::BUY_REQUEST_ATTR_CODE_AMOUNT) == 'custom' &&
            $product->getData(self::ATTRIBUTE_CODE_ALLOW_OPEN_AMOUNT)
        );
    }

    /**
     * Validate amount (is_set, min/max)
     *
     * @param Varien_Object $buyRequest
     * @param $product
     * @param $processMode
     * @param $amount
     * @throws Mage_Core_Exception
     */
    protected function _validateAmount(Varien_Object $buyRequest, $product, $processMode, $amount)
    {
        $minOpenAmount = $product->getData(self::ATTRIBUTE_CODE_OPEN_AMOUNT_MIN);
        $maxOpenAmount = $product->getData(self::ATTRIBUTE_CODE_OPEN_AMOUNT_MAX);

        if ($this->_isStrictProcessMode($processMode)) {
            if (is_null($amount)) {
                Mage::throwException(
                    Mage::helper('aw_giftcard2')->__('Please specify Gift Card amount.')
                );
            }
            if ($this->_isCustomAmount($buyRequest, $product)) {

                if ($minOpenAmount && $amount < $minOpenAmount) {
                    $formattedAmount = Mage::helper('core')->currency($minOpenAmount, true, false);
                    Mage::throwException(
                        Mage::helper('aw_giftcard2')->__('Minimum allowed Gift Card amount is %s', $formattedAmount)
                    );
                }

                if ($maxOpenAmount && $amount > $maxOpenAmount ) {
                    $formattedAmount = Mage::helper('core')->currency($maxOpenAmount, true, false);
                    Mage::throwException(
                        Mage::helper('aw_giftcard2')->__('Maximum allowed Gift Card amount is %s', $formattedAmount)
                    );
                }
            }
        }
    }

    /**
     * Gets amount from buyRequest
     *
     * @param Varien_Object $buyRequest
     * @param Mage_Catalog_Model_Product $product
     * @return int|mixed|null|string
     */
    protected function _getAmount(Varien_Object $buyRequest, $product)
    {
        $amountOptions = $this->getAmountOptions($product);
        $selectedAmountOption = $buyRequest->getData(self::BUY_REQUEST_ATTR_CODE_AMOUNT);
        $customAmount = $buyRequest->getData(self::BUY_REQUEST_ATTR_CODE_CUSTOM_AMOUNT);

        $amount = null;
        if ($this->_isCustomAmount($buyRequest, $product)) {
            $currentCurrencyRate = Mage::app()->getStore()->getCurrentCurrencyRate();
            if ($currentCurrencyRate != 1) {
                $amount = Mage::app()->getStore()->roundPrice($customAmount / $currentCurrencyRate);
            } else {
                $amount = $customAmount;
            }
        }
        elseif (is_numeric($selectedAmountOption) && in_array($selectedAmountOption, $amountOptions)) {
            $amount = $selectedAmountOption;
        }
        elseif (count($amountOptions) == 1) {
            $amount = array_shift($amountOptions);
        }
        elseif ($product->getCustomOption(self::BUY_REQUEST_ATTR_CODE_AMOUNT)) {
            $amount = $product->getCustomOption(self::BUY_REQUEST_ATTR_CODE_AMOUNT)->getValue();
        }

        return $amount;
    }

    /**
     * Adds custom options to Gift Card product from BuyRequest
     *
     * @param Mage_Catalog_Model_Product $product
     * @param Varien_Object $buyRequest
     * @param $amount
     * @return $this
     */
    protected function _addGiftcardOptionsFromBuyRequest($product, $buyRequest, $amount)
    {
        $giftcardOptions = array();

        if (isset($amount)) {
            $giftcardOptions[self::BUY_REQUEST_ATTR_CODE_AMOUNT] = $amount;
        }

        $giftcardOptions[self::BUY_REQUEST_ATTR_CODE_SENDER_NAME] =
            $buyRequest->getData(self::BUY_REQUEST_ATTR_CODE_SENDER_NAME);

        $giftcardOptions[self::BUY_REQUEST_ATTR_CODE_RECIPIENT_NAME] =
            $buyRequest->getData(self::BUY_REQUEST_ATTR_CODE_RECIPIENT_NAME);

        if (!$this->isPhysicalGCType($product)) {

            $giftcardOptions[self::BUY_REQUEST_ATTR_CODE_SENDER_EMAIL] =
                $buyRequest->getData(self::BUY_REQUEST_ATTR_CODE_SENDER_EMAIL);

            $giftcardOptions[self::BUY_REQUEST_ATTR_CODE_RECIPIENT_EMAIL] =
                $buyRequest->getData(self::BUY_REQUEST_ATTR_CODE_RECIPIENT_EMAIL);

            $emailTemplateId = $buyRequest->getData(self::BUY_REQUEST_ATTR_CODE_EMAIL_TEMPLATE);
            $giftcardOptions[self::BUY_REQUEST_ATTR_CODE_EMAIL_TEMPLATE] = $emailTemplateId;

            $giftcardOptions[self::BUY_REQUEST_ATTR_CODE_EMAIL_TEMPLATE_NAME] =
                Mage::getModel('aw_giftcard2/source_giftcard_email_template')->getOptionText($emailTemplateId);
        }

        if ($product->getData(self::ATTRIBUTE_CODE_ALLOW_MESSAGE)) {

            $giftcardOptions[self::BUY_REQUEST_ATTR_CODE_HEADLINE] =
                $buyRequest->getData(self::BUY_REQUEST_ATTR_CODE_HEADLINE);

            $giftcardOptions[self::BUY_REQUEST_ATTR_CODE_MESSAGE] =
                $buyRequest->getData(self::BUY_REQUEST_ATTR_CODE_MESSAGE);
        }

        $product->addCustomOption($this->getCustomOptionsCode(), serialize($giftcardOptions), $product);

        return $this;
    }

    /**
     * Checks if current process mode is strict (compatibility with Magento 1.4.x)
     *
     * @param string $processMode
     * @return bool
     */
    protected function _isStrictProcessMode($processMode)
    {
        $isStrictProcessMode = true;
        if (method_exists(get_parent_class($this), '_isStrictProcessMode')) {
            $isStrictProcessMode = parent::_isStrictProcessMode($processMode);
        }
        return $isStrictProcessMode;
    }

    /**
     * Returns all Gift Card custom options
     *
     * @param Mage_Catalog_Model_Product $product
     * @return array
     */
    public function getCustomOptions($product)
    {
        $options = $product->getCustomOption($this->getCustomOptionsCode());
        if ($options) {
            $value = $options->getValue();
            if ($value) {
                return unserialize($options->getValue());
            }
        }
        return array();
    }

    /**
     * Gets gift card product related option values
     *
     * @param Mage_Catalog_Model_Product $product
     * @param null $storeId
     * @return array
     */
    public function getGiftCardProductOptions(Mage_Catalog_Model_Product $product)
    {
        $options = array();
        $keys = array (
            self::ATTRIBUTE_CODE_TYPE,
            self::ATTRIBUTE_CODE_EXPIRE
        );

        $product->load($product->getId());
        foreach ($keys as $key) {
            if ($product->hasData($key)) {
                $options[$key] = $product->getData($key);
            }
        }
        return $options;
    }

    private function _isNeedToLoadProduct($product = null)
    {
        return (
            $this->getProduct($product)->getData(self::ATTRIBUTE_CODE_TYPE) == null &&
            !(
                Mage::app()->getStore()->isAdmin() &&
                Mage::app()->getRequest()->getControllerName() != 'sales_order_create'
            )
        );
    }
}
