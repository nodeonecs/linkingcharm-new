<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */


class Brainvire_Points_Block_Adminhtml_Rate_Spend_Grid extends Brainvire_Points_Block_Adminhtml_Rate_Earn_Grid {

    protected function _getDirection() {
        return Brainvire_Points_Model_Rate::POINTS_TO_CURRENCY;
    }

}
