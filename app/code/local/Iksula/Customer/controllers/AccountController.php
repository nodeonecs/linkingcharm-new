<?php 
require_once 'Mage/Customer/controllers/AccountController.php';
class Iksula_Customer_AccountController extends Mage_Customer_AccountController
{
    public function changepasswordAction(){
        $this->loadLayout();
        $this->renderLayout();
    } 
     public function editPostAction()
    {
        if (!$this->_validateFormKey()) {
            return $this->_redirect('*/*/changepassword');
        }

        if ($this->getRequest()->isPost()) {
            /** @var $customer Mage_Customer_Model_Customer */
            $customer = $this->_getSession()->getCustomer();
            $oldEmail = $customer->getEmail();

            /** @var $customerForm Mage_Customer_Model_Form */
            $customerForm = $this->_getModel('customer/form');
            $customerForm->setFormCode('customer_account_edit')
                ->setEntity($customer);

            $customerData = $customerForm->extractData($this->getRequest());
            // Set mobile in customer data
            $customerData['mobile'] = $this->getRequest()->getPost('mobile');
            //$customer->setMobile($customerData['mobile']);
            $errors = array();
            $customerErrors = $customerForm->validateData($customerData);
            if ($customerErrors !== true) {
                $errors = array_merge($customerErrors, $errors);
            } else {
                $customerForm->compactData($customerData);
                $errors = array();

                // Set mobile no in customer object
                if($customerData['mobile']){
                    $customer->setMobile($customerData['mobile']); 
                }

                // If password change was requested then add it to common validation scheme
                if ($this->getRequest()->getParam('change_password')) {
                    $currPass   = $this->getRequest()->getPost('current_password');
                    $newPass    = $this->getRequest()->getPost('password');
                    $confPass   = $this->getRequest()->getPost('confirmation');

                    $oldPass = $this->_getSession()->getCustomer()->getPasswordHash();
                    if ( $this->_getHelper('core/string')->strpos($oldPass, ':')) {
                        list($_salt, $salt) = explode(':', $oldPass);
                    } else {
                        $salt = false;
                    }

                    if ($customer->hashPassword($currPass, $salt) == $oldPass) {
                        if (strlen($newPass)) {
                            /**
                             * Set entered password and its confirmation - they
                             * will be validated later to match each other and be of right length
                             */
                            $customer->setPassword($newPass);
                            $customer->setConfirmation($confPass);
                        } else {
                            $errors[] = $this->__('New password field cannot be empty.');
                        }
                    } else {
                        $errors[] = $this->__('Invalid current password');
                    }
                }

                // Validate account and compose list of errors if any
                $customerErrors = $customer->validate();
                if (is_array($customerErrors)) {
                    $errors = array_merge($errors, $customerErrors);
                }
            }

            if (!empty($errors)) {
                $this->_getSession()->setCustomerFormData($this->getRequest()->getPost());
                foreach ($errors as $message) {
                    $this->_getSession()->addError($message);
                }
                // commented by Deepthi Reddy
                // $this->_redirect('*/*/changepassword');
                $this->_redirect('customer/account');
                return $this;
            }

            try {
                $customer->setConfirmation(null);
                $customer->save();

                $newEmail = $customer->getEmail();
                if($newEmail != $oldEmail) {
                    $userName = $customer->getFirstname();
                    
                    $emailTemplate  = Mage::getModel('core/email_template')->loadByCode('updated_email');
                    if($emailTemplate->getId()) {

                        $templateId = $emailTemplate->getId();
                        //Getting the Store E-Mail Sender Name.
                        $senderName = Mage::getStoreConfig('trans_email/ident_general/name');

                        //Getting the Store General E-Mail.
                        $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');

                        $sender = array('name' => $senderName,
                                        'email' => $senderEmail);

                        $vars = array();
                        $vars = array('emailaddress'=>$newEmail,'name'=>$userName);

                        $receiver = array($newEmail,$oldEmail);

                        $storeId = Mage::app()->getStore()->getId();
                        $translate = Mage::getSingleton('core/translate');
                        Mage::getModel('core/email_template')
                            ->sendTransactional($templateId, $sender, $receiver, $userName, $vars, $storeId);
                        $translate->setTranslateInline(true);
                    }
                }



                $this->_getSession()->setCustomer($customer)
                    ->addSuccess($this->__('The account information has been saved.'));

                $this->_redirect('customer/account');
                return;
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->setCustomerFormData($this->getRequest()->getPost())
                    ->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->setCustomerFormData($this->getRequest()->getPost())
                    ->addException($e, $this->__('Cannot save the customer.'));
            }

        }

        $this->_redirect('*/*/changepassword');
    }

    public function editprofilesettingAction(){
    	$this->loadLayout();  	
        $this->renderLayout();
    }

    public function saveprofileAction(){
    	 if (!$this->_validateFormKey()) {

            $response["status"] = "error";
            $response["message"] = "Invalid Form Key";
        }
        try{
	        $profile_name = $this->getRequest()->getParam('profile_name');
	        $profile_status = $this->getRequest()->getParam('profile_status');
	        if(empty($profile_status)){
	        	$response["status"] = "error";
	            $response["message"] = "Data required";	
	        }else{
	        	if(isset($profile_name)){
		        	$result = Mage::getModel('customer/customer')
				              ->getCollection()
				              ->addAttributeToSelect('profile_id')
				              ->addAttributeToFilter('profile_id',array('eq'=> $profile_name))->load();
					
					if (count($result)>0) {

					    /* code to show error message */
					    $response["status"] = "error";
		          	    $response["message"] = "Profile name already exist.";

					}else{ 
			        $customer = Mage::getSingleton('customer/session')->getCustomer();
			        $customer->setProfileId($profile_name);	
				        if($profile_status == "no"){
				        	$customer->setProfileStatus(0);		
				        }else{
				        	$customer->setProfileStatus(1);		
				        }

					$customer->save();
					$response["status"] = "success";
		            $response["message"] = "Profile setting saved successfully.";	
		            $response["publicprofile"] =  Mage::getUrl('wishlistnew/index/publicprofile').'p/'.$profile_name;
		            $response["publicwishlist"] =  Mage::getUrl('wishlistnew/index/index').'p/'.$profile_name;
	        		}
	        	}else{
	        		$customer = Mage::getSingleton('customer/session')->getCustomer();
				        if($profile_status == "no"){
				        	$customer->setProfileStatus(0);		
				        }else{
				        	$customer->setProfileStatus(1);		
				        }

					$customer->save();
					$profile_name = $customer->getResource()->getAttribute('profile_id')->getFrontend()->getValue($customer);
					$response["status"] = "success";
		            $response["message"] = "Profile setting saved successfully.";	
		            $response["publicprofile"] =  Mage::getUrl('wishlistnew/index/publicprofile').'p/'.$profile_name;
		            $response["publicwishlist"] =  Mage::getUrl('wishlistnew/index/index').'p/'.$profile_name;
	        	}
			}
		}
		catch(Exception $ex){
			$response["status"] = "error";
            $response["message"] = "Something goes wrong.";	
		}
		$this->getResponse()->setHeader('Content-type', 'application/json');
		$this->getResponse()->setBody(json_encode($response));
    }

    public function showpublicwishlistAction(){
    	$this->loadLayout();
    	$userid = $this->getRequest()->getParam('p');
    	$result = Mage::getModel('customer/customer')
			              ->getCollection()
			              ->addAttributeToSelect('profile_id')
			              ->addAttributeToSelect('profile_status')
			              ->addAttributeToFilter('profile_id',array('eq'=> $userid))->getFirstItem();
		if(count($result) > 0 && $result->getProfileStatus()){
					
			$customer = Mage::getModel('customer/customer')->setWebsiteId(Mage::app()->getWebsite()->getId())->loadByEmail($result->getEmail());
			
			$wishlist = Mage::getModel('wishlist/wishlist')->loadByCustomer($customer, true);
			$wishListItemCollection = $wishlist->getItemCollection();
			$block = $this->getLayout()->getBlock('publicwishlist');
			$block->setwishlistCollection($wishListItemCollection);
			$block->setpubliccustomerCollection($customer); 
	
		}else{
			Mage::getSingleton('core/session')->addSuccess($this->__('Sorry, This Wishlist Is Not Public'));
		}	              
		$this->renderLayout();
    }
    

}