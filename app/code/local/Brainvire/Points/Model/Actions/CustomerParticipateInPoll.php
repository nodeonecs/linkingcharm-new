<?php
/**
* Brainvire Infotech Pvt. Ltd
  * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */


class Brainvire_Points_Model_Actions_CustomerParticipateInPoll extends Brainvire_Points_Model_Actions_Abstract {

    protected $_action = 'customer_participate_in_poll';
    protected $_comment = 'Reward for participating in poll';

    protected function _applyLimitations($amount) {
        $pointLimitForAction = Mage::helper('points/config')->getPointsLimitForParticipatingInPoll();

        $collection = Mage::getModel('points/transaction')
                ->getCollection()
                ->addFieldToFilter('summary_id', $this->getSummary()->getId())
                ->addFieldToFilter('action', $this->getAction())
                ->limitByDay(Mage::getModel('core/date')->gmtTimestamp());

        
        $summ = 0;
        foreach ($collection as $transaction) {
            $summ += $transaction->getBalanceChange();
        }

        return parent::_applyLimitations($this->_calculateNewAmount($summ, $amount, $pointLimitForAction));
    }

}
