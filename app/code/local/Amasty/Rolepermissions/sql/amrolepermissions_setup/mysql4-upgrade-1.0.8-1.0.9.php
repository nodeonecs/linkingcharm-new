<?php
$installer = $this;

$installer->startSetup();

$installer->run("
ALTER TABLE amasty_amrolepermissions_rule
    ADD custom_role varchar(255) default NULL ;
");

$installer->endSetup();
