<?php
class Rofra_Salesorderitemgrid_Block_Adminhtml_Order_Items_Grid_Renderer_Manageproductimage extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $product = Mage::getModel('catalog/product')->load($row->getId());
        $proimage = Mage::getModel('catalog/product_media_config')->getMediaUrl( $product->getThumbnail() );
        $out = "<img src=". $proimage ." width='70px'/>";
        return $out;
    }
}
