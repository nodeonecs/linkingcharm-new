<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_Block_Adminhtml_Giftcard_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{

    public function __construct()
    {
        $this->_objectId = 'id';
        $this->_controller = 'adminhtml_giftcard';
        $this->_blockGroup = 'aw_giftcard2';
        $this->_formScripts[] = "
            function saveAndContinueEdit(url) {
               editForm.submit(
                    url.replace(/{{tab_id}}/, aw_giftcard2_info_tabsJsTabs.activeTab.id)
                );

            }
            function activationConfirm(message, url) {
                if (confirm(message)) {
                    editForm.submit(url);
                }
            }
            "
        ;
        parent::__construct();
    }

    protected function _prepareLayout()
    {
        $this->_addButton('save_and_continue', array(
            'label' => $this->__('Save and Continue Edit'),
            'onclick' => 'saveAndContinueEdit(\'' . $this->_getSaveAndContinueUrl() . '\')',
            'class' => 'save'
        ), 10);

        /** @var $giftcard AW_Giftcard2_Model_Giftcard */
        $giftcard = Mage::registry('current_giftcard');
        if ($giftcard->getId()) {

            if (!$giftcard->isPhysical()) {
                $this->_addButton('save_and_continue_and_send_giftcard', array(
                    'label' => $this->__('Save and Resend Gift Card'),
                    'onclick' => 'saveAndContinueEdit(\'' . $this->_getSaveAndSendGiftCardUrl() . '\')',
                    'class' => 'saveAndSend'
                ), 10);
            }

            $this->_removeButton('delete');
            if ($giftcard->isDeactivated()) {
                $this->_addButton('activate', array(
                    'label' => $this->__('Activate'),
                    'class' => 'delete',
                    'onclick' => 'activationConfirm(
                        \'' . Mage::helper('core')->jsQuoteEscape($this->__('Are you sure you want to do this?')) . '\',
                        \''. $this->_getActivateGiftCardUrl(). '\')',
                ));
            } else {
                $this->_addButton('deactivate', array(
                    'label' => $this->__('Deactivate'),
                    'class' => 'delete',
                    'onclick' => 'activationConfirm(
                        \'' . Mage::helper('core')->jsQuoteEscape($this->__('Are you sure you want to do this?')) . '\',
                        \'' . $this->_getDeactivateGiftCardUrl() . '\')',
                ));
            }
        } else {
            $this->_addButton('save_and_continue_and_send_giftcard', array(
                'label' => $this->__('Save and Send Gift Card'),
                'onclick' => 'saveAndContinueEdit(\'' . $this->_getSaveAndSendGiftCardUrl() . '\')',
                'class' => 'saveAndSend'
            ), 10);
        }
        parent::_prepareLayout();
    }

    protected function _getSaveAndContinueUrl()
    {
        return $this->getUrl('*/*/save', array(
            '_current' => true,
            'back' => 'edit',
            'tab' => '{{tab_id}}'
        ));
    }

    protected function _getSaveAndSendGiftCardUrl()
    {
        return $this->getUrl('*/*/save', array(
            '_current' => true,
            'back' => 'edit',
            'tab' => '{{tab_id}}',
            'sendEmail' => '1'
        ));
    }

    protected function _getActivateGiftCardUrl()
    {
        return $this->getUrl('*/*/activate', array('_current' => true));
    }

    protected function _getDeactivateGiftCardUrl()
    {
        return $this->getUrl('*/*/deactivate', array('_current' => true));
    }

    /**
     * Getter for form header text
     *
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry('current_giftcard')->getId()) {
            return $this->__("Edit Gift Card Code");
        }
        else {
            return $this->__('New Gift Card Code');
        }
    }
}