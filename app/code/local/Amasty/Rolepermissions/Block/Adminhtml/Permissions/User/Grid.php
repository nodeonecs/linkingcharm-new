<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2013 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Adminhtml permissions user grid
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Amasty_Rolepermissions_Block_Adminhtml_Permissions_User_Grid extends Mage_Adminhtml_Block_Permissions_User_Grid
{

    public function __construct()
    {

        parent::__construct();
        $this->setId('permissionsUserGrid');
        $this->setDefaultSort('username');
        $this->setDefaultDir('asc');
        $this->setUseAjax(true);
    }

    public function showRolepermission(){


        $admin_user_session = Mage::getSingleton('admin/session');
        $adminuserId = $admin_user_session->getUser()->getUserId();
        $currentroleid = Mage::getModel('admin/user')->load($adminuserId)->getRole()->getId();


        $AdvancedCustomRole = Mage::getModel('amrolepermissions/rule')->load($currentroleid , 'role_id')->getCustomRole();



        return array($AdvancedCustomRole , $currentroleid) ;

    }

    protected function _prepareCollection()
    {

        list($role_custom , $role_id) = $this->showRolepermission();

        if($role_custom){
             if($role_custom == 'W_HO'){

             $Ho_websiteId = Mage::getModel('amrolepermissions/rule')->load($role_id , 'role_id')->getScopeWebsites();

            $storesViewsCollections = Mage::getModel('amrolepermissions/rule')->getCollection()->addFieldToFilter('custom_role' , array('eq' => 'STORE_VIEW'))->addFieldToSelect(array('scope_storeviews' , 'role_id'));
            $aStoreRoleId = array();
            foreach($storesViewsCollections as $stores){

                $aStoresViews = explode(',' , $stores->getScopeStoreviews());

                $iStoreViewsWebsiteID = Mage::getModel('core/store')->load($aStoresViews[0])->getWebsiteId();

                if($iStoreViewsWebsiteID == $Ho_websiteId){
                    $aStoreRoleId [] = $stores->getRoleId();
                }

            }

            $CustomRoleId = Mage::getModel('amrolepermissions/rule')->getCollection()->addFieldToFilter('custom_role' , array('nin' => array('S_A' , 'W_HO')))->addFieldToFilter('role_id' , array('in' => $aStoreRoleId))->addFieldToSelect('role_id');
            //exit;
            $User_ids = array();
            foreach($CustomRoleId as $roleid){

                $UserCollections = Mage::getModel('admin/role')->getCollection()->addFieldToFilter('parent_id' , array('eq' => $roleid->getRoleId()))->addFieldToSelect(array('user_id'));

                foreach($UserCollections as $userid){

                    $User_ids [] = $userid->getUserId();

                }
            }

            $collection =  Mage::getModel("admin/user")->getCollection()->addFieldToFilter('user_id' , array('in' => $User_ids));
            //exit;

        }else{

            $collection = Mage::getResourceModel('admin/user_collection');

        }

        }else{
            $collection = Mage::getResourceModel('admin/user_collection');
        }

        //$collection = Mage::getResourceModel('admin/user_collection');
        $this->setCollection($collection);
        //return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('user_id', array(
            'header'    => Mage::helper('adminhtml')->__('ID'),
            'width'     => 5,
            'align'     => 'right',
            'sortable'  => true,
            'index'     => 'user_id'
        ));

        $this->addColumn('username', array(
            'header'    => Mage::helper('adminhtml')->__('User Name'),
            'index'     => 'username'
        ));

        $this->addColumn('firstname', array(
            'header'    => Mage::helper('adminhtml')->__('First Name'),
            'index'     => 'firstname'
        ));

        $this->addColumn('lastname', array(
            'header'    => Mage::helper('adminhtml')->__('Last Name'),
            'index'     => 'lastname'
        ));

        $this->addColumn('email', array(
            'header'    => Mage::helper('adminhtml')->__('Email'),
            'width'     => 40,
            'align'     => 'left',
            'index'     => 'email'
        ));

        $this->addColumn('is_active', array(
            'header'    => Mage::helper('adminhtml')->__('Status'),
            'index'     => 'is_active',
            'type'      => 'options',
            'options'   => array('1' => Mage::helper('adminhtml')->__('Active'), '0' => Mage::helper('adminhtml')->__('Inactive')),
        ));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('user_id' => $row->getId()));
    }

    public function getGridUrl()
    {
        //$uid = $this->getRequest()->getParam('user_id');
        return $this->getUrl('*/*/roleGrid', array());
    }

}
