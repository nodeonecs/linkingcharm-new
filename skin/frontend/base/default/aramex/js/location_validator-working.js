/**
 *
 * @category    design
 * @package     base_default 
 * 
 */ 
 /* billing_aramex_cities and  shipping_aramex_cities */
 var billingAramexCitiesObj;
 var shippingAramexCitiesObj;
 var billing_aramex_cities_temp;
 var shipping_aramex_cities_temp;
 
jQuery(function(){
	billing_aramex_cities_temp = billing_aramex_cities;
	shipping_aramex_cities_temp = shipping_aramex_cities;
	
	if(jQuery("input[name='billing[use_for_shipping]']").prop('checked')){
		billingAramexCitiesObj = AutoSearchControls('billing',billing_aramex_cities);
	}	
	shippingAramexCitiesObj = AutoSearchControls('shipping',shipping_aramex_cities);
	
	jQuery("select[name='billing[country_id]']").change(function(){
			getAllCitiesJson('billing');			
	});
	jQuery("select[name='shipping[country_id]']").change(function(){
			getAllCitiesJson('shipping');
	});	
});

function AutoSearchControls(type,search_city){
	return jQuery('input[name="'+type+'[city]"]')
	.autocomplete({
		/*source: search_city,*/
		minLength: 0,
		scroll: true,
		source: function(req, responseFn) {
			var re = jQuery.ui.autocomplete.escapeRegex(req.term);
			var matcher = new RegExp( "^" + re, "i" );
			var a = jQuery.grep( search_city, function(item,index){
				return matcher.test(item);
			});
			responseFn( a );
		},
		response: function( event, ui ){		
			if(type =='billing' && billing_aramex_cities_temp==''){			
				var temp_arr =[];
				jQuery(ui.content).each(function(i,v){
					temp_arr.push(v.value);
				});
				billing_aramex_cities_temp = temp_arr;
				billingAramexCitiesObj.autocomplete("option","source",function(req, responseFn) {
					var re = jQuery.ui.autocomplete.escapeRegex(req.term);
					var matcher = new RegExp( "^" + re, "i" );
					var a = jQuery.grep( billing_aramex_cities_temp, function(item,index){
						return matcher.test(item);
					});
					responseFn( a );
				});				
				jQuery('input[name="'+type+'[city]"]').val('');
				
			}				
			if(type =='shipping' && shipping_aramex_cities_temp==''){
				
				var temp_arr =[];
				jQuery(ui.content).each(function(i,v){
					temp_arr.push(v.value);
				});
				shipping_aramex_cities_temp = temp_arr;
				shippingAramexCitiesObj.autocomplete("option","source",function(req, responseFn) {
					var re = jQuery.ui.autocomplete.escapeRegex(req.term);
					var matcher = new RegExp( "^" + re, "i" );
					var a = jQuery.grep( billing_aramex_cities_temp, function(item,index){
						return matcher.test(item);
					});
					responseFn( a );
				});
				jQuery('input[name="'+type+'[city]"]').val('');
			}
		},
		select : function(event, ui) {
			jQuery('input[name="'+type+'[city]"]').attr('rel',ui.item.label);
		},
		open : function() {
			jQuery('input[name="'+type+'[city]"]').attr('rel', 0);
		},
		close : function() {                    
			if (jQuery('input[name="'+type+'[city]"]').attr('rel')=='0')
				jQuery('input[name="'+type+'[city]"]').val('');
		}		
	}).focus(function () { jQuery(this).autocomplete("search", ""); });
	
}	

 

 
function getAllCitiesJson(type){
	var system_base_url = getSystemBaseUrl();
	var country_code = jQuery("select[name='"+type+"[country_id]']").val();
	var mycities = ''; 
	var url_check = system_base_url+"apilocationvalidator/index/searchallcitiesJson?country_code="+country_code;
	if(type="billing"){
		billing_aramex_cities_temp = '';
		billingAramexCitiesObj.autocomplete("option","source",url_check);
	}	
}

 function applyAramexValidator(type){  	 
	var country_code = jQuery("select[name='"+type+"[country_id]']").val();
	 var system_base_url = getSystemBaseUrl();
		if(country_code){
		  jQuery.ajax({url: system_base_url+"/apilocationvalidator/index/applyapivalidator", data:{country_code:country_code}, type:'Post', success: function(result){
			var response = jQuery.parseJSON(result);			
			if(typeof(response.post_code_required) != 'undefined'){
				if(response.post_code_required == "1"){					
					jQuery("input[name='"+type+"[postcode]']").addClass('required-entry');					
				}else{
					/* set city as requried */
					jQuery("input[name='"+type+"[postcode]']").removeClass('required-entry');
					jQuery("input[name='"+type+"[city]']").addClass('required-entry');					
				}
			}		
		}
		});
	}
 }
 
