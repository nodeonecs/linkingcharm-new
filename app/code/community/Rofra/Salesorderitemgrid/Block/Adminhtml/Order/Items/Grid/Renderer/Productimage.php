<?php


class Rofra_Salesorderitemgrid_Block_Adminhtml_Order_Items_Grid_Renderer_Productimage
extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract{


public function render(Varien_Object $row)
    {
        $rowItemId = $row->getId();

        $product_id = Mage::getSingleton('sales/order_item')->load($rowItemId)->getProductId();
        $productObj = Mage::getModel('catalog/product')->load($product_id);
        $img = Mage::helper('catalog/image')->init($productObj, 'thumbnail');
        $html = '<center><img width="60px" src="'.$img.'" />';
        return $html;

    }


}
