<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2015 Amasty (https://www.amasty.com)
 * @package Amasty_Rolepermissions
 */

class Amasty_Rolepermissions_Model_System_Store extends Mage_Adminhtml_Model_System_Store
{
    /**
     * Get websites as id => name associative array
     *
     * @param bool $withDefault
     * @param string $attribute
     * @return array
     */

    /*public function __construct()
    {
        return $this->_isAdminScopeAllowed = true;
        parent::_contruct();
    }*/


    public function getWebsiteOptionHash($withDefault = false, $attribute = 'name')
    {
        $options = parent::getWebsiteOptionHash($withDefault, $attribute);

        $rule = Mage::helper('amrolepermissions')->currentRule();

        if ($rule->getScopeStoreviews())
        {
            $accessible = $rule->getPartiallyAccessibleWebsites();

            if (isset($options[0]))
                unset($options[0]); // Unset admin store

            foreach ($options as $id => $value)
            {
                if (!in_array($id, $accessible))
                    unset($options[$id]);
            }
        }

        return $options;
    }

    public function getStoreValuesForFormCustom($empty = false, $all = false)
    {
        /******************  If the current Login is HO(Head Office) of specific website than we have to show only the store views of specific websites for selection **********/

        $iLoginuserid = Mage::getSingleton('admin/session')->getUser()->getUserId();

        $LoginUserRole = Mage::getModel('admin/user')->load($iLoginuserid)->getRole()->getRoleId();

        $iRole_Collectionsid = Mage::getSingleton('amrolepermissions/rule')
                                                ->getCollection()
                                                ->addFieldToFilter('role_id' , array('eq' => $LoginUserRole));


                $ScopeWebsitesid = "";

                 foreach($iRole_Collectionsid as $Role){

                        $RoleCustom = $Role->getCustomRole();
                        if($RoleCustom == 'W_HO'){
                            $aScopeWebsitesid = explode(',',$Role->getScopeWebsites());
                        }
                 }

         /********************************************************************/

        $options = array();
        if ($empty) {
            $options[] = array(
                'label' => '',
                'value' => ''
            );
        }
        if ($all && $this->_isAdminScopeAllowed) {
            $options[] = array(
                'label' => Mage::helper('adminhtml')->__('All Store Views'),
                'value' => 0
            );
        }

        $nonEscapableNbspChar = html_entity_decode('&#160;', ENT_NOQUOTES, 'UTF-8');

        //Mage::log(print_r($this->_websiteCollection->getData() , 1) , null , 'testper.log');

        foreach ($this->_websiteCollection as $website) {
            $websiteShow = false;
            if($aScopeWebsitesid){ // If the website id is present.
                if(!in_array($website->getId(),$aScopeWebsitesid)){ // Check if the website_id in loop has a permission to show for selection.
                    continue;
                }

            }
            foreach ($this->_groupCollection as $group) {
                if ($website->getId() != $group->getWebsiteId()) {
                    continue;
                }
                $groupShow = false;
                foreach ($this->_storeCollection as $store) {
                    if ($group->getId() != $store->getGroupId()) {
                        continue;
                    }
                    if (!$websiteShow) {
                        $options[] = array(
                            'label' => $website->getName(),
                            'value' => array()
                        );
                        $websiteShow = true;
                    }
                    if (!$groupShow) {
                        $groupShow = true;
                        $values    = array();
                    }

                    $isHub = Mage::getModel('iksula_storemanager/storehubrelation')->load($group->getId() , 'store_id')->getIsHub();

                    // var_dump($isHub);
                    
                if($isHub == '0'){

                    $values[] = array(
                        'label' => str_repeat($nonEscapableNbspChar, 4) . $store->getName(),
                        'value' => $store->getId()
                    );
                }

                    /*$values[] = array(
                        'label' => str_repeat($nonEscapableNbspChar, 4) . $store->getName(),
                        'value' => $store->getId()
                    );*/
                }
                /*if ($groupShow) {
                    $options[] = array(
                        'label' => str_repeat($nonEscapableNbspChar, 4) . $group->getName(),
                        'value' => $values
                    );
                }*/
                if($isHub == '0'){
                    if ($groupShow) {
                        $options[] = array(
                            'label' => str_repeat($nonEscapableNbspChar, 4) . $group->getName(),
                            'value' => $values
                        );
                    }
                }
            }
        }
        return $options;
    }


    public function getStoreValuesForFormCustomHub($empty = false, $all = false)
    {
        /******************  If the current Login is HO(Head Office) of specific website than we have to show only the store views of specific websites for selection **********/

        $iLoginuserid = Mage::getSingleton('admin/session')->getUser()->getUserId();

        $LoginUserRole = Mage::getModel('admin/user')->load($iLoginuserid)->getRole()->getRoleId();

        $iRole_Collectionsid = Mage::getSingleton('amrolepermissions/rule')
                                                ->getCollection()
                                                ->addFieldToFilter('role_id' , array('eq' => $LoginUserRole));


                $ScopeWebsitesid = "";

                 foreach($iRole_Collectionsid as $Role){

                        $RoleCustom = $Role->getCustomRole();
                        if($RoleCustom == 'W_HO'){
                            $aScopeWebsitesid = explode(',',$Role->getScopeWebsites());
                        }
                 }

         /********************************************************************/

        $options = array();
        if ($empty) {
            $options[] = array(
                'label' => '',
                'value' => ''
            );
        }
        if ($all && $this->_isAdminScopeAllowed) {
            $options[] = array(
                'label' => Mage::helper('adminhtml')->__('All Store Views'),
                'value' => 0
            );
        }

        $nonEscapableNbspChar = html_entity_decode('&#160;', ENT_NOQUOTES, 'UTF-8');

        //Mage::log(print_r($this->_websiteCollection->getData() , 1) , null , 'testper.log');

        foreach ($this->_websiteCollection as $website) {
            $websiteShow = false;
            if($aScopeWebsitesid){ // If the website id is present.
                if(!in_array($website->getId(),$aScopeWebsitesid)){ // Check if the website_id in loop has a permission to show for selection.
                    continue;
                }

            }
            foreach ($this->_groupCollection as $group) {
                if ($website->getId() != $group->getWebsiteId()) {
                    continue;
                }
                $groupShow = false;
                foreach ($this->_storeCollection as $store) {
                    if ($group->getId() != $store->getGroupId()) {
                        continue;
                    }
                    if (!$websiteShow) {
                        $options[] = array(
                            'label' => $website->getName(),
                            'value' => array()
                        );
                        $websiteShow = true;
                    }
                    if (!$groupShow) {
                        $groupShow = true;
                        $values    = array();
                    }

             $isHub = Mage::getModel('iksula_storemanager/storehubrelation')->load($group->getId() , 'store_id')->getIsHub();

                if($isHub == '1'){

                    $values[] = array(
                        'label' => str_repeat($nonEscapableNbspChar, 4) . $store->getName(),
                        'value' => $store->getId()
                    );
                }


                }

                //echo $group->getId().'<===>';


                if($isHub == '1'){
                    if ($groupShow) {
                        $options[] = array(
                            'label' => str_repeat($nonEscapableNbspChar, 4) . $group->getName(),
                            'value' => $values
                        );
                    }
                }
            }
        }
        return $options;
    }
}
