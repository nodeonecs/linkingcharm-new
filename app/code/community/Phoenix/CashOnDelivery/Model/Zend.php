<?php
/**
 * Extending Zend_Captcha
 *
 * @category   Phoenix
 * @package    Phoenix_CashOnDelivery
 * @author     Leo.r
 */

class Phoenix_CashOnDelivery_Model_Zend extends Mage_Captcha_Model_Zend{


	/**
     * Whether captcha is required to be inserted to this form
     *
     * @param null|string $login
     * @return bool
     */
    
    public function __construct($params)
        {
            if (!isset($params['formId'])) {
                throw new Exception('formId is mandatory');
            }
            $this->_formId = $params['formId'];
            $this->setExpiration($this->getTimeout());

            $this->setDotNoiseLevel(10);     // Added code
            $this->setLineNoiseLevel(0);     // Added code
        }
    public function isRequired($login = null)
    {
        

            if(!in_array($this->_formId, $this->_getTargetForms()) =='onepagecheckout_payment'){
            	if ($this->_isUserAuth() || !$this->_isEnabled() || !in_array($this->_formId, $this->_getTargetForms())) {
                    
                   // if($this->_formId=='')
                        return false;
                }
            }

        return ($this->_isShowAlways() || $this->_isOverLimitAttempts($login)
            || $this->getSession()->getData($this->_getFormIdKey('show_captcha'))
        );
    }

}