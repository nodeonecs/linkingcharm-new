<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_CardController extends Mage_Core_Controller_Front_Action
{
    protected function _validateUser()
    {
        if (!Mage::getSingleton('customer/session')->getCustomerId()) {
            Mage::getSingleton('customer/session')->authenticate($this);
            return false;
        }
        return true;
    }

    public function indexAction()
    {
        if (!$this->_validateUser()) {
            return;
        }
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->getLayout()->getBlock('head')->setTitle($this->__('Gift Card'));
        $this->renderLayout();
    }

    public function checkAction()
    {
        if (!$this->_validateUser()) {
            return;
        }

        try {
            $giftCardCode = $this->_initCard();
            Mage::getSingleton('customer/session')->setAwGiftCardCheckData($giftCardCode->getData());
        } catch (Exception $e) {
            Mage::getSingleton('customer/session')->addError($this->__($e->getMessage()));
        }
        $this->_redirect('*/card/index');
    }

    protected function _initCard()
    {
        $giftcardCode = $this->getRequest()->getParam('giftcard_code', null);
        if (null === $giftcardCode) {
            throw new Mage_Core_Exception($this->__('Please enter gift card code.'));
        }

        $giftcardModel = Mage::getModel('aw_giftcard2/giftcard')->loadByCode(trim($giftcardCode));
        if (null === $giftcardModel->getId()) {
            throw new Mage_Core_Exception(
                $this->__('Gift Card Code %s is not valid.', Mage::helper('core')->escapeHtml($giftcardCode))
            );
        }

        $giftcardModel->validate();

        return $giftcardModel;
    }

    public function applyAction()
    {
        try {
            $giftcardModel = $this->_initCard();
            Mage::helper('aw_giftcard2/giftcard')->addToQuote($giftcardModel);

            Mage::getSingleton('checkout/session')->addSuccess(
                $this->__('Gift Card Code %s has been applied.', Mage::helper('core')->escapeHtml($giftcardModel->getCode()))
            );
        } catch (Exception $e) {
            Mage::getSingleton('checkout/session')->addError($this->__($e->getMessage()));
        }

        $this->_redirectReferer();
    }

    public function ajaxApplyAction()
    {
        try {
            $giftcardModel = $this->_initCard();
            Mage::helper('aw_giftcard2/giftcard')->addToQuote($giftcardModel);

            Mage::getSingleton('checkout/session')->addSuccess(
                $this->__('Gift Card Code %s has been applied.', Mage::helper('core')->escapeHtml($giftcardModel->getCode()))
            );
        } catch (Exception $e) {
            Mage::getSingleton('checkout/session')->addError($this->__($e->getMessage()));
        }

        $response = array(
            'count'    => (int)Mage::getSingleton('checkout/cart')->getSummaryQty(),
            'message'  => '',
            'cart'     => ''
        );
        $layout = $this->getLayout();
        $update = $layout->getUpdate();
        $update->load('default');
        $layout->generateXml();
        $layout->generateBlocks();
        $cartBlock = $layout->getBlock('checkout.cart');
        if ($cartBlock) {
            $response['cart'] = $cartBlock->toHtml();
        }

        $this->getResponse()->setBody(Zend_Json::encode($response));
    }

    public function removeAction()
    {
        $giftcardCode = $this->getRequest()->getParam('code', null);
        if ($giftcardCode) {
            $giftcardCode = base64_decode($giftcardCode);
            $giftcard = Mage::getModel('aw_giftcard2/giftcard')->loadByCode(trim($giftcardCode));
            if ($giftcard->getId()) {
                try {
                    Mage::helper('aw_giftcard2/giftcard')->removeFromQuote($giftcard->getId());
                    Mage::getSingleton('checkout/session')->addSuccess(
                        $this->__('Gift Card Code %s has been removed.', Mage::helper('core')->escapeHtml(trim($giftcardCode)))
                    );
                } catch (Exception $e) {
                    Mage::getSingleton('checkout/session')->addError($this->__('Cannot remove gift card code.'));
                }
            }
        }
        $this->_redirectReferer();
    }
}
