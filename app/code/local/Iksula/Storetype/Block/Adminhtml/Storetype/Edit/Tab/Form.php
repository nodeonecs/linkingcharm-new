<?php
class Iksula_Storetype_Block_Adminhtml_Storetype_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("storetype_form", array("legend"=>Mage::helper("storetype")->__("Item information")));


						/*$fieldset->addField("id", "text", array(
						"label" => Mage::helper("storetype")->__("Store type Id"),
						"name" => "id",
						));*/

						$fieldset->addField("name", "text", array(
						"label" => Mage::helper("storetype")->__("Store type name"),
						"name" => "name",
						));

						$fieldset->addField("is_sellable", "select", array(
						"label" => Mage::helper("storetype")->__("Sellable"),
						"name" => "is_sellable",
		                "values" => array(
		                    array(
		                        'value' => 1,
		                        'label' => Mage::helper('storetype')->__('Yes'),
		                    ),
		                    array(
		                        'value' => 0,
		                        'label' => Mage::helper('storetype')->__('No'),
		                    ),
		                ),

						));



						$fieldset->addField("fulfilment", "select", array(
						"label" => Mage::helper("storetype")->__("Fulfilment"),
						"name" => "fulfilment",
		                "values" => array(
		                    array(
		                        'value' => 1,
		                        'label' => Mage::helper('storetype')->__('Yes'),
		                    ),
		                    array(
		                        'value' => 0,
		                        'label' => Mage::helper('storetype')->__('No'),
		                    ),
		                ),

						));


						$fieldset->addField("returns", "select", array(
						"label" => Mage::helper("storetype")->__("Returns"),
						"name" => "returns",
		                "values" => array(
		                    array(
		                        'value' => 1,
		                        'label' => Mage::helper('storetype')->__('Yes'),
		                    ),
		                    array(
		                        'value' => 0,
		                        'label' => Mage::helper('storetype')->__('No'),
		                    ),
		                ),

						));




				if (Mage::getSingleton("adminhtml/session")->getStoretypeData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getStoretypeData());
					Mage::getSingleton("adminhtml/session")->setStoretypeData(null);
				}
				elseif(Mage::registry("storetype_data")) {
				    $form->setValues(Mage::registry("storetype_data")->getData());
				}
				return parent::_prepareForm();
		}
}
