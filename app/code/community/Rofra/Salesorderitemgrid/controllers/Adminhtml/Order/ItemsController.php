<?php
/**
 * @category    Graphic Sourcecode
 * @package     Rofra_Salesorderitemgrid
 * @license     http://www.apache.org/licenses/LICENSE-2.0
 * @author      Rodolphe Franceschi <rodolphe.franceschi@gmail.com>
 */
require_once('lib/Tcpdf/tcpdf.php');
class Rofra_Salesorderitemgrid_Adminhtml_Order_ItemsController extends Mage_Adminhtml_Controller_Action
{


    protected function _isAllowed()
    {
        return true;
    }

    public function pocancelcallAction(){

        $order_id = $this->getRequest()->getParam('order_id');
        $order = Mage::getModel('sales/order')->load($order_id);
        Mage::helper('salesorderitemgrid/pocreatecancel')->pocancel($order);
        Mage::getSingleton('core/session')->addSuccess($this->__('Po cancel successfully'));
        $this->_redirect('*/sales_order/view', array('order_id' => $order_id));

    }

    public function poprintcallAction(){

        $order_id = $this->getRequest()->getParam('order_id');
        $orderDate = Mage::getModel('sales/order')->load($order_id)->getCreatedAt();
        $poData = Mage::getModel('omniapis/storepo')->getCollection()->addFieldToFilter('order_id',array('eq'=>$order_id))->getData();          
        
        $HTML = Mage::helper('salesorderitemgrid/poprint')->createPOHTML($poData, $order_id, $orderDate);

        // $this->_beforeGetPdf();
        // $this->_initRenderer('invoice');
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false,false ,false);//last parameter for  footer  if true then footer show. if false then footer not show
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetTitle("PO for ORDER ID ".$order_id);
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $pdf->SetMargins(10,15,10,TRUE);
        $pdf->SetAutoPageBreak(TRUE, 15);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->setLanguageArray($l);
        $pdf->SetFont('helvetica', 'B', 20);
        $pdf->SetFont('helvetica', '', 8);
        $pdf->AddPage();
        // $this->_setPdf($pdf);
        $pdf->writeHTML($HTML, true, false, false, false, '');

        ob_end_clean();
        $fileName = $order_id.'_po.pdf';
        $pdf->Output($fileName, 'D');

        $this->_redirect('*/sales_order/view', array('order_id' => $order_id));

    }


    public function indexAction()
    {
        $this->loadLayout()->_setActiveMenu('salesorderitemgrid/orderitems');
        $this->_addContent($this->getLayout()->createBlock('salesorderitemgrid/adminhtml_order_items'));
        $this->getLayout()->getBlock('head')->setTitle($this->__('Order Items'));
        $this->renderLayout();
    }

    public function ajaxUpdateFieldAction()
    {
        $fieldId = (int) $this->getRequest()->getParam('id');
        $title = $this->getRequest()->getParam('title');
        $attributeName = $this->getRequest()->getParam('attr');
        if ($fieldId && $attributeName) {
            $model = Mage::getModel('sales/order_item')->load($fieldId);
            $model->setData($attributeName, $title);
            $model->save();
        }
    }

    /**
     * Export product grid to CSV format
     */
    public function exportCsvAction()
    {
        $fileName   = 'sales_order_items.csv';
        $content    = $this->getLayout()->createBlock('salesorderitemgrid/adminhtml_order_items_grid')->getCsv();

        $this->_sendUploadResponse($fileName, $content);
    }

    /**
     * Export product grid to XML format
     */
    public function exportXmlAction()
    {
        $fileName   = 'sales_order_items.xml';
        $content    = $this->getLayout()->createBlock('salesorderitemgrid/adminhtml_order_items_grid')->getXml();

        $this->_sendUploadResponse($fileName, $content);
    }


    protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream')
    {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK','');

        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename='.$fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
    }

    public function getstoreinventoryAction()
    {

        $storecode = $this->getRequest()->getParam('storecode');
        $sku = $this->getRequest()->getParam('sku');

        $StoreCollection = Mage::getModel('iksula_customstoreinventory/storeinventory')->getCollection()
                                                            ->addFieldToFilter('sku' , array('eq' => $sku))
                                                            ->addFieldToFilter('store_code' , array('eq'=> $storecode));

        $storecollectionarray = $StoreCollection->getData();

        if(!empty($storecollectionarray))
        {
            foreach($StoreCollection as $Storecode)
            {
                $quantity = $Storecode->getInventory();

            }

        }
        else
        {

            $quantity = '0';

        }
        echo $quantity;
    }


    public function allocateOrdersStoresAction()
    {
            $OrdersItemsId = $this->getRequest()->getParam('orderItemsId');
            $storecode = $this->getRequest()->getParam('storecode');
            //$website_id = $this->getRequest()->getParam('website_id');

            $message = Mage::getModel('salesorderitemgrid/salesorderallocate')->saveAllocateData($OrdersItemsId , $storecode , $website_id);

            echo $message;
    }


    public function unallocateOrdersStoresAction()
    {

            $OrdersItemsId = $this->getRequest()->getParam('orderItemsId');
            $storecode = $this->getRequest()->getParam('storecode');
            $website_id = $this->getRequest()->getParam('website_id');

            //$website_id = $this->getRequest()->getParam('website_id');

            $message = Mage::getModel('salesorderitemgrid/salesorderallocate')->saveUnallocateData($OrdersItemsId , $storecode , $website_id);

            echo $message;
    }


    public function getstorelistbywebsitesAction()
    {

        $website_id = $this->getRequest()->getParam('websites_codes');
        $sku = $this->getRequest()->getParam('sku');

        $website_clause = "main_table.website_id = '".$website_id."' and core_store_group.store_definition !='hub'";
        $itemsCollections = Mage::getModel('iksula_customstoreinventory/storeinventory')->getCollection();
        $itemsCollections->getSelect()->join(
        array(
        'core_store_group'=>'core_store_group'),
        'main_table.store_code = core_store_group.group_id',
        array('core_store_group.name','main_table.sku','main_table.inventory','core_store_group.group_id')
        )->where($website_clause);

            foreach($itemsCollections as $items)
            {

                $StoresListData []= array('name' => $items->getName() , 'id' =>$items->getGroupId());

            }
            //$StoresListData = array_unique($StoresListData[]);
            $StoresListData = array_map("unserialize", array_unique(array_map("serialize", $StoresListData)));
        echo json_encode($StoresListData , JSON_FORCE_OBJECT);
    }

    protected function _initOrder()
    {
        $id = $this->getRequest()->getParam('order_id');
        $order = Mage::getModel('sales/order')->load($id);

        if (!$order->getId()) {
            $this->_getSession()->addError($this->__('This order no longer exists.'));
            $this->_redirect('*/*/');
            $this->setFlag('', self::FLAG_NO_DISPATCH, true);
            return false;
        }
        Mage::register('sales_order', $order);
        Mage::register('current_order', $order);
        return $order;
    }

    protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('salesorderitemgrid/orderitems')
            ->_addBreadcrumb($this->__('Sales'), $this->__('Sales'))
            ->_addBreadcrumb($this->__('Orders'), $this->__('Orders'));
        return $this;
    }

    public function viewAction()
    {
        $this->_title($this->__('Sales'))->_title($this->__('Orders'));

        $order = $this->_initOrder();

        if ($order) {

            $isActionsNotPermitted = $order->getActionFlag(
                Mage_Sales_Model_Order::ACTION_FLAG_PRODUCTS_PERMISSION_DENIED
            );
            if ($isActionsNotPermitted)
            {
                $this->_getSession()->addError($this->__('You don\'t have permissions to manage this order because of one or more products are not permitted for your website.'));
            }

            $this->_initAction();

            $this->_title(sprintf("#%s", $order->getRealOrderId()));

            $this->renderLayout();
        }
    }

    public function rejectedreasonAction()
    {

        $rejected_reason = $this->getRequest()->getParam('reason');
        $orderitemsid = $this->getRequest()->getParam('orderItemsId');

        //$isAutoallocateActive = Mage::getStoreConfig('omniapis_section/custom_settings/allocations_of_orders');

            //if($isAutoallocateActive == '1'){
                $saveresult = Mage::getModel('salesorderitemgrid/salesorderallocate')->saverejectedStatusautoallocated($rejected_reason, $orderitemsid);
            /*}else{
                $saveresult = Mage::getModel('salesorderitemgrid/salesorderallocate')->saverejectedreason($rejected_reason, $orderitemsid);
            }*/
        echo json_encode($saveresult) ;

    }


    public function pickupdateAction()
    {
        $params = $this->getRequest()->getParams();
        $item_id = $params['item_id'];
        $store_date = $params['store_date'];

        $item = Mage::getModel('sales/order_item')->load($item_id);
        $item->setPickupEndDate($store_date)->save();

         Mage::helper('salesorderitemgrid')->pickupdateChanged($item);
    }


     public function transferhubAction()
    {
        $orderitemsid = $this->getRequest()->getParam('orderItemsId');
        $hub_id = $this->getRequest()->getParam('hub_id');

        $saveresult = Mage::getModel('salesorderitemgrid/salesorderallocate')->savetransfertohub($hub_id, $orderitemsid);

        echo json_encode($saveresult) ;

    }


    public function setInvoicedShipmentStatusItems($savedQtys , $type){

        if($type == 'invoice'){
            $itemstatus = 'invoiced';
        }elseif($type == 'ship'){
            $itemstatus = 'dispatched';
        }

        foreach($savedQtys as $itemid => $qty){

                        if($qty != '0'){
                            $QtyOrdered = Mage::getModel('sales/order_item')->load($itemid)->getQtyOrdered();
                            $QtyInvoiced = Mage::getModel('sales/order_item')->load($itemid)->getQtyInvoiced();
                            $QtyShipped = Mage::getModel('sales/order_item')->load($itemid)->getQtyShipped();

                            if($type == 'ship' ){
                                $maintestvalue = $QtyShipped;                       
                                
                            }elseif($type == 'invoice'){
                                $maintestvalue = $QtyInvoiced;
                            }

                            if($QtyOrdered > $maintestvalue){

                                $OrdersId = Mage::getModel('sales/order_item')->load($itemid)->getOrderId();
                                $product_type = Mage::getModel('sales/order_item')->load($itemid)->getProductType();

                                if($product_type == 'configurable'){

                                    $aParentObject = Mage::getModel('sales/order_item')->getCollection()->addFieldToFilter('parent_item_id' , $itemid)
                                         ->addFieldToSelect('item_id');


                                    if(count($aParentObject) > 0){
                                        foreach($aParentObject as $parentvalue){

                                            $aOrderItemsIds = array($itemid , $parentvalue->getItemId());
                                        }
                                    }

                                }else{
                                            $aOrderItemsIds = array($itemid);
                                }

                                if($type == 'ship' ){
                                                       
                                                 Mage::register('order_item_ids', $aOrderItemsIds);
                                }else{


                                    foreach($aOrderItemsIds as $mainItemIds){

                                        $OrderItemsStateStatus = json_decode(Mage::helper('orderitemsstatestatus')->getRelatedItemStatusandState('status' , $itemstatus) , true);

                                        $OrderItemsData = array( 'item_state' => $OrderItemsStateStatus['state'] , 'item_status' => $itemstatus );

                                        $ModelOrderStateChange = Mage::getModel('sales/order_item')->load($mainItemIds)->addData($OrderItemsData);
                                            $ModelOrderStateChange->setId($mainItemIds)->save();
                                            Mage::helper('orderitemsstatestatus')->changeOrderStatusState($OrdersId);
                                    }
                                }
                            }
                        }

        }

    }

    /*Start of Mass action for invoice & shipment generation from order view*/
    public function massInvoiceAction()
    {

         $itemIds = $this->getRequest()->getParam('order_ids');

            if(count($itemIds) > 0){

                foreach($itemIds as $Orderitemsids){

                    $qtys = array();

                   $OrderId = Mage::getModel('sales/order_item')->load($Orderitemsids)->getOrderId();
                   $orderobj = Mage::getModel('sales/order')->load($OrderId);
                   $orderItemsObj = $orderobj->getAllItems();
                   $ItemsStatus = Mage::getModel('sales/order_item')->load($Orderitemsids)->getItemStatus();

                   $invoicenotallowedItemStatus = array('store_allocated' , 'store_rejected');

                   if(in_array( $ItemsStatus , $invoicenotallowedItemStatus)){

                        Mage::getSingleton('core/session')->addError('Invoice cannot be created');
                        continue;
                   }

                   foreach($orderItemsObj as $item){

                        if($item->getId() == $Orderitemsids){
                            if( is_null($item->getParentItemId())){
                                $itemId = $item->getId();

                            }else{
                                    $itemId = $item->getParentItemId();

                            }
                            $qty_to_invoice = intval($item->getQtyOrdered());
                            $qtys[$itemId] = $qty_to_invoice;

                            Mage::register('invoice_sub-order_id', $itemId);
                        }

                   }

                            $savedQtys = $this->_getItemQtysForInvoice($qtys , $OrderId);


                            $this->setInvoicedShipmentStatusItems($savedQtys , 'invoice'); // Call the function for the Order Item Status to be Invoiced Status ..........


                        if($orderobj->canInvoice()) {
                                $invoiceId = Mage::getModel('sales/order_invoice_api')
                                                    ->create($orderobj->getIncrementId(), $savedQtys ,'Grid page Order Items invoice create' , 1 , 1);

                          Mage::getSingleton('core/session')->addSuccess($this->__('The Invoice(s) has been created.')); // Message Invoice Created.

                        }
                        Mage::unregister('invoice_sub-order_id'); // Unset Sub Order id register of previous sub order id.



                }


            }else{
                $this->_getSession()->addError($this->__('No order id specified.'));
            }

            $this->_redirect('*/*/index');

            return;

    }


    function getOrderItemId($order_items, $sku) {
        foreach ($order_items as $order_item) {
            if ($order_item->getSku() == $sku) {
                return $order_item->getItemId();
            }
        }
        return 0;
    }

    /*function createShipment($orderItemsObj){

        $orderId = $orderItemsObj->getOrderId();

        try
        {
            // Load Order
            $order = Mage::getModel('sales/order')->load($orderId);


            // Get Order Items
            $order_items = $order->getItemsCollection();

            // Parepare Item Qtys For Shipment
            $item_qtys = array();

            $allocations = array(
                array('sku' => $orderItemsObj->getSku(), 'allocated_qty' => $orderItemsObj->getQtyOrdered())
            );

            foreach ($allocations as $allocation) {
                $item_qtys[$this->getOrderItemId($order_items, $allocation['sku'])] = $allocation['allocated_qty'];
            }

            //prepareShipment
            $shipment = Mage::getModel('sales/service_order', $order)->prepareShipment($savedQtys);
            // Create Shipment
            $shipment = $order->prepareShipment($item_qtys);
            $shipment->register();
            $shipment->sendEmail(false)
                     ->setEmailSent(false)
                     ->save();
            $transactionSave = Mage::getModel('core/resource_transaction')
                               ->addObject($shipment)
                               ->addObject($shipment->getOrder())
                               ->save();

            // Update Magento Order State/Status to Processing/Sent To Picking
            $order->save();
            // Success

            //return $shipment->getIncrementId();
        }
        catch (Exception $e)
        {
            // Log Error On Order Comment History

            $this->_getSession()->addError($this->__('Shipment not created, order ID - '.$orderId));

        }

    }*/
    function createShipment($order , $qtysForProducts){

        $shipment = $order->prepareShipment($qtysForProducts);
        $this->setInvoicedShipmentStatusItems($savedQtys , 'ship'); // Call the function for the Order Item Status to be Shipped Status ..........
        if ($shipment) {
            $shipment->register();

            $shipment->sendEmail(true)
            ->setEmailSent(true)
            ->save();

            $order->setIsInProcess(true);

            $transactionSave = Mage::getModel('core/resource_transaction')
                ->addObject($shipment)
                ->addObject($shipment->getOrder())
                ->save();
        }

        return $shipment;

    }


    public function massShipAction()
    {

            $itemIds = $this->getRequest()->getParam('order_ids');
            $aOrderItems = array();

            if(count($itemIds) > 0){

                foreach($itemIds as $Orderitemsids){

                    $qtys = array();

                   $OrderId = Mage::getModel('sales/order_item')->load($Orderitemsids)->getOrderId();
                   $orderobj = Mage::getModel('sales/order')->load($OrderId);
                   $orderItemsObj = $orderobj->getAllItems();
                   $ItemsStatus = Mage::getModel('sales/order_item')->load($Orderitemsids)->getItemStatus();

                   $shipmentnotallowedItemStatus = array('store_allocated' , 'store_rejected');

                   if(in_array( $ItemsStatus , $shipmentnotallowedItemStatus)){

                        Mage::getSingleton('core/session')->addError('Shipment cannot be created');
                        continue;
                   }

                   foreach($orderItemsObj as $item){

                        if($item->getId() == $Orderitemsids){
                            if( is_null($item->getParentItemId())){
                                $itemId = $item->getId();

                            }else{
                                    $itemId = $item->getParentItemId();

                            }
                            $qty_to_ship = intval($item->getQtyOrdered());
                            $qtys[$itemId] = $qty_to_ship;
                        }

                   }

                   $savedQtys = $this->_getItemQtysForShipment($qtys , $OrderId);

                   $shipments = $this->createShipment($orderobj , $savedQtys);
                   Mage::getSingleton('core/session')->addSuccess($this->__('The shipment(s) has been created.'));
               }
            }
            else{
                $this->_getSession()->addError($this->__('No order id specified.'));
            }


            $this->_redirect('*/*/index');

        return;

    }
    /*End of Mass action for invoice & shipment generation from order view*/


    public function addCommentAction()
    {
        if ($order = $this->_initOrder()) {
            try {
                $response = false;
                $data = $this->getRequest()->getPost('history');
                $sub_order_id = $this->getRequest()->getParam('sub_order_id');
                //Mage::log(print_r($data , 1) , null , 'PostHistory.log');
                $notify = isset($data['is_customer_notified']) ? $data['is_customer_notified'] : false;
                $visible = isset($data['is_visible_on_front']) ? $data['is_visible_on_front'] : false;
                //Mage::log( get_class($order) , null , 'PostHistory.log');
                /*$order->addStatusHistoryComment($data['comment'], $data['status'])
                    ->setIsVisibleOnFront($visible)
                    ->setIsCustomerNotified($notify);*/

                    $order_items = Mage::getModel('sales/order_item')->load($sub_order_id)->setStatus($data['status']);
                    $order_items->save();

                    /*$order->addStatusHistoryComment($data['comment'], $data['status'])
                    ->setIsVisibleOnFront($visible)
                    ->setIsCustomerNotified($notify);*/


              /*  $comment = trim(strip_tags($data['comment']));

                $order->save();
                $order->sendOrderUpdateEmail($notify, $comment);*/

                $this->loadLayout('empty');
                $this->renderLayout();
            }
            catch (Mage_Core_Exception $e) {
                $response = array(
                    'error'     => true,
                    'message'   => $e->getMessage(),
                );
            }
            catch (Exception $e) {
                $response = array(
                    'error'     => true,
                    'message'   => $this->__('Cannot add order history.')
                );
            }
            if (is_array($response)) {
                $response = Mage::helper('core')->jsonEncode($response);
                $this->getResponse()->setBody($response);
            }
        }
    }

    // This is called in initShipment
    protected function _getItemQtysForShipment($data = NULL , $orderid = NULL)
    {

        /*if (isset($data['items'])) {
            $qtys = $data['items'];
        } else {
            $qtys = array();
        }
        return $qtys;*/

        if(is_null($data)){
            //echo 'Null found';
             $data = $this->getRequest()->getParam('shipment');

            //print_r($data);
            if (isset($data['items'])) {
                    $qtys = $data['items'];
            } else {
                    $qtys = array();
            }
            $orderid = $this->getRequest()->getParam('order_id');
        }else{
           // echo 'Null Not found';
            $qtys = $data ;
        }

        $orderObj = Mage::getModel('sales/order')->load($orderid);

            foreach($orderObj->getAllItems() as $_item){

                if(!is_null($_item->getParentItemId())){


                    if(!array_key_exists($_item->getParentItemId(), $qtys)){
                            //echo $_item->getParentItemId();

                        $qtys [$_item->getParentItemId()] = 0;

                    }

                }else{
                    if(!array_key_exists($_item->getId(), $qtys)){


                        $qtys [$_item->getId()] = 0;

                    }

                }

            }


        return $qtys;
    }

    protected function _getItemQtysForInvoice($data = NULL , $orderid = NULL)
    {
        //echo '<pre>';
        if(is_null($data)){
            //echo 'Null found';
            $data = $this->getRequest()->getParam('invoice');

            //print_r($data);
            if (isset($data['items'])) {
                    $qtys = $data['items'];
            } else {
                    $qtys = array();
            }
            $orderid = $this->getRequest()->getParam('order_id');
        }else{
           // echo 'Null Not found';
            $qtys = $data ;
        }

        $orderObj = Mage::getModel('sales/order')->load($orderid);

            foreach($orderObj->getAllItems() as $_item){
                if(!is_null($_item->getParentItemId())){

                    if(!array_key_exists($_item->getParentItemId(), $qtys)){

                        $qtys [$_item->getParentItemId()] = 0;

                    }

                }else{
                    if(!array_key_exists($_item->getId(), $qtys)){

                        $qtys [$_item->getId()] = 0;

                    }

                }

            }

        return $qtys;
    }


    // This is called in new shipment.
     protected function _initShipment()
    {
        $this->_title($this->__('Sales'))->_title($this->__('Shipments'));

        $shipment = false;

        $orderId = $this->getRequest()->getParam('order_id');
        if ($shipmentId) {
            $shipment = Mage::getModel('sales/order_shipment')->load($shipmentId);
        } elseif ($orderId) {
            $order      = Mage::getModel('sales/order')->load($orderId);

            /**
             * Check order existing
             */
            if (!$order->getId()) {
                $this->_getSession()->addError($this->__('The order no longer exists.'));
                return false;
            }
            /**
             * Check shipment is available to create separate from invoice
             */
            if ($order->getForcedDoShipmentWithInvoice()) {
                $this->_getSession()->addError($this->__('Cannot do shipment for the order separately from invoice.'));
                return false;
            }
            /**
             * Check shipment create availability
             */
            if (!$order->canShip()) {
                $this->_getSession()->addError($this->__('Cannot do shipment for the order.'));
                return false;
            }
            $savedQtys = $this->_getItemQtysForShipment();

            /*echo '<pre>';
            print_r($savedQtys);
            exit;*/
            $shipment = Mage::getModel('sales/service_order', $order)->prepareShipment($savedQtys);

            $tracks = $this->getRequest()->getPost('tracking');
            /*echo '<pre>';
            print_r($tracks);
            exit;*/
             //echo get_class(Mage::getModel('sales/order_shipment_track'));
                    //exit;
            if ($tracks) {

                foreach ($tracks as $keys => $data) {
                    if($keys != '__index__'){
                        if (empty($data['number'])) {
                            Mage::throwException($this->__('Tracking number cannot be empty.'));
                        }


                        $track = Mage::getModel('sales/order_shipment_track')
                            ->addData($data);
                        $shipment->addTrack($track);
                    }
                }
            }
        }

        Mage::register('current_shipment', $shipment);

        /*echo '<pre>';
        print_r($shipment->getData());
        exit;*/
        return $shipment;
    }

    // Generate new shipment from order items page
    public function newshipmentAction()
    {

        if ($shipment = $this->_initShipment()) {
            $this->_title($this->__('New Shipment'));

            $comment = Mage::getSingleton('adminhtml/session')->getCommentText(true);
            if ($comment) {
                $shipment->setCommentText($comment);
            }

            $this->loadLayout()
                ->_setActiveMenu('`')
                ->renderLayout();
        } else {
            $this->_redirect('*/sales_order/view', array('order_id'=>$this->getRequest()->getParam('order_id')));
        }
    }


    /**
     * Save shipment
     * We can save only new shipment. Existing shipments are not editable
     *
     * @return null
     */
    public function saveshippingAction()
    {
        $data = $this->getRequest()->getPost('shipment');


        if (!empty($data['comment_text'])) {
            Mage::getSingleton('adminhtml/session')->setCommentText($data['comment_text']);
        }

        try {
            $shipment = $this->_initShipment();
            if (!$shipment) {
                $this->_forward('noRoute');
                return;
            }

            $shipment->register();
            $comment = '';
            if (!empty($data['comment_text'])) {
                $shipment->addComment(
                    $data['comment_text'],
                    isset($data['comment_customer_notify']),
                    isset($data['is_visible_on_front'])
                );
                if (isset($data['comment_customer_notify'])) {
                    $comment = $data['comment_text'];
                }
            }

            if (!empty($data['send_email'])) {
                $shipment->setEmailSent(true);
            }

            $shipment->getOrder()->setCustomerNoteNotify(!empty($data['send_email']));
            $responseAjax = new Varien_Object();
            $isNeedCreateLabel = isset($data['create_shipping_label']) && $data['create_shipping_label'];

            if ($isNeedCreateLabel && $this->_createShippingLabel($shipment)) {
                $responseAjax->setOk(true);
            }

            $this->setInvoicedShipmentStatusItems($data['items'] , 'ship');
            $this->_saveShipment($shipment);

            $shipment->sendEmail(!empty($data['send_email']), $comment);

            $shipmentCreatedMessage = $this->__('The shipment has been created.');
            $labelCreatedMessage    = $this->__('The shipping label has been created.');

            $this->_getSession()->addSuccess($isNeedCreateLabel ? $shipmentCreatedMessage . ' ' . $labelCreatedMessage
                : $shipmentCreatedMessage);
            Mage::getSingleton('adminhtml/session')->getCommentText(true);
        } catch (Mage_Core_Exception $e) {
            if ($isNeedCreateLabel) {
                $responseAjax->setError(true);
                $responseAjax->setMessage($e->getMessage());
            } else {
                $this->_getSession()->addError($e->getMessage());
                $this->_redirect('*/*/new', array('order_id' => $this->getRequest()->getParam('order_id')));
            }
        } catch (Exception $e) {
            Mage::logException($e);
            if ($isNeedCreateLabel) {
                $responseAjax->setError(true);
                $responseAjax->setMessage(
                    Mage::helper('sales')->__('An error occurred while creating shipping label.'));
            } else {
                $this->_getSession()->addError($this->__('Cannot save shipment.'));
                $this->_redirect('*/*/new', array('order_id' => $this->getRequest()->getParam('order_id')));
            }

        }
        if ($isNeedCreateLabel) {
            $this->getResponse()->setBody($responseAjax->toJson());
        } else {
            $this->_redirect('*/order_items/view', array('order_id' => $shipment->getOrderId() , 'sub_order_id' => $this->getRequest()->getParam('sub_order_id')));
        }
    }

    public function updateQtyinvoiceAction()
    {
        try {
            $invoice = $this->_initInvoice(true);
            // Save invoice comment text in current invoice object in order to display it in corresponding view
            $invoiceRawData = $this->getRequest()->getParam('invoice');
            /*echo '<pre>';
            print_r($invoiceRawData);*/
            $aSuborderIds = array_keys($invoiceRawData['items']);
            $iSuborderIds = $aSuborderIds[0];
            //exit;

            $invoiceRawCommentText = $invoiceRawData['comment_text'];
            $invoice->setCommentText($invoiceRawCommentText);

            $this->loadLayout();
            $response = $this->getLayout()->getBlock('order_items_indiviual')->
            setSuborderid($iSuborderIds)->toHtml();
        } catch (Mage_Core_Exception $e) {
            $response = array(
                'error'     => true,
                'message'   => $e->getMessage()
            );
            $response = Mage::helper('core')->jsonEncode($response);
        } catch (Exception $e) {
            $response = array(
                'error'     => true,
                'message'   => $this->__('Cannot update item quantity.')
            );
            $response = Mage::helper('core')->jsonEncode($response);
        }
        $this->getResponse()->setBody($response);
    }


    public function saveinvoiceAction()
    {



        $data = $this->getRequest()->getPost('invoice');
        $orderId = $this->getRequest()->getParam('order_id');

        /*echo '<pre>';
        print_r($data['items']);
        exit;*/

        $this->setInvoicedShipmentStatusItems($data['items'] , 'invoice'); // Call the function for the Order Item Status to be Invoiced Status ..........



        if (!empty($data['comment_text'])) {
            Mage::getSingleton('adminhtml/session')->setCommentText($data['comment_text']);
        }

        try {
            $invoice = $this->_initInvoice();
            if ($invoice) {

                if (!empty($data['capture_case'])) {
                    $invoice->setRequestedCaptureCase($data['capture_case']);
                }

                if (!empty($data['comment_text'])) {
                    $invoice->addComment(
                        $data['comment_text'],
                        isset($data['comment_customer_notify']),
                        isset($data['is_visible_on_front'])
                    );
                }

                $invoice->register();

                if (!empty($data['send_email'])) {
                    $invoice->setEmailSent(true);
                }

                $invoice->getOrder()->setCustomerNoteNotify(!empty($data['send_email']));
                $invoice->getOrder()->setIsInProcess(true);

                $transactionSave = Mage::getModel('core/resource_transaction')
                    ->addObject($invoice)
                    ->addObject($invoice->getOrder());
                $shipment = false;
                if (!empty($data['do_shipment']) || (int) $invoice->getOrder()->getForcedDoShipmentWithInvoice()) {
                    $shipment = $this->_prepareShipment($invoice);
                    if ($shipment) {
                        $shipment->setEmailSent($invoice->getEmailSent());
                        $transactionSave->addObject($shipment);
                    }
                }
                $transactionSave->save();

                if (isset($shippingResponse) && $shippingResponse->hasErrors()) {
                    $this->_getSession()->addError($this->__('The invoice and the shipment  have been created. The shipping label cannot be created at the moment.'));
                } elseif (!empty($data['do_shipment'])) {
                    $this->_getSession()->addSuccess($this->__('The invoice and shipment have been created.'));
                } else {
                    $this->_getSession()->addSuccess($this->__('The invoice has been created.'));
                }

                // send invoice/shipment emails
                $comment = '';
                if (isset($data['comment_customer_notify'])) {
                    $comment = $data['comment_text'];
                }
                try {
                    $invoice->sendEmail(!empty($data['send_email']), $comment);
                } catch (Exception $e) {
                    Mage::logException($e);
                    $this->_getSession()->addError($this->__('Unable to send the invoice email.'));
                }
                if ($shipment) {
                    try {
                        $shipment->sendEmail(!empty($data['send_email']));
                    } catch (Exception $e) {
                        Mage::logException($e);
                        $this->_getSession()->addError($this->__('Unable to send the shipment email.'));
                    }
                }
                Mage::getSingleton('adminhtml/session')->getCommentText(true);
                //$this->_redirect('*/sales_order/view', array('order_id' => $orderId));
                $this->_redirect('*/order_items/view', array('order_id' => $orderId , 'sub_order_id' => $this->getRequest()->getParam('sub_order_id')));
            } else {
                //$this->_redirect('*/*/new', array('order_id' => $orderId));
                $this->_redirect('*/order_items/view', array('order_id' => $orderId , 'sub_order_id' => $this->getRequest()->getParam('sub_order_id')));
            }
            return;
        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            $this->_getSession()->addError($this->__('Unable to save the invoice.'));
            Mage::logException($e);
        }
        //$this->_redirect('*/*/new', array('order_id' => $orderId));
         $this->_redirect('*/order_items/view', array('order_id' => $orderId , 'sub_order_id' => $this->getRequest()->getParam('sub_order_id')));
    }



    protected function _saveShipment($shipment)
    {

        $shipment->getOrder()->setIsInProcess(true);
        $transactionSave = Mage::getModel('core/resource_transaction')
            ->addObject($shipment)
            ->addObject($shipment->getOrder())
            ->save();

        return $this;
    }

    public function newinvoiceAction()
    {
        $invoice = $this->_initInvoice();
        if ($invoice) {
            $this->_title($this->__('New Invoice'));

            if ($comment = Mage::getSingleton('adminhtml/session')->getCommentText(true)) {
                $invoice->setCommentText($comment);
            }

            $this->loadLayout()
                ->_setActiveMenu('salesorderitemgrid/orderitems')
                ->renderLayout();
        } else {
            $this->_redirect('*/sales_order/view', array('order_id'=>$this->getRequest()->getParam('order_id')));
        }
    }

    protected function _initInvoice($update = false)
    {
        $this->_title($this->__('Sales'))->_title($this->__('Invoices'));

        $invoice = false;
        $itemsToInvoice = 0;
        $invoiceId = $this->getRequest()->getParam('invoice_id');
        $orderId = $this->getRequest()->getParam('order_id');
        $item_id = Mage::app()->getRequest()->getParam('sub_order_id');
        Mage::register('invoice_sub-order_id', $item_id);

        if ($invoiceId) {
            $invoice = Mage::getModel('sales/order_invoice')->load($invoiceId);
            if (!$invoice->getId()) {
                $this->_getSession()->addError($this->__('The invoice no longer exists.'));
                return false;
            }
        } elseif ($orderId) {
            $order = Mage::getModel('sales/order')->load($orderId);
            /**
             * Check order existing
             */
            if (!$order->getId()) {
                $this->_getSession()->addError($this->__('The order no longer exists.'));
                return false;
            }
            /**
             * Check invoice create availability
             */
            if (!$order->canInvoice()) {
                $this->_getSession()->addError($this->__('The order does not allow creating an invoice.'));
                return false;
            }

            $savedQtys = $this->_getItemQtysForInvoice();

            /******** Changes in the Shipping Charges and COD for the Items ***************/

            $countitems = 0;
            $InvoiceDone = false;

            foreach($order->getAllItems() as $items){
                    $type = $items->getProductType();
                    $qtyInvoiced = round($items->getQtyInvoiced());
                    $qtyOrdered = round($items->getQtyOrdered());
                    if(strtolower($type) == 'simple'){
                        $countitems++;

                    } // Counts increment for the Simple Type.



            }

            $updatedInvoiceOrder = $order;  // Save order object in the custom variable.



            /********************************************************************/

            $invoice = Mage::getModel('sales/service_order', $updatedInvoiceOrder)->prepareInvoice($savedQtys);


        }

        Mage::register('current_invoice', $invoice);
        return $invoice;
    }

    public function updateInvoiceIdAction(){
        if( $this->getRequest()->getParam('orderItemsId') != '' && $this->getRequest()->getParam('updatedInvoiceId') != '' ){
            $poData = Mage::getModel('omniapis/storepo')->getCollection()->addFieldToFilter('sub_orderid',array('eq'=>$this->getRequest()->getParam('orderItemsId')))->getData();
            
            foreach ($poData as $pod) {
                Mage::getModel('omniapis/storepo')->load($pod['id'])->addData(array('invoice_id'=>$this->getRequest()->getParam('updatedInvoiceId')))->save();
            }
            $response = array('message'=>'Success');
            $response = Mage::helper('core')->jsonEncode($response);
            $this->getResponse()->setBody($response);
        }else{
            $response = array('message'=>'Failure');
            $response = Mage::helper('core')->jsonEncode($response);
            $this->getResponse()->setBody($response);
        }
    }

    public function addTheInvoiceIdAction(){
        if( $this->getRequest()->getParam('orderItemsId') != '' && $this->getRequest()->getParam('updatedInvoiceId') != '' ){
            $poData = Mage::getModel('omniapis/storepo')->getCollection()->addFieldToFilter('sub_orderid',array('eq'=>$this->getRequest()->getParam('orderItemsId')))->getData();
            
            foreach ($poData as $pod) {
                Mage::getModel('omniapis/storepo')->load($pod['id'])->addData(array('invoice_id'=>$this->getRequest()->getParam('updatedInvoiceId')))->save();
            }

            // Mage::getModel('omniapis/storepo')->load($this->getRequest()->getParam('orderItemsId'))->setInvoiceId($this->getRequest()->getParam('updatedInvoiceId'))->save();
            $response = array('message'=>'Success');
            $response = Mage::helper('core')->jsonEncode($response);
            $this->getResponse()->setBody($response);
        }else{
            $response = array('message'=>'Failure');
            $response = Mage::helper('core')->jsonEncode($response);
            $this->getResponse()->setBody($response);
        }
    }

    public function sendOtpBeforeCancelAction(){
        try{
            $otp = rand(0000,9999);
            Mage::getSingleton('core/session')->setOtp($otp);
            $params = $this->getRequest()->getParams();
            $orderModel = Mage::getModel('sales/order')->load($params['order']);
            // $orderItemModel = Mage::getModel('sales/order_item')->load($params['order_item_id']);
            
            $bill = $orderModel->getBillingAddress();
            $name = $bill->getFirstname();
            $phone = $bill->getTelephone();
            $storeId = $orderModel->getStoreId();
            $msg = "Dear ".$name.", Your Order with ID #".$orderModel->getIncrementId()." is about to be canceled. Please share OTP ".$otp." with customer care agent";
            Mage::helper('smsapp')->sensSms($msg, $phone, $storeId);
            echo json_encode(array('response'=>'true','order'=>$params['order'],'order_item_id'=>$params['order_item_id'],'otp'=>$otp));
        }catch(Exception $e){
            echo json_encode(array('response'=>'false'));
        }
    }

    public function pickupstatuscompletedAction(){  //Pickable product is always in qty of 1
        try{
            $params = $this->getRequest()->getParams();
            
            $orderItemModel = Mage::getModel('sales/order_item')->load($params['order_item_id']);
            $orderItemModel->setPickupStatus('completed');
            $orderItemModel->save();
            $orderItemModelForItem = Mage::getModel('sales/order_item')->getCollection()->addFieldToFilter('parent_item_id',$params['order_item_id'])->getData();
            foreach ($orderItemModelForItem as $simpleItem) {
                $orderItemModelForItem = Mage::getModel('sales/order_item')->load($simpleItem['item_id']);
                $orderItemModelForItem->setPickupStatus('completed');
                $orderItemModelForItem->save();
            }
            
            $OrderItemsStateStatus = json_decode(Mage::helper('orderitemsstatestatus')
                ->getRelatedItemStatusandState('status' , 'complete') , true);

            $changeitemstatusstate = array('item_state' => $OrderItemsStateStatus['state'] , 'item_status' => 'complete', 'qty_shipped' => 1 );

                try{
                    $orderItemModelForItem->addData($changeitemstatusstate);
                    $orderItemModelForItem->setId($simpleItem['item_id'])->save();
                    $orderItemModel->addData($changeitemstatusstate);
                    $orderItemModel->setId($params['order_item_id'])->save();

                    Mage::helper('orderitemsstatestatus')->changeOrderStatusState($params['order']);
                    Mage::getSingleton('adminhtml/session')->addSuccess('Order Item Pickup completed.');
                    
                }catch(Exception $e){                    
                    Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                }   

            echo json_encode(array('response'=>'true','order'=>$params['order'],'order_item_id'=>$params['order_item_id']));
        }catch(Exception $e){
            echo json_encode(array('response'=>'false'));
        }
    }

    public function verifyOtpandCancelOrderItemAction(){

            $params = $this->getRequest()->getParams();

            if(Mage::getSingleton('core/session')->getOtp() == $params['otp']){
                Mage::getSingleton('core/session')->unsOtp();

                // echo $params['order_item_id'];
                $orderModel = Mage::getModel('sales/order')->load($params['order']);
                $orderItemModel = Mage::getModel('sales/order_item')->load($params['order_item_id']);                
                $product_type = $orderItemModel->getProductType();

                $aOrderItemsIds = array($params['order_item_id']);

                if(strtolower($product_type) == 'configurable'){                    
                    $getChildItemId = Mage::getModel('sales/order_item')->load($params['order_item_id'] , 'parent_item_id')->getItemId();
                    $aOrderItemsIds []= $getChildItemId;
                }


                $OrderItemsStateStatus = json_decode(Mage::helper('orderitemsstatestatus')
                    ->getRelatedItemStatusandState('status' , 'canceled') , true);



                $changeitemstatusstate = array('item_state' => $OrderItemsStateStatus['state'] , 'item_status' => 'canceled' );


                foreach($aOrderItemsIds as $itemsId){
                    $ModelSaveItemsStatusState = Mage::getModel('sales/order_item')->load($itemsId)->addData($changeitemstatusstate);
                    try{
                        $ModelSaveItemsStatusState->setId($itemsId)->save();

                        Mage::helper('orderitemsstatestatus')->changeOrderStatusState($params['order']);
                        Mage::getSingleton('adminhtml/session')->addSuccess('Order Item canceled.');
                        
                    }catch(Exception $e){                    
                        Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                    }   
                }            
                $this->_redirect('*/order_items/index');
            }else{
                Mage::getSingleton('core/session')->unsOtp();
                Mage::getSingleton('adminhtml/session')->addError('OTP entered mismatched');
                $this->_redirect('*/order_items/index');
            }
    }
}
?>
