<?php
class Iksula_Dashboardsales_Block_Adminhtml_Dashboard_Sales extends Mage_Adminhtml_Block_Dashboard_Sales
{

	protected function _prepareLayout()
    {
    	if (!Mage::helper('core')->isModuleEnabled('Mage_Reports')) {
            return $this;
        }
        $isFilter = $this->getRequest()->getParam('store') || $this->getRequest()->getParam('website') || $this->getRequest()->getParam('group');

        $collection = Mage::getResourceModel('reports/order_collection')
            ->calculateSales($isFilter);

        $refundCollection = Mage::getResourceModel('reports/order_collection')
            ->calculateRefund($isFilter);



        if ($this->getRequest()->getParam('store')) {
            $collection->addFieldToFilter('store_id', $this->getRequest()->getParam('store'));
        } else if ($this->getRequest()->getParam('website')){
            $storeIds = Mage::app()->getWebsite($this->getRequest()->getParam('website'))->getStoreIds();
            $collection->addFieldToFilter('store_id', array('in' => $storeIds));
        } else if ($this->getRequest()->getParam('group')){
            $storeIds = Mage::app()->getGroup($this->getRequest()->getParam('group'))->getStoreIds();
            $collection->addFieldToFilter('store_id', array('in' => $storeIds));
        }

        $collection->load();
        // $refundCollection->load();
        $sales = $collection->getFirstItem();
        // $refunds = $refundCollection->getFirstItem();



        // echo "<pre>";
        // var_dump($refundCollection->getData());
        // exit;
        $this->addTotal($this->__('Lifetime Sales'), $sales->getLifetime());
        $this->addTotal($this->__('Average Orders'), $sales->getAverage());
        $this->addTotal($this->__('Order Count'), $sales->getTotalOrderCount(),true);
        $this->addTotal($this->__('Returns Amount'), $refundCollection['total_return_price']);
        $this->addTotal($this->__('Returns Count'), $refundCollection['return_count'],true);
    }
}
			