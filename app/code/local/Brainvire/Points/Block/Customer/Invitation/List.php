<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   Brainvire
 * @package    Brainvire_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * @license    
 */


class Brainvire_Points_Block_Customer_Invitation_List extends Mage_Core_Block_Template {

    
    protected function _prepareLayout() {
        return parent::_prepareLayout();
    }

    
    public function getCustomer() {
        return Mage::getSingleton('customer/session')->getCustomer();
    }

    
    public function getCollection() {
        if (!$this->getData('collection')) {
            $this->setCollection(
                    Mage::getModel('points/invitation')->getCollection()->addCustomerFilter($this->getCustomer())
            );
            $this->updateNumeralStatuses();
        }
        return $this->getData('collection');
    }

    
    public function getToolbarHtml() {
        return $this->getChildHtml('toolbar');
    }

    
    public function hasInvitees() {
        return $this->getCollection()->count() > 0;
    }

    
    public function getBackUrl() {
        if ($this->getRefererUrl()) {
            return $this->getRefererUrl();
        }
        return $this->getUrl('customer/account/');
    }

    
    public function getInvitationFormUrl() {
        return $this->getUrl('points/invitation/sendinvitation/');
    }

   
    private function updateNumeralStatuses() {

        $data = $this->getData('collection');

        foreach ($data as $item) {

            switch ($item->getStatus()) {
                case Brainvire_Points_Model_Invitation::INVITATION_NEW:
                    $item->setStatus($this->__('Email wasn\'t sent. Please try later. '));
                    break;
                case Brainvire_Points_Model_Invitation::INVITATION_SENT:
                    $item->setStatus($this->__('Invitation sent'));
                    break;
                case Brainvire_Points_Model_Invitation::INVITATION_ACCEPTED:
                    $item->setStatus($this->__('Invitation accepted'));
                    break;
                case Brainvire_Points_Model_Invitation::INVITEE_IS_CUSTOMER:
                    $item->setStatus($this->__('Invitee became a customer'));
                    break;
                default:
                    break;
            }
        }
    }

    protected function _toHtml() {

        $magentoVersionTag = Brainvire_Points_Helper_Data::MAGENTO_VERSION_14 . "/";

        if (Mage::helper('points')->magentoLess14())
            $magentoVersionTag = Brainvire_Points_Helper_Data::MAGENTO_VERSION_13;

        $this->setTemplate("brainvire_points/customer/" . $magentoVersionTag . "/invitation/list.phtml");

        $html = parent::_toHtml();
        return $html;
    }

}

?>
