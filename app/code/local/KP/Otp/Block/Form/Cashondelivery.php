<?php 

class  KP_Otp_Block_Form_Cashondelivery extends Mage_Payment_Block_Form_Cashondelivery{

  protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('otp/form/cashondelivery.phtml');
    }

  public function getOtpIsEnable(){
    return Mage::helper('otp/data')->getIsModuleEnable();
  }

  public function getCustomerMobileNumber(){
    return Mage::getSingleton('checkout/session')->getQuote()
                                      ->getBillingAddress()
                                      ->getData('telephone');
  }

}