<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_Model_Resource_Product_Collection extends Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection
{
    public function addFieldToFilter($field, $condition = null)
    {
        switch ($field) {
            case 'email_templates':
                if (isset($condition['eq'])) {
                    $this->_select->where(new Zend_Db_Expr(
                        "FIND_IN_SET('{$condition['eq']}', giftcard_templates.templates)"
                        ));
                }
                break;
            case 'purchased_amount':
            case 'used_amount':
            case 'purchased_qty':
            case 'used_qty':
                $resultCondition = $this->_translateCondition($field, $condition);
                $this->_select->where($resultCondition);
                break;
            default:
                parent::addFieldToFilter($field , $condition);
        }

        return $this;
    }

    /**
     * Get SQL for get record count
     */
    public function getSelectCountSql()
    {
        $this->_renderFilters();

        $select = clone $this->getSelect();

        $countSelect = clone $this->getSelect();
        $countSelect->reset();
        $countSelect->from(
            array(
                'ps' =>  $select
            ),
            'COUNT(*)'
        );

        return $countSelect;
    }

    /**
     * Retrive all ids for collection
     *
     * @param $limit
     * @param $offset
     * @return array
     */
    public function getAllIds($limit = null, $offset = null)
    {
        $select = clone $this->getSelect();

        $idsSelect = clone $this->getSelect();
        $idsSelect->reset();
        $idsSelect->from(
            array(
                'ps' =>  $select
            ),
            'ps.' . $this->getEntity()->getIdFieldName()
        );
        $idsSelect->limit($limit, $offset);

        return $this->getConnection()->fetchCol($idsSelect, $this->_bindParams);
    }

    public function addGiftcardData($store)
    {
        $this
            ->attachStatistics($store)
            ->attachTemplatesData($store)
        ;

        return $this;
    }

    public function attachStatistics($store)
    {
        if ($store->getId()) {
            $currencyRate = new Zend_Db_Expr("1");
            $storeFilter = "(stats.store_id = 0 OR stats.store_id = {$store->getId()})";
        } else {
            $currencyRate = "o.store_to_base_rate";
            $storeFilter = "(1=1)";
        }

        $this->getSelect()
            ->joinLeft(
                array('gs' => new Zend_Db_Expr(
                    "(SELECT stats.product_id, COALESCE(SUM(stats.purchased_qty), 0) AS `purchased_qty`,
                       COALESCE(SUM(stats.purchased_amount * {$currencyRate}), 0) AS `purchased_amount`,
                       COALESCE(SUM(stats.used_qty), 0) AS `used_qty`,
                       COALESCE(SUM(stats.used_amount  * {$currencyRate}), 0) AS `used_amount`
                    FROM {$this->getTable('aw_giftcard2/statistics')} AS stats
                    LEFT JOIN {$this->getTable('sales/order')} AS o ON o.entity_id = stats.order_id
                    WHERE {$storeFilter} GROUP BY stats.product_id)"
                )),
                "e.entity_id = gs.product_id",
                array(
                    'purchased_qty' => "gs.purchased_qty",
                    'purchased_amount' => "gs.purchased_amount",
                    'used_qty' => "gs.used_qty",
                    'used_amount' => "gs.used_amount"
                )
            )
        ;

        return $this;
    }

    public function attachTemplatesData($store)
    {
        if($store->getId()) {
            $storeFilter = "(store_id = 0 OR store_id = {$store->getId()})";
        } else {
            $storeFilter = "(1=1)";
        }

        $this->getSelect()
            ->joinLeft(
                array('giftcard_templates' => new Zend_Db_Expr(
                    "(SELECT entity_id, GROUP_CONCAT(DISTINCT value SEPARATOR ',') as templates
                        FROM {$this->getTable('aw_giftcard2/product_entity_templates')} WHERE $storeFilter GROUP BY entity_id)"
                )),
                "e.entity_id = giftcard_templates.entity_id",
                array(
                    'email_templates' => 'giftcard_templates.templates'
                )
            )
        ;

        return $this;
    }

    protected function _renderOrders()
    {
        if (!$this->_isOrdersRendered) {
            foreach ($this->_orders as $field => $direction) {
                switch ($field) {
                    case 'email_templates':
                    case 'purchased_amount':
                    case 'used_amount':
                    case 'purchased_qty':
                    case 'used_qty':
                        $this->_select->order(new Zend_Db_Expr($field . ' ' . $direction));
                        break;
                    default:
                        $this->addAttributeToSort($field, $direction);
                }
            }
            $this->_isOrdersRendered = true;
        }

        return $this;
    }

}
