<?php
/**
 * Iksula_Storemanager extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category       Iksula
 * @package        Iksula_Storemanager
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Store model
 *
 * @category    Iksula
 * @package     Iksula_Storemanager
 * @author      Ultimate Module Creator
 */
class Iksula_Storemanager_Model_Storehubrelation extends Mage_Core_Model_Abstract
{
    /**
     * Entity code.
     * Can be used as part of method name for entity processing
     */
    const ENTITY    = 'iksula_storemanager_store_hub';
    const CACHE_TAG = 'iksula_storemanager_store_hub';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'iksula_storemanager_store_hub';

    /**
     * Parameter name in event
     *
     * @var string
     */
    protected $_eventObject = 'store';

    /**
     * constructor
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function _construct()
    {
   //     parent::_construct();
        $this->_init('iksula_storemanager/storehubrelation');
    }




}
