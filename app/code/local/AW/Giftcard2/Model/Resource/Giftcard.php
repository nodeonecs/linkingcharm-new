<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_Model_Resource_Giftcard extends Mage_Core_Model_Mysql4_Abstract
{
    const CODE_GENERATION_ATTEMPTS = 1000;

    public function _construct()
    {
        $this->_init('aw_giftcard2/giftcard', 'giftcard_id');
    }

    protected function _beforeSave(Mage_Core_Model_Abstract $giftCard)
    {
        // Disable validation and code generation for imported gift cards
        if (!$giftCard->getIsImported()) {
            /** @var $giftCard AW_Giftcard2_Model_Giftcard */
            if (!$giftCard->getId()) {
                $giftCard->setBalance($giftCard->getInitialBalance());
            }

            $this->_attachCode($giftCard);

            if (!$giftCard->getId() && $giftCard->isExpired()) {
                throw new Mage_Core_Exception(
                    Mage::helper('aw_giftcard2')->__('Expiration date cannot be in the past')
                );
            }

            if (
                $giftCard->getAvailability() == AW_Giftcard2_Model_Source_Giftcard_Availability::DEACTIVATED_VALUE &&
                $giftCard->getOrigData() &&
                $giftCard->getOrigData('balance') == 0 &&
                $giftCard->getOrigData('balance') != $giftCard->getBalance()
            ) {
                throw new Mage_Core_Exception(
                    Mage::helper('aw_giftcard2')->__('Unable to change balance of deactivated gift card code')
                );
            }
        }

        $this->_attachState($giftCard);

        if ($giftCard->isPhysical()) {
            $giftCard
                ->unsetData('sender_email')
                ->unsetData('recipient_email')
            ;
        }

        return parent::_beforeSave($giftCard);
    }

    protected function _attachState(AW_Giftcard2_Model_Giftcard $giftCard)
    {
        $availability = $giftCard->getAvailability();

        if (null === $availability) {
            $availability = AW_Giftcard2_Model_Source_Giftcard_Availability::ACTIVE_VALUE;
        }
        if ($availability != AW_Giftcard2_Model_Source_Giftcard_Availability::DEACTIVATED_VALUE) {
            if ($giftCard->IsUsed()) {
                $availability = AW_Giftcard2_Model_Source_Giftcard_Availability::USED_VALUE;
            } elseif ($giftCard->isExpired()) {
                $availability = AW_Giftcard2_Model_Source_Giftcard_Availability::EXPIRED_VALUE;
            } else {
                $availability= AW_Giftcard2_Model_Source_Giftcard_Availability::ACTIVE_VALUE;
            }
        }

        if ($giftCard->isUsed()) {
            $isUsed = AW_Giftcard2_Model_Source_Giftcard_Used::YES_VALUE;
        } elseif ($giftCard->isPartiallyUsed()) {
            $isUsed = AW_Giftcard2_Model_Source_Giftcard_Used::PARTIALLY_VALUE;
        } else {
            $isUsed = AW_Giftcard2_Model_Source_Giftcard_Used::NO_VALUE;
        }

        $giftCard->setAvailability($availability);
        $giftCard->setIsUsed($isUsed);
        return $this;
    }

    protected function _attachCode(AW_Giftcard2_Model_Giftcard $giftCard)
    {
        if (null === $giftCard->getId()) {
            $websiteId = $giftCard->getWebsiteId();
            $attempt = 0;
            do {
                if ($attempt >= self::CODE_GENERATION_ATTEMPTS) {
                    throw new Mage_Core_Exception(
                        Mage::helper('aw_giftcard2')->__('Unable to create giftcard code')
                    );
                }
                $code = $this->_generateCode($websiteId);
                $attempt++;
            } while ($this->codeExists($code));

            $giftCard->setCode($code);
        }
        return $this;
    }

    protected function _generateCode($websiteId)
    {
        $website   = Mage::app()->getWebsite($websiteId);
        $format    = str_split((string)Mage::helper('aw_giftcard2/config')->getCouponCodeFormat($website));
        $length    = max(1, (int) Mage::helper('aw_giftcard2/config')->getCouponCodeLength($website));
        $split     = max(0, (int) Mage::helper('aw_giftcard2/config')->getCouponCodeDash($website));
        $suffix    = Mage::helper('aw_giftcard2/config')->getCouponCodeSuffix($website);
        $prefix    = Mage::helper('aw_giftcard2/config')->getCouponCodePrefix($website);
        $splitChar = (string) Mage::helper('aw_giftcard2/config')->getCouponCodeSeparator($website);
        $code      = '';

        for ($i = 0; $i < $length; $i++) {
            $char = $format[array_rand($format)];
            if ($split > 0 && ($i%$split) == 0 && $i != 0) {
                $char = "{$splitChar}{$char}";
            }
            $code .= $char;
        }

        $code = "{$prefix}{$code}{$suffix}";

        return $code;
    }

    public function codeExists($code)
    {
        $collection = Mage::getModel('aw_giftcard2/giftcard')->getCollection()->addCodeFilter($code);
        if ($collection->getSize() > 0) {
            return true;
        }
        return false;
    }

    protected function _afterDelete(Mage_Core_Model_Abstract $giftcard) {
        // DB cleanup after gift card deletion
        $write = $this->_getWriteAdapter();
        $write->query(
            "DELETE FROM {$this->getTable('aw_giftcard2/history')} "
            . "WHERE giftcard_id = {$giftcard->getId()}"
        );
        return parent::_afterDelete($giftcard);
    }

}
