<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   BV
 * @package    BV_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */


class Brainvire_Core_Block_Adminhtml_Log_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('brainvirecoreLogGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection() {
        $collection = Mage::getModel('brainvirecore/logger')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
        $this->addColumn('date', array(
            'header' => Mage::helper('brainvirecore')->__('Date'),
            'align' => 'right',
            'width' => '5',
            'index' => 'date',
            'type' => 'datetime'
        ));
        $this->addColumn('id', array(
            'header' => Mage::helper('brainvirecore')->__('ID'),
            'align' => 'right',
            'width' => '5',
            'index' => 'id',
        ));

        $this->addColumn('module', array(
            'header' => Mage::helper('brainvirecore')->__('Module'),
            'align' => 'left',
            'index' => 'module',
        ));

        $this->addColumn('type', array(
            'header' => Mage::helper('brainvirecore')->__('Title'),
            'align' => 'left',
            'index' => 'title',
        ));

        $this->addColumn('content', array(
            'header' => Mage::helper('brainvirecore')->__('Details'),
            'align' => 'left',
            'index' => 'content',
        ));

        $this->addColumn('object', array(
            'header' => Mage::helper('brainvirecore')->__('Object'),
            'align' => 'left',
            'index' => 'object',
        ));

        

        $ret = parent::_prepareColumns();


        return $ret;
    }

    public function getRowUrl($row) {
        return false;
    }

}
