<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2013 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * roles grid
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Amasty_Rolepermissions_Block_Adminhtml_Permissions_Grid_Role extends Mage_Adminhtml_Block_Permissions_Grid_Role
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('roleGrid');
        $this->setSaveParametersInSession(true);
        $this->setDefaultSort('role_id');
        $this->setDefaultDir('asc');
        $this->setUseAjax(true);
    }

    public function showRolepermission(){

        $admin_user_session = Mage::getSingleton('admin/session');
        $adminuserId = $admin_user_session->getUser()->getUserId();
        $currentroleid = Mage::getModel('admin/user')->load($adminuserId)->getRole()->getId();


        $AdvancedCustomRole = Mage::getModel('amrolepermissions/rule')->load($currentroleid , 'role_id')->getCustomRole();

       /* echo $AdvancedCustomRole;
        exit;*/



        return array($AdvancedCustomRole , $currentroleid) ;

    }



    protected function _prepareCollection()
    {


        list($role_custom , $role_id ) = $this->showRolepermission();
        if($role_custom){
            if($role_custom == 'W_HO'){
                //echo 'I am here';

                $Ho_websiteId = Mage::getModel('amrolepermissions/rule')->load($role_id , 'role_id')->getScopeWebsites();

                $storesViewsCollections = Mage::getModel('amrolepermissions/rule')->getCollection()->addFieldToFilter('custom_role' , array('eq' => 'STORE_VIEW'))->addFieldToSelect(array('scope_storeviews' , 'role_id'));
                $aStoreRoleId = array();
                foreach($storesViewsCollections as $stores){

                    $aStoresViews = explode(',' , $stores->getScopeStoreviews());

                    $iStoreViewsWebsiteID = Mage::getModel('core/store')->load($aStoresViews[0])->getWebsiteId();

                    if($iStoreViewsWebsiteID == $Ho_websiteId){
                        $aStoreRoleId [] = $stores->getRoleId();
                    }

                }

                $CustomRoleId = Mage::getModel('amrolepermissions/rule')->getCollection()->addFieldToFilter('custom_role' , array('nin' => array('S_A' , 'W_HO')))->addFieldToFilter('role_id' , array('in' => $aStoreRoleId))->addFieldToSelect('role_id');

                foreach($CustomRoleId as $roleid){

                    $aRoleId [] = $roleid->getRoleId();
                }

                $collection =  Mage::getModel("admin/roles")->getCollection()->addFieldToFilter('role_id' , array('in' => $aRoleId));

            }else{

                $collection =  Mage::getModel("admin/roles")->getCollection();

            }

        }else{
            $collection =  Mage::getModel("admin/roles")->getCollection();
        }
        //echo $collection->getSelect();
        $this->setCollection($collection);

        //return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {

        $this->addColumn('role_id', array(
            'header'    =>Mage::helper('adminhtml')->__('ID'),
            'index'     =>'role_id',
            'align'     => 'right',
            'width'    => '50px'
        ));

        $this->addColumn('role_name', array(
            'header'    =>Mage::helper('adminhtml')->__('Role Name'),
            'index'     =>'role_name'
        ));

        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/roleGrid', array('_current'=>true));
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/editrole', array('rid' => $row->getRoleId()));
    }
}
