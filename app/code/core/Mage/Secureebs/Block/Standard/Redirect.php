

<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category   Mage
 * @package    Mage_Secureebs
 * @copyright  Copyright (c) 2008 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Redirect to Secureebs
 *
 * @category    Mage
 * @package     Mage_Secureebs
 * @name        Mage_Secureebs_Block_Standard_Redirect
 * @author      Magento Core Team <core@magentocommerce.com>
 */

class Mage_Secureebs_Block_Standard_Redirect extends Mage_Core_Block_Abstract
{

    protected function _toHtml()
    {


        $standard = Mage::getModel('secureebs/standard');
        $form = new Varien_Data_Form();
        $form->setAction($standard->getSecureebsUrl())
            ->setId('secureebs_standard_checkout')
            ->setName('secureebs_standard_checkout')
            ->setMethod('POST')
            ->setUseContainer(true);

        $formArray      = $standard->setOrder($this->getOrder())->getStandardCheckoutFormFields();
        $orderId        = $formArray['order_id'];
        $amount         = $formArray['product_price'];
        $fname          = $formArray['f_name'];
        $lname          = $formArray['s_name'];
        $email          = $formArray['email'];
        $phone          = $formArray['phone'];
        $city           = $formArray['city'];
        $desc           = $formArray['product_desc'];
        $ccName         = $formArray['cc_name'];
        $ccNum          = $formArray['cc_num'];
        $ccExpMon       = $formArray['cc_exp_mon'];
        $ccExpYear      = $formArray['cc_exp_yr'];
        $ccCvv          = $formArray['cc_cvv'];
        $sUrl           = Mage::getUrl('secureebs/standard/success', array('_secure' => true));
        $fUrl           = Mage::getUrl('secureebs/standard/failure', array('_secure' => true));
        $bCode          = $formArray['cc_type'];
        $pg             = 'CC';
        $mId            = Mage::getSingleton('secureebs/config')->getAccountId();
        $salt           = Mage::getSingleton('secureebs/config')->getSecretKey();
		
	$hash =  $mId . '|' . $orderId . '|' . $amount . '|' . $desc . '|' .  $fname . '|' . $email .'|||||||||||'.$salt;
	$secure_hash = hash('sha512',$hash);
	
	//echo $ccCvv;
	//echo Mage::getSingleton('checkout/session')->getCcChecked();

	//exit;
	
	$card_token = Mage::getSingleton('checkout/session')->getCardtoken();
	$cvvnew = Mage::getSingleton('checkout/session')->getCvvnew();
	$var1 = $mId.':'.$email;


	$form->addField('key', 'hidden', array('name'=>'key', 'value'=>$mId));
        $form->addField('hash', 'hidden', array('name'=>'hash', 'value'=>$secure_hash));
        $form->addField('txnid', 'hidden', array('name'=>'txnid', 'value'=>$orderId));
        $form->addField('amount', 'hidden', array('name'=>'amount', 'value'=>$amount));
        $form->addField('email', 'hidden', array('name'=>'email', 'value'=>$email));
        $form->addField('productinfo', 'hidden', array('name'=>'productinfo', 'value'=>$desc));
        $form->addField('firstname', 'hidden', array('name'=>'firstname', 'value'=>$fname));
        $form->addField('City', 'hidden', array('name'=>'City', 'value'=>$city));
        $form->addField('phone', 'hidden', array('name'=>'phone', 'value'=>$phone));
        $form->addField('surl', 'hidden', array('name'=>'surl', 'value'=>$sUrl));
        $form->addField('furl', 'hidden', array('name'=>'furl', 'value'=>$fUrl));
	
	//If card token not found
	if(Mage::getSingleton('checkout/session')->getCardtoken() == "") {
	$form->addField('bankcode','hidden',array('name'=>'bankcode','value'=>$bCode));
	$form->addField('pg','hidden',array('name'=>'pg','value'=>$pg));
	$form->addField('ccname','hidden',array('name'=>'ccname','value'=>$ccName));
	$form->addField('ccnum','hidden',array('name'=>'ccnum','value'=>$ccNum));
	$form->addField('ccvv','hidden',array('name'=>'ccvv','value'=>$ccCvv));
	$form->addField('ccexpmon','hidden',array('name'=>'ccexpmon','value'=>$ccExpMon));
	$form->addField('ccexpyr','hidden',array('name'=>'ccexpyr','value'=>$ccExpYear));
	}

	//If card token found
	if(Mage::getSingleton('checkout/session')->getCardtoken() != "") {
	$form->addField('user_credentials','hidden',array('name'=>'user_credentials','value'=>$var1));
	$form->addField('ccvv','hidden',array('name'=>'ccvv','value'=>$cvvnew));
	$form->addField('store_card_token','hidden',array('name'=>'store_card_token','value'=>$card_token));
	}
	
	//If saved card is checked
	if(Mage::getSingleton('checkout/session')->getCcChecked() == "true") {
		$form->addField('user_credentials','hidden',array('name'=>'user_credentials','value'=>$var1));
		$form->addField('store_card','hidden',array('name'=>'store_card','value'=> 1 ));
	}
	
	
       
        $html = '<html><body>';
        $html.= $this->__('Please be patient. We are processing your request.');
        $html.= $form->toHtml();
        $html.= '<script type="text/javascript">document.getElementById("secureebs_standard_checkout").submit();</script>';
        $html.= '</body></html>';

        return $html;
	}
}
