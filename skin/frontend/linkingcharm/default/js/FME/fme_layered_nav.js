// checking if IE: this variable will be understood by IE: isIE = !false
isIE = /*@cc_on!@*/false;

Control.Slider.prototype.setDisabled = function()
{
    this.disabled = true;

    if (!isIE)
    {
        this.track.parentNode.className = this.track.parentNode.className + ' disabled';
    }
};

function fme_layered_hide_products()
{
    var items = $('fme_filters_list').select('a', 'input');
    n = items.length;
    for (i = 0; i < n; ++i) {
        items[i].addClassName('fme_layered_disabled');
    }

    if (typeof (fme_slider) != 'undefined')
        fme_slider.setDisabled();

    var divs = $$('div.fme_loading_filters');
    for (var i = 0; i < divs.length; ++i)
        divs[i].show();
}

function fme_layered_show_products(transport)
{
    var resp = {};
    if (transport && transport.responseText) {
        try {
            resp = eval('(' + transport.responseText + ')');
        }
        catch (e) {
            resp = {};
        }
    }

    if (resp.products) {

        var ajaxUrl = $('fme_layered_ajax').value;

        if ($('fme_layered_container') == undefined) {

            var c = $$('.col-main')[0];// alert(c.hasChildNodes());
            if (c.hasChildNodes()) {
                while (c.childNodes.length > 2) {
                    c.removeChild(c.lastChild);
                }
            }

            var div = document.createElement('div');
            div.setAttribute('id', 'fme_layered_container');
            $$('.col-main')[0].appendChild(div);

        }

        var el = $('fme_layered_container');
        el.update(resp.products.gsub(ajaxUrl, $('fme_layered_url').value));
        catalog_toolbar_init();

        $('catalog-filters').update(
                resp.layer.gsub(
                        ajaxUrl,
                        $('fme_layered_url').value
                        )
                );

        $('fme_layered_ajax').value = ajaxUrl;
    }

    var items = $('fme_filters_list').select('a', 'input');
    n = items.length;
    for (i = 0; i < n; ++i) {
        items[i].removeClassName('fme_layered_disabled');
    }
    if (typeof (fme_slider) != 'undefined')
        fme_slider.setEnabled();

     jQueryWaiter.execute(function(){
       // alert('test');
        StrategeryInfiniteScroll.init();

    });
}

function fme_layered_add_params(k, v, isSingleVal)
{
    var el = $('fme_layered_params');
    var params = el.value.parseQuery();

    var strVal = params[k];
    if (typeof strVal == 'undefined' || !strVal.length) {
        params[k] = v;
    }
    else if ('clear' == v) {
        params[k] = 'clear';
    }
    else {
        if (k == 'price')
            var values = strVal.split(',');
        else
            var values = strVal.split('-');

        if (-1 == values.indexOf(v)) {
            if (isSingleVal)
                values = [v];
            else
                values.push(v);
        }
        else {
            values = values.without(v);
        }

        params[k] = values.join('-');
    }

    el.value = Object.toQueryString(params).gsub('%2B', '+');
}



function fme_layered_make_request()
{
    fme_layered_hide_products();

    var params = $('fme_layered_params').value.parseQuery();

    if (!params['dir'])
    {
        $('fme_layered_params').value += '&dir=' + 'desc';
    }

    new Ajax.Request(
            $('fme_layered_ajax').value + '?' + $('fme_layered_params').value,
            {
                method: 'get',
                onSuccess: fme_layered_show_products
            }
    );
}


function fme_layered_update_links(evt, className, isSingleVal)
{
    var link = Event.findElement(evt, 'A'),
            sel = className + '-selected';

    if (link.hasClassName(sel))
        link.removeClassName(sel);
    else
        link.addClassName(sel);

    //only one  price-range can be selected
    if (isSingleVal) {
        var items = $('fme_filters_list').getElementsByClassName(className);
        var i, n = items.length;
        for (i = 0; i < n; ++i) {
            if (items[i].hasClassName(sel) && items[i].id != link.id)
                items[i].removeClassName(sel);
        }
    }

    fme_layered_add_params(link.id.split('-')[0], link.id.split('-')[1], isSingleVal);

    fme_layered_make_request();

    Event.stop(evt);
}


function fme_layered_attribute_listener(evt)
{
    fme_layered_add_params('p', 1, 1);
    fme_layered_update_links(evt, 'fme_layered_attribute', 0);
}


function fme_layered_price_listener(evt)
{
    fme_layered_add_params('p', 1, 1);
    fme_layered_update_links(evt, 'fme_layered_price', 1);
}

function fme_layered_clear_listener(evt)
{
    var link = Event.findElement(evt, 'A'),
            varName = link.id.split('-')[0];

    fme_layered_add_params('p', 1, 1);
    fme_layered_add_params(varName, 'clear', 1);

    if ('price' == varName) {
        var from = $('adj-nav-price-from'),
                to = $('adj-nav-price-to');

        if (Object.isElement(from)) {
            from.value = from.name;
            to.value = to.name;
        }
    }

    fme_layered_make_request();

    Event.stop(evt);
}


function roundPrice(num) {
    num = parseFloat(num);
    if (isNaN(num))
        num = 0;

    return Math.round(num);
}

function fme_layered_category_listener(evt) {
    var link = Event.findElement(evt, 'A');
    var catId = link.id.split('-')[1];

    var reg = /cat-/;
    if (reg.test(link.id)) { //is search
        fme_layered_add_params('cat', catId, 1);
        fme_layered_add_params('p', 1, 1);
        fme_layered_make_request();
        Event.stop(evt);
    }
    //do not stop event
}

function catalog_toolbar_listener(evt) {
    catalog_toolbar_make_request(Event.findElement(evt, 'A').href);
    Event.stop(evt);
}

function catalog_toolbar_make_request(href)
{
    var pos = href.indexOf('?');
    if (pos > -1) {
        $('fme_layered_params').value = href.substring(pos + 1, href.length);
    }
    fme_layered_make_request();
}


function catalog_toolbar_init()
{
    var items = $('fme_layered_container').select('.pages a', '.view-mode a', '.sort-by a');
    var i, n = items.length;
    for (i = 0; i < n; ++i) {
        Event.observe(items[i], 'click', catalog_toolbar_listener);
    }
}

function fme_layered_dt_listener(evt) {
    var e = Event.findElement(evt, 'DIV');
    var effect = e.nextSiblings()[0];
    Effect.toggle(effect, 'Slide', {duration:0});
    e.toggleClassName('fme_layered_dt_selected');
}

function fme_layered_clearall_listener(evt)
{
    var params = $('fme_layered_params').value.parseQuery();
    $('fme_layered_params').value = 'clearall=true';
    if (params['q'])
    {
        $('fme_layered_params').value += '&q=' + params['q'];
    }
    fme_layered_make_request();
    Event.stop(evt);
}

function price_input_listener(evt) {
    if (evt.type == 'keypress' && 13 != evt.keyCode)
        return;

    if (evt.type == 'keypress') {
        var inpObj = Event.findElement(evt, 'INPUT');
    } else {
        var inpObj = Event.findElement(evt, 'BUTTON');
    }

    var sKey = inpObj.id.split('---')[1];
    var numFrom = roundPrice($('price_range_from---' + sKey).value),
            numTo = roundPrice($('price_range_to---' + sKey).value);

    if ((numFrom < 0.01 && numTo < 0.01) || numFrom < 0 || numTo < 0)
        return;

    fme_layered_add_params('p', 1, 1);
    fme_layered_add_params(sKey, numFrom + ',' + numTo, true);
    fme_layered_make_request();
}

function fme_layered_init()
{
    var items, i, j, n,
            classes = ['category', 'attribute', 'icon', 'price', 'clear', 'dt', 'clearall'];

    for (j = 0; j < classes.length; ++j) {
        items = $('fme_filters_list').select('.fme_layered_' + classes[j]);
        n = items.length;
        for (i = 0; i < n; ++i) {
            Event.observe(items[i], 'click', eval('fme_layered_' + classes[j] + '_listener'));
        }
    }

    items = $('fme_filters_list').select('.price-input');
    n = items.length;
    var btn = $('price_button_go');
    for (i = 0; i < n; ++i)
    {
        btn = $('price_button_go---' + items[i].value);
        if (Object.isElement(btn)) {
            Event.observe(btn, 'click', price_input_listener);
            Event.observe($('price_range_from---' + items[i].value), 'keypress', price_input_listener);
            Event.observe($('price_range_to---' + items[i].value), 'keypress', price_input_listener);
        }
    }
    // start of quickview patch


      jQuery.noConflict();
            jQuery(function($) {
                var myhref,qsbtt;

                // base function

                //get IE version
                function ieVersion(){
                    var rv = -1; // Return value assumes failure.
                    if (navigator.appName == 'Microsoft Internet Explorer'){
                        var ua = navigator.userAgent;
                        var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
                        if (re.exec(ua) != null)
                            rv = parseFloat( RegExp.$1 );
                    }
                    return rv;
                }

                //read href attr in a tag
                function readHref(){
                    var mypath = arguments[0];
                    var patt = /\/[^\/]{0,}$/ig;
                    if(mypath[mypath.length-1]=="/"){
                        mypath = mypath.substring(0,mypath.length-1);
                        return (mypath.match(patt)+"/");
                    }
                    return mypath.match(patt);
                }


                //string trim
                function strTrim(){
                    return arguments[0].replace(/^\s+|\s+$/g,"");
                }

                function _qsJnit(){



                    var selectorObj = arguments[0];
                        //selector chon tat ca cac li chua san pham tren luoi
                    var listprod = $(selectorObj.itemClass);
                    var qsImg;
                    var mypath = 'quickview/index/view';
                    // alert(EM.Quickview.BASE_URL);
                    // if(EM.Quickview.BASE_URL.indexOf('index.php') == -1){
                        // mypath = 'quickview/index/view';
                    // }else{
                        // mypath = 'index.php/quickview/index/view';
                    // }
                    var baseUrl = EM.Quickview.BASE_URL + mypath;

                    var _qsHref = "<a id=\"md_quickview_handler\" href=\"#\" style=\"visibility:hidden;position:absolute;top:0;left:0\"><img style=\"display:none;\" alt=\"quickview\" src=\""+EM.Quickview.QS_IMG+"\" /></a>";
                    $(document.body).append(_qsHref);

                    var qsHandlerImg = $('#md_quickview_handler img');

                    $.each(listprod, function(index, value) {
                        var reloadurl = baseUrl;

                        //get reload url
                        myhref = $(value).children(selectorObj.aClass );
                        var prodHref = readHref(myhref.attr('href'))[0];
                        prodHref[0] == "\/" ? prodHref = prodHref.substring(1,prodHref.length) : prodHref;
                        prodHref=strTrim(prodHref);

                        reloadurl = baseUrl+"/path/"+prodHref;
                        version = ieVersion();
                        if(version < 8.0 && version > -1){
                            reloadurl = baseUrl+"/path"+prodHref;
                        }
                        //end reload url


                        $(selectorObj.imgClass, this).live('mouseover', function() {
                            var o = $(this).offset();
                            $('#md_quickview_handler').attr('href',reloadurl).show()
                                .css({
                                    'top': o.top+($(this).height() - qsHandlerImg.height())/2+'px',
                                    'left': o.left+($(this).width() - qsHandlerImg.width())/2+'px',
                                    'visibility': 'visible'
                                });
                        });
                        $(value).bind('mouseout', function() {
                            $('#md_quickview_handler').hide();
                        });
                    });

                    //fix bug image disapper when hover
                    $('#md_quickview_handler')
                        .bind('mouseover', function() {
                            $(this).show();
                        })
                        .bind('click', function() {
                            $(this).hide();
                        });
                    //insert quickview popup

                    $('#md_quickview_handler').fancybox({
                            onStart             : function() {$.fancybox.showActivity();},
                            'titleShow'         : false,
                            'width'             : EM.Quickview.QS_FRM_WIDTH,
                            'height'            : 'auto',//EM.Quickview.QS_FRM_HEIGHT,
                            'autoScale'         : false,
                            'transitionIn'      : 'none',
                            'transitionOut'     : 'none',
                            'autoDimensions'    : false,
                            'scrolling'         : 'no',
                            'padding'           :0,
                            'margin'            :0,
                            'type'              : 'ajax',
                            'closeClick'        : false,
                            'overlayColor'      : EM.Quickview.OVERLAYCOLOR

                    });
                    /*custom filters code*/
                    var sortHtml = '';
                    var total = $('.toolbar .sort_select:first option').length;
                    $.each($('.toolbar .sort_select:first option'), function( index, value ) {
                       console.log($(this).text());
                        if($(this).is(':selected')){

                           if (index === total - 1) {
                                sortHtml = sortHtml + '<a href="' + $(this).val()+ '" class="selected-sorting">' +$(this).text() + '</a>';
                            }
                            else{
                                sortHtml = sortHtml + '<a href="' + $(this).val()+ '" class="selected-sorting">' +$(this).text() + '</a><label class="sort-pipe">|</label>';
                            }
                        }
                        else{

                            if (index === total - 1) {
                                sortHtml = sortHtml + '<a href="' + $(this).val()+ '">' +$(this).text() + '</a>';
                            }
                            else{
                                sortHtml = sortHtml + '<a href="' + $(this).val()+ '">' +$(this).text() + '</a><label class="sort-pipe">|</label>';
                            }
                        }
                    });
                    if($('.sort-pipe').length){

                    }
                    else{
                        $(sortHtml).insertAfter(".toolbar .sort-by label");
                    }
                    /*custom filters code*/



                }

                //end base function


                _qsJnit({
                    itemClass : '.products-grid li.item', //selector for each items in catalog product list,use to insert quickview image
                    aClass : 'a.product-image', //selector for each a tag in product items,give us href for one product
                    imgClass: '.product-image img' //class for quickview href product-collateral
                });
            });

    // end of quickview patch


// finish new fix code
}

function create_price_slider(width, from, to, min_price, max_price, sKey)
{
    var price_slider = $('fme_layered_price_slider' + sKey);

    return new Control.Slider(price_slider.select('.handle'), price_slider, {
        range: $R(0, width),
        sliderValue: [from, to],
        restricted: true,
        onChange: function(values) {
            var f = calculateSliderPrice(width, from, to, min_price, max_price, values[0]),
                    t = calculateSliderPrice(width, from, to, min_price, max_price, values[1]);

            fme_layered_add_params(sKey, f + ',' + t, true);

            $('price_range_from' + sKey).update(f);
            $('price_range_to' + sKey).update(t);

            fme_layered_make_request();
        },
        onSlide: function(values) {
            $('price_range_from' + sKey).update(calculateSliderPrice(width, from, to, min_price, max_price, values[0]));
            $('price_range_to' + sKey).update(calculateSliderPrice(width, from, to, min_price, max_price, values[1]));
        }
    });
}

function calculateSliderPrice(width, from, to, min_price, max_price, value)
{
    var calculated = roundPrice(((max_price - min_price) * value / width) + min_price);

    return calculated;
}
