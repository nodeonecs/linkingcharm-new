<?php

class Iksula_Storemanager_Model_Store_Option_Websites extends Varien_Object{


    protected $_options = array();


	public function getOptionArray(){

		$aWebsiteCollections = Mage::getResourceModel('core/website_collection')
        ->addFieldToSelect(array('website_id' , 'name'));
        //$this->_options[] = array('label' => 'Please select the Websites' , 'value' => '');
		//$aWebsites = array();
		foreach($aWebsiteCollections as $aWebsites){

			//$aWebsites [$aWebsites->getWebsiteId()] = $aWebsites->getName();
			$this->_options[] = array(
                'label' => $aWebsites->getName(),
                'value' => $aWebsites->getWebsiteId()
            );

		}


        /*foreach ($storeData as $key => $values) {

            $this->_options[] = array(
                'label' => $values,
                'value' => $key
            );

        }*/
        return $this->_options;

	}




}
