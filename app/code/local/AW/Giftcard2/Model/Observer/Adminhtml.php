<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_Model_Observer_Adminhtml
{
    public function controllerActionPredispatch(Varien_Event_Observer $observer)
    {
        if (!Mage::helper('aw_giftcard2')->isModuleOutputEnabled()) {
            return $this;
        }
        $action = $observer->getControllerAction();
        $session = Mage::getSingleton('adminhtml/session');

        switch ($action->getFullActionName()) {
            case 'adminhtml_awgiftcard2_product_index':
                $session->setBackToAwGiftCardProductsGridFlag(false);
                break;
            case 'adminhtml_catalog_product_index':
                $session->setResetBackToAwGiftCardProductsGridFlag(true);
                if (!$action->getRequest()->getParam('_menu', false)) {
                    if ($session->getBackToAwGiftCardProductsGridFlag() === true) {
                        $action->setFlag('', Mage_Core_Controller_Varien_Action::FLAG_NO_DISPATCH, true);
                        $action->setFlag('', Mage_Core_Controller_Varien_Action::FLAG_NO_POST_DISPATCH, true);
                        $action->getResponse()->setRedirect(
                            Mage::helper('adminhtml')->getUrl(
                                'adminhtml/awgiftcard2_product/index',
                                array('store' => $action->getRequest()->getParam('store', 0))
                            )
                        );
                    }
                } else {
                    $session->setBackToAwGiftCardProductsGridFlag(false);
                }
                break;
            case 'adminhtml_catalog_product_edit':
            case 'adminhtml_catalog_product_new':
                if ($session->getResetBackToAwGiftCardProductsGridFlag() === true) {
                    $session->setBackToAwGiftCardProductsGridFlag(false);
                }
                break;
        }
        return $this;
    }

    public function catalogProductValidateAfter(Varien_Event_Observer $observer)
    {
        $product = $observer->getEvent()->getProduct();

        if ($product->getTypeId() != AW_Giftcard2_Model_Product_Type_Giftcard::TYPE_CODE) {
            return $this;
        }

        $amounts = $product->getData(AW_Giftcard2_Model_Product_Type_Giftcard::ATTRIBUTE_CODE_AMOUNTS);
        $isAllowOpenAmount = $product->getTypeInstance()->isAllowOpenAmount($product);

        if (!$this->_isNotEmptyAmounts($amounts) && !$isAllowOpenAmount) {
            throw new Mage_Core_Exception(Mage::helper('aw_giftcard2')->__('Gift Card amount is not specified'));
        }

        return $this;
    }

    /**
     * Checks if amounts has any actual amount
     *
     * @param array $amounts
     * @return bool
     */
    protected function _isNotEmptyAmounts($amounts)
    {
        if (is_array($amounts)) {
            foreach ($amounts as $amount) {
                if (isset($amount['delete'])) {
                    return true;
                }
            }
        }
        return false;
    }

    public function catalogProductSaveAfter(Varien_Event_Observer $observer)
    {
        $product = $observer->getEvent()->getProduct();

        if ($product->getTypeId() != AW_Giftcard2_Model_Product_Type_Giftcard::TYPE_CODE) {
            return $this;
        }

        Mage::helper('aw_giftcard2/statistics')->addEmptyStatistics($product->getId());

        return $this;
    }

    /**
     * Updates quote total for back-end order creation process
     *
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function createOrderSessionQuoteInitialized(Varien_Event_Observer $observer)
    {
        if (
            !Mage::helper('aw_giftcard2')->isModuleOutputEnabled()
            || Mage::app()->getRequest()->getActionName() != 'index'
        ){
            return $this;
        }

        $quote = $observer->getEvent()->getSessionQuote()->getQuote();
        $appliedGiftCards = Mage::helper('aw_giftcard2/giftcard')->getQuoteGiftCards($quote->getId());
        if (count($appliedGiftCards) > 0) {
            $quote->collectTotals()->save();
        }

        return $this;
    }

    /**
     * Applies/removes gift card to/from quote for back-end order creation process
     *
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function salesOrderCreateProcessData(Varien_Event_Observer $observer)
    {
        if (!Mage::helper('aw_giftcard2')->isModuleOutputEnabled()) {
            return $this;
        }

        $orderCreateModel = $observer->getEvent()->getOrderCreateModel();
        $quote = $orderCreateModel->getQuote();
        $request = $observer->getEvent()->getRequest();
        $gcCodeIdentifier = Mage::helper('aw_giftcard2/config')->getAdminCodeIdentifier();
        $gcCodeIdentifierAddCard = $gcCodeIdentifier . '_add';
        $gcCodeIdentifierRemoveCard = $gcCodeIdentifier . '_remove';

        if (array_key_exists($gcCodeIdentifierAddCard, $request) && trim($request[$gcCodeIdentifierAddCard])) {
            $giftCardCode = trim($request[$gcCodeIdentifierAddCard]);
            try {
                $giftCard = Mage::getModel('aw_giftcard2/giftcard')->loadByCode($giftCardCode);
                Mage::helper('aw_giftcard2/giftcard')->addToQuote($giftCard, $quote->getId(), $quote);

                Mage::getSingleton('adminhtml/session_quote')->addSuccess(
                    Mage::helper('aw_giftcard2')->__('Gift Card Code %s has been applied.', Mage::helper('core')->escapeHtml($giftCardCode))
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session_quote')->addError(Mage::helper('aw_giftcard2')->__($e->getMessage()));
            }
        }

        if (array_key_exists($gcCodeIdentifierRemoveCard, $request) && trim($request[$gcCodeIdentifierRemoveCard])) {
            $giftCardCode = trim($request[$gcCodeIdentifierRemoveCard]);
            try {
                $giftCard = Mage::getModel('aw_giftcard2/giftcard')->loadByCode($giftCardCode);
                if ($giftCard->getId()) {
                    Mage::helper('aw_giftcard2/giftcard')->removeFromQuote($giftCard->getId(), $quote->getId(), $quote);
                    Mage::getSingleton('adminhtml/session_quote')->addSuccess(
                        Mage::helper('aw_giftcard2')->__('Gift Card Code %s has been removed.', Mage::helper('core')->escapeHtml($giftCardCode))
                    );
                }
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session_quote')->addError(Mage::helper('aw_giftcard2')->__($e->getMessage()));
            }
        }

        return $this;
    }
}