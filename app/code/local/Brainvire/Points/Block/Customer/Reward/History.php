<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   Brainvire
 * @package    Brainvire_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * @license    
 */


class Brainvire_Points_Block_Customer_Reward_History extends Mage_Core_Block_Template {

    protected function _construct() {
        parent::_construct();

        $collection = Mage::getModel('points/api')
                        ->getCustomerTransactions(
                                Mage::getSingleton('customer/session')
                                ->getCustomer()
                        )->setOrder('change_date', 'DESC');

        $this->setData('collection', $collection);
    }

    protected function _prepareLayout() {
        parent::_prepareLayout();
        $pager = $this->getLayout()->createBlock('page/html_pager', 'points.rewards.history.pager')
                ->setCollection($this->getCollection());
        $this->setChild('points_pager', $pager);
        return $this;
    }

    public function getPagerHtml() {
        return $this->getChildHtml('points_pager');
    }

    public function getTransactions() {
        return $this->getCollection();
    }

    public function isEnabled() {
        return Mage::helper('points/config')->getIsEnabledRewardsHistory();
    }

    protected function _toHtml() {

        $magentoVersionTag = Brainvire_Points_Helper_Data::MAGENTO_VERSION_14;

        if (Mage::helper('points')->magentoLess14())
            $magentoVersionTag = Brainvire_Points_Helper_Data::MAGENTO_VERSION_13;

        $this->setTemplate("brainvire_points/customer/" . $magentoVersionTag . "/reward/history.phtml");

        $html = parent::_toHtml();
        return $html;
    }

}

?>
