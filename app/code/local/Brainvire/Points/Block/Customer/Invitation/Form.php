<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   Brainvire
 * @package    Brainvire_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * @license    
 */


class Brainvire_Points_Block_Customer_Invitation_Form extends Mage_Core_Block_Template {
    const MAX_INVITATIONS_PER_SEND = 5;

    
    public function getBackUrl() {
        return $this->getUrl('points/invitation/');
    }

    
    public function getMaxInvitationsPerSend() {
        return self::MAX_INVITATIONS_PER_SEND;
    }

    protected function _toHtml() {

        $magentoVersionTag = Brainvire_Points_Helper_Data::MAGENTO_VERSION_14;

        if (Mage::helper('points')->magentoLess14())
            $magentoVersionTag = Brainvire_Points_Helper_Data::MAGENTO_VERSION_13;

        $this->setTemplate("brainvire_points/customer/" . $magentoVersionTag . "/invitation/form.phtml");

        $html = parent::_toHtml();
        return $html;
    }

}
