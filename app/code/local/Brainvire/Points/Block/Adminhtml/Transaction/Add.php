<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   BV
 * @package    BV_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */


class Brainvire_Points_Block_Adminhtml_Transaction_Add extends Mage_Adminhtml_Block_Widget {

    public function getHeaderText() {
        return Mage::helper('points')->__('Add Transaction');
    }

    protected function _prepareLayout() {
        $this->setChild('back_button', $this->getLayout()->createBlock('adminhtml/widget_button')
                        ->setData(array(
                            'label' => Mage::helper('points')->__('Back'),
                            'onclick' => "window.location.href = '" . $this->getUrl('*/*') . "'",
                            'class' => 'back'
                        ))
        );

        $this->setChild('save_button', $this->getLayout()->createBlock('adminhtml/widget_button')
                        ->setData(array(
                            'label' => Mage::helper('points')->__('Save Transaction'),
                            'onclick' => "transactionAddForm.submit();",
                            'class' => 'save'
                        ))
        );

        return parent::_prepareLayout();
    }

    public function getSaveButtonHtml() {
        return $this->getChildHtml('save_button');
    }

    public function getBackButtonHtml() {
        return $this->getChildHtml('back_button');
    }

    public function getSaveUrl() {
        return $this->getUrl('*/*/save');
    }

    public function getForm() {
        return $this->getLayout()
                        ->createBlock('points/adminhtml_transaction_add_form')
                        ->toHtml();
    }

    public function getCustomersGrid() {
        return $this->getLayout()
                        ->createBlock('points/adminhtml_customer_grid')
                        ->toHtml();
    }

}