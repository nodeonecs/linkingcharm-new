<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   Brainvire
 * @package    Brainvire_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * @license    
 */


class Brainvire_Core_Model_Abstract extends Mage_Core_Model_Abstract {
    
    const DB_DATETIME_FORMAT = 'yyyy-MM-dd HH:m:s'; 
    
    const DB_DATE_FORMAT= 'yyyy-MM-dd';
    
    const JS_DATE_FORMAT= 'yyyy-M-d';

    
    const RETURN_BOOLEAN = 'BOOL';
   
    const RETURN_INTEGER = 'INT';
    
    const RETURN_FLOAT = 'FLOAT';
    
    const RETURN_STRING = 'STR';
    
    const RETURN_ARRAY = 'ARR';
   
    const RETURN_OBJECT = 'OBJ';

    
    public function log($message, $severity=null, $details='') {
        Mage::helper('brainvirecore/logger')->log($this, $message, $severity, $details);
    }

}