<?php
require_once 'abstract.php';

class Mage_Shell_Refresh_Statistics extends Mage_Shell_Abstract
{
   
    protected function _getCodes()
    {
        $allcodes = array(
            '0'     => 'sales/report_order',
            '1'     => 'tax/report_tax',
            '2'     => 'sales/report_shipping',
            '3'     => 'sales/report_invoiced',
            '4'     => 'sales/report_refunded',
            '5'     => 'salesrule/report_rule',
            '6'     => 'sales/report_bestsellers',
        );
        return $allcodes;
    }

   
    /**
     * Run script
     *
     */
    public function run()
    {
            $allCodes = $this->_getCodes();
            try {
                foreach ($allCodes as $collectionName) {
                    Mage::getResourceModel($collectionName)->aggregate();
                }
                echo 'Lifetime statistics have been updated.';
            } catch (Mage_Core_Exception $e) {
                echo $e->getMessage();
            } catch (Exception $e) {
                echo 'Unable to refresh lifetime statistics.';
                Mage::logException($e);
            }
           
            echo "Statistics Refreshed!";
    }

    /**
     * Retrieve Usage Help Message
     *
     */
}

$shell = new Mage_Shell_Refresh_Statistics();
$shell->run();