<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   BV
 * @package    BV_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */

class Brainvire_Points_Block_Adminhtml_Transaction extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {
        $this->_controller = 'adminhtml_transaction';
        $this->_blockGroup = 'points';
        $this->_headerText = Mage::helper('points')->__('Transactions Details');
        parent::__construct();
    }

}
