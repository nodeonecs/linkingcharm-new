<?php 
require_once "Mage/Cms/controllers/IndexController.php";
class Iksula_Homepage_Cms_IndexController extends Mage_Cms_IndexController
{
	public function indexAction()
    {
    	$this->loadLayout();
    	$this->renderLayout();
    }
}