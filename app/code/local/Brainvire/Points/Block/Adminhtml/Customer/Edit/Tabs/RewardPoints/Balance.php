<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   BV
 * @package    BV_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */


class Brainvire_Points_Block_Adminhtml_Customer_Edit_Tabs_RewardPoints_Balance extends Mage_Adminhtml_Block_Template {

    protected function _construct() {
        parent::_construct();
        $this->setData('customer', Mage::registry('current_customer'));
        $this->setData('website', $this->getCustomer()->getStore()->getWebsite());
        $this->addData(
                Mage::getModel('points/summary')
                        ->loadByCustomer($this->getCustomer())->getData());
    }

    public function getCurrentBalanceInPoints() {
        return (int) $this->getPoints();
    }

    public function getBalanceLimit() {

        $limit = Mage::helper('points/config')
                ->getMaximumPointsPerCustomer();

        return (int) $limit;
    }

    public function getCurrentBalanceInCurrency() {
        $result = 0;

        try {
            $result = Mage::getModel('points/api')
                    ->changePointsToMoney(
                    $this->getPoints(), $this->getCustomer(), $this->getWebsite()
            );

            if ($result > 0)
                $result = Mage::helper('core')->formatPrice($result);
        } catch (Exception $e) {

            Mage::helper('brainvirecore/logger')->log($this, Mage::helper('points')->__('Unable to convert points into currency to show up in summary section of user account page.'), Brainvire_Core_Model_Logger::LOG_SEVERITY_WARNING, $e->getMessage(), $e->getLine());
        }

        return $result;
    }

}

?>
