<?php
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$setup->startSetup();

$setup->run("
	ALTER TABLE sales_flat_quote_item ADD COLUMN store_code varchar(255) default NULL;
");

$setup->endSetup();
