<?php


class Rofra_Salesorderitemgrid_Block_Adminhtml_Order_Items_Grid_Renderer_Transferhub
extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract{


public function render(Varien_Object $row)
    {
         $rule = Mage::helper('amrolepermissions')->currentRule();
         $sCustomRoles = $rule->getCustomRole()[0];
        $rowItemId = $row->getId();
        $iStoreCollections = Mage::getModel('sales/order_item')->load($rowItemId);
        $sStoreStatusResult = $iStoreCollections->getItemStatus();
        $aAcceptedStatus = array('store_accepted' , 'out_for_hub' , 'rejected_by_hub'); // Required status of the Order Items to fullfill the further steps (as store_accepted , out_for_hub , rejected_by_hub).

        $istransfertohublink = Mage::getStoreConfig('omniapis_section/custom_settings/transfertohublink');

            if($istransfertohublink == '0'){
                return $html = '';
            }
        if(in_array($sStoreStatusResult , $aAcceptedStatus)){

            $storecodeofacceptedstores = Mage::getSingleton('sales/order_item')->load($rowItemId)->getData('store_1');
        $StoreHubsId = Mage::getSingleton('iksula_storemanager/storehubrelation')->load($storecodeofacceptedstores , 'store_id')->getHubId();

        if($StoreHubsId == '0'){
            return $html = "";
        }

            if($sStoreStatusResult == 'out_for_hub'){  // Show the Accept and Reject link to the Hub if the order status id Out for Hub.

                if(strtolower($sCustomRoles) == 'hub'){ // If the Role login is HUB .

                    return $html = '<a onclick="acceptRejectByHub('.$rowItemId.', 1 , 0)">Accept</a>|<a onclick="acceptRejectByHub('.$rowItemId.', 0 ,1)">Reject</a>';

                }

                $hub_id = $iStoreCollections->getHubId();
                $HubName = Mage::getModel('core/store_group')->load($hub_id)->getName();


                   return $html = 'Transfer To '.$HubName; // Show a  label of the (Transfer to Name of the Hub)

            }elseif($sStoreStatusResult == 'rejected_by_hub' || $sStoreStatusResult == 'store_accepted'){ // Show the Transfer to Hub link if the Status of the Order is Store_accepted and Rejected By hub

                return $html = '<a onclick="transfertohub('.$rowItemId.')">Transfer To Hub</a>';
            }

    	}else{
    		return $html = '';
    	}

    }


}
