<?php


class Iksula_Storetype_Block_Adminhtml_Storetype extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_storetype";
	$this->_blockGroup = "storetype";
	$this->_headerText = Mage::helper("storetype")->__("Storetype Manager");
	$this->_addButtonLabel = Mage::helper("storetype")->__("Add New Item");
	parent::__construct();
	
	}

}