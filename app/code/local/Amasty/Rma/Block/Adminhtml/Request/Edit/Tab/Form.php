<?php
class Amasty_RMA_Block_Adminhtml_Request_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("orderitemsstatestatus_form", array("legend"=>Mage::helper("orderitemsstatestatus")->__("Item information")));


						 $fieldset->addField('type', 'select', array(
						'label'     => Mage::helper('orderitemsstatestatus')->__('Type'),
						'values'   => Iksula_Orderitemsstatestatus_Block_Adminhtml_Orderitemsstatestatus_Grid::getValueArray0(),
						'name' => 'type',
						));
						$fieldset->addField("code", "text", array(
						"label" => Mage::helper("orderitemsstatestatus")->__("Code"),
						"name" => "code",
						));

						$fieldset->addField("title", "text", array(
						"label" => Mage::helper("orderitemsstatestatus")->__("Title"),
						"name" => "title",
						));

						$fieldset->addField("event_driven", "select", array(
						"label" => Mage::helper("orderitemsstatestatus")->__("Event Driven"),
						'values'   => Iksula_Orderitemsstatestatus_Block_Adminhtml_Orderitemsstatestatus_Grid::getValueArrayevntdriven(),
						"name" => "event_driven",
						));


						$fieldset->addField("system_required", "select", array(
						"label" => Mage::helper("orderitemsstatestatus")->__("System Required"),
						'values'   => Iksula_Orderitemsstatestatus_Block_Adminhtml_Orderitemsstatestatus_Grid::getValueArraysystemrequired(),
						"name" => "system_required",
						));


				if (Mage::getSingleton("adminhtml/session")->getOrderitemsstatestatusData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getOrderitemsstatestatusData());
					Mage::getSingleton("adminhtml/session")->setOrderitemsstatestatusData(null);
				}
				elseif(Mage::registry("orderitemsstatestatus_data")) {
				    $form->setValues(Mage::registry("orderitemsstatestatus_data")->getData());
				}
				return parent::_prepareForm();
		}
}
