<?php
/**
* Brainvire Infotech Pvt. Ltd
  * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */


class Brainvire_Points_Model_Rule_Condition_Product extends Mage_CatalogRule_Model_Rule_Condition_Product {

    protected function _addSpecialAttributes(array &$attributes) {
        parent::_addSpecialAttributes($attributes);
        $helper = Mage::helper('points');
        $attributes['quote_item_qty'] = $helper->__('Quantity in cart');
        $attributes['quote_item_price'] = $helper->__('Price in cart');
        $attributes['quote_item_row_total'] = $helper->__('Row total in cart');
    }

    
    public function validate(Varien_Object $object) {
        $product = Mage::getModel('catalog/product')
                ->load($object->getProductId())
                ->setQuoteItemQty($object->getQty())
                ->setQuoteItemPrice($object->getPrice())
                ->setQuoteItemRowTotal($object->getRowTotal());

        return parent::validate($product);
    }

}
