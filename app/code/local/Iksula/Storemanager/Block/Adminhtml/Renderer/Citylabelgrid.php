<?php
class Iksula_Storemanager_Block_Adminhtml_Renderer_Citylabelgrid extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{


    public function render(Varien_Object $row)
    {

        $cityid =  $row->getData($this->getColumn()->getIndex());

        $CityObj = Mage::getModel('romcity/romcity')->load($cityid);
        $html = $CityObj->getCityname();
        return $html ;
    }




}
