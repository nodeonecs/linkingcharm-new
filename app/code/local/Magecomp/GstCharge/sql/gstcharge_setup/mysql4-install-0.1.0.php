<?php
/* @var $installer Mage_Sales_Model_Mysql4_Setup */
$installer = $this;

$installer->startSetup();

$installer->addAttribute('quote_address', 'cgst_charge', array('type' => 'decimal'));
$installer->addAttribute('quote_address', 'base_cgst_charge', array('type' => 'decimal'));

$installer->addAttribute('order', 'cgst_charge', array('type' => 'decimal'));
$installer->addAttribute('order', 'base_cgst_charge', array('type' => 'decimal'));

$installer->addAttribute('invoice', 'cgst_charge', array('type' => 'decimal'));
$installer->addAttribute('invoice', 'base_cgst_charge', array('type' => 'decimal'));

$installer->addAttribute('creditmemo', 'cgst_charge', array('type' => 'decimal'));
$installer->addAttribute('creditmemo', 'base_cgst_charge', array('type' => 'decimal'));

$installer->endSetup();