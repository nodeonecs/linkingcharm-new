<?php
/**
 * Iksula_Storemanager extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Iksula
 * @package        Iksula_Storemanager
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Store comment admin edit tabs
 *
 * @category    Iksula
 * @package     Iksula_Storemanager
 * @author      Ultimate Module Creator
 */
class Iksula_Storemanager_Block_Adminhtml_Store_Comment_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Initialize Tabs
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('store_comment_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('iksula_storemanager')->__('Store Comment'));
    }

    /**
     * before render html
     *
     * @access protected
     * @return Iksula_Storemanager_Block_Adminhtml_Store_Edit_Tabs
     * @author Ultimate Module Creator
     */
    protected function _beforeToHtml()
    {
        $this->addTab(
            'form_store_comment',
            array(
                'label'   => Mage::helper('iksula_storemanager')->__('Store comment'),
                'title'   => Mage::helper('iksula_storemanager')->__('Store comment'),
                'content' => $this->getLayout()->createBlock(
                    'iksula_storemanager/adminhtml_store_comment_edit_tab_form'
                )
                ->toHtml(),
            )
        );
        if (!Mage::app()->isSingleStoreMode()) {
            $this->addTab(
                'form_store_store_comment',
                array(
                    'label'   => Mage::helper('iksula_storemanager')->__('Store views'),
                    'title'   => Mage::helper('iksula_storemanager')->__('Store views'),
                    'content' => $this->getLayout()->createBlock(
                        'iksula_storemanager/adminhtml_store_comment_edit_tab_stores'
                    )
                    ->toHtml(),
                )
            );
        }
        return parent::_beforeToHtml();
    }

    /**
     * Retrieve comment
     *
     * @access public
     * @return Iksula_Storemanager_Model_Store_Comment
     * @author Ultimate Module Creator
     */
    public function getComment()
    {
        return Mage::registry('current_comment');
    }
}
