<?php
/**
 * Iksula_Storemanager extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category       Iksula
 * @package        Iksula_Storemanager
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * meta information tab
 *
 * @category    Iksula
 * @package     Iksula_Storemanager
 * @author      Ultimate Module Creator
 */
class Iksula_Storemanager_Block_Adminhtml_Store_Edit_Tab_User extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * prepare the form
     *
     * @access protected
     * @return Iksula_Storemanager_Block_Adminhtml_Store_Edit_Tab_Meta
     * @author Ultimate Module Creator
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setFieldNameSuffix('user');
        $this->setForm($form);
        $fieldset = $form->addFieldset(
            'store_meta_form',
            array('legend' => Mage::helper('iksula_storemanager')->__('Meta information'))
        );


        /*$fieldset->addField(
            'username',
            'text',
            array(
                'label' => Mage::helper('iksula_storemanager')->__('User Name'),
                'name'  => 'username',
                'required'  => true,
            )
        );*/
        $fieldset->addField(
            'firstname',
            'text',
            array(
                'name'      => 'firstname',
                'label'     => Mage::helper('iksula_storemanager')->__('First Name'),
              )
        );

        $fieldset->addField(
            'lastname',
            'text',
            array(
                'name'      => 'lastname',
                'label'     => Mage::helper('iksula_storemanager')->__('Last Name'),
              )
        );

        /*$fieldset->addField(
            'email',
            'text',
            array(
                'name'      => 'email',
                'label'     => Mage::helper('iksula_storemanager')->__('Email Id'),
              )
        );*/

        $fieldset->addField(
            'password',
            'password',
            array(
                'name'      => 'password',
                'label'     => Mage::helper('iksula_storemanager')->__('Password'),
                 'class'     => 'validate-admin-password',
                'required' => true
              )
        );
        $fieldset->addField(
            'password_confirm',
            'password',
            array(
                'name'      => 'password_confirm',
                'label'     => Mage::helper('iksula_storemanager')->__('Password Confirmation'),
                'class'     => 'validate-cpassword',
                'required' => true
              )
        );
       /* $fieldset->addField(
            'is_active',
            'select',
            array(
                'name'      => 'is_active',
                'label'     => Mage::helper('iksula_storemanager')->__('This account is '),
                'values' => array(
                                '0' =>'No',
                                '1' => 'Yes',
                            )
            )
        );*/
        /*$fieldset->addField(
            'meta_keywords',
            'textarea',
            array(
                'name'      => 'meta_keywords',
                'label'     => Mage::helper('iksula_storemanager')->__('Meta-keywords'),
            )
        );*/
        $form->addValues(Mage::registry('current_store')->getData());
        return parent::_prepareForm();
    }
}
