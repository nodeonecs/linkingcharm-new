<?php
/**
 * Iksula_Storemanager extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Iksula
 * @package        Iksula_Storemanager
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Store list block
 *
 * @category    Iksula
 * @package     Iksula_Storemanager
 * @author Ultimate Module Creator
 */
class Iksula_Storemanager_Block_Store_List extends Mage_Core_Block_Template
{
    /**
     * initialize
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function _construct()
    {
        parent::_construct();
        $stores = Mage::getResourceModel('iksula_storemanager/store_collection')
                         ->addStoreFilter(Mage::app()->getStore())
                         ->addFieldToFilter('status', 1);
        $stores->setOrder('store_type_id', 'asc');
        $this->setStores($stores);
    }

    /**
     * prepare the layout
     *
     * @access protected
     * @return Iksula_Storemanager_Block_Store_List
     * @author Ultimate Module Creator
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $pager = $this->getLayout()->createBlock(
            'page/html_pager',
            'iksula_storemanager.store.html.pager'
        )
        ->setCollection($this->getStores());
        $this->setChild('pager', $pager);
        $this->getStores()->load();
        return $this;
    }

    /**
     * get the pager html
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
}
