<?php
/**
 * @category    Graphic Sourcecode
 * @package     Rofra_Salesorderitemgrid
 * @license     http://www.apache.org/licenses/LICENSE-2.0
 * @author      Rodolphe Franceschi <rodolphe.franceschi@gmail.com>
 */

$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$setup->startSetup();

/*$setup->run("
INSERT INTO assignedstatus ( assignedstatus_id, status ) VALUES
( '1', 'allocated' ), ( '2', 'unallocated' ), ( '3', 'accepted' ), ( '4', 'rejected' )
");
*/
$setup->endSetup();
