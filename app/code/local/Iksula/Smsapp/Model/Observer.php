<?php

class Iksula_Smsapp_Model_Observer {

	public function smsOrderSuccess(Varien_Event_Observer $observer) {

		$smsHelper 		 = Mage::helper('smsapp/data');
		$order 	 		 = $observer->getOrder();
		$oderId          = $order->getId();
		$incrementId     = $order->getIncrementId();
		$storeId 		 = $order->getStoreId();
		$brand 			 = $this->getWebsiteName($order->getStoreId());
		$baseUrl 		 = Mage::getBaseUrl();
		$msg 			 = '';
		$payType		 = '';
		$orderSuccessMsg = Mage::getStoreConfig('smsapp/sms_routesm/order_success', $storeId);
		$billing_address = $order->getBillingAddress();
		$mob 			 = $billing_address->getTelephone();
		$cusName 		 = $billing_address->getName();
		if($orderSuccessMsg == 1){
			$quoteId = $order->getQuoteId();
			$quote = Mage::getModel('sales/quote')->load($quoteId);
			$method = $quote->getData('checkout_method');
			if ($method == 'register'){
				$cust_tel = $billing_address->getTelephone();
				$payType = 'customer_signup';
				$smsHelper->getTemplateAndSendSms($payType,$mob,$cusName,null,$brand);
			}
			$payment_method_code = $order->getPayment()->getMethodInstance()->getCode();
			if($payment_method_code == 'cashondelivery' || $payment_method_code == 'phoenix_cashondelivery'){
				$payType = 'cod_pay';
				$smsHelper->getTemplateAndSendSms($payType,$mob,$cusName,$incrementId,$brand);
	        	//$msg = "Dear " . $cusName.', we received your order '. $incrementId . '. You will receive a confirmation call or email within 3 hrs to verify your orders. Once your details are verified, we will process your order further. Please check your email for further details.';
			}else if($payment_method_code == 'payatstore'){ /*|| $payment_method_code == 'cashpayment'*/
				$payType = 'store_pickup';
				Mage::log('store_pickup',null,'pay.log');
				foreach ($order->getAllItems() as $key => $item) {
					if( $item->getCheckoutMethod() == 'pickable' ){
						$smsHelper->getTemplateAndSendSmsForStorePickup($payType,$mob,$cusName,$incrementId,$brand,$item->getStoreId(),$item->getStoreName(),$item->getPickupEndDate(),$item->getSku());
					}
				}
			}
			else{
				$payType = 'prepaid_pay';
				$smsHelper->getTemplateAndSendSms($payType,$mob,$cusName,$incrementId,$brand);
	        	//$msg = "Dear " . $cusName.', thanks for placing order no '. $incrementId . '. Once dispatched, it will reach you in 4-7 days. Please check you email for further details.';
			}
		}
		//For COD verification
		$custEmail = $order->getCustomerEmail();
		$custName  = $order->getCustomerName();
		if($custName == "Guest"){
			$custName = $custEmail;
		}
		/*$payment_method_code = $order->getPayment()->getMethodInstance()->getCode();
		if($payment_method_code == 'cashondelivery' || $payment_method_code == 'phoenix_cashondelivery' || 
			$payment_method_code == 'cashpayment' || $payment_method_code == 'payatstore'){

			if($payment_method_code == 'cashondelivery' || $payment_method_code == 'phoenix_cashondelivery'){
				$email_subject = 'Cash On Delivery';
			}elseif($payment_method_code == 'cashpayment'){
				$email_subject = 'Cash Payment';
			}elseif($payment_method_code == 'payatstore'){
				$email_subject = 'Pay At Store';
			}

			$storeId = Mage::app()->getStore()->getId();
	    			// Mage::log($storeId, null, 'mylogfile.log');
			$templateId = Mage::getStoreConfig('sales_email/order/cod', $storeId);
	    			// Mage::log($templateId, null, 'mylogfilenew.log');
			$code = $incrementId.'/'.time();
			$time = time();
			$url = $baseUrl . 'verification/verification/verify?ver='.$code.'&time='.$time;

			        // Set sender information
			        $senderName = Mage::getStoreConfig('trans_email/ident_support/name');//Getting the Store E-Mail Sender Name.
			        $senderEmail = Mage::getStoreConfig('trans_email/ident_support/email');//Getting the Store General E-Mail.

			        $sender = array('name' => $senderName,
			        	'email' => $senderEmail);


			      	 // Set recepient information
			        $recepientEmail = $custEmail;
			        $recepientName = $custName;    

			        // $receiver = Array($newEmail,$oldEmail);    

			        // Get Store ID
			        

			        // Set variables that can be used in email template
			        $vars = array('custName' => $recepientName,
			        	'customerEmail' => $recepientEmail,
			        	'email_subject' => $email_subject,
			        	'url' => $url);

			        $translate  = Mage::getSingleton('core/translate');

			        // Send Transactional Email
			        try{
			        	Mage::getModel('core/email_template')
			        		->sendTransactional($templateId, $sender, $recepientEmail, $recepientName, $vars, $storeId);
			        	}catch(Exception $e){
			        		Mage::log('observer error found in SMSAPP');
			        	}

			        $translate->setTranslateInline(true);
			    }*/
			}

			public function onSalesOrderPlaceAfter(Varien_Event_Observer $observer) {

				$order = $observer->getOrder();
				/*get device details*/
				$u_agent = $_SERVER['HTTP_USER_AGENT'];
				$bname = 'Unknown';
				$platform = 'Unknown';
				$version= "";
				if (preg_match('/linux/i', $u_agent))
					$platform = 'linux';
				elseif (preg_match('/macintosh|mac os x/i', $u_agent))
					$platform = 'mac';
				elseif (preg_match('/windows|win32/i', $u_agent))
					$platform = 'windows';
				if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)){
					$bname = 'Internet Explorer';
					$ub = "MSIE";
				}
				elseif(preg_match('/Firefox/i',$u_agent)){
					$bname = 'Mozilla Firefox';
					$ub = "Firefox";
				}
				elseif(preg_match('/Chrome/i',$u_agent)){
					$bname = 'Google Chrome';
					$ub = "Chrome";
				}
				elseif(preg_match('/Safari/i',$u_agent)){
					$bname = 'Apple Safari';
					$ub = "Safari";
				}
				elseif(preg_match('/Opera/i',$u_agent)){
					$bname = 'Opera';
					$ub = "Opera";
				}
				elseif(preg_match('/Netscape/i',$u_agent)){
					$bname = 'Netscape';
					$ub = "Netscape";
				}
				$known = array('Version', $ub, 'other');
				$pattern = '#(?<browser>' . join('|', $known) .
					')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
if (!preg_match_all($pattern, $u_agent, $matches)) {
	        // we have no matching number just continue
}
$i = count($matches['browser']);
if ($i != 1) {
	if (strripos($u_agent,"Version") < strripos($u_agent,$ub))
		$version= $matches['version'][0];
	else
		$version= $matches['version'][1];
}
else
	$version= $matches['version'][0];
if ($version==null || $version=="") {$version="?";}
$tablet_browser = 0;
$mobile_browser = 0;

if (preg_match('/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($u_agent))) {
	$tablet_browser++;
}

if (preg_match('/(up.browser|up.link|mmp|BB|symbian|smartphone|midp|wap|phone|android|iemobile)/i', strtolower($u_agent))) {
	$mobile_browser++;
}

if ((strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') > 0) or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE'])))) {
	$mobile_browser++;
}

$mobile_ua = strtolower(substr($u_agent, 0, 4));
$mobile_agents = array(
	'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',
	'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
	'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
	'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
	'newt','noki','palm','pana','pant','phil','play','port','prox',
	'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
	'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',
	'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
	'wapr','webc','winw','winw','xda ','xda-');

if (in_array($mobile_ua,$mobile_agents)) {
	$mobile_browser++;
}

if (strpos(strtolower($u_agent),'opera mini') > 0) {
	$mobile_browser++;
		    //Check for tablets on opera mini alternative headers
	$stock_ua = strtolower(isset($_SERVER['HTTP_X_OPERAMINI_PHONE_UA'])?$_SERVER['HTTP_X_OPERAMINI_PHONE_UA']:(isset($_SERVER['HTTP_DEVICE_STOCK_UA'])?$_SERVER['HTTP_DEVICE_STOCK_UA']:''));
	if (preg_match('/(tablet|ipad|playbook)|(android(?!.*mobile))/i', $stock_ua)) {
		$tablet_browser++;
	}
}

if ($tablet_browser > 0) {$devicename = 'Tablet';}
else if ($mobile_browser > 0) {$devicename = 'Mobile';}
else {$devicename = 'Desktop';}
	    // $name= array(
	    //     'name'      => $bname,
	    //     'version'   => $version,
	    //     'platform'  => $platform,
	    //     'pattern'    => $pattern
	    // );

$mobilename = explode("(",$_SERVER['HTTP_USER_AGENT']);
	$mobilename = explode(")",$mobilename[1]);

	$name = $devicename." - ".$mobilename[0]." - ".$bname."/".$version;
	/*get device details*/
	if($name != ''){
		$order->setOrderdevice($name)->save();
	}

}
public function smsOrderCancel(Varien_Event_Observer $observer) {

	$smsHelper 		 = Mage::helper('smsapp/data');
	$order 	 		 = $observer->getOrder();
	$storeId 		 = $order->getStoreId();
	$incrementId     = $order->getIncrementId();
	$brand 			 = $this->getWebsiteName($order->getStoreId());
	$orderSuccessMsg = Mage::getStoreConfig('smsapp/sms_routesm/order_cancel', $storeId);
	if($orderSuccessMsg == 1){
		$billing_address = $order->getBillingAddress();
		$cusName 		 = $billing_address->getName();
		$mob 			 = $billing_address->getTelephone();
		$payType = 'order_cancel';
		$smsHelper->getTemplateAndSendSms($payType,$mob,$cusName,$incrementId,$brand,$storeId);
	        //$msg 			 = "Items in your order " . $incrementId .' with statusquo.com have been cancelled as per your request. Please check email for more details.';
	}
}

public function smsShipmentCreate(Varien_Event_Observer $observer) {

	$smsHelper  	 = Mage::helper('smsapp/data');
	$shipment 		 = $observer->getEvent()->getShipment();
	$order 	  		 = $shipment->getOrder();
	$storeId 		 = $order->getStoreId();
	$incrementId     = $order->getIncrementId();
	$brand 			 = $this->getWebsiteName($order->getStoreId());
	//$orderSuccessMsg = Mage::getStoreConfig('smsapp/sms_routesm/order_shipped', $storeId);
	//if($orderSuccessMsg == 1){
		$billing_address = $order->getBillingAddress();
		$cusName 		 = $billing_address->getName();
		$mob 			 = $billing_address->getTelephone();

        	/*$shipments = $order->getShipmentsCollection();
			$shippedItemNames = array();
			$itemNames = '';
			foreach ($shipments as $shipment) {
		        $shippedItems = $shipment->getItemsCollection();
		        foreach ($shippedItems as $item) {
		                $shippedItemNames[] = $item->getName();
		                $itemNames .= $item->getName().' ,';
		        }
		    }*/
	        $msg = "Your Linking Charms Order No. ".$incrementId." have been shipped.";
	        $sendSms    = $smsHelper->sensSms($msg, $mob);
		    // $payType = 'order_shipped';
		    // $smsHelper->getTemplateAndSendSms($payType,$mob,$cusName,$incrementId,$brand,$storeId);
	//}

		if($sub_order_id = Mage::app()->getRequest()->getParam('sub_order_id')){
			//Mage::helper('statusnotifer')->notifyHubBySMSaboutShipment($sub_order_id,$storeId,$incrementId,$brand);
		}
	}

	public function smsOrderStatusChange(Varien_Event_Observer $observer) {

		$smsHelper 		 	 = Mage::helper('smsapp/data');
		$order 	 		 	 = $observer->getOrder();
		$brand 				 = $this->getWebsiteName($order->getStoreId());
		$storeId 		 	 = $order->getStoreId();
		$incrementId     	 = $order->getIncrementId();
		$orderReturnedMsg 	 = Mage::getStoreConfig('smsapp/sms_routesm/order_returned', $storeId);
		$orderDeliveredMsg   = Mage::getStoreConfig('smsapp/sms_routesm/order_delivered', $storeId);
		$orderCanceledMsg  	 = Mage::getStoreConfig('smsapp/sms_routesm/order_cancel', $storeId);
		$orderVerifyMsg  	 = Mage::getStoreConfig('smsapp/sms_routesm/order_verify', $storeId);
		$status 		 	 = $order->getStatus();
		$billing_address 	 = $order->getBillingAddress();
		$cusName 		 	 = $billing_address->getName();
		$mob 			 	 = $billing_address->getTelephone();
		$payment_method_code = $order->getPayment()->getMethodInstance()->getCode();

		if($orderReturnedMsg == 1 && $status == 'returned'){
			$payType = 'order_returned';
			$smsHelper->getTemplateAndSendSms($payType,$mob,$cusName,$incrementId,$brand);
		}

		if($orderDeliveredMsg == 1 && ($status == 'order_delivered' || $status == 'delivered') ){
			$payType = 'order_delivered';
			$smsHelper->getTemplateAndSendSms($payType,$mob,$cusName,$incrementId,$brand);
		}

		/*if($orderCanceledMsg == 1 && $status == 'canceled'){
			$payType = 'order_cancel';
	        $smsHelper->getTemplateAndSendSms($payType,$mob,$cusName,$incrementId,$brand);
	    }*/

	    if(($orderVerifyMsg == 1 && $status == 'processing') && ($payment_method_code == 'cashondelivery' || $payment_method_code == 'phoenix_cashondelivery')){
	    	$payType = 'order_verify';
	    	$smsHelper->getTemplateAndSendSms($payType,$mob,$cusName,$incrementId,$brand);
	    }
	}
	// Returns website for store id provided.
	public function getWebsiteName($id){

		$StoreModel = Mage::getModel('core/store')->load($id);
		$websiteId  = $StoreModel->getWebsiteId();
		$website 	= Mage::getModel('core/website')->load($websiteId);
		$webName 	= $website->getName();
		return $webName;
	}
}
