<?php

Class Iksula_Storemanager_Model_Hubunassigned extends Mage_Core_Model_Abstract {


	public function unassignedStoreFromHub($storetohubid){

		$StoresCollections = Mage::getModel('core/store_group')->getCollection()
												->addFieldToFilter('group_id' , array('neq' => $storetohubid))
												->addFieldToSelect(array('store_assigned' , 'group_id'))
												;


		foreach($StoresCollections as $Stores){

			$sStoresAssigned = $Stores->getStoreAssigned();
			$iStoreId = $Stores->getGroupId();

			$aStoresAssigned = explode(',' , $sStoresAssigned);

			if (($key = array_search($storetohubid, $aStoresAssigned)) !== false) {
 			   unset($aStoresAssigned[$key]);
 			   $sStoresAssignedNew = implode(',' , $aStoresAssigned);


				$data = array('store_assigned'=> $sStoresAssigned);
				$StoresModel = Mage::getModel('core/store_group')->load($iStoreId)->addData($data);

				try {
						$StoresModel->setId($iStoreId)->save();
						//echo "Data updated successfully.";

					} catch (Exception $e){
						echo $e->getMessage();
					}
			}

		}



	}



}
