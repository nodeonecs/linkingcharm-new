<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   BV
 * @package    BV_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */


class Brainvire_Core_Helper_Extension extends Varien_Object {

    
    public function isExtensionInstalled($code) {
        $exts = $this->getInstalledExtensions();
        return (isset($exts[$code]));
    }

    
    public function isExtensionActive($code) {
        if ($this->isExtensionInstalled($code)) {
            $exts = $this->getInstalledExtensions();
            return (bool) $exts[$code]['active'];
        }
    }

    
    public function getInstalledExtensions() {
        if (!$this->getData('installed_extensions')) {
            $exts = array();
            $modules = ((array) Mage::getConfig()->getNode('modules')->children());
            foreach ($modules as $k => $Module) {
                $exts[$k] = (array) $Module;
            }
            $this->setData('installed_extensions', $exts);
        }
        return $this->getData('installed_extensions');
    }

}