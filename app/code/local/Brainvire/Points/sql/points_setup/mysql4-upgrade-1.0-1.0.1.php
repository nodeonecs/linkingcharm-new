<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */



$resource = Mage::getSingleton('core/resource');
$connWrite = $resource->getConnection('log_write');
$connRead = $resource->getConnection('log_read');

$summaryTable = $resource->getTableName("points/summary");
$select = $connRead->select();
$select->from($summaryTable, array('id'))->where('customer_id = ?', 0);
$problemSummaryId = $connRead->fetchOne($select);

if ($problemSummaryId) {
    $sql = sprintf("DELETE FROM %s WHERE customer_id = 0", $summaryTable);
    $connWrite->query($sql);

    $transactionsTable = $resource->getTableName("points/transaction");
    $sql = sprintf("DELETE FROM %s WHERE summary_id = %s", $transactionsTable, $problemSummaryId);
    $connWrite->query($sql);
}