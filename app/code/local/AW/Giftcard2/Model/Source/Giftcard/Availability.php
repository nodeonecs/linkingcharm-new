<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_Model_Source_Giftcard_Availability
    implements AW_Giftcard2_Model_Source_SourceInterface
{
    const ACTIVE_VALUE        = 1;
    const EXPIRED_VALUE       = 2;
    const USED_VALUE          = 3;
    const DEACTIVATED_VALUE   = 4;

    const ACTIVE_LABEL       = 'Active';
    const EXPIRED_LABEL      = 'Expired';
    const USED_LABEL         = 'Used';
    const DEACTIVATED_LABEL  = 'Deactivated';

    const EXPIRED_ERROR_MESSAGE  = 'This card has expired';
    const USED_ERROR_MESSAGE     = 'The balance on this card is 0';
    const DEACTIVATED_ERROR_MESSAGE = 'This card has been deactivated';
    const DEFAULT_ERROR_MESSAGE  = 'Gift Card Code is not valid.';

    /**
     * @return array
     */
    public function toArray()
    {
        $helper = Mage::helper('aw_giftcard2');
        return array(
            self::ACTIVE_VALUE          => $helper->__(self::ACTIVE_LABEL),
            self::EXPIRED_VALUE         => $helper->__(self::EXPIRED_LABEL),
            self::USED_VALUE            => $helper->__(self::USED_LABEL),
            self::DEACTIVATED_VALUE     => $helper->__(self::DEACTIVATED_LABEL),
        );
    }

    public function getOptionText($value)
    {
        $options = $this->toArray();
        if (array_key_exists($value, $options)) {
            return $options[$value];
        }
        return null;
    }

    /**
     * Retrieves error message by value
     *
     * @param int $value
     * @return string
     */
    public function getErrorMessage($value)
    {
        $map = array (
            self::EXPIRED_VALUE         => self::EXPIRED_ERROR_MESSAGE,
            self::USED_VALUE            => self::USED_ERROR_MESSAGE,
            self::DEACTIVATED_VALUE     => self::DEACTIVATED_ERROR_MESSAGE
        );
        return isset($map[$value]) ? $map[$value] : self::DEFAULT_ERROR_MESSAGE;
    }
}