<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category   Mage
 * @package    Mage_Secureebs
 * @copyright  Copyright (c) 2008 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Redirect to Secureebs
 *
 * @category    Mage
 * @package     Mage_Secureebs
 * @name        Mage_Secureebs_Block_Standard_Redirect
 * @author      Magento Core Team <core@magentocommerce.com>
 */

class Mage_PayuMoney_Block_Standard_Redirect extends Mage_Core_Block_Abstract
{

    protected function _toHtml()
    {
        $standard = Mage::getModel('payuMoney/standard');
        $form = new Varien_Data_Form();
        $form->setAction($standard->getSecureebsUrl())
            ->setId('payuMoney_standard_checkout')
            ->setName('payuMoney_standard_checkout')
            ->setMethod('POST')
            ->setUseContainer(true);

        $formArray = $standard->setOrder($this->getOrder())->getStandardCheckoutFormFields();
        $orderId   = $formArray['order_id'];
        $amount    = $formArray['product_price'];
        $fname     = $formArray['f_name'];
        $lname     = $formArray['s_name'];
        $email     = $formArray['email'];
        $phone     = $formArray['phone'];
        $city      = $formArray['city'];
        $desc      = $formArray['product_desc'];
        $sUrl      = Mage::getUrl('payuMoney/standard/success', array('_secure' => true));
        $fUrl      = Mage::getUrl('payuMoney/standard/failure', array('_secure' => true));
        $pg        = 'wallet';
        $bCode     = 'payuw';
        $mId   = Mage::getSingleton('payuMoney/config')->getAccountId();
        $salt  = Mage::getSingleton('payuMoney/config')->getSecretKey();

		$hash =  $mId . '|' . $orderId . '|' . $amount . '|' . $desc . '|' .  $fname . '|' . $email .'|||||||||||'.$salt;
		$secure_hash = hash('sha512',$hash);

        $form->addField('key', 'hidden', array('name'=>'key', 'value'=>$mId));
        $form->addField('hash', 'hidden', array('name'=>'hash', 'value'=>$secure_hash));
        $form->addField('txnid', 'hidden', array('name'=>'txnid', 'value'=>$orderId));
        $form->addField('amount', 'hidden', array('name'=>'amount', 'value'=>$amount));
        $form->addField('email', 'hidden', array('name'=>'email', 'value'=>$email));
        $form->addField('productinfo', 'hidden', array('name'=>'productinfo', 'value'=>$desc));
        $form->addField('firstname', 'hidden', array('name'=>'firstname', 'value'=>$fname));
        $form->addField('City', 'hidden', array('name'=>'City', 'value'=>$city));
        $form->addField('phone', 'hidden', array('name'=>'phone', 'value'=>$phone));
        $form->addField('surl', 'hidden', array('name'=>'surl', 'value'=>$sUrl));
        $form->addField('furl', 'hidden', array('name'=>'furl', 'value'=>$fUrl));
		$form->addField('bankcode','hidden',array('name'=>'bankcode','value'=>$bCode));
		$form->addField('pg','hidden',array('name'=>'pg','value'=>$pg));

        $html = '<html><body>';
        $html.= $this->__('Please be patient. We are processing your request.');
        $html.= $form->toHtml();
        $html.= '<script type="text/javascript">document.getElementById("payuMoney_standard_checkout").submit();</script>';
        $html.= '</body></html>';

        return $html;
	}
}
