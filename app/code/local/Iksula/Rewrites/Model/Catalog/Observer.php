<?php

class Iksula_Rewrites_Model_Catalog_Observer extends Mage_Catalog_Model_Observer
{

    protected function _addCategoriesToMenu($categories, $parentCategoryNode, $menuBlock, $addTags = false)
    {
        $categoryModel = Mage::getModel('catalog/category');
        $attribute = Mage::getSingleton('eav/config')->getAttribute('catalog_category', 'cat_menu');
        $labels = array();
        if ($attribute->usesSource()) {
            $options = $attribute->getSource()->getAllOptions(false);
            foreach ($options as $option) {
                $labels[$option['value']] = strtolower($option['label']);
            }
        }        
        foreach ($categories as $category) {
            if (!$category->getIsActive()) {
                continue;
            }

            $nodeId = 'category-node-' . $category->getId();

            $categoryModel->setId($category->getId());
            if ($addTags) {
                $menuBlock->addModelTags($categoryModel);
            }

            $tree = $parentCategoryNode->getTree();

            $singleCategory = Mage::getModel('catalog/category')->load($category->getId());
            
            $categoryData = array(
                'name' => $category->getName(),
                'id' => $nodeId,
                'url' => Mage::helper('catalog/category')->getCategoryUrl($category),
                'cat_menu' => $singleCategory->getCatMenu(),
                'menu_img' => $singleCategory->getMenuImg(),
                'display_position' => $singleCategory->getDisplayPosition(),
                'is_active' => $this->_isActiveMenuCategory($category)
            );
           
            $categoryNode = new Varien_Data_Tree_Node($categoryData, 'id', $tree, $parentCategoryNode);
            $parentCategoryNode->addChild($categoryNode);

            $flatHelper = Mage::helper('catalog/category_flat');
            if ($flatHelper->isEnabled() && $flatHelper->isBuilt(true)) {
                $subcategories = (array)$category->getChildrenNodes();
            } else {
                $subcategories = $category->getChildren();
            }

            $this->_addCategoriesToMenu($subcategories, $categoryNode, $menuBlock, $addTags);
        }
    }
}
