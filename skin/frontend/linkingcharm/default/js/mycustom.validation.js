
var validation_name;
//var all_zip_codes;
//var zip_codes_received = false;
var mobile_match = true;
jQuery(document).ready(function($){

    //var all_customer_data = {};
    var mobileno;
    var phone;

   /* $.ajax({
        url: 'index/getAllCustomers/',
        type:'post',
        dataType: 'json',
        success: function(cust_data){
            all_customer_data = cust_data;
        }
    });*/

    /*function autofillajax(customer_zip){
        var zip_length = customer_zip.toString().length;
        var zip_status = false;
        $.each(all_zip_codes, function(key, value) {
            if(value['postcode'] == customer_zip){
                zip_status = true;
                validate_email = true;
                $('.checkout_error_zip').remove();
                $('#billing\\:postcode').css('border','1px solid #B6B6B6');
                $('.checkout_city').val(value['city']);
                $('.checkout_state').val(value['region_id']);
            }
        });

        if(zip_status == false && zip_length > 0){
            validate_email = false;
            var pin_advice = '<div class="custom_validator checkout_error_zip validation-advice">Invalid pincode. Please enter a valid pincode</div>';
            if(!$('.checkout_error_zip').length){
                $('.checkout_postcode').after(pin_advice);
                $('#billing\\:postcode').css('border','1px solid #e9322d');

            }
        }
    }*/

    function validate_name(field,field_name,error_name){
        if(field == null || field == undefined || field == ''){
            var name_advice = '<div class="custom_validator checkout_error_name">Please enter your ' + error_name +'</div>';
            if(!$('.error_' + field_name).length){
                $('#billing\\:' + field_name).after(name_advice);
                $('#billing\\:' + field_name).css('border','1px solid #e9322d');
                $('.checkout_error_name').addClass('error_' + field_name);
                validation_name = 0;
            }
        }
        else{
            $('#billing\\:' + field_name).css('border','1px solid #B6B6B6');
            $('.error_' + field_name).remove();
            validation_name = 1;
        }
    }

    function validate_address(field,field_name){
        if(field == null || field == undefined || field == ''){
            var addr_advice = '<div class="custom_validator checkout_error_address">Please enter your full address </div>';
             if(!$('.error_' + field_name).length){
                $('#billing\\:' + field_name).after(addr_advice);
                $('#billing\\:' + field_name).css('border','1px solid #e9322d');
                $('.checkout_error_address').addClass('error_' + field_name);
                validation_addr = 0;
            }
        }
        else{
            $('#billing\\:' + field_name).css('border','1px solid #B6B6B6');
            $('.error_' + field_name).remove();
            validation_addr = 1;
        }
    }

    function validate_telephone_data(field,field_name){

        var mobile = parseInt(field);
        if(field.length ==0){
            $('.error_' + field_name).remove();
            var tel_advice = '<div class="custom_validator checkout_error_tel">Please enter Mobile No. </div>';
             if(!$('.error_' + field_name).length){
                $('#billing\\:' + field_name).after(tel_advice);
                $('#billing\\:' + field_name).css('border','1px solid #e9322d');
                $('.checkout_error_tel').addClass('error_' + field_name);
                validation_tel = 0;
            }
        }

        else if(field.length < 10 || isNaN(mobile) ){
             $('.error_' + field_name).remove();
            var tel_advice = '<div class="custom_validator checkout_error_tel">Please enter valid mobile no </div>';
             if(!$('.error_' + field_name).length){
                $('#billing\\:' + field_name).after(tel_advice);
                $('#billing\\:' + field_name).css('border','1px solid #e9322d');
                $('.checkout_error_tel').addClass('error_' + field_name);
                validation_tel = 0;
            }
        }
        else{
            mobile_match = true;
            $('#billing\\:' + field_name).css('border','1px solid #B6B6B6');
            $('.error_' + field_name).remove();
            validation_tel = 1;

            /*mobileno = $('#billing\\:' + field_name).val();
            $.each(all_customer_data,function(key,value){
                mobile_new = value['mobile'];
                phone = value['telephone'];
                if($.trim(mobile_new) == $.trim(mobileno)){
                    mobile_match = false;
                }
                var tele = phone.toString().split(",");
                $.each(tele,function(key,value){

                var val_phone = value;
                //alert(val_phone);
                if(mobileno != '') { 
                    if($.trim(val_phone) == $.trim(mobileno))
                        {   
                            mobile_match = false;
                        }
                    }
                });
            });*/

            /*if(mobile_match == false){
                var tel_advice = '<div class="custom_validator checkout_error_tel">Mobile number already exists.</div>';
                if(!$('.error_' + field_name).length){
                    $('#billing\\:' + field_name).after(tel_advice);
                    $('#billing\\:' + field_name).css('border','1px solid #e9322d');
                    $('.checkout_error_tel').addClass('error_' + field_name);
                    validation_tel = 0;
                }  
            }*/
        }
    }

    function validate_checkout_card_name(field,field_name,error_name,checkout_field_id){
        if(field == null || field == undefined || field == ''){
            var card_advice = '<div class="custom_validator checkout_error_card">Please enter ' + error_name +'</div>';
            if(!$('.error_' + checkout_field_id).length){
                $('#' + checkout_field_id).after(card_advice);
                $('#' + checkout_field_id).css('border','1px solid #e9322d');
                $('.checkout_error_card').addClass('error_' + checkout_field_id);
                validation_card = 0;
            }
        }
        else{
            $('#' + checkout_field_id).css('border','1px solid #B6B6B6');
            $('.error_' + checkout_field_id).remove();
            validation_card = 1;
        }
    }

    function validate_checkout_card_num(field,field_name,error_name,checkout_field_id){
        if(field.length < 16){
            var card_num_advice = '<div class="custom_validator checkout_card_num_error">Please enter' + error_name + ' digit card number</div>';
             if(!$('.error_' + checkout_field_id).length){
                $('#' + checkout_field_id).after(card_num_advice);
                $('#' + checkout_field_id).css('border','1px solid #e9322d');
                $('.checkout_card_num_error').addClass('error_' + checkout_field_id);
                validation_card_num = 0;
            }
        }
        else{
            $('#' + checkout_field_id).css('border','1px solid #B6B6B6');
            $('.error_' + checkout_field_id).remove();
            validation_card_num = 1;
        }
    }

    function validate_checkout_card_cvv(field,field_name,error_name,checkout_field_id){
        if(field.length < 3){
            var card_num_advice = '<div class="custom_validator checkout_card_cvv_error">Please enter appropriate cvv</div>';
             if(!$('.error_' + checkout_field_id).length){
                $('#' + checkout_field_id).after(card_num_advice);
                $('#' + checkout_field_id).css('border','1px solid #e9322d');
                $('.checkout_card_cvv_error').addClass('error_' + checkout_field_id);
                validation_cvv_num = 0;
            }
        }
        else{
            $('#' + checkout_field_id).css('border','1px solid #B6B6B6');
            $('.error_' + checkout_field_id).remove();
            validation_cvv_num = 1;
        }
    }

    function validate_city_data(field,field_name){
        if(field == null || field == undefined || field == ''){
            var city_advice = '<div class="custom_validator checkout_error_city">Please enter city </div>';
             if(!$('.error_' + field_name).length){
                $('#billing\\:' + field_name).after(city_advice);
                $('#billing\\:' + field_name).css('border','1px solid #e9322d');
                $('.checkout_error_city').addClass('error_' + field_name);
                validation_city = 0;
            }
        }
        else{
            $('#billing\\:' + field_name).css('border','1px solid #B6B6B6');
            $('.error_' + field_name).remove();
            validation_city = 1;
        }
    }

    $('.checkout_postcode').focusout(function(){
        var customer_zip = $('.checkout_postcode').val();   
        var zip_length = customer_zip.toString().length;
        var zip_status = false;
            $.ajax({
                method: "POST", 
                dataType: 'json',
                url: baseurl+'onepagecheckout/index/pincode',
                data:{zipcode:customer_zip},
                success: function(response){
                 // all_zip_codes = response;
                    //autofillajax(customer_zip);
                    if(response.status == 'success'){
                        $('.checkout_city').val(response.city);
                        $('#billing\\:region').val(response.state);
                        $('#billing\\:region_id').val(response.region_id);
                         zip_status = true;
                         validate_email = true;
                         $('.custom_validator').remove();
                         $('#billing\\:postcode').css('border','1px solid #b6b6b6 ');

                    }
                     else if(customer_zip!=''){
                            $('.checkout_city').val('');
                            $('#billing\\:region').val('');
                            if(zip_status == false && zip_length > 0){
                            validate_email = false;
                            var pin_advice = '<div class="custom_validator checkout_error_zip">Invalid pincode. Please enter a valid pincode</div>';
                                if(!$('.checkout_error_zip').length){
                                    $('.checkout_postcode').after(pin_advice);
                                    $('#billing\\:postcode').css('border','1px solid #e9322d');
                                }
                            }
                        }
                    else if(customer_zip==''){
                            $('.checkout_city').val('');
                            $('#billing\\:region').val('');
                    }
                }
            });
    });

   $('#billing\\:firstname').focusout(function(){
        var field_name = 'firstname';
        var error_name = 'first name';
        var validate_fname = $('#billing\\:firstname').val();
        validate_name(validate_fname,field_name,error_name);
   });

   $('#billing\\:lastname').focusout(function(){
        var field_name = 'lastname';
        var error_name = 'last name';
        var validate_lname = $('#billing\\:lastname').val();
        validate_name(validate_lname,field_name,error_name);
   });

   $('#billing\\:street1').focusout(function(){
        var field_name = 'street1';
        var validate_street1 = $('#billing\\:street1').val();
        validate_address(validate_street1,field_name);
   });

   /*$('#billing\\:telephone').focusout(function(){
        var field_name = 'telephone';
        var validate_telephone = $('#billing\\:telephone').val();
        validate_telephone_data(validate_telephone,field_name);
   });*/

   $('#billing\\:city').focusout(function(){
        var field_name = 'city';
        var validate_city = $('#billing\\:city').val();
        validate_city_data(validate_city,field_name);
   });

   $('#secureebs_standard_cc_owner, #payudebit_standard_cc_owner').live('focusout',function(){
        var field_name = 'cardname';
        var checkout_field_id = $(this).attr('id');
        var error_name = 'name on card';
        var validate_cname = $('#' + checkout_field_id).val();
        validate_checkout_card_name(validate_cname,field_name,error_name,checkout_field_id);  
    });

   $('#payudebit_standard_cc_number, #secureebs_standard_cc_number').live('focusout',function(){
        var field_name = 'cardnum';
        var checkout_field_id = $(this).attr('id');
        var error_name = '16';
        var validate_dname = $('#' + checkout_field_id).val();
        validate_checkout_card_num(validate_dname,field_name,error_name,checkout_field_id);
    });

   $('#payudebit_standard_cc_cid, #secureebs_standard_cc_cid').live('focusout',function(){
        var field_name = 'cardcvv';
        var checkout_field_id = $(this).attr('id');
        var error_name = 'cvv';
        var validate_cvv = $('#' + checkout_field_id).val();
        validate_checkout_card_cvv(validate_cvv,field_name,error_name,checkout_field_id);
    });

});