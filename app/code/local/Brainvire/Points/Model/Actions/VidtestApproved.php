<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   Brainvire
 * @package    Brainvire_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * @license    
 */


class Brainvire_Points_Model_Actions_VidtestApproved extends Brainvire_Points_Model_Actions_Abstract {

    protected $_action = 'vidtest_approved';
    protected $_comment = 'Video Testimonial upload';

    protected function _applyLimitations($amount) {

        $obj = $this->getObjectForAction();

        $pointLimitForAction = Mage::helper('points/config')
                ->getPointsLimitForVideoTestimonial($obj->getStoreId());

        $collection = Mage::getModel('points/transaction')
                ->getCollection()
                ->addFieldToFilter('summary_id', $this->getSummary()->getId())
                ->addFieldToFilter('action', $this->getAction())
                ->limitByDay(Mage::getModel('core/date')->gmtTimestamp());

       
        $summ = 0;
        foreach ($collection as $transaction) {
            $summ += $transaction->getBalanceChange();
        }

        return parent::_applyLimitations($this->_calculateNewAmount($summ, $amount, $pointLimitForAction));
    }

}