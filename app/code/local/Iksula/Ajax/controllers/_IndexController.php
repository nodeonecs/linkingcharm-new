<?php
require_once 'Mage/Checkout/controllers/CartController.php';
class Iksula_Ajax_IndexController extends Mage_Checkout_CartController
{

	public function indexAction(){
		$this->loadLayout();
		$this->renderLayout();
	}

	public function addAction()
	{
		$cart   = $this->_getCart();
		$params = $this->getRequest()->getParams();
		if($params['isAjax'] == 1){
			$response = array();
			try {
				if (isset($params['qty'])) {
					$filter = new Zend_Filter_LocalizedToNormalized(
					array('locale' => Mage::app()->getLocale()->getLocaleCode())
					);
					$params['qty'] = $filter->filter($params['qty']);
				}

				$product = $this->_initProduct();
				$related = $this->getRequest()->getParam('related_product');

				/**
				 * Check product availability
				 */
				if (!$product) {
					$response['status'] = 'ERROR';
					$response['message'] = $this->__('Unable to find Product ID');
				}

				$cart->addProduct($product, $params);
				if (!empty($related)) {
					$cart->addProductsByIds(explode(',', $related));
				}

				$cart->save();

				$this->_getSession()->setCartWasUpdated(true);

				/**
				 * @todo remove wishlist observer processAddToCart
				 */
				Mage::dispatchEvent('checkout_cart_add_product_complete',
				array('product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse())
				);

				if (!$cart->getQuote()->getHasError()){
					$message = $this->__('%s was added to your shopping cart.', Mage::helper('core')->htmlEscape($product->getName()));
					$response['status'] = 'SUCCESS';
					$response['message'] = $message;
					//New Code Here
					$this->loadLayout();					
					$toplink = $this->getLayout()->getBlock('top.links')->toHtml();
					$sidebar_block = $this->getLayout()->getBlock('cart_sidebar');
					Mage::register('referrer_url', $this->_getRefererUrl());
					$sidebar = $sidebar_block->toHtml();
					$response['toplink'] = $toplink;
					$response['sidebar'] = $sidebar;
				}
			} catch (Mage_Core_Exception $e) {
				$msg = "";
				if ($this->_getSession()->getUseNotice(true)) {
					$msg = $e->getMessage();
				} else {
					$messages = array_unique(explode("\n", $e->getMessage()));
					foreach ($messages as $message) {
						$msg .= $message.'<br/>';
					}
				}

				$response['status'] = 'ERROR';
				$response['message'] = $msg;
			} catch (Exception $e) {
				$response['status'] = 'ERROR';
				$response['message'] = $this->__('Cannot add the item to shopping cart.');
				Mage::logException($e);
			}
			$this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
			return;
		}else{
			return parent::addAction();
		}
	}
	
	public function deleteAction()
	{	

		$id = (int) $this->getRequest()->getParam('id');
		
		if($this->getRequest()->isXmlHttpRequest()) {
			$id = (int) $this->getRequest()->getParam('id');
			if ($id) {
				try {
					$this->_getCart()->removeItem($id)->save();
			  		$this->_getSession()->setCartWasUpdated(true);
			  		$cart = $this->_getCart();
			  		if (!$cart->getQuote()->getHasError()){
						$response['status'] = 'SUCCESS';
						//New Code Here
						$this->loadLayout();
						$toplink = $this->getLayout()->getBlock('top.links')->toHtml();
						$sidebar_block = $this->getLayout()->getBlock('cart_sidebar');
						Mage::register('referrer_url', $this->_getRefererUrl());
						$sidebar = $sidebar_block->toHtml();
						$response['toplink'] = $toplink;
						$response['sidebar'] = $sidebar;
						$this->_redirectReferer(Mage::getUrl('*/*'));
			  		}
				} catch (Exception $e) {
					$response['status'] = 'ERROR';
					$response['message'] = $this->__('Cannot be deleted from shopping cart.');
					Mage::logException($e);
				}
			}else{
				$response['status'] = 'ERROR';
				$response['message'] = $this->__('Unable to find Product in cart');
			}
			$this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
			return;
		} else {
			return parent::deleteAction();
		}
	} 

	
	public function updatePostAction()
    {	
    	//echo "string";exit;
        if (!$this->_validateFormKey()) {
            $this->_redirect('*/*/');
            return;
        }
        //echo "string";exit;
        $updateAction = (string)$this->getRequest()->getParam('update_cart_action');
        try {
	        switch ($updateAction) {
	            case 'empty_cart':
	                $this->_emptyShoppingCart();
	                break;
	            case 'update_qty':
	                $this->_updateShoppingCart();
	                break;
	            default:
	                $this->_updateShoppingCart();
	        }
	        $this->_getSession()->setCartWasUpdated(true);
			$cart = $this->_getCart();
	        if (!$cart->getQuote()->getHasError()){
				$response['status'] = 'SUCCESS';
				//New Code Here
				$this->loadLayout();
				$toplink = $this->getLayout()->getBlock('top.links')->toHtml();
				$sidebar_block = $this->getLayout()->getBlock('cart_sidebar');
				Mage::register('referrer_url', $this->_getRefererUrl());
				$sidebar = $sidebar_block->toHtml();
				$response['toplink'] = $toplink;
				$response['sidebar'] = $sidebar;

				$cartTotals = $this->getLayout()->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/totals.phtml')->toHtml();
				$response['carttotals'] = $cartTotals;

				

				$itemHtml = '';
				foreach($cartBlock->getItems() as $_item) {
					$itemHtml .= $cartBlock->getItemHtml($_item);
				}
				$response['cartitems'] = $itemHtml;

	  		}
		  	 

	  	}catch (Exception $e) {
			$response['status'] = 'ERROR';
			$response['message'] = $this->__('Cannot be deleted from shopping cart.');
			Mage::logException($e);
		}
		$this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
		return;
    }
    
}