<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   Brainvire
 * @package    Brainvire_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * @license    
 */


class Brainvire_Points_Model_Coupon_Transaction extends Mage_Core_Model_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init('points/coupon_transaction');
    }

    
    public function LoadByCouponIdCustomerId($couponId, $customerId) {
        $this->_getResource()->LoadByCouponIdCustomerId($this, $couponId, $customerId);
        return $this;
    }

}