<?php

Class Rofra_Salesorderitemgrid_Model_Observer {


	function incrementalorderitems($observer){


		    $order = $observer->getEvent()->getOrder();

		    //Mage::log(var_dump($order) , null , 'order_details.log');



	}


	public function addCreateButtonOrderItemsGrid($observer)
		{
			$container = $observer->getBlock();
			 $create_orderurl = Mage::helper("adminhtml")->getUrl("*/sales_order_create/start/");

		    if(null !== $container && $container->getType() == 'salesorderitemgrid/adminhtml_order_items') {
		        $data = array(
		            'label'     => 'Create Order',
		            'class'     => 'create-order',
		            'onclick'   => 'setLocation(\' '  . $create_orderurl . '\')',
		        );
		        $container->addButton('create_order', $data);
		    }

		    return $this;
		}


	/*function changeitemstatus($orderids , $OrderItemsData){


			$ModelOrderStateChange1 = Mage::getModel('sales/order_item')->load($orderids)->addData($OrderItemsData);
                    	$ModelOrderStateChange1->setId($orderids)->save();

	}*/







	function invoicecreation($event){


	/*	echo '<pre>';
		print_r(Mage::app()->getRequest()->getParams());
		exit;*/

		$aOrderItemsParams = Mage::app()->getRequest()->getParam('invoice');
		$iOrderIds = Mage::app()->getRequest()->getParam('order_id');
		$aOrderItemIds = $aOrderItemsParams['items'];

		$oOrderItemsObject = Mage::getModel('sales/order_item');

		foreach($aOrderItemIds as $OrderItemId => $Invoiceqty){

				$QtyOrdered = $oOrderItemsObject->load($OrderItemId)->getQtyOrdered();
				$QtyInvoiced = $oOrderItemsObject->load($OrderItemId)->getQtyInvoiced();



					$aParentObject = $oOrderItemsObject->getCollection()->addFieldToFilter('parent_item_id' , $OrderItemId)
					->addFieldToSelect('item_id');


					if(count($aParentObject) > 0){
						foreach($aParentObject as $parentvalue){

							$aOrderItemsIds = array($OrderItemId , $parentvalue->getItemId());
						}
					}else{
							$aOrderItemsIds = array($OrderItemId);
					}

				if($QtyOrdered > ($QtyInvoiced + $Invoiceqty)){

					$OrderItemsType = 'status';
					$OrderItemsStateStatus = 'invoiced';

					//echo 'Ordered qty is still to Invoiced';

				}else{
					$OrderItemsType = 'status';
					$OrderItemsStateStatus = 'invoiced';
					//echo ' All Ordered qty is Invoiced';
				}

				$aOrderItemsStateStatus = json_decode(Mage::helper('orderitemsstatestatus')->getRelatedItemStatusandState($OrderItemsType , $OrderItemsStateStatus) , true);
				echo $aOrderItemsStateStatus['state'].'<====>'.$OrderItemId;
				//exit;

                    $OrderItemsData = array( 'item_state' => 'test_state' , 'item_status' => 'test_status' );


                    echo '<pre>hshshsh';

                    print_r($aOrderItemsIds);

                    //exit;
                    foreach($aOrderItemsIds as $orderIds){

                    	//changeitemstatus($orderIds , $OrderItemsData);
                    	$orderIds = (int)$orderIds;
                    	//var_dump($orderIds);
                    	//echo "<====>";
                    	//var_dump(999);
                    	Mage::helper('salesorderitemgrid')->changestatusitems($orderIds , $OrderItemsData);


                    	/*echo $orderIds.'<=====>';
                    	$ModelOrderStateChange1 = Mage::getSingleton('sales/order_item')->load(1012)->addData($OrderItemsData);
                    	$ModelOrderStateChange1->setId(1012)->save();
                    	//echo 'before';
                    	//exit;

                        $ModelOrderStateChange2 = Mage::getSingleton('sales/order_item')->load(1011)->addData($OrderItemsData);
                    	//echo 'before';
                    	//exit;
                        $ModelOrderStateChange2->setId(1011)->save();
                        echo 'after';
                        exit;*/

                    }

                    //exit;
                    //sleep(5);

		}



	}

	public function checkInvoiceSubmit(Varien_Event_Observer $observer)
 	{
    	$invoice = $observer->getEvent()->getInvoice();

    	$iLoginuserid = Mage::getSingleton('admin/session')->getUser()->getUserId();
		$LoginUserRole = Mage::getModel('admin/user')->load($iLoginuserid)->getRole()->getRoleId();
			
		$iRole_Collectionsid = Mage::getSingleton('amrolepermissions/rule')
		->getCollection()->addFieldToFilter('role_id' , array('eq' => $LoginUserRole));

		foreach($iRole_Collectionsid as $Role)
		{
		    if(strtolower($Role->getCustomRole()) == 'hub'){
		    	$scopeviews = $Role->getScopeHub();
		    }elseif(strtolower($Role->getCustomRole()) != 's_a'){
		        $scopeviews = $Role->getScopeStoreviews();
		    }
		}
		
  		$entityId = $invoice->getData('entity_id');
		$resource = Mage::getSingleton('core/resource');
		$writeConnection = $resource->getConnection('core_write');
		$table1 = $resource->getTableName('sales/invoice');
		$table2 = $resource->getTableName('sales/invoice_grid');

		$query1 = "UPDATE {$table1} SET store_scopeview = '{$scopeviews}' WHERE entity_id = "
		. (int)$entityId;

		$writeConnection->query($query1);

		$query2 = "UPDATE {$table2} SET store_scopeview = '{$scopeviews}' WHERE entity_id = "
		. (int)$entityId;

		$writeConnection->query($query2);
	}


	public function checkShipmentSubmit(Varien_Event_Observer $observer)
 	{ 		
    	$shipment = $observer->getEvent()->getShipment();

    	$iLoginuserid = Mage::getSingleton('admin/session')->getUser()->getUserId();
		$LoginUserRole = Mage::getModel('admin/user')->load($iLoginuserid)->getRole()->getRoleId();
		
			
		$iRole_Collectionsid = Mage::getSingleton('amrolepermissions/rule')
		->getCollection()->addFieldToFilter('role_id' , array('eq' => $LoginUserRole));

		foreach($iRole_Collectionsid as $Role)
		{
		    if(strtolower($Role->getCustomRole()) == 'hub'){
		    	$scopeviews = $Role->getScopeHub();
		    }elseif(strtolower($Role->getCustomRole()) != 's_a'){
		        $scopeviews = $Role->getScopeStoreviews();
		    }
		}

  		$entityId = $shipment->getData('entity_id');
		$resource = Mage::getSingleton('core/resource');
		$writeConnection = $resource->getConnection('core_write');
		$table1 = $resource->getTableName('sales/shipment');
		$table2 = $resource->getTableName('sales/shipment_grid');

		$order_item_ids = Mage::registry('order_item_ids');
	
		$OrderItemsStateStatus = json_decode(Mage::helper('orderitemsstatestatus')->getRelatedItemStatusandState('status' , 'shipped') , true);

			$orderitemstate = $OrderItemsStateStatus['state'];						
			$orderitemstatus = 'shipped';

		foreach($order_item_ids as $itemsid){
			$orderid = Mage::getModel('sales/order_item')->load($itemsid)->getOrderId();
            $query = "UPDATE sales_flat_order_item SET  item_status = '{$orderitemstatus}' , item_state = '{$orderitemstate}' 
            WHERE item_id = ". (int)$itemsid;

			$writeConnection->query($query);
			if($orderid)
		  		Mage::helper('orderitemsstatestatus')->changeOrderStatusState($orderid);
		}

		$query1 = "UPDATE {$table1} SET store_scopeview = '{$scopeviews}' WHERE entity_id = "
		. (int)$entityId;
		$writeConnection->query($query1);

		$query2 = "UPDATE {$table2} SET store_scopeview = '{$scopeviews}' WHERE entity_id = "
		. (int)$entityId;
		$writeConnection->query($query2);
	}

	/*public function getPickupDate() {
		$storeintime = Mage::getStoreConfig('iksula_storemanager/store/storeintime');
	    $storeouttime = date('Y-m-d').' '.Mage::getStoreConfig('iksula_storemanager/store/storeouttime');
	    $storedispatchstarttime = Mage::getStoreConfig('iksula_storemanager/store/storedispatchstarttime');
    	$storedispatchendtime = Mage::getStoreConfig('iksula_storemanager/store/storedispatchendtime');
	    $displaydate = Mage::getModel('core/date')->date('Y-m-d H:i:s',strtotime('+'.$storedispatchstarttime.'hours'));
	    
	    if($displaydate < $storeouttime) {
	        if($displaydate > date('d-m-Y').' '.$storeintime) {
	            $start_date = Mage::getModel('core/date')->date('d-m-Y H:i:s',strtotime('+'.$storedispatchstarttime.'hours'));
	            $end_date = Mage::getModel('core/date')->date(date('d-m-Y',strtotime($start_date.'+'.$storedispatchendtime.'hours')));    
	        }else {
	            $start_date = Mage::getModel('core/date')->date(date('d-m-Y') .' '.$storeintime);        
	            $end_date = Mage::getModel('core/date')->date(date('d-m-Y',strtotime($start_date.'+'.$storedispatchendtime.'hours')));    
	        }
	    }else{
	        $start_date = Mage::getModel('core/date')->date(date('d-m-Y',strtotime('+1 day')) .' '.$storeintime);    
	        $end_date = Mage::getModel('core/date')->date(date('d-m-Y',strtotime($start_date.'+'.$storedispatchendtime.'hours')));    
	    }

	    $result = array();
	    $result['start_date'] = $start_date;
	    $result['end_date'] = $end_date;

	    return $result;
	}*/

	function placeorder_orderitems_shipping_cod($observer){
		$website_id = Mage::getSingleton('core/website')->load('1' , 'is_default')->getWebsiteId();	

		$_order = $observer->getEvent()->getOrder();
		$ShippingFeeItemsAll = 0 ;
		$CODFeeItemsAll = 0;
		$shipping_rate  = 0 ;
		$cod_fee = 0 ;

		foreach($_order->getAllItems() as $items){ 
			if($items->getCheckoutMethod() == 'pickable') {
				$storeviewid = $items->getStorePickupviewid();

				$storeid = Mage::getSingleton('core/store')->load($storeviewid)->getGroupId(); // Get Store Id By Store View Id .

				/*Mage::getModel('salesorderitemgrid/salesorderallocate')->saveAutoallocateAcceptedOrder($items->getItemId() , $storeviewid);*/
				Mage::getModel('salesorderitemgrid/salesorderallocate')->saveAllocateData($items->getItemId() , $storeid);
				/*$result = $this->getPickupDate();
				$items->setPickupStartDate($result['start_date']);
				$items->setPickupEndDate($result['end_date']);*/
		    	
			}else{

				$items->setStoreName(NULL);

			}
			$items->save();
		}

		Mage::dispatchEvent("custom_triger_email", array('order' => $_order));

		 /*$i = 0;
		   foreach($_order->getAllItems() as $items){

		    	
		    	if($i == 0 || $i == 1){
		    		$items->setCheckoutMethod('pickable');
		    	$items->save();
		    	}elseif($i == 2 || $i == 3){

		    			$items->setCheckoutMethod('deliverable');
		    	$items->save();
		    	}		    	
		    	$i++;
		    }*/

		    foreach($_order->getAllItems() as $items){
		    	
		    	$checkoutMethodType = $items->getCheckoutMethod();
		    	if(strtolower($items->getProductType()) == 'simple' && strtolower($checkoutMethodType) == 'deliverable'){
		    		$Totalitemscounts ++;
		    	}		    	
		    }

		    $lastItemsCount = $Totalitemscounts - 1;
		    $countcheck = 1 ;
		    foreach($_order->getAllItems() as $ItemsObj){

		    	$data = array('item_id' => $ItemsObj->getItemId());
		    	$checkoutMethodType = $ItemsObj->getCheckoutMethod();
				//Mage::log(print_r($data ,  1) , null , 'auto_data.log');
				$autoallocatemodel = Mage::getModel('salesorderitemgrid/autoallocatestatus')->setData($data);
				$insertId = $autoallocatemodel->save()->getId();

		    	if($countcheck <= $lastItemsCount){
		    				$shipping_rate = round($_order->getBaseShippingAmount()/$Totalitemscounts);
		    				$cod_fee = round($_order->getBaseCodFee()/$Totalitemscounts);
		    				$ShippingFeeItemsAll += $shipping_rate;
		                    $CODFeeItemsAll += $cod_fee;
		    	}else{

		    				$shipping_rate = ($_order->getBaseShippingAmount() - $ShippingFeeItemsAll);
		            		$cod_fee = ($_order->getBaseCodFee() - $CODFeeItemsAll);


		    	}

		    	/*if($_order->getBaseShippingAmount() != 0 && strtolower($checkoutMethodType) != 'pickable'){

			    		$ItemsObj->setItemBaseShippingAmount($shipping_rate);
			            $ItemsObj->setItemShippingAmount($shipping_rate);
			            $ItemsObj->setItemShippingInclTax($shipping_rate);
			            $ItemsObj->setItemBaseShippingInclTax($shipping_rate);

		    	}*/

		    	if($_order->getBaseShippingAmount() != 0){


		    		if($ItemsObj->getProductType() == 'configurable'){
		    			$parent_id = $ItemsObj->getItemId();		    			

		    			$Parentcollection = Mage::getModel('sales/order_item')
		    			->getCollection()
		    			->addFieldToSelect('checkout_method')
		    			->addFieldToFilter('parent_item_id' , array('eq' => $parent_id));

		    			foreach($Parentcollection as $values){
		    				$childCheckoutMethod = $values->getCheckoutMethod();
		    				Mage::log($childCheckoutMethod , null , 'childCheckoutMethod.log');
		    				break;
		    			}

		    			if($childCheckoutMethod == 'deliverable'){
		    				Mage::log('set' , null , 'childCheckoutMethod.log');
		    				$ItemsObj->setItemBaseShippingAmount($shipping_rate);
					        $ItemsObj->setItemShippingAmount($shipping_rate);
					        $ItemsObj->setItemShippingInclTax($shipping_rate);
					        $ItemsObj->setItemBaseShippingInclTax($shipping_rate);					        

		    			}else{
		    				$ItemsObj->setCheckoutMethod('pickable');	
		    			}

		    		}elseif($ItemsObj->getProductType() == 'simple' && $ItemsObj->getCheckoutMethod() == 'deliverable'){
		    				Mage::log('set1' , null , 'childCheckoutMethod.log');
		    				$ItemsObj->setItemBaseShippingAmount($shipping_rate);
					        $ItemsObj->setItemShippingAmount($shipping_rate);
					        $ItemsObj->setItemShippingInclTax($shipping_rate);
					        $ItemsObj->setItemBaseShippingInclTax($shipping_rate);	
		    		}			    		

		    	}


		    	/*if($_order->getCodFee() != 0 && strtolower($checkoutMethodType) != 'pickable'){

			        $ItemsObj->setItemCodFee($cod_fee);
			        $ItemsObj->setItemBaseCodFee($cod_fee);

		    	}*/


		    	if($_order->getCodFee() != 0){

		    		if($ItemsObj->getProductType() == 'configurable'){
		    			$parent_id = $ItemsObj->getItemId();		    			

		    			$Parentcollection = Mage::getModel('sales/order_item')
		    			->getCollection()
		    			->addFieldToSelect('checkout_method')
		    			->addFieldToFilter('parent_item_id' , array('eq' => $parent_id));

		    			foreach($Parentcollection as $values){
		    				$childCheckoutMethod = $values->getCheckoutMethod();
		    				Mage::log($childCheckoutMethod , null , 'childCheckoutMethodCOD.log');
		    				break;
		    			}

		    			if($childCheckoutMethod == 'deliverable'){
		    				Mage::log('set1' , null , 'childCheckoutMethodCOD.log');
		    				$ItemsObj->setItemCodFee($cod_fee);
			        		$ItemsObj->setItemBaseCodFee($cod_fee);

		    			}

		    		}elseif($ItemsObj->getProductType() == 'simple' && $ItemsObj->getCheckoutMethod() == 'deliverable'){
		    				Mage::log('set2' , null , 'childCheckoutMethodCOD.log');
		    				$ItemsObj->setItemCodFee($cod_fee);
			        		$ItemsObj->setItemBaseCodFee($cod_fee);
		    		}

		    	}

		    	$countcheck ++;
		    	$ItemsObj->setWebsiteId($website_id);

		    	$ItemsObj->save();

		    }

	}

	/*function orderitems_autoallocate($observer){

		$_order = $observer->getEvent()->getOrder();
		$Shipping_address = $_order->getShippingAddress();
    	//$shippingAddress = Mage::getModel('sales/order_address');


			$city_name = $Shipping_address->getCity();
			//Mage::log($city_name , null , 'city_name.log');

			foreach($_order->getAllItems() as $items){

				$data = array('item_id' => $items->getItemId());
				//Mage::log(print_r($data ,  1) , null , 'auto_data.log');
				$autoallocatemodel = Mage::getModel('salesorderitemgrid/autoallocatestatus')->setData($data);
				$insertId = $autoallocatemodel->save()->getId();
				//Mage::log($insertId , null , 'auto_data.log');
				$aStoresCitywiseListing = Mage::helper('salesorderitemgrid')->citywisestorelisting($items->getItemId() , $city_name);
				if(count($aStoresCitywiseListing) > 0 ){
						$item_status_update = 'store_allocated';

					}else{
						$item_status_update = 'store_unallocated';
					}
				$message = Mage::helper('salesorderitemgrid')->autoallocatetostores($aStoresCitywiseListing
					, $items->getItemId());

				$sItemStatus =  json_decode(Mage::helper('orderitemsstatestatus')->getRelatedItemStatusandState('status' , $item_status_update) , true);
	            $data = array('item_status'=> $item_status_update,'item_state'=>$sItemStatus['state']);
	            $ItemsIdmodel = Mage::getModel('sales/order_item')->load($items->getItemId())->addData($data);
	                $ItemsIdmodel->setId($items->getItemId())->save();
			}
	}*/

}
