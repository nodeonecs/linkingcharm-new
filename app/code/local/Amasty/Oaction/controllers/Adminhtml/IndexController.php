<?php
/**
* @author Amasty Team
* @copyright Copyright (c) 2010-2011 Amasty (http://www.amasty.com)
* @package Amasty_Oaction
*/
class Amasty_Oaction_Adminhtml_IndexController extends Mage_Adminhtml_Controller_Action
{


    protected function _isAllowed()
    {
           return Mage::getSingleton('admin/session')
        ->isAllowed('system/config/amoaction');

    }


    public function doorderAction()
    {
        $ids         = $this->getRequest()->getParam('order_ids');
        $val         = trim($this->getRequest()->getParam('amoaction_value'));
        $commandType = trim($this->getRequest()->getParam('command'));


                try {

                    if($val == 'processing' || $val == 'canceled'){

                        foreach($ids as $OrderIds){

                            $OrderObj = Mage::getModel('sales/order')->load($OrderIds);
                            $sItemState =  json_decode(Mage::helper('orderitemsstatestatus')->getRelatedItemStatusandState($commandType , $val) , true);

                           /* echo '<pre>';
                            print_r($sItemState);
                            exit;*/
                            
                            foreach($OrderObj->getAllItems() as $OrderItemsObj){
                                $changestatusflag = false;
                                $OrderItemsIds = $OrderItemsObj->getItemId();
                                $checkouttype = $OrderItemsObj->getCheckoutMethod();
                                if(strtolower($checkouttype) == 'deliverable'){
                                    $changestatusflag = true;
                                    $OrderItemsStatusStatedata = array('item_status'=> $val,'item_state'=>$sItemState['state']);
                                
                                }elseif(strtolower($checkouttype) == 'pickable'){
                                    if($val == 'processing'){
                                        $changestatusflag = false;
                                        /*$OrderItemsStatusStatedata = array('item_status'=> 'pickup_processing','item_state'=>'pickup_processing');    */
                                    }elseif($val == 'canceled'){
                                        $changestatusflag = true;
                                        $OrderItemsStatusStatedata = array('item_status'=> $val,'item_state'=>$sItemState['state']);
                                    }

                                }else{
                                      $changestatusflag = true;
                                    $OrderItemsStatusStatedata = array('item_status'=> $val,'item_state'=>$sItemState['state']);
                                }
                                /*echo '<pre>';
                                print_r($OrderItemsStatusStatedata);*/
                                //exit;
                                if($changestatusflag){
                                    $model = Mage::getModel('sales/order_item')->load($OrderItemsIds)->addData($OrderItemsStatusStatedata);    
                                    try {
                                            /* Code to save PO data  */
                                            $poData = Mage::getModel('omniapis/storepo')->getCollection()->addFieldToFilter('order_id',array('eq'=>$OrderIds))->addFieldToFilter('sub_orderid',array('eq'=>$OrderItemsIds))->getData();
                                            foreach ($poData as $pod) {
                                                Mage::getModel('omniapis/storepo')->load($pod['id'])->setStatus($val)->save();
                                            }
                                            /* Code to save PO data  */

                                        $model->setId($OrderItemsIds)->save();
                                    } catch (Exception $e){
                                        Mage::getSingleton('core/session')->addError($e->getMessage());
                                    }
                                }
                            }

                            //exit;

                        }

                    }


                    $command = Amasty_Oaction_Model_Command_Abstract::factory($commandType);

                    $success = $command->execute($ids, $val);
                    if ($success){
                         $this->_getSession()->addSuccess($success);
                    }

                    // show non critical errors to the user
                    foreach ($command->getErrors() as $err){
                         $this->_getSession()->addError($err);
                    }
                }
                catch (Exception $e) {
                    $this->_getSession()->addError($this->__('Error: %s', $e->getMessage()));
                }

                //exit;


            $this->_redirect('adminhtml/sales_order');
            return $this;
    }


    public function doorderitemsAction()
    {
        $ids         = $this->getRequest()->getParam('order_ids');
        $val         = trim($this->getRequest()->getParam('amoaction_value'));
        $commandType = trim($this->getRequest()->getParam('command'));


        if($val == 'processing'){

            foreach($ids as $key => $ItemValue){
                $OrderitemsObj = Mage::getModel('sales/order_item')->load($ItemValue);
                $OrdersId = $OrderitemsObj->getOrderId();
                $checkouttype = $OrderitemsObj->getCheckoutMethod();

                if($checkouttype == 'pickable'){
                    $valUpdate = 'store_pickup_accepted';
                }else{
                    $valUpdate = 'processing';
                }

                
                $sItemState =  json_decode(Mage::helper('orderitemsstatestatus')->getRelatedItemStatusandState($commandType , $valUpdate) , true);                
                $data = array('item_status'=> $valUpdate,'item_state'=>$sItemState['state']);              

                $model = Mage::getModel('sales/order_item')->load($ItemValue)->addData($data);
                try {
                    $model->setId($ItemValue)->save();
                    Mage::helper('orderitemsstatestatus')->changeOrderStatusState($OrdersId);
                    Mage::getSingleton('core/session')->addSuccess('Order Item Status Updated Successfull');

                } catch (Exception $e){
                    //echo $e->getMessage();
                    Mage::getSingleton('core/session')->addError($e->getMessage());
                }
            }
        }

          $this->_redirect('adminhtml/order_items');
            return $this;

      }
}
