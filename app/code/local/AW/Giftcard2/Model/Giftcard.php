<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_Model_Giftcard extends Mage_Core_Model_Abstract
{
    protected $_historyCollection = null;

    public function _construct()
    {
        parent::_construct();
        $this->_init('aw_giftcard2/giftcard');
    }

    public function loadByCode($code)
    {
        return $this->load($code, 'code');
    }

    /**
     *  Returns history collection for current giftcard
     *
     *  @return AW_Giftcard2_Model_Resource_Giftcard_History_Collection
     */
    public function getHistoryCollection()
    {
        if ($this->_historyCollection === null) {
            $this->_historyCollection = Mage::getModel('aw_giftcard2/giftcard_history')
                                            ->getCollection()
                                            ->addGiftcardFilter($this->getId())
            ;
        }
        return $this->_historyCollection;
    }

    /**
     * Attach text labels to gift card
     *
     * @return Mage_Core_Model_Abstract
     */
    protected function _afterLoad()
    {
        $this
            ->_attachAvailabilityText()
            ->_attachTypeText()
            ->_attachIsUsedText()
        ;
        return parent::_afterLoad();
    }

    /**
     * Attach gift card availability label to the model
     *
     * @return $this
     */
    protected function _attachAvailabilityText()
    {
        $availabilityLabel = Mage::getModel('aw_giftcard2/source_giftcard_availability')->getOptionText($this->getAvailability());
        if (null !== $availabilityLabel) {
            $this->setAvailabilityText($availabilityLabel);
        }
        return $this;
    }

    /**
     * Attach gift card type label to the model
     *
     * @return $this
     */
    protected function _attachTypeText()
    {
        $typeLabel = Mage::getModel('aw_giftcard2/source_entity_attribute_giftcard_type')->getOptionText($this->getType());
        if (null !== $typeLabel) {
            $this->setTypeText($typeLabel);
        }
        return $this;
    }

    /**
     * Attach gift card is used label to the model
     *
     * @return $this
     */
    protected function _attachIsUsedText()
    {
        $isUsedLabel = Mage::getModel('aw_giftcard2/source_giftcard_used')->getOptionText($this->getIsUsed());
        if (null !== $isUsedLabel) {
            $this->setIsUsedText($isUsedLabel);
        }
        return $this;
    }

    /**
     * Check if gift card is physical
     *
     * @return bool
     */
    public function isPhysical()
    {
        return $this->getType() == AW_Giftcard2_Model_Source_Entity_Attribute_Giftcard_Type::PHYSICAL_VALUE;
    }

    /**
     * Check if gift card is expired
     *
     * @return bool
     */
    public function isExpired()
    {
        $currentDate = Mage::app()->getLocale()->date(null, Varien_Date::DATE_INTERNAL_FORMAT, null, false);
        $expirationDate = Mage::app()->getLocale()
            ->date($this->getExpireAt(), Varien_Date::DATE_INTERNAL_FORMAT, null, false)
        ;

        if ($expirationDate->compare($currentDate) == -1) {
            return true;
        }
        return false;
    }

    /**
     * Check if gift card is used completely
     *
     * @return bool
     */
    public function isUsed()
    {
        return !($this->getBalance() > 0);
    }

    /**
     * Check if gift card is partially used
     *
     * @return bool
     */
    public function isPartiallyUsed()
    {
        return !$this->isUsed() && ($this->getBalance() != $this->getInitialBalance());
    }

    /**
     * Check if gift card is deactivated
     *
     * @return bool
     */
    public function isDeactivated()
    {
        return $this->getAvailability() == AW_Giftcard2_Model_Source_Giftcard_Availability::DEACTIVATED_VALUE;
    }

    /**
     * Check if gift card is active
     *
     * @return bool
     */
    public function isActive()
    {
        return $this->getAvailability() == AW_Giftcard2_Model_Source_Giftcard_Availability::ACTIVE_VALUE;
    }

    /**
     * Fill CreatedAt and ExpiredAt fields before save gift card
     *
     * @return Mage_Core_Model_Abstract
     * @throws Mage_Core_Exception
     */
    protected function _beforeSave()
    {
        $_result = parent::_beforeSave();

        if ($this->getId() == null) {
            $this->setIsNew(true);
            $expireAfter = '';
            $data = $this->getData();

            if (isset($data['expire_after'])) {
                $expireAfter = $data['expire_after'];
            }

            if (!$this->getIsImported()) {
                $currentDate = Mage::app()->getLocale()->date();
                $this->setCreatedAt($currentDate->toString(Varien_Date::DATE_INTERNAL_FORMAT));
            }

            if ($expireAfter && is_numeric($expireAfter) && $expireAfter > 0) {
                $currentDate->addDay($expireAfter);
                $this->setExpireAt($currentDate->toString(Varien_Date::DATE_INTERNAL_FORMAT));
            }
        }
        return $_result;
    }

    /**
     * Register history event after gift card save
     *
     * @return Mage_Core_Model_Abstract
     */
    protected function _afterSave()
    {
        $_result = parent::_afterSave();
        $this->_registerHistory();
        return $_result;
    }

    /**
     * Register history event
     *
     * @return $this
     */
    protected function _registerHistory()
    {
        /** @var $giftCardHistory AW_Giftcard2_Model_Giftcard_History */
        $giftCardHistory = Mage::getModel('aw_giftcard2/giftcard_history');

        if ($this->getIsNew()) {
            $giftCardHistory->registerCreateAction($this);
        }

        if (!$this->getIsNew() && $this->_balanceReduced() && $this->getOrder() !== null) {
            if ($this->isPartiallyUsed()) {
                $giftCardHistory->registerPartiallyUsedAction($this);
            } else {
                $giftCardHistory->registerUsedAction($this);
            }
        }
        if (!$this->getIsNew() && $this->getCreditmemo() !== null) {
            $giftCardHistory->registerDeactivatedAction($this);
        }

        if (!$this->getIsNew() && $this->_balanceChanged()) {
            if (null === $this->getOrder()) {
                $giftCardHistory->registerUpdatedAction($this);
            }
        }

        if ($this->getOrigData('availability') && $this->getAvailability() == AW_Giftcard2_Model_Source_Giftcard_Availability::EXPIRED_VALUE) {
            $giftCardHistory->registerExpiredAction($this);
        }
        return $this;
    }

    /**
     * Check if gift card balance is reduced
     *
     * @return bool
     */
    protected function _balanceReduced () {
        return $this->getOrigData('balance') > $this->getBalance();
    }

    /**
     * Check if gift card balance is changed
     *
     * @return bool
     */
    protected function _balanceChanged () {
        return $this->getOrigData('balance') != $this->getBalance();
    }

    /**
     * @param null $storeId
     * @throws Mage_Core_Exception
     */
    public function validate($storeId = null)
    {
        $giftcardAvailability = Mage::getModel('aw_giftcard2/source_giftcard_availability');
        if (
            $this->getAvailability() != AW_Giftcard2_Model_Source_Giftcard_Availability::ACTIVE_VALUE
        ) {
            throw new Mage_Core_Exception(
                Mage::helper('aw_giftcard2')->__($giftcardAvailability->getErrorMessage($this->getAvailability()))
            );
        }

        $websiteId = $storeId ?
            Mage::app()->getStore($storeId)->getWebsiteId() :
            Mage::app()->getWebsite()->getId();
        ;
        if (!$this->_isGiftcardCanBeAppliedOnWebsite($websiteId)) {
            throw new Mage_Core_Exception(
                Mage::helper('aw_giftcard2')->__(AW_Giftcard2_Model_Source_Giftcard_Availability::DEFAULT_ERROR_MESSAGE)
            );
        }
    }

    private function _isGiftcardCanBeAppliedOnWebsite($websiteId)
    {
        $result = false;
        $giftcardScope = Mage::helper('aw_giftcard2/config')->getGiftcardScope();
        switch ($giftcardScope) {
            case AW_Giftcard2_Model_Source_Giftcard_Scope::SINGLE_WEBSITE :
                $result = ($websiteId == $this->getWebsiteId());
                break;
            case AW_Giftcard2_Model_Source_Giftcard_Scope::WEBSITES_WITH_THE_SAME_CURRENCY :
                $currentBaseCurrencyCode = Mage::app()->getWebsite($websiteId)->getBaseCurrencyCode();
                $giftcardBaseCurrencyCode = Mage::app()->getWebsite($this->getWebsiteId())->getBaseCurrencyCode();
                $result = (strcasecmp($currentBaseCurrencyCode, $giftcardBaseCurrencyCode) == 0);
                break;
        }
        return $result;
    }

    /**
     * @param null $storeId
     * @return bool
     */
    public function isValidForRedeem($storeId = null)
    {
        try {
            $this->validate($storeId);
            return true;
        } catch (Mage_Core_Exception $e) {
        }
        return false;
    }

    /**
     * Activate gift card if it is deactivated
     *
     * @return $this
     * @throws Mage_Core_Exception
     */
    public function activate()
    {
        if ($this->getAvailability() == AW_Giftcard2_Model_Source_Giftcard_Availability::DEACTIVATED_VALUE) {
            $this
                ->setAvailability(AW_Giftcard2_Model_Source_Giftcard_Availability::ACTIVE_VALUE)
                ->save()
            ;
        } else {
            throw new Mage_Core_Exception(
                Mage::helper('aw_giftcard2')->__('Unable to activate gift card code %s.', $this->getCode())
            );
        }
        return $this;
    }

    /**
     * Deactivate gift card if it is active
     *
     * @return $this
     * @throws Mage_Core_Exception
     */
    public function deactivate()
    {
        if ($this->getAvailability() == AW_Giftcard2_Model_Source_Giftcard_Availability::ACTIVE_VALUE) {
            $this
                ->setAvailability(AW_Giftcard2_Model_Source_Giftcard_Availability::DEACTIVATED_VALUE)
                ->save()
            ;
        } else {
            throw new Mage_Core_Exception(
                Mage::helper('aw_giftcard2')->__('Unable to deactivate gift card code %s.', $this->getCode())
            );
        }
        return $this;
    }

    /**
     * Refund gift card
     *
     * @return $this
     * @throws Mage_Core_Exception
     */
    public function refund($creditmemo = null)
    {
        if ($creditmemo) {
            $this->setCreditmemo($creditmemo);
        }

        $this
            ->setAvailability(AW_Giftcard2_Model_Source_Giftcard_Availability::DEACTIVATED_VALUE)
            ->setBalance(0)
            ->save()
        ;
        return $this;
    }
}
