<?php
class Incom_Replacementattribute_Model_Observer
{
    public function saveReplacementAttributes($observer)
    {
    	if($replacementFor = Mage::app()->getRequest()->getParam('replacementFor')){
            Mage::register('replacementFor',$replacementFor);
            unset($replacementFor);
        }
    } 
}
