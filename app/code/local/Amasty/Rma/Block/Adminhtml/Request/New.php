<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2015 Amasty (https://www.amasty.com)
 * @package Amasty_Rma
 */
class Amasty_Rma_Block_Adminhtml_Request_New extends Mage_Adminhtml_Block_Widget_Form_Container
{
    protected $_rma_request;

    protected function _getRmaRequest(){
        if (!$this->_rma_request){
            $this->_rma_request = Mage::registry('amrma_request');
        }
        return $this->_rma_request;
    }

    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id'; 
        $this->_blockGroup = 'amrma';
        $this->_controller = 'adminhtml_request';
        
        // $this->_addButton('save_and_continue', array(
        //         'label'     => $this->__('Save and Continue Edit'),
        //         'onclick'   => 'saveAndContinueEdit()',
        //         'class' => 'save'
        //     ), 10);
        
        $allow = $this->_getRmaRequest()->getAllowCreateLabel() == 1;
        
        // $this->_addButton('allow_print_label', array(
        //         'label'     => $this->__(($allow ? 'Remove' : 'Generate').' Shipping Label'),
        //         'onclick'   => 'sllowPrintLabel();',
        //         'style' => 'margin-left: 30px;'
        //     ), 10);
        
        
        $this->_formScripts[] = " function saveAndContinueEdit(){ editForm.submit($('edit_form').action + 'continue/edit') } ";
        
        // $allowUrl = Mage::helper("adminhtml")->getUrl("adminhtml/amrma_request/allow", array(
        //     'id' => $this->_getRmaRequest()->getId()
        // ));
                
        // $this->_formScripts[] = " function sllowPrintLabel() {document.location.href= '".$allowUrl."'}";
        $this->_formScripts[] = " function loadDataUsingEmail(orderid,websiteId) {
             new Ajax.Request('". Mage::helper('adminhtml')->getUrl('adminhtml/amrma_request/extractCustomerOrderByEmail')."', {
              method: 'post',
              parameters: {orderid:orderid,websiteid:websiteId},
              onSuccess: successFunc,
              onFailure:  failureFunc
              })
        }";
        $this->_formScripts[] = ' function successFunc(response){ response = JSON.parse(response.responseText);
            jQuery("#sku").val("");
            jQuery("#name").val("");
            jQuery("#sales_item_id").val("");
            jQuery("#product_id").val("");
            jQuery("#order_id").val("");
            jQuery("#itemselect option").each(function() {
                jQuery(this).remove();
            });
            jQuery("#qty option").each(function() {
                jQuery(this).remove();
            });
            jQuery("#customer_id").val(response["entity_id"]);
            jQuery("#customer_firstname").val(response["firstname"]);
            jQuery("#customer_lastname").val(response["lastname"]);
            jQuery("#customer_phone").val(response["customer_phone"]);
            jQuery("#email").val(response["email"]);
            if(response["order"]){
                jQuery.each(response["order"],function(index,value){
                    jQuery("#itemselect").append("<option data-qty="+value.qtyDropDown+" data-orderid="+value.orderid+"  data-productid="+value.prod_id+" data-sku="+value.sku+" data-name=\'"+value.name+"\' value="+index+">"+ value.name +" - "+ value.sku +"</option>")
                })
            }else{
                alert("Invalid Order Id. Please make sure if the Order is shipped OR Order is already returned.");
            }
        } ';
        $this->_formScripts[] = " function failureFunc(response){ alert('try again'); } ";
        $this->_formScripts[] = " function sendOtpSMS(actualOtp,sku,customerPhone){ 
            if(sku && actualOtp && customerPhone){
                new Ajax.Request('". Mage::helper('adminhtml')->getUrl('adminhtml/amrma_request/sendOtp')."', {
                  method: 'post',
                  parameters: {actualOtp:actualOtp,sku:sku,customerPhone:customerPhone},
                  onSuccess: successFuncOtp,
                  onFailure:  failureFuncOtp
                  })
            }else{
                alert( 'Please make sure Customer Phone and Sku field is filled with proper value before generating the OTP.' );
            }
         } ";
         $this->_formScripts[] = ' function successFuncOtp(response){ response = JSON.parse(response.responseText); 
            alert(response);
         }';

         $this->_formScripts[] = ' function failureFuncOtp(response){ alert("OTP Error"); 

         }';
         $this->_formScripts[] = ' function verifyOtpfn(actualOtp,enteredOtp){ 
            if(enteredOtp == actualOtp){
                alert("OTP verified successfully. Click on Save button on top to save the Return Request. ");
            }else{
                alert("Please enter valid OTP");
                jQuery("#enterOtp").focus();
            }
         }';
        $this->_formScripts[] = " function loadProductFields(){ 
            jQuery('#sku').val(jQuery('#itemselect option:selected').attr('data-sku'));
            jQuery('#name').val(jQuery('#itemselect option:selected').attr('data-name'));
            jQuery('#sales_item_id').val(jQuery('#itemselect option:selected').val());
            jQuery('#product_id').val(jQuery('#itemselect option:selected').attr('data-productid'));
            jQuery('#order_id').val(jQuery('#itemselect option:selected').attr('data-orderid'));
            if(jQuery('#itemselect option:selected').attr('data-qty') > 0){
                for( var i = 1; i <= jQuery('#itemselect option:selected').attr('data-qty'); i++){
                    jQuery('#qty').append('<option value='+ i +'>'+ i +'</option>');
                }
            }
                
        } ";
            //jQuery.post('". Mage::helper('adminhtml')->getUrl('adminhtml/amrma_request/extractCustomerOrderByEmail') ."', {'email': email,'orderid':orderid}, function(data){alert(data)})
    }

    protected function _prepareLayout()
    {
        $this->_removeButton("delete");
        parent::_prepareLayout();
    }

    public function getHeaderText()
    {
        $header = Mage::helper('amrma')->__('New Status');
        // $model = Mage::registry("amrma_request");//$this->getModel();
        // if( Mage::registry("amrma_request") && Mage::registry("amrma_request")->getId() ){
        //     return Mage::helper("amrma")->__("RMA Request %s for order #%s", $model->getId(), $model->getIncrementId());
        // }
        // else{
             return Mage::helper("amrma")->__("Add Item");
        // }

        // if ($model->getId()){
            // $header = Mage::helper('amrma')->__('RMA Request %s for order #%s', $model->getId(), $model->getIncrementId());
        // }
        return $header;
    }
}
