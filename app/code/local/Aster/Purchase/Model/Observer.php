<?php

class Aster_Purchase_Model_Observer
{
	public function setTestAttribute(Varien_Event_Observer $observer) {

		$item = $observer->getQuoteItem();
		$product = $observer->getProduct();
		$item->setCostPrice($product->getCostPrice());
		return $this;
	}
}
