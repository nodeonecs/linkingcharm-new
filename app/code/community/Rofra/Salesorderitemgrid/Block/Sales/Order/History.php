<?php

class Rofra_Salesorderitemgrid_Block_Sales_Order_History extends Mage_Sales_Block_Order_History
{

    public function getReturnLink($orderobj){

        $order_id = $orderobj->getId();

        $collection = Mage::getModel('sales/order_item')->getCollection();

        $collection->getSelect()
                ->joinLeft(
                    array('order' => $collection->getTable('sales/order')),
                    'order.entity_id = main_table.order_id',
                    array('increment_id', 'created_at as order_created_at', 'grand_total as order_grand_total')
                );

        $collection->addFieldToFilter('qty_shipped', array("gt" => 0));
        $checkshipped = $collection->addFieldToFilter('order_id', array("eq" => $order_id));

        if(count($checkshipped) > 0 ){
            return true;
        }

        return false;



    }
}
