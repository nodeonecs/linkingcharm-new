<?php

class SLandsbek_SimpleOrderExport_Model_Export_Gpreport extends SLandsbek_SimpleOrderExport_Model_Export_Abstract
{
    const ENCLOSURE = '"';
    const DELIMITER = ',';

    public function exportOrders($orders) 
    {
        $fileName = 'gpreport_'.date("Ymd_His").'.csv';
        $fp = fopen(Mage::getBaseDir('export').'/'.$fileName, 'w');

        $this->writeHeadRow($fp,$params);
        $this->writeOrder($fp,$params);

        fclose($fp);

        return $fileName;
    }

    protected function writeHeadRow($fp,$params) 
    {
        fputcsv($fp, $this->getHeadRowValues(), self::DELIMITER, self::ENCLOSURE);
    }

    protected function writeOrder($fp,$params) 
    {
        $report_from = Mage::app()->getRequest()->getParam('report_from');
        $report_to = Mage::app()->getRequest()->getParam('report_to');

        $date_from = date('Y-m-d',strtotime($report_from));
        $date_to = date('Y-m-d',strtotime($report_to));
        if(isset($date_from) && isset($date_to)){
          $datefilter = " sales_flat_order_item.updated_at BETWEEN '".$date_from."' AND '".$date_to."' ";
        }

        $read = Mage::getSingleton('core/resource')->getConnection('core_read'); 

        $reportData = "SELECT sku,name,product_id,avg(price) as price,avg(row_total) as row_total,
        sum(cgst_charge) as cgst_charge,sum(sgst_charge) as sgst_charge,
        sum(igst_charge) as igst_charge,hsn_code,sum(discount_amount) as discount_amount,
        avg(cost_price) as cost_price,sum(qty_ordered) as qty_ordered,
        sum(qty_refunded) as qty_refunded 
        FROM sales_flat_order_item where parent_item_id IS NULL AND ".$datefilter."GROUP BY sku";

        $itemList = $read->fetchAll($reportData);
        
        foreach ($itemList as $item) {  

           $singleProductDiscount = ($item['discount_amount']/intval($item['qty_ordered']));
           $totalQty = intval($item['qty_ordered'])-intval($item['qty_refunded']);
           $grossTotal = floatval($item['price']*$totalQty);
           $netSales = floatval($item['price']*$totalQty);
           $netDiscount = floatval($singleProductDiscount*$totalQty);
           $avgCostAmount = $item['cost_price']*$totalQty;
           $grossProfit = $netSales-$avgCostAmount;

           $cgst_charge = ($item['cgst_charge']/intval($item['qty_ordered']))*$totalQty;
           $sgst_charge = ($item['sgst_charge']/intval($item['qty_ordered']))*$totalQty;
           $igst_charge = ($item['igst_charge']/intval($item['qty_ordered']))*$totalQty;

           $_product = Mage::getModel('catalog/product')->load($item['sku']);
           $categories = $this->getItemCategories($_product);

           $record1 = array();
           $record1['sku'] = $item['sku'];
           $record1['hsn_code'] = $_product->getData('hsncode');
           $record1['name'] = $item['name'];
           $record1['category'] = $categories;
           $record1['qty'] = $totalQty;
           $record1['net_sales'] = $netSales;
           $record1['discount'] = $netDiscount;
           $record1['cgst_charge'] = floatval($cgst_charge);
           $record1['sgst_charge'] = floatval($sgst_charge);
           $record1['igst_charge'] = floatval($igst_charge);
           $record1['gross_sales'] = $grossTotal;
           $record1['cost_price'] = $avgCostAmount;
           $record1['gross_profit'] = $grossProfit;
           $record1['gp'] = round(($grossProfit/$netSales)*100);

           fputcsv($fp, $record1, self::DELIMITER, self::ENCLOSURE);
        }
    }

    protected function getHeadRowValues() 
    {
        return array(         
            'SKU',
            'HSN Code',
            'Name',
            'Category Code',
            'Quantity',
            'Net Sales',
            'Discount',
            'IGST',
            'CGST',
            'SGST',
            'Gross Sales',
            'Cost of Sale',
            'Gross Margin',
            'GP%'
    	);
    }
}
?>