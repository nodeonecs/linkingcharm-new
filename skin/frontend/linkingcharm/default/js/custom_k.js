jQuery(document).ready(function($) {
    $(document).ajaxComplete(function() {
        $(".pickupbox").mCustomScrollbar();
    });
    $(".footer-links-mob h4.footerheadings").click(function(event) {
        // $(this).toggleClass("arrow-down");
        // $(this).next('ul').toggle();

        var mmenufootdd = $(this).closest("ul.policy");
        var mmenufootdd_status = $(this).next().css("display");

        if (mmenufootdd_status == "block") {
            $(this).next().slideUp();
            $(this).removeClass("arrow-down");
            // $(this).removeClass("arrow-down");
        }

        if (mmenufootdd_status == "none") {
            // $(mmenufootdd).find("ul.policy").slideUp();
            // $(parent).find(".narrow-by-list dt").removeClass("head-bold");   
            $(this).next().slideDown();
            // $(this).addClass("minus");
            $(this).addClass("arrow-down");
            // $(this).removeClass("head-change"); 
        }



        event.stopImmediatePropagation();
    });

    $(window).load(function() {
        $('nav#menu').removeClass("nav-hide-menu");
        $('.footer-links-mob h4.footerheadings').removeClass("arrow-down");
    });

    $("#mm-blocker").live('click', function() {
        $('.footer-links-mob h4.footerheadings').removeClass("arrow-down");
        $(".footer-links-mob .policy").slideUp();
    });

    $('nav#menu').mmenu({
        slidingSubmenus: false
    });

    $("a.mobile_menu_icon").click(function() {
        $(".mm-listview > li").removeClass("mm-opened");
    });

    // $(".cartitems.tBody").mCustomScrollbar();
    // jQuery(".cartitems.tBody").mCustomScrollbar({
    //         // mouseWheel:{
    //         //     enable:true,
    //         //     scrollAmount:400
    //         // },
    //         // scrollButtons:{
    //         //     enable:false,
    //         //     scrollType:"stepless",
    //         //     scrollAmount:400
    //         // },
    //         // keyboard:{
    //         //     enable:true,
    //         //     scrollType:"stepless",
    //         //     scrollAmount:400
    //         // },
    //         scrollInertia:400
    //    });

    // $(document).ajaxComplete(function() {
    //     $(".cartitems.tBody").mCustomScrollbar();
    // });

    $("#review_anchor").hide();
    $(".review-overlay").hide();
    $(".product-page-write-review").live("click", function() {
        $(".review-overlay").show();
        $("#review_anchor").show();
        $('html, body').animate({
            scrollTop: $("#review_anchor").offset().top
        }, 500);
    });

    $(".close-btn-review").click(function() {
        $(".review-overlay").hide();
        $("#review_anchor").hide();
    });

    $(".search-icon-mob").click(function() {
        $(".head-right").toggle();
    });


});