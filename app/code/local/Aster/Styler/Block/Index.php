<?php   
class Aster_Styler_Block_Index extends Mage_Core_Block_Template{   


public function getTypeBrowseBy()
	{
		$attribute = Mage::getModel('eav/config')->getAttribute( Mage_Catalog_Model_Product::ENTITY ,'product_type');
		$options = $attribute->getSource()->getAllOptions();
		$type_by_browse_by = array();
		foreach ($options as $option) {
			if(!empty($option['label'])){
				$type_by_browse_by[$option['value']] = $option['label'];
						
			}
		}

		// print_r($type_by_browse_by);


		return json_encode($type_by_browse_by);
	}


}