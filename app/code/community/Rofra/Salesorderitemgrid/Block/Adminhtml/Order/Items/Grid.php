 <?php
/**
 * @category    Graphic Sourcecode
 * @package     Rofra_Salesorderitemgrid
 * @license     http://www.apache.org/licenses/LICENSE-2.0
 * @author      Rodolphe Franceschi <rodolphe.franceschi@gmail.com>
 */
class Rofra_Salesorderitemgrid_Block_Adminhtml_Order_Items_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {

        parent::__construct();

        $this->setId('order_items');
        $this->setDefaultSort('item_id');
        $this->prepareDefaults();
        $assigned_status_filter = $this->getRequest()->getParam('assigned_filter');
        if($assigned_status_filter)
        $this->setDefaultFilter( array('assigned_status' => $assigned_status_filter));
        $this->setSaveParametersInSession(false);

    }

    protected function prepareDefaults()
    {

        $this->setDefaultLimit( Mage::getStoreConfig( 'salesorderitemgrid/defaults/limit' ) );
        $this->setDefaultPage( Mage::getStoreConfig( 'salesorderitemgrid/defaults/page' ) );


    }


    /*protected function _filterOnAcceptedStore($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return $this;
        }

        $this->getCollection()->join(array('co' =>'core/store_group'), 'main_table.store_code = co.group_id',array('main_table.name'))->getSelect()->where(
            " co.name like ?"
        , "%$value%");


        return $this;
    }*/

    protected function _filterOnAcceptedStore($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return $this;
        }
            $this->getCollection()->join(array('co' =>'core/store_group'),
             'main_table.store_code_1 = co.group_id',array('main_table.name'))->getSelect()->where(
            " co.name like ?"
        , "%$value%");

        return $this;
    }

    protected function _filterOnOrderItemsStatus($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return $this;
        }



        $this->getCollection()->addAttributeToFilter('main_table.item_status', array('eq' => $value));


        return $this;
    }

    protected function _filterOnOrderItemsState($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return $this;
        }



        $this->getCollection()->addAttributeToFilter('main_table.item_state', array('eq' => $value));


        return $this;
    }



    protected function _prepareCollection()
    {

        $collection  = Mage::getModel('sales/order_item')->getCollection();

        $collection->join(array('og' =>'sales/order_grid'), 'main_table.order_id = og.entity_id', array('billing_name', 'shipping_name', 'increment_id', 'status', 'og_created_at' =>'og.created_at','og.entity_id'));

        $hlp = Mage::helper('amrolepermissions');

         $rule = $hlp->currentRule();

        $stores = $rule->getScopeStoreviews();

        $CustomRole = $rule->getCustomRole()[0];

        //$aStores = array();
        $aStores = '(';
        foreach($stores as $store){
            //$aStores [] = $store;
            $aStores .= "'".$store."',";
        }
         $aStores = rtrim($aStores, ',');
        $aStores .= ')';

    /*echo $aStores;
    exit;
*/
        // Remove configurable products from the list. Show only simple for assignment.
        $collection->getSelect()->where('main_table.product_type = (?)', 'simple');

        //$isAutoallocateActive = Mage::getStoreConfig('omniapis_section/custom_settings/allocations_of_orders');

        /*if($isAutoallocateActive == '0'){
            if(!empty($aStores)){
                        $collection->getSelect()
                    ->where('main_table.store_id IN (?)', $aStores);

            }
        }else{*/

            if($CustomRole == 'W_HO'){

                $aWebsite_ids = explode(',' , $rule->getScopeWebsites()[0]);

                $collection->getSelect()
                    ->where('main_table.website_id in (?)', $aWebsite_ids);

            }elseif($CustomRole != 'S_A'){

                $collection->getSelect()
                ->where(sprintf('(main_table.store_1 IN %s) OR 
                    (main_table.store_2 IN %s) OR (main_table.store_3 IN %s) OR 
                    (main_table.store_4 IN %s)
                 ', $aStores , $aStores , $aStores , $aStores));

                        /*$collection->getSelect()
                    ->where('main_table.store_1 IN (?)', $aStores);
                    $collection->getSelect()
                    ->orwhere('main_table.store_2 IN (?)', $aStores);
                    $collection->getSelect()
                    ->orwhere('main_table.store_3 IN (?)', $aStores);
                    $collection->getSelect()
                    ->orwhere('main_table.store_4 IN (?)', $aStores);
                    $collection->getSelect()
                    ->orwhere('main_table.store_5 IN (?)', $aStores);*/
            }

            /*echo $collection->getSelect();
            exit;*/

        $this->setCollection($collection);
        parent::_prepareCollection();
        return $this;
    }


    protected function _prepareColumns()
    {

        $this->addColumn('item_id', array(
                'header' => Mage::helper('salesorderitemgrid')->__('ID'),
                'sortable' => true,
                'width' => '60',
                'index' => 'item_id'
        ));

        $this->addColumn('created_at', array(
            'header'    => Mage::helper('sales')->__('Order Date'),
            'index'     => 'created_at',
            'type'      => 'datetime',
            'filter_index'=>'main_table.created_at',
        ));

        $this->addColumn('product_image', array(
                'header' => Mage::helper('salesorderitemgrid')->__('Product Image'),
                'width' => '60',
                'index' => 'product_image',
                //'renderer' => 'Rofra_Salesorderitemgrid_Block_Adminhtml_Order_Items_Grid_Renderer_Productimage',
                'filter' => false,
        ));

        $this->addColumn('order_paymentmethod', array(
                'header' => Mage::helper('salesorderitemgrid')->__('Payment Method'),
                'width' => '60',
                'index' => 'order_paymentmethod',
                'renderer' => 'Rofra_Salesorderitemgrid_Block_Adminhtml_Order_Items_Grid_Renderer_Orderpaymentmethod',
                'filter' => false,
        ));


        $this->addColumn('incremental_id', array(
                'header' => Mage::helper('sales')->__('Order #'),
                'sortable' => true,
                'width' => '60',
                'index' => 'increment_id',
                'renderer' => 'Rofra_Salesorderitemgrid_Block_Adminhtml_Order_Items_Grid_Renderer_Order',
        ));

        // $this->addColumn('invoice_id', array(
        //         'header' => Mage::helper('salesorderitemgrid')->__('Invoice Id'),
        //         'sortable' => true,
        //         'width' => '60',
        //         'index' => 'invoice_id',
        //         'renderer' => 'Rofra_Salesorderitemgrid_Block_Adminhtml_Order_Items_Grid_Renderer_Invoiceid'
        // ));

        $this->addColumn('store_name', array(
                'header' => Mage::helper('salesorderitemgrid')->__('Store Name'),
                'sortable' => true,
                'width' => '60',
                'index' => 'store_name'
        ));

        $this->addColumn('checkout_method', array(
                'header' => Mage::helper('salesorderitemgrid')->__('Checkout Method'),
                'sortable' => true,
                'width' => '60',
                'index' => 'checkout_method'
        ));


        $this->addColumn('pickup_start_date', array(
                'header' => Mage::helper('salesorderitemgrid')->__('Pickup Start Date'),
                'sortable' => true,
                'width' => '60',
                'index' => 'pickup_start_date',
                'type'      => 'datetime',
        ));


        $this->addColumn('pickup_end_date', array(
                'header' => Mage::helper('salesorderitemgrid')->__('Pickup End Date'),
                'sortable' => true,
                'width' => '60',
                'index' => 'pickup_end_date',
                'renderer' => 'Rofra_Salesorderitemgrid_Block_Adminhtml_Order_Items_Grid_Renderer_Enddate',
                'type'      => 'datetime',
        ));


        // $this->addColumn('item_id', array(
        //         'header' => Mage::helper('salesorderitemgrid')->__('ID'),
        //         'sortable' => true,
        //         'width' => '60',
        //         'index' => 'item_id'
        // ));



        /*$this->addColumn('created_at', array(
                'header' => Mage::helper('sales')->__('Purchased On'),
                'sortable' => true,
                'width' => '60',
                'index' => 'og_created_at',
                'filter_index' => 'og.created_at',
                'type'  => 'datetime'
        ));*/



        $this->addColumn('sku', array(
                'header' => Mage::helper('salesorderitemgrid')->__('SKU'),
                'sortable' => true,
                'index' => 'sku',
                'renderer' => 'Rofra_Salesorderitemgrid_Block_Adminhtml_Order_Items_Grid_Renderer_Sku',
        ));

        /*$this->addColumn('name', array(
                'header' => Mage::helper('salesorderitemgrid')->__('Name'),
                'sortable' => true,
                'index' => 'name'
        ));*/


         $iLoginuserid = Mage::getSingleton('admin/session')->getUser()->getUserId();


        $LoginUserRole = Mage::getModel('admin/user')->load($iLoginuserid)->getRole()->getRoleId();

        $iRole_Collectionsid = Mage::getSingleton('amrolepermissions/rule')
        ->getCollection()->addFieldToFilter('role_id' , array('eq' => $LoginUserRole));



         foreach($iRole_Collectionsid as $Role)
         {

                $RoleCustomRole = $Role->getCustomRole();
         }




            $this->addColumn('is_accepted', array(
                //'header' => Mage::helper('catalog')->__('Accepted Store'),
                   'header' => Mage::helper('catalog')->__('Current Order Items Info'),
                'sortable' => true,
                'renderer' => 'Rofra_Salesorderitemgrid_Block_Adminhtml_Order_Items_Grid_Renderer_Acceptedstores',
                'filter_condition_callback' => array($this, '_filterOnAcceptedStore'),
            ));

            $aAdminRole = array('S_A' , 'W_HO');

                    /* if(!in_array($RoleCustomRole , $aAdminRole)){

                         $this->addColumn('hub_id', array(
                                'header' => Mage::helper('catalog')->__('After accept'),
                                'sortable' => true,
                                'renderer' => 'Rofra_Salesorderitemgrid_Block_Adminhtml_Order_Items_Grid_Renderer_Transferhub',

                            ));

                    }*/




        $this->addColumn(
            'item_status',  array(
            'header' => Mage::helper('salesorderitemgrid')->__('Order Item Status'),
            'filter_index' => 'item_status',
            'type'  => 'options',
            'filter_condition_callback' => array($this, '_filterOnOrderItemsStatus'),
            //'options' => Mage::getSingleton('sales/order_config')->getStatuses(),
            'options' => Mage::getSingleton('salesorderitemgrid/orderitemsstatusfilter')->getStatusOptions(),
            'index' => 'item_status'
            //'index' => 'sku',
            //'value' => $this->helper('salesorderitemgrid')->__('Test popup dialog >>'),
            //'class' => 'form-button',
            //'onclick' => 'javascript:openMyPopup()',
            //'renderer' => 'Rofra_Salesorderitemgrid_Block_Adminhtml_Order_Items_Grid_Renderer_Orderallocate',
            )
        );

        /*$this->addColumn(
            'item_state',  array(
            'header' => Mage::helper('salesorderitemgrid')->__('Order Item State'),
            'filter_index' => 'item_state',
            'type'  => 'options',
            'filter_condition_callback' => array($this, '_filterOnOrderItemsState'),
            //'options' => Mage::getSingleton('sales/order_config')->getStatuses(),
            'options' => Mage::getSingleton('salesorderitemgrid/orderitemsstatusfilter')->getStateOptions(),
            'index' => 'item_state'
            //'index' => 'sku',
            //'value' => $this->helper('salesorderitemgrid')->__('Test popup dialog >>'),
            //'class' => 'form-button',
            //'onclick' => 'javascript:openMyPopup()',
            //'renderer' => 'Rofra_Salesorderitemgrid_Block_Adminhtml_Order_Items_Grid_Renderer_Orderallocate',
            )
        );*/


        $this->addColumn(
            'status',  array(
            'header' => Mage::helper('salesorderitemgrid')->__('Stores Action'),
            'filter_index' => 'status',
            //'type'  => 'options',
            //'filter_condition_callback' => array($this, '_filterOnOrderItemsStatus'),
            //'options' => Mage::getSingleton('sales/order_config')->getStatuses(),
            //'options' => Mage::getSingleton('salesorderitemgrid/orderitemsstatusfilter')->getStatusOptions(),
            //'index' => 'sku',
            //'value' => $this->helper('salesorderitemgrid')->__('Test popup dialog >>'),
            //'class' => 'form-button',
            //'onclick' => 'javascript:openMyPopup()',
            'renderer' => 'Rofra_Salesorderitemgrid_Block_Adminhtml_Order_Items_Grid_Renderer_Orderallocate',
            )
        );

        $this->addColumn('shipment_view', array(
                'header' => Mage::helper('salesorderitemgrid')->__('Shipment'),
                'sortable' => false,
                'index' => 'link',
                'renderer' => 'Rofra_Salesorderitemgrid_Block_Adminhtml_Order_Items_Grid_Renderer_Viewshipment',
        ));


        $this->addColumn('invoice_view', array(
                'header' => Mage::helper('salesorderitemgrid')->__('Invoice'),
                'sortable' => false,
                'index' => 'link',
                'renderer' => 'Rofra_Salesorderitemgrid_Block_Adminhtml_Order_Items_Grid_Renderer_Viewinvoice',
        ));


        $this->addColumn('replaced_order_id', array(
                'header' => Mage::helper('salesorderitemgrid')->__('Replacement against Order ID'),
                'sortable' => false,
                'index' => 'replaced_order_id',
                'renderer' => 'Rofra_Salesorderitemgrid_Block_Adminhtml_Order_Items_Grid_Renderer_Replacementagainst'
        ));

        $this->addColumn('processed_by_stores', array(
                'header' => Mage::helper('salesorderitemgrid')->__('Processed By Stores'),
                'sortable' => false,
                'index' => 'processed_by_stores',
                'renderer' => 'Rofra_Salesorderitemgrid_Block_Adminhtml_Order_Items_Grid_Renderer_ProcessedbyStore'
        ));

        //if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/view')) {
            // $this->addColumn('action',
            //     array(
            //         'header'    => Mage::helper('sales')->__('Action'),
            //         'width'     => '50px',
            //         'type'      => 'action',
            //         'getter'     => 'getId',
            //         'actions'   => array(
            //             array(
            //                 'caption' => Mage::helper('sales')->__('View'),
            //                 'url'     => array('base'=>'*/*/view' , array("order_id" => $row->getOrderId(),"sub_order_id" => $row->getId())),
            //                 //'field'   => 'order_id'
            //             )
            //         ),
            //         'filter'    => false,
            //         'sortable'  => false,
            //         'index'     => 'stores',
            //         'is_system' => true,
            // ));
        //}



        // SPECIAL COLUMNS
        $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
        $this->addExportType('*/*/exportXml', Mage::helper('sales')->__('XML'));

        return parent::_prepareColumns();
        //return;
    }

    public function getRowUrl($row)
        {

            $parent_id = Mage::getSingleton('sales/order_item')->load($row->getId())->getParentItemId();
            if($parent_id){
                $id = $parent_id;
            }else{
                $id = $row->getId();
            }
        return $this->getUrl("*/*/view", array("order_id" => $row->getOrderId(),"sub_order_id" => $id));
        }

    /* Start - Add new massaction for invoice & shipment creation in order grid view*/
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('order_ids');
        //$bFullifillment_check = false;
        //$this->getMassactionBlock()->setUseSelectAll(false);


        // $bFullifillment_check = Mage::helper('salesorderitemgrid')->getFulfilmentEnable(); // Returns if the Fulfilment Enable not .




        //     if($bFullifillment_check){

        //             $this->getMassactionBlock()->addItem('invoice', array(
        //             'label'=> Mage::helper('salesorderitemgrid')->__('Invoice'),
        //             'url'  => $this->getUrl('*/*/massInvoice', array('' => '')),
        //             'confirm' => Mage::helper('salesorderitemgrid')->__('Are you sure?')
        //             ));

        //             $this->getMassactionBlock()->addItem('ship', array(
        //             'label'=> Mage::helper('salesorderitemgrid')->__('Ship'),
        //             'url'  => $this->getUrl('*/*/massShip', array('' => '')),
        //             'confirm' => Mage::helper('salesorderitemgrid')->__('Are you sure?')
        //             ));
        //     }



       return $this;
    }


    /* End - Add new massaction for invoice & shipment creation in order grid view*/
}
?>
