<?php
class Iksula_Rewrites_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function getconfigAddtocart($configproductId,$packsize_option_value,$qty)
	{	
		$product = $this->_getProductModel();
		$attribute_code = "size"; 
		$attribute_details = Mage::getSingleton("eav/config")->getAttribute('catalog_product', $attribute_code); 
		$packsize_attributeid = $attribute_details->getAttributeId();
		$params = array(
			'product' => $configproductId,
			'super_attribute' => array(
				$packsize_attributeid =>$packsize_option_value,
				),
			'qty' => $qty,
			);

		$cart = Mage::getSingleton('checkout/cart'); 
		$product->load($configproductId); 
		$cart->addProduct($product, $params);
		$cart->save(); 
		Mage::getSingleton('checkout/session')->setCartWasUpdated(true); 
	}

	public function getPackSizeByProductId($productId){
		$_product = $this->_getProductModel();
		$_product->load($productId);
		$packsize = $_product->getData('size');
		return $packsize;
	}

	public function getConfigTypeModel()
	{
		return Mage::getSingleton('catalog/product_type_configurable');
	}
	
	public function _getProductModel()
	{
		return Mage::getSingleton('catalog/product');
	}

	public function getRatingOnHome($productId,$storeId)
	{
		$summaryData = Mage::getModel('review/review_summary')
		->setStoreId($storeId)
		->load($productId);
		return $summaryData['rating_summary'];

	}

	public function getPageUrl()
	{
		if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on"){ 
			return true;
		}
		else{
			return false;
		}
	}
}
