<?php
/**
 * @category    Graphic Sourcecode
 * @package     Rofra_Salesorderitemgrid
 * @license     http://www.apache.org/licenses/LICENSE-2.0
 * @author      Rodolphe Franceschi <rodolphe.franceschi@gmail.com>
 */

$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$setup->startSetup();

$setup->run("
ALTER TABLE autoallocate_status ADD COLUMN `updated_at` TIMESTAMP NOT NULL ON UPDATE CURRENT_TIMESTAMP;
ALTER TABLE autoallocate_status MODIFY COLUMN `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;
");

$setup->endSetup();
