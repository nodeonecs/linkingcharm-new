<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category   Mage
 * @package    Mage_payuemi
 * @copyright  Copyright (c) 2008 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * payuemi Standard Front Controller
 *
 * @category   Mage
 * @package    Mage_payuemi
 * @name       Mage_payuemi_StandardController
 * @author     Magento Core Team <core@magentocommerce.com>
*/

require('Rc43.php');

class Mage_Payuemi_StandardController extends Mage_Core_Controller_Front_Action
{
    /**
     * Order instance
     */
    protected $_order;

    /**
     *  Return debug flag
     *
     *  @return  boolean
     */
    public function getDebug ()
    {
        return Mage::getSingleton('payuemi/config')->getDebug();
    }

    /**
     *  Get order
     *
     *  @param    none
     *  @return	  Mage_Sales_Model_Order
     */
    public function getOrder ()
    {
        if ($this->_order == null) {
            $session = Mage::getSingleton('checkout/session');
            $this->_order = Mage::getModel('sales/order');
            $this->_order->loadByIncrementId($session->getLastRealOrderId());
        }
        return $this->_order;
    }

    /**
     * When a customer chooses payuemi on Checkout/Payment page
     *
     */
    public function redirectAction()
    {
        $session = Mage::getSingleton('checkout/session');
        $session->setpayuemiStandardQuoteId($session->getQuoteId());

        $order = $this->getOrder();

        if (!$order->getId()) {
            $this->norouteAction();
            return;
        }

        $order->addStatusToHistory(
            $order->getStatus(),
            Mage::helper('payuemi')->__('Customer was redirected to payuemi')
        );
        $order->save();

        $this->getResponse()
            ->setBody($this->getLayout()
                ->createBlock('payuemi/standard_redirect')
                ->setOrder($order)
                ->toHtml());

        $session->unsQuoteId();
    }

    /**
     *  Success response from Secure Ebs
     *
     *  @return	  void
     */

    public function getConfig()
    {
        return Mage::getSingleton('payuemi/config');
    }

    public function  successAction()
    {

        $params = $this->getRequest()->getParams();
        $order = $this->getOrder();

        $status  = $params['status'];
        $orderId = $order->getIncrementId();
        $amount  = number_format($order->getBaseGrandTotal(), 2, '.', '');

        if ($this->getConfig()->getDescription()) {
            $desc = $this->getConfig()->getDescription();
        } else {
            $desc = Mage::helper('secureebs')->__('Order #%s', $order->getRealOrderId());
        }

        $billing = $order->getBillingAddress();
        $fname = $billing->getFirstname();
        $email = $order->getCustomerEmail();
        $mId   = Mage::getSingleton('payuemi/config')->getAccountId();
        $salt  = Mage::getSingleton('payuemi/config')->getSecretKey();
        $hash  =  $salt . '|' . $status . '|||||||||||' . $email . '|' . $fname . '|' .  $desc . '|' . $amount .'|'.$orderId.'|'.$mId;


        $request_hash = hash('sha512',$hash);
        $response_payu_hash = $params['hash'];

        if($request_hash == $response_payu_hash){

            if($this->getRequest()->getParam('status') != 'success'){
                $errorMsg = Mage::helper('payudebit')->__('There was an error occurred during paying process, payu sent response in Success Action!');
                $order->addStatusToHistory($order->getStatus(), $errorMsg);
                $order->cancel();
                $order->save();
                $this->_redirect('onepagecheckout/index/failure');
                Mage::getSingleton('checkout/session')->unsLastRealOrderId();
                return false;
            }
            else{

                $order->setStatus('awaitingverification');
                $order->addStatusToHistory(
                    $order->getStatus(),
                    Mage::helper('secureebs')->__('Customer successfully returned from Payu debit card')
                );

                $order->save();
                $order->sendNewOrderEmail();
                $this->_redirect('onepagecheckout/index/success');
            }
        }
        else{
            $order->cancel();
            $order->addStatusToHistory(
                    $order->getStatus(),
                    Mage::helper('secureebs')->__('Payment error')
                );
            $order->save();
        }
    }



    /**
     *  Notification Action from Secure Ebs
     *
     *  @param    none
     *  @return	  void
     */
    public function notifyAction ()
    {
        $postData = $this->getRequest()->getPost();

        if (!count($postData)) {
            $this->norouteAction();
            return;
        }

        if ($this->getDebug()) {
            $debug = Mage::getModel('payuemi/api_debug');
            if (isset($postData['cs2']) && $postData['cs2'] > 0) {
                $debug->setId($postData['cs2']);
            }
            $debug->setResponseBody(print_r($postData,1))->save();
        }

        $order = Mage::getModel('sales/order');
        $order->loadByIncrementId(Mage::helper('core')->decrypt($postData['cs1']));
        if ($order->getId()) {
            $result = $order->getPayment()->getMethodInstance()->setOrder($order)->validateResponse($postData);

            if ($result instanceof Exception) {
                if ($order->getId()) {
                    $order->addStatusToHistory(
                        $order->getStatus(),
                        $result->getMessage()
                    );
                    $order->cancel();
                }
                Mage::throwException($result->getMessage());
                return;
            }

            $order->sendNewOrderEmail();

            $order->getPayment()->getMethodInstance()->setTransactionId($postData['transaction_id']);

            if ($this->saveInvoice($order)) {
                $order->setState(Mage_Sales_Model_Order::STATE_PROCESSING, true);
            }
            $order->save();
        }
    }

    /**
     *  Save invoice for order
     *
     *  @param    Mage_Sales_Model_Order $order
     *  @return	  boolean Can save invoice or not
     */
    protected function saveInvoice (Mage_Sales_Model_Order $order)
    {
        if ($order->canInvoice()) {
            $invoice = $order->prepareInvoice();
            $invoice->register()->capture();
            Mage::getModel('core/resource_transaction')
               ->addObject($invoice)
               ->addObject($invoice->getOrder())
               ->save();
            return true;
        }

        return false;
    }

    /**
     *  Failure response from payuemi
     *
     *  @return	  void
     */
    /*public function failureAction ()
    {

        //echo "<pre>"; print_r($_REQUEST);exit;
        $errorMsg = Mage::helper('payuemi')->__('There was an error occurred during paying process.');

        $order = $this->getOrder();
		//$order->setStatus('pending');

        if (!$order->getId()) {
            $this->norouteAction();
            return;
        }

        if ($order instanceof Mage_Sales_Model_Order && $order->getId()) {
            $order->addStatusToHistory($order->getStatus(), $errorMsg);
            $order->cancel();
            $order->save();
        }

        //$this->loadLayout();
        //$this->renderLayout();
         $this->_redirect('onepagecheckout/index/failure');

        //Mage::getSingleton('checkout/session')->unsLastRealOrderId();
    }*/
    public function failureAction ()
    {
        $order = $this->getOrder();
        $previousStatus = $order->getStatus();
        $txnId = $this->getRequest()->getParam('txnid');
        $currentOrderId = $order->getRealOrderId();
        if($txnId == $currentOrderId && $previousStatus == "pending"){

            Mage::log($this->getRequest()->getParams(), null, 'payudebit.log');
            $errorMsg = Mage::helper('payudebit')->__('There was an error occurred during paying process.');
            if (!$order->getId()) {
                $this->norouteAction();
                return;
            }

            if ($order instanceof Mage_Sales_Model_Order && $order->getId()) {
                $order->setState(Mage_Sales_Model_Order::STATE_CANCELED,'canceledpending','P.G. Error : '.$errorMsg)->save();
            }
            $this->_redirect('onepagecheckout/index/failure');

            Mage::getSingleton('checkout/session')->unsLastRealOrderId();
        }
        else{
            Mage::getSingleton('core/session')->addError('Session for this transaction has been expired!');
            $this->_redirect();
        }
    }


    public function payuAction () {
      Mage::getSingleton('checkout/session')->setCcCheckedEmi($_POST['checked']);
    }

    public function payuoldAction () {
      Mage::getSingleton('checkout/session')->setCcCheckedEmi('false');
      Mage::getSingleton('checkout/session')->setCardtokenEmi($_POST['card_token']);
      Mage::getSingleton('checkout/session')->setCvvnewEmi($_POST['cvv']);
    }


 protected function getPmt($i, $n, $p)
    {
        $i = $i/1200;
        $pmt = round(abs($i * $p * pow((1 + $i), $n) / (1 - pow((1 + $i), $n))),2);
        $return = ($pmt*$n)-$p;

        return array('Indicative EMI' => $pmt ,'Total Interest' => $return );
    }

  public function getEmiOptionsAction()
    {

       $bankCode= Mage::app()->getRequest()->getParam('emitype');

       $quote_id        = Mage::getSingleton('checkout/session')->getQuoteId();

       $item_quote     = Mage::getSingleton('sales/quote')->load($quote_id);

       $basePrice = $item_quote->getGrandTotal();

       //echo "<pre>"; print_r($item_quote);exit;

        $rateOfInterest  = array('Quarter_Half' => 12,'Nine_Yearly' =>  13,'Two_Yearly'=> 15);

         $data='<div class="ttls blk">
                    <span class="radioBtn">&nbsp;</span>
                    <span class="">Tenure (months)</span>
                    <span class="bnkInt">Bank IR</span>
                    <span class="emiAmtTxt">Emi</span>
                    <span class="bnkInt">Int. paid to Bank</span>
                </div>      ';

        $pmtQ = $this->getPmt($rateOfInterest['Quarter_Half'],3,$basePrice);

       // print_r($pmtQ);

        $pmtH = $this->getPmt($rateOfInterest['Quarter_Half'],6,$basePrice);

        $pmtN = $this->getPmt($rateOfInterest['Nine_Yearly'],9,$basePrice);
        $pmtY = $this->getPmt($rateOfInterest['Nine_Yearly'],12,$basePrice);

        $pmtT = $this->getPmt($rateOfInterest['Nine_Yearly'],12,$basePrice);
        $q1 = $this->getPmt($rateOfInterest['Two_Yearly'],18,$basePrice);

        $pmtTf = $this->getPmt($rateOfInterest['Two_Yearly'],24,$basePrice);

       if($bankCode=="CITI")
           {
                $data= $data. '<div class="blk emiOpts">
                                    <label class="blk selected">
                                        <span class="radioBtn">
                                            <input type="radio" name="payment[emi_mode]" class="validate-one-required-by-name" value="EMI03">
                                        </span>
                                        <span>03</span>
                                        <span class="bnkInt">12%</span>
                                        <span class="emiAmtTxt">Rs '.$pmtQ['Indicative EMI'].'</span>
                                        <span class="bnkInt"> Rs '.$pmtQ['Total Interest'].'</span>
                                    </label>
                                </div>
                                <div class="blk emiOpts">
                                    <label class="blk selected">
                                        <span class="radioBtn">
                                            <input type="radio" name="payment[emi_mode]" class="validate-one-required-by-name" value="EMI06">
                                        </span>
                                        <span>06</span>
                                        <span class="bnkInt">12%</span>
                                       <span class="emiAmtTxt">Rs '.$pmtH['Indicative EMI'].'</span>
                                        <span class="bnkInt"> Rs '.$pmtH['Total Interest'].'</span>
                                    </label>
                                </div>';
                                /*
                                <div class="blk emiOpts">
                                    <label class="blk selected">
                                        <span class="radioBtn">
                                            <input type="radio" name="payment[emi_mode]" class="validate-one-required-by-name" value="EMI09">
                                        </span>
                                        <span>09</span>
                                        <span class="bnkInt">13%</span>
                                        <span class="emiAmtTxt">Rs '.$pmtN['Indicative EMI'].'</span>
                                        <span class="bnkInt"> Rs '.$pmtN['Total Interest'].'</span>
                                    </label>
                                </div>
                                <div class="blk emiOpts">
                                    <label class="blk selected">
                                        <span class="radioBtn">
                                            <input type="radio" name="payment[emi_mode]" class="validate-one-required-by-name" value="EMI012">
                                        </span>
                                        <span>12</span>
                                        <span class="bnkInt">13%</span>
                                       <span class="emiAmtTxt">Rs '.$pmtY['Indicative EMI'].'</span>
                                        <span class="bnkInt"> Rs '.$pmtY['Total Interest'].'</span>
                                    </label>
                                </div>
                                <div class="blk emiOpts">
                                    <label class="blk selected">
                                        <span class="radioBtn">
                                            <input type="radio" name="payment[emi_mode]" class="validate-one-required-by-name" value="EMI018">
                                        </span>
                                        <span>18</span>
                                        <span class="bnkInt">13%</span>
                                        <span class="emiAmtTxt">Rs '.$pmtE['Indicative EMI'].'</span>
                                        <span class="bnkInt"> Rs '.$pmtE['Total Interest'].'</span>
                                    </label>
                                </div>
                                <div class="blk emiOpts">
                                    <label class="blk selected">
                                        <span class="radioBtn">
                                            <input type="radio" name="payment[emi_mode]" class="validate-one-required-by-name" value="EMI024">
                                        </span>
                                        <span>24</span>
                                        <span class="bnkInt">13%</span>
                                        <span class="emiAmtTxt">Rs '.$pmtTf['Indicative EMI'].'</span>
                                        <span class="bnkInt"> Rs '.$pmtTf['Total Interest'].'</span>
                                    </label>
                                </div>';*/
           }
       elseif($bankCode=="HDFC")
           {
                $data= $data. '<div class="blk emiOpts">
                    <label class="blk selected">
                        <span class="radioBtn">
                            <input type="radio" name="payment[emi_mode]" class="validate-one-required-by-name" value="EMI">
                        </span>
                        <span>3</span>
                        <span class="bnkInt">12%</span>
                        <span class="emiAmtTxt">Rs '.$pmtQ['Indicative EMI'].'</span>
                        <span class="bnkInt"> Rs '.$pmtQ['Total Interest'].'</span>
                    </label>
                </div>
                <div class="blk emiOpts">
                    <label class="blk selected">
                        <span class="radioBtn">
                            <input type="radio" name="payment[emi_mode]" class="validate-one-required-by-name" value="EMI6">
                        </span>
                        <span>6</span>
                        <span class="bnkInt">12%</span>
                        <span class="emiAmtTxt">Rs '.$pmtH['Indicative EMI'].'</span>
                        <span class="bnkInt"> Rs '.$pmtH['Total Interest'].'</span>
                    </label>
                </div>';/*
                <div class="blk emiOpts">
                    <label class="blk selected">
                        <span class="radioBtn">
                            <input type="radio" name="payment[emi_mode]" class="validate-one-required-by-name" value="EMI9">
                        </span>
                        <span>9</span>
                        <span class="bnkInt">13%</span>
                        <span class="emiAmtTxt">Rs '.$pmtN['Indicative EMI'].'</span>
                        <span class="bnkInt"> Rs '.$pmtN['Total Interest'].'</span>
                    </label>
                </div>
                <div class="blk emiOpts">
                    <label class="blk selected">
                        <span class="radioBtn">
                            <input type="radio" name="payment[emi_mode]" class="validate-one-required-by-name" value="EMI12">
                        </span>
                        <span>12</span>
                        <span class="bnkInt">13%</span>
                        <span class="emiAmtTxt">Rs '.$pmtY['Indicative EMI'].'</span>
                        <span class="bnkInt"> Rs '.$pmtY['Total Interest'].'</span>
                    </label>
                </div>';*/

           }
       elseif($bankCode=="AXIS")
           {/*
                 $data= $data. '<div class="blk emiOpts">
                        <label class="blk selected">
                            <span class="radioBtn">
                                <input type="radio" name="payment[emi_mode]" class="validate-one-required-by-name" value="EMIA3">
                            </span>
                            <span>3</span>
                            <span class="bnkInt">12%</span>
                            <span class="emiAmtTxt">Rs '.$pmtQ['Indicative EMI'].'</span>
                        <span class="bnkInt"> Rs '.$pmtQ['Total Interest'].'</span>
                        </label>
                    </div>
                    <div class="blk emiOpts">
                        <label class="blk selected">
                            <span class="radioBtn">
                                <input type="radio" name="payment[emi_mode]" class="validate-one-required-by-name" value="EMIA6">
                            </span>
                            <span>6</span>
                            <span class="bnkInt">12%</span>
                           <span class="emiAmtTxt">Rs '.$pmtH['Indicative EMI'].'</span>
                        <span class="bnkInt"> Rs '.$pmtH['Total Interest'].'</span>
                        </label>
                    </div>';
                    <div class="blk emiOpts">
                        <label class="blk selected">
                            <span class="radioBtn">
                                <input type="radio" name="payment[emi_mode]" class="validate-one-required-by-name" value="EMIA9">
                            </span>
                            <span>9</span>
                            <span class="bnkInt">13%</span>
                           <span class="emiAmtTxt">Rs '.$pmtN['Indicative EMI'].'</span>
                        <span class="bnkInt"> Rs '.$pmtN['Total Interest'].'</span>
                        </label>
                    </div>
                    <div class="blk emiOpts">
                        <label class="blk selected">
                            <span class="radioBtn">
                                <input type="radio" name="payment[emi_mode]" class="validate-one-required-by-name" value="EMIA12">
                            </span>
                            <span>12</span>
                            <span class="bnkInt">13%</span>
                            <span class="emiAmtTxt">Rs '.$pmtY['Indicative EMI'].'</span>
                        <span class="bnkInt"> Rs '.$pmtY['Total Interest'].'</span>
                        </label>
                    </div>';*/
           }
       elseif($bankCode=="ICICI")
           {/*
                $data= $data. '<div class="blk emiOpts">
                                <label class="blk selected">
                                    <span class="radioBtn">
                                        <input type="radio" name="payment[emi_mode]" class="validate-one-required-by-name" value="EMIIC3">
                                    </span>
                                    <span>3</span>
                                    <span class="bnkInt">12%</span>
                                    <span class="emiAmtTxt">Rs '.$pmtQ['Indicative EMI'].'</span>
                                    <span class="bnkInt"> Rs '.$pmtQ['Total Interest'].'</span>
                                </label>
                            </div>
                            <div class="blk emiOpts">
                                <label class="blk selected">
                                    <span class="radioBtn">
                                        <input type="radio" name="payment[emi_mode]" class="validate-one-required-by-name" value="EMIIC6">
                                    </span>
                                    <span>6</span>
                                    <span class="bnkInt">12%</span>
                                    <span class="emiAmtTxt">Rs '.$pmtH['Indicative EMI'].'</span>
                                    <span class="bnkInt"> Rs '.$pmtH['Total Interest'].'</span>
                                </label>
                            </div>';
                            <div class="blk emiOpts">
                                <label class="blk selected">
                                    <span class="radioBtn">
                                        <input type="radio" name="payment[emi_mode]" class="validate-one-required-by-name" value="EMIIC9">
                                    </span>
                                    <span>9</span>
                                    <span class="bnkInt">13%</span>
                                    <span class="emiAmtTxt">Rs '.$pmtN['Indicative EMI'].'</span>
                                    <span class="bnkInt"> Rs '.$pmtN['Total Interest'].'</span>
                                </label>
                            </div>
                            <div class="blk emiOpts">
                                <label class="blk selected">
                                    <span class="radioBtn">
                                        <input type="radio" name="payment[emi_mode]" class="validate-one-required-by-name" value="EMIIC12">
                                    </span>
                                    <span>12</span>
                                    <span class="bnkInt">13%</span>
                                    <span class="emiAmtTxt">Rs '.$pmtY['Indicative EMI'].'</span>
                                    <span class="bnkInt"> Rs '.$pmtY['Total Interest'].'</span>
                                </label>
                            </div>';*/
           }
       elseif($bankCode=="CITIPM")
           {/*
                    $data= $data. '<div class="blk emiOpts">
                            <label class="blk selected">
                                <span class="radioBtn">
                                    <input type="radio" name="payment[emi_mode]" class="validate-one-required-by-name" value="EMICP3">
                                </span>
                                <span>3</span>
                                <span class="bnkInt">12%</span>
                                <span class="emiAmtTxt">Rs '.$pmtQ['Indicative EMI'].'</span>
                                <span class="bnkInt"> Rs '.$pmtQ['Total Interest'].'</span>
                            </label>
                        </div>
                        <div class="blk emiOpts">
                            <label class="blk selected">
                                <span class="radioBtn">
                                    <input type="radio" name="payment[emi_mode]" class="validate-one-required-by-name" value="EMICP6">
                                </span>
                                <span>6</span>
                                <span class="bnkInt">12%</span>
                                <span class="emiAmtTxt">Rs '.$pmtH['Indicative EMI'].'</span>
                                <span class="bnkInt"> Rs '.$pmtH['Total Interest'].'</span>
                            </label>
                        </div>';
                        <div class="blk emiOpts">
                            <label class="blk selected">
                                <span class="radioBtn">
                                    <input type="radio" name="payment[emi_mode]" class="validate-one-required-by-name" value="EMICP3-C">
                                </span>
                                <span>3</span>
                                <span class="bnkInt">12%</span>
                                <span class="emiAmtTxt">Rs '.$pmtQ['Indicative EMI'].'</span>
                                <span class="bnkInt"> Rs '.$pmtQ['Total Interest'].'</span>
                            </label>
                        </div>
                        <div class="blk emiOpts">
                            <label class="blk selected">
                                <span class="radioBtn">
                                    <input type="radio" name="payment[emi_mode]" class="validate-one-required-by-name" value="EMICP3-M">
                                </span>
                                <span>3</span>
                                <span class="bnkInt">12%</span>
                                <span class="emiAmtTxt">Rs '.$pmtQ['Indicative EMI'].'</span>
                                    <span class="bnkInt"> Rs '.$pmtQ['Total Interest'].'</span>
                            </label>
                        </div>
                         <div class="blk emiOpts">
                            <label class="blk selected">
                                <span class="radioBtn">
                                    <input type="radio" name="payment[emi_mode]" class="validate-one-required-by-name" value="EMICP3-O">
                                </span>
                                <span>3</span>
                                <span class="bnkInt">12%</span>
                               <span class="emiAmtTxt">Rs '.$pmtQ['Indicative EMI'].'</span>
                                    <span class="bnkInt"> Rs '.$pmtQ['Total Interest'].'</span>
                            </label>
                        </div>
                        <div class="blk emiOpts">
                            <label class="blk selected">
                                <span class="radioBtn">
                                    <input type="radio" name="payment[emi_mode]" class="validate-one-required-by-name" value="EMICP6-C">
                                </span>
                                <span>3</span>
                                <span class="bnkInt">12%</span>
                                <span class="emiAmtTxt">Rs '.$pmtH['Indicative EMI'].'</span>
                                    <span class="bnkInt"> Rs '.$pmtH['Total Interest'].'</span>
                            </label>
                        </div>
                        <div class="blk emiOpts">
                            <label class="blk selected">
                                <span class="radioBtn">
                                    <input type="radio" name="payment[emi_mode]" class="validate-one-required-by-name" value="EMICP6-M">
                                </span>
                                <span>3</span>
                                <span class="bnkInt">12%</span>
                                <span class="emiAmtTxt">Rs '.$pmtH['Indicative EMI'].'</span>
                                    <span class="bnkInt"> Rs '.$pmtH['Total Interest'].'</span>
                            </label>
                        </div>
                        <div class="blk emiOpts">
                            <label class="blk selected">
                                <span class="radioBtn">
                                    <input type="radio" name="payment[emi_mode]" class="validate-one-required-by-name" value="EMICP6-O">
                                </span>
                                <span>3</span>
                                <span class="bnkInt">12%</span>
                                <span class="emiAmtTxt">Rs '.$pmtH['Indicative EMI'].'</span>
                                    <span class="bnkInt"> Rs '.$pmtH['Total Interest'].'</span>
                            </label>
                        </div>';*/
           }
       elseif($bankCode=="KOTAK")
           {/*
                $data= $data. '<div class="blk emiOpts">
                                    <label class="blk selected">
                                        <span class="radioBtn">
                                            <input type="radio" name="payment[emi_mode]" class="validate-one-required-by-name" value="EMIK3">
                                        </span>
                                        <span>3</span>
                                        <span class="bnkInt">12%</span>
                                        <span class="emiAmtTxt">Rs '.$pmtQ['Indicative EMI'].'</span>
                                    <span class="bnkInt"> Rs '.$pmtQ['Total Interest'].'</span>
                                    </label>
                                </div>
                                <div class="blk emiOpts">
                                    <label class="blk selected">
                                        <span class="radioBtn">
                                            <input type="radio" name="payment[emi_mode]" class="validate-one-required-by-name" value="EMIK6">
                                        </span>
                                        <span>6</span>
                                        <span class="bnkInt">12%</span>
                                        <span class="emiAmtTxt">Rs '.$pmtH['Indicative EMI'].'</span>
                                    <span class="bnkInt"> Rs '.$pmtH['Total Interest'].'</span>
                                    </label>
                                </div>';
                                <div class="blk emiOpts">
                                    <label class="blk selected">
                                        <span class="radioBtn">
                                            <input type="radio" name="payment[emi_mode]" class="validate-one-required-by-name" value="EMIK9">
                                        </span>
                                        <span>9</span>
                                        <span class="bnkInt">13%</span>
                                        <span class="emiAmtTxt">Rs '.$pmtN['Indicative EMI'].'</span>
                                    <span class="bnkInt"> Rs '.$pmtN['Total Interest'].'</span>
                                    </label>
                                </div>
                                <div class="blk emiOpts">
                                    <label class="blk selected">
                                        <span class="radioBtn">
                                            <input type="radio" name="payment[emi_mode]" class="validate-one-required-by-name" value="EMIK12">
                                        </span>
                                        <span>12</span>
                                        <span class="bnkInt">13%</span>
                                        <span class="emiAmtTxt">Rs '.$pmtY['Indicative EMI'].'</span>
                                    <span class="bnkInt"> Rs '.$pmtY['Total Interest'].'</span>
                                    </label>
                                </div>';*/;
           }
       else
           {
                return false;
           }

                    echo json_encode($data);

    }

}

