<?php


class Amasty_Rma_Block_Adminhtml_Request_Grid_Renderer_Productimage
extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract{


public function render(Varien_Object $row)
    {
        $request_id = $row->getRequestId();
        $product = Mage::getModel('amrma/item')->getCollection()->addFieldToFilter('request_id',array('eq'=>$request_id))->addFieldToSelect('product_id')->getData();
        $productObj = Mage::getModel('catalog/product')->load($product[0]['product_id']);
        $img = Mage::helper('catalog/image')->init($productObj, 'thumbnail');
        $html = '<center><img width="60px" src="'.$img.'" /></center>';
        return $html;

    }


}
