					var make_ajax_call = false; 
					var all_customer_data;
					var mobilenumber = true;
					jQuery(document).ready(function($){

						$('#mobile').live('input', function (event) {

					            this.value = this.value.replace(/[^0-9]/g, '');

					    });

						$('.forgot-pass-submit-button').live('click', function(e){
							e.preventDefault();
							if(make_ajax_call == true) {
								// Show loader and disable submit button
								$('#forgotpass_loader').show();
								$('.forgot-pass-submit-button').prop('disabled', true);

								var serialized_string= $('#forgotpassword_form').serialize();
								$.ajax({
									url: forgotPassword,
									type: 'POST',
									data: serialized_string,
									dataType: 'json',
									success: function(return_data) {

										$('#forgotpass_loader').show();
										$('.forgot-pass-submit-button').prop('disabled', false);

										if(return_data["success"] == true) {
											alert(return_data["message"]);
											$('#email_address').val('');
											$('.forgotpassword').hide();
											$('.youama-login-window .first').show();
											$('.youama-login-window .last .youama-ajaxlogin-button-signin').show();
										} else if(return_data["success"] == false) {
											alert(return_data["error"]);
										}
									}
								});
							}
						});
						return false;
					});


			(function($) {
				$.fn.youamaAjaxLogin = function(options) {

					var opts = $.extend({}, $.fn.youamaAjaxLogin.defaults, options);

					return start();

					function start() {
						removeOriginalJsLocations();
						setSizes();
						responsiveMaker();
						openCloseWindowEvents();
						sendEvents();
					}

					function removeOriginalJsLocations(){
						$('a[href*="customer/account/create"], a[href*="customer/account/login"], .customer-account-login .new-users button').attr('onclick', 'return false;');
					}

					function openCloseWindowEvents(){
						if (opts.autoShowUp == 'yes' && $('.messages').css('display') != 'block'){
							animateShowWindow('login');
						}

						$('a[href*="customer/account/login"], .cms-customer-account a').live('click', function(){
							$('.register-tab').addClass("inactive_element");
							$('.login-tab').removeClass("inactive_element");
							animateShowWindow('login');
							return false;
						});

						$('a[href*="customer/account/create"], .new-users button').live('click', function(){
	                            //animateCloseWindow('login');
	                            $('.register-tab').removeClass("inactive_element");
	                            $('.login-tab').addClass("inactive_element");
	                            animateShowWindow('register');
	                            return false;
	                        });

						$('.ajaxlogin-cover-click').live('click', function(){
							animateCloseWindow('register');
						});

						$('.ajaxlogin-cover-click').live('click', function(){
							animateCloseWindow('login');
						});

						$('.youama-register-close').live('click', function(){
							animateCloseWindow('register');
						});

						$('.youama-login-close').live('click', function(){
							animateCloseWindow('login');
						});

						$('.register-tab').live('click',function(){
							animateCloseWindow('login');
							animateShowWindow('register');
							return false;
						})
						$('.login-tab').live('click',function(){
							animateCloseWindow('register');
							animateShowWindow('login');
							return false;
						})
						$('.youama-ajaxlogin-button-createaccount').live('click',function(){
							animateCloseWindow('login');
							animateShowWindow('register');
							return false;
						})
						$('.already_registered_redirect').live('click',function(){
							animateCloseWindow('register');
							animateShowWindow('login');
							return false;
						})

					}

					function sendEvents(){
						$('.youama-register-window button').live('click', function(){
							setDatas('register');
							if (opts.errors != ''){
								setError(opts.errors, 'register');
							}
							else{
								callAjaxControllerRegistration();
							}
							return false;
						});

						$(document).keypress(function(e) {
							if(e.which == 13 && $('.youama-login-window').css('display') == 'block') {
								setDatas('login');
								if (opts.errors != ''){
									setError(opts.errors, 'login');
								}
								else{
									callAjaxControllerLogin();
								}
							}
						});

						$('.youama-ajaxlogin-button-signin').live('click', function(e){
							e.preventDefault();
							setDatas('login');
							if (opts.errors != ''){
								setError(opts.errors, 'login');
							}
							else{
								callAjaxControllerLogin();
							}
							return false;
						});
					}

					function animateShowWindow(windowName){
						$('.youama-ajaxlogin-cover').show();
						$('.youama-' + windowName + '-window').show();

						var left_offset = ($(window).width() - $('.youama-' + windowName + '-window').width()) / 2;
						$('.youama-register-window, .youama-login-window').css('left', '44%');

						$('.youama-' + windowName + '-window .youama-showhideme').each(function(index, domeE){
							$(this).show();
								// setTop(windowName);
							});

					}


					function animateCloseWindow(windowName){
						if (opts.stop != true){
							$('.youama-ajaxlogin-error').hide();

							var inputs = $('.youama-' + windowName + '-window .youama-showhideme').nextAll();
							var counter = inputs.length;

							$($('.youama-' + windowName + '-window .youama-showhideme').get().reverse()).each(function(index, domeE){
								$(this).hide();
							});

							$('.youama-' + windowName + '-window').hide();
							$('.youama-ajaxlogin-cover').hide();
						}
					}




					function setDatas(windowName){
						if (windowName == 'register'){

							//alert($('#gender').val());
							opts.firstname = $('#firstname').val();
							opts.lastname = $('#lname').val();
							opts.gender = $("input:radio:checked").val();
							opts.mobile = $('#mobile').val();
							opts.dob = $('#month').val() + "/" + $('#day').val() + "/" + $('#year').val();
							opts.newsletter = 'ok';


							opts.email = $('.youama-register-window .second #youama-email-register').val();
							opts.password = $('.youama-register-window .second #youama-password-register').val();
							//opts.passwordsecond = $('.youama-register-window .second #youama-passwordsecond').val();
							opts.licence = 'ok';
						}
						else if (windowName == 'login'){
							opts.email = $('.youama-' + windowName + '-window #youama-email-login').val();
							opts.password = $('.youama-' + windowName + '-window #youama-password-login').val();
						}
					}

					function setError(errors, windowName){
						$('.youama-' + windowName + '-window .youama-ajaxlogin-error').text('');
						$('.youama-' + windowName + '-window .youama-ajaxlogin-error').hide();

						var errorArr = new Array();
						errorArr = errors.split(',');

						var length = errorArr.length - 1;

						for (var i = 0; i < length; i++) {
							var errorText = $('.ytmpa-' + errorArr[i]).text();

							$('.youama-' + windowName + '-window .err-' + errorArr[i]).text(errorText);
						}

						$('.youama-' + windowName + '-window .youama-ajaxlogin-error').fadeIn();
					}

					function callAjaxControllerRegistration(){
						if (opts.stop != true){

							//opts.stop = true;

							//alert(mobilenumber);
							if(make_ajax_call == true) {

							jQuery.fancybox.showLoading();
							var ajaxRegistration = jQuery.ajax({
								url: youama_ajaxlogin_index_index,
								type: 'POST',
								data: {
									ajax : 'register',
									firstname : opts.firstname,
									lastname : opts.lastname,
									newsletter : opts.newsletter,
									email : opts.email,
									password : opts.password,
									//passwordsecond : opts.passwordsecond,
									mobile : opts.mobile,
									dob : opts.dob,
									gender : opts.gender,
									licence : opts.licence
								},
								dataType: "html"
							});

							ajaxRegistration.done(function(msg) {
								jQuery.fancybox.hideLoading();
								if (msg != 'success'){
									setError(msg, 'register');
									if(msg == 'emailisexist,') {
										alert('This e-mail address is already registered with us. Please try a new e-mail address.');
										var emailId = jQuery('#youama-email-register').val();
										jQuery('.youama-register-window').hide();
										animateShowWindow('login');
										$('.register-tab').addClass("inactive_element");
										$('#login-form input').first().val(emailId);
										$('#login-form input:nth-child(2)').focus();

										$('.login-tab').removeClass("inactive_element");
									}
								}
								else{
									//alert(opts.profileUrl);
									opts.stop = false;
									animateCloseWindow('register');
									window.location = opts.profileUrl;
								}
								opts.stop = false;
							});
							ajaxRegistration.fail(function(jqXHR, textStatus, errorThrown) {
								opts.stop = false;
							});

						}
						/*else
						{
							$('#mobile').addClass("validation-failed");
							alert("enter alternate mobile number");

						}*/
					}
					}

					function callAjaxControllerLogin(){
						if (opts.stop != true){


							if(make_ajax_call == true)
							{
							opts.stop = true;

							jQuery.fancybox.showLoading();
							var ajaxRegistration = jQuery.ajax({
								url: youama_ajaxlogin_index_index,
								type: 'POST',
								data: {
									ajax : 'login',
									email : opts.email,
									password : opts.password
								},
								dataType: "html"
							});
							ajaxRegistration.done(function(msg) {
								jQuery.fancybox.hideLoading();
								if (msg != 'success'){
									if(msg == 'wronglogin,' || msg == 'wrongemail,') {
										alert('You have entered a wrong Email ID and/or Password. Please try again.');
									}
									setError(msg, 'login');
								}
								else{
									opts.stop = false;
									animateCloseWindow('login');
									window.location = opts.profileUrl;
								}

								opts.stop = false;
							});
							ajaxRegistration.fail(function(jqXHR, textStatus, errorThrown) {
								opts.stop = false;

							});
					            }
						}
					}

					function responsiveMaker(){
						var position = '';

						if ($.browser.msie && $.browser.version < 9) {
							position = 'absolute';
							$('.youama-ajaxlogin-cover').css('opacity', '0.5');
						}
						else{
							position = 'fixed';
						}

						$(window).resize(function(){
							setSizes(position);
						});
					}

					function setSizes(position){

						var left_offset = ($(window).width() - $('.youama-register-window').width()) / 2;
						$('.youama-register-window, .youama-login-window').css('left', '44%');

						var winHeight = $(window).height();

						if (winHeight < 300){
							$('.youama-login-window').css('top', '50px');
							$('.youama-login-window').css('position', 'absolute');
							setTop('login');
						}
						else{
							$('.youama-login-window').css('position', position);

							if (winHeight < 400){
								$('.youama-login-window').css('top', '25px');
								setTop('login');
							}
							else if (winHeight < 600){
								$('.youama-login-window').css('top', '50px');
								setTop('login');
							}
							else if (winHeight < 800){
								$('.youama-login-window').css('top', '100px');
								setTop('login');
							}
							else if (winHeight < 1000){
								$('.youama-login-window').css('top', '200px');
								setTop('login');
							}
						}

						if (winHeight < 600){
							$('.youama-register-window').css('top', '50px');
							$('.youama-register-window').css('position', 'absolute');
							setTop('register');
						}
						else{
							$('.youama-register-window').css('position', position);

							if (winHeight < 800){
								$('.youama-register-window').css('top', '100px');
								setTop('register');
							}
							else if (winHeight < 1000){
								$('.youama-register-window').css('top', '200px');
								setTop('register');
							}
						}
					}

					function setTop(windowName){
						if ($('.youama-' + windowName + '-window').css('display') == 'block'){
							var height = $('.youama-' + windowName + '-window').height();
							var marginTop = $('.youama-' + windowName + '-window').css('top').replace('px', '');
							var theTop = (parseInt(height) / 2) + parseInt(marginTop);
							$('.youama-ajaxlogin-loader').css('top', parseInt(theTop) + 'px');
							$('.youama-ajaxlogin-loader').css('position', $('.youama-' + windowName + '-window').css('position'));
						}
					}
				};


				$.fn.youamaAjaxLogin.defaults = {
					stop : false,
					profileUrl : current_url,
					autoShowUp : autoShowUp,
					errors : '',
					firstname : '',
					lastname : '',
					newsletter : 'no',
					email : '',
					password : '',
					gender : '',
					mobile : '',
					dob : '',
					licence : 'no'
				};

			})(jQuery);

			jQuery(document).ready(function($){

				/*js to submit login form on enter button*/
				$('#youama-email-register,#youama-password-register,.youama-window-box #mobile').live('keypress', function (event) {
			         var keycode = (event.keyCode ? event.keyCode : event.which);
			        	if(keycode == '13'){
			            $(".youama-ajaxlogin-button").trigger('click');
			        }
			    });

			    $('#youama-email-login,#youama-password-login').live('keypress', function (event) {
			         var keycode = (event.keyCode ? event.keyCode : event.which);
			        	if(keycode == '13'){
			            $(".youama-ajaxlogin-button-signin").trigger('click');
			        }
			    });
			    /*js to submit login form on enter button*/
				//code to display popup based on cookie starts
				function create_cookie(name, value, days2expire, path) {
				  var date = new Date();
				  date.setTime(date.getTime() + (days2expire * 24 * 60 * 60 * 1000));
				  var expires = date.toUTCString();
				  document.cookie = name + '=' + value + ';' +
				                   'expires=' + expires + ';' +
				                   'path=' + path + ';';
				}

				function retrieve_cookie(name) {
					  var cookie_value = "",
					    current_cookie = "",
					    name_expr = name + "=",
					    all_cookies = document.cookie.split(';'),
					    n = all_cookies.length;

					  for(var i = 0; i < n; i++) {
					    //current_cookie = all_cookies[i].trim();
					    current_cookie = all_cookies[i].replace(/^\s+|\s+$/g, '');
					    if(current_cookie.indexOf(name_expr) == 0) {
					      cookie_value = current_cookie.substring(name_expr.length, current_cookie.length);
					      break;
					    }
					  }
					  return cookie_value;
				}


				function animateShowWindow(windowName){
						$('.youama-ajaxlogin-cover').show();
						$('.youama-' + windowName + '-window').show();

						var left_offset = ($(window).width() - $('.youama-' + windowName + '-window').width()) / 2;
						$('.youama-register-window, .youama-login-window').css('left', '44%');

						$('.youama-' + windowName + '-window .youama-showhideme').each(function(index, domeE){
							$(this).show();
								// setTop(windowName);
						});

				}

				function animateCloseWindow(windowName){

							var inputs = $('.youama-' + windowName + '-window .youama-showhideme').nextAll();
							var counter = inputs.length;

							$($('.youama-' + windowName + '-window .youama-showhideme').get().reverse()).each(function(index, domeE){
								$(this).hide();
							});

							$('.youama-' + windowName + '-window').hide();
							$('.youama-ajaxlogin-cover').hide();

					}

				$('.youama-register-close youama-close').live('click', function(){
							animateCloseWindow('register');
				});


				// 
				/*var res = retrieve_cookie("signup_pop");

				if(!res)
				{

					create_cookie("signup_pop", "1", 365, "/");
					animateShowWindow('register');

					$('.register-tab').removeClass("inactive_element");
					$('.login-tab').addClass("inactive_element");
				}*/


				//code to display popup based on cookie ends

				jQuery().youamaAjaxLogin();


				//$('.register-tab').addClass("inactive_element");
				$('.login-tab').click(function(){
					$('.register-tab').addClass("inactive_element");
					$('.login-tab').removeClass("inactive_element");
				});

				$('.register-tab').click(function(){
					$('.login-tab').addClass("inactive_element");
					$('.register-tab').removeClass("inactive_element");
				});
				$('.youama-ajaxlogin-button-createaccount').click(function(){
					$('.login-tab').addClass("inactive_element");
					$('.register-tab').removeClass("inactive_element");
				});
				$('.already_registered_redirect').click(function(){
					$('.register-tab').addClass("inactive_element");
					$('.login-tab').removeClass("inactive_element");
				});


		        $('#email_address').live('focus', function () {
        			$('#email_address').val($('#youama-email-login').val());
                });
				// $('.forgot_password_link').click(function(){

				// 	//$('.youama-login-window .first').hide();
				// 	if($('.forgotpassword').css('display')=='none'){
				// 		//show
				// 		//$('#email_address').val($('#youama-email-login').val());

				// 		$('.forgotpassword').show("normal");
				// 		var ht = parseInt($('.youama-login-window').height()/2);
				// 		var cr_margin = parseInt($('.youama-login-window').css('margin-top'));
				// 		var margin = (cr_margin - (ht/2));
				// 		$('.youama-login-window').css('margin-top',margin);

				// 	}else{
				// 		$('.forgotpassword').hide("normal");
				// 		$('.youama-login-window').css('margin-top','-126px');
				// 		$('.youama-login-window .last .youama-ajaxlogin-button-signin').show();
				// 	}

				// });

				/*$('.back-link-tologin').click(function(){
					$('.forgotpassword').hide();
					$('.youama-login-window .first').show();
					$('.youama-login-window .last .youama-ajaxlogin-button-signin').show();
				});*/

$("a[href='#']").click(function(event){
	event.preventDefault();
});

});
