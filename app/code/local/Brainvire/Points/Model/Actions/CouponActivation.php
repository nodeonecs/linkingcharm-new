<?php
/**
* Brainvire Infotech Pvt. Ltd
  * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */


class Brainvire_Points_Model_Actions_CouponActivation extends Brainvire_Points_Model_Actions_Abstract {

    protected $_action = 'coupon_activation';
    protected $_comment = 'Coupon activation';

}