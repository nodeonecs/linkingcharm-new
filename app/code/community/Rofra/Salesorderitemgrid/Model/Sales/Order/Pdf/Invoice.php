<?php

require_once('lib/Tcpdf/tcpdf.php');
class Rofra_Salesorderitemgrid_Model_Sales_Order_Pdf_Invoice extends Mage_Sales_Model_Order_Pdf_Invoice
{

    public function getPdf($invoiceArray = array())
    {
        $this->_beforeGetPdf();
        $this->_initRenderer('invoice');
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false,false ,false);//last parameter for  footer  if true then footer show. if false then footer not show
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetTitle("Invoice");
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $pdf->SetMargins(10,15,10,TRUE);
        $pdf->SetAutoPageBreak(TRUE, 15);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->setLanguageArray($l);
        $pdf->SetFont('dejavusans', 'B', 20);
        $pdf->SetFont('dejavusans', '', 8);

        $this->_setPdf($pdf);
        
        /*echo "<pre>";
        print_r($invoiceArray);*/
        foreach ($invoiceArray as $invoices)
        {
            $total_qty = 0;
            foreach ($invoices->getAllItems() as $item){
                if ($item->getOrderItem()->getParentItem()) {
                    continue;
                }
                $taxPercent = $item->getOrderItem()->getTaxPercent();
                $invoice_tax_percent[] = number_format($taxPercent,2);
                $taxPercent = number_format($taxPercent,2) . '%';
                $total_qty += $item['qty'];
                $total_cost = $item['base_row_total'];
                $product[] = array('name'=>$item['name'],'productcode'=>$item['sku'],'qty'=>$item['qty'],'net_amount'=>$total_cost,'total_cost'=>$total_cost*$item['qty'],'tax'=>$taxPercent);

            }
            
            $invoice_tax = array_unique($invoice_tax_percent);
            foreach ($invoice_tax as $value) {
               $invoice_tax_array[] = "@ ".$value." %";
            }

            $pdf->AddPage();
            $order = $invoices->getOrder();
            $taxAmount          = number_format($invoices->getData('tax_amount'));

            $billingAdd     = $invoices->getBillingAddress()->getData();
                    //echo '<pre>';print_r($billingAdd);
            $b_name         = $billingAdd['firstname'].' '.$billingAdd['lastname'];
            $b_street       = $billingAdd['street'];
                    //echo $b_street." ";
            $b_city         = $billingAdd['city'];
            $b_postcode     = $billingAdd['postcode'];
            $b_region         = $billingAdd['region'];
            $telephone      = $billingAdd['telephone'];
            $fax_billing            = $billingAdd['fax'];
            $address = Mage::getModel('customer/address')->load($billingAdd['customer_address_id']);
            $landmark_billing       = $address->getData('landmark');
            $landline_billing       = $address->getData('telephonetwo');
            $shipping_amount = $invoices->getBaseShippingAmount();
            $cod_fee = $invoices->getBaseCodFee();
            // $base_grand_total = $invoices->getBaseGrandTotal();
            $base_grand_total = $invoices->getBaseGrandTotal();
            $base_discount_amount = $invoices->getBaseDiscountAmount();
            $base_subtotal_incl_tax = $invoices->getBaseSubtotalInclTax();
            $base_tax_amount = $invoices->getBaseTaxAmount();
            $order_value = $base_subtotal_incl_tax - $base_tax_amount;

            /*Invoice Detail*/
            $invoiceNo      = $invoices->getIncrementId();
            $invoiceDate    = date('d/m/Y', $invoices->getCreatedAtDate()->getTimestamp());

            /*Order Detal*/
                    $orderId        = $order->getData('increment_id');//order no
                    $orderDate      = date('d/m/Y', strtotime($order->getData('created_at')));

                    /* Shipping Address & Shipping & Tracking Detail*/
                    $shippingAdd    = $invoices->getShippingAddress()->getData();
                    //echo '<pre>';print_r($shippingAdd);exit;
                    $filename       = $shippingAdd['firstname'].$shippingAdd['lastname'];
                    $c_name         = $shippingAdd['firstname'].' '.$shippingAdd['lastname'];//customer name
                    $street         = $shippingAdd['street'];//shipn add
                    //echo $street; exit;
                    $city           = $shippingAdd['city'];
                    $postcode       = $shippingAdd['postcode'];
                    $region         = $shippingAdd['region'];
                    //echo $region;exit;
                    /*if(strtolower ($region) == 'maharashtra')
                    {
                        $tax_type = 'VAT ';
                    }
                    else
                    {
                        $tax_type = 'CST ';
                    }*/
                    $tax_info_order = $order->getFullTaxInfo();
                    $tax_type = $tax_info_order[0]['rates'][0]['title'];

                    //echo $tax_type;exit;
                    $telephone      = $shippingAdd['telephone'];
                    $fax_shipping           = $shippingAdd['fax'];
                    $address = Mage::getModel('customer/address')->load($shippingAdd['customer_address_id']);
                    $landmark_shipping      = $address->getData('landmark');
                    $landline_shipping      = $address->getData('telephonetwo');

                    $shipping_no    = '&nbsp;';
                    $shipping_Date  = '&nbsp;';
                    $track_title    = '&nbsp;';
                    $track_number   = '&nbsp;';
                    if ($order->hasShipments()) {
                        foreach ($order->getShipmentsCollection() as $_eachShipment) {
                            $shipping_no = $_eachShipment->getData('increment_id');
                            $shipping_Date =date('d/m/Y', strtotime($_eachShipment->getData('created_at')));


                            $trackArr = $_eachShipment->getTracksCollection()->getData();
                            //$carrier_code = $trackArr[0]['carrier_code'];
                            $track_title = $trackArr[0]['title'];
                            $track_number = $trackArr[0]['track_number'];

                        }
                    }
                    /*Payment Detal*/
                    $paymentMethod  = $order->getPayment()->getMethodInstance()->getTitle();

                    $img_url = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB, true )."media/sales/store/logo/".
                    Mage::getStoreConfig('sales/identity/logo');
                    $CompanyAddress = Mage::getStoreConfig('general/store_information/address');
                    $CompanyName = Mage::getStoreConfig('general/store_information/name')." ";
                    // $iLoginuserid = Mage::getSingleton('admin/session')->getUser()->getUserId();
                    // $LoginUserRole = Mage::getModel('admin/user')->load($iLoginuserid)->getRole()->getRoleId();

                    // $iRole_Collectionsid = Mage::getSingleton('amrolepermissions/rule')
                    // ->getCollection()->addFieldToFilter('role_id' , array('eq' => $LoginUserRole));

                    // foreach($iRole_Collectionsid as $Role)
                    // {
                    //     $scopeviews = $Role->getScopeStoreviews();
                    // }
                        
                        $scopeviews = $invoices->getData('store_scopeview');
                        $group_id = Mage::getSingleton('core/store')->load($scopeviews)->getGroupId();
                        $store_Data = Mage::getSingleton('core/store_group')->load($group_id);

                        $VATNo = $store_Data->getData('store_vat');
                        $TINNo = $store_Data->getData('store_tin');
                        $CSTNo = $store_Data->getData('store_cst');
                        $PanNo = $store_Data->getData('store_pan');
                        $CinNo = $store_Data->getData('store_cin');

                    $HTML .= '<table border="0" cellpadding="0" cellspacing="0" width="100%" style="font-family: arial">
                    <tr>
                    <td>
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                    <td width="40%" align="left" valign="top"><img src="'.$img_url.'" /></td>
                    <td width="60%" align="left" valign="top" style="font-size: 32px">
                    <p>'.$CompanyName.$CompanyAddress.'</p>
                    <p>Tin No : '.$TINNo.' , PAN No : '.$PanNo.'
                    <br>CST No : '.$CSTNo.' , S.T.No : '.$VATNo.
                    '<br /> CIN No : '.$CinNo.'</p>
                    </td>
                    </tr>
                    </table>
                    </td>
                    </tr>

                    <tr>
                    <td style="text-align:center; padding-top: 10px; padding-bottom: 20px; font-size: 48px;"><p style="margin:0; padding:0; line-height: 2px">&nbsp;</p><strong>TAX INVOICE</strong><p style="margin:0; padding:0; line-height: 2px">&nbsp;</p></td>
                    </tr>

                    <tr>
                    <td>
                    <table cellpadding="0" cellspacing="0" width="100%" style="border:1px solid #000000;">
                    <tr>
                    <td width="50%" style="border-right:1px solid #000000;">
                    <table cellpadding="2" cellspacing="0" width="100%" style="font-size: 30px; margin: 10px;">
                    <tr>
                    <td width="129" align="left" valign="top" style=" ">Order Number:</td>
                    <td width="370" align="left" valign="top" style="">'.$orderId.'</td>
                    </tr>
                    <tr>
                    <td align="left" valign="top" style="">Invoice Number:</td>
                    <td align="left" valign="top" style="">'.$invoiceNo.'</td>
                    </tr>
                    <tr>
                    <td style=" ">Order Date:</td>
                    <td style="">'.$orderDate.'</td>
                    </tr>
                    <tr>
                    <td style="">Invoice Date:</td>
                    <td style="">'.$invoiceDate.'</td>
                    </tr>
                    </table>
                    </td>
                    <td width="50%" align="left" valign="top">
                    <table cellpadding="4" cellspacing="0" width="100%" style="font-size: 30px">
                    <tr>
                    <td width="154" align="left" valign="top" style=" ">Payment Method:</td>
                    <td width="377" align="left" valign="top" style="">'.$paymentMethod.'</td>
                    </tr>
                    </table>
                    </td>
                    </tr>
                    </table>
                    </td>
                    </tr>

                    <tr>
                    <td>
                    <table cellpadding="2" cellspacing="0" width="100%" style="border:1px solid #000000; font-size:30px; margin: 10px;">
                    <tr>
                    <td style="line-height: 2px;  border-right: 1px solid #000000;">&nbsp;</td>
                    <td style="line-height: 2px;">&nbsp;</td>
                    </tr>
                    <tr>
                    <td style="text-transform: uppercase; border-bottom: 1px solid #000000; border-right: 1px solid #000000; padding-left: 10px;"><strong>SHIP TO:</strong></td>
                    <td style="text-transform: uppercase; border-bottom: 1px solid #000000; padding-left: 10px;"><strong>BILL TO:</strong></td>
                    </tr>
                    <tr>
                    <td style="border-right: 1px solid #000000; padding-left: 10px">'.$c_name.'</td>
                    <td style="padding-left: 10px">'.$b_name.'</td>
                    </tr>
                    <tr>
                    <td style="border-right: 1px solid #000000; padding-left: 10px">'.$street.'</td>
                    <td style="padding-left: 10px">'.$b_street.'</td>
                    </tr>
                    <tr>
                    <td style="border-right: 1px solid #000000; padding-left: 10px">'.$city.'</td>
                    <td style="padding-left: 10px">'.$b_city.'</td>
                    </tr>
                    <tr>
                    <td style="border-right: 1px solid #000000; padding-left: 10px">'.$region.'</td>
                    <td style="padding-left: 10px">'.$region.'</td>
                    </tr>
                    <tr>
                    <td style="border-right: 1px solid #000000; padding-left: 10px">'.$postcode.'</td>
                    <td style="padding-left: 10px">'.$b_postcode.'</td>
                    </tr>
                    <tr>
                    <td style="border-right: 1px solid #000000; padding-left: 10px">T : '.$telephone.'</td>
                    <td style="padding-left: 10px"> T : '.$telephone.'</td>
                    </tr>
                    </table>
                    </td>
                    </tr>

                    <tr>
                    <td style="">
                    <table width="100%" cellpadding="4" align="center" cellspacing="0" style="font-size: 30px; border-right: 1px solid #000000; border-left: 1px solid #000000;">
                    <tr>
                    <td style="line-height: 2px;">&nbsp;</td>
                    <td style="line-height: 2px;">&nbsp;</td>
                    <td style="line-height: 2px;">&nbsp;</td>
                    <td style="line-height: 2px;">&nbsp;</td>
                    <td style="line-height: 2px;">&nbsp;</td>
                    <td style="line-height: 2px;">&nbsp;</td>
                    <td style="line-height: 2px;">&nbsp;</td>
                    <td style="line-height: 2px;">&nbsp;</td>
                    </tr>
                    <tr style="text-align:center;">
                    <td align="left" valign="top" style="border-bottom: 1px solid #000000" width="10%"><strong>Sr No</strong></td>
                    <td align="left" valign="top" style="border-bottom: 1px solid #000000" width="30%"><strong>Product Code</strong></td>
                    <td align="left" valign="top" style="border-bottom: 1px solid #000000" width="20%"><strong>Product Name</strong></td>
                    <td align="left" valign="top" style="border-bottom: 1px solid #000000" width="10%"><strong>Tax</strong></td>
                    <td align="left" valign="top" style="border-bottom: 1px solid #000000" width="10%"><strong>Net Amount</strong></td>
                    <td align="left" valign="top" style="border-bottom: 1px solid #000000" width="5%"><strong>Qty</strong></td>
                    <td align="left" valign="top" style="border-bottom: 1px solid #000000" width="10%"><strong>Total Net</strong></td>
                    </tr>
                    ';

                    /* Add body */
                    $count = 0;
                    foreach($product as $value){
                        $count++;
                        $HTML .='

                        <tr style="text-align:center;">
                        <td align="left" valign="top" style="padding: 5px 0"> '.$count.'</td>
                        <td align="left" valign="top" style="padding: 5px 0"> '.$value['productcode'].' </td>

                        <td align="left" valign="top" style="padding: 5px 0">'.$value['name'].'</td>
                        <td align="left" valign="top" style="padding: 5px 0">'.$value['tax'].'</td>
                        <td align="left" valign="top" style="padding: 5px 0">'.Mage::helper('core')->currency($value['net_amount'], true, false).'</td>
                        <td align="left" valign="top" style="padding: 5px 0">'.number_format($value['qty']).'</td>
                        <td align="left" valign="top" style="padding: 5px 0">'.Mage::helper('core')->currency($value['total_cost'], true, false).'</td>
                        </tr>
                        ';


                    }


                    $HTML .='
                    </table>
                    </td>
                    </tr>

                    <tr>
                    <td>
                    <table width="100%" cellpadding="8" cellspacing="0" border="0" style="font-size: 32px; margin-top: 20px; border-right: 1px solid #000000; border-left: 1px solid #000000; border-bottom: 1px solid #000000">
                    <tr>
                    <td width="65%" style="padding: 10px"><P>&nbsp;</P><P>&nbsp;</P>We hereby certify that our registration certificate under the Maharashtra Value Added TaxAct, 2002 is in force on the date on which the sale of the goods specified in this tax invoiceis made by us and that the transaction of sale covered by this tax invoice has been effectedby us and it shall be accounted for in the turnover of sales while filing of return and the duetax, if any, payable on the sale has been paid or shall be paid.
                    </td>

                    <td width="35%" style="padding: 10px">

                    <table width="100%" cellpadding="2" cellspacing="0" style="font-size: 30px;">
                    <tr>
                    <td align="right"><strong>Total Qty</strong></td>
                    <td align="right">'.number_format($total_qty).'</td>
                    </tr>
                    <tr>
                    <td align="right"><strong>Order Value</strong></td>
                    <td align="right">'.Mage::helper('core')->currency(abs($order_value), true, false).'</td>
                    </tr>';
                   $HTML .= '<tr>
                   <td align="right"><strong>Tax '.implode(",", $invoice_tax_array).' ('.$tax_type.')</strong></td>
                   <td align="right">'.Mage::helper('core')->currency($base_tax_amount, true, false).'</td>
                   </tr>
                   <tr>
                   <td align="right"><strong>Gross Order Value</strong></td>
                   <td align="right">'.Mage::helper('core')->currency($base_subtotal_incl_tax, true, false).'</td>
                   </tr>
                   <tr>
                   <td align="right"><strong>COD Charge</strong></td>
                   <td align="right">'.Mage::helper('core')->currency($cod_fee, true, false).'</td>
                   </tr>
                   <tr>
                   <td align="right"><strong>Shipping Cost</strong></td>
                   <td align="right">'.Mage::helper('core')->currency($shipping_amount, true, false).'</td>
                   </tr>
                   <tr>
                   <td align="right"><strong>Discount Amount</strong></td>
                   <td align="right">'.Mage::helper('core')->currency($base_discount_amount, true, false).'</td>
                   </tr>
                   <tr>
                   <td align="right"><strong>Grand Total(Inclusive of '.$tax_type.')</strong></td>
                   <td align="right">'.Mage::helper('core')->currency($base_grand_total, true, false).'</td>
                   </tr>
                   </table>
                   </td>
                   </tr>
                   </table>
                   </td>
                   </tr>

                   <tr>
                   <td>
                   <table width="100%" cellpadding="2" cellspacing="0" style="font-size: 30px">
                   <tr>
                   <td style="padding-bottom: 30px; font-size: 40px; padding-top: 10px;">
                   <p style="line-height:0px; padding: 0; margin: 0">&nbsp;</p>
                   <strong>'.number_format($base_grand_total, 2).' Rupees Only </strong>
                   </td>
                   </tr>
                   <tr><td>1.The order value is inclusive of all taxes.</td></tr>
                   <tr><td>2.This is computer generated invoice there is no authorised signature required.</td></tr>
                   <tr><td>3.Post Shipment, the delivery will take anywhere between 5 to 7 days depending on the location being served.</td></tr>
                   <tr><td>4.Refer Company\'s web site www.linkingcharms.com for other terms and conditions.</td></tr>
                   <tr><td align="center" style="padding-top:10px; font-size: 42px;">
                   <p style="line-height:0px; padding: 0; margin: 0">&nbsp;</p>
                   <strong style="text-decoration: underline; ">Thanks for Shopping</strong></td>
                   </tr>
                   </table>
                   </td>
                   </tr>

                   </table>';
                   if ($invoices->getStoreId()) {
                    Mage::app()->getLocale()->revert();
                }
                   //echo $HTML;
                   //exit;
                $pdf->writeHTML($HTML, true, false, false, false, '');

                //}
            }


        // $pdf->writeHTML($HTML, true, false, false, false, '');

        //Close and output PDF document
        //$fileName = 'invoice-'.date('d_m_Y').'.pdf';
            ob_end_clean();
            $fileName = $filename.'_invoice.pdf';
            $pdf->Output($fileName, 'D');

            return $pdf;
        }
    }
