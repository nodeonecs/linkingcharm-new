<?php

class Aster_Banners_Block_Adminhtml_Banners_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("bannersGrid");
				$this->setDefaultSort("banners_id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("banners/banners")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("banners_id", array(
				"header" => Mage::helper("banners")->__("ID"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "number",
				"index" => "banners_id",
				));
                
				$this->addColumn("banner_title", array(
				"header" => Mage::helper("banners")->__("Title"),
				"index" => "banner_title",
				));
				$this->addColumn("banner_link", array(
				"header" => Mage::helper("banners")->__("Link"),
				"index" => "banner_link",
				));
						$this->addColumn('banner_type', array(
						'header' => Mage::helper('banners')->__('Type'),
						'index' => 'banner_type',
						'type' => 'options',
						'options'=>Aster_Banners_Block_Adminhtml_Banners_Grid::getOptionArray4(),				
						));
						
						$this->addColumn('banner_status', array(
						'header' => Mage::helper('banners')->__('Status'),
						'index' => 'banner_status',
						'type' => 'options',
						'options'=>Aster_Banners_Block_Adminhtml_Banners_Grid::getOptionArray5(),				
						));
						
				$this->addColumn("sort_order", array(
				"header" => Mage::helper("banners")->__("Sort Order"),
				"index" => "sort_order",
				));
					$this->addColumn('start_date', array(
						'header'    => Mage::helper('banners')->__('Start Date'),
						'index'     => 'start_date',
						'type'      => 'datetime',
					));
					$this->addColumn('end_date', array(
						'header'    => Mage::helper('banners')->__('End Date'),
						'index'     => 'end_date',
						'type'      => 'datetime',
					));
			$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV')); 
			$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}


		
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('banners_id');
			$this->getMassactionBlock()->setFormFieldName('banners_ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_banners', array(
					 'label'=> Mage::helper('banners')->__('Remove Banners'),
					 'url'  => $this->getUrl('*/adminhtml_banners/massRemove'),
					 'confirm' => Mage::helper('banners')->__('Are you sure?')
				));
			return $this;
		}
			
		static public function getOptionArray4()
		{
            $data_array=array(); 
			$data_array[0]='Desktop';
			$data_array[1]='Mobile';
            return($data_array);
		}
		static public function getValueArray4()
		{
            $data_array=array();
			foreach(Aster_Banners_Block_Adminhtml_Banners_Grid::getOptionArray4() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);

		}
		
		static public function getOptionArray5()
		{
            $data_array=array(); 
			$data_array[0]='Disabled';
			$data_array[1]='Enabled';
            return($data_array);
		}
		static public function getValueArray5()
		{
            $data_array=array();
			foreach(Aster_Banners_Block_Adminhtml_Banners_Grid::getOptionArray5() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);

		}
		
		static public function getOptionArray9()
		{
            $data_array=array(); 
			$data_array[0]='English';
            return($data_array);
		}
		static public function getValueArray9()
		{
            $data_array=array();
			foreach(Aster_Banners_Block_Adminhtml_Banners_Grid::getOptionArray9() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);

		}
		

}