<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   Brainvire
 * @package    Brainvire_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * @license    
 */


class Brainvire_Points_Model_Mysql4_Rule_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract {

    public function _construct() {
        $this->_init('points/rule');
    }

    public function addAvailableFilter() {
        $currentDate = Mage::getModel('core/date')->gmtDate('Y-m-d');
        $this->getSelect()
                ->where('is_active = ?', 1)
                ->where('date(from_date) <= ?', $currentDate)
                ->where('date(to_date) >= ? OR to_date is null', $currentDate);
        return $this;
    }

    public function addFilterByCustomerGroup($customerGroupId) {
        $this->getSelect()
                ->where('FIND_IN_SET(?, customer_group_ids)', $customerGroupId);
        return $this;
    }

    public function addFilterByWebsiteId($websiteId) {
        $this->getSelect()
                ->where('FIND_IN_SET(?, website_ids)', $websiteId);
        return $this;
    }

}
