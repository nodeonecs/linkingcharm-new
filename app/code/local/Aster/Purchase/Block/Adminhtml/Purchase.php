<?php


class Aster_Purchase_Block_Adminhtml_Purchase extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_purchase";
	$this->_blockGroup = "purchase";
	$this->_headerText = Mage::helper("purchase")->__("Purchase Manager");
	$this->_addButtonLabel = Mage::helper("purchase")->__("Add New Item");
	parent::__construct();
	$this->_removeButton('add');
	}

}