<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_Model_Source_Giftcard_History_Action
    implements AW_Giftcard2_Model_Source_SourceInterface
{
    const CREATED_VALUE         = 1;
    const UPDATED_VALUE         = 2;
    const USED_VALUE            = 3;
    const PARTIALLY_USED_VALUE  = 4;
    const EXPIRED_VALUE         = 5;
    const DEACTIVATED_VALUE     = 6;

    const CREATED_LABEL             = 'Created';
    const UPDATED_LABEL             = 'Updated';
    const USED_LABEL                = 'Used';
    const PARTIALLY_USED_LABEL      = 'Partially Used';
    const EXPIRED_LABEL             = 'Expired';
    const DEACTIVATED_LABEL         = 'Deactivated';

    const BY_ADMIN_MESSAGE_VALUE            = 0;
    const BY_ORDER_MESSAGE_VALUE            = 1;
    const BY_CREDITMEMO_MESSAGE_VALUE       = 2;
    const BY_REDEEM_TO_STORECREDIT_VALUE    = 3;
    const BY_IMPORT_MESSAGE_VALUE           = 4;

    const BY_ADMIN_MESSAGE_LABEL            = 'By admin: %s.';
    const BY_ORDER_MESSAGE_LABEL            = 'Order #%s.';
    const BY_CREDITMEMO_MESSAGE_LABEL       = 'Refund for order #%s.';
    const BY_IMPORT_MESSAGE_LABEL           = 'Imported from 1.x version of the module';

    /**
     * @return array
     */
    public function toArray()
    {
        $helper = Mage::helper('aw_giftcard2');
        return array(
            self::CREATED_VALUE => $helper->__(self::CREATED_LABEL),
            self::UPDATED_VALUE => $helper->__(self::UPDATED_LABEL),
            self::USED_VALUE => $helper->__(self::USED_LABEL),
            self::PARTIALLY_USED_VALUE => $helper->__(self::PARTIALLY_USED_LABEL),
            self::EXPIRED_VALUE => $helper->__(self::EXPIRED_LABEL),
            self::DEACTIVATED_VALUE => $helper->__(self::DEACTIVATED_LABEL),
        );
    }

    public function getOptionText($value)
    {
        $options = $this->toArray();
        if (array_key_exists($value, $options)) {
            return $options[$value];
        }
        return null;
    }

    /**
     * @param $messageType
     * @return string
     */
    public function getMessageLabelByType($messageType)
    {
        $label = '';
        switch ($messageType) {
            case self::BY_ADMIN_MESSAGE_VALUE :
                $label = self::BY_ADMIN_MESSAGE_LABEL;
                break;
            case self::BY_ORDER_MESSAGE_VALUE :
                $label = self::BY_ORDER_MESSAGE_LABEL;
                break;
            case self::BY_CREDITMEMO_MESSAGE_VALUE :
                $label = self::BY_CREDITMEMO_MESSAGE_LABEL;
                break;
            case self::BY_IMPORT_MESSAGE_VALUE :
                $label = self::BY_IMPORT_MESSAGE_LABEL;
                break;
        }
        return $label;
    }
}