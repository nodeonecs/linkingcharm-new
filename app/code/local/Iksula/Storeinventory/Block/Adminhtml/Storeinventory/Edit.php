<?php
	
class Iksula_Storeinventory_Block_Adminhtml_Storeinventory_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
		public function __construct()
		{

				parent::__construct();
				$this->_objectId = "entity_id";
				$this->_blockGroup = "storeinventory";
				$this->_controller = "adminhtml_storeinventory";
				$this->_updateButton("save", "label", Mage::helper("storeinventory")->__("Save Item"));
				$this->_updateButton("delete", "label", Mage::helper("storeinventory")->__("Delete Item"));

				$this->_addButton("saveandcontinue", array(
					"label"     => Mage::helper("storeinventory")->__("Save And Continue Edit"),
					"onclick"   => "saveAndContinueEdit()",
					"class"     => "save",
				), -100);



				$this->_formScripts[] = "

							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
						";
		}

		public function getHeaderText()
		{
				if( Mage::registry("storeinventory_data") && Mage::registry("storeinventory_data")->getId() ){

				    return Mage::helper("storeinventory")->__("Edit Item '%s'", $this->htmlEscape(Mage::registry("storeinventory_data")->getId()));

				} 
				else{

				     return Mage::helper("storeinventory")->__("Add Item");

				}
		}
}