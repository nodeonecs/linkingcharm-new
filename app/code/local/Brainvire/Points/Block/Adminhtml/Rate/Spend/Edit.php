<?php
/**
* Brainvire Infotech Pvt. Ltd
  * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */


class Brainvire_Points_Block_Adminhtml_Rate_Spend_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

    public function __construct() {
        $this->_objectId = 'id';
        $this->_blockGroup = 'points';
        $this->_controller = 'adminhtml_rate_spend';

        parent::__construct();

        $this->_updateButton('save', 'label', Mage::helper('points')->__('Save Rate'));
        $this->_updateButton('delete', 'label', Mage::helper('points')->__('Delete Rate'));
    }

    public function getHeaderText() {
        $rate = Mage::registry('points_rate_data');
        if ($rate->getId()) {
            return Mage::helper('points')->__("Edit Rate #%s", $this->htmlEscape($rate->getId()));
        } else {
            return Mage::helper('points')->__('Add Rate');
        }
    }

}
