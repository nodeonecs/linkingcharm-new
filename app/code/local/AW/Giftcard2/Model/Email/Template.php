<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Giftcard2_Model_Email_Template extends Mage_Core_Model_Email_Template
{
    protected $emailTemplate;
    
    const PATH_TO_EMAIL_IMAGES = 'aw_giftcard2/images';

    protected function _construct()
    {
        $this->emailTemplate = Mage::getModel('core/email_template');
        parent::_construct();
    }

    public function sendGiftCardCode(AW_Giftcard2_Model_Giftcard $giftCardCode)
    {
        if ($giftCardCode->getEmailTemplate() != AW_Giftcard2_Model_Source_Giftcard_Email_Template::DO_NOT_SEND_VALUE) {
            $giftCardCodeOrder = null;
            if ($giftCardCode->getOrderId()) {
                $giftCardCodeOrder = Mage::getModel('sales/order')->load($giftCardCode->getOrderId());
                $store = $giftCardCodeOrder->getStore();
            } else {
                $website = Mage::app()->getWebsite($giftCardCode->getWebsiteId());
                $_stores = $website->getStores();
                $store = array_shift($_stores);
            }

            if (!$store->getId()) {
                throw new Mage_Core_Exception(Mage::helper('aw_giftcard2')->__('Unable to send email'));
            }
            if (!$giftCardCode->isActive()) {
                throw new Mage_Core_Exception(Mage::helper('aw_giftcard2')->__('Unable to send email: gift card code is not active'));
            }
            $currencyCode = $store->getCurrentCurrencyCode();
            if ($giftCardCodeOrder) {
                try {
                    $currencyCode = $giftCardCodeOrder->getOrderCurrencyCode();
                } catch (Exception $e) {
                }
            }
            $data = array(
                'store_id' => $store->getId(),
                AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_EMAIL_TEMPLATE => $giftCardCode->getEmailTemplate(),
                AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_RECIPIENT_NAME => $giftCardCode->getRecipientName(),
                AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_RECIPIENT_EMAIL => $giftCardCode->getRecipientEmail(),
                AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_SENDER_NAME => $giftCardCode->getSenderName(),
                AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_SENDER_EMAIL => $giftCardCode->getSenderEmail(),
                'giftcard_codes' => array($giftCardCode->getCode()),
                'balance' => $giftCardCode->getBalance(),
                'currency_code' => $currencyCode,
                'expire_at' => $giftCardCode->getExpireAt()
            );
            $this->prepareEmailAndSend($data);
        }
    }

    /**
     * Prepare email data and send Gift Card email
     *
     * @param array $data
     * @return Mage_Core_Model_Email_Template
     * @throws Mage_Core_Exception
     */
    public function prepareEmailAndSend(array $data)
    {
        $store = Mage::app()->getStore($data['store_id']);
        $this->emailTemplate->setDesignConfig(array('store' => $store->getId()));

        $template = $data[AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_EMAIL_TEMPLATE];

        $variables = $this->prepareTemplateVars($data);

        return $this->emailTemplate->sendTransactional(
            $template,
            Mage::helper('aw_giftcard2/config')->getEmailSender($store),
            $variables['recipient_email'],
            $variables['recipient_name'],
            $variables
        );
    }

    /**
     * @param array $data
     * @return array
     */
    protected function prepareTemplateVars($data)
    {
        $templateVars = array();

        $store = Mage::app()->getStore($data['store_id']);
        $templateVars['store'] = $store;
        $templateVars['store_name'] = $store->getName();

        $templateResourceModel = Mage::getResourceModel('aw_giftcard2/product_attribute_backend_templates');
        $imageFilePath = false;
        if (isset($data['product'])) {
            $imageFilePath = $templateResourceModel->loadTemplateImage(
                $data['product'],
                $data[AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_EMAIL_TEMPLATE]
            );
        }
        if ($imageFilePath) {
            $templateVars['card_image_base_url'] =
                AW_Giftcard2_Helper_Image::getUrlByFilename($imageFilePath) .
                '" data-default-gc2-img="';
        } else {
            $templateVars['card_image_base_url'] = Mage::getModel('core/design_package')
                    ->setStore($store)
                    ->setArea(Mage_Core_Model_Design_Package::DEFAULT_AREA)
                    ->setPackageName(Mage_Core_Model_Design_Package::BASE_PACKAGE)
                    ->setTheme('skin', Mage_Core_Model_Design_Package::DEFAULT_THEME)
                    ->getSkinUrl(self::PATH_TO_EMAIL_IMAGES) . DS
            ;
        }

        if (isset($data[AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_RECIPIENT_NAME])) {
            $templateVars['recipient_name'] = $data[AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_RECIPIENT_NAME];
        }
        if (isset($data[AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_RECIPIENT_EMAIL])) {
            $templateVars['recipient_email'] = $data[AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_RECIPIENT_EMAIL];
        }
        if (isset($data[AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_SENDER_NAME])) {
            $templateVars['sender_name'] = $data[AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_SENDER_NAME];
        }
        if (isset($data[AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_SENDER_EMAIL])) {
            $templateVars['sender_email'] = $data[AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_SENDER_EMAIL];
        }
        if (isset($data[AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_HEADLINE])) {
            $templateVars['headline'] = $data[AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_HEADLINE];
        }
        if (isset($data[AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_MESSAGE])) {
            $templateVars['message'] = $data[AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_MESSAGE];
        }
        if (isset($data['giftcard_codes'])) {
            $templateVars['giftcards'] = $data['giftcard_codes'];
            $templateVars['is_multiple_codes'] = count($data['giftcard_codes']) > 1;
        }
        if (isset($data['balance'])) {
            $balance = $data['balance'];
            $baseCurrencyCode = $store->getWebsite()->getBaseCurrencyCode();
            if ($baseCurrencyCode != $data['currency_code']) {
                $balance = Mage::helper('directory')->currencyConvert(
                    $balance,
                    $baseCurrencyCode,
                    $data['currency_code']
                );
            }
            $templateVars['balance'] = Mage::app()->getLocale()->currency($data['currency_code'])->toCurrency($balance);
        }
        Mage::app()->getLocale()->emulate($store->getId());
        if (isset($data['expire_at']) && !empty($data['expire_at'])) {
            $templateVars['expired_at'] = Mage::helper('core')->formatDate($data['expire_at'], 'medium', false);;
        }
        Mage::app()->getLocale()->revert();

        return $templateVars;
    }

    /**
     * Returns parsed email preview
     *
     * @param $data
     * @param $storeId
     * @return string
     * @throws Exception
     * @throws Mage_Core_Exception
     */
    public function getPreview($data, $storeId)
    {
        $data = $this->_prepare($data, $storeId);
        $templateId = $data[AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_EMAIL_TEMPLATE];

        if (is_numeric($templateId)) {
            $this->emailTemplate->load($templateId);
        } else {
            $localeCode = Mage::getStoreConfig('general/locale/code', $storeId);
            $this->emailTemplate->loadDefault($templateId, $localeCode);
        }

        $content = $this->emailTemplate->getProcessedTemplate($this->prepareTemplateVars($data));
        return $content;
    }

    /**
     * Prepare data for preview
     *
     * @param array $data
     * @param int $storeId
     * @return array
     * @throws Mage_Core_Exception
     */
    protected function _prepare($data, $storeId)
    {
        $this->validate($data);
        $store = Mage::getModel('core/store')->load($storeId);
        if (!$store->getId()) {
            throw new Mage_Core_Exception(Mage::helper('aw_giftcard2')->__('Invalid Store Id'));
        }
        $data['store_id'] = $store->getId();
        $data['currency_code'] = $store->getCurrentCurrencyCode();
        $data['giftcard_codes'] = array('XXXXXXXXXXXX');
        $data['balance'] = (float)($data[AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_AMOUNT] == 'custom' ?
            $data[AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_CUSTOM_AMOUNT] :
            $data[AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_AMOUNT]);
        return $data;
    }

    /**
     * Validate data for preview
     *
     * @param array $data
     * @throws Mage_Core_Exception
     */
    protected function validate($data)
    {
        if (
            empty($data[AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_AMOUNT]) ||
            ($data[AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_AMOUNT] == 'custom'
            && empty($data[AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_CUSTOM_AMOUNT]))
        ) {
            throw new Mage_Core_Exception(Mage::helper('aw_giftcard2')->__('Please specify Gift Card amount.'));
        }
        if (empty($data[AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_EMAIL_TEMPLATE])) {
            throw new Mage_Core_Exception(Mage::helper('aw_giftcard2')->__('Please specify design.'));
        }
    }
}