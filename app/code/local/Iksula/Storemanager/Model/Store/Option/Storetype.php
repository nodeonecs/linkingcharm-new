<?php
class Iksula_Storemanager_Model_Store_Option_Storetype extends Varien_Object
{
    const REPEATER = '_';
    const PREFIX_END = '';
    protected $_options = array();
    /**
     * @param int $parentId
     * @param int $recursionLevel
     *
     * @return array
     */
    public function getOptionArray()
    {

        $storeTypeCollection = Mage::getModel('storetype/storetype')->getCollection();
        $this->_options[] = array(
                'label' => 'Please select Store Type' ,
                'value' => ''
            );
        foreach ($storeTypeCollection as  $StoresType) {
            $label = $StoresType->getName();
            $value = $StoresType->getId();

            /* @var $node Varien_Data_Tree_Node */
            $this->_options[] = array(
                'label' => $label ,
                'value' => $value
            );

        }
        //$this->_options[] = array('label'=> 'test' , 'value' => 'testhshshs');
        return $this->_options;
    }
    /**
     * @param Varien_Data_Tree_Node_Collection $nodeCollection
     */

}
