<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_Model_Source_Giftcard_Used
    implements AW_Giftcard2_Model_Source_SourceInterface
{
    const YES_VALUE         = 1;
    const PARTIALLY_VALUE   = 2;
    const NO_VALUE          = 3;

    const YES_LABEL         = 'Yes';
    const PARTIALLY_LABEL   = 'Partially';
    const NO_LABEL          = 'No';

    /**
     * @return array
     */
    public function toArray()
    {
        $helper = Mage::helper('aw_giftcard2');
        return array(
            self::YES_VALUE         => $helper->__(self::YES_LABEL),
            self::PARTIALLY_VALUE   => $helper->__(self::PARTIALLY_LABEL),
            self::NO_VALUE          => $helper->__(self::NO_LABEL),
        );
    }

    public function getOptionText($value)
    {
        $options = $this->toArray();
        if (array_key_exists($value, $options)) {
            return $options[$value];
        }
        return null;
    }
}