<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   BV
 * @package    BV_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */


class Brainvire_Points_Block_Adminhtml_Coupon_Grid_Renderer_Action extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Action {

    public function render(Varien_Object $object) {

        $actions = array();

        
        $actions[] = array(
            'url' => $this->getUrl('*/*/delete', array('id' => $object->getId())),
            'caption' => Mage::helper('points')->__('Delete'),
            'confirm' => Mage::helper('points')->__('Are you sure?'),
        );


        $this->getColumn()->setActions($actions);
        return parent::render($object);
    }

}
