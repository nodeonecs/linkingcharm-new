<?php

class Magecomp_GstCharge_Model_Sales_Order_Creditmemo_Total_Cgstcharge extends Mage_Sales_Model_Order_Creditmemo_Total_Abstract
{
	public function collect(Mage_Sales_Model_Order_Creditmemo $creditmemo)
    {
    	$creditmemo->setCgstCharge(0);
        $creditmemo->setBaseCgstCharge(0);
        $creditmemoParams = Mage::app()->getRequest()->getParam('creditmemo');
        $amount = $creditmemo->getOrder()->getCgstCharge();
        $totalCgstCharge = 0;
        $creditMemoItemList = array();
        foreach ($creditmemoParams as $items) {
        	foreach ($items as $itemId => $item) {
        		if($item['qty']==1){
        			$creditMemoItemList[] = $itemId;
        		}
        	}
        }

        foreach ($creditmemo->getAllItems() as $item) {
            $orderItem = $item->getOrderItem();

            if ($orderItem->isDummy()) {
                continue;
            }

            if(in_array($orderItem->getId(), $creditMemoItemList)){
            	$totalCgstCharge +=$orderItem->getCgstCharge();
            }
        }
        
        $creditmemo->setCgstCharge($amount);
        
		$quoteId=$creditmemo->getOrder()->getQuoteId();
		//$quote = Mage::getModel('sales/quote')->load($quoteId);
		$quote = Mage::getModel('sales/quote')->getCollection()
    				 ->addFieldToFilter('entity_id', $quoteId)
                     ->getFirstItem();
					 
		$exckudingTax = $quote->getExclPrice();
		$amount = $creditmemo->getOrder()->getBaseCgstCharge();
        $creditmemo->setBaseCgstCharge($amount);
		
		$creditmemo->setCgstCharge($totalCgstCharge);
        $creditmemo->setBaseCgstCharge($totalCgstCharge);

        $subTotal = $creditmemo->getSubtotal();
        $baseSubTotal = $creditmemo->getSubtotal();

        $grandTotal = $creditmemo->getSubtotal();

        $creditmemo->setGrandTotal($grandTotal);
        $creditmemo->setBaseGrandTotal($grandTotal);

		if($exckudingTax)
		{
        	$creditmemo->setGrandTotal($creditmemo->getGrandTotal() + $creditmemo->getCgstCharge());
        	$creditmemo->setBaseGrandTotal($creditmemo->getBaseGrandTotal() + $creditmemo->getBaseCgstCharge());
		}

		$creditmemo->setCgstCharge($totalCgstCharge);
        $creditmemo->setBaseCgstCharge($totalCgstCharge);
		
        return $this;
    }
}