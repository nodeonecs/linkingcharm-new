<?php
$installer = $this;

$installer->startSetup();

$installer->run("
ALTER TABLE {$this->getTable('core/store_group')}
ADD (store_city int(255) NOT NULL, store_state int(255) NOT NULL , store_pincode int(12) NOT NULL, store_zone text(255) NOT NULL , store_longitude text(255) NOT NULL , store_latitude text(255) NOT NULL , store_typeid smallint NOT NULL , store_status smallint NOT NULL , is_hub smallint NOT NULL , store_assigned text(255) NULL , store_address text(255) NOT NULL , store_code varchar(255) NOT NULL , store_country text(255) NOT NULL
 );
");

$installer->endSetup();
