<?php

class Iksula_Crons_Model_Payuapi extends Mage_Core_Model_Abstract
{
    /**
     * Function returns Payu Response by accessing their verify Payment API
     *
     * @param order details
     * @return status of API w.r.t. payment tried by customer
     */

    public function getApiResponse($transactionId){

    	$orderObj      = Mage::getModel('sales/order');
        // $requestUrl    = 'https://info.payu.in/merchant/postservice?form=2';
        $requestUrl    = Mage::getStoreConfig('crons_section/crons_group/payuurl');
        // $command       = 'verify_payment';
        $command       = Mage::getStoreConfig('crons_section/crons_group/payucommand');
        // $key           = 'RgVTU6';
        $key           = Mage::getStoreConfig('crons_section/crons_group/payukey');
        $transactionId = substr($transactionId, 0, -1); //pipe separated transaction IDS
        // $salt          = 'Gz8J5RB2';
        $salt          = Mage::getStoreConfig('crons_section/crons_group/payusalt');
        $hash          = $key . '|' . $command . '|' . $transactionId . '|' . $salt; //hash made up of request parameters
        $hashvalue     = hash('sha512', $hash);
        $data          = array(
            'key' => $key,
            'command' => $command,
            'hash' => $hashvalue,
            'var1' => $transactionId
        );
        $ch            = curl_init();
        curl_setopt($ch, CURLOPT_URL, $requestUrl);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $apiResponse = curl_exec($ch);
        curl_close($ch);
        $statuses = json_decode($apiResponse, 1);
        return $statuses;
    }

}

