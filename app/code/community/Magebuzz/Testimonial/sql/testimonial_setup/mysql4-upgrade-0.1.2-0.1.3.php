<?php
/**
 * @category  Magebuzz
 * @package   Magebuzz_Testimonial
 * @version   0.1.5
 * @copyright Copyright (c) 2012-2015 http://www.magebuzz.com
 * @license   http://www.magebuzz.com/terms-conditions/
 */

$installer = $this;
$installer->startSetup();
	$installer->run("
		ALTER TABLE simple_testimonial ADD title varchar(255) NULL default '' after name;
	");
$installer->endSetup(); 