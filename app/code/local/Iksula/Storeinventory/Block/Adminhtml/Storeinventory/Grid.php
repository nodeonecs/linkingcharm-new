<?php

class Iksula_Storeinventory_Block_Adminhtml_Storeinventory_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("storeinventoryGrid");
				$this->setDefaultSort("entity_id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("storeinventory/storeinventory")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("entity_id", array(
				"header" => Mage::helper("storeinventory")->__("ID"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "number",
				"index" => "entity_id",
				));

				$this->addColumn("store_code", array(
				"header" => Mage::helper("storeinventory")->__("Store Code"),
				"index" => "store_code",
				));
				$this->addColumn("sku", array(
				"header" => Mage::helper("storeinventory")->__("Sku"),
				"index" => "sku",
				));
				$this->addColumn("inventory", array(
				"header" => Mage::helper("storeinventory")->__("Inventory"),
				"index" => "inventory",
				));
				$this->addColumn('website_id', array(
				'header' => Mage::helper('storeinventory')->__('Website'),
				'index' => 'website_id',
				//'renderer'  => 'Iksula_Storeinventory_Block_Adminhtml_Storeinventory_Grid',// THIS IS WHAT THIS POST IS ALL ABOUT
				));

			$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
			$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}

		/*public function render(Varien_Object $row){
			return 'hshsh';
		}
*/


		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('entity_id');
			$this->getMassactionBlock()->setFormFieldName('entity_ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_storeinventory', array(
					 'label'=> Mage::helper('storeinventory')->__('Remove Storeinventory'),
					 'url'  => $this->getUrl('*/adminhtml_storeinventory/massRemove'),
					 'confirm' => Mage::helper('storeinventory')->__('Are you sure?')
				));
			return $this;
		}

		static public function getOptionArray3()
		{
            $data_array=array();
            return($data_array);
		}
		static public function getValueArray3()
		{

			$aWebsiteIds = Mage::getResourceModel('core/website_collection');
			$data_array[] = array('label' => 'Please Select the Website' , 'value' => '');
			foreach($aWebsiteIds as $key => $values){

				$data_array[]=array('label' => $values->getName() , 'value' => $values->getWebsiteId());
			}


            return($data_array);
		}




}
