<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   BV
 * @package    BV_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */


class Brainvire_Core_Block_Adminhtml_System_Config_Form_Fieldset extends Mage_Adminhtml_Block_System_Config_Form_Fieldset {

    
    public function render(Varien_Data_Form_Element_Abstract $element) {
        $html = $this->_getHeaderHtml($element);

        foreach ($element->getElements() as $field) {
            $html.= $field->toHtml();
        }
        $html .= "<tr>
			<td class=\"label\"></td>
			<td class=\"value\">
			<button class=\"scalable\" onclick=\"window.location='" . Mage::getSingleton('adminhtml/url')->getUrl('brainvirecore_admin/viewlog/index') . "'\" type=\"button\">
				<span>View log</span>
			</button
			</td>
		 </tr>
		 ";
        $html .= $this->_getFooterHtml($element);

        return $html;
    }

}