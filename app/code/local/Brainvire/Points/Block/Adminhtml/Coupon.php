<?php
/**
* Brainvire Infotech Pvt. Ltd
*
* @category   BV
* @package    BV_Points
* @version    1.5.1
* @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
* @license    
*/

class Brainvire_Points_Block_Adminhtml_Coupon extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {
        $this->_controller = 'adminhtml_coupon';
        $this->_blockGroup = 'points';
        $this->_headerText = Mage::helper('points')->__('Brainvire Reward Coupons');
        $this->_addButtonLabel = Mage::helper('salesrule')->__('Add Coupon');
        parent::__construct();
    }

}
