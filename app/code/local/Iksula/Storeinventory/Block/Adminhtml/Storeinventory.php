<?php


class Iksula_Storeinventory_Block_Adminhtml_Storeinventory extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_storeinventory";
	$this->_blockGroup = "storeinventory";
	$this->_headerText = Mage::helper("storeinventory")->__("Storeinventory Manager");
	$this->_addButtonLabel = Mage::helper("storeinventory")->__("Add New Item");
	parent::__construct();
			//$this->_removeButton('add');
	}

}
