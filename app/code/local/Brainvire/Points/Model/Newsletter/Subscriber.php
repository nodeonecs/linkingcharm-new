<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   Brainvire
 * @package    Brainvire_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * @license    
 */


class Brainvire_Points_Model_Newsletter_Subscriber extends Mage_Newsletter_Model_Subscriber {
   

    protected $_eventPrefix = 'newsletter_subscriber';

    public function save() {
        parent::save();
        Mage::dispatchEvent($this->_eventPrefix . '_save_after', array('subscriber' => $this));
    }

   
}

?>
