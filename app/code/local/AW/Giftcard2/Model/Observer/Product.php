<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_Model_Observer_Product
{
    public function convertQuoteItemToOrderItem(Varien_Event_Observer $observer)
    {
        $orderItem = $observer->getEvent()->getOrderItem();
        $quoteItem = $observer->getEvent()->getItem();

        if ($quoteItem->getProductType() != AW_Giftcard2_Model_Product_Type_Giftcard::TYPE_CODE) {
            return $this;
        }

        $_product = $quoteItem->getProduct();
        $productOptions = $orderItem->getProductOptions();

        $code = $_product->getTypeInstance()->getCustomOptionsCode();
        $giftcardOptions = $_product->getTypeInstance()->getCustomOptions($_product);
        $giftcardProductOptions = $_product->getTypeInstance()->getGiftCardProductOptions($_product);
        $giftcardOptions = array_merge($giftcardOptions, $giftcardProductOptions);

        $productOptions[$code] = $giftcardOptions;
        $orderItem->setProductOptions($productOptions);

        return $this;
    }

    public function invoicePay(Varien_Event_Observer $observer)
    {
        $invoice = $observer->getInvoice();

        foreach ($invoice->getAllItems() as $item) {
            $orderItem = $item->getOrderItem();
            if ($orderItem->getProductType() == AW_Giftcard2_Model_Product_Type_Giftcard::TYPE_CODE) {
                $options = $orderItem->getProductOptions();
                $giftCardProductOptions = $this->_getGiftCardProductOptions($orderItem);
                $qty = $this->_getQtyToCreate($orderItem);
                $expireAt = null;
                $createdCodes = array();

                while ($qty-- > 0) {
                    try {
                        $giftCard = $this->_createGiftCard(
                            $invoice->getOrder(),
                            $orderItem->getProductId(),
                            $giftCardProductOptions
                        );

                        $createdCodes[] = $giftCard->getCode();

                        if ($giftCard->getOrderId()) {
                            Mage::helper('aw_giftcard2/statistics')->updateStatistics(
                                $giftCard->getProductId(),
                                $giftCard->getOrderId(),
                                $invoice->getStoreId(),
                                array (
                                    'purchased_qty'     => 1,
                                    'purchased_amount'  => $giftCard->getBalance()
                                )
                            );
                        }
                    } catch (Exception $e) {
                        Mage::logException($e);
                    }
                }

                $createdCodesKey = AW_Giftcard2_Model_Product_Type_Giftcard::ORDER_ITEM_CODE_CREATED_CODES;
                $giftCardProductOptions[$createdCodesKey] =
                    isset($giftCardProductOptions[$createdCodesKey]) ?
                        array_merge($giftCardProductOptions[$createdCodesKey], $createdCodes) :
                        $createdCodes
                ;

                // Send email(s) with Gift Card code
                $expiredAt = $giftCard->getExpireAt() ? $giftCard->getExpireAt() : null;
                if (!$giftCard->isPhysical()) {
                    try {
                        $data = array_merge(
                            $giftCardProductOptions,
                            array(
                                /*'store' => $invoice->getStore(),*/
                                'store_id' => $invoice->getStoreId(),
                                'currency_code' => $invoice->getOrderCurrencyCode(),
                                'balance' => $giftCard->getBalance(),
                                'giftcard_codes' => $createdCodes,
                                'expire_at' => $expiredAt,
                                'product' => $giftCard->getProductId()
                            )
                        );
                        Mage::getModel('aw_giftcard2/email_template')->prepareEmailAndSend($data);
                        $giftCardProductOptions[AW_Giftcard2_Model_Product_Type_Giftcard::ORDER_ITEM_CODE_EMAIL_SENT] = true;
                    } catch (Exception $e) {
                        $giftCardProductOptions[AW_Giftcard2_Model_Product_Type_Giftcard::ORDER_ITEM_CODE_EMAIL_SENT] = false;
                        Mage::logException($e);
                    }
                }

                $options[$orderItem->getProduct()->getTypeInstance()->getCustomOptionsCode()] = $giftCardProductOptions;

                $orderItem
                    ->setProductOptions($options)
                    ->save()
                ;
            }
        }
        return $this;
    }

    /**
     * Retrieves qty of codes to create
     *
     * @param Mage_Sales_Model_Order_Invoice_Item $item
     * @return int
     */
    protected function _getQtyToCreate(Mage_Sales_Model_Order_Item $orderItem)
    {
        $qty = $orderItem->getQtyInvoiced();
        $giftcardOptioncCode = $orderItem->getProduct()->getTypeInstance()->getCustomOptionsCode();
        $giftcardOptions = $orderItem->getProductOptionByCode($giftcardOptioncCode);

        if (isset($giftcardOptions[AW_Giftcard2_Model_Product_Type_Giftcard::ORDER_ITEM_CODE_CREATED_CODES])) {
            $createdCodes = $giftcardOptions[AW_Giftcard2_Model_Product_Type_Giftcard::ORDER_ITEM_CODE_CREATED_CODES];
            if ($createdCodes && count($createdCodes) > 0) {
                $qty -= count($createdCodes);
            }
        }

        return $qty;
    }

    protected function _getGiftCardProductOptions(Mage_Sales_Model_Order_Item $orderItem)
    {
        $giftcardOptioncCode = $orderItem->getProduct()->getTypeInstance()->getCustomOptionsCode();
        $giftcardOptions = $orderItem->getProductOptionByCode($giftcardOptioncCode);
        return $giftcardOptions;
    }

    protected function _createGiftCard($order, $productId, $giftCardProductOptions)
    {
        $senderName = '';
        if (isset($giftCardProductOptions[AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_SENDER_NAME])) {
            $senderName = $giftCardProductOptions[AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_SENDER_NAME];
        }
        $senderEmail = '';
        if (isset($giftCardProductOptions[AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_SENDER_EMAIL])) {
            $senderEmail = $giftCardProductOptions[AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_SENDER_EMAIL];
        }
        $recipientName = '';
        if (isset($giftCardProductOptions[AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_RECIPIENT_NAME])) {
            $recipientName = $giftCardProductOptions[AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_RECIPIENT_NAME];
        }
        $recipientEmail = '';
        if (isset($giftCardProductOptions[AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_RECIPIENT_EMAIL])) {
            $recipientEmail = $giftCardProductOptions[AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_RECIPIENT_EMAIL];
        }
        $emailTemplate = AW_Giftcard2_Model_Source_Giftcard_Email_Template::DO_NOT_SEND_VALUE;
        if (isset($giftCardProductOptions[AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_EMAIL_TEMPLATE])) {
            $emailTemplate = $giftCardProductOptions[AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_EMAIL_TEMPLATE];
        }

        $giftCard = Mage::getModel('aw_giftcard2/giftcard');
        $giftCard
            ->setType($giftCardProductOptions[AW_Giftcard2_Model_Product_Type_Giftcard::ATTRIBUTE_CODE_TYPE])
            ->setWebsiteId($order->getStore()->getWebsiteId())
            ->setExpireAfter($giftCardProductOptions[AW_Giftcard2_Model_Product_Type_Giftcard::ATTRIBUTE_CODE_EXPIRE])
            ->setInitialBalance($giftCardProductOptions[AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_AMOUNT])
            ->setSenderName($senderName)
            ->setSenderEmail($senderEmail)
            ->setRecipientName($recipientName)
            ->setRecipientEmail($recipientEmail)
            ->setEmailTemplate($emailTemplate)
            ->setOrder($order)
            ->setOrderId($order->getId())
            ->setProductId($productId)
            ->save()
        ;
        return $giftCard;
    }

    public function salesOrderCreditmemoRefund(Varien_Event_Observer $observer)
    {
        $creditmemo = $observer->getEvent()->getCreditmemo();
        foreach ($creditmemo->getAllItems() as $item) {
            $_qtyToRefund = $item->getQty();
            if (
                $item->getOrderItem()->getProductType() == AW_Giftcard2_Model_Product_Type_Giftcard::TYPE_CODE
                && $_qtyToRefund > 0
            ) {
                $giftCardProductOptions = $this->_getGiftCardProductOptions($item->getOrderItem());
                if (isset($giftCardProductOptions[AW_Giftcard2_Model_Product_Type_Giftcard::ORDER_ITEM_CODE_CREATED_CODES])) {
                    $codes = $giftCardProductOptions[AW_Giftcard2_Model_Product_Type_Giftcard::ORDER_ITEM_CODE_CREATED_CODES];
                    if (is_array($codes)) {
                        $giftcards = $this->_getValidGiftcardToRefund($codes, $_qtyToRefund);
                        foreach ($giftcards as $giftcard) {
                            $statsData = array(
                                'purchased_qty' => -1,
                                'purchased_amount' => -$giftcard->getBalance()
                            );

                            $giftcard->refund($creditmemo);

                            if ($giftcard->getOrderId()) {
                                Mage::helper('aw_giftcard2/statistics')->updateStatistics(
                                    $giftcard->getProductId(),
                                    $giftcard->getOrderId(),
                                    $creditmemo->getStoreId(),
                                    $statsData
                                );
                            }
                        }
                    }
                }
            }
        }
        return $this;
    }

    protected function _getValidGiftcardToRefund(array $codes, $needToRefund)
    {
        $giftcards = array();
        $errors = array();
        foreach ($codes as $code) {
            $giftcardModel = Mage::getModel('aw_giftcard2/giftcard')->loadByCode($code);

            if (null === $giftcardModel->getId()) {
                $errors[] = Mage::helper('aw_giftcard2')->__(
                    'Unable to refund card %s due to it\'s removal', Mage::helper('core')->escapeHtml($code)
                );
                continue;
            }

            if ($giftcardModel->getAvailability() == AW_Giftcard2_Model_Source_Giftcard_Availability::DEACTIVATED_VALUE) {
                continue;
            }

            if (
                $giftcardModel->getAvailability() == AW_Giftcard2_Model_Source_Giftcard_Availability::ACTIVE_VALUE
                && ($giftcardModel->isUsed() || $giftcardModel->isPartiallyUsed())
            ) {
                $errors[] = Mage::helper('aw_giftcard2')->__(
                    'Unable to refund card %s due to it is already used',
                    Mage::helper('core')->escapeHtml($code)
                );
                continue;
            }

            if ($giftcardModel->getAvailability() !=  AW_Giftcard2_Model_Source_Giftcard_Availability::ACTIVE_VALUE) {
                $errors[] = Mage::helper('aw_giftcard2')->__(
                    'Unable to refund card %s due to it is already %s',
                    Mage::helper('core')->escapeHtml($code),
                    $giftcardModel->isUsed()
                        ? Mage::helper('aw_giftcard2')->__("out of balance")
                        : Mage::getModel('aw_giftcard2/source_giftcard_availability')->getOptionText($giftcardModel->getAvailability())
                );
                continue;
            }
            array_push($giftcards, $giftcardModel);

            if (count($giftcards) == $needToRefund) {
                break;
            }
        }
        if (count($giftcards) < $needToRefund) {
            $lastMessage = array_pop($errors);
            foreach ($errors as $error) {
                Mage::getSingleton('adminhtml/session')->addError($error);
            }
            throw new Mage_Core_Exception($lastMessage);
        }
        return $giftcards;
    }

}