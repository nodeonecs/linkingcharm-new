<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */


class Brainvire_Points_Model_Cron {

    public function checkAndCleanExpiredTransactions() {

        $this
                ->_cleanExpiredTransactions()
                ->_sendWarningLetter();
    }
	
	public function checkBirthday() {
		
		$this->_checkBirthday(); 		
 	
	}

   
    protected function _cleanExpiredTransactions() {
        
        Mage::getModel('points/transaction')->getCollection()->addOldLockedFilter()->unlock();

        
        $expiredTransactions = Mage::getModel('points/transaction')->getCollection()
                ->addBalanceActiveFilter()
                ->addNotLockedFilter()
                ->addExpiredFilter();

        $expiredTransactions->getResource()->getReadConnection()->raw_query('LOCK TABLE ' . $expiredTransactions->getMainTable() . " as main_table READ");

        $ids = array();
        foreach ($expiredTransactions as $tr) {
            $ids[] = $tr->getId();
        }

        $expiredTransactions->getResource()->getReadConnection()->raw_query('UNLOCK TABLES');

        
        $expiredTransactions->lock();

        foreach ($ids as $transactionId) {
            $transaction = Mage::getModel('points/transaction')->load($transactionId);
            $result = Mage::getModel('points/api')->addTransaction(
                    $transaction->getBalanceChangeSpent() - $transaction->getBalanceChange(), 'transaction_expired', $transaction->getCustomer(), $transaction, array('transaction_id' => $transaction->getId())
            );
        }


        return $this;
    }

    protected function _sendWarningLetter() {

        if (Mage::helper('points/config')->getIsEnabledNotifications()) {
        	
            
            $sendBeforeDays = Mage::helper('points/config')->getDaysBeforePointExpiredToSendEmail();
            $transactionsToWarn = Mage::getModel('points/transaction')
                    ->getCollection()
                    ->addBalanceActiveFilter()
                    ->addExpiredAfterDaysFilter($sendBeforeDays);


            $summaryData = array();
            $currentDate = new Zend_Date(date('Y-m-d'));

            foreach ($transactionsToWarn as $transaction) {

                $summaryId = $transaction->getSummaryId();
                if (!isset($summaryData[$summaryId])) {
                    $summaryData[$summaryId] = array();
                }

                $expirationDate = $transaction->getData('expiration_date');

                $expirationDate = new Zend_Date($expirationDate);
                $expirationDate = new Zend_Date($expirationDate->toString(Varien_Date::DATE_INTERNAL_FORMAT));

                $daysLeft = $expirationDate->sub($currentDate)->toValue();
                $daysLeft = floor($daysLeft / (60 * 60 * 24));

                if (!isset($summaryData[$summaryId][$daysLeft])) {
                    $summaryData[$summaryId][$daysLeft] = (int) $transaction->getData('balance_change');
                } else {
                    $summaryData[$summaryId][$daysLeft]+=$transaction->getData('balance_change');
                }
            }


            $mail = Mage::getModel('core/email_template');
            foreach ($summaryData as $summaryId => $pointsExpData) {

                $summary = Mage::getModel('points/summary')->load($summaryId);

                if ($summary->getPointsExpirationNotification()) {

                    $customer = $summary->getCustomer();
                    $store = $customer->getStore();
                    $pointUnitName = Mage::helper('points/config')->getPointUnitName($store->getStoreId());


                    Mage::unregister('Brainvire_points_unit_name');
                    Mage::register('Brainvire_points_unit_name', $pointUnitName);

                    Mage::unregister('Brainvire_points_exp_data');
                    Mage::register('Brainvire_points_exp_data', $pointsExpData);

                    $mailParams = array(
                        'store' => $store,
                        'customer' => $customer,
                        'pointsname' => $pointUnitName,
                        'pointstoexpire' => array_sum($pointsExpData),
                        'expirationdays' => $sendBeforeDays,
                    );

                    try {

                        $mail->setDesignConfig(array('area' => 'frontend', 'store' => $store->getStoreId()))
                                ->sendTransactional(
                                        Mage::helper('points/config')->getPointsExpireTemplate($store->getStoreId()), Mage::helper('points/config')->getNotificatioinSender($store->getStoreId()), $customer->getEmail(), null, $mailParams
                        );
                    } catch (Exception $exc) {
                        $logMessage = Mage::helper('points')->__('Unable to send %s expiration email.', $pointUnitName) . '  ' . $exc->getMessage();
                        Mage::helper('Brainvirecore/logger')->log($this, $logMessage, Brainvire_Core_Model_Logger::LOG_SEVERITY_WARNING);
                    }

                    if ($mail->getSentSuccess()) {
                        foreach ($transactionsToWarn as $transaction) {
                            if ($transaction->getSummaryId() == $summaryId) {
                                $transaction->setData('expiration_notification_sent', true)->save();
                            }
                        }
                    }
                }
            }
        }
        return $this;
    }

	protected function _checkBirthday(){
		
	if(Mage::getStoreConfig('points/customer_birthday/customer_birthday')){
		$customer = Mage::getModel("customer/customer")->getCollection();
    	$customer->addFieldToFilter('dob', array('like' => '%'.date("m").'-'.date("d").' 00:00:00'));
    	
    	$_amount = Mage::helper('points/config')->getPointsOnBirthday();
		foreach ($customer as $customer) {
			if($customer){
        		 $result = Mage::getModel('points/api')->addTransaction($_amount,'customer_birthday',$customer,$customer);
        	}
		}    
	}    
        return $this;
	}

	
 	

}
