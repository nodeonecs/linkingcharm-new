<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_Model_Shell_Migration
{
    const LOG_FILE = 'giftcard2_migration.log';

    public function process()
    {
        $this->_processGiftCards();
        $this->_processGiftCardProducts();
    }

    protected function _processGiftCards()
    {
        $gcOneCollection = Mage::getModel('aw_giftcard/giftcard')->getCollection();
        $allGiftCardsCount = $gcOneCollection->getSize();
        $giftCardsCount = 0;
        foreach($gcOneCollection as $giftCard) {

            $balance = $giftCard->getData('balance');
            $initialBalance = $balance;
            $orderId = null;
            $productId = null;

            $giftCardHistoryCreatedAction = $giftCard->getHistoryCollection()
                ->addFieldToFilter('action', 1) //Created action
                ->setOrder('updated_at', 'asc')
                ->getFirstItem()
            ;
            if ($giftCardHistoryCreatedAction) {
                $initialBalance = $giftCardHistoryCreatedAction->getBalanceAmount();
                $additionalData = $giftCardHistoryCreatedAction->getAdditionalInfo();

                // created by order
                if ($additionalData['message_type'] == 1) {
                    $orderIncrementId = $additionalData['message_data'];
                    $order = Mage::getModel('sales/order')->load($orderIncrementId, 'increment_id');
                    if ($order->getId()) {
                        $orderId = $order->getId();
                        $productId = $this->_getProductIdByGiftcardCode($order, $giftCard->getData('code'));
                    }
                }
            }

            $availability = AW_Giftcard2_Model_Source_Giftcard_Availability::ACTIVE_VALUE;
            if ($giftCard->getStatus() == 0) {
                $availability = AW_Giftcard2_Model_Source_Giftcard_Availability::DEACTIVATED_VALUE;
            }

            $isUsed = AW_Giftcard2_Model_Source_Giftcard_Used::YES_VALUE;
            if ($balance > 0) {
                if ($balance < $initialBalance) {
                    $isUsed = AW_Giftcard2_Model_Source_Giftcard_Used::PARTIALLY_VALUE;
                } else {
                    $isUsed = AW_Giftcard2_Model_Source_Giftcard_Used::NO_VALUE;
                }
            }

            try {
                $newGiftCardCode = Mage::getModel('aw_giftcard2/giftcard');
                $newGiftCardCode
                    ->setCode($giftCard->getData('code'))
                    ->setType(AW_Giftcard2_Model_Source_Entity_Attribute_Giftcard_Type::VIRTUAL_VALUE)
                    ->setCreatedAt($giftCard->getData('created_at'))
                    ->setExpireAt($giftCard->getData('expire_at'))
                    ->setWebsiteId($giftCard->getData('website_id'))
                    ->setBalance($balance)
                    ->setInitialBalance($initialBalance)
                    ->setAvailability($availability)
                    ->setIsUsed($isUsed)
                    ->setOrderId($orderId)
                    ->setProductId($productId)
                    ->setEmailTemplate(AW_Giftcard2_Model_Source_Giftcard_Email_Template::DO_NOT_SEND_VALUE)
                    ->setSenderName($giftCard->getData('sender_name'))
                    ->setSenderEmail($giftCard->getData('sender_email'))
                    ->setRecipientName($giftCard->getData('recipient_name'))
                    ->setRecipientEmail($giftCard->getData('recipient_email'))
                    ->setIsImported(true)
                    ->save()
                ;
                $giftCardsCount++;
            } catch (Exception $e) {
                $this->_log(
                    Mage::helper('aw_giftcard2')->__('Gift card %s can not be imported. Error: %s',
                        $giftCard->getData('code'),
                        $e->getMessage()
                    ));
            }
        }
        $this->_log(Mage::helper('aw_giftcard2')->__('(%d) from (%d) giftcard(s) have been imported', $giftCardsCount, $allGiftCardsCount));
    }

    protected function _processGiftCardProducts()
    {
        $giftCardProducts = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToFilter('type_id', 'aw_giftcard')
        ;
        $allGiftCardProductsCount = $giftCardProducts->getSize();
        $giftCardProductsCount = 0;
        foreach ($giftCardProducts as $giftCardProduct) {
            try {
                Mage::getModel('catalog/product_status')->updateProductStatus($giftCardProduct->getId(), 0, Mage_Catalog_Model_Product_Status::STATUS_DISABLED);
                $giftCardProductsCount++;
            } catch (Exception $e) {
                $this->_log(Mage::helper('aw_giftcard2')->__('Product #%s can not be disabled. Error: %s',
                    $giftCardProduct->getId(),
                    $e->getMessage()
                ));
            }
        }
        $this->_log(
            Mage::helper('aw_giftcard2')->__('(%d) from (%d) giftcard product(s) have been disabled',
                $giftCardProductsCount,
                $allGiftCardProductsCount
            )
        );
    }

    /**
     * Gets product id from selected order by gift card code
     *
     * @param $order
     * @param $code
     * @return null
     */
    protected function _getProductIdByGiftcardCode($order, $code)
    {
        foreach ($order->getAllItems() as $orderItem) {
            if ($orderItem->getProductType() == 'aw_giftcard') {
                $options = $orderItem->getProductOptions();
                if (isset($options['aw_gc_created_codes'])) {
                    foreach ($options['aw_gc_created_codes'] as $_code) {
                        if ($_code == $code) {
                            $productId = $orderItem->getProductId();
                            return $productId;
                        }
                    }
                }

            }
        }
        return null;
    }

    protected function _log($message)
    {
        Mage::log($message, 1, self::LOG_FILE, true);
        return $this;
    }
}