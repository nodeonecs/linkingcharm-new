<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   BV
 * @package    BV_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */


class Brainvire_Points_Block_Adminhtml_System_Configuration_Button extends Mage_Adminhtml_Block_System_Config_Form_Field {

    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element) {
        $this->setElement($element);
        $url = $this->getUrl("points_admin/adminhtml_transaction/resetTransactions");

        $html = $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setType('button')
                ->setClass('scalable')
                ->setLabel(Mage::helper('points')->__('Reset all transactions'))
                ->setOnClick("return conformation ();")
                ->toHtml();

        $html .="<p class='note'>";
        $html .="<span style='color:#E02525;'>";
        $html .=Mage::helper('points')->__("This action is unrecoverable and will set all customers' points balance to 0");
        $html .="</span>";
        $html .="</p>";


        $html .= "<script  type='text/javascript'>
                            function conformation (){
                                if(confirm('" . Mage::helper('points')->__('Are you sure to reset all transactions?') . "')){
                                    setLocation('$url');
                                }
                            }
                       </script>";

        return $html;
    }

}
