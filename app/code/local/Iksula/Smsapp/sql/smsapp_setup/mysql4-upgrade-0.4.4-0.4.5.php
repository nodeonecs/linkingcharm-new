<?php
$installer = $this;
$installer->startSetup();
// $installer = new Mage_Sales_Model_Mysql4_Setup;

// $orderSource  = array(
// 	'type'            => 'text',
// 	'backend_type'    => 'text',
// 	'frontend_input'  => 'text',
// 	'is_user_defined' => true,
// 	'label'           => 'Order Device',
// 	'default'         => '',
// 	'visible'         => true,
// 	'required'        => false,
// 	'user_defined'    => false,
// 	'searchable'      => false,
// 	'filterable'      => false,
// 	'comparable'      => false
// );

// $installer->addAttribute('order', 'orderdevice', $orderSource);


$installer = new Mage_Eav_Model_Entity_Setup;

$installer->addAttribute('catalog_category', 'coverimagefirst', array(
    'group'         => 'General',
    'input'         => 'image',
    'type'          => 'varchar',
    'label'         => 'Slider Image',
    'backend'       => 'catalog/category_attribute_backend_image',
    'visible'       => 1,
    'required'        => 0,
    'user_defined' => 1,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
));

$installer->addAttribute('catalog_category', 'coverimagesecond', array(
    'group'         => 'General',
    'input'         => 'image',
    'type'          => 'varchar',
    'label'         => 'Slider Image',
    'backend'       => 'catalog/category_attribute_backend_image',
    'visible'       => 1,
    'required'        => 0,
    'user_defined' => 1,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
));
// $installer->endSetupendSetup();
