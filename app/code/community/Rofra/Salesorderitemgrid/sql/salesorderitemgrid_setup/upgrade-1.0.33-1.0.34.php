<?php
$installer = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();
$installer->run("
    ALTER TABLE sales_flat_shipment ADD COLUMN store_scopeview varchar(255) default NULL;
");
$installer->endSetup();

?>