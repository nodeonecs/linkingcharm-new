<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_Model_Source_Giftcard_Email_Template
    implements AW_Giftcard2_Model_Source_SourceInterface
{
    const DEFAULT_EMAIL_TEMPLATE_PATH = 'aw_giftcard2_email_template';

    const DO_NOT_SEND_VALUE = '0';
    const DO_NOT_SEND_LABEL = 'Do not send';

    /**
     * @return array
     */
    public function toArray()
    {
        $result = array();
        $template = Mage::getModel('adminhtml/system_config_source_email_template');
        $template->setPath(self::DEFAULT_EMAIL_TEMPLATE_PATH);

        $optionArray = $template->toOptionArray();
        array_unshift($optionArray, array(
            'value' => self::DO_NOT_SEND_VALUE,
            'label' => Mage::helper('aw_giftcard2')->__(self::DO_NOT_SEND_LABEL)
        ));
        foreach ($optionArray as $option) {
            $result[$option['value']] = $option['label'];
        }
        return $result;
    }

    public function getOptionText($value)
    {
        $options = $this->toArray();
        if (array_key_exists($value, $options)) {
            return $options[$value];
        }
        return null;
    }
}