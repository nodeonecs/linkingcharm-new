<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


$installer = $this;
$installer->startSetup();

$giftCardInfoGroupName = 'Gift Card Information';

$entityTypeId = $installer->getEntityTypeId('catalog_product');
$attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);
$installer->addAttributeGroup($entityTypeId, $attributeSetId, $giftCardInfoGroupName, 2);

/*
$installer->removeAttribute('catalog_product', AW_Giftcard2_Model_Product_Type_Giftcard::ATTRIBUTE_CODE_TYPE);
$installer->removeAttribute('catalog_product', AW_Giftcard2_Model_Product_Type_Giftcard::ATTRIBUTE_CODE_DESCRIPTION);
$installer->removeAttribute('catalog_product', AW_Giftcard2_Model_Product_Type_Giftcard::ATTRIBUTE_CODE_EXPIRE);
$installer->removeAttribute('catalog_product', AW_Giftcard2_Model_Product_Type_Giftcard::ATTRIBUTE_CODE_ALLOW_MESSAGE);
$installer->removeAttribute('catalog_product', AW_Giftcard2_Model_Product_Type_Giftcard::ATTRIBUTE_CODE_EMAIL_TEMPLATES);
$installer->removeAttribute('catalog_product', AW_Giftcard2_Model_Product_Type_Giftcard::ATTRIBUTE_CODE_AMOUNTS);
$installer->removeAttribute('catalog_product', AW_Giftcard2_Model_Product_Type_Giftcard::ATTRIBUTE_CODE_ALLOW_OPEN_AMOUNT);
$installer->removeAttribute('catalog_product', AW_Giftcard2_Model_Product_Type_Giftcard::ATTRIBUTE_CODE_OPEN_AMOUNT_MIN);
$installer->removeAttribute('catalog_product', AW_Giftcard2_Model_Product_Type_Giftcard::ATTRIBUTE_CODE_OPEN_AMOUNT_MAX);
*/

$installer->addAttribute('catalog_product', AW_Giftcard2_Model_Product_Type_Giftcard::ATTRIBUTE_CODE_TYPE,
    array(
        'group'             => $giftCardInfoGroupName,
        'type'              => 'int',
        'backend'           => '',
        'frontend'          => '',
        'label'             => 'Card Type',
        'input'             => 'select',
        'input_renderer'    => 'aw_giftcard2/adminhtml_product_form_giftcardtype',
        'class'             => '',
        'source'            => 'aw_giftcard2/source_entity_attribute_giftcard_type',
        'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'visible'           => true,
        'required'          => true,
        'user_defined'      => false,
        'default'           => '',
        'searchable'        => false,
        'filterable'        => false,
        'comparable'        => false,
        'visible_on_front'  => false,
        'unique'            => false,
        'apply_to'          => AW_Giftcard2_Model_Product_Type_Giftcard::TYPE_CODE,
        'is_configurable'   => false,
        'sort_order'        => 1,
    )
);

$installer->addAttribute('catalog_product', AW_Giftcard2_Model_Product_Type_Giftcard::ATTRIBUTE_CODE_DESCRIPTION,
    array(
        'group'             => $giftCardInfoGroupName,
        'type'              => 'text',
        'backend'           => '',
        'frontend'          => '',
        'label'             => 'Card Description',
        'input'             => 'textarea',
        'wysiwyg_enabled'   => true,
        'class'             => '',
        'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'visible'           => true,
        'required'          => false,
        'user_defined'      => false,
        'default'           => '',
        'searchable'        => false,
        'filterable'        => false,
        'comparable'        => false,
        'visible_on_front'  => false,
        'unique'            => false,
        'apply_to'          => AW_Giftcard2_Model_Product_Type_Giftcard::TYPE_CODE,
        'is_configurable'   => false,
        'sort_order'        => 2,
    )
);

$installer->addAttribute('catalog_product', AW_Giftcard2_Model_Product_Type_Giftcard::ATTRIBUTE_CODE_EXPIRE,
    array(
        'group'             => $giftCardInfoGroupName,
        'type'              => 'int',
        'backend'           => '',
        'frontend'          => '',
        'label'             => 'Expires After (days)',
        'input'             => 'text',
        'class'             => '',
        'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'visible'           => true,
        'required'          => false,
        'user_defined'      => false,
        'default'           => '',
        'searchable'        => false,
        'filterable'        => false,
        'comparable'        => false,
        'visible_on_front'  => false,
        'unique'            => false,
        'apply_to'          => AW_Giftcard2_Model_Product_Type_Giftcard::TYPE_CODE,
        'is_configurable'   => false,
        'sort_order'        => 3,
    )
);

$installer->addAttribute('catalog_product', AW_Giftcard2_Model_Product_Type_Giftcard::ATTRIBUTE_CODE_ALLOW_MESSAGE,
    array(
        'group'             => $giftCardInfoGroupName,
        'type'              => 'int',
        'backend'           => '',
        'frontend'          => '',
        'label'             => 'Allow Message',
        'input'             => 'select',
        'class'             => '',
        'source'            => 'aw_giftcard2/source_entity_attribute_option_yesno',
        'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'visible'           => true,
        'required'          => false,
        'user_defined'      => false,
        'default'           => '1',
        'searchable'        => false,
        'filterable'        => false,
        'comparable'        => false,
        'visible_on_front'  => false,
        'unique'            => false,
        'apply_to'          => AW_Giftcard2_Model_Product_Type_Giftcard::TYPE_CODE,
        'is_configurable'   => false,
        'sort_order'        => 4,
    )
);

$installer->addAttribute('catalog_product', AW_Giftcard2_Model_Product_Type_Giftcard::ATTRIBUTE_CODE_EMAIL_TEMPLATES,
    array(
        'group'             => $giftCardInfoGroupName,
        'type'              => 'static',
        'backend'           => 'aw_giftcard2/product_attribute_backend_templates',
        'frontend'          => '',
        'label'             => 'Email Templates',
        /*'input'             => 'text',*/
        'input_renderer'    => 'aw_giftcard2/adminhtml_product_form_templates',
        'class'             => '',
        'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'visible'           => true,
        'required'          => false,
        'user_defined'      => false,
        'default'           => '',
        'searchable'        => false,
        'filterable'        => false,
        'comparable'        => false,
        'visible_on_front'  => false,
        'unique'            => false,
        'apply_to'          => AW_Giftcard2_Model_Product_Type_Giftcard::TYPE_CODE,
        'is_configurable'   => false,
        'sort_order'        => 5,
    )
);

$installer->addAttribute('catalog_product', AW_Giftcard2_Model_Product_Type_Giftcard::ATTRIBUTE_CODE_AMOUNTS,
    array(
        'group'             => $giftCardInfoGroupName,
        'type'              => 'decimal',
        'backend'           => 'aw_giftcard2/product_attribute_backend_amounts',
        'frontend'          => '',
        'label'             => 'Amounts',
        /*'input'             => 'price',*/
        'input_renderer'    => 'aw_giftcard2/adminhtml_product_form_amounts',
        'class'             => '',
        'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'visible'           => true,
        'required'          => false,
        'user_defined'      => false,
        'default'           => '',
        'searchable'        => false,
        'filterable'        => false,
        'comparable'        => false,
        'visible_on_front'  => false,
        'unique'            => false,
        'apply_to'          => AW_Giftcard2_Model_Product_Type_Giftcard::TYPE_CODE,
        'is_configurable'   => false,
        'used_in_product_listing' => true,
        'sort_order'        => 6,
    )
);

$installer->addAttribute('catalog_product', AW_Giftcard2_Model_Product_Type_Giftcard::ATTRIBUTE_CODE_ALLOW_OPEN_AMOUNT,
    array(
        'group'             => $giftCardInfoGroupName,
        'type'              => 'int',
        'backend'           => '',
        'frontend'          => '',
        'label'             => 'Allow Open Amount',
        'input'             => 'select',
        'input_renderer'    => 'aw_giftcard2/adminhtml_product_form_allowopenamount',
        'class'             => '',
        'source'            => 'aw_giftcard2/source_entity_attribute_option_yesno',
        'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_WEBSITE,
        'visible'           => true,
        'required'          => false,
        'user_defined'      => false,
        'default'           => '',
        'searchable'        => false,
        'filterable'        => false,
        'comparable'        => false,
        'visible_on_front'  => false,
        'unique'            => false,
        'apply_to'          => AW_Giftcard2_Model_Product_Type_Giftcard::TYPE_CODE,
        'is_configurable'   => false,
        'used_in_product_listing' => true,
        'sort_order'        => 7,
    )
);

$installer->addAttribute('catalog_product', AW_Giftcard2_Model_Product_Type_Giftcard::ATTRIBUTE_CODE_OPEN_AMOUNT_MIN,
    array(
        'group'             => $giftCardInfoGroupName,
        'type'              => 'decimal',
        'backend'           => 'catalog/product_attribute_backend_price',
        'frontend'          => '',
        'label'             => 'Open Amount Min Value',
        'input'             => 'price',
        'class'             => 'validate-number validate-greater-than-zero',
        'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'visible'           => true,
        'required'          => true,
        'user_defined'      => false,
        'default'           => '',
        'searchable'        => false,
        'filterable'        => false,
        'comparable'        => false,
        'visible_on_front'  => false,
        'unique'            => false,
        'apply_to'          => AW_Giftcard2_Model_Product_Type_Giftcard::TYPE_CODE,
        'is_configurable'   => false,
        'used_in_product_listing' => true,
        'sort_order'        => 8,
    )
);

$installer->addAttribute('catalog_product', AW_Giftcard2_Model_Product_Type_Giftcard::ATTRIBUTE_CODE_OPEN_AMOUNT_MAX,
    array(
        'group'             => $giftCardInfoGroupName,
        'type'              => 'decimal',
        'backend'           => 'catalog/product_attribute_backend_price',
        'frontend'          => '',
        'label'             => 'Open Amount Max Value',
        'input'             => 'price',
        'class'             => 'validate-number validate-greater-than-zero',
        'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'visible'           => true,
        'required'          => true,
        'user_defined'      => false,
        'default'           => '',
        'searchable'        => false,
        'filterable'        => false,
        'comparable'        => false,
        'visible_on_front'  => false,
        'unique'            => false,
        'apply_to'          => AW_Giftcard2_Model_Product_Type_Giftcard::TYPE_CODE,
        'is_configurable'   => false,
        'used_in_product_listing' => true,
        'sort_order'        => 9,
    )
);

$fieldList = array('weight','tax_class_id');
foreach ($fieldList as $field) {
    $applyTo = explode(',', $installer->getAttribute('catalog_product', $field, 'apply_to'));
    if (!in_array(AW_Giftcard2_Model_Product_Type_Giftcard::TYPE_CODE, $applyTo)) {
        $applyTo[] = AW_Giftcard2_Model_Product_Type_Giftcard::TYPE_CODE;
        $installer->updateAttribute('catalog_product', $field, 'apply_to', join(',', $applyTo));
    }
}

$installer->run("
    CREATE TABLE IF NOT EXISTS {$this->getTable('aw_giftcard2/giftcard')} (
      `giftcard_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
      `code` VARCHAR(255) NOT NULL,
      `type` SMALLINT(5) NOT NULL,
      `created_at` DATE NOT NULL,
      `expire_at` DATE NULL DEFAULT NULL,
      `website_id` SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0',
      `balance` DECIMAL(12,2) NOT NULL DEFAULT '0.00',
      `initial_balance` DECIMAL(12,2) NOT NULL DEFAULT '0.00',
      `availability` SMALLINT(5) NOT NULL DEFAULT '0',
      `is_used` SMALLINT(5) NOT NULL DEFAULT '3',
      `order_id` INT(10) NULL DEFAULT NULL,
      `product_id` INT(10) NOT NULL DEFAULT '0',
      `email_template` VARCHAR(255) NULL DEFAULT '0',
      `sender_name` VARCHAR(255) NOT NULL,
      `sender_email` VARCHAR(255) NULL DEFAULT NULL,
      `recipient_name` VARCHAR(255) NOT NULL,
      `recipient_email` VARCHAR(255) NULL DEFAULT NULL,
      PRIMARY KEY (`giftcard_id`),
      INDEX `idx_aw_giftcard2_website_id_idx` (`website_id` ASC),
      CONSTRAINT `fk_aw_giftcard2_website`
        FOREIGN KEY (`website_id`)
        REFERENCES {$this->getTable('core/website')} (`website_id`)
        ON DELETE CASCADE
        ON UPDATE CASCADE
    ) ENGINE = InnoDB DEFAULT CHARACTER SET = utf8 COMMENT = 'Giftcard';

    CREATE TABLE IF NOT EXISTS {$this->getTable('aw_giftcard2/history')} (
      `history_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
      `giftcard_id` INT(10) UNSIGNED NOT NULL,
      `updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
      `action` SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0',
      `balance_amount` DECIMAL(12,2) NOT NULL DEFAULT '0.00',
      `balance_delta` DECIMAL(12,4) NOT NULL DEFAULT '0.00',
      `additional_info` VARCHAR(255) NULL DEFAULT NULL,
      PRIMARY KEY (`history_id`),
      INDEX `fk_aw_giftcard2_history_aw_giftcard2_idx` (`giftcard_id` ASC),
      CONSTRAINT `fk_aw_giftcard2_history_aw_giftcard2`
        FOREIGN KEY (`giftcard_id`)
        REFERENCES {$this->getTable('aw_giftcard2/giftcard')} (`giftcard_id`)
        ON DELETE CASCADE
        ON UPDATE CASCADE
    ) ENGINE = InnoDB DEFAULT CHARACTER SET=utf8 COMMENT = 'Giftcard History';

    CREATE TABLE IF NOT EXISTS {$this->getTable('aw_giftcard2/quote')} (
      `link_id` INT(10) UNSIGNED NOT NULL  AUTO_INCREMENT,
      `giftcard_id` INT(10) UNSIGNED NOT NULL,
      `quote_id` INT(10) UNSIGNED NOT NULL,
      `base_giftcard_amount` DECIMAL(12,2) UNSIGNED NULL,
      `giftcard_amount` DECIMAL(12,2) UNSIGNED NULL,
      PRIMARY KEY (`link_id`),
      INDEX `fk_aw_giftcard2_quote_aw_giftcard2_idx` (`giftcard_id` ASC),
      CONSTRAINT `fk_aw_giftcard2_quote_aw_giftcard2`
        FOREIGN KEY (`giftcard_id`)
        REFERENCES {$this->getTable('aw_giftcard2/giftcard')} (`giftcard_id`)
        ON DELETE CASCADE
        ON UPDATE CASCADE
    ) ENGINE = InnoDB DEFAULT CHARACTER SET=utf8 COMMENT = 'Giftcard To Quote Linkage Table';

    CREATE TABLE IF NOT EXISTS {$this->getTable('aw_giftcard2/invoice')} (
      `link_id` INT(10) UNSIGNED NOT NULL  AUTO_INCREMENT,
      `giftcard_id` INT(10) UNSIGNED NOT NULL,
      `invoice_id` INT(10) UNSIGNED NOT NULL,
      `order_id` INT(10) UNSIGNED NULL DEFAULT NULL,
      `base_giftcard_amount` DECIMAL(12,2) UNSIGNED NULL DEFAULT NULL,
      `giftcard_amount` DECIMAL(12,2) UNSIGNED NULL DEFAULT NULL,
      PRIMARY KEY (`link_id`),
      INDEX `fk_aw_giftcard2_invoice_aw_giftcard2_idx` (`giftcard_id` ASC),
      CONSTRAINT `fk_aw_giftcard2_invoice_aw_giftcard2`
        FOREIGN KEY (`giftcard_id`)
        REFERENCES {$this->getTable('aw_giftcard2/giftcard')} (`giftcard_id`)
        ON DELETE CASCADE
        ON UPDATE CASCADE)
    ENGINE = InnoDB DEFAULT CHARACTER SET=utf8 COMMENT = 'Giftcard To Invoice Linkage Table';

    CREATE TABLE IF NOT EXISTS {$this->getTable('aw_giftcard2/creditmemo')} (
      `link_id` INT(10) UNSIGNED NOT NULL  AUTO_INCREMENT,
      `giftcard_id` INT(10) UNSIGNED NOT NULL,
      `creditmemo_id` INT(10) UNSIGNED NOT NULL,
      `order_id` INT(10) UNSIGNED NULL DEFAULT NULL,
      `base_giftcard_amount` DECIMAL(12,2) UNSIGNED NULL DEFAULT NULL,
      `giftcard_amount` DECIMAL(12,2) UNSIGNED NULL DEFAULT NULL,
      PRIMARY KEY (`link_id`),
      INDEX `fk_aw_giftcard2_creditmemo_aw_giftcard2_idx` (`giftcard_id` ASC),
      CONSTRAINT `fk_aw_giftcard2_creditmemo_aw_giftcard2`
        FOREIGN KEY (`giftcard_id`)
        REFERENCES {$this->getTable('aw_giftcard2/giftcard')} (`giftcard_id`)
        ON DELETE CASCADE
        ON UPDATE CASCADE)
    ENGINE = InnoDB DEFAULT CHARACTER SET=utf8 COMMENT = 'Giftcard To Creditmemo Linkage Table';

    CREATE TABLE IF NOT EXISTS {$this->getTable('aw_giftcard2/product_entity_amounts')} (
      `value_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
      `entity_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
      `value` DECIMAL(12,2) NULL DEFAULT NULL,
      `website_id` SMALLINT(5) UNSIGNED NOT NULL,
      PRIMARY KEY  (`value_id`),
      INDEX `idx_aw_giftcard2_product_entity_amounts_website_id_idx` (`website_id` ASC),
      CONSTRAINT `fk_aw_giftcard2_product_entity_amounts_product`
        FOREIGN KEY (`entity_id`)
        REFERENCES {$this->getTable('catalog/product')} (`entity_id`)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
      CONSTRAINT `fk_aw_giftcard2_product_entity_amounts_website`
        FOREIGN KEY (`website_id`)
        REFERENCES {$this->getTable('core/website')} (`website_id`)
        ON DELETE CASCADE
        ON UPDATE CASCADE
    ) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COMMENT = 'Giftcard Product Amounts Attribute Backend Table';

    CREATE TABLE IF NOT EXISTS {$this->getTable('aw_giftcard2/product_entity_templates')} (
      `value_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
      `entity_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
      `value` VARCHAR(255) NULL DEFAULT NULL,
      `image` VARCHAR(255) NULL DEFAULT NULL,
      `store_id` SMALLINT(5) UNSIGNED NOT NULL,
      PRIMARY KEY  (`value_id`),
      INDEX `idx_aw_giftcard2_product_entity_templates_store_id_idx` (`store_id` ASC),
      CONSTRAINT `fk_aw_giftcard2_product_entity_templates_product`
        FOREIGN KEY (`entity_id`)
        REFERENCES {$this->getTable('catalog/product')} (`entity_id`)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
      CONSTRAINT `fk_aw_giftcard2_product_entity_templates_store`
        FOREIGN KEY (`store_id`)
        REFERENCES {$this->getTable('core/store')} (`store_id`)
        ON DELETE CASCADE
        ON UPDATE CASCADE
    ) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8 COMMENT = 'Giftcard Product Email Templates Attribute Backend Table';

    CREATE TABLE IF NOT EXISTS {$this->getTable('aw_giftcard2/statistics')} (
      `stat_id` INT(10) UNSIGNED NOT NULL  AUTO_INCREMENT,
      `product_id` INT(10) UNSIGNED NOT NULL,
       `order_id` INT(10) UNSIGNED NULL DEFAULT NULL,
      `purchased_qty` INT(10) UNSIGNED NULL DEFAULT '0',
      `purchased_amount` DECIMAL(12,2) NULL DEFAULT '0.00',
      `used_qty` INT(10) UNSIGNED NULL DEFAULT '0',
      `used_amount` DECIMAL(12,2) NULL DEFAULT '0.00',
      `store_id` SMALLINT(5) UNSIGNED NOT NULL DEFAULT '0',
      PRIMARY KEY (`stat_id`),
      INDEX `idx_aw_giftcard2_statistics_store_id_idx` (`store_id` ASC),
      CONSTRAINT `fk_aw_giftcard2_statistics_product`
        FOREIGN KEY (`product_id`)
        REFERENCES {$this->getTable('catalog/product')} (`entity_id`)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
      CONSTRAINT `fk_aw_giftcard2_statistics_store`
        FOREIGN KEY (`store_id`)
        REFERENCES {$this->getTable('core/store')} (`store_id`)
        ON DELETE CASCADE
        ON UPDATE CASCADE
    )
    ENGINE = InnoDB DEFAULT CHARACTER SET=utf8 COMMENT = 'Giftcard Statictics Table';

");

function installSampleEmailTemplates()
{
    libxml_use_internal_errors(true);
    try {
        $filename = Mage::getModel('core/config')->getOptions()->getCodeDir() . DS . 'local' . DS . 'AW'
            . DS . 'Giftcard2' . DS . 'sql' . DS . 'aw_giftcard2_setup' . DS . 'sample_email_templates.xml'
        ;
        $xml = simplexml_load_file($filename, 'SimpleXMLElement', LIBXML_NOCDATA);

        if (!$xml) {
            foreach (libxml_get_errors() as $error) {
                $message = 'Failed to load XML : ' . $error->message;
                Mage::log($message);
            }
            return;
        }

        libxml_clear_errors();

        foreach ($xml as $templateData) {
            $template = Mage::getModel('core/email_template');
            if (!isset($templateData->template_code) || $template->loadByCode((string)$templateData->template_code)->getId()) {
                continue;
            }
            $template
                ->setTemplateSubject((string)$templateData->template_subject)
                ->setTemplateCode((string)$templateData->template_code)
                ->setTemplateText((string)$templateData->template_text)
                ->setTemplateStyles((string)$templateData->template_styles)
                ->setModifiedAt(Mage::getSingleton('core/date')->gmtDate())
                ->setOrigTemplateCode(AW_Giftcard2_Model_Source_Giftcard_Email_Template::DEFAULT_EMAIL_TEMPLATE_PATH)
                ->setAddedAt(Mage::getSingleton('core/date')->gmtDate())
                ->setTemplateType(Mage_Core_Model_Email_Template::TYPE_HTML)
            ;
            $template->save();
        }
    } catch (Exception $e) {
        Mage::logException($e);
    }
}

installSampleEmailTemplates();

$installer->endSetup();
