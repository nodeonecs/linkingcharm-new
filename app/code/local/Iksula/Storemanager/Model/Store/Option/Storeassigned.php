<?php
class Iksula_Storemanager_Model_Store_Option_Storeassigned extends Varien_Object
{
    const REPEATER = '_';
    const PREFIX_END = '';
    protected $_options = array();
    /**
     * @param int $parentId
     * @param int $recursionLevel
     *
     * @return array
     */
    public function getOptionArray()
    {

        $store_id = Mage::app()->getRequest()->getParam('id');
        //$website_id = Mage::app()->getRequest()->getParam('websiteid');
        $website_id = '1';
        //exit;


        $aStoreCollection = Mage::getModel('core/store_group')->getCollection()->addFieldToFilter('group_id' , array('neq' => $store_id))->addFieldToSelect(array('store_assigned'));

       /* echo '<pre>';
        print_r($aStoreCollection->getData());
        exit;*/

        $aTotalStoreAssigned = array();

        foreach($aStoreCollection as $store){

            $aStoreAssigned = explode(',' , $store->getStoreAssigned());

            foreach($aStoreAssigned as $iStore){
                $aTotalStoreAssigned[] = $iStore;
            }
        }


        $aStoreTotalCollection = Mage::getModel('core/store_group')->getCollection()->addFieldToFilter('store_definition' , array('eq' => 'store'))->addFieldToFilter('website_id' , array('eq' => $website_id))->addFieldToFilter('default_store_id' , array('neq' => '1'))->addFieldToSelect(array('group_id' , 'name'));
        $aStoreTotalData = array();
        foreach($aStoreTotalCollection as $aTotalStore){

            $aStoreTotalData [$aTotalStore->getName()] = $aTotalStore->getGroupId()  ;

        }


        $aTotalStoreNotAssigned = array_diff($aStoreTotalData , $aTotalStoreAssigned);
        //$aTotalStoreNotAssigned = $aStoreTotalData;

        /*Mage::log(print_r($aTotalStoreAssigned , 1 ) , null , 'store_collect.log');

        Mage::log(print_r($aStoreTotalData , 1 ) , null , 'store_collect.log');

        Mage::log(print_r($aTotalStoreNotAssigned , 1 ) , null , 'store_collect.log');*/





        //$storeData = array('1' => 'store 1' , '2' => 'store 2' , '3' => 'store 3');
        foreach ($aTotalStoreNotAssigned as $key => $values) {

            if($values == $store_id) continue;
            /* @var $node Varien_Data_Tree_Node */
            $this->_options[] = array(
                'label' => $key ,
                'value' => $values,
                'is-red' => $values
            );
            /*if ($node->hasChildren()) {
                $this->_getChildOptions($node->getChildren());
            }*/
        }
        return $this->_options;
    }
    /**
     * @param Varien_Data_Tree_Node_Collection $nodeCollection
     */

}
