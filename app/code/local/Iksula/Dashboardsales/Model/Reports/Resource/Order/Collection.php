<?php
class Iksula_Dashboardsales_Model_Reports_Resource_Order_Collection extends Mage_Reports_Model_Resource_Order_Collection
{

	public function calculateRefund($isFilter = 0)
	{
		$statuses = Mage::getSingleton('sales/config')
            ->getOrderStatusesForState(Mage_Sales_Model_Order::STATE_CANCELED);

        if (empty($statuses)) {
            $statuses = array(0);
        }

        $collection = Mage::getModel('amrma/request')->getCollection();

		$collection->join(array('items' => 'amrma/item'), 
								'main_table.request_id = items.request_id', 
								array('items.qty'));

		$collection->join(array('sales_items' => 'sales/order_item'), 
								'sales_items.item_id = items.sales_item_id', 
								array('sales_items.price'));
        
        $collectionData =  $collection->getData();
        $returnPriceSum  = 0;
        $returnCount = 0;

        if($collectionData){
	        $returnPriceSum = array_sum(array_column($collectionData, 'price'));
	        $returnCount = count($collectionData);
	    }
	    
        $data['total_return_price'] = $returnPriceSum;
        $data['return_count'] = $returnCount;

        return $data;
	}


	public function calculateSales($isFilter = 0)
    {
        $statuses = Mage::getSingleton('sales/config')
            ->getOrderStatusesForState(Mage_Sales_Model_Order::STATE_CANCELED);

        if (empty($statuses)) {
            $statuses = array(0);
        }

        $adapter = $this->getConnection();

        if (Mage::getStoreConfig('sales/dashboard/use_aggregated_data')) {
            $this->setMainTable('sales/order_aggregated_created');
            $this->removeAllFieldsFromSelect();
            $averageExpr = $adapter->getCheckSql(
                'SUM(main_table.orders_count) > 0',
                'SUM(main_table.total_revenue_amount)/SUM(main_table.orders_count)',
                0);
            $this->getSelect()->columns(array(
                'lifetime' => 'SUM(main_table.total_revenue_amount)',
                'average'  => $averageExpr
            ));

            if (!$isFilter) {
                $this->addFieldToFilter('store_id',
                    array('eq' => Mage::app()->getStore(Mage_Core_Model_Store::ADMIN_CODE)->getId())
                );
            }
            $this->getSelect()->where('main_table.order_status NOT IN(?)', $statuses);
        } else {
            $this->setMainTable('sales/order');
            $this->removeAllFieldsFromSelect();

            $expr = $this->_getSalesAmountExpression();

            if ($isFilter == 0) {
                $expr = '(' . $expr . ') * main_table.base_to_global_rate';
            }

            $this->getSelect()
                ->columns(array(
                    'lifetime' => "SUM({$expr})",
                    'average'  => "AVG({$expr})",
                    'total_order_count'  => "COUNT({$expr})"
                    
                ))
                ->where('main_table.status NOT IN(?)', $statuses)
                ->where('main_table.state NOT IN(?)', array(
                    Mage_Sales_Model_Order::STATE_NEW,
                    Mage_Sales_Model_Order::STATE_PENDING_PAYMENT)
                );
        }
        return $this;
    }
}
		