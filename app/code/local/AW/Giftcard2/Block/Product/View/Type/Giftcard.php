<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_Block_Product_View_Type_Giftcard extends Mage_Catalog_Block_Product_View_Abstract
{
    protected $_preConfiguredValues = null;

    /**
     * @return bool
     */
    public function canRenderDescription()
    {
        $description = $this->getDescription();
        return !empty($description);
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->getProduct()->getTypeInstance()->getGiftCardDescription($this->getProduct());
    }

    /**
     * @return bool
     */
    public function canRenderOptions()
    {
        return (
            $this->getProduct()->isSaleable() &&
            ($this->isAllowOpenAmount() || count($this->getAmountOptions()) > 0)
        );
    }

    /**
     * @return bool
     */
    public function isAllowOpenAmount()
    {
        return (bool)$this->getProduct()->getTypeInstance()->isAllowOpenAmount($this->getProduct());
    }

    /**
     * @return mixed
     */
    public function getOpenAmountMin()
    {
        return $this->getProduct()->getTypeInstance()->getOpenAmountMin($this->getProduct());
    }

    /**
     * @return mixed
     */
    public function getOpenAmountMax()
    {
        return $this->getProduct()->getTypeInstance()->getOpenAmountMax($this->getProduct());
    }

    /**
     * @return array
     */
    public function getAmountOptions()
    {
        return $this->getProduct()->getTypeInstance()->getAmountOptions(
            $this->getProduct()
        );
    }

    public function isCustomAmountOnly()
    {
        return (count($this->getAmountOptions()) == 0 && $this->isAllowOpenAmount());
    }

    /**
     * @return mixed
     */
    public function getFixedAmount()
    {
        $amountOptions = $this->getAmountOptions();
        return array_shift($amountOptions);
    }

    /**
     * @return bool
     */
    public function isFixedAmount()
    {
        return (count($this->getAmountOptions()) == 1) && !$this->isAllowOpenAmount();
    }

    /**
     * @return string
     */
    public function getDisplayCurrencySymbol()
    {
        return Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol();
    }

    /**
     * @return bool
     */
    public function isAllowDesignSelect()
    {
        return !$this->getProduct()->getTypeInstance()->isPhysicalGCType($this->getProduct()) && !$this->isSingleDesign();
    }

    /**
     * @return bool
     */
    public function isSingleDesign()
    {
        return count($this->getTemplateOptions($this->getProduct())) == 1;
    }

    /**
     * @return array
     */
    public function getTemplateOptions()
    {
        $templateOptions = $this->getProduct()->getTypeInstance()->getTemplateOptions(
            $this->getProduct()
        );
        foreach ($templateOptions as $key => $option) {
            $templateOptions[$key]['template_name'] =
                Mage::getModel('aw_giftcard2/source_entity_attribute_giftcard_email_template')->getOptionText($option['template']);

            if ($option['image']) {
                $templateOptions[$key]['image_url'] =
                    AW_Giftcard2_Helper_Image::resizeImage(
                        $option['image'],
                        AW_Giftcard2_Helper_Image::FRONTEND_IMAGE_SIZE,
                        AW_Giftcard2_Helper_Image::FRONTEND_IMAGE_SIZE
                    );
            }
        };
        return $templateOptions;
    }

    /**
     * @return mixed
     */
    public function getTemplateValue()
    {
        $options = $this->getTemplateOptions();
        return $options[0]['template'];
    }

    /**
     * @return bool
     */
    public function allowedEmail()
    {
        return !$this->getProduct()->getTypeInstance()->isPhysicalGCType($this->getProduct());
    }

    /**
     * @return bool
     */
    public function isAllowMessage()
    {
        return (bool)$this->getProduct()->getTypeInstance()->isAllowMessage($this->getProduct());
    }

    /**
     * @return bool
     */
    public function isAllowPreview()
    {
        return !$this->getProduct()->getTypeInstance()->isPhysicalGCType($this->getProduct());
    }

    public function getAmount()
    {
        $amount = $this->getPreconfiguredValues()
            ->getData(AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_AMOUNT);
        if (!$amount) {
            $amount = '';
        }
        return $this->escapeHtml($amount);
    }

    public function getCustomAmount()
    {
        $customAmount = $this->getPreconfiguredValues()
            ->getData(AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_CUSTOM_AMOUNT);
        if ($this->getAmount() != 'custom' || !$customAmount) {
            $customAmount  = '';
        }
        return $this->escapeHtml($customAmount );
    }

    public function getEmailTemplate()
    {
        $emailTemplate = $this->getPreconfiguredValues()
            ->getData(AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_EMAIL_TEMPLATE);
        if (!$emailTemplate) {
            $emailTemplate = '';
        }
        return $this->escapeHtml($emailTemplate);
    }

    public function getRecipientName()
    {
        $recipientName = $this->getPreconfiguredValues()
                            ->getData(AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_RECIPIENT_NAME);
        if (!$recipientName) {
            $recipientName = '';
        }
        return $this->escapeHtml($recipientName);
    }

    public function getRecipientEmail()
    {
        $recipientEmail = $this->getPreconfiguredValues()
            ->getData(AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_RECIPIENT_EMAIL);
        if (!$recipientEmail) {
            $recipientEmail = '';
        }
        return $this->escapeHtml($recipientEmail);
    }

    public function getSenderName()
    {
        $senderName = $this->getPreconfiguredValues()
            ->getData(AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_SENDER_NAME);
        if (!$senderName) {
            if (Mage::getSingleton('customer/session')->isLoggedIn()) {
                $customer = Mage::getSingleton('customer/session')->getCustomer();
                if ($customer) {
                    $senderName = $customer->getName();
                } else {
                    $senderName = '';
                }
            }
        }
        return $this->escapeHtml($senderName);
    }

    public function getSenderEmail()
    {
        $senderEmail = $this->getPreconfiguredValues()
            ->getData(AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_SENDER_EMAIL);
        if (!$senderEmail) {
            if (Mage::getSingleton('customer/session')->isLoggedIn()) {
                $customer = Mage::getSingleton('customer/session')->getCustomer();
                if ($customer) {
                    $senderEmail = $customer->getEmail();
                } else {
                    $senderEmail = '';
                }
            }
        }
        return $this->escapeHtml($senderEmail);
    }

    public function getHeadline()
    {
        $headline = $this->getPreconfiguredValues()
            ->getData(AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_HEADLINE);
        if (!$headline ) {
            $headline  = '';
        }
        return $this->escapeHtml($headline);
    }

    public function getMessage()
    {
        $message = $this->getPreconfiguredValues()
            ->getData(AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_MESSAGE);
        if (!$message ) {
            $message  = '';
        }
        return $this->escapeHtml($message);
    }

    public function getPreconfiguredValues()
    {
        if (!$this->_preConfiguredValues) {
            $values = $this->getProduct()->getPreConfiguredValues();
            if ($values === null) {
                $values = new Varien_Object();
            }
            $this->_preConfiguredValues = $values;
        }
        return $this->_preConfiguredValues;
    }

    public function displayProductStockStatus()
    {
        if (version_compare(Mage::getVersion(), '1.8', '<')) {
            return true;
        } else {
            return parent::displayProductStockStatus();
        }
    }
}