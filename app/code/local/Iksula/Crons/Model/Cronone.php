<?php

class Iksula_Crons_Model_Cronone extends Mage_Core_Model_Abstract
{
    /**
     * This function is called from CrononeController to check and update awaitingverification Orders
     *
     * @return processed orders
     */
    public function Hitapi($paymentMethod = 'payu')
    {

        $flag = Mage::getStoreConfig('crons_section/crons_group/cronone');
        // check if cron is enabled from backend
        if ($flag == 1) {

            $fromDate            = new DateTime();
            $fromDate            = $fromDate->format("Y-m-d H:i:s");
            $updateCroneStatuses = Mage::getModel('crons/crons')->getCollection()->getFirstItem();
            $updateCroneStatuses->setData('cronone_status', 'Running : Started at - ' . $fromDate)->save();
            $orders = $this->getOrderCollection($paymentMethod);
            $this->segregateOrders($orders);
        } else {
            $fromDate            = new DateTime();
            $fromDate            = $fromDate->format("Y-m-d H:i:s");
            $updateCroneStatuses = Mage::getModel('crons/crons')->getCollection()->getFirstItem();
            $updateCroneStatuses->setData('cronone_status', 'Disabled from backend : Tried to run at - ' . $fromDate)->save();
        }

    }

    /**
     * Function returns order collection which will be processed further
     *
     * @return array of sales_order objects(orders)
     */

    private function getOrderCollection($paymentMethod)
    {

        // List of all payment methods availeble in payu
        $back15minutes = date('Y-m-d h:i:s',strtotime('-15 min'));
        $orderObj = Mage::getModel('sales/order');
        $orders   = $orderObj->getCollection()->addFieldToFilter('created_at', array('to'=>$back15minutes))->join(array('payment'=>'sales/order_payment'),'main_table.entity_id=parent_id','method');
        if($paymentMethod == 'payu'){
            $orders->addFieldToFilter('method',array('in'=>array('secureebs_standard','payuemi_standard','payudebit_standard','payuMoney_standard','payunet_standard')))->addFieldToFilter('status', 'awaitingverification')->setPageSize(30)->setCurPage(1);
        }
        // elseif($paymentMethod == 'innoviti'){
        //     $orders->addFieldToFilter('method',array('in'=>array('gateway')))->addFieldToFilter('status', 'awaitingverification')->setPageSize(30)->setCurPage(1);
        // }
        return $orders;

    }

    /**
     * Function returns order collection which will be processed further
     * @param array of orders to process further
     */

    public function segregateOrders($orders)
    {
        $payuMethods   = array(
            'secureebs_standard',
            'payuairtel_standard',
            'payudebit_standard',
            'payuMoney_standard',
            'payunet_standard'
        );
        $transactionId = '';
        foreach ($orders as $orderData) {
            if ($orderData->getPayment()) {
                $method = $orderData->getPayment()->getMethodInstance()->getCode();
                if ($method == "gateway") {
                    // $this->fetchInnovitiDataFromApi($orderData);
                } elseif (in_array($method, $payuMethods)) {
                    // get all order Ids and append them with pipe separated so that we can send only one request to payu and get statuses for all of them. This will reduce execution time
                    $transactionId = $transactionId . $orderData->getRealOrderId() . '|';
                }
            }

        }
        // check if payu orders are actually there
        if ($transactionId != "") {
            $this->fetchPayuDataFromApi($transactionId);
        }

        $fromDate            = new DateTime();
        $fromDate            = $fromDate->format("Y-m-d H:i:s");
        $updateCroneStatuses = Mage::getModel('crons/crons')->getCollection()->getFirstItem();
        $updateCroneStatuses->setData('cronone_status', 'Complete : Execution Finished at - ' . $fromDate)->save();
    }

    /**
     * Function returns order information for Innoviti order from the Innoviti API
     *
     * @param data of order to be checked in Innoviti API
     * @return ----
     */

    // private function fetchInnovitiDataFromApi($orderData)
    // {

    //     $status = Mage::getModel('crons/innovityapi')->getApiResponse($orderData);
    //     $orderIdvalue = $orderData->getRealOrderId();
    //     $loadedOrder = Mage::getModel('sales/order')->loadByIncrementId($orderIdvalue);

    //     // If captured at Innoviti, change order status from 'awaitingverification' to 'awaitingconfirmation'
    //     // Else, change order status from 'awaitingverification' to 'canceledpending'
    //     if ($status == '00') {
    //         $this->makeAwaitingConfirmation($loadedOrder, '');
    //     } else {
    //         $this->makeCanceledPending($loadedOrder, '');
    //     }
    // }

    /**
     * Function returns order information for PayU orders from the PayU API
     *
     * @param list of pipe separated order IDs
     * @return ----
     */

    private function fetchPayuDataFromApi($transactionId)
    {

        $statuses = Mage::getModel('crons/payuapi')->getApiResponse($transactionId);
        $orderResponse = $statuses['transaction_details']; //get order data array
        foreach ($orderResponse as $orderid => $orderData) {
            $payuStatus  = $orderData['unmappedstatus']; //get payment status
            $loadedOrder = Mage::getModel('sales/order')->loadByIncrementId($orderid);

            // If captured at PayU, change order status from 'awaitingverification' to 'awaitingconfirmation'
            // Else, change order status from 'awaitingverification' to 'canceledpending'
            if ($payuStatus == 'captured') {
                $authcode = $orderData['mihpayid'];
                // echo $loadedOrder->getRealOrderId()."-".$payuStatus."if<br>";
                $this->makeAwaitingConfirmation($loadedOrder, $authcode);
            } else {
                // echo $loadedOrder->getRealOrderId()."-".$payuStatus."else<br>";
                $this->makeCanceledPending($loadedOrder, $authcode);
            }
        }
    }

    /**
     * Function takes the order and changes its status to awaitingconfirmation
     *
     * @param orderData,authcode
     * @return ----
     */

    private function makeAwaitingConfirmation($loadedOrder, $authcode = '')
    {

        try {

            $loadedOrder->setState(Mage_Sales_Model_Order::STATE_PROCESSING,'processing','Updated By Cron:1')->save();
            Mage::helper('orderitemsstatestatus')->changeItemStatebyOrderState('processing' , $loadedOrder);
            
            /* PO Data not saved using Observer */
                $poData = Mage::getModel('omniapis/storepo')->getCollection()->addFieldToFilter('order_id',array('eq'=>$loadedOrder->getId()))->getData();
                foreach ($poData as $pod) {
                    Mage::getModel('omniapis/storepo')->load($pod['id'])->setStatus('processing')->save();
                }
            /* PO Data not saved using Observer */

            if ($authcode != '') {
                $loadedOrder->setAuthcode($authcode);
            }
            $loadedOrder->save();

        }
        catch (Exception $e) {
            mail("twinkal.g@iksula.com,satyendra.mishra@iksula.com", "Payu Issue OrderId #" . $loadedOrder->getIncrementId(), 'order was not updated');
        }

    }

    /**
     * Function takes the order and changes its status to canceledpending
     *
     * @param orderData,authcode
     * @return ----
     */

    private function makeCanceledPending($loadedOrder, $authcode = '')
    {

        try {

            $loadedOrder->setState(Mage_Sales_Model_Order::STATE_CANCELED,'canceledpending','Updated by Cron:1')->save();
            Mage::helper('orderitemsstatestatus')->changeItemStatebyOrderState('canceledpending' , $loadedOrder);
            
            /* PO Data not saved using Observer */
                $poData = Mage::getModel('omniapis/storepo')->getCollection()->addFieldToFilter('order_id',array('eq'=>$loadedOrder->getId()))->getData();
                foreach ($poData as $pod) {
                    Mage::getModel('omniapis/storepo')->load($pod['id'])->setStatus('canceledpending')->save();
                }
            /* PO Data not saved using Observer */

            if ($authcode != '') {
                $loadedOrder->setAuthcode($authcode);
            }
            $loadedOrder->save();

        }
        catch (Exception $e) {
            mail("twinkal.g@iksula.com,satyendra.mishra@iksula.com", "Payu Issue OrderId #" . $loadedOrder->getIncrementId(), 'order was not updated');
        }

    }

}
