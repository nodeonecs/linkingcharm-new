<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Giftcard2_Model_Import
{
    const COLUMN_COUNT_IN_CSV_FILE = 14;
    const LOG_FILE = 'aw_giftcard2_import.log';
    const FILE_ID = 'giftcard2_csv';
    protected $_uploadFilePathToImport = null;

    //columns
    const CREATED_AT_NUMBER = 0;
    const ORDER_ID_NUMBER = 1;
    const PRODUCT_ID_NUMBER = 2;
    const TYPE_NUMBER = 3;
    const CODE_NUMBER = 4;
    const INITIAL_BALANCE_NUMBER = 5;
    const AVAILABILITY_NUMBER = 6;
    const IS_USED_NUMBER = 7;
    const BALANCE_NUMBER = 8;
    const EXPIRE_AT_NUMBER = 9;
    const RECIPIENT_NAME_NUMBER = 10;
    const SENDER_NAME_NUMBER = 11;
    const WEBSITE_ID_NUMBER = 12;

    public function uploadFile() {
        $uploadPath = Mage::getBaseDir('var') . DS . 'importexport' . DS ;
        $uploadFileName = 'aw_giftcard2.csv';

        $uploader = new Varien_File_Uploader(self::FILE_ID);

        $uploader->setAllowedExtensions(array('csv'));
        $uploader->setAllowRenameFiles(false);
        $uploader->setFilesDispersion(false);

        $uploader->save($uploadPath, $uploadFileName);

        $file = $uploader->getUploadedFileName();
        $this->_uploadFilePathToImport = $uploadPath . $file;
    }


    public function importFromFile() {
        $result = array(
            'status' => true,
            'message' => ''
        );

        $csvObject = new Varien_File_Csv();
        $csvData = $csvObject->getData($this->_uploadFilePathToImport);

        //first line in file - title column
        if (count($csvData) > 1) {
            Mage::log(Mage::helper('aw_giftcard2')->__('Start parsing csv file'), null, self::LOG_FILE);

            $importSuccess = 0;
            foreach ($csvData as $rowNumber => $row) {
                //if title column
                if ($rowNumber == 0) {
                    continue;
                }

                if (count($row) != self::COLUMN_COUNT_IN_CSV_FILE) {
                    Mage::log(Mage::helper('aw_giftcard2')->__(
                        'An error occurred while parsing row %s. The number of columns is not equal to %s',
                        $rowNumber + 1,
                        self::COLUMN_COUNT_IN_CSV_FILE
                    ), null, self::LOG_FILE);
                    continue;
                }

                $resultRowData = array(
                    "created_at"  => "",
                    "order_id" => null,
                    "product_id" => "",
                    "type" => "",
                    "code" => "",
                    "initial_balance" => "",
                    "availability" => "",
                    "is_used" => "",
                    "balance" => "",
                    "expire_at" => "",
                    "recipient_name"  => "",
                    "sender_name"     => "",
                    "website_id" => "",
                );

                $columnNumber = 0;
                $website = null;
                $allWebsites = $this->_getAllWebsites();
                $isSaveGiftCard = true;
                foreach ($resultRowData as $key => $value) {
                    if (!$isSaveGiftCard)
                        break;

                    $resultStr = $str = trim($row[$columnNumber]);
                    switch ($columnNumber) {
                        case self::CREATED_AT_NUMBER:
                            $currentDate = Mage::app()->getLocale()
                                ->date()
                                ->setTimezone(Mage_Core_Model_Locale::DEFAULT_TIMEZONE)
                            ;
                            $resultStr = $this->_getDateFromStr($str);
                            if (!$resultStr || $currentDate->compare($resultStr) < 0) {
                                $resultStr = $currentDate->toString(Varien_Date::DATE_INTERNAL_FORMAT);
                            } else {
                                $resultStr = $resultStr->toString('YYYY-MM-dd');
                            }
                            break;
                        case self::ORDER_ID_NUMBER:
                            $resultStr = null;
                            break;
                        case self::PRODUCT_ID_NUMBER:
                            try {
                                $product = Mage::getModel('catalog/product')->load((int)$str);
                                if (
                                    !$product->getId()
                                    && $product->getProductType() != AW_Giftcard2_Model_Product_Type_Giftcard::TYPE_CODE
                                ) {
                                    $resultStr = 0;
                                }
                            } catch (Exception $e) {
                                Mage::log(Mage::helper('aw_giftcard2')->__(
                                    'An error occurred while parsing row %s. %s',
                                    $rowNumber + 1,
                                    $e->getMessage()
                                ), null, self::LOG_FILE);
                                $resultStr = 0;
                            }
                            break;
                        case self::TYPE_NUMBER:
                            $typesArray =
                                Mage::getModel('aw_giftcard2/source_entity_attribute_giftcard_type')->toArray()
                            ;
                            $typeKey = array_search($str, $typesArray);
                            if (!$typeKey) {
                                $resultStr = 1;
                                Mage::log(Mage::helper('aw_giftcard2')->__(
                                    'Provided type not found in row %s. The default value was set for this Gift Card',
                                    $rowNumber + 1
                                ), null, self::LOG_FILE);
                            } else {
                                $resultStr = $typeKey;
                            }
                            break;
                        case self::CODE_NUMBER:
                            if (empty($str)) {
                                Mage::log(
                                    Mage::helper('aw_giftcard2')->__('GiftCard code is empty in row %s', $rowNumber + 1),
                                    null,
                                    self::LOG_FILE
                                );
                                $isSaveGiftCard = false;
                                break;
                            }
                            $giftcardCollection = Mage::getModel('aw_giftcard2/giftcard')
                                ->getCollection()
                                ->addFieldToFilter('code', $str)
                            ;
                            if ($giftcardCollection->getSize()) {
                                Mage::log(Mage::helper(
                                    'aw_giftcard2')->__('GiftCard code already exists in row %s',
                                    $rowNumber + 1
                                ), null, self::LOG_FILE);
                                $isSaveGiftCard = false;
                            }
                            break;
                        case self::INITIAL_BALANCE_NUMBER:
                            $locale = new Zend_Locale(Mage::app()->getLocale()->getLocaleCode());

                            try {
                                $resultStr = Zend_Locale_Format::getFloat(preg_replace("/[^-0-9\.\,]/", "", $str),
                                    array(
                                        'precision' => 2,
                                        'locale' => $locale
                                    )
                                );
                            } catch (Exception $e) {
                                $resultStr = 0.00;
                                Mage::log(Mage::helper('aw_giftcard2')->__(
                                    'Initial balance is incorrect in row %s. The default value was set for this Gift Card. %s',
                                    $rowNumber + 1,
                                    $e->getMessage()
                                ), null, self::LOG_FILE);
                            }
                            break;
                        case self::AVAILABILITY_NUMBER:
                            $availArray = Mage::getModel('aw_giftcard2/source_giftcard_availability')->toArray();
                            $availKey = array_search($str, $availArray);
                            if (!$availKey) {
                                $resultStr = 1;
                                Mage::log(Mage::helper('aw_giftcard2')->__(
                                    '"Availability" option not found in row %s. The default value was set for this Gift Card',
                                    $rowNumber + 1
                                ), null, self::LOG_FILE);
                            } else {
                                $resultStr = $availKey;
                            }
                            break;
                        case self::IS_USED_NUMBER:
                            $usedArray = Mage::getModel('aw_giftcard2/source_giftcard_used')->toArray();
                            $usedKey = array_search($str, $usedArray);
                            if (!$usedKey) {
                                $resultStr = 3;
                                Mage::log(Mage::helper('aw_giftcard2')->__(
                                    '"Is Used" option not found in row %s. The default value was set for this Gift Card',
                                    $rowNumber + 1
                                ), null, self::LOG_FILE);
                            } else {
                                $resultStr = $usedKey;
                            }
                            break;
                        case self::BALANCE_NUMBER:
                            $locale = new Zend_Locale(Mage::app()->getLocale()->getLocaleCode());

                            try {
                                $resultStr = Zend_Locale_Format::getFloat(preg_replace("/[^-0-9\.\,]/", "", $str),
                                    array(
                                        'precision' => 2,
                                        'locale' => $locale
                                    )
                                );
                            } catch (Exception $e) {
                                $resultStr = $resultRowData[5];
                                Mage::log(Mage::helper('aw_giftcard2')->__(
                                    'Balance is incorrect in row %s. The default value was set for this Gift Card',
                                    $rowNumber + 1,
                                    $e->getMessage()
                                ), null, self::LOG_FILE);
                            }
                            break;
                        case self::EXPIRE_AT_NUMBER:
                            $resultStr = $this->_getDateFromStr($str);
                            if (!$resultStr) {
                                $expireAfter = Mage::helper('aw_giftcard2/config')->getExpireValue($website);
                                if ($expireAfter > 0) {
                                    $createdAt = $this->_getDateFromStr($row[0]);
                                    $expireFrom = $createdAt ? $createdAt : null;

                                    $resultStr = Mage::app()->getLocale()
                                        ->date($expireFrom)
                                        ->setTimezone(Mage_Core_Model_Locale::DEFAULT_TIMEZONE)
                                        ->addDay($expireAfter)
                                        ->toString(Varien_Date::DATE_INTERNAL_FORMAT)
                                    ;
                                } else {
                                    $resultStr = null;
                                }
                            } else {
                                $resultStr = $resultStr->toString('YYYY-MM-dd');
                            }
                            break;
                        case self::RECIPIENT_NAME_NUMBER:
                            $fullName = explode('|', $str);
                            if (isset($fullName[0]) && trim($fullName[0]) != '') {
                                $resultStr = trim($fullName[0]);
                            } else {
                                Mage::log(Mage::helper('aw_giftcard2')->__(
                                    '"Recipient Name" is incorrect in row %s.',
                                    $rowNumber + 1
                                ), null, self::LOG_FILE);
                                $isSaveGiftCard = false;
                            }
                            break;
                        case self::SENDER_NAME_NUMBER:
                            $fullName = explode('|', $str);
                            if (isset($fullName[0]) && trim($fullName[0]) != '') {
                                $resultStr = trim($fullName[0]);
                            } else {
                                Mage::log(Mage::helper('aw_giftcard2')->__(
                                    '"Sender Name" is incorrect in row %s.',
                                    $rowNumber + 1
                                ), null, self::LOG_FILE);
                                $isSaveGiftCard = false;
                            }
                            break;
                        case self::WEBSITE_ID_NUMBER:
                            if (isset($allWebsites[$str])) {
                                $resultStr = $allWebsites[$str];
                                $website = Mage::getModel('core/website')->load($resultStr);
                            } else {
                                $website = Mage::getModel('core/website')->load(1, 'is_default');
                                $resultStr = $website->getId();
                                Mage::log(Mage::helper('aw_giftcard2')->__(
                                    'Website not found in row %s. The default value was set for this Gift Card',
                                    $rowNumber + 1
                                ), null, self::LOG_FILE);
                            }
                            break;
                    }
                    if (!empty($resultStr) || (empty($resultStr) && $resultStr === "0"))
                        $resultRowData[$key] = $resultStr;

                    $columnNumber++;
                }

                if (
                    $resultRowData['type'] !== AW_Giftcard2_Model_Source_Entity_Attribute_Giftcard_Type::PHYSICAL_VALUE
                ) {

                    // Retrieve "Recipient Email"
                    $fullName = explode('|', $row[self::RECIPIENT_NAME_NUMBER]);
                    if (isset($fullName[1]) && trim($fullName[1]) != '') {
                        $resultRowData['recipient_email'] = trim($fullName[1]);
                    } else {
                        Mage::log(
                            Mage::helper('aw_giftcard2')->__('"Recipient Email" is incorrect in row %s.', $rowNumber + 1),
                            null,
                            self::LOG_FILE
                        );
                        $isSaveGiftCard = false;
                    }

                    // Retrieve "Sender Email"
                    $fullName = explode('|', $row[self::SENDER_NAME_NUMBER]);
                    if (isset($fullName[1]) && trim($fullName[1]) != '') {
                        $resultRowData['sender_email'] = trim($fullName[1]);
                    } else {
                        Mage::log(
                            Mage::helper('aw_giftcard2')->__('"Sender Email" is incorrect in row %s.', $rowNumber + 1),
                            null,
                            self::LOG_FILE
                        );
                        $isSaveGiftCard = false;
                    }
                } else {
                    $resultRowData['recipient_email'] = null;
                    $resultRowData['sender_email'] = null;
                }

                if ($isSaveGiftCard) {
                    $resultRowData['email_template'] = 0;

                    Mage::getModel('aw_giftcard2/giftcard')
                        ->setIsImported(true)
                        ->addData($resultRowData)
                        ->save();

                    $importSuccess++;
                }
            }
            $result['message'] = Mage::helper('aw_giftcard2')->__(
                'Imported %s row(s) out of %s',
                $importSuccess,
                count($csvData) - 1
            );
            Mage::log(
                Mage::helper('aw_giftcard2')->__('Stop parsing csv file. ') . $result['message'],
                null,
                self::LOG_FILE
            );

            if ($importSuccess) {
                $result['status'] = true;
            } else {
                $result['status'] = false;
            }
        } else {
            $result['status'] = false;
            $result['message'] = Mage::helper('aw_giftcard2')->__('Csv file is empty');
        }

        return $result;
    }

    protected function _getDateFromStr($input = null) {
        $locale = new Zend_Locale(Mage::app()->getLocale()->getLocaleCode());

        $date = new Zend_Date(null, null, $locale);
        try {
            $date->setDate($input, $locale->getTranslation(null, 'date', $locale));
        } catch (Exception $e) {
            return false;
        }

        return $date;
    }

    protected function _getAllWebsites() {
        $result = array();
        foreach (Mage::app()->getWebsites() as $website) {
            $result[$website->getName()] = $website->getId();
        }
        return $result;
    }

}