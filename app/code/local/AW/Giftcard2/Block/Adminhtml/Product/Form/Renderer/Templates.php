<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_Block_Adminhtml_Product_Form_Renderer_Templates extends Mage_Adminhtml_Block_Widget
    implements Varien_Data_Form_Element_Renderer_Interface
{
    protected $_websites = null;
    protected $_templatesTableId = 'aw-giftcard2-templates';

    public function _construct()
    {
        $this->setTemplate('aw_giftcard2/product/edit/templates.phtml');
    }

    public function getRowsContainerId()
    {
        return $this->_templatesTableId . '_container';
    }

    public function getAddButtonHtml()
    {
        return $this->getLayout()->createBlock('adminhtml/widget_button')
            ->setData(
                array(
                    'label'     => $this->__('Add Template'),
                    'onclick'   => "awGCTemplates.addTemplate()",
                    'class'     => 'add'
                )
            )
            ->toHtml();
    }

    protected function _getRowTemplate()
     {
         $htmlClass = $this->getElement()->getClass();
         $htmlName = $this->getElement()->getName();

         $storeOptions = '';
         $storeValues = Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true);
         foreach ($storeValues as $key => $value) {
             if (!is_array($value)) {
                 $storeOptions .= $this->_optionToHtml(array('value' => $key, 'label' => $value), $storeValues);
             } elseif (is_array($value['value'])) {
                 $storeOptions .= '<optgroup label="' . $value['label'] . '">' . "\n";
                 foreach ($value['value'] as $groupItem) {
                     $storeOptions .= $this->_optionToHtml($groupItem, $storeValues);
                 }
                 $storeOptions .= '</optgroup>' . "\n";
             } else {
                 $storeOptions .= $this->_optionToHtml($value, $storeValues);
             }
         }

         $templateOptions = '';
         $_templateOptionValues = Mage::getModel('aw_giftcard2/source_entity_attribute_giftcard_email_template')->toOptionArray();
         foreach ($_templateOptionValues as $_optionValue) {
             $templateOptions .= "<option value=\"{$_optionValue['value']}\">{$_optionValue['label']}</option>";
         }
         $imageUploaderTemplate = $this->_getImageUploaderTemplate();
         $deleteBtnLabel = $this->__('Delete');
         return <<<HTML
    <tr>
        <td>
            <select class="{$htmlClass} required-entry"
                name="{$htmlName}[#{data.index}][store_id]"
                id="aw_gc2_template_row_#{data.index}_store">
                {$storeOptions}
            </select>
        </td>
        <td>
            <select class="{$htmlClass} required-entry"
                name="{$htmlName}[#{data.index}][template]"
                id="aw_gc2_template_row_#{data.index}_template">
                {$templateOptions}
            </select>
        </td>
        <td>
            {$imageUploaderTemplate}
            <input type="hidden" name="{$htmlName}[#{data.index}][image]" value="" id="aw_gc2_template_row_#{data.index}_image" />
        </td>
        <td class="col-delete">
            <input type="hidden" name="{$htmlName}[#{data.index}][delete]" class="delete" value="" id="aw_gc2_template_row_#{data.index}_delete" />
            <button title="{$deleteBtnLabel}" type="button" class="action scalable delete icon-btn delete-product-option"
                    id="aw_gc2_template_row_#{data.index}_delete_button"
                    onclick="awGCTemplates.deleteTemplate('aw_gc2_template_row_#{data.index}_delete');return false">
                <span>{$deleteBtnLabel}</span>
            </button>
        </td>
    </tr>
HTML;
    }

    protected function _optionToHtml($option, $selected)
    {
        if (is_array($option['value'])) {
            $html = '<optgroup label="' . $option['label'] . '">' . "\n";
            foreach ($option['value'] as $groupItem) {
                $html .= $this->_optionToHtml($groupItem, $selected);
            }
            $html .= '</optgroup>' . "\n";
        } else {
            $html = '<option value="' . $this->escapeHtml($option['value']) . '"';
            $html .= isset($option['title']) ? 'title="' . $this->escapeHtml($option['title']) . '"' : '';
            $html .= isset($option['style']) ? 'style="' . $option['style'] . '"' : '';
            if (in_array($option['value'], $selected)) {
                $html .= ' selected="selected"';
            }
            $html .= '>' . $this->escapeHtml($option['label']) . '</option>' . "\n";
        }
        return $html;
    }

    protected function _getImageUploaderTemplate()
    {
        $imagePlaceholderText = $this->__('Click here or drag and drop to add image');
        return <<<HTML
<div id="aw_gc2_template_row_#{data.index}_image_container" class="images">
    <div class="image place-holder">
        {$imagePlaceholderText}
        <input type="file" name="image" />
    </div>
</div>
HTML;
    }

    protected function _getImageTemplate()
    {
        $deleteImageText = $this->__('Delete image');
        return <<<HTML
        <div class="image">
            <img
                class="product-image"
                src="#{data.image_url}"
                alt="#{data.image_label}" />
            <div class="actions">
                <button type="button" class="delete" title="{$deleteImageText}">
                    <span>{$deleteImageText}</span>
                </button>
            </div>
        </div>
HTML;

    }

    protected function _getTemplatesValues()
    {
        $result = array();
        $values = $this->getElement()->getValue();
        if (is_array($values)) {
            foreach ($values as $value) {
                $data = array(
                    'template' => $value['template'],
                    'store_id' => $value['store_id']
                );
                if ($value['image']) {
                    $data['image'] = $value['image'];
                    $data['image_url'] = AW_Giftcard2_Helper_Image::resizeImage(
                        $value['image'],
                        AW_Giftcard2_Helper_Image::BACKEND_IMAGE_SIZE,
                        AW_Giftcard2_Helper_Image::BACKEND_IMAGE_SIZE
                    );
                }
                $result[] = $data;
            }
        }
        return $result;
    }

    /**
     * Get url to upload files
     *
     * @return string
     */
    protected function _getImageUploadUrl()
    {
        return $this->getUrl('adminhtml/awgiftcard2_product/imageUpload');
    }

    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        $this->setElement($element);
        return $this->toHtml();
    }
}