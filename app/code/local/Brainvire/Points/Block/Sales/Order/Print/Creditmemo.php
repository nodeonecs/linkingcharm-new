<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   Brainvire
 * @package    Brainvire_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * @license    
 */


class Brainvire_Points_Block_Sales_Order_Print_Creditmemo extends Mage_Sales_Block_Order_Print_Creditmemo {

    public function _toHtml() {
        $this->setTemplate('brainvire_points/sales/order/print/creditmemo.phtml');
        return parent::_toHtml();
    }

}

