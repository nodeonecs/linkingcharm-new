<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Rma
 */

/**
 * @var $installer Mage_Core_Model_Resource_Setup
 */
$installer  = $this;
$connection = $installer->getConnection();
$connection->insert(
    $installer->getTable('amrma/status'), array(
        'is_active'  => 1,
        'status_key' => Amasty_Rma_Model_Status::STATUS_DELETED
    )
);

$connection->insert(
    $installer->getTable('amrma/label'), array(
        'status_id' => $connection->lastInsertId($installer->getTable('amrma/status')),
        'store_id'  => 0,
        'label'     => 'Deleted'
    )
);
