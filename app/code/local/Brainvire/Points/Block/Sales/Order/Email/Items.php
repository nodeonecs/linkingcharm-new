<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   Brainvire
 * @package    Brainvire_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * @license    
 */


class Brainvire_Points_Block_Sales_Order_Email_Items extends Mage_Sales_Block_Order_Email_Items {

    public function _toHtml() {
        $this->setTemplate('brainvire_points/email/order/items.phtml');
        return parent::_toHtml();
    }

}