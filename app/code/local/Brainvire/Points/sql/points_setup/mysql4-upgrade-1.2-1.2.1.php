<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   BV
 * @package    BV_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */


$installer = $this;
$installer->startSetup();

$installer->run("
ALTER TABLE {$this->getTable('points/transaction')}
ADD `is_locked` TINYINT( 1 ) NOT NULL DEFAULT '0',
ADD INDEX ( `is_locked` );

ALTER TABLE {$this->getTable('points/transaction')} ADD `lock_changed_date` DATETIME NOT NULL ,
ADD INDEX ( `lock_changed_date` );
");

$installer->endSetup();