<?php
class Rofra_Salesorderitemgrid_Block_Adminhtml_Order_Items_Grid_Renderer_Price
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $value =  $row->getData($this->getColumn()->getIndex());     
        
        //$rowItemId = $row->getId();

        $_productId = $row->getData('product_id');

        $_product = Mage::getModel('catalog/product')->load($_productId);

        $_actualPrice = $_product->getPrice();
         
        echo $_formattedActualPrice = Mage::helper('core')->currency($_product->getPrice(),true,false);
    }
}
?>