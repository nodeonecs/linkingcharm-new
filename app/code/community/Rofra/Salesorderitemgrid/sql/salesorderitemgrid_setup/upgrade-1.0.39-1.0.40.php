<?php
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$setup->startSetup();

$setup->run("		
	ALTER TABLE sales_flat_order_item ADD COLUMN escalation_raised int default 0;
");

$setup->endSetup();
