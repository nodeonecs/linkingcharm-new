<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   BV
 * @package    BV_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */


class Brainvire_Core_Helper_Logger extends Mage_Core_Helper_Abstract {
    const PARENT_HELPER = 'Mage_Core_Helper_Abstract';
    const PARENT_MODEL = 'Mage_Core_Model_Abstract';
    const RESOURCE_MODEL = 'Mage_Core_Model_Mysql4_Abstract';
    const RESOURCE_COLLECTION = 'Mage_Core_Model_Mysql4_Collection_Abstract';
    const BLOCK_TEMPLATE = 'Mage_Core_Block_Template';
    
    const XML_PATH_ENABLE_LOG = 'brainvireall/brainvirecore/logger_enabled';

    
    protected static $_logger;

    
    protected function _getLogger() {
        if (self::$_logger instanceof Brainvire_Core_Model_Logger) {
            
        } else {
            self::$_logger = Mage::getSingleton('brainvirecore/logger');
        }
        return self::$_logger;
    }

    
    public function log($Object, $message, $severity=null, $description=null, $line=null) {

        if (!Mage::getStoreConfig(self::XML_PATH_ENABLE_LOG)) {
            return $this;
        }
        $class_name = get_class($Object);
        $this->_getLogger()->setData(array());
        if (preg_match("/Brainvire_([a-z]+)+/i", $class_name, $matches)) {
            $this->_getLogger()->setModule(@$matches[1]);
        } else {
            $this->_getLogger()->setModule('');
        }
        $this->_getLogger()
                ->setObject($class_name)
                ->setTitle($message)
                ->setLine($line)
                ->setSeverity($severity)
                ->setContent($description)
                ->save();
        return $this;
    }

    
    public function logInvisible($Object, $message, $severity=null) {
        $class_name = get_class($Object);
        if (preg_match("/Brainvire_([a-z]+)+/i", $class_name, $matches)) {
            $this->_getLogger()->setModule(@$matches[1]);
        } else {
            $this->_getLogger()->setModule('');
        }
        $this->_getLogger()
                ->setTitle($message)
                ->setObject($class_name)
                ->setVisibility(0)
                ->setSeverity($severity)
                ->save();
        return $this;
    }

    
    public function exorcise() {
        $Date = new Zend_Date();
        Zend_Date::setOptions(array('extend_month' => true));
        $Date->addDayOfYear((0 - (int) Mage::getStoreConfig('brainvireall/brainvirecore/logger_store_days')));

        foreach (Mage::getModel('brainvirecore/logger')->getCollection()->addOlderThanFilter($Date) as $entry) {
            $entry->delete();
        }
        return $this;
    }

}