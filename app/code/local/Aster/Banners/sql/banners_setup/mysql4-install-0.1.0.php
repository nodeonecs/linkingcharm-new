<?php
$installer = $this;
$installer->startSetup();
$sql=<<<SQLTEXT
CREATE TABLE IF NOT EXISTS `banners` (
  `banners_id` int(11) unsigned NOT NULL auto_increment,
  `banner_title` varchar(255) NOT NULL DEFAULT '',
  `banner_image` varchar(255) NOT NULL DEFAULT '',
  `banner_link` varchar(255) DEFAULT NULL,
  `banner_content` text NOT NULL,
  `sort_order` int(11) DEFAULT '0',
  `banner_type` int(11) DEFAULT '0',
  `banner_status` smallint(6) NOT NULL DEFAULT '0',
  `created_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `store_id` varchar(255) NOT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  PRIMARY KEY  (`banners_id`)
)
SQLTEXT;

$installer->run($sql);
//demo 
//Mage::getModel('core/url_rewrite')->setId(null);
//demo 
$installer->endSetup();
	 