<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   Brainvire
 * @package    Brainvire_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * @license    
 */


class Brainvire_Points_Model_Total_Quote_Points extends Mage_Sales_Model_Quote_Address_Total_Abstract {

    public function __construct() {
        $this->setCode('points');
    }

    public function collect(Mage_Sales_Model_Quote_Address $address) {
        $quote = $address->getQuote();
        $session = Mage::getSingleton('checkout/session');
        $is_customer_logedIn = (bool) $quote->getCustomer()->getId();

        if ($session->getData('use_points') && $address->getBaseGrandTotal() && $is_customer_logedIn) {
            $pointsAmountUsed = abs($session->getData('points_amount'));

            $pointsAmountAllowed = Mage::getModel('points/summary')
                    ->loadByCustomer($quote->getCustomer())
                    ->getPoints();

            $sum = $address->getData('base_subtotal') + $address->getData('base_discount_amount');
            $limitedPoints = Mage::helper('points')->getLimitedPoints($sum);
            $pointsAmountUsed = min($pointsAmountUsed, $pointsAmountAllowed, $limitedPoints);
            $session->setData('points_amount', $pointsAmountUsed);

            $rate = Mage::getModel('points/rate')->loadByDirection(Brainvire_Points_Model_Rate::POINTS_TO_CURRENCY);

            $moneyBaseCurrencyForPoints = $rate->exchange($pointsAmountUsed);
            $moneyCurrentCurrencyForPoints = Mage::app()->getStore()->convertPrice($moneyBaseCurrencyForPoints);

            $baseSubtotalWithDiscount = $address->getData('base_subtotal') + $address->getData('base_discount_amount');
            $subtotalWithDiscount = $address->getData('subtotal') + $address->getData('discount_amount');
            
            if ($moneyBaseCurrencyForPoints >= $baseSubtotalWithDiscount) {
                $neededAmount = ceil($baseSubtotalWithDiscount * $rate->getPoints() / $rate->getMoney());
                $neededAmountBaseCurrency = $rate->exchange($neededAmount);
                $neededAmountCurrentCurrency = Mage::app()->getStore()->convertPrice($neededAmountBaseCurrency);
                $session->setData('points_amount', $neededAmount);
                $address->setGrandTotal($address->getData('grand_total') - $subtotalWithDiscount);
                $address->setBaseGrandTotal($address->getData('base_grand_total') - $baseSubtotalWithDiscount);
                $address->setMoneyForPoints($neededAmountCurrentCurrency);
                $address->setBaseMoneyForPoints($neededAmountBaseCurrency);
                $quote->setMoneyForPoints($neededAmountCurrentCurrency);
                $quote->setBaseMoneyForPoints($neededAmountBaseCurrency);
            } else {
                $address->setGrandTotal($address->getGrandTotal() - $moneyCurrentCurrencyForPoints);
                $address->setBaseGrandTotal($address->getBaseGrandTotal() - $moneyBaseCurrencyForPoints);
                $address->setMoneyForPoints($moneyCurrentCurrencyForPoints);
                $address->setBaseMoneyForPoints($moneyBaseCurrencyForPoints);
                $quote->setMoneyForPoints($moneyCurrentCurrencyForPoints);
                $quote->setBaseMoneyForPoints($moneyBaseCurrencyForPoints);
            }
        }
        return $this;
    }

    public function fetch(Mage_Sales_Model_Quote_Address $address) {
        $session = Mage::getSingleton('checkout/session');
        $quote = $address->getQuote();

        if ($address->getMoneyForPoints()) {
            $description = $session->getData('points_amount');
            $moneyForPoints = $address->getMoneyForPoints();

            $textForPoints = Mage::helper('points/config')->getPointUnitName();
            if ($description) {
                $title = Mage::helper('sales')->__('%s (%s)', $textForPoints, $description);
            } else {
                $title = Mage::helper('sales')->__('%s', $textForPoints);
            }
            $address->addTotal(array(
                'code' => $this->getCode(),
                'title' => $title,
                'value' => -$moneyForPoints
            ));
        }
        return $this;
    }

}
