/**
 *
 * @category    design
 * @package     base_default 
 * 
 */ 
 /* billing_aramex_cities and  shipping_aramex_cities */
 var billingAramexCitiesObj;
 var shippingAramexCitiesObj;
 var billing_aramex_cities_temp;
 var shipping_aramex_cities_temp;
 
jQuery(function(){
	billing_aramex_cities_temp = billing_aramex_cities;
	shipping_aramex_cities_temp = shipping_aramex_cities;
	
	jQuery('.loading_autocomplete').hide();
	    var active = jQuery('.aramex_apilocation_validator').text();
        
        if (active == "0") {
            return false;
        }
	
	/*if(jQuery("input[name='billing[use_for_shipping]']").prop('checked')){*/
		 
		billingAramexCitiesObj = AutoSearchControls('billing',billing_aramex_cities);
		
	/*}*/
	
	shippingAramexCitiesObj = AutoSearchControls('shipping',shipping_aramex_cities);
	
	jQuery("select[name='billing[country_id]']").change(function(){
			getAllCitiesJson('billing');			
	});
	jQuery("select[name='shipping[country_id]']").change(function(){
			getAllCitiesJson('shipping');
	});
	
	
	getAllCitiesJson('billing');
	getAllCitiesJson('shipping');
	
	/*jQuery('input[name="shipping[city]"]').focus(function(){		
		if(shipping_aramex_cities_temp == '' && jQuery(this).val().length >=3){				
			var container = $('shipping-buttons-container');
			container.addClassName('disabled');
			container.setStyle({opacity:.5});           
			jQuery('#shipping_city_loading_autocomplete').show();
		}
	});
	jQuery('input[name="billing[city]"]').focus(function(){
		 if(billing_aramex_cities_temp == '' && jQuery(this).val().length >=3){			 				
				var container = $('billing-buttons-container');
				container.addClassName('disabled');
				container.setStyle({opacity:.5});           
				jQuery('#billing_city_loading_autocomplete').show();
		 }
	});*/
	
	jQuery('input[name="billing[city]"]').blur(function(){addressApiValidation('billing')});
	jQuery('input[name="billing[region_id]"]').blur(function(){addressApiValidation('billing')});
	jQuery('input[name="billing[postcode]"]').blur(function(){addressApiValidation('billing')});
	
	jQuery('input[name="shipping[city]"]').blur(function(){addressApiValidation('shipping')});	
	jQuery('input[name="shipping[region_id]"]').blur(function(){addressApiValidation('shipping')});
	jQuery('input[name="shipping[postcode]"]').blur(function(){addressApiValidation('shipping')});
	
	
});

function replaceStrAll(find, replace, str) {
  return str.replace(new RegExp(find, 'g'), replace);
}


function addressApiValidation(type){
	
	var chk_city = jQuery('input[name="'+type+'[city]"]').val();
	var chk_region_id = jQuery('input[name="'+type+'[region_id]"]').val();
	var chk_postcode = jQuery('input[name="'+type+'[postcode]"]').val();
	var country_code = jQuery("select[name='"+type+"[country_id]']").val();
	var system_base_url = getSystemBaseUrl();
	if(chk_city =='') {chk_city = 'default';}
	if(chk_city =='' || chk_region_id =='' || chk_postcode ==''){
		return false;
	}else{		
		var isDisabled = true;	
       if(type == 'shipping'){
			var container = jQuery('#shipping-buttons-container');
			var show_loading = jQuery('#shipping-please-wait');
		}else{
			var container = jQuery('#billing-buttons-container');
			var show_loading = jQuery('#billing-please-wait');
		}
		var text_info = show_loading.html();
            var replace_str = 'Loading Address Validation...';
			var find_str =  'Loading next step...';
			var new_text = replaceStrAll(find_str, replace_str,text_info);			
		if(isDisabled){
            isDisabled   = false;			
			show_loading.html(new_text);
			container.addClass('disabled');
			container.css({opacity:0.5});
			show_loading.show();
			
			 jQuery.ajax({url: system_base_url+"/apilocationvalidator/index/applyvalidation", data:{city:chk_city,post_code:chk_postcode, country_code: country_code}, type:'Post', 
				success: function(result){
					var response = jQuery.parseJSON(result);
					if(!response.is_valid){
						if(!(response.suggestedAddresses) && response.message !='' ){
							alert(response.message);
							jQuery('input[name="'+type+'[postcode]"]').val('');
						}else if(response.suggestedAddresses){
							jQuery('input[name="'+type+'[city]"]').val(response.suggestedAddresses.City);
						}					
					}
					isDisabled  = true;
				}
			 });
			
		}
		
		
	   setTimeout(function(){
			show_loading.html(text_info);
			container.removeClass('disabled');
			container.css({opacity:1});
			show_loading.hide();
	   },3000);
		
		
		
	}
	
}

function AutoSearchControls(type,search_city){
	return jQuery('input[name="'+type+'[city]"]')
	.autocomplete({
		/*source: search_city,*/
		minLength: 3,
		scroll: true,
		source: function(req, responseFn) {			
			var re = jQuery.ui.autocomplete.escapeRegex(req.term);
			var matcher = new RegExp( "^" + re, "i" );
			var a = jQuery.grep( search_city, function(item,index){
				return matcher.test(item);
			});
			responseFn( a );
		},
		search: function( event, ui ){
			/* open initializer */
			forceDisableNext(type);
		},
		response: function( event, ui ){			
			if(type =='billing' && billing_aramex_cities_temp==''){				
				var temp_arr =[];
				jQuery(ui.content).each(function(i,v){
					temp_arr.push(v.value);
				});				
				if(temp_arr.length == 0){					
					forcetoEanbleNext(type,true);
				}
				billing_aramex_cities_temp = '';
				billingAramexCitiesObj.autocomplete("option","source",function(req, responseFn) {
					var re = jQuery.ui.autocomplete.escapeRegex(req.term);
					var matcher = new RegExp( "^" + re, "i" );
					var a = jQuery.grep( temp_arr, function(item,index){
						return matcher.test(item);
					});					
					responseFn( a );
				});
				/* force to enable to go next step*/
				var container = $('billing-buttons-container');
                var isDisabled = (false ? true : false);
                if (!isDisabled) {
                    container.removeClassName('disabled');
                    container.setStyle({opacity:1});
                }
                //checkout._disableEnableAll(container, isDisabled);
                Element.hide('billing_city_loading_autocomplete');
				/*jQuery('input[name="'+type+'[city]"]').val('');*/
				
			}				
			if(type =='shipping' && shipping_aramex_cities_temp==''){
				var temp_arr =[];
				jQuery(ui.content).each(function(i,v){
					
					temp_arr.push(v.value);
				});
				if(temp_arr.length == 0){					
					forcetoEanbleNext(type,true);
				}
				shipping_aramex_cities_temp = '';
				shippingAramexCitiesObj.autocomplete("option","source",function(req, responseFn){
					var re = jQuery.ui.autocomplete.escapeRegex(req.term);
					var matcher = new RegExp( "^" + re, "i" );
					var a = jQuery.grep( temp_arr, function(item,index){
						return matcher.test(item);
					});					
				});
				/* force to enable to go next step*/
				var container = $('shipping-buttons-container');
                var isDisabled = (false ? true : false);
                if (!isDisabled) {
                    container.removeClassName('disabled');
                    container.setStyle({opacity:1});
                }
                //checkout._disableEnableAll(container, isDisabled);
                Element.hide('shipping_city_loading_autocomplete');				
				/*jQuery('input[name="'+type+'[city]"]').val('');*/
			}
		},
		select : function(event, ui) {
			jQuery('input[name="'+type+'[city]"]').attr('rel',ui.item.label);
		},
		open : function() {
			jQuery('input[name="'+type+'[city]"]').attr('rel', 0);
		},
		
	
		close : function() {                    
			if (jQuery('input[name="'+type+'[city]"]').attr('rel')=='0')
				/*jQuery('input[name="'+type+'[city]"]').val('');*/
			forcetoEanbleNext(type);
		}		
	}).focus(function () { jQuery(this).autocomplete("search", ""); });
	
	
	
	
}

function forceDisableNext(type){
	var container = $(type+'-buttons-container');
	container.addClassName('disabled');
	container.setStyle({opacity:.5});           
	jQuery('#'+type+'_city_loading_autocomplete').show();	
}	

 
 function  forcetoEanbleNext(type,reset=false){		
		var container = $(type+'-buttons-container');
		var isDisabled = (false ? true : false);
		if (!isDisabled) {
			container.removeClassName('disabled');
			container.setStyle({opacity:1});
		}
		if(reset){
			/*jQuery('input[name="'+type+'[city]"]').val('');*/
		}				
		Element.hide(type+'_city_loading_autocomplete');	
 }

 
function getAllCitiesJson(type){
	var system_base_url = getSystemBaseUrl();
	var country_code = jQuery("select[name='"+type+"[country_id]']").val();
	var mycities = ''; 
	var url_check = system_base_url+"apilocationvalidator/index/searchallcitiesJson?country_code="+country_code;
	if(type="billing"){
		billing_aramex_cities_temp = '';	
		billingAramexCitiesObj.autocomplete("option","source",url_check);	
	}
	
	if(type="shipping"){
		shipping_aramex_cities_temp = '';
		shippingAramexCitiesObj.autocomplete("option","source",url_check);	
	}
    
	
}

 function applyAramexValidator(type){  	 
	var country_code = jQuery("select[name='"+type+"[country_id]']").val();
	 var system_base_url = getSystemBaseUrl();
		if(country_code){
		  jQuery.ajax({url: system_base_url+"/apilocationvalidator/index/applyapivalidator", data:{country_code:country_code}, type:'Post', success: function(result){
			var response = jQuery.parseJSON(result);			
			if(typeof(response.post_code_required) != 'undefined'){
				if(response.post_code_required == "1"){					
					jQuery("input[name='"+type+"[postcode]']").addClass('required-entry');					
				}else{
					/* set city as requried */
					jQuery("input[name='"+type+"[postcode]']").removeClass('required-entry');
					jQuery("input[name='"+type+"[city]']").addClass('required-entry');					
				}
			}		
		}
		});
	}
 }
 
