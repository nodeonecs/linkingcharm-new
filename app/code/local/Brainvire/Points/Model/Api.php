<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */


class Brainvire_Points_Model_Api {

    
    public function addTransaction($amount, $action, $customer, $objectForAction = null, $commentParams = array(), $additionalData = array()) {
        $transactionId = 0;
        try {
            $a = Brainvire_Points_Model_Actions_Abstract::getInstance($action, $customer)
                    ->setAmount($amount)
                    ->setObjectForAction($objectForAction)
                    ->setCommentParams($commentParams)
                    ->addTransaction($additionalData);

            $transactionId = $a->getTransaction()->getId();
        } catch (Exception $ex) {
            return 0;
        }
        return $transactionId;
    }

   
    public function changeMoneyToPoints($amount, $customer = null, $website = null) {
        $rate = Mage::getModel('points/rate');
        if ($customer instanceof Mage_Customer_Model_Customer) {
            $rate->setCurrentCustomer($customer);
        }
        if ($website instanceof Mage_Core_Model_Website) {
            $rate->setCurrentWebsite($website);
        }
        return
                        $rate
                        ->loadByDirection(Brainvire_Points_Model_Rate::CURRENCY_TO_POINTS)
                        ->exchange($amount);
    }

   
    public function changePointsToMoney($amount, $customer = null, $website = null) {
        $rate = Mage::getModel('points/rate');
        if ($customer instanceof Mage_Customer_Model_Customer) {
            $rate->setCurrentCustomer($customer);
        }
        if ($website instanceof Mage_Core_Model_Website) {
            $rate->setCurrentWebsite($website);
        }
        return
                        $rate
                        ->loadByDirection(Brainvire_Points_Model_Rate::POINTS_TO_CURRENCY)
                        ->exchange($amount);
    }

    
    public function getCustomerTransactions($customer) {
        $summary = Mage::getModel('points/summary')->loadByCustomer($customer);
        return Mage::getModel('points/transaction')->getCollection()->addFieldToFilter('summary_id', $summary->getId());
    }

}
