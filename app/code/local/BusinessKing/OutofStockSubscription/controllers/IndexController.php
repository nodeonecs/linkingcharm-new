<?php

/**
 * Out of Stock Subscription index controller
 *
 * @category    BusinessKing
 * @package     BusinessKing_OutofStockSubscription
 */
class BusinessKing_OutofStockSubscription_IndexController extends Mage_Core_Controller_Front_Action
{
	public function indexAction()
	{ 
		$productId = $this->getRequest()->getPost('product');
		$email = $this->getRequest()->getPost('subscription_email');
		if ($email && $productId) {
			
		Mage::getModel('outofstocksubscription/info')->saveSubscrition($productId, $email);
  
			if(Mage::registry('is_subscribed'))
			{
				echo json_encode('<span class="success">Subscription Success </span>');
			}
			else
			{
				echo json_encode('<span class="alert">You Are Already Subscribed To This Product</span>');
			}
			
		Mage::unregister('is_subscribed');
			//$this->_getSession()->addSuccess($this->__('Subscription added successfully.'));
						
			//$product = Mage::getModel('catalog/product')->load($productId);
			//$product->getProductUrl();
			//$url = $product->getData('url_path');
			//$this->_redirect('catalog/product/view', array('id'=>$productId));
			//$this->_redirect($url);
			
			//return false;
		}
		else {
			$this->_redirect('');
		}		
	}
	
    protected function _getSession()
    {
        return Mage::getSingleton('checkout/session');
    }
}