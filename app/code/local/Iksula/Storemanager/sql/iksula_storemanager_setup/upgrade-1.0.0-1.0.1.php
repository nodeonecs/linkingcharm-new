<?php
$installer = $this;

$installer->startSetup();

$installer->run("
ALTER TABLE {$this->getTable('core/store_group')}
    ADD UNIQUE (website_id, store_code);
");

$installer->endSetup();
