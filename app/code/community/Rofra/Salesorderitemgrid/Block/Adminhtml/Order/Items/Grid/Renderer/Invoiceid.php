<?php
/**
 * @category    Graphic Sourcecode
 * @package     Rofra_Salesorderitemgrid
 * @license     http://www.apache.org/licenses/LICENSE-2.0
 * @author      Rodolphe Franceschi <rodolphe.franceschi@gmail.com>
 */
class Rofra_Salesorderitemgrid_Block_Adminhtml_Order_Items_Grid_Renderer_Invoiceid
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $item_id = $row->getSubOrderid();
        $invoiceId =  $row->getInvoiceId();        
        if(!$invoiceId){
            echo "<a onclick='addInvoiceId(".$item_id.")'>Add Invoice Id</a>";
        }else{
            echo "<a onclick='updateInvoiceId(".$item_id.",".$invoiceId.")'>".$invoiceId."</a>";
        }
    }
}