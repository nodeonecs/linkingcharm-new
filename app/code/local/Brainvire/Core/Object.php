<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   Brainvire
 * @package    Brainvire_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * @license    
 */


class Brainvire_Core_Object extends Varien_Object {

    
    public function log($message, $severity=null, $details='') {
        Mage::helper('brainvirecore/logger')->log($this, $message, $severity, $details);
    }

}