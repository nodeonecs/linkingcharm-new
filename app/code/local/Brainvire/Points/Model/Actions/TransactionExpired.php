<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   Brainvire
 * @package    Brainvire_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * @license    
 */


class Brainvire_Points_Model_Actions_TransactionExpired extends Brainvire_Points_Model_Actions_Abstract {

    protected $_action = 'transaction_expired';
    protected $_comment = 'Transaction expired #%s';
    protected $_commentHtml = 'Transaction expired #%s';

    protected function _applyLimitations($amount) {
        
        if ($amount > 0 || $this->getSummary()->getPoints() <= 0)
            $amount = 0;
        else {
            
            if ($this->getSummary()->getPoints() <= - $amount)
                $amount = - $this->getSummary()->getPoints();
        }

        return $amount;
    }

    public function getComment() {
        if (isset($this->_commentParams['transaction_id'])) {
            return Mage::helper('points')->__($this->_comment, $this->_commentParams['transaction_id']);
        }
        return $this->_comment;
    }

    public function getCommentHtml($area = self::ADMIN) {
        if (!$this->getTransaction())
            return;

        $expiredTransactionId = substr($this->getTransaction()->getComment(), strpos($this->getTransaction()->getComment(), '#') + 1);
        if ($expiredTransactionId) {
            return Mage::helper('points')->__($this->_comment, $expiredTransactionId);
        }
    }

    protected function _updateTransactionsBalancePointsSpent() {
        return $this;
    }

    public function addTransaction($additionalData = array()) {
       
        $this->getObjectForAction()
                ->setBalanceChangeSpent($this->getObjectForAction()->getBalanceChange())
                ->save();
        parent::addTransaction($additionalData);
        return $this;
    }

}
