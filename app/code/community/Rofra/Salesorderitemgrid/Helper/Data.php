<?php
/**
 * @category    Graphic Sourcecode
 * @package     Rofra_Salesorderitemgrid
 * @license     http://www.apache.org/licenses/LICENSE-2.0
 * @author      Rodolphe Franceschi <rodolphe.franceschi@gmail.com>
 */
class Rofra_Salesorderitemgrid_Helper_Data extends Mage_Core_Helper_Abstract
{


	public function getFulfilmentEnable(){


		$hlp = Mage::helper('amrolepermissions');

         $rule = $hlp->currentRule();
         $custom_role = $rule->getCustomRole();

                  $custom_role = $custom_role[0];

         $aStoreViewAccess = $rule->getScopeStoreviews();
         $aStoreGroupIds = array();

         if($custom_role != 'S_A'){

            if(!empty($aStoreViewAccess)){

                foreach($aStoreViewAccess as $storekey => $storeidValue){

                   $storegroupid = Mage::getSingleton('core/store')->load($storeidValue)->getGroupId();
                   $aStoreGroupIds [] = $storegroupid;

                }

            }

            $aStoreType = Mage::getSingleton('storetype/storetype')->getCollection()
                ->addFieldToFilter('fulfilment' , array('eq' => '1'))->addFieldToSelect(array('id'));
                //exit;


                foreach($aStoreType as $storekey => $storevalueid ){

                    $aStoreTypeIds [] = $storevalueid->getId();

                }



            foreach($aStoreGroupIds as $storekey => $storegroupids){

                $aStoretypeids []= Mage::getModel('core/store_group')->load($storegroupids)->getStoreTypeid();


            }




            $aStoretypeids = array_unique($aStoretypeids);



            foreach($aStoretypeids as $storeid => $storevalueType){

                if(in_array($storevalueType , $aStoreTypeIds)){


                    $bFullifillment_check = true;
                }


            }

        }else{
                    $bFullifillment_check = true;

        }


        return $bFullifillment_check;
	}



    public function changestatusitems($orderitemid , $aOrderstatus){


        // Mage::log(print_r(Mage::getModel('sales/order_item')->load($orderitemid)->getData()) , null , 'before.log');
        $ModelOrderStateChange1 = Mage::getModel('sales/order_item')->load($orderitemid)->addData($aOrderstatus);

        $ModelOrderStateChange1->setId($orderitemid)->save();

        /*Mage::log(print_r(Mage::getModel('sales/order_item')->load($orderitemid)->getData() ) , null , 'after.log');*/


    }

    /********************
        @function :- Return the citywise listing of stores for the autoallocation removing the Rejected stores.

    /*************/

    public function citywisestorelisting($item_id , $city_name){



        $city_id = Mage::getModel('romcity/romcity')->load($city_name , 'cityname')->getCityId();
      $cityCollections = Mage::getModel('core/store_group')->getCollection()->addFieldToFilter('store_city' , array('eq' => $city_id))->addFieldToFilter('store_definition' , array('eq' => 'store'))->addFieldToFilter('active_status' , array('eq' => '1'));
      //echo $cityCollections->getSelect();

      /*$cityCollections = Mage::getModel('core/store_group')->getCollection()->join(array('og' =>'storeinventory/storeinventory'), 'main_table.store_code = og.store_code', array('main_table.group_id'));

        $cityCollections->getSelect()->where('main_table.store_definition = (?)', 'store');
        $cityCollections->getSelect()->where('main_table.store_city = (?)', $city_id);
        $cityCollections->getSelect()->where('main_table.active_status = (?)', '1');
        $cityCollections->getSelect()->order('og.inventory desc');// DESC OR ASC*/



      /*$cityCollections = Mage::getModel('core/store_group')->getCollection()->join(array('og' =>'storeinventory/storeinventory'), 'main_table.store_code = og.store_code', array('main_table.group_id'));

        $cityCollections->getSelect()->where('main_table.store_definition = (?)', 'store');
        $cityCollections->getSelect()->where('main_table.store_city = (?)', $city_id);
        $cityCollections->getSelect()->where('main_table.active_status = (?)', '1');
        $cityCollections->getSelect()->order('og.inventory desc');// DESC OR ASC*/



      $aStoresReadyAllocation = array();

      foreach($cityCollections as $cityData){

        $sStoreViewId = Mage::getModel('core/store')->load($cityData->getGroupId() , 'group_id')->getStoreId();
        $inventory_check = Mage::getModel('salesorderitemgrid/salesorderallocate')->getInventoryCheckforautoallocation($item_id , $cityData->getGroupId());// Inventory Check for the Store View Id and sku .
        $active_check = Mage::getModel('salesorderitemgrid/salesorderallocate')->getStoresActiveStatusforautoallocation($sStoreViewId); // Check Store Status is active or inactive.

        if($inventory_check && $active_check) // Assign only if the inventory is available and
          $aStoresReadyAllocation [] = $sStoreViewId;
      }
      /*echo '<pre>';
      print_r($aStoresReadyAllocation);*/
      $AllocateItems = Mage::getModel('salesorderitemgrid/autoallocatestatus')->load($item_id , 'item_id');

      $allocateid = $AllocateItems->getId();
      /*$CurrentAllocatedStores = $AllocateItems->getCurrentAllocatedStores();
      $CurrentAcceptedStores = $AllocateItems->getCurrentAcceptedStores();*/
      $sRejectedStores = $AllocateItems->getRejectedStores();

      if($sRejectedStores != 'NULL'){
        $aRejectedStores =  explode(',' , $AllocateItems->getRejectedStores());
      }
      else{
        $aRejectedStores = array();
      }

      $aStoresReadyAllocation = array_diff($aStoresReadyAllocation , $aRejectedStores);
      shuffle($aStoresReadyAllocation);

      return $aStoresReadyAllocation;
    }


/*******
      @function:- Autoallocate the order to valid Stores.

*********************/

    public function autoallocatetostores($aStoresCitywiseListing , $item_id){



        $AllocateItems = Mage::getModel('salesorderitemgrid/autoallocatestatus')->load($item_id , 'item_id');


        $allocateid = $AllocateItems->getId();
        $oCoreGroupModel = Mage::getSingleton('core/store');
        $iStoreCodeId1 = (isset($aStoresCitywiseListing[0]) ? $oCoreGroupModel->load($aStoresCitywiseListing[0])->getGroupId() : 'NULL');
        $iStoreCodeId2 = (isset($aStoresCitywiseListing[1]) ? $oCoreGroupModel->load($aStoresCitywiseListing[1])->getGroupId() : 'NULL');
        $iStoreCodeId3 = (isset($aStoresCitywiseListing[2]) ? $oCoreGroupModel->load($aStoresCitywiseListing[2])->getGroupId() : 'NULL');
        $iStoreCodeId4 = (isset($aStoresCitywiseListing[3]) ? $oCoreGroupModel->load($aStoresCitywiseListing[3])->getGroupId() : 'NULL');
        $iStoreCodeId5 = (isset($aStoresCitywiseListing[4]) ? $oCoreGroupModel->load($aStoresCitywiseListing[4])->getGroupId() : 'NULL');

        $aAllocatedStores = $aStoresCitywiseListing[0].','.$aStoresCitywiseListing[1].','.$aStoresCitywiseListing[2].','.$aStoresCitywiseListing[3].','.$aStoresCitywiseListing[4];
        $aAutoallocationsData = array('current_allocated_stores' => $aAllocatedStores);
        $OrderItemsAllocate = array('store_1' => $aStoresCitywiseListing[0] , 'store_2' => $aStoresCitywiseListing[1] , 'store_3' => $aStoresCitywiseListing[2]  , 'store_4' => $aStoresCitywiseListing[3]  , 'store_5' => $aStoresCitywiseListing[4] , 'store_code_1' => $iStoreCodeId1 , 'store_code_2' => $iStoreCodeId2 , 'store_code_3' => $iStoreCodeId3  , 'store_code_4' => $iStoreCodeId4  , 'store_code_5' => $iStoreCodeId5);

        $autoallocate_model = Mage::getModel('sales/order_item')->load($item_id)->addData($OrderItemsAllocate);

        $autosalesorderallocate_model = Mage::getModel('salesorderitemgrid/autoallocatestatus')->load($allocateid)->addData($aAutoallocationsData);


      try {
        $autoallocate_model->setId($item_id)->save();
        $autosalesorderallocate_model->setId($allocateid)->save();
        //return "Data updated successfully.";

      } catch (Exception $e){
        return $e->getMessage();
      }


    }

    public function pickupdateChanged($item){
      $orderId = $item->getOrderId();
      $orderModel = Mage::getModel('sales/order')->load($orderId);
      $address = $orderModel->getBillingAddress();
      $payType = 'store_pickup';
      
      $emailTemplateId = Mage::getStoreConfig('payment/payatstore/payatstore_template');
      
      $smsHelper = Mage::helper('smsapp');
        if( $item->getCheckoutMethod() == 'pickable' ){
          /* For SMS */
          $smsHelper->getTemplateAndSendSmsForStorePickup($payType,$address->getTelephone(),$address->getFirstname(),$orderModel->getIncrementId(),'',$item->getStoreId(),$item->getStoreName(),$item->getPickupEndDate(),$item->getSku());
          
          /* For EMAIL */ 
          $itemsData[$item->getSku()] = array('sku'=>$item->getSku(),'store_name' => $item->getStoreName(),'store_address' => $item->getStoreAddress(),'store_city' => $item->getStoreCity(),'store_pincode' => $item->getStorePincode(),'store_zone' => $item->getStoreZone(),'store_region' => $item->getStoreRegion(),'pickup_start_date' => $item->getPickupStartDate(),'pickup_date' => $item->getData('pickup_end_date'));
            foreach ($itemsData as $key => $emailTemplateVariables) {
                  $emailTemplate  = Mage::getModel('core/email_template');
                  $emailTemplate->load($emailTemplateId);
                  
                  // Get General email address (Admin->Configuration->General->Store Email Addresses)
                  $salesData['email'] = Mage::getStoreConfig('trans_email/ident_general/email');
                  $salesData['name'] = Mage::getStoreConfig('trans_email/ident_general/name');
           
                  $emailTemplate->setSenderName($salesData['name']);
                  $emailTemplate->setSenderEmail($salesData['email']);

                  $emailTemplate->send($orderModel->getCustomerEmail(), $orderModel->getStoreName(), $emailTemplateVariables);
            }

        }

    }


    public function getInvoiceOfOrderItem($orderId){

        
        $oOrderItemsObject = Mage::getModel('sales/order_item')->load($orderId);
         $sOrderItemsType = $oOrderItemsObject->getProductType();
         $iParentItemId = $oOrderItemsObject->getParentItemId();
        if(strtolower($sOrderItemsType) == 'simple'){

            if($iParentItemId){

                $orderId = $iParentItemId;

            }

        }

        $order = Mage::getModel('sales/order')->loadByIncrementId($oOrderItemsObject->getIncrementalId());

        $orderItems = $order->getItemsCollection();
        $invoiceItems = Mage::getModel('sales/order_invoice')->getCollection()
                        ->addAttributeToSelect('entity_id');

        $invoiceItems->getSelect()->join(
            array('invoice_item' =>'sales_flat_invoice_item'),
             'main_table.entity_id= invoice_item.parent_id',
            array('invoice_item.order_item_id'));

        $invoiceItems->addFieldToFilter('invoice_item.order_item_id', array('eq'=>$orderId));

        return $invoiceItems;


    }


    public function getShipmentOfOrderItem($orderId){        
        
         $oOrderItemsObject = Mage::getModel('sales/order_item')->load($orderId);
         $sOrderItemsType = $oOrderItemsObject->getProductType();
         $iParentItemId = $oOrderItemsObject->getParentItemId();
        if(strtolower($sOrderItemsType) == 'simple'){

            if(!is_null($iParentItemId)){

                $orderId = $iParentItemId;

            }

        }        

        $order = Mage::getModel('sales/order')->loadByIncrementId($oOrderItemsObject->getIncrementalId());

        $orderItems = $order->getItemsCollection();
        $shippedItems = Mage::getModel('sales/order_shipment')->getCollection()
                        ->addAttributeToSelect('entity_id');

        $shippedItems->getSelect()->join(
            array('shipment_item' =>'sales_flat_shipment_item'),
             'main_table.entity_id= shipment_item.parent_id',
            array('shipment_item.order_item_id'));

        $shippedItems->addFieldToFilter('shipment_item.order_item_id', array('eq'=>$orderId));

        return $shippedItems;

    }

    public function getPickupDate() {
    $storeintime = Mage::getStoreConfig('iksula_storemanager/store/storeintime');
      $storeouttime = date('Y-m-d').' '.Mage::getStoreConfig('iksula_storemanager/store/storeouttime');
      $storedispatchstarttime = Mage::getStoreConfig('iksula_storemanager/store/storedispatchstarttime');
      $storedispatchendtime = Mage::getStoreConfig('iksula_storemanager/store/storedispatchendtime');
      $displaydate = Mage::getModel('core/date')->date('Y-m-d H:i:s',strtotime('+'.$storedispatchstarttime.'hours'));
      
      if($displaydate < $storeouttime) {
          if($displaydate > date('d-m-Y').' '.$storeintime) {
              $start_date = Mage::getModel('core/date')->date('d-m-Y H:i:s',strtotime('+'.$storedispatchstarttime.'hours'));
              $end_date = Mage::getModel('core/date')->date(date('d-m-Y',strtotime($start_date.'+'.$storedispatchendtime.'hours')));    
          }else {
              $start_date = Mage::getModel('core/date')->date(date('d-m-Y') .' '.$storeintime);        
              $end_date = Mage::getModel('core/date')->date(date('d-m-Y',strtotime($start_date.'+'.$storedispatchendtime.'hours')));    
          }
      }else{
          $start_date = Mage::getModel('core/date')->date(date('d-m-Y',strtotime('+1 day')) .' '.$storeintime);    
          $end_date = Mage::getModel('core/date')->date(date('d-m-Y',strtotime($start_date.'+'.$storedispatchendtime.'hours')));    
      }

      $result = array();
      $result['start_date'] = $start_date;
      $result['end_date'] = $end_date;

      return $result;
  }

    
}
