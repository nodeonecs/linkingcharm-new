<?php 

class Iksula_Rewrites_ChangepasswordController extends Mage_Core_Controller_Front_Action
{
    //change password action 
    public function passwordAction()
    {
        $params = $this->getRequest()->getParams();

        if($params)
        {
        	$email = $this->getRequest()->getParam('email');

        	$password = $this->getRequest()->getParam('password');

        	//Mage::getSingleton('checkout/session')->clear();

        	Mage::getSingleton('customer/session')->clear();


        	$session = Mage::getSingleton('customer/session');

       		$session->login($email, $password);

       		//$url = 'customer/account/changepassword';

       		$this->getResponse()->setRedirect(Mage::getUrl("customer/account/changepassword/"));


        }


    }

}