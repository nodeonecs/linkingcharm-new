<?php
class Brainvire_OrderImport_ImportController extends Mage_Adminhtml_Controller_action{
    public function importAction() {
      $this->loadLayout();   
      $this->renderLayout(); 
	  
    }
    public function orderimportAction(){

        try {
                //save text file
                $uploader = new Varien_File_Uploader('import_order');
                $uploader->setAllowedExtensions(array('csv'));
                $path = Mage::app()->getConfig()->getTempVarDir() . '/import/';
                $uploader->save($path);

                //If file is uploaded
                if ($uploadFile = $uploader->getUploadedFileName()) {
                    //load file
                    $filePath = $path . $uploadFile;
                    $lines = file($filePath);
                     $csv = new Varien_File_Csv();
                       // Mage::log($filepath);
                     $data = $csv->getData($filePath);
                foreach ($data as $index => $orderinfo) {
                            $i = 0;
                            //$tmp=$data[$index];
                            if ($index == 0) {
                                $header = $data[$index];
                                continue;
                            }

                            $tmp[$data[$index][0]][] = $data[$index];
                        }
     foreach ($tmp as $vno => $orderItem) {
       
       //echo '<pre>';print_r($orderItem);exit;
       $this->createOrder($orderItem, $header);
    }
           
        }
                   
                else
                    throw new Exception('Unable to load file');
            } catch (Exception $ex) {
                Mage::getSingleton('adminhtml/session')->addError($this->__('An error occured : %s', $ex->getMessage()));
            }
    }
   
    
    public function createOrder($data = null, $header = null){
         //echo '<pre>';print_r($data);exit;

         foreach ($data as $orderinfo) {

            $mail = trim(trim($orderinfo[10])); //64
            
            break;
        }
          
            $customer = Mage::getModel('customer/customer')
                      ->setWebsiteId(1)
                      ->loadByEmail($mail);
            $transaction = Mage::getModel('core/resource_transaction');
            $storeId = $customer->getStoreId();
            $reservedOrderId = Mage::getSingleton('eav/config')->getEntityType('order')->fetchNewIncrementId($storeId);
            //echo $reservedOrderId;exit;
            $order = Mage::getModel('sales/order')
                ->setIncrementId($reservedOrderId)
                ->setStoreId($storeId)
                ->setQuoteId(0)
                ->setGlobal_currency_code('INR')
                ->setBase_currency_code('INR')
                ->setStore_currency_code('INR')
                ->setOrder_currency_code('INR');

            // set Customer data
            $order->setCustomer_email($customer->getEmail())
                ->setCustomerFirstname($customer->getFirstname())
                ->setCustomerLastname($customer->getLastname())
                ->setCustomerGroupId($customer->getGroupId())
                ->setCustomer_is_guest(0)
                ->setCustomer($customer);

            // set Billing Address
            $billing = $customer->getDefaultBillingAddress();
            $billingAddress = Mage::getModel('sales/order_address')
                ->setStoreId($storeId)
                ->setAddressType(Mage_Sales_Model_Quote_Address::TYPE_BILLING)
                ->setCustomerId($customer->getId())
                ->setCustomerAddressId($customer->getDefaultBilling())
                ->setCustomer_address_id($billing->getEntityId())
                ->setPrefix($billing->getPrefix())
                ->setFirstname($billing->getFirstname())
                ->setMiddlename($billing->getMiddlename())
                ->setLastname($billing->getLastname())
                ->setSuffix($billing->getSuffix())
                ->setCompany($billing->getCompany())
                ->setStreet($billing->getStreet())
                ->setCity($billing->getCity())
                ->setCountry_id($billing->getCountryId())
                ->setRegion($billing->getRegion())
                ->setRegion_id($billing->getRegionId())
                ->setPostcode($billing->getPostcode())
                ->setTelephone($billing->getTelephone())
                ->setFax($billing->getFax());
            $order->setBillingAddress($billingAddress);

            $shipping = $customer->getDefaultShippingAddress();
            $shippingAddress = Mage::getModel('sales/order_address')
                ->setStoreId($storeId)
                ->setAddressType(Mage_Sales_Model_Quote_Address::TYPE_SHIPPING)
                ->setCustomerId($customer->getId())
                ->setCustomerAddressId($customer->getDefaultShipping())
                ->setCustomer_address_id($shipping->getEntityId())
                ->setPrefix($shipping->getPrefix())
                ->setFirstname($shipping->getFirstname())
                ->setMiddlename($shipping->getMiddlename())
                ->setLastname($shipping->getLastname())
                ->setSuffix($shipping->getSuffix())
                ->setCompany($shipping->getCompany())
                ->setStreet($shipping->getStreet())
                ->setCity($shipping->getCity())
                ->setCountry_id($shipping->getCountryId())
                ->setRegion($shipping->getRegion())
                ->setRegion_id($shipping->getRegionId())
                ->setPostcode($shipping->getPostcode())
                ->setTelephone($shipping->getTelephone())
                ->setFax($shipping->getFax());

            $order->setShippingAddress($shippingAddress)
                ->setShippingMethod($orderinfo[5]);
                   
           
            $orderPayment = Mage::getModel('sales/order_payment')
                ->setStoreId($storeId)
                ->setCustomerPaymentId(0)
                ->setMethod($orderinfo[4]);

            $order->setPayment($orderPayment);
            $subTotal = 0;
            try{
            foreach($data as $orderinfo){
                
                $sku=trim($orderinfo[11]);
                $qty=trim($orderinfo[13]);
               /*  if ($sku != null) {
                   $productId = $this->getProductBySku($sku);
                    if ($productId  != null) {
                        $simpleProductId = $simpleProduct->getEntityId(); $sku = $simpleProduct->getSku();
                        // adding Stock before order import
                        $this->addStocksBeforeImport( $simpleProductId, $xl3, $sku, $voucherno );
                        $orderData = $this->setProductsInfo($simpleProductId, $orderData, $xl3);
                    } else {
                        throw new Exception('Error For this Style code' . $styleCode);
                    }
                }*/
            
            $product = Mage::getModel('catalog/product');
            $productId = $product->getIdBySku($sku);
         

            // let say, we have 2 products
            
            $products = array(
                 $productId => array(
                    'qty' => $qty
                ), 
               
            );
           //echo '<pre>';print_r($products);exit;
             $prodPrice   = trim($orderinfo[14]);
             $grandTotal  = trim($orderinfo[9]);
             $productName = trim($orderinfo[12]);

            foreach ($products as $productId=>$product) {
                //$_product = Mage::getModel('catalog/product')->load($productId);        
                $rowTotal = $prodPrice * $product['qty'];
                $orderItem = Mage::getModel('sales/order_item')
                    ->setStoreId($storeId)
                    ->setQuoteItemId(0)
                    ->setQuoteParentItemId(NULL)
                    ->setProductId($productId)
                    //->setProductType($_product->getTypeId())
                    ->setQtyBackordered(NULL)
                    ->setTotalQtyOrdered($product['rqty'])
                    ->setQtyOrdered($product['qty'])
                    ->setName($productName)
                    ->setSku($sku)
                    ->setPrice($prodPrice)
                    ->setBasePrice($prodPrice)
                    ->setOriginalPrice($prodPrice)
                    ->setRowTotal($rowTotal)
                    ->setBaseRowTotal($rowTotal);
               
                $subTotal += $rowTotal;
                $order->addItem($orderItem);
            }
            }
            $order->setSubtotal($subTotal)
                ->setBaseSubtotal($subTotal)
                ->setGrandTotal($subTotal)
                ->setBaseGrandTotal($subTotal)
                ->setCreatedAt(Varien_Date::formatDate($orderinfo[1], false));

            $transaction->addObject($order);
            $transaction->addCommitCallback(array($order, 'place'));
            $transaction->addCommitCallback(array($order, 'save'));
            $transaction->save();
            Mage::getSingleton('adminhtml/session')->addSuccess('Order was successfully Created');
            $this->_redirect('*/*/import');
            }
         catch(Exception $e){
            Mage::log(sprintf('Order saving error: %s', $e->getMessage()), Zend_Log::ERR);
            echo('order unsuccessfull--------------------' . $e->getMessage().' for order number '. trim($orderinfo[0]));
            echo "\n ";
            echo "-------------------------------------------------------------";
            echo "\n ";
            echo "exiting as error came-------------------";
            exit();
           }
                   
    }
      function getProductBySku($sku) {

        $productId = Mage::getModel('catalog/product')->getIdBySku($sku);
        if ($productId != null) {
            return $productId;
        } else {
            echo "\n\n  no  productId found for Sku  " . $sku;
            return null;
        }
    }


    public function customerImportAction(){
        

        try {
            $uploader = new Varien_File_Uploader('import_customer');
            $uploader->setAllowedExtensions(array('csv'));
            $path = Mage::app()->getConfig()->getTempVarDir() . '/import/';
            $uploader->save($path);

            //If file is uploaded
            if ($uploadFile = $uploader->getUploadedFileName()) {
                //load file
                $filePath = $path . $uploadFile;
                $lines = file($filePath);
                $csv = new Varien_File_Csv();
                // Mage::log($filepath);
                $customerDatas = $csv->getData($filePath);
                /*foreach ($data as $index => $orderinfo) {
                    $i = 0;
                    if ($index == 0) {
                    $header = $data[$index];
                    continue;
                    }

                    $tmp[$data[$index][0]][] = $data[$index];
                }*/
                

                foreach ($customerDatas as $vno => $customerData) {
                    if($vno != 0){
                        $returnData = $this->createCustomer($customerData);
                        //echo $vno.'</br>'; 
                        //echo $returnData .' </br>';
                    }
                }

                echo 'All customer have been successfully uploaded';
                //$this->_redirect('*/*/import');

            }

            else
            throw new Exception('Unable to load file');

        } catch (Exception $ex) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('An error occured : %s', $ex->getMessage()));
        }
    }

    public function createCustomer($customerData){
        
        $provinceModel = Mage::getModel('directory/region');
        $customerModel = Mage::getModel("customer/customer");
        $addressModel  = Mage::getModel('customer/address');

        $fname          = '';
        $lname          = '';
        $email          = '';
        $websiteId      = '1';
        $store          = '0';
        $fname          = $customerData[0];
        $lname          = $customerData[1];
        $email          = $customerData[2];
        $company        = $customerData[3];
        $stree1         = $customerData[4];
        $stree2         = $customerData[5];
        $city           = $customerData[6];
        $provinde_code  = $customerData[8];
        $zip            = $customerData[11];
        $phone          = $customerData[12];
        $state_id       = '';
        $customerId     = '';
        $password       = '123456';
        $mainCustomerId = '';

        if($provinde_code != ''){
            $region = $provinceModel->loadByCode($provinde_code, 'IN');
            $state_id = $region->getId();
        }

        // For billing and shipping address.
        $_custom_address = array (
            'firstname'  => $fname,
            'lastname'   => $lname,
            'street'     => array (
                '0'      => $stree1 . ' ' . $stree2
            ),
            'city'       => $city,
            'company'    => $company,
            'postcode'   => $zip,
            'region_id'  => $state_id,
            'country_id' => 'IN',
            'telephone'  => $phone,
        );


        $customerModel->setWebsiteId($websiteId)
            //->setStore($store)
            ->setFirstname($fname)
            ->setLastname($lname)
            ->setEmail($email)
            ->setPassword($password);

        try{
            $customerModel->save();

            $customerId = $customerModel->loadByEmail($email)->getId();
            
            if($customerId != ''){
                $addressModel->setData($_custom_address)
                    ->setCustomerId($customerId)
                    ->setIsDefaultBilling('1')
                    ->setIsDefaultShipping('1')
                    ->setSaveInAddressBook('1');
                $addressModel->save();
            }

        }
        catch (Exception $e) {
            Zend_Debug::dump($e->getMessage());
        }
    }

}