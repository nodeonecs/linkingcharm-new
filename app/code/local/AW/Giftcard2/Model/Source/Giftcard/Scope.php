<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_Model_Source_Giftcard_Scope
{
    const SINGLE_WEBSITE                    = 1;
    const WEBSITES_WITH_THE_SAME_CURRENCY   = 2;

    const SINGLE_WEBSITE_LABEL                  = 'A single website';
    const WEBSITES_WITH_THE_SAME_CURRENCY_LABEL = 'All websites with the same currency';

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $helper = Mage::helper('aw_giftcard2');
        return array(
            array(
                'value' => self::SINGLE_WEBSITE,
                'label' => $helper->__(self::SINGLE_WEBSITE_LABEL)
            ),
            array(
                'value' => self::WEBSITES_WITH_THE_SAME_CURRENCY,
                'label' => $helper->__(self::WEBSITES_WITH_THE_SAME_CURRENCY_LABEL)
            ),
        );
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        $helper = Mage::helper('aw_giftcard2');
        return array(
            self::SINGLE_WEBSITE                    => $helper->__(self::SINGLE_WEBSITE_LABEL),
            self::WEBSITES_WITH_THE_SAME_CURRENCY   => $helper->__(self::WEBSITES_WITH_THE_SAME_CURRENCY_LABEL),
        );
    }
}