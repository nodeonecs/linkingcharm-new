<?php
class Iksula_Autotagging_Adminhtml_IndexController extends Mage_Adminhtml_Controller_Action{

  public function IndexAction() {
    $this->loadLayout();
    $this->renderLayout();
  }
   public function autotaggingAction() {
        $collection = Mage::getResourceModel('catalog/product_collection')
        	->addAttributeToSelect(array("category_ids","special_price","price"))
        	->addAttributeToFilter('type_id', 'configurable')->load();

        $result = array();
        $result['Status'] = 0;
        $result['Message'] = "There is an issue while autotagging";
        ini_set('max_execution_time', 60000);

        $catName =  Mage::getStoreConfig('autotagging_section/autotagging/category_name');
		$category = Mage::getResourceModel('catalog/category_collection')->addFieldToFilter('name', $catName);
		$catId = "";

		if($category) {
			$catId = $category->getFirstItem()->getEntityId();
		}
		if(!empty($catId)) {
			try {
				foreach($collection as $product) {

					if($product->getId()) {
						
						$categories = array();

						$specialPrice = $product->getSpecialPrice();
						$mrpPrice = $product->getPrice();						

						if($product->getCategoryIds()) {
		    				$categories = $product->getCategoryIds();
						}

						if(  !empty($specialPrice) && ($specialPrice < $mrpPrice) ) {
							if(!in_array($catId,$categories)) {
								$categories[] = $catId;
								$product->setCategoryIds($categories);
								$product->save();
							}

						} else {
							$key = "";
							$key = array_search($catId,$categories);
							if(isset($key) && $key >= 0) {
								unset($categories[$key]);
								$product->setCategoryIds($categories);
								$product->save();
							}
						}
					}
				}
				$result['Status'] = 1;
				$result['Message'] = "Autotagging is done successfully";
			} catch(Exception $e) {
				$result['Status'] = 0;
				$result['Message'] = $e->getMessage();
				
			}
		} else {
			$result['Message'] = "Specified category is not available to tag products";
		}

		Mage::app()->getResponse()->setBody(json_encode($result));
		return;
    }
}