<?php
/**
* Brainvire Infotech Pvt. Ltd
  * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */


class Brainvire_Points_Helper_Config extends Mage_Core_Helper_Abstract {
   
    const FIRST_ORDER_ONLY = 1;

    
    const EACH_ORDER = 2;

    
    const POINTS_GENERAL_ENABLE_YESNO = 'points/general/enable';

    
    const POINT_UNIT_NAME = 'points/general/point_unit_name';

    
    const POINTS_EXPIRATION_DAYS = 'points/general/points_expiration_days';

    
    const ENABLE_REWARDS_HISTORY_YESNO = 'points/general/enable_rewards_history';

    
    const APPLY_EARN_RATE = 'points/general/apply_earn_rate';

    
    const MINIMUM_POINTS_AMOUNT_TO_REDEEM = 'points/general/minimum_points_amount_for_spend';

    
    const MAXIMUM_POINTS_PER_CUSTOMER = 'points/general/maximum_points_per_customer';

    
    const WHAT_IS_IT_PAGE_ID = 'points/general/info_page';

    
   
    const PAYING_AMOUNT_PERCENT_LIMIT = 'points/general/paying_amount_percent_limit';


    
    const POINTS_EARNING_FOR_REGISTRATION = 'points/earning_points/for_registration';

    
    const POINTS_EARNING_FOR_NEWSLETTER_SINGUP = 'points/earning_points/for_newsletter_signup';

    const POINTS_EARNING_CONSIDER_NEWSLETTER_SIGNUP_BY_ADMIN = 'points/earning_points/consider_newsletter_signup_by_admin';

   
    const POINTS_EARNING_FOR_REVIEWING_PRODUCT = 'points/earning_points/for_reviewing_product';
    
    
    const POINTS_EARNING_FOR_VIDEO_TESTIMONIAL = 'points/earning_points/for_video_testimonial';

    
    const POINTS_EARNING_LIMIT_FOR_REVIEWING_PRODUCT = 'points/earning_points/reviewing_product_points_limit';
    
    
    const POINTS_EARNING_LIMIT_FOR_VIDEO_TESTIMONIAL = 'points/earning_points/for_video_testimonial_limit';

    
    const POINTS_EARNING_RESTRICTION_YESNO = 'points/earning_points/restriction';


    
    const POINTS_EARNING_FOR_TAGGING_PRODUCT = 'points/earning_points/for_tagging_product';

    const POINTS_EARNING_LIMIT_FOR_TAGGING_PRODUCT = 'points/earning_points/tagging_product_points_limit';

    const POINTS_EARNING_FOR_PARTICIPATING_IN_POLL = 'points/earning_points/for_participating_in_poll';

    
    const POINTS_EARNING_LIMIT_FOR_PARTICIPATING_IN_POLL = 'points/earning_points/participating_in_poll_points_limit';

    
    const REFERRAL_SYSTEM_YESNO = 'points/referal_system_configuration/enablerefsyst';

    
    const PRICE_OF_INVITATION = 'points/referal_system_configuration/priceofinvitation';

    
    const PRICE_OF_INVITATION_DAY_LIMIT = 'points/referal_system_configuration/price_of_invitation_limit';

    
    const POINTS_FOR_ORDER = 'points/referal_system_configuration/pointsfororder';

    
    const POINTS_FOR_ORDER_FIXED = 'points/referal_system_configuration/pointsfororderfixed';

    
    const POINTS_FOR_ORDER_PERCENT = 'points/referal_system_configuration/points_for_order_percent';

    
    const ENABLE_NOTIFICATIONS_YESNO = 'points/notifications/enable';

   
    const NOTIFICATIONS_SENDER = 'points/notifications/identity';

    
    const NOTIFICATIONS_BALANCE_UPDATE_TEMPLATE = 'points/notifications/balance_update_template';

    
    const NOTIFICATIONS_POINTS_EXPIRE_TEMPLATE = 'points/notifications/points_expire_template';

    
    const NOTIFICATIONS_INVITATION_TEMPLATE = 'points/notifications/template';

   
    const NOTIFICATIONS_SUBSCRIBE_BY_DEFAULT_YESNO = 'points/notifications/subscribe_by_default';

   
    const NOTIFICATIONS_POINT_BEFORE_EXPIRE_EMAIL_SENT = 'points/notifications/point_before_expire_email_sent';
	
	
	
    const POINT_AFTER_FACEBOOK_SHARE = 'points/facebook_reward/fb_points_share';
	
	const POINT_AFTER_TWITTER_SHARE = 'points/facebook_reward/twitter_points_share'; 
	
    const POINT_AFTER_FACEBOOK_LIKE = 'points/facebook_reward/fb_points_like'; 
	
	
    const FACEBOOK_APP_ID = 'points/facebook_reward/fb_app_id';
	
	
	const POINT_ON_BIRTHDAY = 'points/customer_birthday/customer_birthday';
	
 
    

    public function isPointsEnabled($storeId=null) {
        if ($storeId) {
            return (bool) (int) Mage::getStoreConfig(self::POINTS_GENERAL_ENABLE_YESNO, $storeId);
        } else {
            return (bool) (int) Mage::getStoreConfig(self::POINTS_GENERAL_ENABLE_YESNO);
        }
    }

    public function getPointUnitName($storeId=null) {
        if ($storeId) {
            return Mage::getStoreConfig(self::POINT_UNIT_NAME, $storeId);
        } else {
            return Mage::getStoreConfig(self::POINT_UNIT_NAME);
        }
    }

    public function getPointsExpirationDays($storeId=null) {
        if ($storeId) {
            return (int) Mage::getStoreConfig(self::POINTS_EXPIRATION_DAYS, $storeId);
        } else {
            return (int) Mage::getStoreConfig(self::POINTS_EXPIRATION_DAYS);
        }
    }

    public function getIsEnabledRewardsHistory($storeId=null) {
        if ($storeId) {
            return (bool) (int) Mage::getStoreConfig(self::ENABLE_REWARDS_HISTORY_YESNO, $storeId);
        } else {
            return (bool) (int) Mage::getStoreConfig(self::ENABLE_REWARDS_HISTORY_YESNO);
        }
    }

    public function getIsApplyEarnRates($storeId=null) {
        if ($storeId) {
            return (bool) (int) Mage::getStoreConfig(self::APPLY_EARN_RATE, $storeId);
        } else {
            return (bool) (int) Mage::getStoreConfig(self::APPLY_EARN_RATE);
        }
    }

    public function getMinimumPointsToRedeem($storeId=null) {
        if ($storeId) {
            return (int) Mage::getStoreConfig(self::MINIMUM_POINTS_AMOUNT_TO_REDEEM, $storeId);
        } else {
            return (int) Mage::getStoreConfig(self::MINIMUM_POINTS_AMOUNT_TO_REDEEM);
        }
    }

    public function getMaximumPointsPerCustomer($storeId=null) {
        if ($storeId) {
            return (int) Mage::getStoreConfig(self::MAXIMUM_POINTS_PER_CUSTOMER, $storeId);
        } else {
            return (int) Mage::getStoreConfig(self::MAXIMUM_POINTS_PER_CUSTOMER);
        }
    }

    public function getInfoPageId($storeId=null) {
        if ($storeId) {
            return Mage::getStoreConfig(self::WHAT_IS_IT_PAGE_ID, $storeId);
        } else {
            return Mage::getStoreConfig(self::WHAT_IS_IT_PAGE_ID);
        }
    }
    
    public function getPayingAmountPercentLimit($storeId=null) {
        if ($storeId) {
            return Mage::getStoreConfig(self::PAYING_AMOUNT_PERCENT_LIMIT, $storeId);
        } else {
            return Mage::getStoreConfig(self::PAYING_AMOUNT_PERCENT_LIMIT);
        }
    }

    


    public function getPointsForRegistration($storeId=null) {
        if ($storeId) {
            return (int) Mage::getStoreConfig(self::POINTS_EARNING_FOR_REGISTRATION, $storeId);
        } else {
            return (int) Mage::getStoreConfig(self::POINTS_EARNING_FOR_REGISTRATION);
        }
    }
	
	
	
	public function getPointsForFacebookShare($storeId=null) {
        if ($storeId) {
            return (int) Mage::getStoreConfig(self::POINT_AFTER_FACEBOOK_SHARE, $storeId);
        } else {
            return (int) Mage::getStoreConfig(self::POINT_AFTER_FACEBOOK_SHARE);
        }
    }
	
	
	public function getPointsForFacebookLike($storeId=null) {
        if ($storeId) {
            return (int) Mage::getStoreConfig(self::POINT_AFTER_FACEBOOK_LIKE, $storeId);
        } else {
            return (int) Mage::getStoreConfig(self::POINT_AFTER_FACEBOOK_LIKE);
        }
    }
	
	public function getPointsForTwitterShare($storeId=null) {
        if ($storeId) {
            return (int) Mage::getStoreConfig(self::POINT_AFTER_TWITTER_SHARE, $storeId);
        } else {
            return (int) Mage::getStoreConfig(self::POINT_AFTER_TWITTER_SHARE);
        }
    }
	
	public function getPointsOnBirthday($storeId=null) {
        if ($storeId) {
            return (int) Mage::getStoreConfig(self::POINT_ON_BIRTHDAY, $storeId);
        } else {
            return (int) Mage::getStoreConfig(self::POINT_ON_BIRTHDAY);
        }
    }
	
	
	
	
	
	
	
    public function getPointsForNewsletterSingup($storeId=null) {
        if ($storeId) {
            return (int) Mage::getStoreConfig(self::POINTS_EARNING_FOR_NEWSLETTER_SINGUP, $storeId);
        } else {
            return (int) Mage::getStoreConfig(self::POINTS_EARNING_FOR_NEWSLETTER_SINGUP);
        }
    }

    public function isConsiderNewsletterSignupByAdmin($storeId=null) {
        if ($storeId) {
            return (int) Mage::getStoreConfig(self::POINTS_EARNING_CONSIDER_NEWSLETTER_SIGNUP_BY_ADMIN, $storeId);
        } else {
            return (int) Mage::getStoreConfig(self::POINTS_EARNING_CONSIDER_NEWSLETTER_SIGNUP_BY_ADMIN);
        }
    }

    public function getPointsForReviewingProduct($storeId=null) {
        if ($storeId) {
            return (int) Mage::getStoreConfig(self::POINTS_EARNING_FOR_REVIEWING_PRODUCT, $storeId);
        } else {
            return (int) Mage::getStoreConfig(self::POINTS_EARNING_FOR_REVIEWING_PRODUCT);
        }
    }
    
    public function getPointsForVideoTestimonial($storeId=null) {
        if ($storeId) {
            return (int) Mage::getStoreConfig(self::POINTS_EARNING_FOR_VIDEO_TESTIMONIAL, $storeId);
        } else {
            return (int) Mage::getStoreConfig(self::POINTS_EARNING_FOR_VIDEO_TESTIMONIAL);
        }
    }

    public function getPointsLimitForReviewingProduct($storeId=null) {
        if ($storeId) {
            return (int) Mage::getStoreConfig(self::POINTS_EARNING_LIMIT_FOR_REVIEWING_PRODUCT, $storeId);
        } else {
            return (int) Mage::getStoreConfig(self::POINTS_EARNING_LIMIT_FOR_REVIEWING_PRODUCT);
        }
    }
    
    public function getPointsLimitForVideoTestimonial($storeId=null) {
        if ($storeId) {
            return (int) Mage::getStoreConfig(self::POINTS_EARNING_LIMIT_FOR_VIDEO_TESTIMONIAL, $storeId);
        } else {
            return (int) Mage::getStoreConfig(self::POINTS_EARNING_LIMIT_FOR_VIDEO_TESTIMONIAL);
        }
    }

    public function isForBuyersOnly($storeId=null) {
        if ($storeId) {
            return (bool) (int) Mage::getStoreConfig(self::POINTS_EARNING_RESTRICTION_YESNO, $storeId);
        } else {
            return (bool) (int) Mage::getStoreConfig(self::POINTS_EARNING_RESTRICTION_YESNO);
        }
    }

    public function getPointsForTaggingProduct($storeId=null) {
        if ($storeId) {
            return (int) Mage::getStoreConfig(self::POINTS_EARNING_FOR_TAGGING_PRODUCT, $storeId);
        } else {
            return (int) Mage::getStoreConfig(self::POINTS_EARNING_FOR_TAGGING_PRODUCT);
        }
    }

    public function getPointsLimitForTaggingProduct($storeId=null) {
        if ($storeId) {
            return (int) Mage::getStoreConfig(self::POINTS_EARNING_LIMIT_FOR_TAGGING_PRODUCT, $storeId);
        } else {
            return (int) Mage::getStoreConfig(self::POINTS_EARNING_LIMIT_FOR_TAGGING_PRODUCT);
        }
    }

    public function getPointsForParticipatingInPoll($storeId=null) {
        if ($storeId) {
            return (int) Mage::getStoreConfig(self::POINTS_EARNING_FOR_PARTICIPATING_IN_POLL, $storeId);
        } else {
            return (int) Mage::getStoreConfig(self::POINTS_EARNING_FOR_PARTICIPATING_IN_POLL);
        }
    }

    public function getPointsLimitForParticipatingInPoll($storeId=null) {
        if ($storeId) {
            return (int) Mage::getStoreConfig(self::POINTS_EARNING_LIMIT_FOR_PARTICIPATING_IN_POLL, $storeId);
        } else {
            return (int) Mage::getStoreConfig(self::POINTS_EARNING_LIMIT_FOR_PARTICIPATING_IN_POLL);
        }
    }

   

    public function isReferalSystemEnabled($storeId=null) {
        if ($storeId) {
            return Mage::getStoreConfigFlag(self::REFERRAL_SYSTEM_YESNO, $storeId);
        } else {
            return Mage::getStoreConfigFlag(self::REFERRAL_SYSTEM_YESNO);
        }
    }

    public function getInvitationToRegistrationConversion($storeId=null) {
        if ($storeId) {
            return Mage::getStoreConfig(self::PRICE_OF_INVITATION, $storeId);
        } else {
            return Mage::getStoreConfig(self::PRICE_OF_INVITATION);
        }
    }

    public function getLimitPointsOfInvitationForDay($storeId=null) {
        if ($storeId) {
            return Mage::getStoreConfig(self::PRICE_OF_INVITATION_DAY_LIMIT, $storeId);
        } else {
            return Mage::getStoreConfig(self::PRICE_OF_INVITATION_DAY_LIMIT);
        }
    }

    public function getPointsForOrder($storeId=null) {
        if ($storeId) {
            return Mage::getStoreConfig(self::POINTS_FOR_ORDER, $storeId);
        } else {
            return Mage::getStoreConfig(self::POINTS_FOR_ORDER);
        }
    }

    public function getFixedPointsForOrder($storeId=null) {
        if ($storeId) {
            return Mage::getStoreConfig(self::POINTS_FOR_ORDER_FIXED, $storeId);
        } else {
            return Mage::getStoreConfig(self::POINTS_FOR_ORDER_FIXED);
        }
    }

    public function getPercentPointsForOrder($storeId=null) {
        if ($storeId) {
            return Mage::getStoreConfig(self::POINTS_FOR_ORDER_PERCENT, $storeId);
        } else {
            return Mage::getStoreConfig(self::POINTS_FOR_ORDER_PERCENT);
        }
    }

    

    public function getIsEnabledNotifications($storeId=null) {
        if ($storeId) {
            return (bool) (int) Mage::getStoreConfig(self::ENABLE_NOTIFICATIONS_YESNO, $storeId);
        } else {
            return (bool) (int) Mage::getStoreConfig(self::ENABLE_NOTIFICATIONS_YESNO);
        }
    }

    public function getNotificatioinSender($storeId=null) {
        if ($storeId) {
            return Mage::getStoreConfig(self::NOTIFICATIONS_SENDER, $storeId);
        } else {
            return Mage::getStoreConfig(self::NOTIFICATIONS_SENDER);
        }
    }

    public function getBalanceUpdateTemplate($storeId=null) {
        if ($storeId) {
            return Mage::getStoreConfig(self::NOTIFICATIONS_BALANCE_UPDATE_TEMPLATE, $storeId);
        } else {
            return Mage::getStoreConfig(self::NOTIFICATIONS_BALANCE_UPDATE_TEMPLATE);
        }
    }

    public function getPointsExpireTemplate($storeId=null) {
        if ($storeId) {
            return Mage::getStoreConfig(self::NOTIFICATIONS_POINTS_EXPIRE_TEMPLATE, $storeId);
        } else {
            return Mage::getStoreConfig(self::NOTIFICATIONS_POINTS_EXPIRE_TEMPLATE);
        }
    }

    public function getInvitationTemplate($storeId=null) {
        if ($storeId) {
            return Mage::getStoreConfig(self::NOTIFICATIONS_INVITATION_TEMPLATE, $storeId);
        } else {
            return Mage::getStoreConfig(self::NOTIFICATIONS_INVITATION_TEMPLATE);
        }
    }

    public function getIsSubscribedByDefault($storeId=null) {
        if ($storeId) {
            return (bool) (int) Mage::getStoreConfig(self::NOTIFICATIONS_SUBSCRIBE_BY_DEFAULT_YESNO, $storeId);
        } else {
            return (bool) (int) Mage::getStoreConfig(self::NOTIFICATIONS_SUBSCRIBE_BY_DEFAULT_YESNO);
        }
    }

    public function getDaysBeforePointExpiredToSendEmail($storeId=null) {
        if ($storeId) {
            return Mage::getStoreConfig(self::NOTIFICATIONS_POINT_BEFORE_EXPIRE_EMAIL_SENT, $storeId);
        } else {
            return Mage::getStoreConfig(self::NOTIFICATIONS_POINT_BEFORE_EXPIRE_EMAIL_SENT);
        }
    }

}

?>
