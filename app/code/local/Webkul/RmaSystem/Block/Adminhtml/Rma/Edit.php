<?php

    class Webkul_RmaSystem_Block_Adminhtml_Rma_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

        public function __construct() {
            parent::__construct();
            $this->_objectId    = "id";
            $this->_blockGroup  = "rmasystem";
            $this->_controller  = "adminhtml_rma";
            $this->_updateButton("save", "label", $this->__("Update Status"));
            $this->_removeButton("reset");
            $this->_removeButton("delete");
            $this->_addButton("saveandcontinue", array(
                "label"     => $this->__("Update And Continue Edit"),
                "onclick"   => "saveAndContinueEdit()",
                "class"     => "save",
            ), -100);        
            $this->_formScripts[] = "function saveAndContinueEdit(){
                                        editForm.submit($('edit_form').action+'back/edit/');
                                    }";
        }

        public function getHeaderText() {
            return $this->__("View RMA id ").$this->htmlEscape(Mage::registry("rma_data")->getIncrementId()."-".Mage::registry("rma_data")->getId());
        }

    }