<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2015 Amasty (https://www.amasty.com)
 * @package Amasty_Rma
 */
$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$this->startSetup();

$this->run("
	ALTER TABLE `{$this->getTable('amrma/status')}`
    ADD COLUMN (`paired_with_order_status` VARCHAR(255) DEFAULT NULL,`status_for` VARCHAR(255) DEFAULT NULL);
    
    ALTER TABLE `{$this->getTable('amrma/request')}`
    ADD COLUMN `status_id_for_payment` int(10) DEFAULT NULL;

    ALTER TABLE `{$this->getTable('amrma/comment')}`
    ADD COLUMN (`status_id_comment` VARCHAR(250) DEFAULT NULL, `status_id_for_payment_comment` VARCHAR(250) DEFAULT NULL);
");

$this->endSetup();