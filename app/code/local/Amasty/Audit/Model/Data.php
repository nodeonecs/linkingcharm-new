<?php
/**
* @author Amasty Team
* @copyright Copyright (c) 2013 Amasty (http://www.amasty.com)
* @package Amasty_Audit 
*/
class Amasty_Audit_Model_Data extends Mage_Core_Model_Abstract
{
    public function _construct()
    {    
        $this->_init('amaudit/data', 'entity_id');
    }

    public function getLoggedRows(){
        $read = Mage::getSingleton('core/resource')->getConnection('core_read');
        $aaldTable = $read->getTableName('amasty_audit_log_details');
        $aalTable = $read->getTableName('amasty_audit_log');

        $select = 'SELECT GROUP_CONCAT( DISTINCT aal.entity_id
            SEPARATOR ", " ) AS existings
            FROM ' . $aalTable . ' aal
            RIGHT JOIN ' . $aaldTable . ' aald ON aal.entity_id = aald.log_id';
        $result = $read->fetchRow($select);
        return $result['existings'];
    }
}