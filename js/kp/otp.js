var isverified;
jQuery("#otp-link").live('click',function(){
var mobile = jQuery("#otp_mobile").val();
var sendotpurl = jQuery("#sendotpurl").val();
  jQuery.ajax({
      url:sendotpurl,
      data:{mobile:mobile},
      dataType:'JSON',
      type:'POST',
      beforeSend:function(){},
      success:function(responses){
        if(responses.response){
          jQuery(".messages").html('<li class="success-msg"><ul><li>'+responses.message+'</li></ul></li>').fadeIn().delay(5000).fadeOut();
          jQuery("#otp_click_here").html("Retry?");
        }else{
          jQuery(".messages").html('<li class="error-msg"><ul><li>'+responses.message+'</li></ul></li>').fadeIn().delay(5000).fadeOut();
          jQuery("#otp_click_here").html("Retry?");
        }
      },
      error:function(e){
        jQuery(".messages").html('<li class="error-msg"><ul><li>Something went wrong, Please try again later.</li></ul></li>').fadeIn().delay(5000).fadeOut();
      }
    });

});

jQuery(document).ready(function(){

var verifymobileurl = jQuery("#verifymobileurl").val();
  jQuery("#otp_submit").live('click',function(e){
    e.preventDefault();
    var otp = jQuery("#confirm-otp").val();
      jQuery.ajax({
        url:verifymobileurl,
        data:{otp:otp},
        dataType:'JSON',
        type:'POST',
        beforeSend:function(){},
        success:function(responses){
          if(responses.response){
              isverified = true;
             jQuery(".messages").html('<li class="success-msg"><ul><li>'+responses.message+'</li></ul></li>').fadeIn().delay(5000).fadeOut();
            }else{
              isverified = false;
              jQuery(".messages").html('<li class="error-msg"><ul><li>'+responses.message+'</li></ul></li>').fadeIn().delay(5000).fadeOut();
            }
        },
        error:function(){
              isverified = false;
          jQuery(".messages").html('<li class="error-msg"><ul><li>Something went wrong, Please try again later.</li></ul></li>').fadeIn().delay(5000).fadeOut();
        }

      });

  });

});