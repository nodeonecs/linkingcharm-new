<?php
	
class Iksula_Storetype_Block_Adminhtml_Storetype_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
		public function __construct()
		{

				parent::__construct();
				$this->_objectId = "id";
				$this->_blockGroup = "storetype";
				$this->_controller = "adminhtml_storetype";
				$this->_updateButton("save", "label", Mage::helper("storetype")->__("Save Item"));
				$this->_updateButton("delete", "label", Mage::helper("storetype")->__("Delete Item"));

				$this->_addButton("saveandcontinue", array(
					"label"     => Mage::helper("storetype")->__("Save And Continue Edit"),
					"onclick"   => "saveAndContinueEdit()",
					"class"     => "save",
				), -100);



				$this->_formScripts[] = "

							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
						";
		}

		public function getHeaderText()
		{
				if( Mage::registry("storetype_data") && Mage::registry("storetype_data")->getId() ){

				    return Mage::helper("storetype")->__("Edit Item '%s'", $this->htmlEscape(Mage::registry("storetype_data")->getId()));

				} 
				else{

				     return Mage::helper("storetype")->__("Add Item");

				}
		}
}