<?php
class Iksula_Storetype_Block_Adminhtml_Storetype_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
		public function __construct()
		{
				parent::__construct();
				$this->setId("storetype_tabs");
				$this->setDestElementId("edit_form");
				$this->setTitle(Mage::helper("storetype")->__("Item Information"));
		}
		protected function _beforeToHtml()
		{
				$this->addTab("form_section", array(
				"label" => Mage::helper("storetype")->__("Item Information"),
				"title" => Mage::helper("storetype")->__("Item Information"),
				"content" => $this->getLayout()->createBlock("storetype/adminhtml_storetype_edit_tab_form")->toHtml(),
				));
				return parent::_beforeToHtml();
		}

}
