<?php
$installer = $this;

$installer->startSetup();

$installer->run("
ALTER TABLE amasty_amrolepermissions_rule
    ADD scope_websites_warehouse varchar(255) ;
");

$installer->endSetup();
