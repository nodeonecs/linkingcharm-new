<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2015 Amasty (https://www.amasty.com)
 * @package Amasty_Rolepermissions
 */

class Amasty_Rolepermissions_Block_Adminhtml_Tab_Scope extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    const MODE_NONE = 0;
    const MODE_SITE = 1;
    const MODE_VIEW = 2;
    const MODE_WAREHOUSE = 3;
    const MODE_HUB = 4;

    public function getTabLabel()
    {
        return $this->__('Advanced: Scope');
    }


    public function getTabTitle()
    {
        return $this->getTabLabel();
    }

    public function canShowTab()
    {
        return true;
    }

    public function isHidden()
    {
        return false;
    }

    public function _beforeToHtml() {
        $this->_initForm();

        return parent::_beforeToHtml();
    }

    protected function _initForm()
    {
        $form = new Varien_Data_Form();

        $model = Mage::registry('current_rule');

        Mage::log(print_r($model->getData() , 1) , null , 'per.log');


        $fieldset = $form->addFieldset('access_scope_fieldset', array('legend'=>$this->__('Choose Access Scope')));

        $fieldset->addField('role_id', 'hidden',
            array(
                'name' => 'amrolepermissions[role_id]',
            )
        );

        $custom_role_fields = $fieldset->addField('custom_role', 'hidden',
            array(
                'name' => 'amrolepermissions[custom_role_val]',
            )
        );



       /* if ($model->getScopeHub())
            $model->setScopeMode(self::MODE_HUB);
        else if ($model->getScopeStoreviews())
            $model->setScopeMode(self::MODE_VIEW);
        else if ($model->getScopeWebsitesWarehouse())
            $model->setScopeMode(self::MODE_WAREHOUSE);
        else if($model->getScopeWebsites())
            $model->setScopeMode(self::MODE_SITE);
        else
            $model->setScopeMode(self::MODE_NONE);*/


            if ($model->getCustomRole() == 'HUB')
            $model->setScopeMode(self::MODE_HUB);
        else if ($model->getCustomRole() == 'STORE_VIEW')
            $model->setScopeMode(self::MODE_VIEW);
        else if ($model->getCustomRole() == 'WAREHOUSE')
            $model->setScopeMode(self::MODE_WAREHOUSE);
        else if($model->getCustomRole() == 'W_HO')
            $model->setScopeMode(self::MODE_SITE);
        else
            $model->setScopeMode(self::MODE_NONE);



        $iLoginuserid = Mage::getSingleton('admin/session')->getUser()->getUserId();
        //$iRole_id = Mage::getSingleton('amrolepermissions/rule')->getRoleId();

        $LoginUserRole = Mage::getModel('admin/user')->load($iLoginuserid)->getRole()->getRoleId();
        //var_dump($role_data);
        $iRole_Collectionsid = Mage::getSingleton('amrolepermissions/rule')
                                                ->getCollection()
                                                ->addFieldToFilter('role_id' , array('eq' => $LoginUserRole));



                 foreach($iRole_Collectionsid as $Role){

                        $RoleCustom = $Role->getCustomRole();
                 }




        if($RoleCustom){

            if($RoleCustom == 'S_A'){

                        $mode = $fieldset->addField('scope_mode', 'select',
                array(
                    'label' => $this->__('Limit Access To'),
                    'id'    => 'scope_mode',
                    'values'=> array(
                        self::MODE_NONE => $this->__('Super Admin'),
                        self::MODE_WAREHOUSE => $this->__('Warehouse'),
                        self::MODE_HUB => $this->__('HUB'),
                        self::MODE_SITE => $this->__('Website Head Office'),
                        self::MODE_VIEW => $this->__('Stores'),
                    ),
                    'onchange' => 'changeCustomRole(this)',
                )
            );

            }elseif($RoleCustom == 'W_HO'){

                $mode = $fieldset->addField('scope_mode', 'select',
                array(
                    'label' => $this->__('Limit Access To'),
                    'id'    => 'scope_mode',
                    'values'=> array(
                        self::MODE_VIEW => $this->__('Stores'),
                    ),
                    'onchange' => 'changeCustomRole(this)',
                )
            );


            }elseif($RoleCustom == 'WAREHOUSE'){

                $mode = $fieldset->addField('scope_mode', 'select',
                array(
                    'label' => $this->__('Limit Access To'),
                    'id'    => 'scope_mode',
                    'values'=> array(
                        self::MODE_VIEW => $this->__('Stores'),
                    ),
                    'onchange' => 'changeCustomRole(this)',
                )
            );


            }elseif($RoleCustom == 'HUB'){

                $mode = $fieldset->addField('scope_mode', 'select',
                array(
                    'label' => $this->__('Limit Access To'),
                    'id'    => 'scope_mode',
                    'values'=> array(
                        self::MODE_HUB => $this->__('Stores'),
                    ),
                    'onchange' => 'changeCustomRole(this)',
                )

            );
            }else{

                $mode = $fieldset->addField('scope_mode', 'select',
                array(
                    'label' => $this->__('Limit Access To'),
                    'id'    => 'scope_mode',
                    'values'=> array(
                        self::MODE_NONE => $this->__('Super Admin'),
                            self::MODE_WAREHOUSE => $this->__('Warehouse'),
                            self::MODE_SITE => $this->__('Website Head Office'),
                            self::MODE_VIEW => $this->__('Stores'),
                        ),
                    'onchange' => 'changeCustomRole(this)',
                    )


                );
            }
        }else{

            $mode = $fieldset->addField('scope_mode', 'select',
                array(
                    'label' => $this->__('Limit Access To'),
                    'id'    => 'scope_mode',
                    'values'=> array(
                        self::MODE_NONE => $this->__('Super Admin'),
                            self::MODE_WAREHOUSE => $this->__('Warehouse'),
                            self::MODE_SITE => $this->__('Website Head Office'),
                            self::MODE_VIEW => $this->__('Stores'),
                        ),
                    'onchange' => 'changeCustomRole(this)',
                    )


                );
        }

        /*$headofficeviews = $fieldset->addField('head_office', 'select', array(
            'name'      => 'amrolepermissions[head_office]',
            'label'     => $this->__('Head Office'),
            'title'     => $this->__('Head Office'),
            'values' => array(
                    array(
                        'value' => 0,
                        'label' => Mage::helper('amrolepermissions')->__('No'),
                    ),
                    array(
                        'value' => 1,
                        'label' => Mage::helper('amrolepermissions')->__('Yes'),
                    ),
                ),
        ));*/

$custom_role_fields->setAfterElementHtml("


    <script>

function changeCustomRole(selectElement){



    switch(selectElement.value) {
        case '0':
            var custom_role = 'S_A';
            break;
        case '1':
            var custom_role = 'W_HO';
            break;
        case '2':
            var custom_role = 'STORE_VIEW';
            break;
        case '3':
            var custom_role = 'WAREHOUSE';
            break;
        case '4':
            var custom_role = 'HUB';
            break;

    }

    document.getElementById('custom_role').value = custom_role;


}
</script>");



        $websites = array();
        //$websites ['-'] = 'Please select an option';
        foreach (Mage::getResourceModel('core/website_collection') as $id => $ws)
            $websites []= array('label' => $ws->getName(), 'value' => $ws->getWebsiteId());
            $websites_warehouse = $websites;
            //$websites_hub = $websites;

        $websites = $fieldset->addField('scope_websites', 'select',
            array(
                'name'  => 'amrolepermissions[scope_websites]',
                'label' => $this->__('Websites'),
                'title' => $this->__('Websites'),
                'values'=> $websites,
                //'class' => 'validate-select'
            )
        );

        $websites_warehouse = $fieldset->addField('scope_websites_warehouse', 'select',
            array(
                'name'  => 'amrolepermissions[scope_websites_warehouse]',
                'label' => $this->__('Websites'),
                'title' => $this->__('Websites'),
                'values'=> $websites_warehouse,
            )
        );



        $views = $fieldset->addField('scope_storeviews', 'multiselect', array(
            'name'      => 'amrolepermissions[scope_storeviews]',
            'label'     => $this->__('Store Views'),
            'title'     => $this->__('Store Views'),
            'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForFormCustom(false, false),
        ));


         $websites_hub = $fieldset->addField('scope_hub', 'multiselect',
            array(
                'name'  => 'amrolepermissions[scope_websites_hub]',
                'label' => $this->__('Websites'),
                'title' => $this->__('Websites'),
                'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForFormCustomHub(false, false),
            )
        );

         /*$fieldset->addType('extended_label','Amasty_Rolepermissions_Model_ExtendedLabel');
         $fieldset->addField('mycustom_element', 'extended_label', array(
            'label'         => 'My Custom Element Label',
            'name'          => 'mycustom_element',
            'required'      => false,
            'value'     => $this->getLastEventLabel($lastEvent),
            'bold'      =>  true,
            'label_style'   =>  'font-weight: bold;color:red;',
        ));*/





        $form->setValues($model->getData());
        $this->setForm($form);

        $this->setChild('form_after', $this->getLayout()->createBlock('adminhtml/widget_form_element_dependence')
                //->addFieldMap($headofficeviews->getHtmlId(), $views->getName())
                ->addFieldMap($mode->getHtmlId(), $mode->getName())
                //->addFieldMap($websites_warehouse->getHtmlId(), $websites_warehouse->getName())
                ->addFieldMap($websites->getHtmlId(), $websites->getName())
                ->addFieldMap($websites_warehouse->getHtmlId(), $websites_warehouse->getName())
                ->addFieldMap($views->getHtmlId(), $views->getName())
                ->addFieldMap($websites_hub->getHtmlId(), $websites_hub->getName())
                ->addFieldDependence(
                    $websites->getName(),
                    $mode->getName(),
                    self::MODE_SITE
                )
                ->addFieldDependence(
                    $websites_warehouse->getName(),
                    $mode->getName(),
                    self::MODE_WAREHOUSE
                )
                ->addFieldDependence(
                    $views->getName(),
                    $mode->getName(),
                    self::MODE_VIEW
                )
                ->addFieldDependence(
                    $websites_hub->getName(),
                    $mode->getName(),
                    self::MODE_HUB
                )
        );

        return parent::_prepareForm();
    }
}
