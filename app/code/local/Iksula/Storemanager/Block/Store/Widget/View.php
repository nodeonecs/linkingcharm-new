<?php
/**
 * Iksula_Storemanager extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Iksula
 * @package        Iksula_Storemanager
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Store widget block
 *
 * @category    Iksula
 * @package     Iksula_Storemanager
 * @author      Ultimate Module Creator
 */
class Iksula_Storemanager_Block_Store_Widget_View extends Mage_Core_Block_Template implements
    Mage_Widget_Block_Interface
{
    protected $_htmlTemplate = 'iksula_storemanager/store/widget/view.phtml';

    /**
     * Prepare a for widget
     *
     * @access protected
     * @return Iksula_Storemanager_Block_Store_Widget_View
     * @author Ultimate Module Creator
     */
    protected function _beforeToHtml()
    {
        parent::_beforeToHtml();
        $storeId = $this->getData('store_id');
        if ($storeId) {
            $store = Mage::getModel('iksula_storemanager/store')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->load($storeId);
            if ($store->getStatus()) {
                $this->setCurrentStore($store);
                $this->setTemplate($this->_htmlTemplate);
            }
        }
        return $this;
    }
}
