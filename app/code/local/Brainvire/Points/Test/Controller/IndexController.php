<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   BV
 * @package    BV_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */



class Brainvire_Points_Test_Controller_IndexController extends EcomDev_PHPUnit_Test_Case_Controller {

    
    public function infopageAction($testId, $pageId, $return) {
        $this->registerPointsConfigMockObject($pageId);
        $expected = $this->expected('id' . $testId);
        if (!$return)
            $this->registerCmsPageMockObject($return);
        $this->reset()->dispatch('points/index/infopage');
        $this->assertRequestRoute($expected->getRequestRoute());
        $this->assertResponseBodyContains($expected->getBodyContains());
    }

    
    protected function registerPointsConfigMockObject($pageId) {
        $stub = $this->getHelperMock('points/config', array('getInfoPageId'));
        $stub->expects($this->any())
                ->method('getInfoPageId')
                ->will($this->returnValue($pageId));
        $this->replaceByMock('helper', 'points/config', $stub);
    }

    
    protected function registerCmsPageMockObject($return) {
        $stub = $this->getHelperMock('cms/page', array('renderPage'));
        $stub->expects($this->any())
                ->method('renderPage')
                ->will($this->returnValue($return));
        $this->replaceByMock('helper', 'cms/page', $stub);
    }

}