<?php

class Iksula_Rewrites_Helper_Captcha_Data extends Mage_Captcha_Helper_Data
{
    /** @Created: Deepthi Reddy
        rewritten to use website base url instead of cloudfront url

    **/
    public function getImgUrl($website = null)
    {
        $websiteCode = Mage::app()->getWebsite($website)->getCode();
        $imageUrl = Mage::getBaseUrl().'/media/captcha' . '/' . $websiteCode."/";
        // appending https for secure url
        if (Mage::app()->getStore()->isCurrentlySecure()) {
    		$imageUrl = str_replace("http:","https:",$imageUrl);
		}
		return $imageUrl;
    }
}
