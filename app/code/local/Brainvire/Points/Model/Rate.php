<?php
/**
* Brainvire Infotech Pvt. Ltd
  * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */


class Brainvire_Points_Model_Rate extends Mage_Core_Model_Abstract {
    const POINTS_TO_CURRENCY = 1;
    const CURRENCY_TO_POINTS = 2;

    protected $_currentCustomer;
    protected $_currentWebsite;

    public function _construct() {
        parent::_construct();
        $this->_init('points/rate');
    }

    public function getWebsite() {
        return Mage::app()->getWebsite($this->getWebsiteId());
    }

    public function getCurrentCustomer() {
        if (!$this->_currentCustomer) {
            $this->_currentCustomer = Mage::getModel('customer/session')->getCustomer();
        }
        return $this->_currentCustomer;
    }

    public function setCurrentCustomer($customer) {
        $this->_currentCustomer = $customer;
        return $this;
    }

    public function getCurrentWebsite() {
        if (!$this->_currentWebsite) {
            $this->_currentWebsite = Mage::app()->getWebsite();
        }
        return $this->_currentWebsite;
    }

    public function setCurrentWebsite($website) {
        $this->_currentWebsite = $website;
        return $this;
    }

    
    public function loadByDirection($direction) {
        $this->getResource()->loadRateByCustomerWebsiteDirection($this, $this->getCurrentCustomer(), $this->getCurrentWebsite(), $direction);
        return $this;
    }

    
    public function exchange($amount) {
        if (!$this->getPoints() || !$this->getMoney()) {
            throw new Exception(Mage::helper('points')->__('Exchange rates are incorrect'));
        }

        $newAmount = 0;
        if ($this->getDirection() == self::POINTS_TO_CURRENCY) {
            $newAmount = round($amount * $this->getMoney() / $this->getPoints(), 2);
        } else {
            $newAmount = (int) ($amount * $this->getPoints() / $this->getMoney());
        }
        return $newAmount;
    }

    public function getRateText() {
        return Mage::helper('points')->getRateText($this->getDirection(), $this->getPoints(), $this->getMoney());
    }

}
