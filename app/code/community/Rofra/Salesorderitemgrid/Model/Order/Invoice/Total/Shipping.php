<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Sales
 * @copyright   Copyright (c) 2013 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Order invoice shipping total calculation model
 *
 * @category   Mage
 * @package    Mage_Sales
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Rofra_Salesorderitemgrid_Model_Order_Invoice_Total_Shipping extends Mage_Sales_Model_Order_Invoice_Total_Shipping
{
    public function collect(Mage_Sales_Model_Order_Invoice $invoice)
    {
        //$invoice = Mage::getModel('sales/order_invoice_item');
        $invoice->setShippingAmount(0);
        $invoice->setBaseShippingAmount(0);
        ///$ItemId = '1112';
        //$ItemId = Mage::app()->getRequest()->getParam('sub_order_id');
        $ItemId =  Mage::registry('invoice_sub-order_id');
        $ItemsObj = Mage::getModel('sales/order_item')->load($ItemId);
        /*$orderShippingAmount        = $invoice->getOrder()->getShippingAmount();
        $baseOrderShippingAmount    = $invoice->getOrder()->getBaseShippingAmount();
        $shippingInclTax            = $invoice->getOrder()->getShippingInclTax();
        $baseShippingInclTax        = $invoice->getOrder()->getBaseShippingInclTax();*/

        $orderShippingAmount        = $ItemsObj->getItemShippingAmount();
        $baseOrderShippingAmount    = $ItemsObj->getItemBaseShippingAmount();
        $shippingInclTax            = $ItemsObj->getItemShippingInclTax();
        $baseShippingInclTax        = $ItemsObj->getItemBaseShippingInclTax();

        if(floor($ItemsObj->getQtyInvoiced()) != 0){
            return $this;
        }
        if ($orderShippingAmount) {

            /**
             * Check shipping amount in previus invoices
             */
            /*foreach ($invoice->getOrder()->getInvoiceCollection() as $previusInvoice) {
                if ($previusInvoice->getShippingAmount() && !$previusInvoice->isCanceled()) {
                    return $this;
                }
            }*/
            $invoice->setShippingAmount($orderShippingAmount);
            $invoice->setBaseShippingAmount($baseOrderShippingAmount);
            $invoice->setShippingInclTax($shippingInclTax);
            $invoice->setBaseShippingInclTax($baseShippingInclTax);

            $invoice->setGrandTotal($invoice->getGrandTotal()+$orderShippingAmount);
            $invoice->setBaseGrandTotal($invoice->getBaseGrandTotal()+$baseOrderShippingAmount);
        }
        return $this;
    }
}
