<?php

Class Rofra_Salesorderitemgrid_Block_Orders_Transfertohub extends Mage_Core_Block_Template{


	public function getHubdetails(){

		 $orderitemsid = $this->getRequest()->getParam('itemid');
//		$storecode = Mage::getSingleton('sales/order_item')->load($orderitemsid)->getStoreCode(); // For manual Allocation
		 $storecode = Mage::getSingleton('sales/order_item')->load($orderitemsid)->getData('store_1');
		$StoreHubsId = Mage::getSingleton('iksula_storemanager/storehubrelation')->load($storecode , 'store_id')->getHubId();

		if($StoreHubsId == '0'){
			$HubName = "";
		}else{
			$HubName = Mage::getSingleton('core/store_group')->load($StoreHubsId)->getName();
		}



		return array('orderitemid' => $orderitemsid , 'hubname' => $HubName , 'hubid' => $StoreHubsId);
	}



}
