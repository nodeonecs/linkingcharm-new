<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   Brainvire
 * @package    Brainvire_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * @license    
 */


class Brainvire_Points_Block_Customer_Reward_Notifications extends Mage_Core_Block_Template {
    const SUBSCRIPTION_ACTION = "subscribe";

    public function getIsSubscribed() {
        $summary = Mage::getModel('points/summary')
                ->loadByCustomer(
                Mage::getSingleton('customer/session')
                ->getCustomer()
        );
        return (bool) (int) $summary->getBalanceUpdateNotification();
    }

    public function getIsSubscribedToExpireNotification() {
        $summary = Mage::getModel('points/summary')
                ->loadByCustomer(
                Mage::getSingleton('customer/session')
                ->getCustomer()
        );
        return (bool) $summary->getData('points_expiration_notification');
    }

    public function getAction() {
        return self::SUBSCRIPTION_ACTION;
    }

    public function isEnabled() {
        return Mage::helper('points/config')->getIsEnabledNotifications();
    }

    protected function _toHtml() {

        $magentoVersionTag = Brainvire_Points_Helper_Data::MAGENTO_VERSION_14;

        if (Mage::helper('points')->magentoLess14())
            $magentoVersionTag = Brainvire_Points_Helper_Data::MAGENTO_VERSION_13;

        $this->setTemplate("brainvire_points/customer/" . $magentoVersionTag . "/reward/notifications.phtml");

        $html = parent::_toHtml();
        return $html;
    }

}

?>
