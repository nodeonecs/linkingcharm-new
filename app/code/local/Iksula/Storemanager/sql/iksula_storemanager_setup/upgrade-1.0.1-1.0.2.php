<?php
$installer = $this;

$installer->startSetup();

$installer->run("
ALTER TABLE {$this->getTable('core/store_group')}
    ADD store_definition varchar(255) not null ;
");

$installer->endSetup();
