<?php
class Iksula_Smsapp_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function sensSms($msg, $mob, $storeId = ''){

		$msg = rawurlencode($msg);
		//$mob = "918097175958";
		if($storeId == '')
		{
			$storeId = Mage::app()->getStore()->getStoreId();
		}
		$user = Mage::getStoreConfig('smsapp/sms_routesm/smsuser', Mage::app()->getStore()->getStoreId());
		$pass = Mage::getStoreConfig('smsapp/sms_routesm/smspass', Mage::app()->getStore()->getStoreId());
		$code = Mage::getStoreConfig('smsapp/sms_routesm/smsbrand', $storeId);
		$base = Mage::getStoreConfig('smsapp/sms_routesm/smsapiurl', $storeId);
		//$user = "iksulat";
		//$pass = "iks84ult";
		//sms6.routesms.com:8080/bulksms/bulksms?username=iksulat&password=iks84ult&type=0&dlr=1&destination=919820256213&source=UPDATE&message=Dear%20Abhishek
		//$base = "http://sms6.routesms.com:8080/bulksms/bulksms";
		// $urlMsg = $base . '?username=' . $user . '&password=' .  $pass . '&senderid=' . $code . '&route=1&number='. $mob .'&message=' . $msg;
		$urlMsg =  $base . "?method=SendMessage&send_to=". $mob ."&msg=". $msg ."&msg_type=TEXT&userid=". $user ."&auth_scheme=plain&password=". $pass ."&v=1.1&format=text";

		/*echo $urlMsg;
		exit;*/
		$curl = curl_init($urlMsg);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	    curl_exec ($curl);
	    curl_close ($curl);
	}

	public function getTemplateAndSendSms($payType,$mob,$cusName ='',$incrementId ='',$brand ='',$storeId = ''){

		$smsTemp = '';
		switch($payType){
			case 'cod_pay':
			$smsTemp = Mage::getStoreConfig('smsapp/sms_routesm/order_success_cod', Mage::app()->getStore()->getStoreId());
			break;
			case 'prepaid_pay':
			$smsTemp = Mage::getStoreConfig('smsapp/sms_routesm/order_success_pre', Mage::app()->getStore()->getStoreId());
			break;
			case 'order_cancel':
			$smsTemp = Mage::getStoreConfig('smsapp/sms_routesm/order_cancel_temp', Mage::app()->getStore()->getStoreId());
			break;
			case 'order_shipped':
			$smsTemp = Mage::getStoreConfig('smsapp/sms_routesm/order_shipped_temp', Mage::app()->getStore()->getStoreId());
			break;
			case 'order_returned':
			$smsTemp = Mage::getStoreConfig('smsapp/sms_routesm/order_returned_temp', Mage::app()->getStore()->getStoreId());
			break;
			case 'order_delivered':
			$smsTemp = Mage::getStoreConfig('smsapp/sms_routesm/order_delivered_temp', Mage::app()->getStore()->getStoreId());
			break;
			case 'order_verify':
			$smsTemp = Mage::getStoreConfig('smsapp/sms_routesm/order_verify_temp', Mage::app()->getStore()->getStoreId());
			break;
			case 'customer_signup':
			$smsTemp = Mage::getStoreConfig('smsapp/sms_routesm/customer_signup', Mage::app()->getStore()->getStoreId());
			break;
			default:
			$smsTemp = 0;
		}

		$msgReplace['{NAME}'] 		 = $cusName;
        $msgReplace['{ORDERNUMBER}'] = $incrementId;
        $msgReplace['{BRAND}'] 		 = $brand;
        // code by Mihir for OTP
      //   if($payType = 'order_success_cod'){
      //   	$otp = time();
      //   	$msgReplace['{OTP}'] = $otp;
      //   	$dataToAddInOTPGrid = array(
      //   		'otp' => $otp,
      //   		'orderid' => $incrementId,
      //   		'isverified' => 0
    		// );
    		// Mage::getModel('codotp/codotp')->setData($dataToAddInOTPGrid)->save();
      //   }
        // code by Mihir for OTP
        $msg 	 	= strtr($smsTemp,$msgReplace);
        $sendSms   	= $this->sensSms($msg, $mob, $storeId);
		return;
	}

	public function getTemplateAndSendSmsForStorePickup($sDefineValue = "", $mob, $incrementId ='', $brand ='', $storeId = '', $storeName, $pickup_start_date, $pickup_end_date ,  $sku , $QTY , $customer_name , $ITEMID){
		//Mage::log('getTemplateAndSendSmsForStorePickup',null,'pay.log');
		$smsTemp = '';
		
			$recipients_template = Mage::getStoreConfig('order_notification/'.$sDefineValue.'/customer_recipients_template',$notificationParams['store_id']);
			$smsTemp = $recipients_template;
			
        $msgReplace['{ORDERNUMBER}'] = $incrementId;
        //$msgReplace['{BRAND}'] 		 = $brand;
        $msgReplace['{pickup_start_date}'] = $pickup_start_date;
        $msgReplace['{pickup_end_date}'] = $pickup_end_date;
        $msgReplace['{order_increment_id}'] = $incrementId;
        //$msgReplace['{PICKUPUPTODATE}'] = $pickupUptoDate;
        $msgReplace['{STORENAME}'] 	 = $storeName;
        $msgReplace['{SKU}'] 	 = $sku;
        $msgReplace['{QTY}'] 	 = $QTY;
        $msgReplace['{CUSTOMER_NAME}'] 	 = $customer_name;
        $msgReplace['{ITEMID}'] 	 = $ITEMID;
        
        $msg 	 	= strtr($smsTemp,$msgReplace);		
        $sendSms   	= $this->sensSms($msg, $mob, $storeId);
		return;
	}

	public function getTemplateAndSendSmsForNotification($smsTemp,$allPhonesToNotify,$notificationParams){

		foreach($notificationParams as $key =>$sNotifymsg){

				$msgReplace['{'.$key.'}'] = $sNotifymsg;

		}

		/*echo '<pre>';
		print_r($msgReplace);
		exit;*/

		/*$msgReplace['{STORENAME}'] 		 = $notificationParams['STORENAME'];
        $msgReplace['{ITEMID}'] = $notificationParams['ITEMID'];
        $msgReplace['{SKU}'] = $notificationParams['SKU'];
        $msgReplace['{QTY}'] = $notificationParams['QTY'];
        $msgReplace['{REJECTEDREASON}'] = $notificationParams['REJECTEDREASON'];*/
        
        $msg 	 	= strtr($smsTemp,$msgReplace);
        // print_r($msg); print_r($allPhonesToNotify);exit();
        foreach ($allPhonesToNotify as $mob) {
        	$sendSms   	= $this->sensSms($msg, $mob);
        }
		return;
	}
	
	public function getParentproduct($productobject){

		// $product = Mage::getModel('catalog/product');
		// $productobject = $product->load($product->getIdBySku($sku));
		// $ProductId = $product->getIdBySku($sku);
		// $helperdata = Mage::helper("modulename/data");

		if($productobject->getTypeId() == 'simple'){
 		//product_type_grouped
			$parentIds = Mage::getModel('catalog/product_type_grouped')
			->getParentIdsByChild($productobject->getId());


		//product_type_configurable
			if(!$parentIds){
				$parentIds = Mage::getModel('catalog/product_type_configurable')
				->getParentIdsByChild($productobject->getId());

			}
 			/*//product_type_bundle
			if(!$parentIds){
				$parentIds = Mage::getModel('bundle/product_type')
				->getParentIdsByChild($productobject->getId());

			}*/

		return $parentIds[0];
		}
		return '';
		//return $parentIds;
	}

	public function getParentUrl($product){

		$config_id = Mage::getModel('catalog/product_type_configurable')->getParentIdsByChild($product->getId());
		$configProduct = Mage::getModel('catalog/product')->load($config_id);
		$configUrl     = $configProduct->getProductUrl();
		$color_id = Mage::getResourceSingleton('catalog/product')->getAttributeRawValue($product->getId(), 'color', Mage::app()->getStore());
		$size_id = Mage::getResourceSingleton('catalog/product')->getAttributeRawValue($product->getId(), 'size', Mage::app()->getStore());
		$product_url = $configUrl."?attr=".$color_id;
		//$product_url = $configUrl."?attr=".$color_id."/".$size_id;
		if($color_id){
			return $product_url;
		}
		else{
			return $product->getProductUrl();
		}
	}

}
