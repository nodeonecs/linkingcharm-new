<?php

class SLandsbek_SimpleOrderExport_Model_Export_Purchasereport extends SLandsbek_SimpleOrderExport_Model_Export_Abstract
{
    const ENCLOSURE = '"';
    const DELIMITER = ',';

    public function exportOrders($params) 
    {
        $fileName = 'purchase_report_'.date("Ymd_His").'.csv';
        $fp = fopen(Mage::getBaseDir('export').'/'.$fileName, 'w');

        $this->writeHeadRow($fp,$params);
        $this->writeOrder($fp,$params);

        fclose($fp);

        return $fileName;
    }

    protected function writeHeadRow($fp,$params) 
    {
        fputcsv($fp, $this->getHeadRowValues(), self::DELIMITER, self::ENCLOSURE);
    }

    protected function writeOrder($fp,$params) 
    {
        $report_from = Mage::app()->getRequest()->getParam('report_from');
        $report_to = Mage::app()->getRequest()->getParam('report_to');

        $inward_date_from = date('Y-m-d',strtotime($report_from));
        $inward_date_to = date('Y-m-d',strtotime($report_to));
        if(isset($inward_date_from) && isset($inward_date_to)){
          $datefilter = " where created_at BETWEEN '".$inward_date_from."' AND '".$inward_date_to."' ";
        }
        $read = Mage::getSingleton('core/resource')->getConnection('core_read'); 

        $reportData = "SELECT sku,sum(qty) as qty,avg(avg_price) as price
        FROM purchase_master ".$datefilter." GROUP BY sku";

        $itemList = $read->fetchAll($reportData);
        
        foreach ($itemList as $item) {
           
           $_product = Mage::getModel('catalog/product')->load($item['sku']);
           $_salesData = $this->getSalesDataBySku($item['sku'],$inward_date_from,$inward_date_to);
           
           $discountPerProduct = $_salesData['discount_amount']/intval($_salesData['qty_ordered']);
           $sales_qty = intval($_salesData['qty_ordered'])-intval($_salesData['qty_refunded']);
           $discount_amount = $discountPerProduct*$sales_qty;

           $total_qty_remaining = $item['qty']-$sales_qty;
           $stock_value = $total_qty_remaining*$item['price'];
           $purchase_amount = $item['price']*intval($item['qty']);
           $gross_amount = $_salesData['price']*$sales_qty;

           $net_sales_amount = floatval($gross_amount)-floatval($discount_amount);

           $categories = $this->getItemCategories($_product);
           $record1 = array();
           $record1['sku'] = $item['sku'];
           $record1['hsn_code'] = $_product->getData('hsncode');
           $record1['name'] = $_product->getData('name');
           $record1['category'] = $categories;
           $record1['purchase_qty'] = intval($item['qty']);//Purchase Quantity
           $record1['purchase_amount'] = floatval($purchase_amount);//Purchase Amount
           $record1['sales_qty'] = intval($sales_qty);//Sales Quantity
           $record1['gross_amount'] = floatval($gross_amount);
           $record1['discount_amount'] = floatval($discount_amount);
           $record1['net_sales_amount'] = floatval($net_sales_amount);
           $record1['stock_quantity'] = intval($total_qty_remaining);
           $record1['stock_value'] = $stock_value;

           fputcsv($fp, $record1, self::DELIMITER, self::ENCLOSURE);
        }
        // exit;
    }

    protected function getHeadRowValues() 
    {
        return array(         
            'SKU',
            'HSN Code',
            'Name',
            'Category',
            'Purchase Quantity',
            'Purchase Amount',
            'Sales Quantity',
            'Gross Amount',
            'Discount Amount',
            'Net Sales Amount',
            'Stock quantity',
            'Stock Value',
    	);
    }
}
?>