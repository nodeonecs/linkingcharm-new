// // HOTFIX: We can't upgrade to jQuery UI 1.8.6 (yet)
// This hotfix makes older versions of jQuery UI drag-and-drop work in IE9
if (typeof jQuery['ui'] !== 'undefined') {
	(function($) {
		if ($.browser.msie && document.documentMode >= 9) {
			var a = $.ui.mouse.prototype._mouseMove;
			$.ui.mouse.prototype._mouseMove = function(b) {
				b.button = 1;
				a.apply(this, [b]);
			};
		}
	}(jQuery));
}

// Specify the range for charms' dropping.
var _dropRange = 125;
var cookie_name = 'current_profile';

var cookie_path = '/';
var first_time = null;
var cart_item = null;

if (typeof $j === 'undefined') {
	$j = jQuery.noConflict();
}

(function($) {
// VERTICALLY ALIGN FUNCTION
$j.fn.vAlign = function() {
	return this.each(function(i) {
		var ah = $j(this).height();
		var ph = $j(this).parent().height();
		var mh = (ph - ah) / 2;
		$(this).css('margin-top', mh);
	});
};
})(jQuery);
var sampleListItem, sampleBrowseItem;

$j(document).ready(function() {
	showOverlay();
	$j(window).resize(function() {
		if($j(window).width() <= 760) {
			var viewportHeight = $j(window).height() - $j('#header').height() - $j('#stylerOptions > div.mainStylerContent').height();
			$j('#stylerCanvas').css('height', viewportHeight+'px');
			autoScrollCanvas();
		} else {
			$j('#stylerCanvas').css('height', '');
		}
	});

	sampleListItem = $j('#sampleListItem').removeAttr('id');
	sampleListItem.detach();
	sampleBrowseItem = $j('#sampleBrowseItem').removeAttr('id');
	sampleBrowseItem.detach();

	$j('.expandCollapse').click(function() { //collapse bottom for mobile
		$j(this).toggleClass('collapse');
		$j('.expandCollapseContent.scrollingContent').toggleClass('display');
		$j(window).resize();
	});

	$j('.menuClickTarget').click(function() { //dropdown tabs & fields
		$j(this).parent().toggleClass('openDropdown');
	});
	$j('.pUIDropdown .dropdownContent').children().click(function() {
		$j('.dropdownContent').parent().removeClass('openDropdown');
		$j(this).addClass('selected').siblings().removeClass('selected');
	});

	//lazy loading of browse images
	$j('#itemBrowseContainer, #itemListContainer').scroll(function() {
		var offset = $j(this).offset(),
		width = $j(this).width(),
		height = $j(this).height();
		$j(this).children('.img-not-loaded').each(function() {
			var itemOffset = $j(this).offset();
			if(itemOffset.top >= offset.top - height && itemOffset.top <= offset.top + 2 * height &&
				itemOffset.left >= offset.left - width && itemOffset.left <= offset.left + 2 * width) {
				$j(this).find('img').attr('src', $j(this).data('product-info').default_filename);
			$j(this).removeClass('img-not-loaded');
		}
	});
	});

	//Just used to test. Remove/Edit later
	$j('.tabMenu.itemList').click(function() {
		$j(this).toggleClass('active');
		$j('.tabContent.itemList').toggleClass('display');
	});
	$j('div.tabContent.itemList span.actionItems input.cancel').click(function() {
		$j('.tabMenu.itemList').click();
	});
	$j('div.tabContent.browse span.actionItems input.cancel').click(function() {
		$j('.tabMenu.browse').click();
	});
	$j('.tabMenu.browse').click(function() {
		if($j('.tabMenu.itemList.active').size()) {
			$j('.tabMenu.itemList').click();
		} else {
			$j(this).toggleClass('active');
			$j('div.mainStylerContent').toggleClass('display');
		}
	});
	$j('#filterBrowse').selectbox({
		onChange: function(val,inst) {
			if(val === 'search') {
				$j('#searchFilter').show();
				$j('#categoryFilter').hide();
				$j('#filterItems').addClass('itemSearch');
			} else {
				$j('#filterItems').removeClass('itemSearch');
				$j('#searchFilter').hide();
				$j('#categoryFilter').show();
				console.log(typeof type_by_browse_by[val]);
				if(typeof type_by_browse_by !== 'undefined' && typeof type_by_browse_by[val] !== 'undefined') 
				{
					var lastVal = $j('#filterType').val();
					$j('#filterType option').remove();
					$j('#filterType').append('<option value="">Theme</option>');
					if(!empty(type_by_browse_by[val])){
						for(var i in type_by_browse_by[val]) {
							$j('#filterType').append($j('<option value="'+i+'"'+(i==lastVal?' selected':'')+'>'+type_by_browse_by[val][i]+'</option>'))
						}
					}			
					
				}
				if(typeof type_by_theme!== 'undefined' && typeof type_by_theme[val] !== 'undefined') 
				{
					var lastVal1 = $j('#filterFinish').val();
					$j('#filterFinish option').remove();
					$j('#filterFinish').append('<option value="">Finish</option>');
					if(!empty(type_by_theme[val])){
						for(var k in type_by_theme[val]) {
							$j('#filterFinish').append($j('<option value="'+k+'"'+(k==lastVal?' selected':'')+'>'+type_by_theme[val][k]+'</option>'))
						}
					}
				}

				if(typeof type_by_color!== 'undefined' && typeof type_by_color[val] !== 'undefined') 
				{
					var lastVal2 = $j('#filterColor').val();
					$j('#filterColor option').remove();
					$j('#filterColor').append('<option value="">Colors</option>');
					if(!empty(type_by_color[val])){
						for(var l in type_by_color[val]) {
							$j('#filterColor').append($j('<option value="'+l+'"'+(l==lastVal2?' selected':'')+'>'+type_by_color[val][l]+'</option>'))
						}
					}
				}
			$j('.itemFilter').first().change();
		}
		$j('#sbOptions_'+inst.uid+' li a').removeClass('sbSelected');
		$j('#sbOptions_'+inst.uid+' li a[rel='+val+']').addClass('sbSelected');
	}
}).selectbox('change', $j('#filterBrowse').val());

	// if build overlay exists move it to the top of the stack
	var buildOverlay = document.getElementById('build-overlay');
	if (buildOverlay) {
		buildOverlay.parentNode.removeChild(buildOverlay);
		document.body.appendChild(buildOverlay);
	}

	//load profile
	initProfile();

	$j('.itemFilter').change(function() {
		if(!$j('#filterItems:visible').size()) {
			$j('#filterItems').click();
		}
	});

	$j('#filterForm').submit(function() {
		return false;
	})

	$j('#filterItems').click(function() {
		if ($j.fancybox.hasOwnProperty("showActivity")) {
			$j.fancybox.showActivity(); // v1
		} else {
			$j.fancybox.showLoading(); // v2
		}
		var params = $j('#filterForm').serialize();
		$j.get(
			baseurl+'styler/index/get_items',
			params,
			function(response) {
				if ($j.fancybox.hasOwnProperty("hideActivity")) {
					$j.fancybox.hideActivity(); // v1
				} else {
					$j.fancybox.hideLoading(); // v2
				}

				if (response.error) {
					alert(response.error);
				} else {
					$j('#itemBrowseContainer').html('').scrollTop(0);
					for (var i = 0; i < response.items.length; i++) {
						var data = response.items[i];
						var newItem = sampleBrowseItem.clone();
						//newItem.find('img').data('product-info', data);
						// console.log(data);
						var typeOfProduct = 'product_type_' + data.product_type.toLowerCase().replace(/\s*/g, '');
						
						$j('#itemBrowseContainer').append(populateItem(data, newItem));
						if(data.product_type === 'Charm') {
							newItem.find('img').draggable({
								cursorAt: {left: 0, top: 0},
								helper: function() {
									var data = $j.extend({}, $j(this).closest('.item').data('product-info'));
									return getHelper(data);
								},
								zIndex: 50,
								start: function(event, ui) {
									var data = $j(this).closest('.item').data('product-info');
									if (data.product_type === 'Charm') {
										if (data.isStopper != 0 && $j('#canvas').data('product-info').isBarrel == 0) {
											$j(document).trigger("mouseup");
											alert("Stopper beads are only required on barrel lock bracelets and will not fit our other bracelet types (including the one you are using). You can either select a different bead to add to your bracelet, or change your current bracelet to one of our barrel lock bracelets.");
											return false;
										}
									}
								},
								appendTo: 'body',
								drag: dragItem,
								stop: dragStop
							});
						}
					}
					if($j('div.mainStylerContent.display').size()) {
						$j('.tabMenu.browse').toggleClass('active');
						$j('div.mainStylerContent').toggleClass('display');
					}
					$j('#itemBrowseContainer').scroll();
				}
			},
			'json'
			);
	}).click();

	$j('.hide-build-overlay, .close-btn').click(function() {
		hideOverlay();
	});

	$j('#show-build-overlay').live('click', function() {
		showOverlay();
		return false;
	});

	$j('#stylerCanvas').kinetic({
		cursor: 'inherit',
		maxvelocity: 60,
		filterTarget: function(t) {
			if($j(t).hasClass('ui-draggable') || $j(t).attr('id') === 'add_selected_to_cart') {
				return false;
			}
			console.log(t);
			return true;
		}
	});

	$j('#clear_chain').click(function() {
		$j('#canvas img').remove();
		$j('#itemListContainer .product_type_charm').remove().scroll();
		edited();
	});

	$j('.item .icon.info').live('click', function() {
		var data = $j(this).closest('.item').data('product-info');
		$j.fancybox({
			href: 'browse/' + toSeoUrl(data.product_id, data.style) + '?popup',
			type: "ajax",
		});
	});

	$j('input.add_item').live('click', function() {
		var data = $j(this).closest('.item').data('product-info');
		if (data.product_type === 'Chain') {
			data.actual_width = parseFloat(data.actual_width);
			if(getTotalCharmWidth() <=  data.actual_width) {
				addChain(data);
				//shift the charm positions to the new path
				chainMakeRoom(0,0);
				//lock the new positions in place
				chainReset(true);
				return edited();
			} else {
				$j(ui.helper).remove();
				alert('You have too many charms for this chain, please remove some');
			}
		} else {
			alert('Invalid product type.');
		}
	});
	
	// $j('#show_selected_to_cart').live('click', function () {
	// 	prepareStylerPop(getCartItems());
	// });

	
	$j('#show_selected_to_cart').bind("click touchstart", function() {
		prepareStylerPop(getCartItems());
	});

	$j('#styler_product_list .close').bind("click touchstart", function() {
		$j('#styler_product_list').css('display','none');
	});

	// $j('#styler_product_list .close').on('click', function () {
	// 	$j('#styler_product_list').css('display','none');
	// });

	$j('#add_to_cart_styler').bind('click touchstart', function (event) {
		event.preventDefault();
		addToCartStyler($j( "form#styler_products" ).serializeArray());
	});

	$j('#move_to_checkout_styler').bind('click touchstart', function (event) {
		event.preventDefault();
		console.log(cartUrl);
		if ($j.fancybox.hasOwnProperty("showActivity")) {
			$j.fancybox.showActivity(); // v1
		} else {
			$j.fancybox.showLoading(); // v2
		}
		window.location.href = cartUrl;
	});

	$j('#add_selected_to_cart').on('click', function () {
		addToCart(getCartItems());
	});


	$j('input.add_item_to_cart').live('click', function() {
		var parent = $j(this).closest('.itemListItem');
		if(parent.size()) {
			var item = $j.extend({}, parent.data('product-info'));
			if(item) {
				if(parent.find('input.qty').size()) {
					item.quantity = parent.find('input.qty').val();
				}
				addToCart(item);
			}
		}
	});
});

function toSeoUrl(id, name) {
	console.log("hiiiii"+(id?id + '-':'') + name.replace(/[^a-zA-Z0-9]+/g, '-').replace(/^\-+|\-+$/g, ''));
	return (id?id + '-':'') + name.replace(/[^a-zA-Z0-9]+/g, '-').replace(/^\-+|\-+$/g, '');
}

function getHelper(data) {
	var scale = getPxPerMM();
	data.full_width = parseFloat(data.full_width);
	data.full_height = parseFloat(data.full_height);

	var helper = $j('<img src="' + data.filename + '" width="'+(data.full_width*scale)+'" height="'+(data.full_height*scale)+'">');
	helper.data('product-info', data);
	return helper;
}

function getCartItems() {
	var ret = {charms:[]};
	ret.chain = $j('#canvas').data('product-info');
	$j('#canvas img').each(function() {
		ret.charms.push($j(this).data('product-info'));
	});
	return ret;
}

function populateItem(data, el) {
	if(el.find('img').size()) {
		el.addClass('img-not-loaded');
		el.find('img').attr('src','/images/product-browse-loader.gif');
	}
	el.data('product-info', data);
	if(el.find('.price').size()) {
		el.find('.price').text(defaultCurrencyChar + parseFloat(data.price).toFixed(2));
	}
	if(el.find('.styleNumber').size()) {
		el.find('.styleNumber').text(data.sku);
	}
	if(el.find('.styleName').size()) {
		el.find('.styleName').text(data.style);
	}

	if(el.find('.system_id').size()) {
		el.find('.system_id').val(data.product_id);
	}
	if(typeof data['quantity'] !== 'undefined' && el.find('.quantity').size()) {
		if(el.find('.quantity').size()) {
			el.find('.quantity').text(data.quantity);
		}
		if(el.find('input.qty').size()) {
			el.find('input.qty').val(data.quantity);
		}
	}
	if(el.find('input.product_id').size()) {
		el.find('input.product_id').val(data.retailer_product_data);
	}
	el.addClass('product_type_' + data.product_type.toLowerCase().replace(/\s*/g, ''));
	return el;
}

function empty( val ) {
	if (val === undefined)
		return true;

	if (typeof (val) == 'function' || typeof (val) == 'number' || typeof (val) == 'boolean' || Object.prototype.toString.call(val) === '[object Date]')
		return false;

    if (val == null || val.length === 0)        // null or 0 length array
    	return true;

    if (typeof (val) == "object") {
        // empty object

        var r = true;

        for (var f in val)
        	r = false;

        return r;
    }

    return false;
}

function dragStop(event, ui) {
	$j('#indicator').hide();
	var data = $j.extend({}, $j(ui.helper).data('product-info'));
	if(!$j(ui.helper).parent('body').size()) {
		$j(ui.helper).unwrap();
	}
	$j(ui.helper).remove();

	var el = $j(this);
	if(el.closest('#canvas').size()) {
		//need to remove the rotation span in ie8
		while(el.parent().attr('id') !== 'canvas') {
			el = el.parent();
		}
		el.remove();
		removeCharm(data);
	}
	if (data.product_type === 'Charm') {
		if(typeof data['left'] !== 'undefined') {
			addCharm(data);
			chainReset(true);
		} else {
			chainReset();
		}
	}
	return edited();
}

function dragItem(event, ui) {
	var data = $j(ui.helper).data('product-info');
	delete(data.last_left);
	var indicator = $j('#indicator');

	if (data.product_type === 'Charm') {
		var tryLeft = event.originalEvent.pageX - $j('#canvas').offset().left;
		var tryTop = event.originalEvent.pageY - $j('#canvas').offset().top;
		var percent = getClosestPointAsPercent(tryLeft, tryTop, $j('#canvas').data('product-info'));
		var left = chainMakeRoom(percent, data.chain_width);
		if (left >= 0) {

			percent = returnArcPercent(left);
			//get the correct point location
			var point = getBezierPoint(percent, $j('#canvas').data('product-info'));

			data.left = Math.round(left);
			//we need to store the rotation so we can correctly offset any rotation and shifting
			//upon subsequent drags
			data.rotation = point[2];

			//show the indicator on top of the drop point
			indicator.css({
				left: point[0] - indicator.width() / 2 + 'px',
				top: point[1] - indicator.height() / 2 + 'px'
			}).show();
		} else {
				//we are not within drop-range, we should unset the parameters that suggest
				delete(data['left']);
				indicator.hide();
			}
			var img = $j(ui.helper);

		//if ie8
		if(document.all && !document.addEventListener) {

			var handle = getImgHandle(data);
			img.css({
				marginLeft: -handle.left+'px',
				marginTop: -handle.top+'px'
			});
			/**
			 * this almost works
			img.rotate(point[2]);
			var parent = img.parent();
			var shift = getRotationShift(data, data.rotation, parent);
			img.data('product-info', data);
			if(parent.css('top') === 'auto') {
				parent.css({
					position: 'absolute',
					top: '0px',
					left: '0px'
				});
			}
			if (typeof data.start_rotation !== 'undefined') {
				var originalShift = getRotationShift(data, data.start_rotation, parent);
				var originalSize = {w: $j(this).width(), h: $j(this).height()};
				var actualSize = {w: parent.width(), h: parent.height()};
				log({
					original_rotation: data.start_rotation,
					originalShiftX: originalShift.x,
					originalShiftY: originalShift.x,
					originalWidth: originalSize.w,
					actualWidth: actualSize.w,
					originalHeight: originalSize.h,
					actualHeight: actualSize.h
				});
				adjustment.x = shift.x + originalShift.x;
				adjustment.y = shift.y + originalShift.y;
			} else {
				adjustment.x = shift.x;
				adjustment.y = shift.y;
			}
			parent.css({
				left: adjustment.x + 'px',
				top: adjustment.y + 'px'
			});*/
		} else {
			if(data.rotation) {
				img.rotate(data.rotation);
				var shift = getRotationShift(data, data.rotation, img);
			} else {
				var shift = getRotationShift(data, 0, img);
			}
			if (typeof data.start_rotation !== 'undefined') {
				var originalShift = getRotationShift(data, data.start_rotation, img);
				shift.x += originalShift.x;
				shift.y += originalShift.y;
			}
			var adjustment = getImgHandle(data);
			shift.x -= adjustment.left;
			shift.y -= adjustment.top;
			img.css({
				marginLeft: shift.x + 'px',
				marginTop: shift.y + 'px'
			});
		}
	}
}

function removeCharm(data) {
	var el = $j('#itemListContainer #item_'+data.product_id);
	if(el.size()) {
		var data = el.data('product-info');
		data.quantity--;
		if(data.quantity > 0) {
			populateItem(data, el);
		} else {
			el.remove();
		}
	}
	$j('#itemListContainer').scroll();
}

function chainReset(keepCurrentLocations) {
	$j('#canvas img').each(function() {
		var data = $j(this).data('product-info');
		if(typeof data['temp_left'] !== 'undefined') {
			if(keepCurrentLocations) {
				data.left = data.temp_left;
			}
			delete(data['temp_left']);
			shiftCharm($j(this));
		}
	});
}

function chainMakeRoom(percent, width) {
	var charms = jQuery.makeArray($j('#canvas img:not(.ui-draggable-dragging)').sort(sortLeftAsc)),
	totalWidth = Math.floor(getTotalArcLength()),
	shiftBackLeft = 0,
	shiftBackRight = 0,
	targetLeft = -1,
	currentLeft,
	currentRight,
	scale = getPxPerMM(),
	shiftRight = Math.floor(width*scale/2),
	shiftLeft = Math.ceil(width*scale/2),
	charmData = new Array();
	for(var i = 0; i < charms.length; i++) {
		var charm = $j(charms[i]).data('product-info');
		charmData.push(charm);
		charm.chain_width = parseFloat(charm.chain_width);
		charm.left = parseFloat(charm.left);
		charm.temp_left = charm.left;
	}

	//first shift any conflicting charms to the right
	if(percent >= 0 && percent <= 100) {
		targetLeft = Math.max(shiftLeft, Math.min(totalWidth-shiftRight, Math.floor(getArcLength(percent))));

		var firstCharm = null;
		currentRight = targetLeft + shiftRight;
		for(var i = 0; i < charmData.length; i++) {
			var charm = charmData[i];
			if(charm.temp_left >= targetLeft) {
				if(charm.temp_left - Math.floor(charm.chain_width*scale/2) <= currentRight) {
					if(firstCharm === null) {
						firstCharm = charm;
					}
					charm.temp_left = currentRight + Math.floor(charm.chain_width*scale/2);
				}
				currentRight = charm.temp_left + Math.floor(charm.chain_width*scale/2);
			}
			if(charm.temp_left + Math.floor(charm.chain_width*scale/2) > totalWidth) {
				shiftBackLeft = charm.temp_left + Math.floor(charm.chain_width*scale/2) - totalWidth;
				if(firstCharm !== null && (targetLeft >= charm.temp_left || targetLeft + shiftRight === totalWidth)) {
					targetLeft = firstCharm.temp_left - shiftBackLeft + Math.floor(firstCharm.chain_width*scale/2) - shiftRight;
					firstCharm.temp_left = firstCharm.left - width;
				} else {
					targetLeft -= shiftBackLeft;
				}
			}
		}
		if(shiftBackLeft){
			for(var i = 0; i < charmData.length; i++) {
				if(typeof charmData[i]['temp_left'] !== 'undefined' && charmData[i].temp_left > targetLeft) {
					charmData[i].temp_left -= shiftBackLeft;
				}
			}
			//if width = 0 then we are just changing the shape of the chain
			//this may mean moving charms around to fit the new length
			if(width === 0) {
				shiftBackLeft = 0;
			}
		}

		charmData.reverse();
		var firstCharm = null;
		currentLeft = targetLeft - shiftLeft;
		for(var i = 0; i < charmData.length; i++) {
			var charm = charmData[i];
			if(charm.temp_left <= targetLeft) {
				if(charm.temp_left + Math.ceil(charm.chain_width*scale/2) >= currentLeft) {
					if(firstCharm === null) {
						firstCharm = charm;
					}
					charm.temp_left = currentLeft - Math.ceil(charm.chain_width*scale/2);
				}
				currentLeft = charm.temp_left - Math.ceil(charm.chain_width*scale/2);
			}
			if(charm.temp_left - Math.ceil(charm.chain_width*scale/2) < 0) {
				shiftBackRight = Math.ceil(charm.chain_width*scale/2) - charm.temp_left;
				if(firstCharm !== null && (targetLeft <= charm.temp_left || targetLeft - shiftLeft === 0)) {
					targetLeft += firstCharm.temp_left + shiftBackRight - Math.ceil(firstCharm.chain_width*scale/2) + shiftLeft;
					firstCharm.temp_left = firstCharm.left + width;
				} else {
					targetLeft += shiftBackRight;
				}
			}
		}
		if(shiftBackLeft && shiftBackRight) {
			for(var i = 0; i < charmData.length; i++) {
				delete(charmData[i]['temp_left']);
			}
			targetLeft = -1;
		} else if(shiftBackRight) {
			for(var i = 0; i < charmData.length; i++) {
				if(typeof charmData[i]['temp_left'] !== 'undefined' && charmData[i].temp_left <= targetLeft) {
					charmData[i].temp_left += shiftBackRight;
				}
			}

			currentRight = targetLeft + shiftRight;
			charmData.reverse();
			for(var i = 0; i < charmData.length; i++) {
				var charm = charmData[i];
				if(charm.temp_left - Math.floor(charm.chain_width*scale/2) <= currentRight &&
					charm.temp_left >= targetLeft) {
					charm.temp_left = currentRight + Math.floor(charm.chain_width*scale/2);
				currentRight = charm.temp_left + Math.ceil(charm.chain_width*scale);
			}
			if(charm.temp_left + Math.floor(charm.chain_width*scale/2) > totalWidth) {
				shiftBackLeft = charm.temp_left + Math.floor(charm.chain_width*scale/2) - totalWidth;
			}
		}
		if(shiftBackLeft) {
			targetLeft = -1;
			for(var i = 0; i < charmData.length; i++) {
				delete(charmData[i]['temp_left']);
			}
		}
	}
}
for(var i = 0; i < charms.length; i++) {
	shiftCharm($j(charms[i]));
}
return targetLeft;
}

function shiftCharm(img) {
	var data = img.data('product-info'),
	chainData = $j('#canvas').data('product-info'),
	left = data.left,
	scale = getPxPerMM();

	img.width(scale*data.full_width);
	img.height(scale*data.full_height);

	if(typeof data['temp_left'] !== 'undefined') {
		left = data.temp_left;
	}
	if(typeof data['last_left'] === 'undefined' ||
		data.last_left !== left ||
		typeof data['last_product_id'] === 'undefined' ||
		data.last_product_id !== chainData.product_id) {
		var point = getBezierPoint(returnArcPercent(left), $j('#canvas').data('product-info'));
	img.rotate(point[2]);

		//this should happen for ie7/8
		if(img.parent().attr('id') !== 'canvas') {
			img = img.parent();
			img.parent().css('border','1px solid red');
		}

		var handle = getImgHandle(data);
		img.css({
			left: point[0] - handle.left + 'px',
			top: point[1] - handle.top + 'px',
			position: 'absolute'
		});
		data.rotation = point[2];
		var shift = getRotationShift(data, data.rotation, img);
		img.css({
			marginLeft: shift.x + 'px',
			marginTop: shift.y + 'px'
		});
		data.last_left = left;
		data.last_product_id = chainData.product_id;
	}
}

function getTotalCharmWidth() {
	var width = 0;
	$j('#canvas img').each(function() {
		width += parseFloat($j(this).data('product-info').chain_width);
	});
	return width;
}

function sortLeftAsc(a, b) {
	return parseInt($j(a).data('product-info').left) > parseInt($j(b).data('product-info').left) ? 1 : -1;
}


function getRotationShift(data, angle, el) {
	var r = angle * Math.PI / 180;
	var center = {
		x: el.width() / 2,
		y: el.height() / 2
	};
	var pivot = getImgHandle(data);
	var offset = {
		x: pivot.left - center.x,
		y: pivot.top - center.y
	};
	var shift = {
		x: Math.cos(r) * offset.x - Math.sin(r) * offset.y,
		y: Math.sin(r) * offset.x + Math.cos(r) * offset.y
	};
	var margin = {
		x: offset.x - shift.x,
		y: offset.y - shift.y
	};

	return margin;
}

function addChain(data) {
	var oldData = $j('#canvas').data('product-info');
	if(typeof oldData === 'undefined' || oldData.product_id !== data.product_id) {
		$j('#canvas').css('background-image', 'url(' + data.filename + ')');
		$j('#canvas').data('product-info', data);
		var item;
		if($j('#itemListContainer .product_type_chain').size()) {
			item = $j('#itemListContainer .product_type_chain');
		} else {
			item = sampleListItem.clone();
			data.quantity = 1;
			$j('#itemListContainer').append(item);
		}
		populateItem(data, item);
		autoScrollCanvas(true);
		$j('#itemListContainer').scroll();
	}
}

function autoScrollCanvas(force) {
	var i = 0;
	if(force || (!$j('#stylerCanvas').scrollTop() && !$j('#stylerCanvas').scrollLeft())) {
		var canvasWidth = $j('#canvas').width(),
		canvasHeight = $j('#canvas').height(),
		stylerWidth = $j('#stylerCanvas').width(),
		stylerHeight = $j('#stylerCanvas').height(),
		widthDiff = canvasWidth - stylerWidth,
		heightDiff = canvasHeight - stylerHeight;
		if(heightDiff > 0 || widthDiff > 0) {
			var data = $j('#canvas').data('product-info'),
			targetStylerX = Math.round(stylerWidth/2),
			targetStylerY = Math.round(stylerHeight*.65),
			minX, maxX, minY, maxY;
			if(data) {
				for(var i = 0; i <= 100; i+=5) {
					var p = getBezierPoint(i, data);
					if(typeof minX === 'undefined') {
						minX = maxX = p[0];
						minY = maxY = p[1];
					}
					if(p[0] < minX) {
						minX = p[0];
					} else if(p[0] > maxX) {
						maxX = p[0];
					}
					if(p[1] < minY) {
						minY = p[1];
					} else if(p[1] > maxY) {
						maxY = p[1];
					}
				}
				var targetX = Math.round((maxX+minX)/2), targetY = maxY,
				scrollX = Math.max(0, Math.min(widthDiff, targetX-targetStylerX)),
				scrollY = Math.max(0, Math.min(heightDiff, targetY-targetStylerY));
				$j('#stylerCanvas').scrollLeft(scrollX).scrollTop(scrollY);
			}
		}
	}
}

function addCharm(data) {
	if (typeof data['left'] !== 'undefined') {
		var img = $j('<img />');
		img.draggable({
			cursorAt: {left: 0, top: 0},
			helper: function() {
				var data = $j(this).data('product-info');
				return getHelper(data);
			},
			zIndex: 50,
			start: function(event, ui) {
				var data = $j(ui.helper).data('product-info');
				$j(this).addClass('ui-draggable-dragging');
				if(typeof data !== 'undefined' && typeof data['rotation'] !== 'undefined')
					data.start_rotation = data.rotation;
				$j(this).hide();
			},
			appendTo: 'body',
			drag: dragItem,
			stop: dragStop
		});
		img.data('product-info', data);
		img.load(function() {
			shiftCharm($j(this));
			$j(this).fadeIn();
		});
		//not ie8 we can hide the image so it can fade in
		if(!document.all || document.addEventListener) {
			img.hide();
		}
		$j('#canvas').append(img);
		img.attr('src', data.filename);
	}
	var item;
	if($j('#itemListContainer #item_'+data.product_id).size()) {
		item = $j('#itemListContainer #item_'+data.product_id);
		data = item.data('product-info');
		data.quantity += 1;
	} else {
		item = sampleListItem.clone();
		data.quantity = 1;
		$j('#itemListContainer').append(item);
		item.find('img').draggable({
			cursorAt: {left: 0, top: 0},
			helper: function() {
				var data = $j.extend({}, $j(this).closest('.itemListItem').data('product-info'));
				return getHelper(data);
			},
			zIndex: 50,
			start: function(event, ui) {
				var data = $j(this).closest('.itemListItem').data('product-info');
				if (data.product_type === 'Charm') {
					if (data.isStopper != 0 && $j('#canvas').data('product-info').isBarrel == 0) {
						$j(document).trigger("mouseup");
						alert("Stopper beads are only required on barrel lock bracelets and will not fit our other bracelet types (including the one you are using). You can either select a different bead to add to your bracelet, or change your current bracelet to one of our barrel lock bracelets.");
						return false;
					}
				}
			},
			appendTo: 'body',
			drag: dragItem,
			stop: dragStop
		});
	}
	populateItem(data, item);
	item.attr('id', 'item_'+data.product_id);
	$j('#itemListContainer').scroll();
}

function recalculateTotal() {
	var totalPrice = 0;
	var mobileView = '';
	$j('#canvas, #canvas img').each(function() {
		console.log($j(this).attr('class'));
		if($j(this).attr('class')=='ui-draggable')
		{
			mobileView += "<span class='mobile-ui-dragdrop'><img class='mobile-ui-dragdrop-img' src='"+$j(this).attr('src')+"'></span>";
		}
		$j('#mobile_sidebar').html(mobileView);
		totalPrice += parseFloat($j(this).data('product-info').price);
	});
	if(isNaN(totalPrice)) {
		$j('div.total span.price').text('--');
	} else {
		$j('div.total span.price').text(defaultCurrencyChar + parseFloat(totalPrice).toFixed(2));
	}
}

function loadProfile(data) {
	if (data.profile) {
		var img = new Image;
		img.onload = function() {
			var charms = data.charms, profile = data.profile;
			//log(charms);
			addChain(profile);
			loadCharms(charms);
			//this corrects positioning issues
			edited();
		};
		img.src = data.profile.filename;

	} else if(data.charms) {
		loadCharms(data.charms);
		edited();
	}

	$j(window).resize();
}

function loadCharms(charms) {
	if (charms) {
		for(var k = 0; k < charms.length; k++) {
			addCharm(charms[k]);
		}
	}
}

function hideOverlay() {
	if ($j('#build-overlay').size()) {
		$j('#build-overlay').fadeOut();
		$j(document).css('overflow', 'auto');
	}
}

function showOverlay() {
	if ($j('#build-overlay').size()) {
		$j('#build-overlay').fadeIn();
		$j('#build-overlay-text').vAlign();
		$j(document).css('overflow', 'hidden');
	}
}

function getPxPerMM(data) {
	if(typeof data === 'undefined') {
		data = $j('#canvas').data('product-info');
		data.actual_width = parseFloat(data.actual_width);
	}
	return getTotalArcLength(data)/data.actual_width;

}

function getImgHandle(data) {
	var scale = getPxPerMM();
	data.x = parseFloat(data.x);
	data.y = parseFloat(data.y);
	data.full_width = parseFloat(data.full_width);
	data.full_height = parseFloat(data.full_height);
	switch(data.product_type) {
		case 'Charm':
		return {
			left: data.x * scale * data.full_width,
			top: data.y * scale * data.full_height
		};
		case 'Chain':
	}
}

//-----------------------------------------------------------------------------
//from add_chain_builder.tpl the calculation part.
function getClosestPointAsPercent(mX, mY, data) {
	var dX, dY, dS = 0,
	index, minimumDistance,
	precisions = [2, 1, .5, .1, .05, .01],
	start = 0, stop = 100;
	for (var i = 0; i < precisions.length; i++) {
		var precision = precisions[i];
		if (typeof (precisions[i - 1]) !== undefined && typeof (index) !== 'undefined') {
			start = Math.max(0, index - precisions[i - 1]);
			stop = Math.min(100, index + precisions[i - 1]);
		}
		for (var t = start; t <= stop; t += precision) {
			var point = getBezierPoint(t, data);
			dX = mX - point[0];
			dY = mY - point[1];
			dS = Math.sqrt(dX * dX + dY * dY);
			if (typeof (minimumDistance) === 'undefined' || dS < minimumDistance) {
				minimumDistance = dS;
				index = t;
			}
		}
	}
	if (typeof _dropRange !== 'undefined' && _dropRange > 0 && minimumDistance > _dropRange) {
		return -1;
	}
	return index;

}
var factorials = {};
function Factorial(n) {
	if(typeof factorials[n] === 'undefined') {
		var rval = 1;
		for (var i = 2; i <= n; i++) {
			rval = rval * i;
		}
		factorials[n] = rval;
	}
	return factorials[n];
}
var binomials = {};
//Calculate the binomial coefficients.
function binomial(n,k) {
	if(typeof binomials[n+":"+k] === 'undefined') {
		binomials[n+":"+k] = Factorial(n) / (Factorial(k) * Factorial(n - k));
	}
	return binomials[n+":"+k];
}

function getBezierPoint(percent, data) {
	if(percent < 0) {
		percent = 0;
	} else if(percent > 100) {
		percent = 100;
	}

	if(typeof data['real_points_by_percent'] === 'undefined') {
		data.real_points_by_percent = {};
	}
	var percentKey = parseFloat(percent).toFixed(2);
	if(typeof data.real_points_by_percent[percentKey] === 'undefined') {
		var interval = getIntervalAtArcLength(getArcLength(percent), data);
		data.real_points_by_percent[percentKey] = getBezierPointAtInterval(interval, data);
	}
	return data.real_points_by_percent[percentKey];
}

//get the Bezier Curve percentage from a specific length.
function returnArcPercent(arclength) {
	return arclength / getTotalArcLength() * 100;
}

function getBezierPointAtInterval(interval, data) {
	if(!(interval >= 0 && interval <= 1)) {
		alert('invalid interval');
	}

	if(typeof data['interval_points'] === 'undefined') {
		data.interval_points = {};
	}

	if(typeof data.interval_points[interval.toFixed(4)] === 'undefined') {
		var points = data.control_point_array;
		var t = interval;
		var n = points.length - 1;
		var xVal = 0,
		yVal = 0,
		rVal = 0,
		dxVal = 0,
		dyVal = 0;
		//Points on the n-th order Bezier Curve.
		for (var i = 0; i < points.length; i++) {
			var bin = binomial(n, i),
			powdiff = Math.pow(1 - t, n - i),
			pow = Math.pow(t, i);
			xVal += bin * powdiff * pow * points[i].x;
			yVal += bin * powdiff * pow * points[i].y;

			if (typeof (points[i + 1]) !== 'undefined') {
				var bin2 = binomial(n - 1, i);
				dxVal += n * bin2 * powdiff * pow * (points[i + 1].x - points[i].x);
				dyVal += n * bin2 * powdiff * pow * (points[i + 1].y - points[i].y);
			}
		}

		rVal = ((Math.atan2(dyVal, dxVal)) * 180 / Math.PI);

		//now we need to adjust the positions relative to the canvas size
		var oWidth = 650,
		oHeight = 700,
		cWidth = $j('#canvas').width(),
		cHeight = $j('#canvas').height();
		data.interval_points[interval.toFixed(4)] = [xVal - (oWidth-cWidth)/2, yVal - (oHeight-cHeight)/2, rVal];
	}
	return data.interval_points[interval.toFixed(4)];
}

function getArcLengthAtInterval(interval, data) {
	if(!(interval >= 0 && interval <= 1)) {
		alert('invalid interval');
	}

	var points = data.control_point_array;

	var sumArc = 0;
	var x = 0,
	y = 0,
	n = points.length - 1;
	if (n >= Tval.length) {
		return -1;
	} else if(n === 1) {
		x = points[0].x - points[n].x;
		y = points[0].y - points[n].y;
		return Math.sqrt(x * x + y * y) * interval;
	} else {
		var t = interval/2;
		for (var i = 0; i < n; i++) {
			var xVal = 0, yVal = 0, base = 0;
			for (var j = 0; j < n; j++) {
				var pow = Math.pow(1 - (t * Tval[n][i] + t), n - j - 1),
				powj = Math.pow(t * Tval[n][i] + t, j),
				bin = binomial(n - 1, j);
				xVal += n * bin * pow * powj * (points[j + 1].x - points[j].x);
				yVal += n * bin * pow * powj * (points[j + 1].y - points[j].y);
			}
			base = Math.sqrt(xVal * xVal + yVal * yVal);
			sumArc += Cval[n][i] * base;
		}
		return t * sumArc;
	}
}

function getIntervalAtArcLength(arcLength, data) {
	var
	left = 0,
	right = 1,
	mid = 0,
	midLength = 0,
	leftLength = 0,
	rightLength = getTotalArcLength(data);
	while(left < right) {
		mid = left+(right-left)*(arcLength-leftLength)/(rightLength-leftLength);
		midLength = getArcLengthAtInterval(mid, data);

		if(Math.abs(midLength - arcLength) < .5) {
			break;
		} else {
			if(midLength < arcLength) {
				left = mid + .000001;
				leftLength = midLength + .000001;
			} else {
				right = mid - .000001;
				rightLength = midLength - .000001;
			}
		}
	}
	return mid;
}


function getTotalArcLength(data) {
	if(!data) {
	// Legendre-Gauss abscissae (xi values, defined at i=n as the roots of the nth order Legendre polynomial Pn(x))
	data = $j('#canvas').data('product-info');
}

if(typeof data['total_length'] === 'undefined') {
	data['total_length'] = getArcLengthAtInterval(1, data);
}
return data['total_length'];

}

//Use Legendre-Gauss Quadrature to calculate the arc length of n-th order Bezier Curve.
function getArcLength(percent) {
	return getTotalArcLength() * percent / 100;
}

// Legendre-Gauss weights (wi values, defined by a function linked to in the Bezier primer article---A primer on bezier curves)
var Tval = [[],[], [ -0.5773502691896257645091487805019574556476,0.5773502691896257645091487805019574556476], [0,-0.7745966692414833770358530799564799221665,0.7745966692414833770358530799564799221665], [ -0.3399810435848562648026657591032446872005,0.3399810435848562648026657591032446872005,-0.8611363115940525752239464888928095050957,0.8611363115940525752239464888928095050957], [0,-0.5384693101056830910363144207002088049672,0.5384693101056830910363144207002088049672,-0.9061798459386639927976268782993929651256,0.9061798459386639927976268782993929651256], [ 0.6612093864662645136613995950199053470064,-0.6612093864662645136613995950199053470064,-0.2386191860831969086305017216807119354186,0.2386191860831969086305017216807119354186,-0.9324695142031520278123015544939946091347,0.9324695142031520278123015544939946091347], [0, 0.4058451513773971669066064120769614633473,-0.4058451513773971669066064120769614633473,-0.7415311855993944398638647732807884070741,0.7415311855993944398638647732807884070741,-0.9491079123427585245261896840478512624007,0.9491079123427585245261896840478512624007], [ -0.1834346424956498049394761423601839806667,0.1834346424956498049394761423601839806667,-0.5255324099163289858177390491892463490419,0.5255324099163289858177390491892463490419,-0.7966664774136267395915539364758304368371,0.7966664774136267395915539364758304368371,-0.9602898564975362316835608685694729904282,0.9602898564975362316835608685694729904282], [0,-0.8360311073266357942994297880697348765441,0.8360311073266357942994297880697348765441,-0.9681602395076260898355762029036728700494,0.9681602395076260898355762029036728700494,-0.3242534234038089290385380146433366085719,0.3242534234038089290385380146433366085719,-0.6133714327005903973087020393414741847857,0.6133714327005903973087020393414741847857], [ -0.1488743389816312108848260011297199846175,0.1488743389816312108848260011297199846175,-0.4333953941292471907992659431657841622000,0.4333953941292471907992659431657841622000,-0.6794095682990244062343273651148735757692,0.6794095682990244062343273651148735757692,-0.8650633666889845107320966884234930485275,0.8650633666889845107320966884234930485275,-0.9739065285171717200779640120844520534282,0.9739065285171717200779640120844520534282], [0,-0.2695431559523449723315319854008615246796,0.2695431559523449723315319854008615246796,-0.5190961292068118159257256694586095544802,0.5190961292068118159257256694586095544802,-0.7301520055740493240934162520311534580496,0.7301520055740493240934162520311534580496,-0.8870625997680952990751577693039272666316,0.8870625997680952990751577693039272666316,-0.9782286581460569928039380011228573907714,0.9782286581460569928039380011228573907714], [ -0.1252334085114689154724413694638531299833,0.1252334085114689154724413694638531299833,-0.3678314989981801937526915366437175612563,0.3678314989981801937526915366437175612563,-0.5873179542866174472967024189405342803690,0.5873179542866174472967024189405342803690,-0.7699026741943046870368938332128180759849,0.7699026741943046870368938332128180759849,-0.9041172563704748566784658661190961925375,0.9041172563704748566784658661190961925375,-0.9815606342467192506905490901492808229601,0.9815606342467192506905490901492808229601], [0,-0.2304583159551347940655281210979888352115,0.2304583159551347940655281210979888352115,-0.4484927510364468528779128521276398678019,0.4484927510364468528779128521276398678019,-0.6423493394403402206439846069955156500716,0.6423493394403402206439846069955156500716,-0.8015780907333099127942064895828598903056,0.8015780907333099127942064895828598903056,-0.9175983992229779652065478365007195123904,0.9175983992229779652065478365007195123904,-0.9841830547185881494728294488071096110649,0.9841830547185881494728294488071096110649], [ -0.1080549487073436620662446502198347476119,0.1080549487073436620662446502198347476119,-0.3191123689278897604356718241684754668342,0.3191123689278897604356718241684754668342,-0.5152486363581540919652907185511886623088,0.5152486363581540919652907185511886623088,-0.6872929048116854701480198030193341375384,0.6872929048116854701480198030193341375384,-0.8272013150697649931897947426503949610397,0.8272013150697649931897947426503949610397,-0.9284348836635735173363911393778742644770,0.9284348836635735173363911393778742644770,-0.9862838086968123388415972667040528016760,0.9862838086968123388415972667040528016760], [0,-0.2011940939974345223006283033945962078128,0.2011940939974345223006283033945962078128,-0.3941513470775633698972073709810454683627,0.3941513470775633698972073709810454683627,-0.5709721726085388475372267372539106412383,0.5709721726085388475372267372539106412383,-0.7244177313601700474161860546139380096308,0.7244177313601700474161860546139380096308,-0.8482065834104272162006483207742168513662,0.8482065834104272162006483207742168513662,-0.9372733924007059043077589477102094712439,0.9372733924007059043077589477102094712439,-0.9879925180204854284895657185866125811469,0.9879925180204854284895657185866125811469], [ -0.0950125098376374401853193354249580631303,0.0950125098376374401853193354249580631303,-0.2816035507792589132304605014604961064860,0.2816035507792589132304605014604961064860,-0.4580167776572273863424194429835775735400,0.4580167776572273863424194429835775735400,-0.6178762444026437484466717640487910189918,0.6178762444026437484466717640487910189918,-0.7554044083550030338951011948474422683538,0.7554044083550030338951011948474422683538,-0.8656312023878317438804678977123931323873,0.8656312023878317438804678977123931323873,-0.9445750230732325760779884155346083450911,0.9445750230732325760779884155346083450911,-0.9894009349916499325961541734503326274262,0.9894009349916499325961541734503326274262], [0,-0.1784841814958478558506774936540655574754,0.1784841814958478558506774936540655574754,-0.3512317634538763152971855170953460050405,0.3512317634538763152971855170953460050405,-0.5126905370864769678862465686295518745829,0.5126905370864769678862465686295518745829,-0.6576711592166907658503022166430023351478,0.6576711592166907658503022166430023351478,-0.7815140038968014069252300555204760502239,0.7815140038968014069252300555204760502239,-0.8802391537269859021229556944881556926234,0.8802391537269859021229556944881556926234,-0.9506755217687677612227169578958030214433,0.9506755217687677612227169578958030214433,-0.9905754753144173356754340199406652765077,0.9905754753144173356754340199406652765077], [ -0.0847750130417353012422618529357838117333,0.0847750130417353012422618529357838117333,-0.2518862256915055095889728548779112301628,0.2518862256915055095889728548779112301628,-0.4117511614628426460359317938330516370789,0.4117511614628426460359317938330516370789,-0.5597708310739475346078715485253291369276,0.5597708310739475346078715485253291369276,-0.6916870430603532078748910812888483894522,0.6916870430603532078748910812888483894522,-0.8037049589725231156824174550145907971032,0.8037049589725231156824174550145907971032,-0.8926024664975557392060605911271455154078,0.8926024664975557392060605911271455154078,-0.9558239495713977551811958929297763099728,0.9558239495713977551811958929297763099728,-0.9915651684209309467300160047061507702525,0.9915651684209309467300160047061507702525], [0,-0.1603586456402253758680961157407435495048,0.1603586456402253758680961157407435495048,-0.3165640999636298319901173288498449178922,0.3165640999636298319901173288498449178922,-0.4645707413759609457172671481041023679762,0.4645707413759609457172671481041023679762,-0.6005453046616810234696381649462392798683,0.6005453046616810234696381649462392798683,-0.7209661773352293786170958608237816296571,0.7209661773352293786170958608237816296571,-0.8227146565371428249789224867127139017745,0.8227146565371428249789224867127139017745,-0.9031559036148179016426609285323124878093,0.9031559036148179016426609285323124878093,-0.9602081521348300308527788406876515266150,0.9602081521348300308527788406876515266150,-0.9924068438435844031890176702532604935893,0.9924068438435844031890176702532604935893], [ -0.0765265211334973337546404093988382110047,0.0765265211334973337546404093988382110047,-0.2277858511416450780804961953685746247430,0.2277858511416450780804961953685746247430,-0.3737060887154195606725481770249272373957,0.3737060887154195606725481770249272373957,-0.5108670019508270980043640509552509984254,0.5108670019508270980043640509552509984254,-0.6360536807265150254528366962262859367433,0.6360536807265150254528366962262859367433,-0.7463319064601507926143050703556415903107,0.7463319064601507926143050703556415903107,-0.8391169718222188233945290617015206853296,0.8391169718222188233945290617015206853296,-0.9122344282513259058677524412032981130491,0.9122344282513259058677524412032981130491,-0.9639719272779137912676661311972772219120,0.9639719272779137912676661311972772219120,-0.9931285991850949247861223884713202782226,0.9931285991850949247861223884713202782226], [0,-0.1455618541608950909370309823386863301163,0.1455618541608950909370309823386863301163,-0.2880213168024010966007925160646003199090,0.2880213168024010966007925160646003199090,-0.4243421202074387835736688885437880520964,0.4243421202074387835736688885437880520964,-0.5516188358872198070590187967243132866220,0.5516188358872198070590187967243132866220,-0.6671388041974123193059666699903391625970,0.6671388041974123193059666699903391625970,-0.7684399634756779086158778513062280348209,0.7684399634756779086158778513062280348209,-0.8533633645833172836472506385875676702761,0.8533633645833172836472506385875676702761,-0.9200993341504008287901871337149688941591,0.9200993341504008287901871337149688941591,-0.9672268385663062943166222149076951614246,0.9672268385663062943166222149076951614246,-0.9937521706203895002602420359379409291933,0.9937521706203895002602420359379409291933], [ -0.0697392733197222212138417961186280818222,0.0697392733197222212138417961186280818222,-0.2078604266882212854788465339195457342156,0.2078604266882212854788465339195457342156,-0.3419358208920842251581474204273796195591,0.3419358208920842251581474204273796195591,-0.4693558379867570264063307109664063460953,0.4693558379867570264063307109664063460953,-0.5876404035069115929588769276386473488776,0.5876404035069115929588769276386473488776,-0.6944872631866827800506898357622567712673,0.6944872631866827800506898357622567712673,-0.7878168059792081620042779554083515213881,0.7878168059792081620042779554083515213881,-0.8658125777203001365364256370193787290847,0.8658125777203001365364256370193787290847,-0.9269567721871740005206929392590531966353,0.9269567721871740005206929392590531966353,-0.9700604978354287271239509867652687108059,0.9700604978354287271239509867652687108059,-0.9942945854823992920730314211612989803930,0.9942945854823992920730314211612989803930], [0,-0.1332568242984661109317426822417661370104,0.1332568242984661109317426822417661370104,-0.2641356809703449305338695382833096029790,0.2641356809703449305338695382833096029790,-0.3903010380302908314214888728806054585780,0.3903010380302908314214888728806054585780,-0.5095014778460075496897930478668464305448,0.5095014778460075496897930478668464305448,-0.6196098757636461563850973116495956533871,0.6196098757636461563850973116495956533871,-0.7186613631319501944616244837486188483299,0.7186613631319501944616244837486188483299,-0.8048884016188398921511184069967785579414,0.8048884016188398921511184069967785579414,-0.8767523582704416673781568859341456716389,0.8767523582704416673781568859341456716389,-0.9329710868260161023491969890384229782357,0.9329710868260161023491969890384229782357,-0.9725424712181152319560240768207773751816,0.9725424712181152319560240768207773751816,-0.9947693349975521235239257154455743605736,0.9947693349975521235239257154455743605736], [ -0.0640568928626056260850430826247450385909,0.0640568928626056260850430826247450385909,-0.1911188674736163091586398207570696318404,0.1911188674736163091586398207570696318404,-0.3150426796961633743867932913198102407864,0.3150426796961633743867932913198102407864,-0.4337935076260451384870842319133497124524,0.4337935076260451384870842319133497124524,-0.5454214713888395356583756172183723700107,0.5454214713888395356583756172183723700107,-0.6480936519369755692524957869107476266696,0.6480936519369755692524957869107476266696,-0.7401241915785543642438281030999784255232,0.7401241915785543642438281030999784255232,-0.8200019859739029219539498726697452080761,0.8200019859739029219539498726697452080761,-0.8864155270044010342131543419821967550873,0.8864155270044010342131543419821967550873,-0.9382745520027327585236490017087214496548,0.9382745520027327585236490017087214496548,-0.9747285559713094981983919930081690617411,0.9747285559713094981983919930081690617411,-0.9951872199970213601799974097007368118745,0.9951872199970213601799974097007368118745] ];
var Cval = [[],[],[1.0,1.0],[0.8888888888888888888888888888888888888888,0.5555555555555555555555555555555555555555,0.5555555555555555555555555555555555555555],[0.6521451548625461426269360507780005927646,0.6521451548625461426269360507780005927646,0.3478548451374538573730639492219994072353,0.3478548451374538573730639492219994072353],   [0.5688888888888888888888888888888888888888,0.4786286704993664680412915148356381929122,0.4786286704993664680412915148356381929122,0.2369268850561890875142640407199173626432,0.2369268850561890875142640407199173626432], [0.3607615730481386075698335138377161116615,0.3607615730481386075698335138377161116615,0.4679139345726910473898703439895509948116,0.4679139345726910473898703439895509948116,0.1713244923791703450402961421727328935268,0.1713244923791703450402961421727328935268], [0.4179591836734693877551020408163265306122,0.3818300505051189449503697754889751338783,0.3818300505051189449503697754889751338783,0.2797053914892766679014677714237795824869,0.2797053914892766679014677714237795824869,0.1294849661688696932706114326790820183285,0.1294849661688696932706114326790820183285], [0.3626837833783619829651504492771956121941,0.3626837833783619829651504492771956121941,0.3137066458778872873379622019866013132603,0.3137066458778872873379622019866013132603,0.2223810344533744705443559944262408844301,0.2223810344533744705443559944262408844301,0.1012285362903762591525313543099621901153,0.1012285362903762591525313543099621901153], [0.3302393550012597631645250692869740488788,0.1806481606948574040584720312429128095143,0.1806481606948574040584720312429128095143,0.0812743883615744119718921581105236506756,0.0812743883615744119718921581105236506756,0.3123470770400028400686304065844436655987,0.3123470770400028400686304065844436655987,0.2606106964029354623187428694186328497718,0.2606106964029354623187428694186328497718], [0.2955242247147528701738929946513383294210,0.2955242247147528701738929946513383294210,0.2692667193099963550912269215694693528597,0.2692667193099963550912269215694693528597,0.2190863625159820439955349342281631924587,0.2190863625159820439955349342281631924587,0.1494513491505805931457763396576973324025,0.1494513491505805931457763396576973324025,0.0666713443086881375935688098933317928578,0.0666713443086881375935688098933317928578], [0.2729250867779006307144835283363421891560,0.2628045445102466621806888698905091953727,0.2628045445102466621806888698905091953727,0.2331937645919904799185237048431751394317,0.2331937645919904799185237048431751394317,0.1862902109277342514260976414316558916912,0.1862902109277342514260976414316558916912,0.1255803694649046246346942992239401001976,0.1255803694649046246346942992239401001976,0.0556685671161736664827537204425485787285,0.0556685671161736664827537204425485787285], [0.2491470458134027850005624360429512108304,0.2491470458134027850005624360429512108304,0.2334925365383548087608498989248780562594,0.2334925365383548087608498989248780562594,0.2031674267230659217490644558097983765065,0.2031674267230659217490644558097983765065,0.1600783285433462263346525295433590718720,0.1600783285433462263346525295433590718720,0.1069393259953184309602547181939962242145,0.1069393259953184309602547181939962242145,0.0471753363865118271946159614850170603170,0.0471753363865118271946159614850170603170], [0.2325515532308739101945895152688359481566,0.2262831802628972384120901860397766184347,0.2262831802628972384120901860397766184347,0.2078160475368885023125232193060527633865,0.2078160475368885023125232193060527633865,0.1781459807619457382800466919960979955128,0.1781459807619457382800466919960979955128,0.1388735102197872384636017768688714676218,0.1388735102197872384636017768688714676218,0.0921214998377284479144217759537971209236,0.0921214998377284479144217759537971209236,0.0404840047653158795200215922009860600419,0.0404840047653158795200215922009860600419], [0.2152638534631577901958764433162600352749,0.2152638534631577901958764433162600352749,0.2051984637212956039659240656612180557103,0.2051984637212956039659240656612180557103,0.1855383974779378137417165901251570362489,0.1855383974779378137417165901251570362489,0.1572031671581935345696019386238421566056,0.1572031671581935345696019386238421566056,0.1215185706879031846894148090724766259566,0.1215185706879031846894148090724766259566,0.0801580871597602098056332770628543095836,0.0801580871597602098056332770628543095836,0.0351194603317518630318328761381917806197,0.0351194603317518630318328761381917806197], [0.2025782419255612728806201999675193148386,0.1984314853271115764561183264438393248186,0.1984314853271115764561183264438393248186,0.1861610000155622110268005618664228245062,0.1861610000155622110268005618664228245062,0.1662692058169939335532008604812088111309,0.1662692058169939335532008604812088111309,0.1395706779261543144478047945110283225208,0.1395706779261543144478047945110283225208,0.1071592204671719350118695466858693034155,0.1071592204671719350118695466858693034155,0.0703660474881081247092674164506673384667,0.0703660474881081247092674164506673384667,0.0307532419961172683546283935772044177217,0.0307532419961172683546283935772044177217], [0.1894506104550684962853967232082831051469,0.1894506104550684962853967232082831051469,0.1826034150449235888667636679692199393835,0.1826034150449235888667636679692199393835,0.1691565193950025381893120790303599622116,0.1691565193950025381893120790303599622116,0.1495959888165767320815017305474785489704,0.1495959888165767320815017305474785489704,0.1246289712555338720524762821920164201448,0.1246289712555338720524762821920164201448,0.0951585116824927848099251076022462263552,0.0951585116824927848099251076022462263552,0.0622535239386478928628438369943776942749,0.0622535239386478928628438369943776942749,0.0271524594117540948517805724560181035122,0.0271524594117540948517805724560181035122], [0.1794464703562065254582656442618856214487,0.1765627053669926463252709901131972391509,0.1765627053669926463252709901131972391509,0.1680041021564500445099706637883231550211,0.1680041021564500445099706637883231550211,0.1540457610768102880814315948019586119404,0.1540457610768102880814315948019586119404,0.1351363684685254732863199817023501973721,0.1351363684685254732863199817023501973721,0.1118838471934039710947883856263559267358,0.1118838471934039710947883856263559267358,0.0850361483171791808835353701910620738504,0.0850361483171791808835353701910620738504,0.0554595293739872011294401653582446605128,0.0554595293739872011294401653582446605128,0.0241483028685479319601100262875653246916,0.0241483028685479319601100262875653246916], [0.1691423829631435918406564701349866103341,0.1691423829631435918406564701349866103341,0.1642764837458327229860537764659275904123,0.1642764837458327229860537764659275904123,0.1546846751262652449254180038363747721932,0.1546846751262652449254180038363747721932,0.1406429146706506512047313037519472280955,0.1406429146706506512047313037519472280955,0.1225552067114784601845191268002015552281,0.1225552067114784601845191268002015552281,0.1009420441062871655628139849248346070628,0.1009420441062871655628139849248346070628,0.0764257302548890565291296776166365256053,0.0764257302548890565291296776166365256053,0.0497145488949697964533349462026386416808,0.0497145488949697964533349462026386416808,0.0216160135264833103133427102664524693876,0.0216160135264833103133427102664524693876], [0.1610544498487836959791636253209167350399,0.1589688433939543476499564394650472016787,0.1589688433939543476499564394650472016787,0.1527660420658596667788554008976629984610,0.1527660420658596667788554008976629984610,0.1426067021736066117757461094419029724756,0.1426067021736066117757461094419029724756,0.1287539625393362276755157848568771170558,0.1287539625393362276755157848568771170558,0.1115666455473339947160239016817659974813,0.1115666455473339947160239016817659974813,0.0914900216224499994644620941238396526609,0.0914900216224499994644620941238396526609,0.0690445427376412265807082580060130449618,0.0690445427376412265807082580060130449618,0.0448142267656996003328381574019942119517,0.0448142267656996003328381574019942119517,0.0194617882297264770363120414644384357529,0.0194617882297264770363120414644384357529], [0.1527533871307258506980843319550975934919,0.1527533871307258506980843319550975934919,0.1491729864726037467878287370019694366926,0.1491729864726037467878287370019694366926,0.1420961093183820513292983250671649330345,0.1420961093183820513292983250671649330345,0.1316886384491766268984944997481631349161,0.1316886384491766268984944997481631349161,0.1181945319615184173123773777113822870050,0.1181945319615184173123773777113822870050,0.1019301198172404350367501354803498761666,0.1019301198172404350367501354803498761666,0.0832767415767047487247581432220462061001,0.0832767415767047487247581432220462061001,0.0626720483341090635695065351870416063516,0.0626720483341090635695065351870416063516,0.0406014298003869413310399522749321098790,0.0406014298003869413310399522749321098790,0.0176140071391521183118619623518528163621,0.0176140071391521183118619623518528163621], [0.1460811336496904271919851476833711882448,0.1445244039899700590638271665537525436099,0.1445244039899700590638271665537525436099,0.1398873947910731547221334238675831108927,0.1398873947910731547221334238675831108927,0.1322689386333374617810525744967756043290,0.1322689386333374617810525744967756043290,0.1218314160537285341953671771257335983563,0.1218314160537285341953671771257335983563,0.1087972991671483776634745780701056420336,0.1087972991671483776634745780701056420336,0.0934444234560338615532897411139320884835,0.0934444234560338615532897411139320884835,0.0761001136283793020170516533001831792261,0.0761001136283793020170516533001831792261,0.0571344254268572082836358264724479574912,0.0571344254268572082836358264724479574912,0.0369537897708524937999506682993296661889,0.0369537897708524937999506682993296661889,0.0160172282577743333242246168584710152658,0.0160172282577743333242246168584710152658], [0.1392518728556319933754102483418099578739,0.1392518728556319933754102483418099578739,0.1365414983460151713525738312315173965863,0.1365414983460151713525738312315173965863,0.1311735047870623707329649925303074458757,0.1311735047870623707329649925303074458757,0.1232523768105124242855609861548144719594,0.1232523768105124242855609861548144719594,0.1129322960805392183934006074217843191142,0.1129322960805392183934006074217843191142,0.1004141444428809649320788378305362823508,0.1004141444428809649320788378305362823508,0.0859416062170677274144436813727028661891,0.0859416062170677274144436813727028661891,0.0697964684245204880949614189302176573987,0.0697964684245204880949614189302176573987,0.0522933351526832859403120512732112561121,0.0522933351526832859403120512732112561121,0.0337749015848141547933022468659129013491,0.0337749015848141547933022468659129013491,0.0146279952982722006849910980471854451902,0.0146279952982722006849910980471854451902], [0.1336545721861061753514571105458443385831,0.1324620394046966173716424647033169258050,0.1324620394046966173716424647033169258050,0.1289057221880821499785953393997936532597,0.1289057221880821499785953393997936532597,0.1230490843067295304675784006720096548158,0.1230490843067295304675784006720096548158,0.1149966402224113649416435129339613014914,0.1149966402224113649416435129339613014914,0.1048920914645414100740861850147438548584,0.1048920914645414100740861850147438548584,0.0929157660600351474770186173697646486034,0.0929157660600351474770186173697646486034,0.0792814117767189549228925247420432269137,0.0792814117767189549228925247420432269137,0.0642324214085258521271696151589109980391,0.0642324214085258521271696151589109980391,0.0480376717310846685716410716320339965612,0.0480376717310846685716410716320339965612,0.0309880058569794443106942196418845053837,0.0309880058569794443106942196418845053837,0.0134118594871417720813094934586150649766,0.0134118594871417720813094934586150649766], [0.1279381953467521569740561652246953718517,0.1279381953467521569740561652246953718517,0.1258374563468282961213753825111836887264,0.1258374563468282961213753825111836887264,0.1216704729278033912044631534762624256070,0.1216704729278033912044631534762624256070,0.1155056680537256013533444839067835598622,0.1155056680537256013533444839067835598622,0.1074442701159656347825773424466062227946,0.1074442701159656347825773424466062227946,0.0976186521041138882698806644642471544279,0.0976186521041138882698806644642471544279,0.0861901615319532759171852029837426671850,0.0861901615319532759171852029837426671850,0.0733464814110803057340336152531165181193,0.0733464814110803057340336152531165181193,0.0592985849154367807463677585001085845412,0.0592985849154367807463677585001085845412,0.0442774388174198061686027482113382288593,0.0442774388174198061686027482113382288593,0.0285313886289336631813078159518782864491,0.0285313886289336631813078159518782864491,0.0123412297999871995468056670700372915759,0.0123412297999871995468056670700372915759] ];















var print_r = function (obj, t) {

    // define tab spacing
    var tab = t || '';

    // check if it's array
    var isArr = Object.prototype.toString.call(obj) === '[object Array]';

    // use {} for object, [] for array
    var str = isArr ? ('Array\n\r' + tab + '[\n\r') : ('Object\n\r' + tab + '{\n\r');

    // walk through it's properties
    for (var prop in obj) {
    	if (obj.hasOwnProperty(prop)) {
    		var val1 = obj[prop];
    		var val2 = '';
    		var type = Object.prototype.toString.call(val1);
    		switch (type) {

                // recursive if object/array
                case '[object Array]':
                case '[object Object]':
                val2 = print_r(val1, (tab + '\t'));
                break;

                case '[object String]':
                val2 = '\'' + val1 + '\'';
                break;

                default:
                val2 = val1;
            }
            str += tab + '\t' + prop + ' => ' + val2 + ',\n\r';
        }
    }

    // remove extra comma for last property
    str = str.substring(0, str.length - 2) + '\n\r' + tab;

    return isArr ? (str + ']') : (str + '}');
};
var logCount = 1;
var log = function(text) {
	if($j('#log').size()) {
		if(typeof text === 'object' || typeof text === 'array') {
			text = print_r(text);
		}
		$j('#log').text("" + $j('#log').html() + '\n\r' + logCount + ': ' + text);
		$j('#log')[0].scrollTop = $j('#log')[0].scrollHeight;
		logCount ++;
	}
};

function getParameterByName(name) {
	var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
	return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}
