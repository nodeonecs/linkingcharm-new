<?php
/**
* Brainvire Infotech Pvt. Ltd
  * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */


class Brainvire_Points_Model_Coupon extends Mage_Core_Model_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init('points/coupon');
    }

    
    public function LoadByCouponCode($couponCode) {
        $this->_getResource()->LoadByCouponCode($this, $couponCode);
        return $this;
    }

    

    public function isExpired() {
        $toDate = ($this->getData('to_date')) ? $this->getData('to_date') : '2112-01-01';
        $today = Mage::app()->getLocale()->date();
        $today = $today->toString(Varien_Date::DATE_INTERNAL_FORMAT);
        if (strtotime($today) > strtotime($toDate)) {
            return TRUE;
        }
        return FALSE;
    }


    public function isStarted() {

        $fromDate = ($this->getData('from_date')) ? $this->getData('from_date') : '2000-01-01';
        $today = Mage::app()->getLocale()->date();
        $today = $today->toString(Varien_Date::DATE_INTERNAL_FORMAT);
        if (strtotime($today) >= strtotime($fromDate)) {
            return TRUE;
        }
        return FALSE;
    }

    

    public function validateCustomerGroup($customer) {
        if (!$this->getId() || !$customer->getId()) {
            return FALSE;
        }

        $customerGroupId = $customer->getData('group_id');
        $couponGroupIds = $this->getData('customer_group_ids');
        $result = in_array($customerGroupId, $couponGroupIds);
        return $result;
    }

   

    public function validateWebsite() {

        $websiteId = Mage::app()->getStore()->getWebsiteId();
        $couponWebsiteIds = $this->getData('website_ids');

        $result = in_array($websiteId, $couponWebsiteIds);
        return $result;
    }

    public function activate() {

        $activationCnt = (int) $this->getData('activation_cnt') + 1;
        $this
                ->setData('activation_cnt', $activationCnt)
                ->save();
    }

}