<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   BV
 * @package    BV_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */


class Brainvire_Points_Block_Adminhtml_Customer_Edit_Tabs_RewardPoints extends Mage_Adminhtml_Block_Template implements Mage_Adminhtml_Block_Widget_Tab_Interface {

    
    public function getTabLabel() {
        return Mage::helper('points')->__("Reward points");
    }

    
    public function getTabTitle() {
        return Mage::helper('points')->__("Reward points");
    }

    public function canShowTab() {
        if (Mage::registry('current_customer')->getId()) {
            return true;
        }
        return false;
    }

    public function isHidden() {
        if (Mage::registry('current_customer')->getId()) {
            return false;
        }
        return true;
    }

}

?>
