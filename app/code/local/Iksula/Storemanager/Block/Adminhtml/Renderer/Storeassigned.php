<?php
class Iksula_Storemanager_Block_Adminhtml_Renderer_Storeassigned extends Varien_Data_Form_Element_Abstract
{


    public function getElementHtml()
    {

        //return 'test';
        $store_id = Mage::app()->getRequest()->getParam('id');

        if(isset($store_id)){

            $aStoreCollection = Mage::getModel('iksula_storemanager/storehubrelation')->getCollection()->addFieldToFilter('hub_id' , array('in' => array('0' , $store_id)))->addFieldToFilter('is_hub' , array('eq' => '0'))->addFieldToSelect(array('store_id' , 'website_id'));

            $oCurrentStoreAssignedStores = Mage::getModel('iksula_storemanager/storehubrelation')->getCollection()->addFieldToFilter('hub_id' , array('eq' => $store_id))->addFieldToFilter('is_hub' , array('eq' => '0'))->addFieldToSelect(array('store_id'));

            foreach($oCurrentStoreAssignedStores as $CurrentStores){

            $aCurrentStoreAssignedStores [] = $CurrentStores->getStoreId();

        }


        }else{

            $aStoreCollection = Mage::getModel('iksula_storemanager/storehubrelation')->getCollection()->addFieldToFilter('hub_id' , array('in' => array('0')))->addFieldToFilter('is_hub' , array('eq' => '0'))->addFieldToSelect(array('store_id' , 'website_id'));

            $aCurrentStoreAssignedStores = array();

        }



        foreach($aStoreCollection as $storeData){

            $aStoresUnassigned [] = $storeData->getStoreId();

        }

            //$sStoresUnassigned = implode(',' , $aStoresUnassigned);
        /*echo '<pre>';
        print_r($aStoresUnassigned);
        exit;*/


             $aStoreUnassignedMainCollection = Mage::getModel('core/store_group')->getCollection()->addFieldToFilter('group_id', array('in'=>$aStoresUnassigned))->addFieldToSelect(array('name' , 'website_id'));




        $aStoreUnassignedMainCollection = Mage::getModel('core/store_group')->getCollection()->addFieldToFilter('group_id', array('in'=>$aStoresUnassigned))->addFieldToSelect(array('group_id' , 'name' , 'website_id'));
        $aStoreTotalData = array();
        foreach($aStoreUnassignedMainCollection as $aTotalStore){

            $aStoreTotalData [$aTotalStore->getName()] = $aTotalStore->getGroupId()."|".$aTotalStore->getWebsiteId();

        }

        $html = "<select id='store_store_assigned' name='store[store_assigned][]' class='select multiselect required-entry' size='10' multiple='multiple'>";
        $selected = "";
        foreach($aStoreTotalData as $Storename => $StoreId){

                $aStoreWebsite = explode('|' , $StoreId);
                if(in_array($aStoreWebsite[0] , $aCurrentStoreAssignedStores)){
                    $selected = "selected = 'selected'";
                }else{
                    $selected = "";
                }
            $html .= "<option ".$selected." cust_website_id ='".$aStoreWebsite[1]."' value='".$aStoreWebsite[0]."'>".$Storename."</option>";

        }

        $html .= "<select>";


        return $html ;
    }
    /**
     * @param Varien_Data_Tree_Node_Collection $nodeCollection
     */

}
