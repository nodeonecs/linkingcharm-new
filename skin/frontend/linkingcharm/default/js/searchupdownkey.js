jQuery(document).ready(function($){

	function listKeyDown() {

		if($('#search_autocomplete ul li.selected-li').length) {
			if($('#search_autocomplete ul li.selected-li').next('li').length) {
				var selector = $('#search_autocomplete ul li.selected-li').next('li');
				$('#search_autocomplete ul li').removeClass('selected-li');
				selector.addClass('selected-li');
				$('#search').val($('#search_autocomplete ul li.selected-li a').html().replace(/<\/?[^>]+(>|$)/g, ""));
			} else {
				$('#search_autocomplete ul li').removeClass('selected-li');
				$('#search').val(str);
			}
		} else {
			str = $('#search').val();
			$('#search_autocomplete ul li').first().addClass('selected-li');
			$('#search').val($('#search_autocomplete ul li.selected-li a').html().replace(/<\/?[^>]+(>|$)/g, ""));
		}
		//scrollCustomBox();
	}

	function listKeyUp() {
		if($('#search_autocomplete ul li.selected-li').length) {
			if($('#search_autocomplete ul li.selected-li').prev('li').length) {
				var selector = $('#search_autocomplete ul li.selected-li').prev('li');
				$('#search_autocomplete ul li').removeClass('selected-li');
				selector.addClass('selected-li');
				$('#search').val($('#search_autocomplete ul li.selected-li a').html().replace(/<\/?[^>]+(>|$)/g, ""));
			} else {
				$('#search_autocomplete ul li').removeClass('selected-li');
				$('#search').val(str);
			}
		} else {
			str = $('#search').val();
			$('#search_autocomplete ul li').last().addClass('selected-li');
			$('#search').val($('#search_autocomplete ul li.selected-li a').html().replace(/<\/?[^>]+(>|$)/g, ""));
		}
		//scrollCustomBox();
	}

	function redirectoProduct(event){
		if($('.selected-li').length > 0){
			var Producturl = $('.selected-li a').attr('href');
			window.location.href = Producturl;
			event.preventDefault();
			return false;
		}
	}


	function scrollCustomBox() {
		$("#search_autocomplete").mCustomScrollbar("scrollTo", ".selected-li");
	}

	$('#search').keydown(function(event){
		switch(event.which) {
			case 38:
				listKeyUp();
				break;
			case 40:
				listKeyDown();
				break;
			case 13:
				redirectoProduct(event);
			default:
		}
	});
});

// jQuery(window).keydown(function(event) {
// 		event.preventDefault();
// 		alert('here');
// 		alert(event.which + '@@' + event.target.id + '@@' + jQuery(".selected-li").length);
// 	if(event.which == 13 && event.target.id == 'search' && jQuery(".selected-li").length > 0) {
// 		alert('here');
// 		window.location.href = jQuery(".selected-li a").attr('href');
// 	}
// });
