<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Sorting
 */
class Amasty_Sorting_Helper_Data extends Mage_Core_Helper_Abstract 
{
    private $methodCodes = null;

    public function getMethods()
    {
//        $isSearch = in_array(Mage::app()->getRequest()->getModuleName(), array('sqli_singlesearchresult', 'catalogsearch')); 
//        if ($isSearch)
//            return array();
        
        
        // class names. order defines the position in the dropdown
        $methods = array(
            'new',    
            'saving',
            'bestselling',    
            'mostviewed',    
            'toprated',    
            'commented',    
            'wished',
            'qty',
            'profit',
            'revenue',
            'revenueview',
            'orderview',
        ); 

        return $methods;
    }

    public function getMethodModels()
    {
        if ($this->methodCodes === null) {
            $this->methodCodes = array();
            foreach ($this->getMethods() as $className) {
                $method = Mage::getSingleton('amsorting/method_' . $className);
                $this->methodCodes[$method->getCode()] = $method;
            }
        }

        return $this->methodCodes;
    }
}
