<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_Block_Adminhtml_Giftcard_Edit_Tab_Information extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getTabLabel()
    {
        return $this->__('Information');
    }

    public function getTabTitle()
    {
        return $this->__('Information');
    }

    public function canShowTab()
    {
        return true;
    }

    public function isHidden()
    {
        return false;
    }

    public function initForm()
    {
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('_info_');

        $giftcardModel = Mage::registry('current_giftcard');
        $fieldsetInformation = $form->addFieldset(
            'fieldsetInfo',
            array(
                'fieldset_container_id' => 'aw_gc2_giftcard_info',
                'legend' => $this->__('Gift Card Information')
            )
        );

        if ($giftcardModel->getId()) {
            $fieldsetInformation->addField('code', 'label', array(
                'name' => 'code',
                'label' => $this->__('Code'),
                'title' => $this->__('Code')
            ));

            $defaultStoreId = Mage::app()
                ->getWebsite($giftcardModel->getWebsiteId())
                ->getDefaultGroup()
                ->getDefaultStoreId();
            $initialBalanceWithCurrency = Mage::helper('core')->currencyByStore(
                $giftcardModel->getInitialBalance(),  $defaultStoreId, true, false
            );
            $giftcardModel->setInitialBalanceWithCurrency($initialBalanceWithCurrency);
            $fieldsetInformation->addField('initial_balance_with_currency', 'label', array(
                'name' => 'initial_balance_with_currency',
                'label' => $this->__('Initial Amount'),
                'title' => $this->__('Initial Amount'),
                'readonly' => true,
            ));

            $fieldsetInformation->addField('availability_text', 'label', array(
                'name' => 'availability_text',
                'label' => $this->__('Availability'),
                'title' => $this->__('Availability'),
            ));

            $fieldsetInformation->addField('is_used_text', 'label', array(
                'name' => 'is_used_text',
                'label' => $this->__('Is Used'),
                'title' => $this->__('Is Used'),
            ));

            $fieldsetInformation->addField('expire_at', 'date', array(
                'name' => 'expire_at',
                'label' => $this->__('Expiration Date'),
                'title' => $this->__('Expiration Date'),
                'image' => $this->getSkinUrl('images/grid-cal.gif'),
                'format' => Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT)
            ));

            $createdAt = new Zend_Date($giftcardModel->getCreatedAt(), Varien_Date::DATE_INTERNAL_FORMAT);
            $giftcardModel->setCreatedAtLocale(Mage::helper('core')->formatDate($createdAt));
            $fieldsetInformation->addField('created_at_locale', 'label', array(
                'name' => 'created_at_locale',
                'label' => $this->__('Created At'),
                'title' => $this->__('Created At'),
            ));

            $websiteName =  Mage::getSingleton('adminhtml/system_store')->getWebsiteName($giftcardModel->getWebsiteId());
            $giftcardModel->setWebsiteName($websiteName);
            $fieldsetInformation->addField('website_name', 'label', array(
                'name'      => 'website_name',
                'label'     => $this->__('Website'),
                'title'     => $this->__('Website'),
            ));
        } else {
            $fieldsetInformation->addField('initial_balance', 'text', array(
                'label'     => $this->__('Initial Amount'),
                'title'     => $this->__('Initial Amount'),
                'name'      => 'initial_balance',
                'class'     => 'validate-greater-than-zero validate-number',
                'required'  => true,
                'note'      => '<div id="balance_currency"></div>'
            ));

            $expireAfter = $this->helper('aw_giftcard2/config')->getExpireValue();
            $giftcardModel->setExpireAfter($expireAfter > 0 ? $expireAfter : '');
            $fieldsetInformation->addField('expire_after', 'text', array(
                'label'     => $this->__('Expires After, days'),
                'title'     => $this->__('Expires After, days'),
                'name'      => 'expire_after',
                'class'     => 'validate-zero-or-greater validate-number',
            ));

            $fieldsetInformation->addField('website_id', 'select', array(
                'name'      => 'website_id',
                'label'     => $this->__('Website'),
                'title'     => $this->__('Website'),
                'required'  => true,
                'values'    => Mage::getSingleton('adminhtml/system_store')->getWebsiteValuesForForm(true),
            ));
        }


        if ($giftcardModel->getOrderId()){
            $fieldsetOrderInformation = $form->addFieldset(
                'fieldsetOrderInfo',
                array(
                    'fieldset_container_id' => 'aw_gc2_order_info',
                    'legend' => $this->__('Order Information')
                )
            );
            if ($giftcardModel->getProductId()) {
                $afterProductLinkText = $giftcardModel->getTypeText();
                $product = Mage::getModel('catalog/product')->load($giftcardModel->getProductId());
                if ($product->getId()) {
                    $fieldsetOrderInformation->addField(
                        'product_name',
                        'link',
                        array(
                            'href'  => $this->getUrl('adminhtml/catalog_product/edit', array('id' => $giftcardModel->getProductId())),
                            'after_element_html' => "({$afterProductLinkText})",
                            'label' => $this->__('Product'),
                            'title' => $this->__('Product'),
                        )
                    );
                    $giftcardModel->setProductName($product->getName());
                } else {
                    $fieldsetOrderInformation->addField(
                        'product_name',
                        'label',
                        array(
                            'label' => $this->__('Product'),
                            'title' => $this->__('Product'),
                        )
                    );
                    $giftcardModel->setProductName('');
                }


            }

            $order = Mage::getModel('sales/order')->load($giftcardModel->getOrderId());
            if ($order->getId()) {
                $fieldsetOrderInformation->addField(
                    'order_increment_id',
                    'link',
                    array(
                        'href'  => $this->getUrl('adminhtml/sales_order/view', array('order_id' => $order->getId())),
                        'label' => $this->__('Order'),
                        'title' => $this->__('Order')
                    )
                );
                $giftcardModel->setOrderIncrementId('#' . $order->getIncrementId());

                if ($order->getCustomerId()) {
                    $fieldsetOrderInformation->addField(
                        'order_customer',
                        'link',
                        array(
                            'href'  => $this->getUrl('adminhtml/customer/edit', array('id' => $order->getCustomerId())),
                            'label' => $this->__('Customer Name'),
                            'title' => $this->__('Customer Name')
                        )

                    );
                    $giftcardModel->setOrderCustomer($order->getCustomerName());
                } else {
                    $fieldsetOrderInformation->addField(
                        'order_customer_name',
                        'label',
                        array (
                            'label' => $this->__('Customer Name'),
                            'title' => $this->__('Customer Name')
                        )
                    );
                    $giftcardModel->setOrderCustomerName($order->getBillingAddress()->getName());

                    $fieldsetOrderInformation->addField(
                        'order_customer_email',
                        'label',
                        array(
                            'label' => $this->__('Customer Email'),
                            'title' => $this->__('Customer Email')
                        )
                    );
                    $giftcardModel->setOrderCustomerEmail($order->getCustomerEmail());
                }
            }
        } else {
            $fieldsetSenderDetails = $form->addFieldset(
                'fieldsetSender',
                array(
                    'fieldset_container_id' => 'aw_gc2_sender_details',
                    'legend' => $this->__('Sender Details')
                )
            );

            $giftcardModel->setType(AW_Giftcard2_Model_Source_Entity_Attribute_Giftcard_Type::VIRTUAL_VALUE);
            $fieldsetSenderDetails->addField('type', 'hidden', array(
                'name'      => 'type',
            ));

            if (!$giftcardModel->getId()) {
                $giftcardModel->setSenderName(Mage::helper('aw_giftcard2/config')->getEmailSenderName());
                $giftcardModel->setSenderEmail(Mage::helper('aw_giftcard2/config')->getEmailSenderEmail());
            }

            $fieldsetSenderDetails->addField('sender_name', 'text', array(
                'name'      => 'sender_name',
                'label'     => $this->__('Sender Name'),
                'title'     => $this->__('Sender Name'),
                'required'  => true,
            ));

            $fieldsetSenderDetails->addField('sender_email', 'text', array(
                'name'      => 'sender_email',
                'label'     => $this->__('Sender Email'),
                'title'     => $this->__('Sender Email'),
                'required'  => !$giftcardModel->isPhysical(),
                'class'     => 'validate-email',
            ));
        }

        $fieldsetRecipientDetails = $form->addFieldset(
            'fieldsetRecipient',
            array(
                'fieldset_container_id' => 'aw_gc2_recipient_details',
                'legend' => $this->__('Recipient Details')
            )
        );

        $fieldsetRecipientDetails->addField('recipient_name', 'text', array(
            'name'      => 'recipient_name',
            'label'     => $this->__('Recipient Name'),
            'title'     => $this->__('Recipient Name'),
            'required'  => true,
        ));

        if (!$giftcardModel->getId() || $giftcardModel->getId() && !$giftcardModel->isPhysical()) {
            $fieldsetRecipientDetails->addField('recipient_email', 'text', array(
                'name' => 'recipient_email',
                'label' => $this->__('Recipient Email'),
                'title' => $this->__('Recipient Email'),
                'required' => true,
                'class' => 'validate-email',
            ));
        }

        if ($giftcardModel->getId()) {
            $website = Mage::app()->getWebsite($giftcardModel->getWebsiteId());
            $currencyCode = $website->getBaseCurrencyCode();
            $fieldsetRecipientDetails->addField('balance', 'text', array(
                'label' => $this->__('Balance'),
                'title' => $this->__('Balance'),
                'name' => 'balance',
                'class' => 'validate-greater-than-zero validate-number',
                'required' => true,
                'note' => '<div id="balance_currency"><b>['.$currencyCode.']</b></div>'
            ));
        }

        if ($giftcardModel->isPhysical()) {
            $fieldsetRecipientDetails->addField(
                'email_template',
                'hidden',
                array (
                    'name' => 'email_template',
                    'value' => AW_Giftcard2_Model_Source_Giftcard_Email_Template::DO_NOT_SEND_VALUE
                )
            );
        } else {
            $fieldsetRecipientDetails->addField('email_template', 'select', array(
                'label' => $this->__('Email Template'),
                'title' => $this->__('Email Template'),
                'name' => 'email_template',
                'options' => Mage::getModel('aw_giftcard2/source_giftcard_email_template')->toArray(),
            ));
        }

        $formData = $giftcardModel->getData();
        $form->setValues($formData);

        $this->setForm($form);
        return $this;
    }

    public function getCurrencyJson()
    {
        $result = array();
        $websites = Mage::getSingleton('adminhtml/system_store')->getWebsiteCollection();
        foreach ($websites as $id => $website) {
            $result[$id] = $website->getBaseCurrencyCode();
        }
        return Mage::helper('core')->jsonEncode($result);
    }

}