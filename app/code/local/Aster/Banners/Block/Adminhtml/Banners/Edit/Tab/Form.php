<?php
class Aster_Banners_Block_Adminhtml_Banners_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm()
	{

		$form = new Varien_Data_Form();
		$this->setForm($form);
		$fieldset = $form->addFieldset("banners_form", array("legend"=>Mage::helper("banners")->__("Item information")));


		$fieldset->addField("banner_title", "text", array(
			"label" => Mage::helper("banners")->__("Title"),					
			"class" => "required-entry",
			"required" => true,
			"name" => "banner_title",
		));

		$fieldset->addField('banner_image', 'image', array(
			'label' => Mage::helper('banners')->__('Image'),
			'name' => 'banner_image',
			'note' => '(*.jpg, *.png, *.gif)',
		));
		$fieldset->addField("banner_link", "text", array(
			"label" => Mage::helper("banners")->__("Link"),
			"name" => "banner_link",
		));

		$fieldset->addField("banner_content", "textarea", array(
			"label" => Mage::helper("banners")->__("Content"),
			"name" => "banner_content",
		));

		$fieldset->addField('banner_type', 'select', array(
			'label'     => Mage::helper('banners')->__('Type'),
			'values'   => Aster_Banners_Block_Adminhtml_Banners_Grid::getValueArray4(),
			'name' => 'banner_type',					
			"class" => "required-entry",
			"required" => true,
		));				
		$fieldset->addField('banner_status', 'select', array(
			'label'     => Mage::helper('banners')->__('Status'),
			'values'   => Aster_Banners_Block_Adminhtml_Banners_Grid::getValueArray5(),
			'name' => 'banner_status',					
			"class" => "required-entry",
			"required" => true,
		));
		$fieldset->addField("sort_order", "text", array(
			"label" => Mage::helper("banners")->__("Sort Order"),					
			"class" => "required-entry",
			"required" => true,
			"name" => "sort_order",
		));

		$dateFormatIso = Mage::app()->getLocale()->getDateTimeFormat(
			Mage_Core_Model_Locale::FORMAT_TYPE_SHORT
		);

		$fieldset->addField('start_date', 'date', array(
			'label'        => Mage::helper('banners')->__('Start Date'),
			'name'         => 'start_date',
			'time' => true,
			'image'        => $this->getSkinUrl('images/grid-cal.gif'),
			'format'       => $dateFormatIso
		));
		
		$dateFormatIso = Mage::app()->getLocale()->getDateTimeFormat(
			Mage_Core_Model_Locale::FORMAT_TYPE_SHORT
		);

		$fieldset->addField('end_date', 'date', array(
			'label'        => Mage::helper('banners')->__('End Date'),
			'name'         => 'end_date',
			'time' => true,
			'image'        => $this->getSkinUrl('images/grid-cal.gif'),
			'format'       => $dateFormatIso
		));

		// $fieldset->addField('store_id', 'multiselect', array(
		// 	'label'     => Mage::helper('banners')->__('Store'),
		// 	'values'   => Aster_Banners_Block_Adminhtml_Banners_Grid::getValueArray9(),
		// 	'name' => 'store_id',
		// ));

		if (Mage::getSingleton("adminhtml/session")->getBannersData())
		{
			$form->setValues(Mage::getSingleton("adminhtml/session")->getBannersData());
			Mage::getSingleton("adminhtml/session")->setBannersData(null);
		} 
		elseif(Mage::registry("banners_data")) {
			$form->setValues(Mage::registry("banners_data")->getData());
		}
		return parent::_prepareForm();
	}
}
