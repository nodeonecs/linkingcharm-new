var AWGC2ImageUploader = Class.create();
AWGC2ImageUploader.prototype = {
    initialize: function(config) {
        this.containerEl = config.imageContainer;
        this.imagePlaceholderClass = config.imagePlaceholderClass;
        this.imageDragoverClass = config.imageDragoverClass;
        this.loadingClass = config.loadingClass;
        this.imagePlaceHolderEl = this.containerEl.down(this.imagePlaceholderClass);
        this.uploadUrl = config.uploadUrl;
        this.secureFormKey = config.secureFormKey;
        this.template = new Template(config.imageTemplate);
        this.imageEl = config.imageValue;
        this.imageLoaded = config.imageLoaded;
        this.dontSupportFileTypeMessage = config.dontSupportFileTypeMessage;
        this.errorMessage = config.errorMessage;
        this.acceptFileTypes = /(\.|\/)(gif|jpe?g|png)$/i;

        this.init();
    },

    init: function() {
        var me = this;

        this.imagePlaceHolderEl.observe('dragover', function(e){
            me.imagePlaceHolderEl.addClassName(me.imageDragoverClass);
            e.dataTransfer.dropEffect = 'move';
            e.stop();
        });
        this.imagePlaceHolderEl.observe('dragleave', function(e){
            me.imagePlaceHolderEl.removeClassName(me.imageDragoverClass);
        });
        this.imagePlaceHolderEl.observe('dragend', function(e){
            me.imagePlaceHolderEl.removeClassName(me.imageDragoverClass);
        });
        this.imagePlaceHolderEl.observe('drop', function(e){
            me.imagePlaceHolderEl.removeClassName(me.imageDragoverClass);
            e.stop();
            me.onFileDrop(e.dataTransfer.files);
        });
        var fileInput = this.imagePlaceHolderEl.select('input[type="file"]').first();
        this.imagePlaceHolderEl.observe('click', function(e){
            fileInput.click();
        });
        fileInput.observe('change', function(e){
            e.stop();
            me.onFileDrop(fileInput.files);
            fileInput.value = "";
        });
        this.updateImageStatuses();
    },

    updateImageStatuses: function () {
        var me = this;
        if (this.imageLoaded !== false ) {
            var data = {'image_url': this.imageLoaded};
            this.containerEl.insert(this.template.evaluate({data: data}));
            this.containerEl.down('button.delete').observe('click', function(e){
                me.removeImage();
            });
            this.imagePlaceHolderEl.hide();
        }
    },

    removeImage: function () {
        this.containerEl.down('.product-image').up().remove();
        this.imagePlaceHolderEl.show();
        this.imageEl.value = '';
    },

    addProgress: function () {
        this.imagePlaceHolderEl.addClassName(this.loadingClass);
    },

    removeProgress: function () {
        this.imagePlaceHolderEl.removeClassName(this.loadingClass);
    },

    onFileDrop: function(files) {
        var me = this;
        if (files.length <= 0) {
            return;
        }
        me.addProgress();
        var file = files[0];

        if (file.name && file.name.match(this.acceptFileTypes) === null) {
            alert(this.dontSupportFileTypeMessage);
            return;
        }

        var successFn = (function(me){
            return function(json){
                me.imageLoaded = json.url;
                me.imageEl.value = json.file;
                me.removeProgress();
                me.updateImageStatuses();
            };
        })(me);

        var failureFn = (function(me){
            return function(json){
                alert(me.errorMessage)
            };
        })(me);

        this.fileUpload(file, successFn, failureFn);
    },

    fileUpload: function(file, successFn, failureFn) {
        successFn = successFn||Prototype.emptyFunction;
        failureFn = failureFn||Prototype.emptyFunction;

        var formData = new FormData();
        formData.append('file', file, file.name);
        formData.append('form_key', this.secureFormKey);

        var xhr = new XMLHttpRequest();
        xhr.open('POST', this.uploadUrl, true);
        xhr.onload = function () {
            if (xhr.status !== 200) {
                failureFn();
                return;
            }
            try {
                var json = xhr.responseText.evalJSON();
            } catch (e) {
                failureFn();
                return;
            }
            if (!json.success) {
                failureFn();
                return;
            }
            successFn(json);
        };

        xhr.onerror = function() {
            failureFn();
        };

        xhr.send(formData);
    },
}