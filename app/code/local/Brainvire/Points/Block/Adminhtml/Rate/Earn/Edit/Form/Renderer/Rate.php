<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */


class Brainvire_Points_Block_Adminhtml_Rate_Earn_Edit_Form_Renderer_Rate extends Mage_Adminhtml_Block_Template implements Varien_Data_Form_Element_Renderer_Interface {

    protected function _construct() {
        $this->setTemplate('brainvire_points/rate/form/renderer/rate.phtml');
    }

    public function render(Varien_Data_Form_Element_Abstract $element) {
        $this->setElement($element);
        return $this->toHtml();
    }

}
