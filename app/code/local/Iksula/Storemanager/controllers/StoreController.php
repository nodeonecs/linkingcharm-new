<?php
/**
 * Iksula_Storemanager extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category       Iksula
 * @package        Iksula_Storemanager
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Store front contrller
 *
 * @category    Iksula
 * @package     Iksula_Storemanager
 * @author      Ultimate Module Creator
 */
class Iksula_Storemanager_StoreController extends Mage_Core_Controller_Front_Action
{

    /**
      * default action
      *
      * @access public
      * @return void
      * @author Ultimate Module Creator
      */



    public function storeAction(){

        $stores = Mage::app()->getStores();
        foreach ($stores as $store)
        {
            echo '<pre>';
            print_r($store);
        }
    }

    // pickup for store functionality 
    public function selectedstoreAction(){
        $params = $this->getRequest()->getParams();
        $data = (array)json_decode($params['store_data']);
        $layout = $this->getLayout();
        $block = $layout->createBlock('storeinventory/storelisting');
        $block->setStoredata($data);
        $block->setTemplate('storeinventory/storedetails.phtml');    
        $response = $block->toHtml();
        $this->getResponse()->setBody($response);
        // $data['product_sku'] = $productSku;
        $this->getResponse()->setBody(json_encode($data));
    }

    public function setpopuphtmlAction(){
        $params = $this->getRequest()->getParams();
        $layout = $this->getLayout();
        $block = $layout->createBlock('storeinventory/storelisting');
        $block->setStorePopupdata($params);
        $block->setTemplate('storeinventory/pickuppopup.phtml');
        $response = $block->toHtml();
        $this->getResponse()->setBody($response);
        // $data['product_sku'] = $productSku;

        // $this->getResponse()->setBody(json_encode($data));
    }

    public function simpleforconfigurableAction(){
        $params = $this->getRequest()->getParams();
        $id = $params['product_id'];
        $result = array();
        
        $attribute_code = explode(",", $params['attribute_code']);
        if(is_array($params['attribute_value'])) {
            $attribute_value = $params['attribute_value'];
        }else {
            $attribute_value = explode(",", $params['attribute_value']);
        }

        $result = array_combine($attribute_code,$attribute_value);
        
        $finalArr = array();
        $i = 0;
        foreach ($result as $key => $data) {
            $finalArr[$i] = array('attribute' => $key, 'eq' => $data);
            $i++;
        }

        $ids = Mage::getModel('catalog/product_type_configurable')->getChildrenIds($id);
        $_subproducts = Mage::getModel('catalog/product')->getCollection()
                        ->addAttributeToFilter('entity_id', $ids)
                        ->addAttributeToFilter($finalArr);

        foreach ($_subproducts as $value) {
            $simple_productSku = $value->getData('sku');
        }

        $this->getResponse()->setBody($simple_productSku);
    }

    public function citywisestoreAction(){
        $params = $this->getRequest()->getParams();
        $product_sku = $params['product_sku'];
        $item_id = $params['item_id'];
        $page_type = $params['page_type'];
        $product_type = $params['product_type'];
        $store_code = $params['store_code'];
        $store_code = explode(",", $store_code);
        $bufferValue = Mage::getStoreConfig('iksula_storemanager/store/bufferfield');
        
        $collection = Mage::getModel('storeinventory/storeinventory')->getCollection();
        $collection->addFieldToFilter('sku',$product_sku);
        $collection->addFieldToFilter('store_code',array('in' => $store_code));
        $collection->addFieldToFilter('inventory',array('gt' => $bufferValue));
        
        $store_code_group = array();
        foreach ($collection as $value) {
             $store_code_group[] = $value->getData('store_code');
        }
            
        $storeCollection = Mage::getModel('core/store_group')->getCollection();
        $storeCollection->addFieldToFilter('store_code',array('in' => $store_code_group));
        $storeCollection->addFieldToFilter('active_status',1);
        $storeCollection->getSelect()->join(
        'directory_country_region_city', 
        'main_table.store_city = directory_country_region_city.city_id', 
        array('*') 
        );
        $storeCollection->getSelect()->join(
        'directory_country_region', 
        'main_table.store_state = directory_country_region.region_id', 
        array('*') 
        );

        if($storeCollection->getData()) {
            $layout = $this->getLayout();
            $block = $layout->createBlock('storeinventory/storelisting');
            //     $block->setConfigurableProductSku($configurable_sku);
            $block->setFeedback($storeCollection);
            if($product_type == 'configurable') {
                $block->setConfigurableProductSku($params['configurable_sku']);
            }
            //     $block->setProductType($product_type);
            //     $block->setProductSku($product_sku);
            // }else {
            // }
            $block->setItemId($item_id);
            $block->setPageType($page_type);
            $block->setProductType($product_type);
            $block->setProductSku($product_sku);
            $block->setTemplate('storeinventory/storelisting.phtml');    
            $response = $block->toHtml();
        } else {
            $storeunavilable = Mage::getStoreConfig('iksula_storemanager/store/storeunavilable');    
            $response = $storeunavilable;
        }

        $this->getResponse()->setBody($response);
    }

    public function pickuplistAction() {
        $params = $this->getRequest()->getParams();
        $product_type = $params['product_type'];
        $product_sku = $params['product_sku'];
        if($product_type == 'configurable'){
            $configurableProduct = Mage::getModel('catalog/product')->loadByAttribute('sku',$product_sku);; 
            $childProducts = Mage::getModel('catalog/product_type_configurable')->getUsedProducts(null,$configurableProduct);   
            foreach($childProducts as $child) {
                $sku[] = $child->getData('sku');
            }
        } else {
            $sku[] = $product_sku; 
        }
        
        $cart = Mage::getModel('checkout/cart')->getQuote();
        $storeCollection = array();
        $i = 0;
        foreach ($cart->getAllItems() as $item) {
            $productSku = $item->getProduct()->getSku();
            if($item->getCheckoutMethod() == 'pickable' && in_array($productSku, $sku)) {
                $storeCollection[$i]['product_sku'] = $productSku;
                $storeCollection[$i]['store_name'] = $item->getStoreName();
                $storeCollection[$i]['store_address'] = $item->getStoreAddress();
                $storeCollection[$i]['store_city'] = $item->getStoreCity();
                $storeCollection[$i]['store_pincode'] = $item->getStorePincode();
                $storeCollection[$i]['store_zone'] = $item->getStoreZone();
                $storeCollection[$i]['store_region'] = $item->getStoreRegion();
            }
            $i++;
        }

        $storeCollection = array_map("unserialize", array_unique(array_map("serialize", $storeCollection)));
        $layout = $this->getLayout();
        $block = $layout->createBlock('storeinventory/storelisting');
        $block->setStorelist($storeCollection);
        $block->setTemplate('storeinventory/pickupstore.phtml');    
        $response = $block->toHtml();
        $this->getResponse()->setBody($response);
    }

    public function changestoreAction(){
        $params = $this->getRequest()->getParams();
        $data = (array)json_decode($params['store_data']);
        $item_id = $params['item_id'];
        $items = Mage::getModel('checkout/cart')->getQuote()->getAllItems();
        foreach ($items as $value) {
            if ($value->getParentItemId() == $item_id) {
                $value->setStoreName($data['name']);
                $value->setStoreAddress($data['store_address']);
                $value->setStoreCity($data['cityname']);
                $value->setStorePincode($data['store_pincode']);
                $value->setStoreZone($data['store_zone']);
                $value->setStoreRegion($data['region']);
                $value->save();
            }
        }
        
        $this->getResponse()->setBody(json_encode($value->getData()));
    }

    //  public function pincodewisestoreAction(){
    //     $params = $this->getRequest()->getParams();
    //     $item_id = $params['item_id'];
    //     $product_sku = $params['product_sku'];
    //     $page_type = $params['page_type'];
    //     $product_type = $params['product_type'];
    //     $store_pincode = $params['store_pincode'];
    //     $bufferValue = Mage::getStoreConfig('iksula_storemanager/store/bufferfield');
        
    //     // $collection = Mage::getModel('onepagecheckout/onepagecheckout')->getCollection()->addFieldToFilter('postcode',$store_pincode);
    //     $collection = Mage::getModel('core/store_group')->getCollection()->addFieldToFilter('store_pincode',$store_pincode);
    
    //     if($collection->getData()){
    //         foreach ($collection as $value) {
    //             $city_id = $value->getData('store_city');
    //         }

    //         // $cityCollection = Mage::getModel('romcity/romcity')->getCollection()->addFieldToFilter('cityname',$city)->getData();
    //         // $city_id = $cityCollection[0]['city_id'];
        
    //         $storeCollection = Mage::getModel('core/store_group')->getCollection();
    //         $storeCollection->addFieldToFilter('store_city',$city_id);
            
    //         foreach ($storeCollection as $value) {
    //             $store_code[] = $value->getData('store_code');
    //         }

    //         $collection = Mage::getModel('storeinventory/storeinventory')->getCollection();
    //         $collection->addFieldToFilter('sku',$product_sku);
    //         $collection->addFieldToFilter('store_code',array('in' => $store_code));
    //         $collection->addFieldToFilter('inventory',array('gt' => $bufferValue));
                 
    //         $store_code_group = array();
    //         foreach ($collection as $value) {
    //              $store_code_group[] = $value->getData('store_code');
    //         }


    //         $storeCollection->addFieldToFilter('store_code',array('in' => $store_code_group));
    //         $storeCollection->getSelect()->join(
    //         'directory_country_region_city', 
    //         'main_table.store_city = directory_country_region_city.city_id', 
    //         array('*') 
    //         );
    //         $storeCollection->getSelect()->join(
    //         'directory_country_region', 
    //         'main_table.store_state = directory_country_region.region_id', 
    //         array('*') 
    //         );

    //         if($storeCollection->getData()) {
    //             $distanceResult = $this->findlatlon($store_pincode);
    //             foreach ($storeCollection->getData() as $value) {
    //                 $store_longitude = $value['store_longitude'];
    //                 $store_latitude = $value['store_latitude'];
    //                 $result = $this->distance($distanceResult['lat'],$distanceResult['long'],$store_latitude,$store_longitude,'K');
    //                 $value['distance'] = number_format($result, 2, null, ''); 
    //                 $StoreArray[] = $value ;
    //             }

    //             $SortedArray = $this->array_sort_by_column($StoreArray, 'distance');
    //             $SortedStoreCollection = $this->getVarienDataCollection($SortedArray);
                    
    //             $layout = $this->getLayout();
    //             $block = $layout->createBlock('storeinventory/storelisting');
    //             $block->setFeedback($SortedStoreCollection);
    //             if($product_type == 'configurable') {
    //                 $block->setConfigurableProductSku($params['configurable_sku']);
    //             }
    //             //     $block->setProductType($product_type);
    //             //     $block->setProductSku($product_sku);
    //             // }else {
    //             // }
    //             $block->setItemId($item_id);
    //             $block->setPageType($page_type);
    //             $block->setProductType($product_type);
    //             $block->setProductSku($product_sku);
    //             $block->setTemplate('storeinventory/storelisting.phtml');    
    //             $response = $block->toHtml();
    //         } else {
    //             $storeunavilable = Mage::getStoreConfig('iksula_storemanager/store/storeunavilable');    
    //             $response = $storeunavilable;    
    //         }
    //     }else {
    //         $pincoderror = Mage::getStoreConfig('iksula_storemanager/store/pincoderror');
    //         $response = '<span class="invalid_pincode">'.$pincoderror.'</span>';
    //     }        

    //     $this->getResponse()->setBody($response);
    // }


    public function pincodewisestoreAction(){
        $params = $this->getRequest()->getParams();
        $item_id = $params['item_id'];
        $product_sku = $params['product_sku'];
        $page_type = $params['page_type'];
        $product_type = $params['product_type'];
        $store_pincode = $params['store_pincode'];
        $bufferValue = Mage::getStoreConfig('iksula_storemanager/store/bufferfield');
        
        
        $collection = Mage::getModel('storeinventory/storeinventory')->getCollection();
        $collection->addFieldToFilter('sku',$product_sku);
        $collection->addFieldToFilter('inventory',array('gt' => $bufferValue));
       
        if($collection->getData()){
            $store_code_group = array();
            foreach ($collection as $value) {
                 $store_code_group[] = $value->getData('store_code');
            }

            $storeCollection = Mage::getModel('core/store_group')->getCollection();
            $storeCollection->addFieldToFilter('store_code',array('in' => $store_code_group));
            $storeCollection->addFieldToFilter('active_status',1);
            $storeCollection->getSelect()->join(
            'directory_country_region_city', 
            'main_table.store_city = directory_country_region_city.city_id', 
            array('*') 
            );
            $storeCollection->getSelect()->join(
            'directory_country_region', 
            'main_table.store_state = directory_country_region.region_id', 
            array('*') 
            );
            
            $distanceResult = $this->findlatlon($store_pincode);
            
            if($storeCollection->getData() && $distanceResult['status'] == 'valid') {
                $i = 0;
                foreach ($storeCollection->getData() as $value) {
                    if($i <= 4) {
                        $store_longitude = $value['store_longitude'];
                        $store_latitude = $value['store_latitude'];
                        $result = $this->distance($distanceResult['lat'],$distanceResult['long'],$store_latitude,$store_longitude,'K');
                        $value['distance'] = number_format($result, 2, null, ''); 
                        $StoreArray[] = $value ;
                    }else {
                        break;
                    }

                    $i++;
                }

                $SortedArray = $this->array_sort_by_column($StoreArray, 'distance');
                $SortedStoreCollection = $this->getVarienDataCollection($SortedArray);
                    
                $layout = $this->getLayout();
                $block = $layout->createBlock('storeinventory/storelisting');
                $block->setFeedback($SortedStoreCollection);
                if($product_type == 'configurable') {
                    $block->setConfigurableProductSku($params['configurable_sku']);
                }
                
                $block->setItemId($item_id);
                $block->setPageType($page_type);
                $block->setProductType($product_type);
                $block->setProductSku($product_sku);
                $block->setTemplate('storeinventory/storelisting.phtml');    
                $response = $block->toHtml();
            }else {
                $pincoderror = Mage::getStoreConfig('iksula_storemanager/store/pincoderror');
                $response = '<span class="invalid_pincode">'.$pincoderror.'</span>';    
            } 
            
        }else {
            $storeunavilable = Mage::getStoreConfig('iksula_storemanager/store/storeunavilable');
            $response = '<span class="invalid_pincode">'.$storeunavilable.'</span>';
        }        

        $this->getResponse()->setBody($response);
    }

    public function getVarienDataCollection($items) {
        $collection = new Varien_Data_Collection();             
        foreach ($items as $item) {
            $varienObject = new Varien_Object();
            $varienObject->setData($item);
            $collection->addItem($varienObject);
        }
        return $collection;
    }


    function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
        $sort_col = array();
        foreach ($arr as $key=> $row) {
            $sort_col[$key] = $row[$col];
        }

        array_multisort($sort_col, $dir, $arr);
        return $arr;
    }

    public function findlatlon($store_pincode) {
        $file = 'http://maps.google.com/maps/api/geocode/json?address='.$store_pincode.',India&sensor=false';
        $geocode=file_get_contents($file);
        $output= json_decode($geocode);
        $lat = $output->results[0]->geometry->location->lat;
        $long = $output->results[0]->geometry->location->lng;

        $data = array();
        $data['lat'] = $lat;
        $data['long'] = $long;
        
        if($data['lat'] > 0 && $data['long'] > 0){
           $data['status'] = 'valid';
        }else{
            $data['status'] = 'invalid';
        }

        return $data;
    }

    public function distance($lat1, $lon1, $lat2, $lon2, $unit) {

      $theta = $lon1 - $lon2;
      $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
      $dist = acos($dist);
      $dist = rad2deg($dist);
      $miles = $dist * 60 * 1.1515;
      $unit = strtoupper($unit);

      if ($unit == "K") {
        return ($miles * 1.609344);
      } else if ($unit == "N") {
          return ($miles * 0.8684);
        } else if ($unit == "M") {
          return ($miles * 1609.34);
        } 
        else {
            return $miles;
          }
    }



    public function indexAction()
    {
        $this->loadLayout();
        $this->_initLayoutMessages('catalog/session');
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('checkout/session');
        if (Mage::helper('iksula_storemanager/store')->getUseBreadcrumbs()) {
            if ($breadcrumbBlock = $this->getLayout()->getBlock('breadcrumbs')) {
                $breadcrumbBlock->addCrumb(
                    'home',
                    array(
                        'label' => Mage::helper('iksula_storemanager')->__('Home'),
                        'link'  => Mage::getUrl(),
                    )
                );
                $breadcrumbBlock->addCrumb(
                    'stores',
                    array(
                        'label' => Mage::helper('iksula_storemanager')->__('Store'),
                        'link'  => '',
                    )
                );
            }
        }
        $headBlock = $this->getLayout()->getBlock('head');
        if ($headBlock) {
            $headBlock->addLinkRel('canonical', Mage::helper('iksula_storemanager/store')->getStoresUrl());
        }
        if ($headBlock) {
            $headBlock->setTitle(Mage::getStoreConfig('iksula_storemanager/store/meta_title'));
            $headBlock->setKeywords(Mage::getStoreConfig('iksula_storemanager/store/meta_keywords'));
            $headBlock->setDescription(Mage::getStoreConfig('iksula_storemanager/store/meta_description'));
        }
        $this->renderLayout();
    }

    /**
     * init Store
     *
     * @access protected
     * @return Iksula_Storemanager_Model_Store
     * @author Ultimate Module Creator
     */
    protected function _initStore()
    {
        $storeId   = $this->getRequest()->getParam('id', 0);
        $store     = Mage::getModel('iksula_storemanager/store')
            ->setStoreId(Mage::app()->getStore()->getId())
            ->load($storeId);
        if (!$store->getId()) {
            return false;
        } elseif (!$store->getStatus()) {
            return false;
        }
        return $store;
    }

    /**
     * view store action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function viewAction()
    {
        $store = $this->_initStore();
        if (!$store) {
            $this->_forward('no-route');
            return;
        }
        Mage::register('current_store', $store);
        $this->loadLayout();
        $this->_initLayoutMessages('catalog/session');
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('checkout/session');
        if ($root = $this->getLayout()->getBlock('root')) {
            $root->addBodyClass('storemanager-store storemanager-store' . $store->getId());
        }
        if (Mage::helper('iksula_storemanager/store')->getUseBreadcrumbs()) {
            if ($breadcrumbBlock = $this->getLayout()->getBlock('breadcrumbs')) {
                $breadcrumbBlock->addCrumb(
                    'home',
                    array(
                        'label'    => Mage::helper('iksula_storemanager')->__('Home'),
                        'link'     => Mage::getUrl(),
                    )
                );
                $breadcrumbBlock->addCrumb(
                    'stores',
                    array(
                        'label' => Mage::helper('iksula_storemanager')->__('Store'),
                        'link'  => Mage::helper('iksula_storemanager/store')->getStoresUrl(),
                    )
                );
                $breadcrumbBlock->addCrumb(
                    'store',
                    array(
                        'label' => $store->getStoreTypeId(),
                        'link'  => '',
                    )
                );
            }
        }
        $headBlock = $this->getLayout()->getBlock('head');
        if ($headBlock) {
            $headBlock->addLinkRel('canonical', $store->getStoreUrl());
        }
        if ($headBlock) {
            if ($store->getMetaTitle()) {
                $headBlock->setTitle($store->getMetaTitle());
            } else {
                $headBlock->setTitle($store->getStoreTypeId());
            }
            $headBlock->setKeywords($store->getMetaKeywords());
            $headBlock->setDescription($store->getMetaDescription());
        }
        $this->renderLayout();
    }

    /**
     * store rss list action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function rssAction()
    {
        if (Mage::helper('iksula_storemanager/store')->isRssEnabled()) {
            $this->getResponse()->setHeader('Content-type', 'text/xml; charset=UTF-8');
            $this->loadLayout(false);
            $this->renderLayout();
        } else {
            $this->getResponse()->setHeader('HTTP/1.1', '404 Not Found');
            $this->getResponse()->setHeader('Status', '404 File not found');
            $this->_forward('nofeed', 'index', 'rss');
        }
    }

    /**
     * Submit new comment action
     * @access public
     * @author Ultimate Module Creator
     */
    public function commentpostAction()
    {
        $data   = $this->getRequest()->getPost();
        $store = $this->_initStore();
        $session    = Mage::getSingleton('core/session');
        if ($store) {
            if ($store->getAllowComments()) {
                if ((Mage::getSingleton('customer/session')->isLoggedIn() ||
                    Mage::getStoreConfigFlag('iksula_storemanager/store/allow_guest_comment'))) {
                    $comment  = Mage::getModel('iksula_storemanager/store_comment')->setData($data);
                    $validate = $comment->validate();
                    if ($validate === true) {
                        try {
                            $comment->setStoreId($store->getId())
                                ->setStatus(Iksula_Storemanager_Model_Store_Comment::STATUS_PENDING)
                                ->setCustomerId(Mage::getSingleton('customer/session')->getCustomerId())
                                ->setStores(array(Mage::app()->getStore()->getId()))
                                ->save();
                            $session->addSuccess($this->__('Your comment has been accepted for moderation.'));
                        } catch (Exception $e) {
                            $session->setStoreCommentData($data);
                            $session->addError($this->__('Unable to post the comment.'));
                        }
                    } else {
                        $session->setStoreCommentData($data);
                        if (is_array($validate)) {
                            foreach ($validate as $errorMessage) {
                                $session->addError($errorMessage);
                            }
                        } else {
                            $session->addError($this->__('Unable to post the comment.'));
                        }
                    }
                } else {
                    $session->addError($this->__('Guest comments are not allowed'));
                }
            } else {
                $session->addError($this->__('This store does not allow comments'));
            }
        }
        $this->_redirectReferer();
    }

    public function assignedAction(){

        $test = Mage::getModel('salesorderitemgrid/assignedstatusstores')->getCollection();

        echo '<pre>';
        print_r($test->getData());
        exit;


    }
}
