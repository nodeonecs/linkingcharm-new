<?php
class IWD_OnepageCheckout_TrackController extends Mage_Checkout_Controller_Action
{

	public function getTrackDetailsStepTwoAction(){
		
		/*$quote = Mage::getModel('sales/quote')->load(2);
		echo $orderId = $quote->getReservedOrderId();exit;
		echo '<pre>';print_r($quote);*/

		$checkoutstep = Mage::app()->getRequest()->getPost('checkoutstep');
		$trackCheckoutHelper = Mage::helper('onepagecheckout/Track');
		$trackCheckoutHelper->getTrackDetailsStepTwo($checkoutstep);
	}

	public function getTrackDetailsStepThreeAction(){
		
		$checkoutstep 	= Mage::app()->getRequest()->getPost('checkoutstep');
		$checkoutemail 	= Mage::app()->getRequest()->getPost('cemailidcheckout');
		$checkoutf		= Mage::app()->getRequest()->getPost('cfname');
		$checkoutl 		= Mage::app()->getRequest()->getPost('clname');
		$checkoutm 		= Mage::app()->getRequest()->getPost('cmob');
		$trackCheckoutHelper = Mage::helper('onepagecheckout/Track');
		$trackCheckoutHelper->getTrackDetailsStepThree($checkoutstep,$checkoutemail,$checkoutf,$checkoutl,$checkoutm);
	}
}