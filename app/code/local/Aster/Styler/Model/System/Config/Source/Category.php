<?php
class Aster_Styler_Model_System_Config_Source_Category
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray($addEmpty = true)
    {
        $collection = Mage::getModel('catalog/category')->getCollection()->addAttributeToSelect('*');

        $options = array();   

        $excludeCategory = array('Root Catalog','root');

        foreach($collection as $category){
            if(!in_array($category->getName(),$excludeCategory)){
                $options[] = array(
                   'label' => $category->getName(),
                   'value' => $category->getId()
                );
            }
        }
        return $options;
    }

}
