<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_Helper_Giftcard extends Mage_Core_Helper_Abstract
{
    /**
     * Adds Gift Card to selected quote
     *
     * @param AW_Giftcard2_Model_Giftcard $giftcard
     * @param null $quoteId
     * @param null $quote
     * @return $this
     * @throws Mage_Core_Exception
     */
    public function addToQuote(AW_Giftcard2_Model_Giftcard $giftcard, $quoteId = null, $quote = null)
    {
        if ($quote === null) {
            if (is_null($quoteId)) {
                $quoteId = $this->_getQuote()->getId();
                $quote = $this->_getQuote();
            } else {
                $quote = Mage::getModel('sales/quote')->load($quoteId);
            }
        }

        if ($giftcard->getId()) {
            $giftcard->validate($quote->getStoreId());
            $giftCardToQuote = Mage::getModel('aw_giftcard2/giftcard_quote');
            $collection = $giftCardToQuote->getCollection()
                ->setFilterByQuoteId($quoteId)
                ->setFilterByGiftCardId($giftcard->getId())
            ;
            if ($collection->getSize() > 0) {
                throw new Mage_Core_Exception(
                    $this->__('This gift card is already applied.')
                );
            } else {
                $giftCardToQuote
                    ->setQuoteId($quoteId)
                    ->setGiftcardId($giftcard->getId())
                    ->save()
                ;
            }
        } else {
            throw new Mage_Core_Exception(
                $this->__('Gift Card Code is not valid.')
            );
        }

        $quote
            ->setTotalsCollectedFlag(false)
            ->collectTotals()
            ->save()
        ;
        return $this;
    }

    /**
     * Removes Gift Card from selected quote
     *
     * @param $giftCardId
     * @param null $quoteId
     * @param null $quote
     * @return $this
     */
    public function removeFromQuote($giftCardId, $quoteId = null, $quote = null)
    {
        if ($quote === null) {
            if (is_null($quoteId)) {
                $quoteId = $this->_getQuote()->getId();
                $quote = $this->_getQuote();
            } else {
                $quote = Mage::getModel('sales/quote')->load($quoteId);
            }
        }

        $giftCardToQuote = Mage::getModel('aw_giftcard2/giftcard_quote');

        $collection = $giftCardToQuote->getCollection()
            ->setFilterByQuoteId($quoteId)
            ->setFilterByGiftCardId($giftCardId)
        ;
        $giftcardRemoved = false;
        foreach ($collection as $giftcardQuote) {
            $giftcardQuote->delete();
            $giftcardRemoved = true;
        }

        if ($giftcardRemoved) {
            $quote
                ->setTotalsCollectedFlag(false)
                ->collectTotals()
                ->save()
            ;
        }
        return $this;
    }

    /**
     * Returns collection of Gift Cards applied to selected quote
     *
     * @param null $quoteId
     * @return mixed
     */
    public function getQuoteGiftCards($quoteId = null)
    {
        if (is_null($quoteId)) {
            $quoteId = $this->_getQuote()->getId();
        }
        $collection = Mage::getModel('aw_giftcard2/giftcard_quote')->getCollection();
        $collection
            ->setFilterByQuoteId($quoteId)
            ->addGiftCardsData()
            ->addOrder('balance', Varien_Data_Collection::SORT_ORDER_ASC)
        ;
        return $collection->getItems();
    }

    /**
     *  Returns collection of Gift Cards applied to selected invoice
     *
     * @param $invoiceId
     * @return array
     */
    public function getInvoiceGiftCards($invoiceId)
    {
        /** @var AW_Giftcard2_Model_Resource_Giftcard_Invoice_Collection $collection */
        $collection = Mage::getModel('aw_giftcard2/giftcard_invoice')->getCollection();
        $collection
            ->setFilterByInvoiceId($invoiceId)
            ->addGiftCardsData()
            ->addOrder('balance', Varien_Data_Collection::SORT_ORDER_ASC)
        ;
        return $collection->getItems();
    }

    /**
     *  Returns collection of Gift Cards applied to selected credit memo
     *
     * @param $creditMemoId
     * @return array
     */
    public function getCreditMemoGiftCards($creditMemoId)
    {
        /** @var AW_Giftcard2_Model_Resource_Giftcard_Creditmemo_Collection $collection */
        $collection = Mage::getModel('aw_giftcard2/giftcard_creditmemo')->getCollection();
        $collection
            ->setFilterByCreditMemoId($creditMemoId)
            ->addGiftCardsData()
            ->addOrder('balance', Varien_Data_Collection::SORT_ORDER_ASC)
        ;
        return $collection->getItems();
    }

    /**
     * Returns current quote
     *
     * @return mixed
     */
    protected function _getQuote()
    {
        return Mage::getSingleton('checkout/session')->getQuote();
    }
}