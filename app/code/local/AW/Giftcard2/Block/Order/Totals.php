<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_Block_Order_Totals extends Mage_Core_Block_Template
{
    public function getSource()
    {
        /** @var Mage_Sales_Block_Order_Totals $parent */
        $parent = $this->getParentBlock();
        return $parent->getSource();
    }

    public function initTotals()
    {
        if ($this->getSource()->getAwGiftCardsUsed() && count($this->getSource()->getAwGiftCardsUsed()) > 0) {
            foreach ($this->getSource()->getAwGiftCardsUsed() as $usedGiftCard) {
                $this->getParentBlock()->addTotal(
                    new Varien_Object(
                        array(
                            'code'   => 'aw_giftcard2_' . $usedGiftCard->getGiftcardId(),
                            'strong' => false,
                            'label'  => $this->__('Gift Card (%s)', $usedGiftCard->getCode()),
                            'value'  => -$usedGiftCard->getGiftcardAmount(),
                        )
                    ),
                    'tax'
                );
            }
        }
        return $this;
    }
}