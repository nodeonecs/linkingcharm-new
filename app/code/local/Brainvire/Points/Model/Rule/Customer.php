<?php
/**
* Brainvire Infotech Pvt. Ltd
  * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */


class Brainvire_Points_Model_Rule_Customer extends Mage_Core_Model_Abstract {

    protected function _construct() {
        parent::_construct();
        $this->_init('points/rule_customer');
    }

}