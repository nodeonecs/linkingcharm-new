<?php
class IWD_OnepageCheckout_Helper_Track extends Mage_Core_Helper_Abstract
{
    public function getTrackDetailsStepOne($checkoutStep){
    	$data 		 		= array();
    	$productSku  		= '';
    	$productName 		= '';
    	$fullname	 		= '';
    	$email 		 		= '';
    	$telephone 	 		= '';
    	//$checkoutStep 		= 0;
    	$session 			= Mage::getSingleton('checkout/session');
    	$quote_id			= $session->getQuoteId();

    	$quoteDetails = $this->checkQuoteExists($quote_id);
        if(Mage::getSingleton('customer/session')->isLoggedIn()){
            $customer       = Mage::getSingleton('customer/session')->getCustomer();
            $fullname       = $customer->getName();
            $email          = $customer->getEmail();
            if($customer->getPrimaryBillingAddress()){
                $telephone      = $customer->getPrimaryBillingAddress()->getTelephone();
            }
        }

    	if(!$quoteDetails->getData()){
    		//echo '<pre>';print_r($quoteDetails->getData());exit;
    		//$oldQuoteId 		= $quoteDetails->getData('quote_id');
    		//$oldcheckoutStep 	= $quoteDetails->getData('checkout_step');
	    	$cart = Mage::getModel('checkout/cart')->getQuote();
	    	$grandTotal =  $cart->getGrandTotal();
			foreach ($cart->getAllVisibleItems() as $item) {
			    $productName .= $item->getProduct()->getName().',';
			    $productSku  .= $item->getProduct()->getSku().',';
			}

			
			$data['quote_id'] 		= $quote_id;
			date_default_timezone_set("Asia/Kolkata");
			$data['abandoned_date'] = date('Y-m-d H:i:s');
			$data['checkout_step'] 	= $checkoutStep;
			$data['skus'] 			=  $productSku;
			$data['product_names'] 	= $productName;
			$data['order_total'] 	= $grandTotal;
			//$data['order_id'] =;
			$data['customer_name'] 	= $fullname;
			$data['user_email'] 	= $email;
			$data['mobile_no'] 		= $telephone;
			//$data['customer_address'] = $quote_id;

			$this->insertData($data);
		}
        else{
            $oldcheckoutStep = $quoteDetails->getData('checkout_step');
            if($checkoutStep > $oldcheckoutStep)
            {
                $data['abandoned_date'] = date('Y-m-d H:i:s');
                $data['checkout_step']  = $checkoutStep;
                $data['customer_name']  = $fullname;
                $data['user_email']     = $email;
                $data['mobile_no']      = $telephone;
                $id = $quoteDetails['trackcheckout_id'];
                $this->updateData($data,$id);
            }
        }
    }

    public function getTrackDetailsStepTwo($checkoutstep){
    	$session 			= Mage::getSingleton('checkout/session');
    	$quote_id			= $session->getQuoteId();
    	$data 		 		= array();
    	date_default_timezone_set("Asia/Kolkata");
		$data['abandoned_date'] = date('Y-m-d H:i:s');
		$data['checkout_step'] 	= $checkoutstep;

		$quoteDetails = $this->checkQuoteExists($quote_id);
		$id = $quoteDetails['trackcheckout_id'];
		$this->updateData($data,$id);
    }


    public function getTrackDetailsStepThree($checkoutstep,$checkoutemail,$checkoutf,$checkoutl,$checkoutm){
    	$session 			= Mage::getSingleton('checkout/session');
    	$quote_id			= $session->getQuoteId();
    	$data 		 		= array();
    	
        date_default_timezone_set("Asia/Kolkata");
		$data['abandoned_date'] = date('Y-m-d H:i:s');
        $data['checkout_step']  = $checkoutstep;
        $data['customer_name']  = $checkoutf .' '. $checkoutl;
        $data['mobile_no']      = $checkoutm;

        if(!Mage::getSingleton('customer/session')->isLoggedIn()){
            $data['user_email'] 	= $checkoutemail;  
        }

		$quoteDetails = $this->checkQuoteExists($quote_id);
		$id = $quoteDetails['trackcheckout_id'];
		$this->updateData($data,$id);
    }

    public function getTrackDetailsStepFour($checkoutstep,$quote_id){

    	/*$session 			= Mage::getSingleton('checkout/session');
    	$quote_id			= $session->getQuoteId();*/
    	$quote 				= Mage::getModel('sales/quote')->load($quote_id);
    	$orderId 			= $quote->getReservedOrderId();
    	$data 		 		= array();
    	date_default_timezone_set("Asia/Kolkata");
		$data['abandoned_date'] = date('Y-m-d H:i:s');
		$data['checkout_step'] 	= $checkoutstep;
		$data['order_id'] 		= $orderId;

		$quoteDetails = $this->checkQuoteExists($quote_id);
		$id = $quoteDetails['trackcheckout_id'];
		$this->updateData($data,$id);
    }

    public function getTrackDetailsStepFive($checkoutstep,$lastOrderId,$lastQuoteId){
 
    	$quote_id			= $lastQuoteId;
    	$data 		 		= array();
    	date_default_timezone_set("Asia/Kolkata");
		$data['abandoned_date'] = date('Y-m-d H:i:s');
		$data['checkout_step'] 	= $checkoutstep;

		$quoteDetails = $this->checkQuoteExists($quote_id);
		$id = $quoteDetails['trackcheckout_id'];
		$this->updateData($data,$id);
    }

    public function insertData($data){
    	
    	$trackModel = $this->getModel();
    	try{
    		$trackModel->setData($data)->save();
    	}
    	catch (Exeption $e){
    		echo $e->getMessage();
    	}
    }

    public function updateData($data,$id){

    	$trackModel = $this->getModel();
    	try{
    		$trackModel->load($id)->addData($data)->save();
    	}
    	catch (Exeption $e){
    		echo $e->getMessage();
    	}
    }

    public function deleteData($id){

        $trackModel = $this->getModel();
        Mage::log($id . 'delete');
        try{
            $trackModel->setId($id)->delete();
        }
        catch (Exeption $e){
            echo $e->getMessage();
        }
    }

    Public function getModel(){
    	return Mage::getModel('trackcheckout/trackcheckout');	
    }

    // Check if the quote value exists.
    public function checkQuoteExists($quote_id){
    	$trackModel = $this->getModel();
    	$trackData  = $trackModel->getCollection()
    				->addFieldToFilter('quote_id',$quote_id)
    				->getFirstItem();
    	return $trackData;
    	//echo '<pre>';print_r($trackData->getData());exit;
    }
}
