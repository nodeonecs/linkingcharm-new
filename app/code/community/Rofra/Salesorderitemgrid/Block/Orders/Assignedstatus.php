<?php
Class Rofra_Salesorderitemgrid_Block_Orders_Assignedstatus extends Mage_Core_Block_Template{

	public function getAssignedCounts(){

		$aAssignedStatusCollections = Mage::getModel('salesorderitemgrid/assignedstatusstores')
		->getCollection();

		foreach($aAssignedStatusCollections->getData() as $oStatus){
				$aAssigned_status[$oStatus['assignedstatus_id']] = $oStatus['status'];
		}

		$i = 0 ;
		foreach($aAssigned_status as $status_id => $StatusName){

			$aAssignedDashboard [$i]['status_id'] = $status_id ;
			$aAssignedDashboard [$i]['counts'] = Mage::getModel('sales/order_item')->getCollection()->addFieldToFilter('assigned_status' , array('eq' => $status_id ))->getSize();
			$aAssignedDashboard [$i]['status_name'] = $StatusName ;
			$i++;
		}

		return $aAssignedDashboard;
	}

}


