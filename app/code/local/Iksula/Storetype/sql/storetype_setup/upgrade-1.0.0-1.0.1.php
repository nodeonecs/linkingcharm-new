<?php
$installer = $this;
$installer->startSetup();
$sql=<<<SQLTEXT

INSERT INTO store_type (id, name, status) VALUES
(1, 'Fully Operational', NULL),
(2, 'Fulfilment through HUB', NULL),
(3, 'Stock Only', NULL),
(4, 'Offline', NULL),
(5, 'HUB', NULL),
(6, 'Warehouse', NULL);

SQLTEXT;

$installer->run($sql);
//demo
//Mage::getModel('core/url_rewrite')->setId(null);
//demo
$installer->endSetup();




