<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_Model_Resource_Product_Indexer_Price_Giftcard
    extends Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Indexer_Price_Default
{
    /**
     * Reindex for all Giftcard products
     *
     * @return $this
     * @throws Exception
     */

    public function reindexAll()
    {
        $this->useIdxTable(true);
        $this->beginTransaction();
        try {
            $this->_prepareGiftcardProductPriceData();
            $this->commit();
        } catch (Exception $e) {
            $this->rollBack();
            throw $e;
        }
        return $this;
    }

    /**
     * Reindex for defined product(s)
     *
     * @param array|int $entityIds
     * @return $this
     */
    public function reindexEntity($entityIds)
    {
        $this->_prepareGiftcardProductPriceData($entityIds);
        return $this;
    }

    /**
     *  Calculate minimal and maximal prices for Giftcard products
     *
     * @param null $entityIds
     * @return $this
     */
    protected function _prepareGiftcardProductPriceData($entityIds = null)
    {
        $write = $this->_getWriteAdapter();
        $table = $this->getIdxTable();

        $select = $write->select()
            ->from(array('e' => $this->getTable('catalog/product')), 'entity_id')
            ->join(
                array('cg' => $this->getTable('customer/customer_group')),
                '',
                array('customer_group_id'));
        $this->_addWebsiteJoinToSelect($select, true);
        $this->_addProductWebsiteJoinToSelect($select, 'cw.website_id', 'e.entity_id');

        $select->columns('website_id', 'cw')
            ->group(array('e.entity_id', 'cg.customer_group_id', 'cw.website_id'))
            ->where('e.type_id=?', $this->getTypeId())
        ;

        $statusCond = $write->quoteInto(' = ?', Mage_Catalog_Model_Product_Status::STATUS_ENABLED);
        $this->_addAttributeToSelect($select, 'status', 'e.entity_id', 'cs.store_id', $statusCond);

        $select->joinLeft(
            array('pa'=> $this->getTable('aw_giftcard2/product_entity_amounts')),
            'pa.entity_id = e.entity_id AND (pa.website_id = cw.website_id OR pa.website_id = 0)',
            array('')
        );

        $expAllowOpenAmount = $this->_addAttributeToSelect(
            $select, AW_Giftcard2_Model_Product_Type_Giftcard::ATTRIBUTE_CODE_ALLOW_OPEN_AMOUNT, 'e.entity_id', 'cs.store_id'
        );
        $expOpenAmountMin = $this->_addAttributeToSelect(
            $select, AW_Giftcard2_Model_Product_Type_Giftcard::ATTRIBUTE_CODE_OPEN_AMOUNT_MIN, 'e.entity_id', 'cs.store_id'
        );
        $expOpenAmountMax =$this->_addAttributeToSelect(
            $select, AW_Giftcard2_Model_Product_Type_Giftcard::ATTRIBUTE_CODE_OPEN_AMOUNT_MAX, 'e.entity_id', 'cs.store_id'
        );

        $minOpenAmountExp = new Zend_Db_Expr("IF($expAllowOpenAmount, $expOpenAmountMin, NULL)");
        $maxOpenAmountExp = new Zend_Db_Expr("IF($expAllowOpenAmount, $expOpenAmountMax, NULL)");
        $minAmountExp = new Zend_Db_Expr("MIN(pa.value)");
        $maxAmountExp = new Zend_Db_Expr("MAX(pa.value)");

        $select->columns(
            array(
                'tax_class_id'  => new Zend_Db_Expr('0'),
                'price'         => new Zend_Db_Expr('NULL'),
                'final_price'   => new Zend_Db_Expr('NULL'),
                'min_price'     => $this->_getMinValueExpr($minAmountExp, $minOpenAmountExp),
                'max_price'     => $this->_getMaxValueExpr($maxAmountExp, $maxOpenAmountExp),
                'tier_price'    => new Zend_Db_Expr('NULL'),
                'group_price'   => new Zend_Db_Expr('NULL'),
            )
        );

        if (!is_null($entityIds)) {
            $select->where('e.entity_id IN(?)', $entityIds);
        }

        /**
         * Add additional external limitation
         */
        Mage::dispatchEvent('catalog_product_prepare_index_select', array(
            'select'        => $select,
            'entity_field'  => new Zend_Db_Expr('e.entity_id'),
            'website_field' => new Zend_Db_Expr('cw.website_id'),
            'store_field'   => new Zend_Db_Expr('cs.store_id')
        ));

        $query = $select->insertFromSelect($table);
        $write->query($query);

        return $this;
    }

    protected function _getMinValueExpr ($amountExpr, $openAmountExpr)
    {
        $expr = new Zend_Db_Expr(
            "IF($openAmountExpr IS NULL, $amountExpr, IF($amountExpr < $openAmountExpr, $amountExpr, $openAmountExpr))"
        );
        return $expr;
    }

    protected function _getMaxValueExpr ($amountExpr, $openAmountExpr)
    {
        $expr = new Zend_Db_Expr(
            "IF($openAmountExpr IS NULL, $amountExpr, IF($amountExpr > $openAmountExpr, $amountExpr, $openAmountExpr))"
        );
        return $expr;
    }
}