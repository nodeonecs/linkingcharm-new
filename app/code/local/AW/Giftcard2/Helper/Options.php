<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_Helper_Options extends Mage_Core_Helper_Abstract
{
    /**
     * Returns Giftcard product options from quote
     *
     * @param $item
     * @return array
     */
    public function getCartOptions($item)
    {
        $code = $item->getProduct()->getTypeInstance()->getCustomOptionsCode();
        $option = $item->getOptionByCode($code);
        if ($option) {
            $customOptions = $option->getValue();
            if ($customOptions) {
                $customOptions = unserialize($customOptions);
            } else {
                return array();
            }
        }
        return $this->_getOptions($customOptions);
    }

    /**
     * Returns Giftcard product options from order
     *
     * @param $item
     * @return array
     */
    public function getOrderOptions($item, $skipType = false)
    {
        $store = $item->getOrder()->getStore();
        $code = $item->getProduct()->getTypeInstance()->getCustomOptionsCode();
        $option = $item->getProductOptionByCode($code);
        if (!$option) {
            return array();
        }
        if ($skipType) {
            unset($option[AW_Giftcard2_Model_Product_Type_Giftcard::ATTRIBUTE_CODE_TYPE]);
        }
        return $this->_getOptions($option, $store);
    }

    /**
     * Fills array by selected options
     *
     * @param array $customOptions
     * @return array
     */
    protected function _getOptions(array $customOptions, $store = null)
    {
        $options = array();

        $type = $this->_getOptionValue(
            $customOptions,
            AW_Giftcard2_Model_Product_Type_Giftcard::ATTRIBUTE_CODE_TYPE
        );
        if ($type) {
            $options[] = array(
                'label' => Mage::helper('aw_giftcard2')->__('Gift Card Type'),
                'value' => $this->escapeHtml(
                    Mage::getModel('aw_giftcard2/source_entity_attribute_giftcard_type')->getOptionText($type)
                )
            );
        }

        $amount = $this->_getOptionValue(
            $customOptions,
            AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_AMOUNT
        );
        if ($amount) {
            $options[] = array(
                'label' => Mage::helper('aw_giftcard2')->__('Gift Card Amount'),
                'value' => $this->escapeHtml(Mage::helper('core')->currencyByStore($amount, $store, true, false))
            );

        }

        $template = $this->_getOptionValue(
            $customOptions,
            AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_EMAIL_TEMPLATE_NAME
        );
        if ($template) {
            $options[] = array(
                'label' => Mage::helper('aw_giftcard2')->__('Gift Card Design'),
                'value' => $this->escapeHtml($template)
            );
        }

        $senderName = $this->_getOptionValue(
            $customOptions,
            AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_SENDER_NAME
        );
        if ($senderName) {
            $senderEmail = $this->_getOptionValue(
                $customOptions,
                AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_SENDER_EMAIL
            );
            $options[] = array(
                'label' => Mage::helper('aw_giftcard2')->__('Gift Card Sender'),
                'value' => $this->escapeHtml($senderEmail ? "{$senderName} &lt;{$senderEmail}&gt;" : $senderName)
            );
        }

        $recipientName = $this->_getOptionValue(
            $customOptions,
            AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_RECIPIENT_NAME
        );
        if ($recipientName) {
            $recipientEmail = $this->_getOptionValue(
                $customOptions,
                AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_RECIPIENT_EMAIL
            );
            $options[] = array(
                'label' => Mage::helper('aw_giftcard2')->__('Gift Card Recipient'),
                'value' => $this->escapeHtml($recipientEmail ? "{$recipientName} &lt;{$recipientEmail}&gt;" : $recipientName)
            );
        }

        $headline = $this->_getOptionValue(
            $customOptions,
            AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_HEADLINE
        );
        if ($headline) {
            $headline = trim($headline);
            $options[] = array(
                'label' => Mage::helper('aw_giftcard2')->__('Gift Card Headline'),
                'value' => $this->escapeHtml($headline)
            );
        }

        $message = $this->_getOptionValue(
            $customOptions,
            AW_Giftcard2_Model_Product_Type_Giftcard::BUY_REQUEST_ATTR_CODE_MESSAGE
        );
        if ($message) {
            $message = trim($message);
            $options[] = array(
                'label' => Mage::helper('aw_giftcard2')->__('Gift Card Message'),
                'value' => $this->escapeHtml($message)
            );
        }

        $emailSent = $this->_getOptionValue(
            $customOptions,
            AW_Giftcard2_Model_Product_Type_Giftcard::ORDER_ITEM_CODE_EMAIL_SENT
        );
        if ($emailSent) {
            $options[] = array (
                'label' =>  Mage::helper('aw_giftcard2')->__('Email Sent'),
                'value' => $emailSent ?
                    Mage::helper('aw_giftcard2')->__('Yes') :
                    Mage::helper('aw_giftcard2')->__('No')
            );
        }

        $codes = $this->_getOptionValue(
            $customOptions,
            AW_Giftcard2_Model_Product_Type_Giftcard::ORDER_ITEM_CODE_CREATED_CODES
        );
        if ($codes) {
            if (is_array($codes) && count($codes) > 0) {
                $options[] = array(
                    'label' => Mage::helper('aw_giftcard2')->__('Gift Card Codes'),
                    'value' => implode('<br/>', $this->escapeHtml($codes))
                );
            }
        }

        return $options;
    }

    /**
     * Returns custom option value
     *
     * @param array $options
     * @param $code
     * @return mixed|null
     */
    protected function _getOptionValue(array $options, $code)
    {
        if (isset($options[$code])) {
            return $this->escapeHtml($options[$code]);
        }
        return null;
    }
}