<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   BV
 * @package    BV_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */


class Brainvire_Points_Model_Source_Cmspages {

    public function toOptionArray() {
        $toReturn = array(
            array(
                'value' => '0',
                'label' => Mage::helper('points')->__('-- None --')
                ));

        return array_merge($toReturn, Mage::getModel('cms/page')->getCollection()->toOptionArray());
    }

}