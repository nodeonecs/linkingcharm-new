<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_Adminhtml_Awgiftcard2_ProductController extends Mage_Adminhtml_Controller_Action
{
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('catalog/aw_giftcard2/products');
    }

    public function indexAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('catalog/aw_giftcard2');
        $this
            ->_title($this->__('Catalog'))
            ->_title($this->__('Gift Cards'))
            ->_title($this->__('Gift Card Products'))
        ;
        $this->renderLayout();
    }

    public function newAction()
    {
        $this->_getSession()->setBackToAwGiftCardProductsGridFlag(true);
        $this->_getSession()->setResetBackToAwGiftCardProductsGridFlag(false);
        $this->_redirect('adminhtml/catalog_product/new',
            array(
                'set'  => Mage::getModel('catalog/product')->getDefaultAttributeSetId(),
                'type' => AW_Giftcard2_Model_Product_Type_Giftcard::TYPE_CODE,
            )
        );
    }

    public function editAction()
    {
        $this->_getSession()->setBackToAwGiftCardProductsGridFlag(true);
        $this->_getSession()->setResetBackToAwGiftCardProductsGridFlag(false);
        $this->_redirect('adminhtml/catalog_product/edit', array(
            'id' => $this->getRequest()->getParam('id'),
            '_secure' => Mage::app()->getStore()->isCurrentlySecure(),
            'store' => $this->getRequest()->getParam('store'),
        ));
    }

    public function massDeleteAction()
    {
        $giftcardProductIds = $this->getRequest()->getParam('giftcard', null);

        if (is_array($giftcardProductIds)) {
            $productCollection = Mage::getModel('catalog/product')
                                    ->getCollection()
                                    ->addAttributeToFilter('entity_id', array('in' => $giftcardProductIds))
            ;
            $count = 0;
            foreach ($productCollection as $product) {
                try {
                    $product->delete();
                    $count++;
                } catch (Exception $e) {
                    $this->_getSession()->addError($e->getMessage());
                }

            }
            $this->_getSession()->addSuccess($this->__('A total of %s gift card product(s) have been deleted.', $count));
        }
        return $this->_redirect('*/*/index');
    }

    public function massEnableAction()
    {
        $storeId    = (int)$this->getRequest()->getParam('store', 0);
        $giftcardProductIds = $this->getRequest()->getParam('giftcard', null);


        if (is_array($giftcardProductIds)) {

            $count = 0;
            foreach ($giftcardProductIds as $productId) {
                try {
                    Mage::getModel('catalog/product_status')->updateProductStatus($productId, $storeId, Mage_Catalog_Model_Product_Status::STATUS_ENABLED);
                    $count++;
                } catch (Exception $e) {
                    $this->_getSession()->addError($e->getMessage());
                }

            }
            $this->_getSession()->addSuccess($this->__('A total of %s gift card product(s) have been enabled.', $count));
        }
        return $this->_redirect('*/*/index');
    }

    public function massDisableAction()
    {
        $storeId    = (int)$this->getRequest()->getParam('store', 0);
        $giftcardProductIds = $this->getRequest()->getParam('giftcard', null);

        if (is_array($giftcardProductIds)) {

            $count = 0;
            foreach ($giftcardProductIds as $productId) {
                try {
                    Mage::getModel('catalog/product_status')->updateProductStatus($productId, $storeId, Mage_Catalog_Model_Product_Status::STATUS_DISABLED);
                    $count++;
                } catch (Exception $e) {
                    $this->_getSession()->addError($e->getMessage());
                }

            }
            $this->_getSession()->addSuccess($this->__('A total of %s gift card product(s) have been disabled.', $count));
        }
        return $this->_redirect('*/*/index');
    }

    public function imageUploadAction ()
    {
        $result = array(
            'success' => true,
            'url' => null,
            'file' => null,
        );
        try {
            $uploader = new Varien_File_Uploader('file');
            $uploader->setFilesDispersion(true);
            $uploader->setFilenamesCaseSensitivity(false);
            $uploader->setAllowRenameFiles(true);
            $uploader->save(AW_Giftcard2_Helper_Image::getDirPath());
            $result['url'] = AW_Giftcard2_Helper_Image::resizeImage(
                $uploader->getUploadedFileName(),
                AW_Giftcard2_Helper_Image::BACKEND_IMAGE_SIZE,
                AW_Giftcard2_Helper_Image::BACKEND_IMAGE_SIZE
            );
            $result['file'] = $uploader->getUploadedFileName();
        } catch (Exception $e) {
            $result['success'] = false;
            $result['msg'] = $e->getMessage();
        }

        $this->getResponse()->setBody(
            Zend_Json::encode($result)
        );
    }
}