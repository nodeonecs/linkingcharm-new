<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   Brainvire
 * @package    Brainvire_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * @license    
 */


class Brainvire_Points_Model_Mysql4_Summary extends Mage_Core_Model_Mysql4_Abstract {

    public function _construct() {
        $this->_init('points/summary', 'id');
    }

    public function loadByCustomer($summary, $customer) {
        $select = $this->_getReadAdapter()->select()
                ->from($this->getMainTable())
                ->where('customer_id = ?', $customer->getId());
        if ($data = $this->_getReadAdapter()->fetchRow($select)) {
            $summary->addData($data);
        }
        $this->_afterLoad($summary);
        return $this;
    }

    public function loadByCustomerID($summary, $customerID) {
        $select = $this->_getReadAdapter()->select()
                ->from($this->getMainTable())
                ->where('customer_id = ?', $customerID);
        if ($data = $this->_getReadAdapter()->fetchRow($select)) {
            $summary->addData($data);
        }
        $this->_afterLoad($summary);
        return $this;
    }

}
