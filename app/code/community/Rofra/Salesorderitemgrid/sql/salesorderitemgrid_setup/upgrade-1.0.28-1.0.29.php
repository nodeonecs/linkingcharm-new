<?php
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$setup->startSetup();

$setup->run("
	ALTER TABLE sales_flat_quote_item ADD COLUMN checkout_method varchar(255);
	ALTER TABLE sales_flat_quote_item ADD COLUMN store_name varchar(255) default 0;
	ALTER TABLE sales_flat_quote_item ADD COLUMN store_address varchar(255) default 0;
	ALTER TABLE sales_flat_quote_item ADD COLUMN store_city varchar(255) default 0;
	ALTER TABLE sales_flat_quote_item ADD COLUMN store_pincode varchar(255) default 0;
	ALTER TABLE sales_flat_quote_item ADD COLUMN store_zone varchar(255) default 0;
	ALTER TABLE sales_flat_quote_item ADD COLUMN store_region varchar(255) default 0;

	ALTER TABLE sales_flat_order_item ADD COLUMN checkout_method varchar(255);
	ALTER TABLE sales_flat_order_item ADD COLUMN store_name varchar(255) default 0;
	ALTER TABLE sales_flat_order_item ADD COLUMN store_address varchar(255) default 0;
	ALTER TABLE sales_flat_order_item ADD COLUMN store_city varchar(255) default 0;
	ALTER TABLE sales_flat_order_item ADD COLUMN store_pincode varchar(255) default 0;
	ALTER TABLE sales_flat_order_item ADD COLUMN store_zone varchar(255) default 0;
	ALTER TABLE sales_flat_order_item ADD COLUMN store_region varchar(255) default 0;

");

$setup->endSetup();
