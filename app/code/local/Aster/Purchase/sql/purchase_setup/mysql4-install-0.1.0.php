<?php
$installer = $this;
$installer->startSetup();
$sql=<<<SQLTEXT
DROP TABLE IF EXISTS `purchase_master`;
create table IF NOT EXISTS purchase_master(purchase_id int not null auto_increment,
sku varchar(256),
avg_price double,
po_number varchar(256),
qty int(11), 
inward_date DATETIME NOT NULL,
created_at DATETIME NOT NULL,
updated_at DATETIME on update CURRENT_TIMESTAMP NOT NULL,
primary key(purchase_id));
SQLTEXT;

$installer->run($sql);
//demo 
//Mage::getModel('core/url_rewrite')->setId(null);
//demo 
$installer->endSetup();
	 