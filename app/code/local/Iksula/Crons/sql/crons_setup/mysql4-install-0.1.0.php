<?php
$installer = $this;
$installer->startSetup();
$sql=<<<SQLTEXT
create table tms_crons(id int not null auto_increment, cronone_status varchar(100), crontwo_status varchar(100), cronthree_status varchar(100), primary key(id));
SQLTEXT;

$installer->run($sql);

$installer->endSetup();
	 