<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_Model_Observer_Giftcard
{
    /**
     * Adds Gift Cards block to checkout/cart
     *
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function coreBlockAbstractToHtmlAfter(Varien_Event_Observer $observer)
    {
        if (!Mage::helper('aw_giftcard2/config')->isEnabled()) {
            return $this;
        }

        if (
            (Mage::app()->getRequest()->getRequestedRouteName() == 'checkout'
                && Mage::app()->getRequest()->getControllerName() == 'cart'
                && Mage::app()->getRequest()->getActionName() == 'index')
            || Mage::helper('aw_giftcard2')->isMobileVersionEnabled()
        ) {
            if ($observer->getBlock() instanceof Mage_Checkout_Block_Cart_Coupon) {
                $couponBlockHtml = $observer->getTransport()->getHtml();
                $giftcardBlockHtml = Mage::app()->getLayout()->createBlock('aw_giftcard2/checkout_cart_giftcard')->toHtml();

                if (Mage::helper('aw_giftcard2')->isMobileVersionEnabled()) {
                    $search = '<div class="panel__container-shadow"></div>';
                    $result = str_replace($search, $giftcardBlockHtml.$search, $couponBlockHtml);
                } else {
                    $result = $couponBlockHtml . $giftcardBlockHtml;
                }

                $observer->getTransport()->setHtml($result);
            }
        }
        return $this;
    }

    /**
     * Hides all payments except Zero Subtotal Checkout when an order will be paid by Gift Cards only
     *
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function paymentMethodIsActive(Varien_Event_Observer $observer)
    {
        if (!Mage::helper('aw_giftcard2/config')->isEnabled()) {
            return $this;
        }

        $quote = $observer->getEvent()->getQuote();
        $paymentMethod = $observer->getEvent()->getMethodInstance()->getCode();
        $result = $observer->getEvent()->getResult();

        if (
            $quote
            && $quote->getBaseGrandTotal() == 0
            && null !== $quote->getBaseAwGCAmount()
            // Compatibility with AW_Points
            && 0 == ($quote->hasBaseMoneyForPoints() ? $quote->getBaseMoneyForPoints() : 0)
        ) {

            if ($paymentMethod === 'free' && !$result->isDeniedInConfig) {
                $result->isAvailable = true;
            } else {
                $result->isAvailable = false;
            }

        }
        
        // Hide COD for gift card.
        if($paymentMethod == 'phoenix_cashondelivery') {
            foreach ($quote->getAllItems() as $item) {
                if ($item->getProductType() == 'aw_giftcard2') {
                    $result->isAvailable = false;
                }
            }
        }

        return $this;
    }

    /**
     * Decreases balance of applied Gift Card after an order was placed
     *
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function salesOrderPlaceAfter(Varien_Event_Observer $observer)
    {
        if (!Mage::helper('aw_giftcard2/config')->isEnabled()) {
            return $this;
        }

        $order = $observer->getEvent()->getOrder();
        $orderGiftCards = Mage::helper('aw_giftcard2/giftcard')->getQuoteGiftCards($order->getQuoteId());
        if (count($orderGiftCards) > 0) {
            $order->setAwGiftCardsUsed($orderGiftCards);
            foreach ($orderGiftCards as $card) {
                $giftCardModel = Mage::getModel('aw_giftcard2/giftcard')->load($card->getGiftcardId());
                $giftCardModel
                    ->setOrder($order)
                    ->setBalance($giftCardModel->getBalance() - $card->getBaseGiftcardAmount())
                    ->save()
                ;

                if ($giftCardModel->getOrderId()) {
                    $order = Mage::getModel('sales/order')->load($giftCardModel->getOrderId());
                    Mage::helper('aw_giftcard2/statistics')->updateStatistics(
                        $giftCardModel->getProductId(),
                        $giftCardModel->getOrderId(),
                        $order->getStoreId(),
                        array (
                            'used_qty'     => $giftCardModel->isUsed() ? 1 : 0,
                            'used_amount'  => $card->getBaseGiftcardAmount()
                        )
                    );
                }
            }
        }
        return $this;
    }

    /**
     * Attaches info about used Gift Cards to an order
     *
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function salesOrderLoadAfter(Varien_Event_Observer $observer)
    {
        if (!Mage::helper('aw_giftcard2/config')->isEnabled()) {
            return $this;
        }
        $order = $observer->getEvent()->getOrder();
        $orderGiftCards = Mage::helper('aw_giftcard2/giftcard')->getQuoteGiftCards($order->getQuoteId());

        if (count($orderGiftCards) > 0) {
            $order->setAwGiftCardsUsed($orderGiftCards);
        }

        if (
            !$order->isCanceled()
            && $order->getState() != Mage_Sales_Model_Order::STATE_CLOSED
            && !$order->canCreditmemo()
            && count($orderGiftCards) > 0
        ) {
            foreach ($order->getAllItems() as $item) {
                if ($item->canRefund()) {
                    $order->setForcedCanCreditmemo(true);
                }
            }
        }
        return $this;
    }

    /**
     * Attaches info about used Gift Cards to an invoice
     *
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function salesOrderInvoiceLoadAfter(Varien_Event_Observer $observer)
    {
        if (!Mage::helper('aw_giftcard2/config')->isEnabled()) {
            return $this;
        }
        $invoice = $observer->getEvent()->getInvoice();
        $invoiceGiftCards = Mage::helper('aw_giftcard2/giftcard')->getInvoiceGiftCards($invoice->getId());
        if (count($invoiceGiftCards) > 0) {
            $invoice->setAwGiftCardsUsed($invoiceGiftCards);
        }
        return $this;
    }

    /**
     * Attaches info about used Gift Cards to a credit memo
     *
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function salesOrderCreditMemoLoadAfter(Varien_Event_Observer $observer)
    {
        if (!Mage::helper('aw_giftcard2/config')->isEnabled()) {
            return $this;
        }
        $creditMemo = $observer->getEvent()->getCreditmemo();
        $creditMemoGiftCards = Mage::helper('aw_giftcard2/giftcard')->getCreditMemoGiftCards($creditMemo->getId());

        if (count($creditMemoGiftCards) > 0) {
            $creditMemo->setAwGiftCardsUsed($creditMemoGiftCards);
        }
        return $this;
    }

    /**
     * Saves info about used Gift Cards from an invoice
     *
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function salesOrderInvoiceSaveAfter(Varien_Event_Observer $observer)
    {
        if (!Mage::helper('aw_giftcard2/config')->isEnabled()) {
            return $this;
        }

        $invoice = $observer->getEvent()->getInvoice();
        if (!$invoice->getAwGiftCardsUsed() || count($invoice->getAwGiftCardsUsed()) == 0) {
            return $this;
        }

        foreach ($invoice->getAwGiftCardsUsed() as $invoiceGiftCard) {
            if ($this->_isInvoiceGiftCardNotSaved(
                    $invoice->getId(),
                    $invoice->getOrderId(),
                    $invoiceGiftCard->getGiftcardId()
                )
            ) {
                $invoiceGiftCardModel = Mage::getModel('aw_giftcard2/giftcard_invoice');
                $invoiceGiftCardModel
                    ->setInvoiceId($invoice->getId())
                    ->setGiftcardId($invoiceGiftCard->getGiftcardId())
                    ->setOrderId($invoice->getOrderId())
                    ->setBaseGiftcardAmount($invoiceGiftCard->getBaseGiftcardAmount())
                    ->setGiftcardAmount($invoiceGiftCard->getGiftcardAmount())
                ;
                try {
                    $invoiceGiftCardModel->save();
                } catch (Exception $e) {
                    Mage::logException($e);
                }
            }
        }
        return $this;
    }

    /**
     * Check if Gift card data is not saved for selected invoice
     *
     * @param $invoiceId
     * @param $orderId
     * @param $giftcardId
     * @return bool
     */
    protected function _isInvoiceGiftCardNotSaved($invoiceId, $orderId, $giftcardId) {
        $collection = Mage::getModel('aw_giftcard2/giftcard_invoice')->getCollection()
            ->setFilterByInvoiceId($invoiceId)
            ->setFilterByOrderId($orderId)
            ->setFilterByGiftCardId($giftcardId)
        ;
        if ($collection->getSize() > 0) {
            return false;
        }
        return true;
    }

    /**
     * Saves info about used Gift Cards from an credit memo
     *
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function salesOrderCreditMemoSaveAfter(Varien_Event_Observer $observer)
    {
        if (!Mage::helper('aw_giftcard2/config')->isEnabled()) {
            return $this;
        }

        $creditMemo = $observer->getEvent()->getCreditmemo();
        if (!$creditMemo->getAwGiftCardsUsed() || count($creditMemo->getAwGiftCardsUsed()) == 0) {
            return $this;
        }

        foreach ($creditMemo->getAwGiftCardsUsed() as $creditMemoGiftCard) {
            if ($this->_isCreditMemoGiftCardNotSaved(
                    $creditMemo->getId(),
                    $creditMemo->getOrderId(),
                    $creditMemoGiftCard->getGiftcardId()
                )
            ) {
                $creditMemoGiftCardModel = Mage::getModel('aw_giftcard2/giftcard_creditmemo');
                $creditMemoGiftCardModel
                    ->setCreditmemoId($creditMemo->getId())
                    ->setGiftcardId($creditMemoGiftCard->getGiftcardId())
                    ->setOrderId($creditMemo->getOrderId())
                    ->setBaseGiftcardAmount($creditMemoGiftCard->getBaseGiftcardAmount())
                    ->setGiftcardAmount($creditMemoGiftCard->getGiftcardAmount());
                try {
                    $creditMemoGiftCardModel->save();
                } catch (Exception $e) {
                    Mage::logException($e);
                }
            }
        }
        return $this;
    }

    /**
     * Check if Gift card data is not saved for selected credit memo
     *
     * @param $invoiceId
     * @param $orderId
     * @param $giftcardId
     * @return bool
     */
    protected function _isCreditMemoGiftCardNotSaved($creditMemoId, $orderId, $giftcardId) {
        $collection = Mage::getModel('aw_giftcard2/giftcard_creditmemo')->getCollection()
            ->setFilterByCreditMemoId($creditMemoId)
            ->setFilterByOrderId($orderId)
            ->setFilterByGiftCardId($giftcardId)
        ;
        if ($collection->getSize() > 0) {
            return false;
        }
        return true;
    }

    /**
     * Restore Gift Card balance after refund of an order
     *
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function salesOrderCreditMemoRefund(Varien_Event_Observer $observer)
    {
        if (!Mage::helper('aw_giftcard2/config')->isEnabled()) {
            return $this;
        }

        $creditMemo = $observer->getEvent()->getCreditmemo();

        foreach ($creditMemo->getAwGiftCardsUsed() as $creditMemoGiftCard) {
            if ($creditMemoGiftCard->getBaseGiftcardAmount() > 0) {
                $giftCardModel = Mage::getModel('aw_giftcard2/giftcard')->load($creditMemoGiftCard->getGiftcardId());

                $statsData = array (
                    'used_qty'     => $giftCardModel->isUsed() ? -1 : 0,
                    'used_amount'  => -$creditMemoGiftCard->getBaseGiftcardAmount()
                );

                $giftCardModel
                    ->setCreditmemo($creditMemo)
                    ->setBalance($giftCardModel->getBalance() + $creditMemoGiftCard->getBaseGiftcardAmount());
                try {
                    $giftCardModel->save();

                    if ($giftCardModel->getOrderId()) {
                        Mage::helper('aw_giftcard2/statistics')->updateStatistics(
                            $giftCardModel->getProductId(),
                            $giftCardModel->getOrderId(),
                            $creditMemo->getStoreId(),
                            $statsData
                        );
                    }
                } catch (Exception $e) {
                    Mage::logException($e);
                }
            }
        }
        return $this;
    }

    /**
     * Restore Gift Card balance after an order was cancelled
     *
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function salesOrderPaymentCancel(Varien_Event_Observer $observer)
    {
        if (!Mage::helper('aw_giftcard2/config')->isEnabled()) {
            return $this;
        }

        $payment = $observer->getEvent()->getPayment();
        $order = $payment->getOrder();

        foreach ($order->getAwGiftCardsUsed() as $orderGiftCard) {
            if ($orderGiftCard->getBaseGiftcardAmount() > 0) {
                $giftCardModel = Mage::getModel('aw_giftcard2/giftcard')->load($orderGiftCard->getGiftcardId());

                $statsData = array (
                    'used_qty'     => $giftCardModel->isUsed() ? -1 : 0,
                    'used_amount'  => -$orderGiftCard->getBaseGiftcardAmount()
                );

                $giftCardModel
                    ->setCreditmemo($order)
                    ->setBalance($giftCardModel->getBalance() + $orderGiftCard->getBaseGiftcardAmount());
                try {
                    $giftCardModel->save();

                    if ($giftCardModel->getOrderId()) {
                        Mage::helper('aw_giftcard2/statistics')->updateStatistics(
                            $giftCardModel->getProductId(),
                            $giftCardModel->getOrderId(),
                            $order->getStoreId(),
                            $statsData
                        );
                    }
                } catch (Exception $e) {
                    Mage::logException($e);
                }
            }
        }
        return $this;
    }

    /**
     * Adds Gift Cards from old quote to current quote
     *
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function salesQuoteMergeAfter(Varien_Event_Observer $observer)
    {
        if (!Mage::helper('aw_giftcard2/config')->isEnabled()) {
            return $this;
        }

        $quote = $observer->getEvent()->getQuote();
        $oldQuote = $observer->getEvent()->getSource();

        $quoteGiftCards = Mage::helper('aw_giftcard2/giftcard')-> getQuoteGiftCards($oldQuote->getId());
        foreach ($quoteGiftCards as $quoteGiftcard) {
            $giftCard = Mage::getModel('aw_giftcard2/giftcard')->load($quoteGiftcard->getGiftcardId());
            if ($giftCard->getId()) {
                try {
                    Mage::helper('aw_giftcard2/giftcard')->addToQuote($giftCard, $quote->getId());
                } catch (Exception $e) {
                    //Mage::logException($e);
                }
                try {
                    Mage::helper('aw_giftcard2/giftcard')->removeFromQuote($quoteGiftcard->getGiftcardId(), $oldQuote->getId());
                } catch (Exception $e) {
                   //Mage::logException($e);
                }
            }
        }
        return $this;
    }

    public function paypalPrepareLineItems(Varien_Event_Observer $observer)
    {
        if (!Mage::helper('aw_giftcard2/config')->isEnabled()) {
            return $this;
        }

        $paypalCart = $observer->getEvent()->getPaypalCart();
        $salesEntity = $paypalCart->getSalesEntity();

        if (null === $salesEntity) {
            return $this;
        }

        $giftCards = null;
        if ($salesEntity instanceof Mage_Sales_Model_Quote) {
            $giftCards = Mage::helper('aw_giftcard2/giftcard')->getQuoteGiftCards($salesEntity->getId());
        }
        if ($salesEntity instanceof Mage_Sales_Model_Order) {
            $giftCards = Mage::helper('aw_giftcard2/giftcard')->getQuoteGiftCards($salesEntity->getQuoteId());
        }

        if (null === $giftCards || count($giftCards) == 0) {
            return $this;
        }

        $baseAmount = 0;
        foreach ($giftCards as $giftCard) {
            $baseAmount += $giftCard->getBaseGiftcardAmount();
        }

        $baseAmount = round($baseAmount, 4);
        if ($baseAmount > 0.0001) {
            $paypalCart->updateTotal(
                Mage_Paypal_Model_Cart::TOTAL_DISCOUNT, $baseAmount,
                Mage::helper('aw_giftcard2')->__(
                    'Gift Card (%s)', Mage::app()->getStore()->convertPrice($baseAmount, true, false)
                )
            );
        }
        return $this;
    }

}