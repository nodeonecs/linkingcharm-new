<?php

class Magecomp_GstCharge_Model_Sales_Order_Creditmemo_Total_Igstcharge extends Mage_Sales_Model_Order_Creditmemo_Total_Abstract
{
	public function collect(Mage_Sales_Model_Order_Creditmemo $creditmemo)
    {
    	$creditmemo->setIgstCharge(0);
        $creditmemo->setBaseIgstCharge(0);

        $amount = $creditmemo->getOrder()->getIgstCharge();        
        $creditmemo->setIgstCharge($amount);
        
        $creditmemoParams = Mage::app()->getRequest()->getParam('creditmemo');
        $totalIgstCharge = 0;
        $creditMemoItemList = array();
        foreach ($creditmemoParams as $items) {
            foreach ($items as $itemId => $item) {
                if($item['qty']==1){
                    $creditMemoItemList[] = $itemId;
                }
            }
        }

        foreach ($creditmemo->getAllItems() as $item) {
            $orderItem = $item->getOrderItem();

            if ($orderItem->isDummy()) {
                continue;
            }

            if(in_array($orderItem->getId(), $creditMemoItemList)){
                $totalIgstCharge +=$orderItem->getIgstCharge();
            }
        }

		$quoteId=$creditmemo->getOrder()->getQuoteId();
		//$quote = Mage::getModel('sales/quote')->load($quoteId);
		$quote = Mage::getModel('sales/quote')->getCollection()
    				 ->addFieldToFilter('entity_id', $quoteId)
                     ->getFirstItem();
		$exckudingTax=$quote->getExclPrice();
		
        $amount = $creditmemo->getOrder()->getBaseIgstCharge();
        $creditmemo->setBaseIgstCharge($amount);
		
        $creditmemo->setIgstCharge($totalIgstCharge);
        $creditmemo->setBaseIgstCharge($totalIgstCharge);

        $subTotal = $creditmemo->getSubtotal();
        $baseSubTotal = $creditmemo->getSubtotal();

        $grandTotal = $creditmemo->getSubtotal();

        $creditmemo->setGrandTotal($grandTotal);
        $creditmemo->setBaseGrandTotal($grandTotal);

		if($exckudingTax)
		{
        	$creditmemo->setGrandTotal($creditmemo->getGrandTotal() + $creditmemo->getIgstCharge());
        	$creditmemo->setBaseGrandTotal($creditmemo->getBaseGrandTotal() + $creditmemo->getBaseIgstCharge());
		}
        
        $creditmemo->setIgstCharge($totalIgstCharge);
        $creditmemo->setBaseIgstCharge($totalIgstCharge);
        

        return $this;
    }
}