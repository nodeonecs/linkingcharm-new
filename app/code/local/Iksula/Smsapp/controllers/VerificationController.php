<?php
class Iksula_Smsapp_VerificationController extends Mage_Core_Controller_Front_Action{
	
	/**
	 * Purpose: To verify order
	 * Input: order id
	 * Output: verification status (html)
	 */	
	public function verifyAction(){
		$OrderData  = $_GET['ver'];
		$orderArray = explode('/',$OrderData);
		$orderId 	= $orderArray[0];
		$expireTime = $orderArray[1];
		$order 		= Mage::getModel('sales/order')->loadByIncrementId($orderId);
		$status     = $order->getStatus();
		
		if(isset($status)){
			$payment_method_code = $order->getPayment()->getMethodInstance()->getCode();
			if($payment_method_code == 'phoenix_cashondelivery' || $payment_method_code == 'cashondelivery'){				
				if($status == 'cod_verified'){
					$newstate = 'processing';
		        	$newstatus = 'processing';
		        	$comment = 'Customer has verified cod payment';
		        	$isCustomerNotified = false;
		        	$order->setState($newstate, $newstatus, $comment, $isCustomerNotified);		        
		        	$order->save();
		        	$this->setOrderItemsStatusByProcessing($order);
		        	/* PO Data not saved using Observer */
		        	$poData = Mage::getModel('omniapis/storepo')->getCollection()->addFieldToFilter('order_id',array('eq'=>$order->getId()))->getData();
			            foreach ($poData as $pod) {
			                Mage::getModel('omniapis/storepo')->load($pod['id'])->setStatus($newstatus)->save();
			            }
			        /* PO Data not saved using Observer */
		        	$this->layoutChange('codverification/verify_success.phtml');
				}
				else if($status == 'canceled'){
					$this->layoutChange('codverification/verify_canceled.phtml');
				} 
				else {
						$this->layoutChange('codverification/verify.phtml');
					}
			}elseif($payment_method_code == 'payatstore'){
				//$this->setOrderItemsStatusByProcessing($order);
				if($status == 'pending'){
					$newstate = 'processing';
		        	$newstatus = 'processing';
		        	$comment = 'Customer has verified Pay at store payment';
		        	$isCustomerNotified = false;
		        	$order->setState($newstate, $newstatus, $comment, $isCustomerNotified);
		        	$order->save();
		        	$this->setOrderItemsStatusByProcessing($order);
		        	
		        	/* PO Data not saved using Observer */
		        	$poData = Mage::getModel('omniapis/storepo')->getCollection()->addFieldToFilter('order_id',array('eq'=>$order->getId()))->getData();
			            foreach ($poData as $pod) {
			                Mage::getModel('omniapis/storepo')->load($pod['id'])->setStatus($newstatus)->save();
			            }
			        /* PO Data not saved using Observer */
		        	$this->layoutChange('payatstore/verify_success.phtml');
				}

			}elseif($payment_method_code == 'cashpayment'){
				if($status == 'pending'){
					$newstate = 'processing';
		        	$newstatus = 'processing';
		        	$comment = 'Customer has verified Cash payment';
		        	$isCustomerNotified = false;
		        	$order->setState($newstate, $newstatus, $comment, $isCustomerNotified);		        	
		        	$order->save();
		        	$this->setOrderItemsStatusByProcessing($order);
		        	/* PO Data not saved using Observer */
		        	$poData = Mage::getModel('omniapis/storepo')->getCollection()->addFieldToFilter('order_id',array('eq'=>$order->getId()))->getData();
			            foreach ($poData as $pod) {
			                Mage::getModel('omniapis/storepo')->load($pod['id'])->setStatus($newstatus)->save();
			            }
			        /* PO Data not saved using Observer */
		        	$this->layoutChange('cashpayment/verify_success.phtml');
				}

			}
			else{
				
				$this->layoutChange('codverification/verify_fail.phtml');
			}	
	  }
	  else{
	  	$this->layoutChange('codverification/no_order.phtml');
	  }	
	}

	public function layoutChange($phtml){
		$this->loadLayout();
		$verifyBlock = $this->getLayout()->createBlock('core/template')->setTemplate($phtml);
		$this->getLayout()->getBlock('content')->append($verifyBlock);
		$this->renderLayout();
	}

/***********Set the Order Item state and Status to Processing ***************/
	public function setOrderItemsStatusByProcessing($orderobj){  

		foreach($orderobj->getAllItems() as $OrderItemsObj){
				if($OrderItemsObj->getCheckoutMethod() != 'pickable'){ //  For Deliverable Product only (Also Configurable products)

                                $OrderItemsIds = $OrderItemsObj->getItemId();
                                $OrderItemsStatusStatedata = array('item_status'=> 'processing','item_state'=>'processing');

                                $model = Mage::getModel('sales/order_item')->load($OrderItemsIds)->addData($OrderItemsStatusStatedata);
                                try {
                                    $model->setId($OrderItemsIds)->save();
                                } catch (Exception $e){
                                    Mage::getSingleton('core/session')->addError($e->getMessage());
                                }                            
                }
        }
	}
}
