<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   BV
 * @package    BV_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */


class Brainvire_Core_Block_Adminhtml_Log extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {
        $this->_controller = 'adminhtml_log';
        $this->_blockGroup = 'brainvirecore';
        $this->_headerText = Mage::helper('brainvirecore')->__('Extensions Log');

        parent::__construct();

        $this->setTemplate('widget/grid/container.phtml');
        $this->_removeButton('add');
        $this->_addButton('clear', array(
            'label' => Mage::helper('brainvirecore')->__('Clear Log'),
            'onclick' => 'if(confirm(\'' . Mage::helper('brainvirecore')->__('Are you sure to clear all log entries?') . '\'))setLocation(\'' . $this->getClearUrl() . '\')',
            'class' => 'delete',
        ));
    }

    
    public function getClearUrl() {
        return Mage::getSingleton('adminhtml/url')->getUrl('brainvirecore_admin/viewlog/clear');
    }

}