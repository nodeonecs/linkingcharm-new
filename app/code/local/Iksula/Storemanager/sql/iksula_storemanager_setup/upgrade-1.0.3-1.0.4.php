<?php
/**
 * Iksula_Storemanager extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category       Iksula
 * @package        Iksula_Storemanager
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Storemanager module install script
 *
 * @category    Iksula
 * @package     Iksula_Storemanager
 * @author      Ultimate Module Creator
 */

$installer = $this;
$installer->startSetup();

$installer->run("
DROP TABLE IF EXISTS {$this->getTable('iksula_storemanager/store_hub_assigned')};
CREATE TABLE IF NOT EXISTS {$this->getTable('iksula_storemanager/store_hub_assigned')} (
  entity_id int(11) NOT NULL auto_increment,
  store_id int(11) default 0,
  hub_id int(11) default 0,
  website_id int(11) default 0,
  PRIMARY KEY (entity_id)
 ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;"
);
//$this->run("DROP TABLE IF EXISTS {iksula_storemanager/store_store}");



$installer->endSetup();
