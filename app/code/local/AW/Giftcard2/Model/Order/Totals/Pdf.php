<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_Model_Order_Totals_Pdf extends Mage_Sales_Model_Order_Pdf_Total_Default
{
    public function getTotalsForDisplay()
    {
        $_result = array();
        if ($this->getSource()->getAwGiftCardsUsed() && count($this->getSource()->getAwGiftCardsUsed()) > 0) {
            $fontSize = $this->getFontSize() ? $this->getFontSize() : 7;
            $_giftCardTotals = array();
            foreach ($this->getSource()->getAwGiftCardsUsed() as $usedGiftCard) {
                $_giftCardTotals['aw_giftcard2_' . $usedGiftCard->getGiftcardId()] = array (
                    'amount'    => '-' . $this->getOrder()->formatPriceTxt($usedGiftCard->getGiftcardAmount()),
                    'label'     => Mage::helper('aw_giftcard2')->__('Gift Card (%s)', $usedGiftCard->getCode()),
                    'font_size' => $fontSize,
                );
            }
            $_result = $_giftCardTotals;
        }
        return $_result;
    }
}