<?php
/**
* Brainvire Infotech Pvt. Ltd
  * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */


class Brainvire_Points_Model_Transaction extends Mage_Core_Model_Abstract {
    const ACTION_WRITING_REVIEW = 'rewiew_write';
    const ACTION_TAGGING_PRODUCT = 'tag_product';
    const ACTION_REFERAL_REGISTERED = 'ref_registation';
    const ACTION_REFERAL_PAYED_ORDER = 'ref_order_payed';
    const ACTION_NEWSLETTER_SIGNUP = 'newsletter_signup';
    const ACTION_POINTS_EXPIRATION = 'points_expiration';
    const ACTION_POINTS_ADDED_BY_ADMIN = 'added_by_admin';
    const ACTION_OTHER = 'other';

    const COMMENT_REFERAL_PAYED_ORDER = 'Reward for invited user placed order';
    const COMMENT_REFERAL_REGISTERED = 'Reward for registration of invited user %s';

    public function _construct() {
        parent::_construct();
        $this->_init('points/transaction');
    }

    public function getCustomer() {
        return Mage::getModel('points/summary')->load($this->getSummaryId())->getCustomer();
    }

    public function loadByOrder($order) {
        $this->getResource()->loadByOrderIncrementId($this, $order->getIncrementId());
        return $this;
    }

    
    public function changePoints($amount, $action, $summary, $additionalData = array()) {
        if (!($summary instanceof Brainvire_Points_Model_Summary) || !$summary->getId()) {
            throw new Brainvire_Core_Exception(Mage::helper('points')->__('Cannot load summary, action - %s, amount - %s', $action, $amount));
        }

        if (!$amount)
            throw new Exception(Mage::helper('points')->__('Zero transaction amount'));

        $customer = $summary->getCustomer();

        if (!$customer->getId())
            throw new Exception(Mage::helper('points')->__('Guest can not work with points'));

        $expirationDate = null;
        if ($amount > 0)
            $expirationDate = isset($additionalData['expiration_date']) ? $additionalData['expiration_date'] : $this->_prepareExpirationDate();

        if (isset($additionalData['comment']))
            $additionalData['comment'] = htmlspecialchars($additionalData['comment']);
        if (isset($additionalData['notice']))
            $additionalData['notice'] = htmlspecialchars($additionalData['notice']);

        $this
                ->addData($additionalData)
                ->setSummaryId($summary->getId())
                ->setAction($action)
                ->setChangeDate(Mage::getModel('core/date')->gmtDate())
                ->setExpirationDate($expirationDate)
                ->setBalanceChange($amount)
                ->setCustomerName($customer->getName())
                ->setCustomerEmail($customer->getEmail())
                ->save();

        $summary
                ->setPoints($summary->getPoints() + $amount)
                ->save();

        return $this;
    }

    public function getActionInstance() {
        return
                        Brainvire_Points_Model_Actions_Abstract::getInstance($this->getAction(), $this->getCustomer())
                        ->setTransaction($this);
    }

    protected function _prepareExpirationDate() {
        if ($addDays = Mage::helper('points/config')->getPointsExpirationDays()) {
            $newDate = new Zend_Date();
            return $newDate->addDay($addDays)->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
        }
        return;
    }

    public function saveSpendOrderInfo($order) {
        $this->getResource()->saveSpendOrderInfo($this, $order);
        return $this;
    }

}
