<?php
/**
 * Iksula_Storemanager extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category       Iksula
 * @package        Iksula_Storemanager
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Store comment form block
 *
 * @category    Iksula
 * @package     Iksula_Storemanager
 * @author Ultimate Module Creator
 */
class Iksula_Storemanager_Block_Store_Comment_Form extends Mage_Core_Block_Template
{
    /**
     * initialize
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        $customerSession = Mage::getSingleton('customer/session');
        parent::__construct();
        $data =  Mage::getSingleton('customer/session')->getStoreCommentFormData(true);
        $data = new Varien_Object($data);
        // add logged in customer name as nickname
        if (!$data->getName()) {
            $customer = $customerSession->getCustomer();
            if ($customer && $customer->getId()) {
                $data->setName($customer->getFirstname());
                $data->setEmail($customer->getEmail());
            }
        }
        $this->setAllowWriteCommentFlag(
            $customerSession->isLoggedIn() ||
            Mage::getStoreConfigFlag('iksula_storemanager/store/allow_guest_comment')
        );
        if (!$this->getAllowWriteCommentFlag()) {
            $this->setLoginLink(
                Mage::getUrl(
                    'customer/account/login/',
                    array(
                        Mage_Customer_Helper_Data::REFERER_QUERY_PARAM_NAME => Mage::helper('core')->urlEncode(
                            Mage::getUrl('*/*/*', array('_current' => true)) .
                            '#comment-form'
                        )
                    )
                )
            );
        }
        $this->setCommentData($data);
    }

    /**
     * get current store
     *
     * @access public
     * @return Iksula_Storemanager_Model_Store
     * @author Ultimate Module Creator
     */
    public function getStore()
    {
        return Mage::registry('current_store');
    }

    /**
     * get form action
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getAction()
    {
        return Mage::getUrl(
            'iksula_storemanager/store/commentpost',
            array('id' => $this->getStore()->getId())
        );
    }
}
