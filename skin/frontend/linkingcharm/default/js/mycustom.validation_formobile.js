
var validation_name;
var mobile_no_exists = false;
jQuery(document).ready(function($){

    function autofillajax(customer_zip){
        var zip_status = false;
        $.each(all_zip_codes, function(key, value) {
            if(value['postcode'] == customer_zip){
                zip_status = true;
                validate_email = true;
                $('.checkout_error_zip').remove();
                $('#billing\\:postcode').css('border','1px solid #B6B6B6');
                $('.checkout_city').val(value['city']);
                $('.checkout_state').val(value['region_id']);
            }
        });

        if(zip_status == false){
            validate_email = false;
            var pin_advice = '<div class="custom_validator checkout_error_zip validation-advice">Invalid pincode. Please enter a valid pincode</div>';
            if(!$('.checkout_error_zip').length){
                $('#billing\\:postcode').after(pin_advice);
                $('#billing\\:postcode').css('border','1px solid #e9322d');

            }
        }
    }

    function validate_name(field,field_name,error_name){
        if(field == null || field == undefined || field == ''){
            var name_advice = '<div class="custom_validator checkout_error_name">Please enter your ' + error_name +'</div>';
            if(!$('.error_' + field_name).length){
                $('#billing\\:' + field_name).after(name_advice);
                $('#billing\\:' + field_name).css('border','1px solid #e9322d');
                $('.checkout_error_name').addClass('error_' + field_name);
                validation_name = 0;
            }
        }
        else{
            $('#billing\\:' + field_name).css('border','1px solid #B6B6B6');
            $('.error_' + field_name).remove();
            validation_name = 1;
        }
    }

    function validate_address(field,field_name){
        if(field == null || field == undefined || field == ''){
            var addr_advice = '<div class="custom_validator checkout_error_address">Please enter your full address </div>';
             if(!$('.error_' + field_name).length){
                $('#billing\\:' + field_name).after(addr_advice);
                $('#billing\\:' + field_name).css('border','1px solid #e9322d');
                $('.checkout_error_address').addClass('error_' + field_name);
                validation_addr = 0;
            }
        }
        else{
            $('#billing\\:' + field_name).css('border','1px solid #B6B6B6');
            $('.error_' + field_name).remove();
            validation_addr = 1;
        }
    }

    function validate_telephone_data(field,field_name){
        var mobile = parseInt(field);
        //Check if user entered mobile already exists in database
        $.each(all_customer_data,function(key,value){
            if($.trim(value['mobile']) == mobile)
            {   
                mobile_no_exists = true;
                return false;
            }
            else{
                mobile_no_exists = false;
            }
        });
        if(field.length < 10 || isNaN(mobile) || mobile_no_exists == true){
            
            if(field.length < 10){
                var tel_advice = '<div class="custom_validator checkout_error_tel">Please enter 10 digit mobile no </div>'; 
            }
            else if(mobile_no_exists == true){
                var tel_advice = '<div class="custom_validator checkout_error_tel">Mobile no already exists.</div>'; 
            }
           
            //if(!$('.error_' + field_name).length){
                $('.error_' + field_name).remove();
                $('#billing\\:' + field_name).after(tel_advice);
                $('#billing\\:' + field_name).css('border','1px solid #e9322d');
                $('.checkout_error_tel').addClass('error_' + field_name);
                validation_tel = 0;
            //}
        }
        else{
            $('#billing\\:' + field_name).css('border','1px solid #B6B6B6');
            $('.error_' + field_name).remove();
            validation_tel = 1;
        }
    }

    function validate_checkout_card_name(field,field_name,error_name,checkout_field_id){
        if(field == null || field == undefined || field == ''){
            var card_advice = '<div class="custom_validator checkout_error_card">Please enter ' + error_name +'</div>';
            if(!$('.error_' + checkout_field_id).length){
                $('#' + checkout_field_id).after(card_advice);
                $('#' + checkout_field_id).css('border','1px solid #e9322d');
                $('.checkout_error_card').addClass('error_' + checkout_field_id);
                validation_card = 0;
            }
        }
        else{
            $('#' + checkout_field_id).css('border','1px solid #B6B6B6');
            $('.error_' + checkout_field_id).remove();
            validation_card = 1;
        }
    }

    function validate_checkout_card_num(field,field_name,error_name,checkout_field_id){
        if(field.length < 16){
            var card_num_advice = '<div class="custom_validator checkout_card_num_error">Please enter' + error_name + ' digit card number</div>';
             if(!$('.error_' + checkout_field_id).length){
                $('#' + checkout_field_id).after(card_num_advice);
                $('#' + checkout_field_id).css('border','1px solid #e9322d');
                $('.checkout_card_num_error').addClass('error_' + checkout_field_id);
                validation_card_num = 0;
            }
        }
        else{
            $('#' + checkout_field_id).css('border','1px solid #B6B6B6');
            $('.error_' + checkout_field_id).remove();
            validation_card_num = 1;
        }
    }

    function validate_checkout_card_cvv(field,field_name,error_name,checkout_field_id){
        if(field.length < 3){
            var card_num_advice = '<div class="custom_validator checkout_card_cvv_error">Please enter appropriate cvv</div>';
             if(!$('.error_' + checkout_field_id).length){
                $('#' + checkout_field_id).after(card_num_advice);
                $('#' + checkout_field_id).css('border','1px solid #e9322d');
                $('.checkout_card_cvv_error').addClass('error_' + checkout_field_id);
                validation_cvv_num = 0;
            }
        }
        else{
            $('#' + checkout_field_id).css('border','1px solid #B6B6B6');
            $('.error_' + checkout_field_id).remove();
            validation_cvv_num = 1;
        }
    }

    function validate_city_data(field,field_name){
        if(field == null || field == undefined || field == ''){
            var city_advice = '<div class="custom_validator checkout_error_city">Please enter city </div>';
             if(!$('.error_' + field_name).length){
                $('#billing\\:' + field_name).after(city_advice);
                $('#billing\\:' + field_name).css('border','1px solid #e9322d');
                $('.checkout_error_city').addClass('error_' + field_name);
                validation_city = 0;
            }
        }
        else{
            $('#billing\\:' + field_name).css('border','1px solid #B6B6B6');
            $('.error_' + field_name).remove();
            validation_city = 1;
        }
    }

   $('.checkout_postcode').focusout(function(){
        if(!zip_codes_received){
            $.ajax({
                url: basejsonurl,
                success: function(data){
                  all_zip_codes = data;
                  customer_zip = $('.checkout_postcode').val();
                    autofillajax(customer_zip);
                    zip_codes_received = true;
                }
            });
        }
        else{
            customer_zip = $('.checkout_postcode').val();
            autofillajax(customer_zip);
        }   
    });

   $('#billing\\:firstname').focusout(function(){
        var field_name = 'firstname';
        var error_name = 'first name';
        var validate_fname = $('#billing\\:firstname').val();
        validate_name(validate_fname,field_name,error_name);
   });

   $('#billing\\:lastname').focusout(function(){
        var field_name = 'lastname';
        var error_name = 'last name';
        var validate_lname = $('#billing\\:lastname').val();
        validate_name(validate_lname,field_name,error_name);
   });

   $('#billing\\:street1').focusout(function(){
        var field_name = 'street1';
        var validate_street1 = $('#billing\\:street1').val();
        validate_address(validate_street1,field_name);
   });

   $('#billing\\:telephone').focusout(function(){
        var field_name = 'telephone';
        var validate_telephone = $('#billing\\:telephone').val();
        validate_telephone_data(validate_telephone,field_name);
   });

   $('#billing\\:city').focusout(function(){
        var field_name = 'city';
        var validate_city = $('#billing\\:city').val();
        validate_city_data(validate_city,field_name);
   });

   $('#secureebs_standard_cc_owner, #payudebit_standard_cc_owner').live('focusout',function(){
        var field_name = 'cardname';
        var checkout_field_id = $(this).attr('id');
        var error_name = 'name on card';
        var validate_cname = $('#' + checkout_field_id).val();
        validate_checkout_card_name(validate_cname,field_name,error_name,checkout_field_id);  
    });

   $('#payudebit_standard_cc_number, #secureebs_standard_cc_number').live('focusout',function(){
        var field_name = 'cardnum';
        var checkout_field_id = $(this).attr('id');
        var error_name = '16';
        var validate_dname = $('#' + checkout_field_id).val();
        validate_checkout_card_num(validate_dname,field_name,error_name,checkout_field_id);
    });

   $('#payudebit_standard_cc_cid, #secureebs_standard_cc_cid').live('focusout',function(){
        var field_name = 'cardcvv';
        var checkout_field_id = $(this).attr('id');
        var error_name = 'cvv';
        var validate_cvv = $('#' + checkout_field_id).val();
        validate_checkout_card_cvv(validate_cvv,field_name,error_name,checkout_field_id);
    });

});