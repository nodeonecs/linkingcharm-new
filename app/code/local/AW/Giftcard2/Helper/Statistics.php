<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_Helper_Statistics extends Mage_Core_Helper_Abstract
{
    /**
     * * Updates statistics data
     *
     * @param $productId
     * @param $orderId
     * @param $storeId
     * @param array $data
     * @return $this
     * @throws Exception
     */
    public function updateStatistics($productId, $orderId, $storeId, array $data)
    {
        $statsCollection = Mage::getModel('aw_giftcard2/statistics')->getCollection()
            ->addProductFilter($productId)
            ->addOrderFilter($orderId)
            ->addStoreFilter($storeId)
        ;
        if ($statsCollection->getSize() > 0) {
            $stats = $statsCollection->getFirstItem();
        } else {
            $stats = Mage::getModel('aw_giftcard2/statistics');
            $stats->setProductId($productId);
            $stats->setOrderId($orderId);
            $stats->setStoreId($storeId);
        }

        foreach ($data as $key => $value) {
            $stats->setData($key, $stats->getData($key) + $value);
        }

        $stats->save();

        return $this;
    }

    /**
     * Adds empty statistics data
     *
     * @param $productId
     * @return $this
     */
    public function addEmptyStatistics($productId)
    {
        $statsCollection = Mage::getModel('aw_giftcard2/statistics')->getCollection()
            ->addProductFilter($productId)
            ->addOrderFilter(null)
            ->addStoreFilter(0)
        ;
        if ($statsCollection->getSize() > 0) {
            return $this;
        } else {
            $stats = Mage::getModel('aw_giftcard2/statistics')
                ->setProductId($productId)
                ->setOrderId(null)
                ->setStoreId(0)
                ->setPurchasedQty(0)
                ->setPurchasedAmount(0)
                ->setUsedQty(0)
                ->setUsedAmount(0)
            ;
            $stats->save();
        }

        return $this;
    }
}