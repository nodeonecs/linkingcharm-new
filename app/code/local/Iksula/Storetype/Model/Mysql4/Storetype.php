<?php
class Iksula_Storetype_Model_Mysql4_Storetype extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init("storetype/storetype", "id");
    }
}