jQuery(document).ready(function($){
	// var catNavHtml = $("#nav").html();
	// var list_stucture_open_tag = "<li class='level0'>";
	// var list_stucture_close_tag = "</li>";
	// var homeLink = list_stucture_open_tag + $("#nav li").first().html()+ list_stucture_close_tag;
	// var blogLink = list_stucture_open_tag + $("#nav li.blog_link_wrapper").html() + list_stucture_close_tag;
	// var cartLink = list_stucture_open_tag + $("#nav li.cart_link_wrapper").html() + list_stucture_close_tag;
	// var newNavHtml = "<li class='level0 new_nav'><div class='nav_title'>Menu</div><ul>"+catNavHtml+"</ul></li>";

	// $(".new_nav").append(homeLink);
	// $(".new_nav").append(newNavHtml);
	// $(".new_nav").append(cartLink);
	// $(".new_nav").append(blogLink);

	var menu_toogle_arrow = "<div class='toogle_wrap'><span class='menu_toogle_arrow'>&nbsp;</span></div>"
	//var product_img_height = Math.round($(".product-view .product-img-box .product-image").width() * 1.2);
	//$(".product-view .product-img-box .product-image, .mousetrap, .more-views").height(product_img_height);

	$(".cat_nav li.parent").each(function(){
		$(this).prepend(menu_toogle_arrow);
	})

	$(".category_menu_title, .nav-container .overlay_account").click(function(){
		$(".nav-container .overlay_account").stop(true,true).toggle();
		$(".cat_nav li ul").slideUp();
		$(".menu_icon, .menu_icon1").stop(true,true).toggle();
		$(".cat_nav li .toogle_wrap span").stop(true,true).removeClass("menu_toogle_arrow_up");
		$(".cat_nav").stop(true,true).toggle();
		$(".category_menu_title").parent("li").stop(true,true).toggleClass('activ');
		$(".menu_toogle_arrow").stop(true,true).removeClass('menu_toogle_arrow_up');
	})

	$(".cat_nav li .toogle_wrap").live('click', function(){
		var status_sub_nav = $(this).siblings("ul").is(':visible');
		var parent_el = $(this).closest('ul').attr("class");
		if(!status_sub_nav){
			// alert("none");
			$("li .toogle_wrap", "."+parent_el).children('span').removeClass("menu_toogle_arrow_up");;
			$("li", "."+parent_el).children('ul').slideUp();
			$(this).siblings("ul").stop(true,true).slideDown();
			$(this).find("span").stop(true,true).addClass("menu_toogle_arrow_up");
		}else{
			// alert("block");
			$(this).siblings("ul").stop(true,true).slideUp();
			$(this).find("span").stop(true,true).removeClass("menu_toogle_arrow_up");
		}

	});

	jQuery(window).resize(function(){
		var $ = jQuery.noConflict();
		var win_width = $(window).width();
		var cat_nav_status = $(".cat_nav").is(':visible');
		//var product_img_height = Math.round($(".product-view .product-img-box .product-image").width() * 1.2);
		//$(".product-view .product-img-box .product-image, .mousetrap, .more-views").height(product_img_height);
		

		if(win_width <= 640){
			if(cat_nav_status){
				$(".menu_icon").stop(true,true).show();
				$(".menu_icon1").stop(true,true).hide();
			}else{
				$(".menu_icon1").stop(true,true).show();
				$(".menu_icon").stop(true,true).hide();
			}
		}

		// if(win_width >= 640){
		// 	if(!cat_nav_status){
		// 		$(".menu_icon").stop(true,true).show();
		// 		$(".menu_icon1").stop(true,true).hide();
		// 	}else{
		// 		$(".menu_icon1").stop(true,true).show();
		// 		$(".menu_icon").stop(true,true).hide();
		// 	}
		// }
	})

})