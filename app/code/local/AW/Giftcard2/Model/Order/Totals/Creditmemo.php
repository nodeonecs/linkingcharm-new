<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_Model_Order_Totals_Creditmemo extends Mage_Sales_Model_Order_Creditmemo_Total_Abstract
{
    public function collect(Mage_Sales_Model_Order_Creditmemo $creditMemo)
    {
        $_result = parent::collect($creditMemo);

        $baseGrandTotal = $creditMemo->getBaseGrandTotal();
        $grandTotal = $creditMemo->getGrandTotal();

        $baseTotalGiftCardAmount = 0;
        $totalGiftCardAmount = 0;

        $orderId = $creditMemo->getOrderId();
        $creditMemoGiftCards = array();
        if ($grandTotal > 0 && $baseGrandTotal > 0 && !$creditMemo->getId()) {
            /** @var AW_Giftcard2_Model_Giftcard_Invoice $invoiceGiftCard */
            foreach ($this->_getInvoicedGiftCardCodes($orderId) as $invoiceGiftCard) {
                $baseGiftCardAmount = $invoiceGiftCard->getBaseGiftcardAmount();
                $giftCardAmount = $invoiceGiftCard->getGiftcardAmount();

                /** @var AW_Giftcard2_Model_Giftcard_Creditmemo $creditMemoGiftCard */
                foreach ($this->_getRefundedGiftCardCodes($orderId, $invoiceGiftCard->getGiftcardId()) as $creditMemoGiftCard) {
                    $baseGiftCardAmount -= $creditMemoGiftCard->getBaseGiftcardAmount();
                    $giftCardAmount -= $creditMemoGiftCard->getGiftcardAmount();
                }

                if ($baseGiftCardAmount >= $baseGrandTotal) {
                    $baseCardUsedAmount = $baseGrandTotal;
                } else {
                    $baseCardUsedAmount = $baseGiftCardAmount;
                }
                $baseGrandTotal -= $baseCardUsedAmount;

                if ($giftCardAmount >= $grandTotal) {
                    $cardUsedAmount = $grandTotal;
                } else {
                    $cardUsedAmount = $giftCardAmount;
                }
                $grandTotal -= $cardUsedAmount;

                $baseGiftCardAmount = round($baseCardUsedAmount, 4);
                $giftCardAmount = round($cardUsedAmount, 4);

                $baseTotalGiftCardAmount += $baseGiftCardAmount;
                $totalGiftCardAmount += $giftCardAmount;

                if ($baseGiftCardAmount > 0) {
                    $_creditMemoGiftCard = new Varien_Object($invoiceGiftCard->getData());
                    $_creditMemoGiftCard
                        ->setBaseGiftcardAmount($baseGiftCardAmount)
                        ->setGiftcardAmount($giftCardAmount);
                    array_push($creditMemoGiftCards, $_creditMemoGiftCard);
                }
            }
        }

        if ($creditMemo->getId() && $creditMemo->getAwGiftCardsUsed()) {
            foreach ($creditMemo->getAwGiftCardsUsed() as $creditMemoGiftCard) {
                $baseTotalGiftCardAmount += $creditMemoGiftCard->getBaseGiftcardAmount();
                $totalGiftCardAmount += $creditMemoGiftCard->getGiftcardAmount();
            }
        }

        if (count($creditMemoGiftCards) > 0) {
            $creditMemo->setAllowZeroGrandTotal(true);
        }

        $creditMemo
            ->setAwGiftCardsUsed($creditMemoGiftCards)
            ->setBaseAwGCAmount($baseTotalGiftCardAmount)
            ->setAwGCAmount($totalGiftCardAmount)
            ->setBaseGrandTotal($creditMemo->getBaseGrandTotal() - $baseTotalGiftCardAmount)
            ->setGrandTotal($creditMemo->getGrandTotal() - $totalGiftCardAmount)
        ;

        return $_result;
    }

    /**
     * Retrieves gift card codes that had been invoiced in the current order
     *
     * @param $orderId
     * @return mixed
     */
    protected function _getInvoicedGiftCardCodes($orderId)
    {
        /** @var AW_Giftcard2_Model_Resource_Giftcard_Invoice_Collection $collection */
        $collection = Mage::getModel('aw_giftcard2/giftcard_invoice')->getCollection()
            ->setFilterByOrderId($orderId)
            ->addSumAmountsPerGiftCard()
            ->addGiftCardsData()
        ;
        return $collection;
    }

    /**
     * Retrieves gift card codes that got refunded balance in the current order
     *
     * @param int $orderId
     * @param int $giftCardId
     * @return mixed
     */
    protected function _getRefundedGiftCardCodes($orderId, $giftCardId)
    {
        /** @var AW_Giftcard2_Model_Resource_Giftcard_Creditmemo_Collection $collection */
        $collection = Mage::getModel('aw_giftcard2/giftcard_creditmemo')->getCollection()
            ->setFilterByOrderId($orderId)
            ->setFilterByGiftCardId($giftCardId)
        ;
        return $collection;
    }
}