var AWGC2PreviewPopup = Class.create();
AWGC2PreviewPopup.prototype = {
    initialize: function (config) {
        this.form = $(config.formSelector);
        this.popupOverlaySelector = config.popupOverlaySelector;
        this.popupProgressSelector = config.popupProgressSelector;
        this.popupWindowSelector = config.popupWindowSelector;
        this.url = config.url;
    },
    previewClick: function () {
        var me = this;
        var oldAction =  this.form.action;
        this.form.action = this.url;
        if(productAddToCartForm.validator && productAddToCartForm.validator.validate()){
            this.showOverlay();
            this.form.request({
                onSuccess: function (transport) {
                    var response = transport.responseText.evalJSON(true);
                    if (response.success) {
                       $('aw-gc2-product-preview-popup_content').innerHTML = response.content;
                        me.showPreview();
                    } else {
                        me.hideOverlay();
                        alert(response.content);
                    }
                },
            });
        }
        this.form.action = oldAction;
    },
    showPreview: function () {
        this.adjustPopup();
        this.showPopup()
    },
    adjustPopup: function() {
        this.popup = $(this.popupWindowSelector);

        //default size window
        var width = 500;
        var height = 500;

        // AW Mobile 3 compatibility
        var currentWidth = window.innerWidth
            || document.documentElement.clientWidth
            || document.body.clientWidth;
        if (currentWidth < width) {
            width = currentWidth;
        }

        var currentHeight = window.innerHeight
            || document.documentElement.clientHeight
            || document.body.clientHeight;
        if (currentHeight < height) {
            height = currentHeight;
        }

        // set size window
        this.popup.style.width = width + 'px';
        this.popup.style.height = height + 'px';
        this.popup.style.marginLeft = '-' + (width / 2) + 'px';
        this.popup.style.marginTop = '-' + (height / 2) + 'px';
        this.popup.style.top = '50%';
        this.popup.style.left = '50%';
        $('aw-gc2-product-preview-popup_content').style.height = (height - 45) + 'px';
    },
    showOverlay: function () {
        $(this.popupOverlaySelector).show();
        $(this.popupProgressSelector).show();
    },
    hideOverlay: function () {
        $(this.popupProgressSelector).hide()
        $(this.popupOverlaySelector).hide();
    },
    showPopup: function () {
        Effect.Appear($(this.popupWindowSelector), { duration: 0.1 });
        this.clickHandler = this. clickHandler.bind(this);
        $(this.popupOverlaySelector).observe('click', this.clickHandler);
    },
    hidePopup: function () {
        Effect.Fade($(this.popupWindowSelector), { duration: 0.1 });
        $('aw-gc2-product-preview-popup_content').innerHTML = '';
        this.hideOverlay()
    },
    clickHandler: function () {
        $(this.popupOverlaySelector).stopObserving('click', this.clickHandler);
        this.hidePopup()
    }
}