<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */


$installer = $this;
$installer->startSetup();

$installer->run("

ALTER TABLE {$this->getTable('points/rule')}
ADD `stop_rules` TINYINT NOT NULL ,
ADD `priority` INT NOT NULL

");

$installer->endSetup();