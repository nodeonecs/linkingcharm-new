<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   Brainvire
 * @package    Brainvire_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * @license    
 */


class Brainvire_Core_Exception extends Mage_Core_Exception {

    
    protected static $_log;

    public function __construct($message, $details='', $level=1, $module='') {
        Mage::helper('brainvirecore/logger')->log($this, $message, Brainvire_Core_Model_Logger::LOG_SEVERITY_WARNING, $details, $this->getLine());
        return parent::__construct($message);
    }

}