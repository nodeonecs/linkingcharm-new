<?php
/**
 * @category    Graphic Sourcecode
 * @package     Rofra_Salesorderitemgrid
 * @license     http://www.apache.org/licenses/LICENSE-2.0
 * @author      Rodolphe Franceschi <rodolphe.franceschi@gmail.com>
 */

$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$setup->startSetup();

$setup->run("
ALTER TABLE sales_flat_order_item ADD (store_code_1 varchar(255), store_code_2 varchar(255) , store_code_3 varchar(255) , store_code_4 varchar(255) , store_code_5 varchar(255));
");

$setup->endSetup();
