<?php
class Aster_Purchase_Model_Mysql4_Report_Purchase_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
	protected $_periodFormat;
	protected $_selectedColumns     = array();
	protected $_from                = null;
	protected $_to                  = null;
	protected $_orderStatus         = null;
	protected $_period              = null;
	protected $_storesIds           = 0;
	protected $_applyFilters        = true;
	protected $_isTotals            = false;
	protected $_isSubTotals         = false;
	protected $_aggregatedColumns   = array();

/**
* Initialize custom resource model
*
* @param array $parameters
*/
public function __construct()
{
	$this->setModel('adminhtml/report_item');
	$this->_resource = Mage::getResourceModel('purchase/purchase')->init('purchase/purchase');
	$this->setConnection($this->getResource()->getReadConnection());
	$this->_applyFilters = false;
}

protected function _getSelectedColumns()
{
	if (!$this->_selectedColumns) {
		if ($this->isTotals()) {
			$this->_selectedColumns = $this->getAggregatedColumns();
		} else {
			$this->_selectedColumns = array(
				'period'         => 'period',
				'sku'          => 'sku',
				'qty'    => 'qty',
				);
			if ('year' == $this->_period) {
				$this->_selectedColumns['period'] = 'YEAR(period)';
			} else if ('month' == $this->_period) {
				$this->_selectedColumns['period'] = "DATE_FORMAT(period, '%Y-%m')";
			}
		}
	}
	return $this->_selectedColumns;

}

protected  function _initSelect()
{

	if (!$this->_period) {
		$cols = $this->_getSelectedColumns();
		$cols['qty'] = 'SUM(qty)';
		$this->getSelect()->from($this->getTable('purchase/purchase'), $cols);
		$this->_applyDateRangeFilter();
		$this->getSelect()
		->group('sku')
		->order('qty DESC');
		return $this;
	}
	$this->getSelect()->from($this->getTable('purchase/purchase'), $this->_getSelectedColumns());

	if (!$this->isTotals()) {
		$this->getSelect()->group(array('period', 'sku'));
	}

//
	$selectUnions = array();

// apply date boundaries (before calling $this->_applyDateRangeFilter())
	$dtFormat   = Varien_Date::DATE_INTERNAL_FORMAT;
	$periodFrom = (!is_null($this->_from) ? new Zend_Date($this->_from, $dtFormat) : null);
	$periodTo   = (!is_null($this->_to)   ? new Zend_Date($this->_to,   $dtFormat) : null);
	if ('year' == $this->_period) {

		if ($periodFrom) {
if ($periodFrom->toValue(Zend_Date::MONTH) != 1 || $periodFrom->toValue(Zend_Date::DAY) != 1) {  // not the first day of the year
	$dtFrom = $periodFrom->getDate();
$dtTo = $periodFrom->getDate()->setMonth(12)->setDay(31);  // last day of the year
if (!$periodTo || $dtTo->isEarlier($periodTo)) {
	$selectUnions[] = $this->_makeBoundarySelect($dtFrom->toString($dtFormat), $dtTo->toString($dtFormat));

$this->_from = $periodFrom->getDate()->addYear(1)->setMonth(1)->setDay(1)->toString($dtFormat);  // first day of the next year
}
}
}

if ($periodTo) {
if ($periodTo->toValue(Zend_Date::MONTH) != 12 || $periodTo->toValue(Zend_Date::DAY) != 31) {  // not the last day of the year
$dtFrom = $periodTo->getDate()->setMonth(1)->setDay(1);  // first day of the year
$dtTo = $periodTo->getDate();
if (!$periodFrom || $dtFrom->isLater($periodFrom)) {
	$selectUnions[] = $this->_makeBoundarySelect($dtFrom->toString($dtFormat), $dtTo->toString($dtFormat));

$this->_to = $periodTo->getDate()->subYear(1)->setMonth(12)->setDay(31)->toString($dtFormat);  // last day of the previous year
}
}
}

if ($periodFrom && $periodTo) {
if ($periodFrom->toValue(Zend_Date::YEAR) == $periodTo->toValue(Zend_Date::YEAR)) {  // the same year
	$dtFrom = $periodFrom->getDate();
	$dtTo = $periodTo->getDate();
	$selectUnions[] = $this->_makeBoundarySelect($dtFrom->toString($dtFormat), $dtTo->toString($dtFormat));

	$this->getSelect()->where('1<>1');
}
}

}
else if ('month' == $this->_period) {
// Start of custom hackish...
	if (!$this->isTotals()) {
		$columns = $this->getSelect()->getPart('columns');
		foreach($columns as $index => $column){
			if ($column[1] == 'qty'){
				$column[1] = new Zend_Db_Expr('sum(qty)');
				$columns[$index] = $column;
			}
		}
		$this->getSelect()->setPart('columns', $columns);

	}

	$this->getSelect()->reset('group');
	$this->getSelect()->group(array(new Zend_Db_Expr("DATE_FORMAT(period, '%Y-%m')"), 'sku'));
// End of custom hackish...

	if ($periodFrom) {
if ($periodFrom->toValue(Zend_Date::DAY) != 1) {  // not the first day of the month
	$dtFrom = $periodFrom->getDate();
$dtTo = $periodFrom->getDate()->addMonth(1)->setDay(1)->subDay(1);  // last day of the month
if (!$periodTo || $dtTo->isEarlier($periodTo)) {
	$selectUnions[] = $this->_makeBoundarySelect($dtFrom->toString($dtFormat), $dtTo->toString($dtFormat));

$this->_from = $periodFrom->getDate()->addMonth(1)->setDay(1)->toString($dtFormat);  // first day of the next month
}
}
}

if ($periodTo) {
if ($periodTo->toValue(Zend_Date::DAY) != $periodTo->toValue(Zend_Date::MONTH_DAYS)) {  // not the last day of the month
$dtFrom = $periodTo->getDate()->setDay(1);  // first day of the month
$dtTo = $periodTo->getDate();
if (!$periodFrom || $dtFrom->isLater($periodFrom)) {
	$selectUnions[] = $this->_makeBoundarySelect($dtFrom->toString($dtFormat), $dtTo->toString($dtFormat));

$this->_to = $periodTo->getDate()->setDay(1)->subDay(1)->toString($dtFormat);  // last day of the previous month
}
}
}

if ($periodFrom && $periodTo) {
	if ($periodFrom->toValue(Zend_Date::YEAR) == $periodTo->toValue(Zend_Date::YEAR)
&& $periodFrom->toValue(Zend_Date::MONTH) == $periodTo->toValue(Zend_Date::MONTH)) {  // the same month
		$dtFrom = $periodFrom->getDate();
	$dtTo = $periodTo->getDate();
	$selectUnions[] = $this->_makeBoundarySelect($dtFrom->toString($dtFormat), $dtTo->toString($dtFormat));

	$this->getSelect()->where('1<>1');
}
}

}

$this->_applyDateRangeFilter();

// add unions to select
if ($selectUnions) {
	$unionParts = array();
	$cloneSelect = clone $this->getSelect();
	$unionParts[] = '(' . $cloneSelect . ')';
	foreach ($selectUnions as $union) {
		$unionParts[] = '(' . $union . ')';
	}
	$this->getSelect()->reset()->union($unionParts, Zend_Db_Select::SQL_UNION_ALL);
}

if ($this->isTotals()) {
// calculate total
	$cloneSelect = clone $this->getSelect();
	$this->getSelect()->reset()->from($cloneSelect, $this->getAggregatedColumns());
} else {
// add sorting
	$this->getSelect()->order(array('period ASC', 'qty DESC'));
}

return $this;
}

protected function _makeBoundarySelect($from, $to)
{
	$cols = $this->_getSelectedColumns();
	$cols['qty'] = 'SUM(qty)';
	$sel = $this->getConnection()->select()
	->from($this->getResource()->getMainTable(), $cols)
	->where('period >= ?', $from)
	->where('period <= ?', $to)
	->group('sku')
	->order('qty DESC');
	return $sel;
}

public function addStoreFilter($storeIds)
{
	$this->_storesIds = $storeIds;
	return $this;
}

public function addOrderStatusFilter($orderStatus)
{
	$this->_orderStatus = $orderStatus;
	return $this;
}

protected function _applyStoresFilterToSelect(Zend_Db_Select $select)
{
	return $this;
}

public function setAggregatedColumns(array $columns)
{
	$this->_aggregatedColumns = $columns;
	return $this;
}

public function getAggregatedColumns()
{
	return $this->_aggregatedColumns;
}

public function setDateRange($from = null, $to = null)
{
	$this->_from = $from;
	$this->_to = $to;
	return $this;
}

public function setPeriod($period)
{
	$this->_period = $period;
	return $this;
}

protected function _applyDateRangeFilter()
{
	if (!is_null($this->_from)) {
		$this->getSelect()->where('period >= ?', $this->_from);
	}
	if (!is_null($this->_to)) {
		$this->getSelect()->where('period <= ?', $this->_to);
	}
	return $this;
}

public function setApplyFilters($flag)
{
	$this->_applyFilters = $flag;
	return $this;
}

public function isTotals($flag = null)
{
	if (is_null($flag)) {
		return $this->_isTotals;
	}
	$this->_isTotals = $flag;
	return $this;
}

public function isSubTotals($flag = null)
{
	if (is_null($flag)) {
		return $this->_isSubTotals;
	}
	$this->_isSubTotals = $flag;
	return $this;
}

public function load($printQuery = false, $logQuery = false)
{
	if ($this->isLoaded()) {
		return $this;
	}
	$this->_initSelect();
	if ($this->_applyFilters) {
		$this->_applyDateRangeFilter();
	}
	return parent::load($printQuery, $logQuery);
}		

}
