<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_Adminhtml_Awgiftcard2_GiftcardController extends Mage_Adminhtml_Controller_Action
{
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('catalog/aw_giftcard2/codes');
    }

    protected function _initGiftcard ($requestParamName = 'id')
    {
        $giftcardId = (int)$this->getRequest()->getParam($requestParamName, 0);
        $giftcard = Mage::getModel('aw_giftcard2/giftcard');
        if ($giftcardId > 0) {
            $giftcard->load($giftcardId);
        }
        if ($data = Mage::getSingleton('adminhtml/session')->getGiftcardFormData()) {
            $giftcard->addData($data);
            Mage::getSingleton('adminhtml/session')->setGiftcardFormData(null);
        }
        Mage::register('current_giftcard', $giftcard);
        return $giftcard;
    }

    public function indexAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('catalog/aw_giftcard2');
        $this
            ->_title($this->__('Catalog'))
            ->_title($this->__('Gift Cards'))
            ->_title($this->__('Gift Card Codes'))
        ;
        $this->renderLayout();
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction()
    {
        try {
            $giftcard = $this->_initGiftcard();
        } catch (Exception $e) {
            $this->_getSession()->addError($this->__($e->getMessage()));
            return $this->_redirect('*/*/index');
        }

        $this->loadLayout()
            ->_setActiveMenu('catalog/aw_giftcard2');

        $this
            ->_title($this->__('Catalog'))
            ->_title($this->__('Gift Cards'))
        ;

        if ($giftcard->getId()) {
            $this->_title($this->__('Edit Gift Card Code'));
        } else {
            $this->_title($this->__('New Gift Card Code'));
        }

        $this->renderLayout();
    }

    public function saveAction() {
        $data = $this->getRequest()->getPost();
        try {
            $giftcard = $this->_initGiftcard();
            try {
                $data = $this->_convertDatesFromLocaleToDBFormat($data);
            } catch (Exception $e) {
                throw new Mage_Core_Exception(Mage::helper('aw_giftcard2')->__('Date is not valid.'));
            }

            $giftcard
                ->addData($data)
                ->save()
            ;
            $this->_getSession()->addSuccess($this->__('Item was successfully saved'));

            if ($this->getRequest()->getParam('sendEmail') && $giftcard->getEmailTemplate()) {
                $sender = Mage::getModel('aw_giftcard2/email_template');
                try {
                    $sender->sendGiftCardCode($giftcard);

                    Mage::getSingleton('adminhtml/session')->addSuccess(
                        Mage::helper('aw_giftcard2')->__('Email was successfully sent')
                    );
                } catch (Exception $e) {
                    Mage::getSingleton('adminhtml/session')->addError(
                        Mage::helper('aw_giftcard2')->__('An error occurred while sending email')
                    );
                }
            }

            Mage::getSingleton('adminhtml/session')->setFormData(null);
            if ($this->getRequest()->getParam('back')) {
                $this->_redirect('*/*/edit',
                    array(
                        'id'  => $giftcard->getId(),
                        'tab' => $this->getRequest()->getParam('tab', null)
                    )
                );
                return;
            }
            $this->_redirect('*/*/');
        } catch (Exception $e) {
            $this->_getSession()->addError($e->getMessage());
            $this->_getSession()->setFormData($data);
            return $this->_redirect(
                '*/*/edit', array('_current' => true, 'id' => $giftcard->getId())
            );
        }
    }

    public function activateAction()
    {
        $giftcard = $this->_initGiftcard();
        try {
            $giftcard->activate();
            $this->_getSession()->addSuccess($this->__('Gift card code was successfully activated'));
        } catch (Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        }
        return $this->_redirect('*/*/index');
    }

    public function deactivateAction()
    {
        $giftcard = $this->_initGiftcard();
        try {
            $giftcard->deactivate();
            $this->_getSession()->addSuccess($this->__('Gift card code was successfully deactivated'));
        } catch (Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        }
        return $this->_redirect('*/*/index');
    }

    public function historyGridFilterAction()
    {
        $giftCard = $this->_initGiftcard();
        if (!$giftCard->getId()) {
            return;
        }
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('aw_giftcard2/adminhtml_giftcard_edit_tab_history')->toHtml()
        );
    }

    public function massActivateAction()
    {
        $giftcardIds = $this->getRequest()->getParam('giftcard', null);
        $count = 0;
        /** @var AW_Giftcard2_Model_Resource_Giftcard_Collection $giftcardCollection */
        $giftcardCollection = Mage::getModel('aw_giftcard2/giftcard')
            ->getCollection()
            ->addGiftcardFilter($giftcardIds)
        ;
        foreach ($giftcardCollection as $giftcard) {
            try {
                $giftcard->activate();
                $count++;
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_getSession()->addSuccess($this->__('A total of %s gift card code(s) have been activated.', $count));
        return $this->_redirect('*/*/index');
    }

    public function massDeactivateAction()
    {
        $giftcardIds = $this->getRequest()->getParam('giftcard', null);
        $count = 0;
        /** @var AW_Giftcard2_Model_Resource_Giftcard_Collection $giftcardCollection */
        $giftcardCollection = Mage::getModel('aw_giftcard2/giftcard')
                                ->getCollection()
                                ->addGiftcardFilter($giftcardIds)
        ;
        foreach ($giftcardCollection as $giftcard) {
            try {
                $giftcard->deactivate();
                $count++;
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_getSession()->addSuccess($this->__('A total of %s gift card code(s) have been deactivated.', $count));
        return $this->_redirect('*/*/index');
    }

    public function massDeleteAction()
    {
        $giftcardIds = $this->getRequest()->getParam('giftcard', null);
        $count = 0;
        /** @var AW_Giftcard2_Model_Resource_Giftcard_Collection $giftcardCollection */
        $giftcardCollection = Mage::getModel('aw_giftcard2/giftcard')
            ->getCollection()
            ->addGiftcardFilter($giftcardIds)
        ;
        foreach ($giftcardCollection as $giftcard) {
            try {
                $giftcard->delete();
                $count++;
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_getSession()->addSuccess($this->__('A total of %s gift card(s) have been deleted.', $count));
        return $this->_redirect('*/*/index');
    }

    /**
     * Convert admin locale date to db date format
     *
     * @param array $data
     * @return array
     */
    protected function _convertDatesFromLocaleToDBFormat(array $data) {
        if (isset($data['expire_at'])) {
            if ($data['expire_at'] != '' && !is_null($data['expire_at'])) {
                $expireDate = new Zend_Date(
                    $data['expire_at'],
                    Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT)
                );
                $data['expire_at'] = $expireDate->toString(Varien_Date::DATE_INTERNAL_FORMAT);
            }
        }
        if (isset($data['created_at'])) {
            $createdDate = new Zend_Date(
                $data['created_at'],
                Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT)
            );
            $data['created_at'] = $createdDate->toString(Varien_Date::DATE_INTERNAL_FORMAT);
        }
        return $data;
    }

    public function exportCsvAction()
    {
        $fileName = 'aw_giftcard2.csv';
        $content = $this->getLayout()->createBlock('aw_giftcard2/adminhtml_giftcard_grid')
            ->getCsvFile()
        ;
        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName = 'aw_giftcard2.xml';
        $content = $this->getLayout()->createBlock('aw_giftcard2/adminhtml_giftcard_grid')
            ->getExcelFile()
        ;
        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function importAction()
    {
        $import = Mage::getModel('aw_giftcard2/import');

        try {
            $import->uploadFile();
            $result = $import->importFromFile();
            if($result['status']){
                Mage::getSingleton('adminhtml/session')->addSuccess($result['message']);
            }else{
                Mage::getSingleton('adminhtml/session')->addError($result['message']);
            }
        }catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }

        $this->_redirect('*/*/');
    }
}