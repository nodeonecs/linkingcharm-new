<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   BV
 * @package    BV_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */


class Brainvire_Points_Block_Adminhtml_Customer_Edit_Tabs_RewardPoints_Notifications extends Mage_Adminhtml_Block_Widget_Form {

    protected function _construct() {
        parent::_construct();
        $this->setData('customer', Mage::registry('current_customer'));
        $this->addData(
                Mage::getModel('points/summary')
                        ->loadByCustomer($this->getCustomer())->getData());
    }

    public function initForm() {
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('_brainvire_points_notification');

        $fieldset = $form->addFieldset('notification_fieldset', array('legend' => Mage::helper('points')->__('Reward Points Notification')));

        $fieldset->addField('balance_update_notification', 'checkbox', array(
            'label' => Mage::helper('points')->__('Subscribe to balance update'),
            'name' => 'balance_update_notification',
            'id' => 'brainvire_points_balance_update_notification',
            'value' => 1,
            'checked' => (bool) (int) $this->getBalanceUpdateNotification()
        ));

        $fieldset->addField('points_expire_notification', 'checkbox', array(
            'label' => Mage::helper('points')->__('Subscribe to points expiration notification'),
            'name' => 'points_expire_notification',
            'id' => 'brainvire_points_points_expire_notification',
            'value' => 1,
            'checked' => (bool) (int) $this->getPointsExpirationNotification()
        ));

        $this->setForm($form);
        return $this;
    }

}

