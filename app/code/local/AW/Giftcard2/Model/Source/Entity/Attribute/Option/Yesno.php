<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_Model_Source_Entity_Attribute_Option_Yesno extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
    implements AW_Giftcard2_Model_Source_SourceInterface
{
    /**
     * Option values
     */
    const DISABLED_VALUE = 0;
    const ENABLED_VALUE = 1;


    /**
     * Option labels
     */
    const DISABLED_LABEL = 'No';
    const ENABLED_LABEL = 'Yes';


    /**
     * @return array
     */
    public function toArray()
    {
        $helper = Mage::helper('aw_giftcard2');
        return array(
            self::DISABLED_VALUE         => $helper->__(self::DISABLED_LABEL),
            self::ENABLED_VALUE          => $helper->__(self::ENABLED_LABEL),
        );
    }

    public function getAllOptions()
    {
        $result = array();
        foreach ($this->toArray() as $key => $value) {
            $result[] = array('value' => $key, 'label' => $value);
        }
        return $result;
    }

    public function getOptionText($value)
    {
        $options = $this->toArray();
        if (array_key_exists($value, $options)) {
            return $options[$value];
        }
        return null;
    }

}