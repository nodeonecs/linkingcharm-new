<?php

class Magecomp_GstCharge_Model_Sales_Order_Creditmemo_Total_Sgstcharge extends Mage_Sales_Model_Order_Creditmemo_Total_Abstract
{
	public function collect(Mage_Sales_Model_Order_Creditmemo $creditmemo)
    {
    	$creditmemo->setSgstCharge(0);
        $creditmemo->setBaseSgstCharge(0);

        $amount = $creditmemo->getOrder()->getSgstCharge();        
        $creditmemo->setSgstCharge($amount);
        $creditmemoParams = Mage::app()->getRequest()->getParam('creditmemo');
        $totalSgstCharge = 0;
        $creditMemoItemList = array();
        foreach ($creditmemoParams as $items) {
            foreach ($items as $itemId => $item) {
                if($item['qty']==1){
                    $creditMemoItemList[] = $itemId;
                }
            }
        }

        foreach ($creditmemo->getAllItems() as $item) {
            $orderItem = $item->getOrderItem();

            if ($orderItem->isDummy()) {
                continue;
            }

            if(in_array($orderItem->getId(), $creditMemoItemList)){
                $totalSgstCharge +=$orderItem->getSgstCharge();
            }
        }

		$quoteId=$creditmemo->getOrder()->getQuoteId();
		//$quote = Mage::getModel('sales/quote')->load($quoteId);
		$quote = Mage::getModel('sales/quote')->getCollection()
    				 ->addFieldToFilter('entity_id', $quoteId)
                     ->getFirstItem();
		$exckudingTax = $quote->getExclPrice();
		
        $amount = $creditmemo->getOrder()->getBaseSgstCharge();
        $creditmemo->setBaseSgstCharge($amount);
		
        $creditmemo->setSgstCharge($totalSgstCharge);
        $creditmemo->setBaseSgstCharge($totalSgstCharge);

        $subTotal = $creditmemo->getSubtotal();
        $baseSubTotal = $creditmemo->getSubtotal();

        $grandTotal = $creditmemo->getSubtotal();

        $creditmemo->setGrandTotal($grandTotal);
        $creditmemo->setBaseGrandTotal($grandTotal);
		
        if($exckudingTax)
		{
        	$creditmemo->setGrandTotal($creditmemo->getGrandTotal() + $creditmemo->getSgstCharge());
        	$creditmemo->setBaseGrandTotal($creditmemo->getBaseGrandTotal() + $creditmemo->getBaseSgstCharge());
		}

        $creditmemo->setSgstCharge($totalSgstCharge);
        $creditmemo->setBaseSgstCharge($totalSgstCharge);
		
        return $this;
    }
}