<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   BV
 * @package    BV_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */


class Brainvire_Points_Block_Adminhtml_Customer_Edit_Tabs_RewardPoints_BalanceUpdate extends Mage_Adminhtml_Block_Widget_Form {

    public function initForm() {
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('_points');

        $fieldset = $form->addFieldset('points_fieldset', array('legend' => Mage::helper('points')->__('Update Reward Points Balance')));

        $fieldset->addField('update_points', 'text', array(
            'label' => Mage::helper('points')->__('Update Points'),
            'name' => 'brainvire_update_points',
            'note' => Mage::helper('points')->__('Enter a negative number to subtract from balance')
                )
        );

        $fieldset->addField('comment', 'text', array(
            'label' => Mage::helper('points')->__('Comment'),
            'name' => 'brainvire_update_points_comment'
                )
        );

        $this->setForm($form);
        return $this;
    }

}

