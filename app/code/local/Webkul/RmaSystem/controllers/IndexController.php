<?php

	require_once "Mage/Customer/controllers/AccountController.php";
	require_once Mage::getBaseDir("lib")."/rmasystem/Barcode39.php";
	class Webkul_RmaSystem_IndexController extends Mage_Customer_AccountController	{

		public function indexAction() {
			$this->loadLayout(array("default","rmasystem_account"));
			$this->getLayout()->getBlock("head")->setTitle($this->__("RMA System"));
			$this->renderLayout();
		}

		public function newAction(){
			$this->loadLayout(array("default","rmasystem_index_new"));
			$this->getLayout()->getBlock("head")->setTitle($this->__("File Your Returns"));
			$this->renderLayout();
		}

		public function setfilterSessionAction(){
			$data = $this->getRequest()->getPost();
			Mage::getSingleton("customer/session")->setFilterData($data);
		}

		public function setsortingSessionAction(){
			$data = $this->getRequest()->getPost();
			Mage::getSingleton("customer/session")->setSortingData($data);
		}

		public function setrmasortingSessionAction(){
			$data = $this->getRequest()->getPost();
			Mage::getSingleton("customer/session")->setRmaSortingData($data);
		}

		public function setrmafilterSessionAction(){
			$data = $this->getRequest()->getPost();
			Mage::getSingleton("customer/session")->setRmaFilterData($data);
		}

		public function getorderDetailsAction(){
			$data = $this->getRequest()->getParams();
			$order_id=$data['order_id']; 
			$order_details = array();
			
			$all_items = Mage::getModel("sales/order")->load($order_id)->getAllVisibleItems();

			foreach($all_items as $item){
				$url = Mage::getModel("catalog/product")->getCollection()->addFieldToFilter("entity_id",$item->getProductId())->getFirstItem()->getProductUrl();

				$qty_order=$item->getQtyOrdered();
	            $qty_ship=$item->getQtyShipped();
				if($qty_order==$qty_ship){
	            	array_push($order_details,array("url" => $url,"name" => $item->getName(),"sku" => $item->getSku(),"qty" => intval($item->getQtyOrdered()),"itemid" => $item->getItemId(),"product_id"=>$item->getProductId(),"price"=>$item->getPrice()));
	             }
			}
	    		$this->getResponse()->setHeader("Content-type","application/json");
				$this->getResponse()->setBody(Mage::helper("core")->jsonEncode(array_reverse($order_details)));
		}

		public function getitemDetailsAction(){
			$data = $this->getRequest()->getParams();
			$all_items = Mage::getModel("sales/order")->load($data["order_id"])->getAllVisibleItems();
			$order_details = array();
			foreach($all_items as $item){
				$qty_order=$item->getQtyOrdered();
	            $qty_invoiced=$item->getQtyInvoiced();
				if($item->getProductType()!=='downloadable' && $item->getProductType()!=='virtual'){ ;
					$url = Mage::getModel("catalog/product")->getCollection()->addFieldToFilter("entity_id",$item->getProductId())->getFirstItem()->getProductUrl();
					array_push($order_details,array("url" => $url,"name" => $item->getName(),"sku" => $item->getSku(),"qty" => intval($item->getQtyOrdered()),"itemid" => $item->getItemId(),"product_id"=>$item->getProductId(),"price"=>$item->getPrice()));
				}
				else if($qty_order!==$qty_invoiced){
					$url = Mage::getModel("catalog/product")->getCollection()->addFieldToFilter("entity_id",$item->getProductId())->getFirstItem()->getProductUrl();
					array_push($order_details,array("url" => $url,"name" => $item->getName(),"sku" => $item->getSku(),"qty" => intval($item->getQtyOrdered()),"itemid" => $item->getItemId(),"product_id"=>$item->getProductId(),"price"=>$item->getPrice()));
				}
			}
			$this->getResponse()->setHeader("Content-type","application/json");
			$this->getResponse()->setBody(Mage::helper("core")->jsonEncode(array_reverse($order_details)));
		}

		public function savermaAction(){
			$post = $this->getRequest()->getPost();
			$error = false;
			if($post){
				$file = new Varien_Io_File();
				$customer_id = Mage::getSingleton("customer/session")->getId();
				$rma = Mage::getModel("rmasystem/rma")
						->setOrderId($post["order_id"])
						->setGroup("customer")
						->setIncrementId($post["increment_id"])
						->setResolutionType($post["resolution_type"])
						->setPackageCondition($post["package_condition"])
						->setCustomerId($customer_id)
						->setAdditionalInfo($post["additional_info"])
						->setCustomerDeliveryStatus($post["customer_delivery_status"])
						->setCustomerConsignmentNo($post["customer_consignment_no"])
						->setStatus(1)
						->setCreatedAt(time());
				$last_rma_id = $rma->save()->getId();
				$image_array = array();
				$ext_array = array("jpg","JPG","jpeg","JPEG","gif","GIF","png","PNG","bmp","BMP");
				if($_FILES["related_images"]["tmp_name"][0] != ""){
					$path = Mage::getBaseDir("media").DS."RMA".DS.$last_rma_id.DS;
					$file->mkdir($path);
					foreach($_FILES["related_images"]["tmp_name"] as $key => $value){
						$ext = explode(".",$_FILES["related_images"]["name"][$key]);
						if(in_array(end($ext), $ext_array)){
							$new_image_name = time().$_FILES["related_images"]["name"][$key];
							move_uploaded_file($value, $path.$new_image_name);
							$image_array[$new_image_name] = $_FILES["related_images"]["name"][$key];
						}
						else
							$error = true;
					}
		        }
		        Mage::getModel("rmasystem/rma")->load($last_rma_id)->setImages(serialize($image_array))->save();
		        foreach($post["item_checked"] as $key => $item) {
		        	Mage::getModel("rmasystem/items")
		        		->setRmaId($last_rma_id)
		        		->setItemId($key)
		        		->setReasonId($post["item_reason"][$key])
		        		->setQty($post["return_item"][$key])
		        		->save();
		        }
		        $bar_code = new Barcode39($post["increment_id"]);
				$bar_code->barcode_text_size = 5; 
				$bar_code->barcode_bar_thick = 4; 
				$bar_code->barcode_bar_thin = 2;
				$bar_code_path = Mage::getBaseDir("media")."/RMA/Barcodes/";
				$file->mkdir($bar_code_path);
				$bar_code->draw($bar_code_path.$last_rma_id.".gif");
				if($last_rma_id > 0)
	            	Mage::getModel("rmasystem/mails")->NewRma($post,$last_rma_id,$rma->getGroup());
	            if($error == true)
		        	Mage::getSingleton("core/session")->addNotice($this->__("All files may not be uploaded"));
	        	Mage::getSingleton("core/session")->addSuccess($this->__("RMA Saved Successfully"));
		    	$this->_redirect("*/");
		    }
		    else{
	    		Mage::getSingleton("core/session")->addError($this->__("Unable to save"));
		    	$this->_redirect("*/index/new");
		    }
        }

        public function viewAction(){
        	$id = $this->getRequest()->getParam("id");
        	$rma = Mage::getModel("rmasystem/rma")->load($id);
        	if($rma->getCustomerId() == Mage::getSingleton("customer/session")->getId()){
        		$this->loadLayout(array("default","rmasystem_view"));
				$this->getLayout()->getBlock("head")->setTitle($this->__("RMA Details"));
				$this->renderLayout();
        	}
        	else{
        		Mage::getSingleton("core/session")->addError($this->__("Sorry You Are Not Authorised to view this RMA request"));
		    	$this->_redirect("*/*/");
        	}
        }

        protected function printAction(){
        	$id = $this->getRequest()->getParam("id");
        	$rma = Mage::getModel("rmasystem/rma")->load($id);
        	if($rma->getCustomerId() == Mage::getSingleton("customer/session")->getId()){
        		$this->loadLayout(array("default","rmasystem_print"));
				$this->getLayout()->getBlock("head")->setTitle($this->__("RMA Details"));
				$this->renderLayout();
        	}
        	else{
        		Mage::getSingleton("core/session")->addError($this->__("Sorry You Are Not Authorised to print this RMA request"));
		    	$this->_redirect("*/*/");
        	}
        }

        protected function printlabelAction(){
        	$id = $this->getRequest()->getParam("id");  
        	$rma = Mage::getModel("rmasystem/rma")->load($id);
        	if($rma->getCustomerId() == Mage::getSingleton("customer/session")->getId() && $rma->getShippingLabel() > 0){
        		$this->loadLayout(array("default","rmasystem_printlabel"));
				$this->getLayout()->getBlock("head")->setTitle($this->__("Pre Shipping Label"));
				$this->renderLayout();
        	}
        	else{
        		Mage::getSingleton("core/session")->addError($this->__("Shipping Label Not Available"));
		    	$this->_redirect("*/*/");
        	}
	    }

        protected function cancelAction(){
        	$id = $this->getRequest()->getParam("id");
        	$rma = Mage::getModel("rmasystem/rma")->load($id);
        	if($rma->getCustomerId() == Mage::getSingleton("customer/session")->getId()){
        		$rma->setStatus(5)->save();
        		Mage::getModel("rmasystem/mails")->CancelRma($id,$rma->getGroup());
        		Mage::getSingleton("core/session")->addSuccess($this->__("RMA with id ").$id.$this->__(" has been cancelled successfully"));
		    	$this->_redirect("*/");
        	}
        	else{
        		Mage::getSingleton("core/session")->addError($this->__("Sorry You Are Not Authorised to cancel this RMA request"));
		    	$this->_redirect("*/*/");
        	}
        }

        protected function updatermaAction(){
        	$post = $this->getRequest()->getPost();
        	if($post){
        		$image_error = false;$is_rma_save_required = false;$status_flag = false;$delivery_flag = false;
        		$rma = Mage::getModel("rmasystem/rma")->load($post["rma_id"]);
        		if($post["message"] != ""){
		        	Mage::getModel("rmasystem/conversation")
			        	->setRmaId($post["rma_id"])
			        	->setMessage($post["message"])
			        	->setCreatedAt(time())
			        	->setSender(Mage::getSingleton("customer/session")->getId())
			        	->save();
		        }
		        else
		        	Mage::getSingleton("core/session")->addError($this->__("Unable to save Message"));
		        if(isset($post["solved"])){
		        	$rma->setStatus(4);
		        	$is_rma_save_required = true;
		        	$status_flag = true;
		        }
		        if(isset($post["pending"])){
		        	$rma->setStatus(1);
		        	$is_rma_save_required = true;
		        	$status_flag = true;
		        }
		        if($rma->getCustomerConsignmentNo() != $post["customer_consignment_no"]){
		        	$rma->setCustomerConsignmentNo($post["customer_consignment_no"]);
		        	$is_rma_save_required = true;
		        	$delivery_flag = true;
		        }
	            if($_FILES["related_images"]["tmp_name"][0] != ""){
	            	$file = new Varien_Io_File();
	            	$image_array = unserialize($rma->getImages());
	        		$ext_array = array("jpg","JPG","jpeg","JPEG","gif","GIF","png","PNG","bmp","BMP");
		            $path = Mage::getBaseDir("media").DS."RMA".DS.$rma->getId().DS;
        			$file->mkdir($path);
		            foreach($_FILES["related_images"]["tmp_name"] as $key => $value){
		            	$ext = explode(".",$_FILES["related_images"]["name"][$key]);
		            	if(in_array(end($ext), $ext_array)){
		            		$new_image_name = time().$_FILES["related_images"]["name"][$key];
		            		move_uploaded_file($value, $path.$new_image_name);
		            		$image_array[$new_image_name] = $_FILES["related_images"]["name"][$key];
		            	}
		            	else
		            		$image_error = true;
		            }
		            $rma->setImages(serialize($image_array));
		            $is_rma_save_required = true;
		        }
		        if($is_rma_save_required == true)
		        	$rma->save();
		        if($status_flag == true || $delivery_flag == true)
		        	Mage::getModel("rmasystem/mails")->RMAUpdate($post,$status_flag,$delivery_flag,$rma->getGroup());
	        	else
	        		Mage::getModel("rmasystem/mails")->NewMessage($post,$rma->getGroup(),"front");
		        if($image_error == true)
		        	Mage::getSingleton("core/session")->addError($this->__("All files may not be uploaded"));
	        	Mage::getSingleton("core/session")->addSuccess($this->__("RMA Successfully Updated"));
		    	$this->_redirect("*/index/view/", array("id" => $post["rma_id"]));
		    }
		    else{
	    		Mage::getSingleton("core/session")->addError($this->__("Unable to save"));
		    	$this->_redirect("*/index/view",array("id",$post["rma_id"]));
		    }
        }

	}