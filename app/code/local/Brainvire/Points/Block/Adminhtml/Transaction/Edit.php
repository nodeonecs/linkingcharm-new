<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   BV
 * @package    BV_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */


class Brainvire_Points_Block_Adminhtml_Transaction_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

    public function __construct() {
        $this->_objectId = 'id';
        $this->_blockGroup = 'points';
        $this->_controller = 'adminhtml_transaction';
        parent::__construct();
        $this->_removeButton('save');
        $this->_removeButton('reset');
        $this->_removeButton('delete');
    }

    public function getHeaderText() {
        return Mage::helper('points')->__('Transaction # %s | %s', Mage::registry('points_current_transaction')->getId(), Mage::getModel('core/date')->date('M d, Y H:i:s', Mage::registry('points_current_transaction')->getChangeDate())
        );
    }

}