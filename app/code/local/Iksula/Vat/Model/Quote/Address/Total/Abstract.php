<?php

/**
 * Your custom total model
 *
 */
class Iksula_Vat_Model_Quote_Address_Total_Abstract extends Mage_Sales_Model_Quote_Address_Total_Abstract
{
    /**
     * Constructor that should initiaze
     */
    public function __construct()
    {
        $this->setCode('vattax');
    }

    /**
     * Used each time when collectTotals is invoked
     *
     * @param Mage_Sales_Model_Quote_Address $address
     * @return Your_Module_Model_Total_Custom
     */
    public function collect(Mage_Sales_Model_Quote_Address $address)
    {
        // echo "<pre>";
        // $quoteCurrent = Mage::getModel('checkout/session')->getQuote();
        // print_r($address->getData());exit;
        parent::collect($address);

        $this->_setAmount(0);
        $this->_setBaseAmount(0);

        $items = $this->_getAddressItems($address);
        if (!count($items)) {
            return $this; //this makes only address type shipping to come through
        }
        $fee = $this->calculateVat();
        $quote = $address->getQuote();
        $exist_amount = $quote->getVattax();
        // $fee = 20;
        $balance = $fee - $exist_amount;
        $address->setVattax($balance);
        $address->setBaseVattax($balance);

        $quote->setVattax($balance);
        $address->setGrandTotal($address->getGrandTotal() + $address->getVattax());
        $address->setBaseGrandTotal($address->getBaseGrandTotal() + $address->getBaseVattax());
    }

    /**
     * Used each time when totals are displayed
     *
     * @param Mage_Sales_Model_Quote_Address $address
     * @return Your_Module_Model_Total_Custom
     */
    public function fetch(Mage_Sales_Model_Quote_Address $address)
    {
        $backendFlag = Mage::getStoreConfig('vat_section/vat_group/flag');
        // Display total only if it is not zero
        if ($address->getVattax() != 0 && $backendFlag == 1) {
            $address->addTotal(array(
                'code' => $this->getCode(),
                'title' => 'Value Added Tax',
                'value' => $address->getVattax()
            ));
        }
    }

    private function calculateVat(){
        $quote = Mage::getSingleton('checkout/session')->getQuote();
        $isVatSameForAllProducts = Mage::getStoreConfig('vat_section/vat_group/globalflag');
        /*check if Vat is same for all products*/
        if($isVatSameForAllProducts == 1){
            $totalFee = $this->calculateFeeOnSubtotal();
        }
        else{
            $cartItems = $quote->getAllVisibleItems();
            $totalFee = 0;
            foreach($cartItems as $item){
                $totalFee = $totalFee + $this->calculateFeeForProduct($item->getProductId());
            }
        }
        return $totalFee;
    }

    private function checkWholeCartDiscount(){
        $appliedRuleIds = Mage::getSingleton('checkout/session')->getQuote()->getAppliedRuleIds();
        $appliedCoupon = Mage::getSingleton('checkout/session')->getQuote()->getCouponCode();
        if($appliedRuleIds != '' || $$appliedCoupon != ''){
            return true;
        }
        else{
            return false;
        }
    }

    private function checkIfThereIsAProductWithSpecialPrice(){

        $flag = false;
        $quote = Mage::getSingleton('checkout/session')->getQuote();
        $cartItems = $quote->getAllVisibleItems();
        $totalFee = 0;
        foreach($cartItems as $item){
            $productData = Mage::getModel('catalog/product')->load($id);
            $specialPrice = $productData->getSpecialPrice();
            if($specialPrice && $specialPrice > 0){
                $flag = true;
            }
        }
        return $flag;
    }

    private function calculateFeeForProduct($id){

        $productData = Mage::getModel('catalog/product')->load($id);
        $specialPrice = $productData->getSpecialPrice();
        $productVatPercentage = $productData->getVattax();
        $returnCalculatedFee = 0;
        if($specialPrice && $specialPrice > 0 && $productVatPercentage > 0 && $this->checkWholeCartDiscount()){
            $returnCalculatedFee = $specialPrice * $productVatPercentage / 100;
        }
        return $returnCalculatedFee;
    }

    private function calculateFeeOnSubtotal(){
        $quoteEntityId = Mage::getSingleton('checkout/session')->getQuote()->getEntityId();
        $quote = Mage::getModel('sales/quote')->load($quoteEntityId);
        //echo '<pre>';print_r($quote->getData());exit;
        $subtotal = $quote->getSubtotalWithDiscount();

        //echo $subtotal;exit;
        $VatPercentage = Mage::getStoreConfig('vat_section/vat_group/globalvalue');
        if($subtotal > 0){
            $returnCalculatedFee = $subtotal * $VatPercentage / 100;
        }
        return $returnCalculatedFee;
    }
}
