<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_Model_Product_Attribute_Backend_Amounts extends Mage_Catalog_Model_Product_Attribute_Backend_Price
{
    protected function _getResource()
    {
        return Mage::getResourceSingleton('aw_giftcard2/product_attribute_backend_amounts');
    }

    public function validate($object)
    {
        $amountsData = $object->getData($this->getAttribute()->getName());

        if ($amountsData === null) {
            return $this;
        }

        $checkedAmounts = array();
        foreach ($amountsData as $amount) {
            if (!isset($amount['delete'])) {
                continue;
            }
            if (in_array(array($amount['website_id'], $amount['price']), $checkedAmounts)) {
                Mage::throwException(Mage::helper('aw_giftcard2')->__('Amount %s is duplicated', $amount['price']));
            }
            $checkedAmounts[] = array($amount['website_id'], $amount['price']);
        }

        return $this;
    }

    public function afterLoad($object)
    {
        $data = $this->_getResource()->loadAmountsData($object->getId());
        $object->setData($this->getAttribute()->getName(), $data);
        $object->setOrigData($this->getAttribute()->getName(), $data);

        return $this;
    }

    public function afterSave($object)
    {
        $origAmountsData = $object->getOrigData($this->getAttribute()->getName());
        $amountsData = $object->getData($this->getAttribute()->getName());

        if ($amountsData === null) {
            return $this;
        }

        $old = $new = array();

        if ($origAmountsData && is_array($origAmountsData)) {
            foreach ($origAmountsData as $data) {
                if ($data['website_id'] > 0 || $data['website_id'] == '0') {
                    $key = join('-', array($data['website_id'], $data['price']));
                    $old[$key] = $data;
                }
            }
        }

        if ($amountsData && is_array($amountsData)) {
            foreach ($amountsData as $data) {
                if (!isset($data['delete'])) {
                    continue;
                }
                $key = join('-', array($data['website_id'], $data['price']));
                $new[$key] = array(
                    'website_id' => $data['website_id'],
                    'value' => $data['price']
                );
            }
        }

        $delete = array_diff_key($old, $new);
        $insert = array_diff_key($new, $old);
        $update = array_intersect_key($new, $old);

        if (!empty($delete)) {
            foreach ($delete as $data) {
                $this->_getResource()->deleteAmountsData($this->_getAmountObject($data, $object->getId()));
            }
        }
        if (!empty($insert)) {
            foreach ($insert as $data) {
                $this->_getResource()->saveAmountsData($this->_getAmountObject($data, $object->getId()));
            }
        }
        if (!empty($update)) {
            foreach ($update as $k => $v) {
                if ($old[$k]['price'] != $v['value'] || $old[$k]['website_id'] != $v['website_id']) {
                    $obj = $this->_getAmountObject(
                        array_merge($v, array('value_id' => $old[$k]['value_id'])),
                        $object->getId()
                    );
                    $this->_getResource()->saveAmountsData($obj);
                }
            }
        }

        return $this;
    }

    protected function _getAmountObject($data, $entityId)
    {
        $data['entity_id'] = $entityId;
        return new Varien_Object($data);
    }

}