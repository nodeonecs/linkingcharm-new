<?php
$installer = $this;
$installer->startSetup();
$installer = new Mage_Sales_Model_Mysql4_Setup;

$orderSource  = array(
	'type'            => 'text',
	'backend_type'    => 'text',
	'frontend_input'  => 'text',
	'is_user_defined' => true,
	'label'           => 'Order Device',
	'default'         => '',
	'visible'         => true,
	'required'        => false,
	'user_defined'    => false,
	'searchable'      => false,
	'filterable'      => false,
	'comparable'      => false
);

$installer->addAttribute('order', 'orderdevice', $orderSource);

$installer->endSetup();
