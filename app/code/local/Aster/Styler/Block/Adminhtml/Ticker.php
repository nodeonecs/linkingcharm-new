<?php

class Aster_Styler_Block_Adminhtml_Ticker
    extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{
    /**
     * Add custom config field columns, set template, add values.
     */
    public function __construct()
    {
        /** @var ArchApps_CustomAdminConfig_Helper_Data $helper */
        $helper = Mage::helper('styler/ticker');

        $this->addColumn('tiker', array(
            'style' => 'width:200px',
            'label' => $helper->__('Ticker'),
        ));

        parent::__construct();
    }
}