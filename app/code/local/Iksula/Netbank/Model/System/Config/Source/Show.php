<?php
class Iksula_Netbank_Model_System_Config_Source_Show
{
	public function toOptionArray(){
		return array(
            //array('value' => 0, 'label'=>Mage::helper('adminhtml')->__('AXIS Bank NetBanking')),
            array('value'=>'AXIB',  'label'   => Mage::helper('adminhtml')->__('AXIS Bank NetBanking')),
            array('value'=>'BOIB',  'label'   => Mage::helper('adminhtml')->__('Bank of India')),
            array('value'=>'BOMB',  'label'   => Mage::helper('adminhtml')->__('Bank of Maharashtra')),
            array('value'=>'CBIB',  'label'   => Mage::helper('adminhtml')->__('Central Bank of India')),
            array('value'=>'CRPB',  'label'   => Mage::helper('adminhtml')->__('Corporation Bank')),
            array('value'=>'DCBB',  'label'   => Mage::helper('adminhtml')->__('Development Credit Bank')),
            array('value'=>'FEDB',  'label'   => Mage::helper('adminhtml')->__('Federal Bank')),
            array('value'=>'HDFB',  'label'   => Mage::helper('adminhtml')->__('HDFC Bank')),
            array('value'=>'ICIB',  'label'   => Mage::helper('adminhtml')->__('ICICI Netbanking')),
            array('value'=>'IDBB',  'label'   => Mage::helper('adminhtml')->__('Industrial Development Bank of India')),
            array('value'=>'INDB',  'label'   => Mage::helper('adminhtml')->__('Indian Bank ')),
            array('value'=>'INIB',  'label'   => Mage::helper('adminhtml')->__('IndusInd Bank')),
            array('value'=>'INOB',  'label'   => Mage::helper('adminhtml')->__('Indian Overseas Bank')),
            array('value'=>'JAKB',  'label'   => Mage::helper('adminhtml')->__('Jammu and Kashmir Bank')),
            array('value'=>'KRKB',  'label'   => Mage::helper('adminhtml')->__('Karnataka Bank')),
            array('value'=>'KRVB',  'label'   => Mage::helper('adminhtml')->__('Karur Vysya ')),
            array('value'=>'SBBJB', 'label'   => Mage::helper('adminhtml')->__('State Bank of Bikaner and Jaipur')),
            array('value'=>'SBHB',  'label'   => Mage::helper('adminhtml')->__('State Bank of Hyderabad')),
            array('value'=>'SBIB',  'label'   => Mage::helper('adminhtml')->__('State Bank of India')),
            array('value'=>'SBMB',  'label'   => Mage::helper('adminhtml')->__('State Bank of Mysore')),
            array('value'=>'SBTB',  'label'   => Mage::helper('adminhtml')->__('State Bank of Travancore')),
            array('value'=>'SOIB',  'label'   => Mage::helper('adminhtml')->__('South Indian Bank')),
            array('value'=>'UBIB',  'label'   => Mage::helper('adminhtml')->__('Union Bank of India')),
            array('value'=>'UNIB',  'label'   => Mage::helper('adminhtml')->__('United Bank Of India')),
            array('value'=>'VJYB',  'label'   => Mage::helper('adminhtml')->__('Vijaya Bank')),
            array('value'=>'YESB',  'label'   => Mage::helper('adminhtml')->__('Yes Bank')),
            array('value'=>'CUBB',  'label'   => Mage::helper('adminhtml')->__('CityUnion')),
            array('value'=>'CABB',  'label'   => Mage::helper('adminhtml')->__('Canara Bank')), 
            array('value'=>'SBPB',  'label'   => Mage::helper('adminhtml')->__('State Bank of Patiala')),
            array('value'=>'CITNB', 'label'   => Mage::helper('adminhtml')->__('Citi Bank NetBanking')),
            array('value'=>'DSHB',  'label'   => Mage::helper('adminhtml')->__('Deutsche Bank Netbanking')),
            array('value'=>'162B',  'label'   => Mage::helper('adminhtml')->__('Kotak Bank Netbanking')),
        );      		 
    } 
} 
