<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   Brainvire
 * @package    Brainvire_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * @license    
 */


class Brainvire_Core_Model_Mysql4_Logger extends Mage_Core_Model_Mysql4_Abstract {

    protected function _construct() {
        $this->_init('brainvirecore/logger', 'id');
    }

    
    public function truncateAll() {
        $this->_getWriteAdapter()->delete($this->getMainTable(), '');
        return $this;
    }

}