<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   BV
 * @package    BV_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */


class Brainvire_Points_Block_Adminhtml_Transaction_Grid_Renderer_Notice extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function _getValue(Varien_Object $row) {
        $_value = parent::_getValue($row);
        return html_entity_decode($_value);
    }

}
