<?php


class Rofra_Salesorderitemgrid_Block_Adminhtml_Order_Items_Grid_Renderer_Viewinvoice
extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract{


public function render(Varien_Object $row)
    {

        $rowItemId = $row->getId();
        $invoiceItems = Mage::helper('salesorderitemgrid')->getInvoiceOfOrderItem($rowItemId);
         /*$oOrderItemsObject = Mage::getModel('sales/order_item')->load($orderId);
         $sOrderItemsType = $oOrderItemsObject->getProductType();
         $iParentItemId = $oOrderItemsObject->getParentItemId();
        if(strtolower($sOrderItemsType) == 'simple'){

            if($iParentItemId){

                $orderId = $iParentItemId;

            }

        }

        $order = Mage::getModel('sales/order')->loadByIncrementId($row->getIncrementalId());

        $orderItems = $order->getItemsCollection();
        $invoiceItems = Mage::getModel('sales/order_invoice')->getCollection()
                        ->addAttributeToSelect('entity_id');

        $invoiceItems->getSelect()->join(
            array('invoice_item' =>'sales_flat_invoice_item'),
             'main_table.entity_id= invoice_item.parent_id',
            array('invoice_item.order_item_id'));

        $invoiceItems->addFieldToFilter('invoice_item.order_item_id', array('eq'=>$orderId));    */    
        $html = '';
        if(count($invoiceItems) > 0){
            
            $invoiceUrl = $this->getUrl('*/sales_invoice/view',
                                array(
                                    'invoice_id'=> $invoiceItems->getFirstItem()->getId(),
                                )
                            );

            
            $html = '<a href="'.$invoiceUrl.'">View Invoice</a>';
        }



        return $html;
    }


}
