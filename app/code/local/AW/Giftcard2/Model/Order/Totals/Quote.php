<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_Model_Order_Totals_Quote extends Mage_Sales_Model_Quote_Address_Total_Abstract
{
    public function __construct()
    {
        $this->setCode('aw_giftcard2');
    }

    public function collect(Mage_Sales_Model_Quote_Address $address)
    {
        $_result = parent::collect($address);

        $baseGrandTotal = $address->getBaseGrandTotal();
        $grandTotal = $address->getGrandTotal();

        $baseTotalGiftCardAmount = 0;
        $totalGiftCardAmount = 0;

        $quote = $address->getQuote();

        if ($baseGrandTotal
            && (
                $quote->getIsActive()
                || Mage::app()->getStore()->isAdmin() // when quote is created in the backend it gets is_active = false immediately, which prevents us from calculating the GC discount
            )
        ) {
            $appliedGiftCards = Mage::helper('aw_giftcard2/giftcard')->getQuoteGiftCards($quote->getId());

            foreach ($appliedGiftCards as $giftCard) {
                $baseGiftCardAmount = $giftCard->getBalance();
                if ($baseGiftCardAmount >= $baseGrandTotal) {
                    $baseGiftCardUsedAmount = $baseGrandTotal;
                } else {
                    $baseGiftCardUsedAmount = $baseGiftCardAmount;
                }
                $baseGrandTotal -= $baseGiftCardUsedAmount;

                $giftCardAmount = $quote->getStore()->convertPrice($baseGiftCardAmount);
                if ($giftCardAmount >= $grandTotal) {
                    $giftCardUsedAmount = $grandTotal;
                } else {
                    $giftCardUsedAmount = $giftCardAmount;
                }
                $grandTotal -= $giftCardUsedAmount;

                $baseGiftCardAmount = round($baseGiftCardUsedAmount, 4);
                $giftCardAmount = round($giftCardUsedAmount, 4);

                $baseTotalGiftCardAmount += $baseGiftCardAmount;
                $totalGiftCardAmount += $giftCardAmount;

                $giftCardQuote = Mage::getModel('aw_giftcard2/giftcard_quote')->load($giftCard->getLinkId());
                if ($giftCardQuote->getId()) {
                    $giftCardQuote
                        ->setBaseGiftcardAmount($baseGiftCardAmount)
                        ->setGiftcardAmount($giftCardAmount)
                        ->save()
                    ;
                }
            }

            $quote
                ->setBaseAwGCAmount($baseTotalGiftCardAmount)
                ->setAwGCAmount($totalGiftCardAmount)
            ;
            $address
                ->setBaseAwGCAmount($baseTotalGiftCardAmount)
                ->setAwGCAmount($totalGiftCardAmount)
                ->setBaseGrandTotal($address->getBaseGrandTotal() - $baseTotalGiftCardAmount + $address->getBaseDiscountAmount())
                ->setGrandTotal($address->getGrandTotal() - $totalGiftCardAmount + $address->getDiscountAmount())
            ;
        }
        return $_result;
    }

    public function fetch(Mage_Sales_Model_Quote_Address $address)
    {
        $_result = parent::fetch($address);

        $giftCards = Mage::helper('aw_giftcard2/giftcard')->getQuoteGiftCards($address->getQuote()->getId());
        $address->addTotal(
            array(
                'code'       => $this->getCode(),
                'title'      => Mage::helper('aw_giftcard2')->__('Gift Cards'),
                'value'      => -$address->getAwGCAmount(),
                'gift_cards' => $giftCards,
            )
        );
        return $_result;
    }
}
