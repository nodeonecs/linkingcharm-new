<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */


class Brainvire_Points_Model_Source_Pointsfororder {

    public function toOptionArray() {
        return array(
            array(
                'value' => Brainvire_Points_Helper_Config::FIRST_ORDER_ONLY,
                'label' => Mage::helper('points')->__('First order')
            ),
            array(
                'value' => Brainvire_Points_Helper_Config::EACH_ORDER,
                'label' => Mage::helper('points')->__('Each order')
                ));
    }

}

?>
