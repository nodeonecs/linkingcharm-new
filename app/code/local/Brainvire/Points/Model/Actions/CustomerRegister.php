<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */


class Brainvire_Points_Model_Actions_CustomerRegister extends Brainvire_Points_Model_Actions_Abstract {

    protected $_action = 'customer_register';
    protected $_comment = 'Reward for registration';

    public function setAmount($_amount) {
        if (!is_null($_amount))
            $this->_amount = $_amount;
        else
            $_amount = Mage::helper('points/config')->getPointsForRegistration();
        return $this;
    }

}
