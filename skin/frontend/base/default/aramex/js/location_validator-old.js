/**
 *
 * @category    design
 * @package     base_default 
 * 
 */

 
 
 document.observe('dom:loaded', function(){
	
 });
 
 	var options = {
		  caseSensitive: false,
		  includeScore: false,
		  shouldSort: true,
		  threshold: 0.0,
		  location: 0,
		  distance: 100,
		  maxPatternLength: 32,
		  keys: ["title"]
		};
	
 
 
 jQuery(function(){	
		jQuery(".autocomplete").hide();
        var billing_aramex_city_obj = new Fuse(billing_aramex_cities, options); 
		var shipping_aramex_city_obj = new Fuse(shipping_aramex_cities, options);		
        var system_base_url = getSystemBaseUrl(); 
		var stype = "billing";		
		jQuery("input[name='billing[city]']").keyup(function(){
			var result = billing_aramex_city_obj.search(jQuery(this).val());
			var content_html = '<ul>';
			  jQuery(result).each(function(i,v){				 
				  content_html += '<li>'+v.title+'</li>';
			  });
			  content_html += '</ul>';
			jQuery("#"+stype+"_city_autocomplete").html(content_html);
			jQuery("#"+stype+"_city_autocomplete").show();
		});
		
		var ship_type = "shipping";
		jQuery("input[name='shipping[city]']").keyup(function(){
			var result = shipping_aramex_city_obj.search(jQuery(this).val());
			var content_html = '<ul>';
			  jQuery(result).each(function(i,v){				 
				  content_html += '<li>'+v.title+'</li>';
			  });
			  content_html += '</ul>';
			jQuery("#"+ship_type+"_city_autocomplete").html(content_html);
			jQuery("#"+ship_type+"_city_autocomplete").show();
		});
		
		
				
		jQuery(".autocomplete").on('click','li',function(){					     
			jQuery(this).parents('.input-box').find('input').val(jQuery(this).text());
		});
		
		
		jQuery(".autocomplete").mouseleave(function(){
		 jQuery(this).hide();
		  // validation code 
		 id_val = jQuery(this).attr('id');
		
		 if(id_val=="billing_city_autocomplete"){
			var is_valid_city = false;
			var old_value = jQuery("input[name='billing[city]']").val();
			var result = billing_aramex_city_obj.search(old_value);
			jQuery(result).each(function(i,v){				 
				  if(is_valid_city == false && old_value.toLowerCase() == v.title.toLowerCase())
					  is_valid_city = true;
			  });
			if(is_valid_city == false){
				jQuery("input[name='billing[city]']").val('');
			}
		 }
		 
		 if(id_val=="shipping_city_autocomplete"){
			var is_valid_city = false;
			var old_value = jQuery("input[name='shipping[city]']").val();
			var result = billing_aramex_city_obj.search(old_value);
			 jQuery(result).each(function(i,v){				 
				  if(is_valid_city == false && old_value.toLowerCase() == v.title.toLowerCase())
					  is_valid_city = true;
			  });
			if(is_valid_city == false){
				jQuery("input[name='shipping[city]']").val('');
			}
		 }
		
		});
		
		jQuery("input[name='billing[city]']").focus(function(){
			 jQuery("#billing_city_autocomplete").show();
		});
		jQuery("input[name='shipping[city]']").focus(function(){
			 jQuery("#shipping_city_autocomplete").show();
		});
		
		


		
		/*var billingAutocompleterCities = new Ajax.Autocompleter("billing:city", stype+"_city_autocomplete", system_base_url+"apilocationvalidator/index/searchautocities", {
			  paramName: "value", 
			  minChars: 2,
			  parameters: "&country_code="+jQuery("select[name='"+stype+"[country_id]']").val()
			  
		});
		stype = "shipping";
		var shippingAutocompleterCities = new Ajax.Autocompleter("shipping:city", stype+"_city_autocomplete", system_base_url+"apilocationvalidator/index/searchautocities", {
			  paramName: "value", 
			  minChars: 2,
			  parameters: "&country_code="+jQuery("select[name='"+stype+"[country_id]']").val()
			  
		});*/
		
		/* ALLOW SELECT LISTED VALUE */	
		jQuery("select[name='billing[country_id]']").change(function(){
			var current_billing_city = getAllCitiesJson('billing');
			console.info();
		});
		jQuery("select[name='billing[country_id]']").change(function(){
			getallCities('shipping');
		})
		
		
		/* ALLOW SELECT LISTED VALUE */
		
	/*jQuery("select[name='billing[country_id]']").change(function(){
		 applyAramexValidator('billing');				 
	});
	 jQuery("select[name='shipping[country_id]']").change(function(){
		 applyAramexValidator('shipping');	
	 });	 
	   applyAramexValidator('billing');
	   applyAramexValidator('shipping');*/		
		
 });
 


 function checkCity(type){
	 var old_value = jQuery("input[name='"+type+"[city]']").val();							 
	 var isEvalid  = false;
	 jQuery("#"+type+"-all-cities ul li").each(function(){
		if(old_value.toLowerCase() == jQuery(this).text().toLowerCase())  isEvalid  = true;
	 });
	 if(isEvalid == false){
		jQuery("input[name='"+type+"[city]']").val('');
	 }
 }
 function getallCities(type){
	var system_base_url = getSystemBaseUrl();
	var country_code = jQuery("select[name='"+type+"[country_id]']").val();
	jQuery.ajax({url: system_base_url+"apilocationvalidator/index/searchallcities", data:{'country_code':country_code},type:'post', success: function(result){
		jQuery("#"+type+"-all-cities").html(result);
	}});	
 }
 
function getAllCitiesJson(type){
	var system_base_url = getSystemBaseUrl();
	var country_code = jQuery("select[name='"+type+"[country_id]']").val();
	var mycities = ''; 
	jQuery.ajax({url: system_base_url+"apilocationvalidator/index/searchallcitiesJson", data:{'country_code':country_code},type:'post', success: function(result){
		return mycities  = result;
	}});	
}

 

 function applyAramexValidator(type){  	 
	var country_code = jQuery("select[name='"+type+"[country_id]']").val();
	 var system_base_url = getSystemBaseUrl();
		if(country_code){
		  jQuery.ajax({url: system_base_url+"/apilocationvalidator/index/applyapivalidator", data:{country_code:country_code}, type:'Post', success: function(result){
			var response = jQuery.parseJSON(result);			
			if(typeof(response.post_code_required) != 'undefined'){
				if(response.post_code_required == "1"){					
					jQuery("input[name='"+type+"[postcode]']").addClass('required-entry');					
				}else{
					/* set city as requried */
					jQuery("input[name='"+type+"[postcode]']").removeClass('required-entry');
					jQuery("input[name='"+type+"[city]']").addClass('required-entry');					
				}
			}		
		}
		});
	}
 }
 
