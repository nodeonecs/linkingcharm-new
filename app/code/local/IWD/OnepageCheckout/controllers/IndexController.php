<?php
class IWD_OnepageCheckout_IndexController extends Mage_Checkout_Controller_Action
{
	private $_current_layout = null;

    protected $_sectionUpdateFunctions = array(
    	'review'          => '_getReviewHtml',
    	'shipping-method' => '_getShippingMethodsHtml',
        'payment-method'  => '_getPaymentMethodsHtml',
    );

    public function preDispatch()
    {
        parent::preDispatch();
        $this->_preDispatchValidateCustomer();
        return $this;
    }

    protected function _ajaxRedirectResponse()
    {
        $this->getResponse()
            ->setHeader('HTTP/1.1', '403 Session Expired')
            ->setHeader('Login-Required', 'true')
            ->sendResponse();
        return $this;
    }

    protected function _expireAjax()
    {
        if (!$this->getOnepagecheckout()->getQuote()->hasItems()
            || $this->getOnepagecheckout()->getQuote()->getHasError()
            || $this->getOnepagecheckout()->getQuote()->getIsMultiShipping()) {
            $this->_ajaxRedirectResponse();
            return true;
        }
        /*$action = $this->getRequest()->getActionName();
        if (Mage::getSingleton('checkout/session')->getCartWasUpdated(true)
            && !in_array($action, array('index', 'progress'))) {
            $this->_ajaxRedirectResponse();
            return true;
        }*/

        return false;
    }

    protected function _getUpdatedLayout()
    {$this->_initLayoutMessages('checkout/session');
        if ($this->_current_layout === null)
        {
            $layout = $this->getLayout();
            $update = $layout->getUpdate();
            $update->load('onepagecheckout_index_updatecheckout');

            $layout->generateXml();
            $layout->generateBlocks();
            $this->_current_layout = $layout;
        }

        return $this->_current_layout;
    }

    protected function _getShippingMethodsHtml()
    {
    	$layout	= $this->_getUpdatedLayout();
        return $layout->getBlock('checkout.shipping.method')->toHtml();
    }

    protected function _getPaymentMethodsHtml()
    {
    	$layout	= $this->_getUpdatedLayout();
        return $layout->getBlock('checkout.payment.method')->toHtml();
    }

	protected function _getCouponDiscountHtml()
    {
    	$layout	= $this->_getUpdatedLayout();
        return $layout->getBlock('checkout.cart.coupon')->toHtml();
    }

    protected function _getReviewHtml()
    {
    	$layout	= $this->_getUpdatedLayout();
        return $layout->getBlock('checkout.review')->toHtml();
    }

    protected function _isOnepagecheckoutBlock($ajax = false)
    {
    	if(Mage::helper('onepagecheckout')->isOPCBlock())
    	{
    		Mage::getSingleton('checkout/session')->addError(Mage::helper('onepagecheckout')->__('The one page checkout is blocked.'));
    		if(!$ajax)
            	$this->_redirect('checkout/cart');
            else
            {
				$result['redirect'] = Mage::getUrl('checkout/cart');
				$this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
            }

            return false;
    	}
    	return true;
    }


    public function getOnepagecheckout()
    {
        return Mage::getSingleton('onepagecheckout/type_geo');
    }

    public function indexAction()
    {
            if(Mage::getSingleton('customer/session')->getMobileapp() == "mobile"){
                Mage::getSingleton('customer/session')->unsMobileapp();
            }
            if (!Mage::helper('onepagecheckout')->isOnepageCheckoutEnabled())
            {
                Mage::getSingleton('checkout/session')->addError(Mage::helper('onepagecheckout')->__('The one page checkout is disabled.'));
                $this->_redirect('checkout/cart');
                return;
            }

            $this->_isOnepagecheckoutBlock();

            $quote = $this->getOnepagecheckout()->getQuote();
            if (!$quote->hasItems() || $quote->getHasError()) {
                $this->_redirect('checkout/cart');
                return;
            }
            if (!$quote->validateMinimumAmount()) {
                $error = Mage::getStoreConfig('sales/minimum_order/error_message');
                Mage::getSingleton('checkout/session')->addError($error);
                $this->_redirect('checkout/cart');
                return;
            }

            Mage::getSingleton('checkout/session')->setCartWasUpdated(false);
            Mage::getSingleton('customer/session')->setBeforeAuthUrl(Mage::getUrl('*/*/*', array('_secure'=>true)));

            $this->getOnepagecheckout()->initDefaultData()->initCheckout();
            $this->loadLayout();
            $this->_initLayoutMessages('customer/session');
            $title	= Mage::getStoreConfig('onepagecheckout/general/title');
            $this->getLayout()->getBlock('head')->setTitle($title);
            $this->renderLayout();
        // }
    }
    public function mobileapponepageAction()
    {
            // if(Mage::getSingleton('customer/session')->isLoggedIn()){
            //     Mage::getSingleton('customer/session')->unsMobileapp();
            // }
            if (!Mage::helper('onepagecheckout')->isOnepageCheckoutEnabled())
            {
                Mage::getSingleton('checkout/session')->addError(Mage::helper('onepagecheckout')->__('The one page checkout is disabled.'));
                $this->_redirect('checkout/cart');
                return;
            }

            $this->_isOnepagecheckoutBlock();

            $quote = $this->getOnepagecheckout()->getQuote();
            if (!$quote->hasItems() || $quote->getHasError()) {
                $this->_redirect('checkout/cart');
                return;
            }
            if (!$quote->validateMinimumAmount()) {
                $error = Mage::getStoreConfig('sales/minimum_order/error_message');
                Mage::getSingleton('checkout/session')->addError($error);
                $this->_redirect('checkout/cart');
                return;
            }

            Mage::getSingleton('checkout/session')->setCartWasUpdated(false);
            Mage::getSingleton('customer/session')->setBeforeAuthUrl(Mage::getUrl('*/*/*', array('_secure'=>true)));

            $this->getOnepagecheckout()->initDefaultData()->initCheckout();
            $this->loadLayout();
            $this->_initLayoutMessages('customer/session');
            $title  = Mage::getStoreConfig('onepagecheckout/general/title');
            $this->getLayout()->getBlock('head')->setTitle($title);
            $this->renderLayout();
        // }
    }
    public function successAction()
    {
        $isMobile = Mage::getSingleton('customer/session')->getMobileapp();
        if($isMobile == 'mobile'){
            //Is Mobile
            Mage::getSingleton('customer/session')->unsMobileapp();        
            $this->_redirect('mobileapp/index/success');
            //echo "Mobile"; exit;
        } else {
            //Not Mobile
            $error  = Mage::helper('onepagecheckout')->checkParams($this->getRequest());
            if($error)
            {
                $this->_redirect('checkout/cart');
                return;
            }

            $this->_isOnepagecheckoutBlock();

            $session = $this->getOnepagecheckout()->getCheckout();
            if (!$session->getLastSuccessQuoteId()) {
                $this->_redirect('checkout/cart');
                return;
            }

            $lastQuoteId = $session->getLastQuoteId();
            $lastOrderId = $session->getLastOrderId();
            //Load order to pass in event
            $order = Mage::getModel('sales/order')->load($lastOrderId);
            $lastRecurringProfiles = $session->getLastRecurringProfileIds();
            if (!$lastQuoteId || (!$lastOrderId && empty($lastRecurringProfiles))) {
                $this->_redirect('checkout/cart');
                return;
            }
            $session->clear();
            $this->loadLayout();
            $this->_initLayoutMessages('checkout/session');

            // mark that order will be saved by OPC module
            $session->setProcessedOPC('opc');

            Mage::dispatchEvent('checkout_onepage_controller_success_action', array('order_ids' => array($lastOrderId)));
            Mage::dispatchEvent('custom_checkout_order_place_success_action', array('order' => $order));
            $this->renderLayout();

            $checkoutstep = 4;
            $trackCheckoutHelper = Mage::helper('onepagecheckout/Track');
            $trackCheckoutHelper->getTrackDetailsStepFive($checkoutstep,$lastOrderId,$lastQuoteId);

        }
        
    }

    public function failureAction()
    {
        $isMobile = Mage::getSingleton('customer/session')->getMobileapp();
        if($isMobile == 'mobile'){
            //Is Mobile
            Mage::getSingleton('customer/session')->unsMobileapp();
            $this->_redirect('mobileapp/index/failure');
            //echo "Mobile"; exit;
        } else {
            //Not Mobile
            $lastQuoteId = $this->getOnepagecheckout()->getCheckout()->getLastQuoteId();
            $lastOrderId = $this->getOnepagecheckout()->getCheckout()->getLastOrderId();

            /*if (!$lastQuoteId || !$lastOrderId) {
                $this->_redirect('checkout/cart');
                return;
            }

            $this->loadLayout();
            $this->renderLayout(); */
           /* $session            = Mage::getSingleton('checkout/session');
            echo $quote_id           = $session->getQuoteId();exit;*/
            $checkoutstep = 3;
            $order = Mage::getModel('sales/order')->loadByIncrementId(Mage::getSingleton('checkout/session')->getLastRealOrderId());
            $quoteId = $order['quote_id'];
            $trackCheckoutHelper = Mage::helper('onepagecheckout/Track');
            $trackCheckoutHelper->getTrackDetailsStepFour($checkoutstep,$quoteId);

            Mage::getSingleton('checkout/session')->unsLastRealOrderId();
            $this->_redirect('checkout/onepage/failure');

        }
        
    }

    public function getAddressAction()
    {
        if ($this->_expireAjax()) {
            return;
        }
        $addressId = $this->getRequest()->getParam('address', false);
        if ($addressId) {
            $address = $this->getOnepagecheckout()->getAddress($addressId);

            if (Mage::getSingleton('customer/session')->getCustomer()->getId() == $address->getCustomerId()) {
                $this->getResponse()->setHeader('Content-type', 'application/x-json');
                $this->getResponse()->setBody($address->toJson());
            } else {
                $this->getResponse()->setHeader('HTTP/1.1','403 Forbidden');
            }
        }
    }

    public function getCustomerEmailAction()
    {
        $customer_email_not_logged=$this->getRequest()->getParam('s');
        // print_r($customer_email_not_logged);
        $customer_id=Mage::getSingleton('checkout/session')->setCustEmail($customer_email_not_logged);
        // print_r($customer_email_not_logged);
        // exit;
    }
    public function getCustomCouponCodeAction($couponData){
        // echo "hii";exit;
           // $customer_email_not_logged=$this->getRequest()->getParam('s');
            // print_r($customer_email_not_logged);
            // exit;
    	//if ($couponChanged) {
            //echo 'aaa';exit;
    		$couponChanged      = false;
    		$quote 				= $this->getOnepagecheckout()->getQuote();
        	//$couponData 		= $this->getRequest()->getPost('coupon', array());
        	$processCoupon 		= $this->getRequest()->getPost('process_coupon', false);
            $cust_coupon 		= $couponData['code'];

            if($couponData['code'] == ''){
            	$quote->setCouponCode(
                    strlen($couponData['code']) ? $couponData['code'] : ''
                );
                $this->getRequest()->setPost('payment-method', true);
                $this->getRequest()->setPost('shipping-method', true);
                if ($couponData['code']) {
                    $couponChanged = true;
                } else {
                	$couponChanged = true;
                    Mage::getSingleton('checkout/session')->addSuccess(Mage::helper('onepagecheckout')->__('Coupon code was canceled.'));
                    //echo 'aaaa';exit;
                }
            }
            else{

            	$oCoupon = Mage::getModel('salesrule/coupon')->load($cust_coupon, 'code');
	            $oRule = Mage::getModel('salesrule/rule')->load($oCoupon->getRuleId());
	            $rule_data=$oRule->getData();
	            $rule_id=$rule_data['rule_id'];
	            //echo $rule_id;exit;
	            if ($rule_id==11) {
	            	// Static variable here.
	                // $n_date  = '2015-03-10';
	                $n_date=Mage::helper('onepagecheckout/data')->Getsmscouponexpiry($cust_coupon);
	            }
	            elseif ($rule_id==10) {
	            	// Static variable here.
	                // $n_date  = '2015-03-10';
	                $n_date=Mage::helper('onepagecheckout/data')->Getemailcouponexpiry($cust_coupon);
	            }

	            $current_Date = date('Y-m-d');

	            $curdate=strtotime($current_Date);
	            // echo "<br>";
	            $expiry_date=strtotime($n_date);

	            // $customer = Mage::getSingleton('customer/session')->getCustomer();
	            // $customer_email=$customer->getEmail();

                Mage::getSingleton('core/session', array('name' => 'frontend'));

                $sessionCustomer = Mage::getSingleton("customer/session");

                if($sessionCustomer->isLoggedIn()) {
                    $checkout = Mage::getSingleton('checkout/session')->getQuote();
                    $billAddress = $checkout->getBillingAddress();
                    $customer_email=$billAddress->getData('email');
                  // echo "Logged";
                } else {
                    $customer_email=Mage::getSingleton('checkout/session')->getCustEmail();
                    Mage::getSingleton('checkout/session')->unsetCustEmail();
                   // echo "Not Logged";
                }
	            // $customerAddressId = Mage::getSingleton('customer/session')->getCustomer()->getDefaultBilling();
	            // $address = Mage::getModel('customer/address')->load($customerAddressId);
	            // $customer_number = $address->getTelephone();

	            //echo 'abcde';exit;
	                //echo 'abc';exit;
	            if ($rule_id==10 || $rule_id==11) {

	            	// Static variable here.
	                // $coupon_valid_count = 1;
	                $coupon_valid_count = Mage::helper('onepagecheckout/data')->Validatecouponcode($cust_coupon,$customer_email);
                    // echo $coupon_valid_count;
                    // echo $customer_email;
                    // // echo $curdate;
                    // // echo $expiry_date;
                    // exit;
                    //$coupon_valid_count=1;
	                // if($coupon_valid_count==1 && $curdate<=$expiry_date)
                    if($curdate<=$expiry_date)
	                    {

	                        //if ($couponData['code'] == $quote->getCouponCode()) {
	                            if($couponData['code'] != NULL){

	                            	$quote->setCouponCode($couponData['code']);
					                $this->getRequest()->setPost('payment-method', true);
					                $this->getRequest()->setPost('shipping-method', true);
					                if ($couponData['code']) {
					                    $couponChanged = true;
					                } else {
					                	$couponChanged = true;
					                    Mage::getSingleton('checkout/session')->addSuccess(Mage::helper('onepagecheckout')->__('Coupon code was canceled.'));
					                    //echo 'aaaa';exit;
					                }

	                              /*Mage::getSingleton('checkout/session')->addSuccess(
	                                Mage::helper('onepagecheckout')->__('Coupon code "%s" was applied.', Mage::helper('core')->htmlEscape($couponData['code']))
	                            );  */
	                          }
	                        /*} else {
	                            Mage::getSingleton('checkout/session')->addError(
	                                Mage::helper('onepagecheckout')->__('Coupon code "%s" is not valid.', Mage::helper('core')->htmlEscape($couponData['code']))
	                            );
	                        }*/
	                }
	                else{
	                    Mage::getSingleton('checkout/session')->addError(
	                    Mage::helper('onepagecheckout')->__('Coupon code "%s" is not valid.', Mage::helper('core')->htmlEscape($couponData['code']))
	                            );
	                }

	            }
	            else if(($rule_id!=10 || $rule_id!=11) && $rule_id != '')

                    {

						if($couponData['code'] != NULL){

							$quote->setCouponCode($couponData['code']);
							$this->getRequest()->setPost('payment-method', true);
							$this->getRequest()->setPost('shipping-method', true);
							if ($couponData['code']) {
								$couponChanged = true;
							} else {
								$couponChanged = true;
								Mage::getSingleton('checkout/session')->addSuccess(Mage::helper('onepagecheckout')->__('Coupon code was canceled.'));

							}
								/*Mage::getSingleton('checkout/session')->addSuccess(
								Mage::helper('onepagecheckout')->__('Coupon code "%s" was applied.', Mage::helper('core')->htmlEscape($couponData['code']))
							);  */
						}
                    }

	            // }
	            else
	                {
	                    Mage::getSingleton('checkout/session')->addError(
	                        $this->__('Coupon code "%s" is not valid.', Mage::helper('core')->escapeHtml($couponData['code']))
	                    );

	                }


	            //$method = str_replace(' ', '', ucwords(str_replace('-', ' ', 'coupon-discount')));
            	//$result['update_section']['coupon-discount'] = $this->{'_get' . $method . 'Html'}();
	            return $couponChanged;
            }


           // echo "hii";exit;
        //}
    }

    public function updateCheckoutAction()
    {
        if ($this->_expireAjax() || !$this->getRequest()->isPost()) {
            return;
        }

        $blockopc	= $this->getRequest()->getPost('blockopc', false);
        Mage::helper('onepagecheckout')->checkBlock($blockopc);

        if(!$this->_isOnepagecheckoutBlock(true))
        	return;

		/*********** DISCOUNT CODES **********/

        $quote 				= $this->getOnepagecheckout()->getQuote();
        $couponData 		= $this->getRequest()->getPost('coupon', array());
        $processCoupon 		= $this->getRequest()->getPost('process_coupon', false);

        $couponChanged 		= false;
	    if ($couponData && $processCoupon) {
	            if (!empty($couponData['remove'])) {
	                $couponData['code'] = '';

	            }
	            //else{
	            	$oldCouponCode = $quote->getCouponCode();
		            if ($oldCouponCode != $couponData['code']) {
		                /*try {
		                    $quote->setCouponCode(
		                        strlen($couponData['code']) ? $couponData['code'] : ''
		                    );
		                    $this->getRequest()->setPost('payment-method', true);
		                    $this->getRequest()->setPost('shipping-method', true);
		                    if ($couponData['code']) {
		                        $couponChanged = true;
		                    } else {
		                    	$couponChanged = true;
		                        Mage::getSingleton('checkout/session')->addSuccess(Mage::helper('onepagecheckout')->__('Coupon code was canceled.'));
		                    }
		                } catch (Mage_Core_Exception $e) {
		                	$couponChanged = true;
		                    Mage::getSingleton('checkout/session')->addError($e->getMessage());
		                } catch (Exception $e) {
		                	$couponChanged = true;
		                    Mage::getSingleton('checkout/session')->addError(Mage::helper('onepagecheckout')->__('Cannot apply the coupon code.'));
		                }*/

		                $couponChanged = $this->getCustomCouponCodeAction($couponData);
		            }
	           //}



            }

        /***********************************/

        $bill_data = $this->getRequest()->getPost('billing', array());
        $bill_data = $this->_filterPostData($bill_data);
        $bill_addr_id = $this->getRequest()->getPost('billing_address_id', false);
        $result = array();
        $ship_updated = false;

        if ($this->_checkChangedAddress($bill_data, 'Billing', $bill_addr_id) || $this->getRequest()->getPost('payment-method', false))
        {
            if (isset($bill_data['email']))
            {
                $bill_data['email'] = trim($bill_data['email']);
            }

            $bill_result = $this->getOnepagecheckout()->saveBilling($bill_data, $bill_addr_id, false);

            if (!isset($bill_result['error']))
            {
                $pmnt_data = $this->getRequest()->getPost('payment', array());
                $this->getOnepagecheckout()->usePayment(isset($pmnt_data['method']) ? $pmnt_data['method'] : null);

                // $result['update_section']['payment-method'] = $this->_getPaymentMethodsHtml();

                if (isset($bill_data['use_for_shipping']) && $bill_data['use_for_shipping'] == 1 && !$this->getOnepagecheckout()->getQuote()->isVirtual())
				{
                    $result['update_section']['shipping-method'] = $this->_getShippingMethodsHtml();
                    $result['duplicateBillingInfo'] = 'true';

                    $ship_updated = true;
                }
            }
            else
            {
                $result['error_messages'] = $bill_result['message'];
            }
        }

        $ship_data = $this->getRequest()->getPost('shipping', array());
        $ship_addr_id = $this->getRequest()->getPost('shipping_address_id', false);
        $ship_method	= $this->getRequest()->getPost('shipping_method', false);

        if (!$ship_updated && !$this->getOnepagecheckout()->getQuote()->isVirtual())
        {
            if ($this->_checkChangedAddress($ship_data, 'Shipping', $ship_addr_id) || $ship_method)
            {
                $ship_result = $this->getOnepagecheckout()->saveShipping($ship_data, $ship_addr_id, false);

                if (!isset($ship_result['error']))
                {
                    $result['update_section']['shipping-method'] = $this->_getShippingMethodsHtml();
                }
            }

// fix
            if(!isset($result['update_section']['shipping-method']) && $this->getRequest()->getPost('shipping-method', false))
            {
            	$result['update_section']['shipping-method'] = $this->_getShippingMethodsHtml();
            }

        }

        $check_shipping_diff	= false;

        // check how many shipping methods exist
        $rates = Mage::getModel('sales/quote_address_rate')->getCollection()->setAddressFilter($this->getOnepagecheckout()->getQuote()->getShippingAddress()->getId())->toArray();
        if(count($rates['items'])==1)
        {
        	if($rates['items'][0]['code']!=$ship_method)
        	{
        		$check_shipping_diff	= true;

        		$result['reload_totals'] = 'true';
        	}
        }
        else
			$check_shipping_diff	= true;

// get prev shipping method
		if($check_shipping_diff){
			$shipping = $this->getOnepagecheckout()->getQuote()->getShippingAddress();
			$shippingMethod_before = $shipping->getShippingMethod();
		}

        $this->getOnepagecheckout()->useShipping($ship_method);

        $this->getOnepagecheckout()->getQuote()->collectTotals()->save();

		if($check_shipping_diff){
			$shipping = $this->getOnepagecheckout()->getQuote()->getShippingAddress();
			$shippingMethod_after = $shipping->getShippingMethod();

	        if($shippingMethod_before != $shippingMethod_after)
	        {
	        	$result['update_section']['shipping-method'] = $this->_getShippingMethodsHtml();
	        	$result['reload_totals'] = 'true';
	        }
	        else
	        	unset($result['reload_totals']);
        }
///////////////

        $result['update_section']['review'] = $this->_getReviewHtml();




        /*********** DISCOUNT CODES **********/
    	if ($couponChanged) {
            if ($couponData['code'] == $quote->getCouponCode()) {
                if($couponData['code'] != NULL){
                  Mage::getSingleton('checkout/session')->addSuccess(
                    Mage::helper('onepagecheckout')->__('Coupon code "%s" was applied.', Mage::helper('core')->htmlEscape($couponData['code']))
                );
              }
            } else {
                Mage::getSingleton('checkout/session')->addError(
                    Mage::helper('onepagecheckout')->__('Coupon code "%s" is not valid.', Mage::helper('core')->htmlEscape($couponData['code']))
                );
            }

        }
            $method = str_replace(' ', '', ucwords(str_replace('-', ' ', 'coupon-discount')));
            $result['update_section']['coupon-discount'] = $this->{'_get' . $method . 'Html'}();
        /************************************/

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    public function forgotpasswordAction()
    {
        $session = Mage::getSingleton('customer/session');

        if ($this->_expireAjax() || $session->isLoggedIn()) {
            return;
        }

        $email = $this->getRequest()->getPost('email');
        $result = array('success' => false);

        if (!$email)
        {
			$result['error'] = Mage::helper('onepagecheckout')->__('Please enter your email.');
        }
        else
        {
			if (!Zend_Validate::is($email, 'EmailAddress'))
			{
                $session->setForgottenEmail($email);
                $result['error'] = Mage::helper('onepagecheckout')->__('Invalid email address.');
            }
            else
            {
                $customer = Mage::getModel('customer/customer')->setWebsiteId(Mage::app()->getStore()->getWebsiteId())->loadByEmail($email);
                if(!$customer->getId())
                {
                	$session->setForgottenEmail($email);
                    $result['error'] = Mage::helper('onepagecheckout')->__('This email address was not found in our records.');
                }
                else
                {
                    try
                    {
						$new_pass = $customer->generatePassword();
                        $customer->changePassword($new_pass, false);
                        $customer->sendPasswordReminderEmail();
                        $result['success'] = true;
                        $result['message'] = Mage::helper('onepagecheckout')->__('A new password has been sent.');
                    }
                    catch (Exception $e)
                    {
                        $result['error'] = $e->getMessage();
                    }
                }
            }
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    public function loginAction()
    {
    	Mage::helper('onepagecheckout')->checkParams($this->getRequest());

        $session = Mage::getSingleton('customer/session');
        if ($this->_expireAjax() || $session->isLoggedIn()) {
            return;
        }

        $result = array('success' => false);

        if ($this->getRequest()->isPost())
        {
            $login_data = $this->getRequest()->getPost('login');
            if (empty($login_data['username']) || empty($login_data['password'])) {
            	$result['error'] = Mage::helper('onepagecheckout')->__('Login and password are required.');
            }
            else
            {
				try
				{
                    $session->login($login_data['username'], $login_data['password']);
                    $result['success'] = true;
                    $result['redirect'] = Mage::getUrl('*/*/index', array('_secure'=>true));
                }
                catch (Mage_Core_Exception $e)
                {
                    switch ($e->getCode()) {
                        case Mage_Customer_Model_Customer::EXCEPTION_EMAIL_NOT_CONFIRMED:
                            $message = Mage::helper('onepagecheckout')->__('Email is not confirmed. <a href="%s">Resend confirmation email.</a>', Mage::helper('customer')->getEmailConfirmationUrl($login_data['username']));
                            break;
                        default:
                            $message = $e->getMessage();
                    }
                    $result['error'] = $message;
                    $session->setUsername($login_data['username']);
                }
            }
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    public function saveOrderAction()
    {
      //  echo $this->getResponse()->getBody()."____";exit;
        if ($this->_expireAjax()) {
            return;
        }

        if(!$this->_isOnepagecheckoutBlock(true))
        	return;

        $result = array();
        try {
            $bill_data = $this->_filterPostData($this->getRequest()->getPost('billing', array()));

//			$result = $this->getOnepagecheckout()->saveBilling($bill_data,$this->getRequest()->getPost('billing_address_id', false));
			$result = $this->getOnepagecheckout()->saveBilling($bill_data,$this->getRequest()->getPost('billing_address_id', false),true,true);
            if ($result)
            {
            	$result['error_messages'] = $result['message'];
            	$result['error'] = true;
                $result['success'] = false;
                $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
                return;
            }

            if ((!$bill_data['use_for_shipping'] || !isset($bill_data['use_for_shipping'])) && !$this->getOnepagecheckout()->getQuote()->isVirtual())
            {
//				$result = $this->getOnepagecheckout()->saveShipping($this->_filterPostData($this->getRequest()->getPost('shipping', array())),$this->getRequest()->getPost('shipping_address_id', false));
                // print_r($_POST);exit();
				$result = $this->getOnepagecheckout()->saveShipping($this->_filterPostData($this->getRequest()->getPost('shipping', array())),$this->getRequest()->getPost('shipping_address_id', false), true, true);
                if ($result)
                {
                	$result['error_messages'] = $result['message'];
                	$result['error'] = true;
                    $result['success'] = false;
                    $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
                    return;
                }
            }

            $agreements = Mage::helper('onepagecheckout')->getAgreeIds();
            if($agreements)
            {
				$post_agree = array_keys($this->getRequest()->getPost('agreement', array()));
				$is_different = array_diff($agreements, $post_agree);
                if ($is_different)
                {
                	$result['error_messages'] = Mage::helper('onepagecheckout')->__('Please agree to all the terms and conditions.');
                	$result['error'] = true;
                    $result['success'] = false;

                    $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
                    return;
                }
            }

            $result = $this->_saveOrderPurchase();

            if($result && !isset($result['redirect']))
            {
                $result['error_messages'] = $result['error'];
            }

            if(!isset($result['error']))
            {
                Mage::dispatchEvent('checkout_controller_onepage_save_shipping_method', array('request'=>$this->getRequest(), 'quote'=>$this->getOnepagecheckout()->getQuote()));
                $this->_subscribeNews();
            }

            Mage::getSingleton('customer/session')->setOrderCustomerComment($this->getRequest()->getPost('order-comment'));

            if (!isset($result['redirect']) && !isset($result['error']))
            {
            	$pmnt_data = $this->getRequest()->getPost('payment', false);
                if ($pmnt_data)
                    $this->getOnepagecheckout()->getQuote()->getPayment()->importData($pmnt_data);

                $this->getOnepagecheckout()->saveOrder();
                $redirectUrl = $this->getOnepagecheckout()->getCheckout()->getRedirectUrl();

                $result['success'] = true;
                $result['error']   = false;
                $result['order_created'] = true;
            }
        }
        catch (Mage_Core_Exception $e)
        {
            Mage::logException($e);
            Mage::helper('checkout')->sendPaymentFailedEmail($this->getOnepagecheckout()->getQuote(), $e->getMessage());

            $result['error_messages'] = $e->getMessage();
            $result['error'] = true;
            $result['success'] = false;

            $goto_section = $this->getOnepagecheckout()->getCheckout()->getGotoSection();
            if ($goto_section)
            {
            	$this->getOnepagecheckout()->getCheckout()->setGotoSection(null);
                $result['goto_section'] = $goto_section;
            }

            $update_section = $this->getOnepagecheckout()->getCheckout()->getUpdateSection();
            if ($update_section)
            {
                if (isset($this->_sectionUpdateFunctions[$update_section]))
                {
                    $layout = $this->_getUpdatedLayout();

                    $updateSectionFunction = $this->_sectionUpdateFunctions[$update_section];
                    $result['update_section'] = array(
                        'name' => $update_section,
                        'html' => $this->$updateSectionFunction()
                    );
                }
                $this->getOnepagecheckout()->getCheckout()->setUpdateSection(null);
            }

            $this->getOnepagecheckout()->getQuote()->save();
        }
        catch (Exception $e)
        {
            Mage::logException($e);
            Mage::helper('checkout')->sendPaymentFailedEmail($this->getOnepagecheckout()->getQuote(), $e->getMessage());
            $result['error_messages'] = Mage::helper('onepagecheckout')->__('There was an error processing your order. Please contact support or try again later.');
            $result['error']    = true;
            $result['success']  = false;

            $this->getOnepagecheckout()->getQuote()->save();
        }

        if (isset($redirectUrl)) {
            $result['redirect'] = $redirectUrl;
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    protected function _saveOrderPurchase()
    {
    	$result = array();

        try
        {
            $pmnt_data = $this->getRequest()->getPost('payment', array());
            $result = $this->getOnepagecheckout()->savePayment($pmnt_data);

            $redirectUrl = $this->getOnepagecheckout()->getQuote()->getPayment()->getCheckoutRedirectUrl();
            if ($redirectUrl)
            {
                $result['redirect'] = $redirectUrl;
            }
        }
        catch (Mage_Payment_Exception $e)
        {
            if ($e->getFields()) {
                $result['fields'] = $e->getFields();
            }
            $result['error'] = $e->getMessage();
        }
        catch (Mage_Core_Exception $e)
        {
            $result['error'] = $e->getMessage();
        }
        catch (Exception $e)
        {
            Mage::logException($e);
            $result['error'] = Mage::helper('onepagecheckout')->__('Unable to set Payment Method.');
        }
        return $result;
    }

    protected function _subscribeNews()
    {
        if ($this->getRequest()->isPost() && $this->getRequest()->getPost('newsletter'))
        {
            $customerSession = Mage::getSingleton('customer/session');

            if($customerSession->isLoggedIn())
            	$email = $customerSession->getCustomer()->getEmail();
            else
            {
            	$bill_data = $this->getRequest()->getPost('billing');
            	$email = $bill_data['email'];
            }

            try {
                if (!$customerSession->isLoggedIn() && Mage::getStoreConfig(Mage_Newsletter_Model_Subscriber::XML_PATH_ALLOW_GUEST_SUBSCRIBE_FLAG) != 1)
                    Mage::throwException(Mage::helper('onepagecheckout')->__('Sorry, subscription for guests is not allowed. Please <a href="%s">register</a>.', Mage::getUrl('customer/account/create/')));

                $ownerId = Mage::getModel('customer/customer')->setWebsiteId(Mage::app()->getStore()->getWebsiteId())->loadByEmail($email)->getId();

                if ($ownerId !== null && $ownerId != $customerSession->getId())
                    Mage::throwException(Mage::helper('onepagecheckout')->__('Sorry, you are trying to subscribe email assigned to another user.'));

                $status = Mage::getModel('newsletter/subscriber')->subscribe($email);
            }
            catch (Mage_Core_Exception $e) {
            }
            catch (Exception $e) {
            }
        }
    }

    protected function _filterPostData($data)
    {
        $data = $this->_filterDates($data, array('dob'));
        return $data;
    }

    protected function _checkChangedAddress($data, $addr_type = 'Billing', $addr_id = false)
    {
    	$method	= "get{$addr_type}Address";
        $address = $this->getOnepagecheckout()->getQuote()->{$method}();

        if(!$addr_id)
        {
        	if(($address->getRegionId()	!= $data['region_id']) || ($address->getPostcode() != $data['postcode']) || ($address->getCountryId() != $data['country_id']))
        		return true;
        	else
        		return false;
        }
        else{
        	if($addr_id != $address->getCustomerAddressId())
        		return true;
        	else
        		return false;
        }
    }

    public function updatedShippingAction(){

        $address  = Mage::getSingleton('checkout/session')->getQuote()->getBillingAddress();
        $shipping_addr_data  = '<div class="shiphead">Shipping address</div>';
        $shipping_addr_data .= '<div class="show_addr_shipping" id="billingaddress-'.$address->getEntityId().'>';
        $shipping_addr_data .= '<span class="ship_custname">'.$address->getFirstname().' '.$address->getLastname().'</span>';
        $shipping_addr_data .= '<div>'.$address->getStreet[0].'</div>';
            if($address->getStreet[1]){
        $shipping_addr_data .= '<div>'.$address->getStreet[1].'</div>';
            }
        $shipping_addr_data .= '<div>'.$address->getCity().'</div>';
        $shipping_addr_data .= '<div>'.$address->getRegion().'</div>';
        $shipping_addr_data .= '<div>'.$address->getPostCode().'</div>';
        $shipping_addr_data .= '<div>'.$address->getTelephone().'</div>';
        $shipping_addr_data .= '</div>';
        //print_r($shipping_addr_data);
        echo $shipping_addr_data;
    }

    public function loginGuestUserAction(){
        $email = $this->getRequest()->getPost('email');

        $customer = Mage::getModel('customer/customer');
        $websiteId = Mage::app()->getWebsite()->getId();
       /* if (array_key_exists('email', $_POST)) {
            $email = $_POST['email'];
        } else {
            $this->getResponse()->setBody(false);
            return;
        }*/
        if ($websiteId) {
            $customer->setWebsiteId($websiteId);
        }
        $customer->loadByEmail($email);
        if ($customer->getId()) {
            $this->getResponse()->setBody(true);
            return;
        }
        $this->getResponse()->setBody(false);
        return;
    }

    public function registerUserCheckoutAction(){

        $emailCheckout = $this->getRequest()->getPost('email');
        $fnameCheckout = $this->getRequest()->getPost('fname');
        $lnameCheckout = $this->getRequest()->getPost('lname');
        $mob           = $this->getRequest()->getPost('mob');
        $current_step  = $this->getRequest()->getPost('current_step');
        $mobMatch = 0;
        $passwordLength = 8;
        if($emailCheckout != '' && $fnameCheckout != '' && $lnameCheckout != '' && $mob != '' && $current_step == "address-step"){
            $customer = Mage::getModel('customer/customer');
            $customer->setWebsiteId(Mage::app()->getWebsite()->getId());
            $customer->loadByEmail($emailCheckout);
            /*
            * Check if the email exist on the system.
            * If YES,  it will not create a user account.
            */

            if(!$customer->getId()) {
                // $allTelephoneDatas = $this->getAllTelephoneDataMobileAction();

                // foreach($allTelephoneDatas as $allTelephoneData){
                //     if(trim($allTelephoneData) == trim($mob)){
                //         $mobMatch = 1;
                //         break;
                //     }
                // }
                // Register user only if the mobile dosenot match with existing in db.
                // if($mobMatch == 0){

                   //setting data such as email, firstname, lastname, and password
                    try{
                        $customer->setEmail($emailCheckout);
                        $customer->setFirstname($fnameCheckout);
                        $customer->setLastname($lnameCheckout);
                        $login_password = $customer->generatePassword($passwordLength);
                        $customer->setPassword($login_password);
                        //the save the data and send the new account email.
                        $customer->save();
                        $customer->setConfirmation(null);
                        $customer->save();
                        //$customer->sendNewAccountEmail();
                        // Send sms on user registration.
                        $webisteName = strtoupper(Mage::app()->getWebsite()->getName());
                        $msg = "You are now registered with ".$webisteName." . Thankyou";
                        $smsHelper = Mage::helper('smsapp/data');
                        $sendSms   = $smsHelper->sensSms($msg, $mob);

                      $template = Mage::getModel('core/email_template') ->loadByCode('new_account_checkout')->getTemplateId();

                    // create the sender array containing the store name and email
                    $sender  = array(
                        'name' => Mage::getStoreConfig('trans_email/ident_support/name', Mage::app()->getStore()->getId()),
                        'email' => Mage::getStoreConfig('trans_email/ident_support/email', Mage::app()->getStore()->getId())
                    );

                    // customers email gathered previously
                    $customerEmail = $emailCheckout;

                    // customers name gathered previously
                    $customerName = $fnameCheckout." ".$lnameCheckout;

                    // // create the order object based on the orders ID, which was gathered previously
                    // $order = Mage::getModel('sales/order')->load($order_id);

                    // create our vars for our email, $customer object created previously
                    // $vars = Array( 'customer'=> $customer, 'order' => $order );
                    $vars = Array( 'customer'=> $customer );

                    // send the email using the default store
                    Mage::getModel('core/email_template')->sendTransactional($template, $sender, $customerEmail, $customerName, $vars);

                    Mage::getModel('newsletter/subscriber')->subscribe($emailCheckout);

                    //Store registered user email in session

                    $email_session = Mage::getSingleton('core/session')->setEmailCheckout($customerEmail);
                    echo 'successfully registered ';

                    }

                    catch(Exception $ex){
                        echo 'Not registered';
                    }
                // }
                // else{
                //     echo 'Mobile no exists';
                // }
            }
            else{
                echo 'customer already registered';
            }
        }
    }

    /*public function getAllCustomersAction(){
    $currentCustomerMobile = Mage::getSingleton('customer/session')->getCustomer()->getMobile();

    $custEmails = array();
    $i = 0;
    $custWebsiteId   = Mage::app()->getWebsite()->getId();
    $custCollections = Mage::getModel('customer/customer')->getCollection()
        ->addFieldToFilter('website_id', $custWebsiteId)
        ->addAttributeToSelect('email')
        ->addAttributeToSelect('mobile');

        foreach ($custCollections as $custCollection)
        {

            if($currentCustomerMobile=="" || $currentCustomerMobile!=$custCollection->getMobile()):
                   $custEmails[$i]['email']  = $custCollection->getEmail();
                   $custEmails[$i]['mobile'] = $custCollection->getMobile();

                   $customer = Mage::getModel('customer/customer');

                   $customer->setWebsiteId(Mage::app()->getWebsite()->getId());

                   $customer->loadByEmail($custCollection->getEmail());

                   $data = array();
                   $j = 0;
                     foreach ($customer->getAddresses() as $address)
                     {
                        $data[$j] = $address['telephone'];
                        $j++;
                     }

                     $custEmails[$i]['telephone'] = $data;
                   $i++;
            endif;
        }
        //echo '<pre>';print_r($custEmails);
       echo json_encode($custEmails);
    }*/

    /*public function getAllTelephonesAction(){
        $customerId     = Mage::app()->getRequest()->getPost('customId');
        $customerEmail  = Mage::app()->getRequest()->getPost('custom_email');
        $customerMobile = Mage::app()->getRequest()->getPost('custom_mobile');
        $custMobile = array();
        $i = 0;
        $custWebsiteId   = Mage::app()->getWebsite()->getId();
        $custCollections = Mage::getModel('customer/customer')->getCollection()
            ->addFieldToFilter('website_id', $custWebsiteId)
            ->addAttributeToSelect('email')
            ->addAttributeToSelect('mobile');

            $customer = Mage::getModel('customer/customer');
            $customer->setWebsiteId(Mage::app()->getWebsite()->getId());
            foreach ($custCollections as $custCollection)
            {
               //$custMobile[$i]['email']  = $custCollection->getEmail();
                if($custCollection->getMobile() != '' && $custCollection->getMobile() != $customerMobile){
                    $custMobile[$i] = $custCollection->getMobile();
                    $i++;
                }

                if($custCollection->getEmail() != $customerEmail){
                    $customer->loadByEmail($custCollection->getEmail());
                    $data = array();
                    foreach ($customer->getAddresses() as $address)
                    {
                        if($address['telephone'] != ''){
                            $custMobile[$i] = $address['telephone'];
                            $i++;
                        }
                    }
                }
            }
        echo json_encode($custMobile);
    }*/

    public function getAllTelephoneDataMobileAction(){
        //$customerId     = Mage::app()->getRequest()->getPost('customId');
        //$customerEmail  = Mage::app()->getRequest()->getPost('custom_email');
        //$customerMobile = Mage::app()->getRequest()->getPost('custom_mobile');
        $custMobile = array();
        $i = 0;
        $custWebsiteId   = Mage::app()->getWebsite()->getId();
        $custCollections = Mage::getModel('customer/customer')->getCollection()
            ->addFieldToFilter('website_id', $custWebsiteId)
            ->addAttributeToSelect('email')
            ->addAttributeToSelect('mobile');

            $customer = Mage::getModel('customer/customer');
            $customer->setWebsiteId(Mage::app()->getWebsite()->getId());
            foreach ($custCollections as $custCollection)
            {
               //$custMobile[$i]['email']  = $custCollection->getEmail();
                if($custCollection->getMobile() != ''){
                    $custMobile[$i] = $custCollection->getMobile();
                    $i++;
                }

                //if($custCollection->getEmail() != $customerEmail){
                    $customer->loadByEmail($custCollection->getEmail());
                    $data = array();
                    foreach ($customer->getAddresses() as $address)
                    {
                        if($address['telephone'] != ''){
                            $custMobile[$i] = $address['telephone'];
                            $i++;
                        }
                    }
                //}
            }
            //echo '<pre>';
            //print_r($custMobile);
        return $custMobile;
        //echo json_encode($custMobile);
    }

    public function checkCaptchaAction()
    {
        $paymentType=array();
        $paymentType = Mage::app()->getRequest()->getParam('payment');
        if(($paymentType['method']=='phoenix_cashondelivery') || ($paymentType['method']=='payatstore') || ($paymentType['method']=='trailatstore') || ($paymentType['method']=='cashpayment')):
        //echo Mage::getSingleton('checkout/session')->getQuote()->getPayment()->getMethodInstance()->getTitle();;;exit;
        $formId = 'onepagecheckout_payment';
        $captchaModel = Mage::helper('captcha')->getCaptcha($formId);   //.$payment['method']
        if ($captchaModel->isRequired()) {
           $controller = $this;
           Mage::log(" Check ".$formId.$paymentType['method']."- " .$captchaModel->getWord(). "==" .$this->_getCaptchaString($controller->getRequest(), $formId.$paymentType['method']),null,'captcha.log',true);
            if (!$captchaModel->isCorrect($this->_getCaptchaString($controller->getRequest(), $formId.$paymentType['method']))) {
                //Mage::getSingleton('customer/session')->addError(Mage::helper('captcha')->__('Incorrect CAPTCHA.'));
                $controller->setFlag('', Mage_Core_Controller_Varien_Action::FLAG_NO_DISPATCH, true);
                $result = array('error' => 1, 'message' => Mage::helper('captcha')->__('Incorrect CAPTCHA.'));
                $controller->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
               }
            else
            {
                $result = array('error' => 0, 'message' => Mage::helper('captcha')->__('Correct CAPTCHA.'));
                $controller->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
            }

        }
        return $this;

        endif;
    }

     /**
     * Get Captcha String
     *
     * @param Varien_Object $request
     * @param string $formId
     * @return string
     */
    protected function _getCaptchaString($request, $formId)
    {
        $captchaParams = $request->getPost(Mage_Captcha_Helper_Data::INPUT_NAME_FIELD_VALUE);
        // echo $captchaParams[$formId];
        return $captchaParams[$formId];
    }


    /**
     * Action to save payment details while selecting
     * payment option
     *
     * @param Null
     * @param Null
     * @return Boolean True
     */

    public function savePaymentAction()
        {
            $data = Mage::app()->getRequest()->getPost('payment', array());
            $quote = Mage::getModel('checkout/session')->getQuote();

            if (empty($data)) {
                return array('error' => -1, 'message' => Mage::helper('checkout')->__('Invalid data.'));
            }


            $quote = Mage::getModel('checkout/session')->getQuote();
            if ($quote->isVirtual()) {
                $quote->getBillingAddress()->setPaymentMethod(isset($data['method']) ? $data['method'] : null);
            } else {
                $quote->getShippingAddress()->setPaymentMethod(isset($data['method']) ? $data['method'] : null);
            }

           if (!$quote->isVirtual() && $quote->getShippingAddress()) {
                $quote->getShippingAddress()->setCollectShippingRates(true);
            }

            if($data['method'] == 'trailatstore'){
                $data['trail_date'] = ' ';
                $data['trail_time'] = ' ';
                // $quote->getShippingAddress()->setFreeShipping(true);
                $quote->getShippingAddress()->setShippingMethod('freeshipping_freeshipping');
            }else if($data['method'] == 'payatstore'){
                $quote->getShippingAddress()->setShippingMethod('freeshipping_freeshipping');
            }
            $data['checks'] = Mage_Payment_Model_Method_Abstract::CHECK_USE_CHECKOUT
                | Mage_Payment_Model_Method_Abstract::CHECK_USE_FOR_COUNTRY
                | Mage_Payment_Model_Method_Abstract::CHECK_USE_FOR_CURRENCY
                | Mage_Payment_Model_Method_Abstract::CHECK_ORDER_TOTAL_MIN_MAX
                | Mage_Payment_Model_Method_Abstract::CHECK_ZERO_TOTAL;
            $payment = $quote->getPayment();
            $payment->importData($data);

            $quote->save();
            Mage::dispatchEvent('checkout_save_payment_after', null);
            return true;
        }

    public function setCustomerNameCheckoutAction(){
        $fname = Mage::app()->getRequest()->getParam('fname');
        $lname = Mage::app()->getRequest()->getParam('lname');
        $customerSession = Mage::getSingleton('customer/session')->getCustomer();
        $customerSession->setFirstname($fname);
        $customerSession->setLastname($lname)->save();
        echo 'name saved successfully';
    }

    public function PincodeAction(){
        $response=array();
        $zipcode = $this->getRequest()->getParam('zipcode');
        $pincode=Mage::getModel('onepagecheckout/onepagecheckout')->load($zipcode,"postcode");
        
        $cart = Mage::getModel('checkout/cart')->getQuote();
        foreach ($cart->getAllItems() as $item) {
            $checkoutMethod[] = $item->getCheckoutMethod();
        }

        $response = array();
        if(in_array('deliverable', $checkoutMethod)) {
            if($pincode->getData()) {
                $response['status']="success";
                $response['city'] = $pincode->getCity();
                $response['state'] = $pincode->getRegion();
                $response['region_id'] = $pincode->getRegionId();
            }       
        } else{
            $response['status']="success";
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
        return;
    }

}
