<?php
class Rofra_Salesorderitemgrid_Block_Adminhtml_Sales_Order_Shipment_View extends Mage_Adminhtml_Block_Sales_Order_Shipment_View
{
	public function updateBackButtonUrl($flag)
    {
        if ($flag) {
            if ($this->getShipment()->getBackUrl()) {
                return $this->_updateButton('back', 'onclick', 'setLocation(\'' . $this->getShipment()->getBackUrl()
                    . '\')');
            }
            return $this->_updateButton('back', 'onclick', 'setLocation(\'' . $_SERVER["HTTP_REFERER"] . '\')');
        }
        return $this;
    }
}
			