<?php


class Rofra_Salesorderitemgrid_Block_Adminhtml_Order_Items_Grid_Renderer_Replacementagainst
extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract{


public function render(Varien_Object $row)
    {
        $parentId = $row->getParentItemId();
        if($parentId){  //Product is configurable
            $orderItemModel = Mage::getModel('sales/order_item')->load($parentId);
            $replacedOrderId = $orderItemModel->getReplacedOrderId();
            if($replacedOrderId){
                echo $replacedOrderId;
            }
        }else{      //Product is simple
            $lineItemId = $row->getId();
            $orderItemModel = Mage::getModel('sales/order_item')->load($lineItemId);
            $replacedOrderId = $orderItemModel->getReplacedOrderId();
            if($replacedOrderId){
                echo $replacedOrderId;
            }
        }
        return $html;
    }


}
