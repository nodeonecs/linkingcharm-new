<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   Brainvire
 * @package    Brainvire_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * @license    
 */



class Brainvire_Points_Model_Paypal_Standard extends Mage_Paypal_Model_Standard {

    public function getStandardCheckoutFormFields() {
        if ($this->getQuote()->getIsVirtual()) {
            $a = $this->getQuote()->getBillingAddress();
            $b = $this->getQuote()->getShippingAddress();
        } else {
            $a = $this->getQuote()->getShippingAddress();
            $b = $this->getQuote()->getBillingAddress();
        }
       
        $currency_code = $this->getQuote()->getBaseCurrencyCode();
        

        $sArr = array(
            'charset' => self::DATA_CHARSET,
            'business' => Mage::getStoreConfig('paypal/wps/business_account'),
            'return' => Mage::getUrl('paypal/standard/success', array('_secure' => true)),
            'cancel_return' => Mage::getUrl('paypal/standard/cancel', array('_secure' => false)),
            'notify_url' => Mage::getUrl('paypal/standard/ipn'),
            'invoice' => $this->getCheckout()->getLastRealOrderId(),
            'currency_code' => $currency_code,
            'address_override' => 1,
            'first_name' => $a->getFirstname(),
            'last_name' => $a->getLastname(),
            'address1' => $a->getStreet(1),
            'address2' => $a->getStreet(2),
            'city' => $a->getCity(),
            'state' => $a->getRegionCode(),
            'country' => $a->getCountry(),
            'zip' => $a->getPostcode(),
            'bn' => 'Varien_Cart_WPS_US'
        );

        $logoUrl = Mage::getStoreConfig('paypal/wps/logo_url');
        if ($logoUrl) {
            $sArr = array_merge($sArr, array(
                'cpp_header_image' => $logoUrl
                    ));
        }

        if ($this->getConfigData('payment_action') == self::PAYMENT_TYPE_AUTH) {
            $sArr = array_merge($sArr, array(
                'paymentaction' => 'authorization'
                    ));
        }

        $transaciton_type = $this->getConfigData('transaction_type');
        
        if ($transaciton_type == 'O') {
            $businessName = Mage::getStoreConfig('paypal/wps/business_name');
            $storeName = Mage::getStoreConfig('store/system/name');
            $amount = ($a->getBaseSubtotal() + $b->getBaseSubtotal()) - ($a->getBaseDiscountAmount() + $b->getBaseDiscountAmount());

            $session = Mage::getSingleton('checkout/session');
            if ($session->getData('use_points')) {
                if ($a->getBaseGrandTotal() || $b->getBaseGrandTotal()) {
                    $pointsAmountUsed = $session->getData('points_amount');
                    $rate = Mage::getModel('points/rate')->loadByDirection(Brainvire_Points_Model_Rate::POINTS_TO_CURRENCY);
                    $moneyBaseCurrencyForPoints = $rate->exchange($pointsAmountUsed);
                    $amount -= $moneyBaseCurrencyForPoints;
                    if ($amount <= 0)
                        $amount = 0;
                }
            }

            $sArr = array_merge($sArr, array(
                'cmd' => '_ext-enter',
                'redirect_cmd' => '_xclick',
                'item_name' => $businessName ? $businessName : $storeName,
                'amount' => sprintf('%.2f', $amount),
                    ));
            $_shippingTax = $this->getQuote()->getShippingAddress()->getBaseTaxAmount();
            $_billingTax = $this->getQuote()->getBillingAddress()->getBaseTaxAmount();
            $tax = sprintf('%.2f', $_shippingTax + $_billingTax);
            if ($tax > 0) {
                $sArr = array_merge($sArr, array(
                    'tax' => $tax
                        ));
            }
        } else {
            $sArr = array_merge($sArr, array(
                'cmd' => '_cart',
                'upload' => '1',
                    ));

           
            $session = Mage::getSingleton('checkout/session');
            if ($session->getData('use_points')) {
                if ($a->getBaseGrandTotal() || $b->getBaseGrandTotal()) {
                    $pointsAmountUsed = $session->getData('points_amount');
                    $rate = Mage::getModel('points/rate')->loadByDirection(Brainvire_Points_Model_Rate::POINTS_TO_CURRENCY);
                    $moneyBaseCurrencyForPoints = $rate->exchange($pointsAmountUsed);
                    $sArr = array_merge($sArr, array(
                        'discount_amount_cart' => sprintf('%.2f', $moneyBaseCurrencyForPoints)
                            ));
                }
            }

            $items = $this->getQuote()->getAllItems();
            if ($items) {
                $i = 1;
                $summaryTax = 0;
                foreach ($items as $item) {
                    if ($item->getParentItem()) {
                        continue;
                    }
                    
                    $sArr = array_merge($sArr, array(
                        'item_name_' . $i => $item->getName(),
                        'item_number_' . $i => $item->getSku(),
                        'quantity_' . $i => $item->getQty(),
                        'amount_' . $i => sprintf('%.2f', ($item->getBaseCalculationPrice() - $item->getBaseDiscountAmount())),
                            ));
                    if ($item->getBaseTaxAmount() > 0) {
                        $summaryTax += $item->getBaseTaxAmount() / $item->getQty();
                    }
                    $i++;
                }
            }
        }

        $totalArr = $a->getTotals();
        $shipping = sprintf('%.2f', $this->getQuote()->getShippingAddress()->getBaseShippingAmount());
        if ($shipping > 0 && !$this->getQuote()->getIsVirtual()) {
            if ($transaciton_type == 'O') {
                $sArr = array_merge($sArr, array(
                    'shipping' => $shipping
                        ));
            } else {
                $shippingTax = $this->getQuote()->getShippingAddress()->getBaseShippingTaxAmount();
                $sArr = array_merge($sArr, array(
                    'item_name_' . $i => $totalArr['shipping']->getTitle(),
                    'quantity_' . $i => 1,
                    'amount_' . $i => sprintf('%.2f', $shipping),
                        ));
                $summaryTax += $shippingTax;
                $i++;
            }
        }

        if ($transaciton_type != 'O') {
            $sArr = array_merge($sArr, array(
                'tax_cart' => sprintf('%.2f', $summaryTax),
                    ));
        }

        $sReq = '';
        $sReqDebug = '';
        $rArr = array();


        foreach ($sArr as $k => $v) {
            
            $value = str_replace("&", "and", $v);
            $rArr[$k] = $value;
            $sReq .= '&' . $k . '=' . $value;
            $sReqDebug .= '&' . $k . '=';
            if (in_array($k, $this->_debugReplacePrivateDataKeys)) {
                $sReqDebug .= '***';
            } else {
                $sReqDebug .= $value;
            }
        }

        if ($this->getDebug() && $sReq) {
            $sReq = substr($sReq, 1);
            $debug = Mage::getModel('paypal/api_debug')
                    ->setApiEndpoint($this->getPaypalUrl())
                    ->setRequestBody($sReq)
                    ->save();
        }

        return $rArr;
    }

}
