<?php
class Aster_Styler_Helper_Ticker extends Mage_Core_Helper_Abstract
{
	const XML_PATH_CONFIG_TICKER = 'linkingcharm_config/general_config/header_ticker';

    /**
     * https://raivis.com/how-to-add-custom-table-like-configuration-field-in-magento-admin/
     * Returns un-serialized data of the custom config field one
     *
     * @return array
     */
    public function getAllTickers()
    {
        $config = Mage::getStoreConfig(self::XML_PATH_CONFIG_TICKER);

        if (!$config) {
            return array();
        }

        try {
            $config = Mage::helper('core/unserializeArray')->unserialize($config);
        } catch (Exception $exception) {
            Mage::logException($exception);
            $config = array(); // Return an array if failed to un-serialize data
        }

        return $config;
    }
}
	 