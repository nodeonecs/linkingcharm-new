<?php

class Brainvire_Points_Block_About
    extends Mage_Adminhtml_Block_Abstract
    implements Varien_Data_Form_Element_Renderer_Interface
{

    
    public function render(Varien_Data_Form_Element_Abstract $element)
    {
		$logopath	=	'http://www.brainvire.com/wp-content/themes/brainvire/images/brainvire-logo.png';
        $html = <<<HTML
<div style="background:url('$logopath') no-repeat scroll 14px 14px #EAF0EE;border:1px solid #CCCCCC;margin-bottom:10px;padding:10px 5px 5px 264px;">
     <p>
        <strong>PREMIUM and FREE MAGENTO EXTENSIONS</strong><br />
        <a href="http://www.magentocommerce.com/magento-connect/developer/brainvire#extensions" target="_blank">Brainvire</a> offers a wide choice of nice-looking and easily editable free and premium Magento extensions. You can find free and paid extensions for the extremely popular Magento eCommerce platform.<br />       
    </p>
    <p>
        My extensions on <a href="http://www.magentocommerce.com/magento-connect/developer/brainvire#extensions" target="_blank">MagentoConnect</a><br />
        Should you have any questions email at <a href="mailto:info@brainvire.com">info@brainvire.com</a>
        <br />
    </p>
</div>
HTML;
        return $html;
    }
}
