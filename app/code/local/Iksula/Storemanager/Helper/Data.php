<?php
class Iksula_Storemanager_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * convert array to options
     *
     * @access public
     * @param $options
     * @return array
     * @author Ultimate Module Creator
     */
    public function convertOptions($options)
    {
        $converted = array();
        foreach ($options as $option) {
            if (isset($option['value']) && !is_array($option['value']) &&
                isset($option['label']) && !is_array($option['label'])) {
                $converted[$option['value']] = $option['label'];
            }
        }
        return $converted;
    }

    /**************
            Author :- Rahul Bhandary
            function :- Create a Role ,User and Permission according to the store created.
            Date :- 07-April-16


    ***************************/

    public function createPermissionAmrole($role_id , $store_def , $storeviewid , $website_id){
            $data ['role_id'] = $role_id;
        if($store_def == 'store'){
            $data['scope_websites_warehouse'] = NULL;
            $data['scope_hub'] = NULL;
            $data['scope_storeviews'] = $storeviewid;
            $data['custom_role'] = 'STORE_VIEW';
        }elseif($store_def == 'warehouse'){
            $data['scope_websites'] = $website_id;
            $data['scope_hub'] = NULL;
            $data['custom_role'] = 'WAREHOUSE';
            $data['scope_websites_warehouse'] = $website_id;
        }elseif($store_def == 'hub'){
            $data['scope_websites_warehouse'] = NULL;
            $data['scope_hub'] = $storeviewid;
            $aStoresViewIds = array();
            $aHubsIdsSelected = $storeviewid;
            $StoreGroupIdCollections = Mage::getModel('core/store')->getCollection()
            ->addFieldToFilter('store_id' , array('in' => $aHubsIdsSelected));
            foreach($StoreGroupIdCollections as $storekey => $storeValue)
                $aStoreIds [] = $storeValue->getGroupId();
            $StoreHubCheck = Mage::getModel('iksula_storemanager/storehubrelation')->getCollection()
                                ->addFieldToFilter('store_id' , array('in' => $aStoreIds));

               foreach($StoreHubCheck as $key => $StoreData){
                    if($StoreData->getIsHub() == '1'){
                            $Hub_id = $StoreData->getStoreId();
                        $StoreHubAssignment = Mage::getModel('iksula_storemanager/storehubrelation')->getCollection()
                                ->addFieldToFilter('hub_id' , array('eq' => $Hub_id))
                                ->addFieldToSelect('store_id');
                         foreach($StoreHubAssignment as $key => $value)
                            $aStoresIds [] = $value->getStoreId();

                    }

               }

               $StoreCollectionsFinal = Mage::getModel('core/store')->getCollection()->addFieldToFilter('group_id' , array('in' => $aStoresIds));
               foreach($StoreCollectionsFinal as $storekey => $storevalue)
                        $aStoresViewIds [] = $storevalue->getStoreId();

            $data['scope_storeviews'] = implode(',',array_unique($aStoresViewIds));
            $data['custom_role'] = 'HUB';

        }

            $model = Mage::getModel('amrolepermissions/rule')->setData($data);
            try {
                $insertId = $model->save()->getId();
                echo "Data successfully inserted. Insert ID: ".$insertId;
                //Mage::log($insertId , null , 'amrole.log');
            } catch (Exception $e){
             echo $e->getMessage();
             //Mage::log($e->getMessage() , null , 'amrole.log');
            }
    }


    public function createUserRoleForStore($aData , $lastviewid){

            $rolename = $aData ['store']['store_alias'];
            $store_def = $aData['store']['store_definition'];
            $website_id = $aData['store']['website_id'];
            $sUserName = $aData['store']['store_alias'];

        $resources = explode(',', $aData['resource']);

        $col = Mage::getModel('admin/role')->setRoleName($rolename)->setRoleType('G')->setTreeLevel(1)->save();
        if($col->getRoleId()){

        $this->createPermissionAmrole($col->getRoleId() , $store_def , $lastviewid , $website_id);

                if($aData['all']){

                    $resources = array("all");
                }

                    $rules = Mage::getModel('admin/rules')->setRoleId($col->getRoleId())->setResources($resources);

                $rules = Mage::getModel('admin/resource_rules')->saveRel($rules);
        }

        $user = Mage::getModel('admin/user')->setData(array(
            'username' => $sUserName,
            'firstname' => $aData['user']['firstname'],
            'lastname' => $aData['user']['lastname'],
            //'email' => $aData['user']['email'],
            //'email' => $sUserName.'@gmail.com',
            'password' => $aData['user']['password'],
            'is_active' => 1
        ))
            ->save();
            $user->setRoleIds(array($col->getRoleId()))
            ->setRoleUserId($user->getUserId())
            ->saveRelations();
    }

        public function getStoreAllCities() {
            $collection = Mage::getModel('core/store_group')->getCollection();
            $collection->getSelect()->join(
            'directory_country_region_city', 
            'main_table.store_city = directory_country_region_city.city_id', 
            array('*') 
            );
            // ->distinct(true)->group('store_city');
            $result = array();
            foreach ($collection as $value) {
            $cityname = $value->getData('cityname');
            $store_code = $value->getData('store_code');
            $result[$cityname][] .= $store_code;
            }
            return $result;
        }

    public function deleteUserRoleForstores($role_name , $user_name){

        $rolemodel = Mage::getSingleton('admin/role');
        $RoleCollections = $rolemodel->getCollection()
                        ->addFieldToFilter('role_name' , array('eq' => $role_name))
                        ->addFieldToFilter('parent_id' , array('eq' => '0'));
        foreach($RoleCollections as $role){

            $role_id = $role->getRoleId();
            $resource_model = Mage::getModel('admin/rules')->getCollection()->addFieldToFilter('role_id' , array('eq' => $role->getRoleId()));


            foreach($resource_model as $resource){

                try{
                   Mage::getModel('admin/rules')->setId($resource->getRuleId())->delete();
                }
                catch (Mage_Core_Exception $e) {
                    Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                }

            }
            Mage::getModel('admin/role')->setId($role->getRoleId())->delete();
        }

                $user_role_id = Mage::getModel('admin/role')->load($role_id , 'parent_id')->getRoleId();
                 try{
                   Mage::getModel('admin/role')->setId($user_role_id)->delete();
                }
                catch (Mage_Core_Exception $e) {
                    Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                }



                $user_id = Mage::getModel('admin/user')->load($user_name , 'username')->getUserId();
                 try{
                   Mage::getModel('admin/user')->setId($user_id)->delete();
                }
                catch (Mage_Core_Exception $e) {
                    Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                }
    }

}
