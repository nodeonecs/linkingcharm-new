<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   Brainvire
 * @package    Brainvire_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * @license    
 */


class Brainvire_Points_Model_Mysql4_Transaction extends Mage_Core_Model_Mysql4_Abstract {

    public function _construct() {
        $this->_init('points/transaction', 'id');
    }

    public function loadByOrderIncrementId($transaction, $orderIncrementId) {
        $select = $this->_getReadAdapter()->select()
                ->from($this->getTable('transaction_orderspend'))
                ->where('order_increment_id = ?', $orderIncrementId);
        $data = $this->_getReadAdapter()->fetchRow($select);
        if (isset($data['transaction_id'])) {
            $transaction->load($data['transaction_id'])->addData($data);
        }
        return $this;
    }

    public function saveSpendOrderInfo($transaction, $order) {
        $orderCustomer = Mage::getModel('customer/customer')->load($order->getCustomerId());
        $orderWebsite = $order->getStore()->getWebsite();
        $moneyForPointsBase = Mage::getModel('points/api')->changePointsToMoney($transaction->getBalanceChange(), $orderCustomer, $orderWebsite);
        $moneyForPoints = $order->getBaseCurrency()->convert($moneyForPointsBase, $order->getOrderCurrencyCode());

        $data = array(
            'transaction_id' => $transaction->getId(),
            'order_increment_id' => $order->getIncrementId(),
            'points_to_money' => $moneyForPoints,
            'base_points_to_money' => $moneyForPointsBase
        );
        $this->_getWriteAdapter()->insert($this->getTable('transaction_orderspend'), $data);
        return $this;
    }

}
