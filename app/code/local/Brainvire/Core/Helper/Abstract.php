<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */


class Brainvire_Core_Helper_Abstract extends Mage_Core_Helper_Abstract {

    
    public function log($message, $severity=null) {
        Mage::helper('brainvirecore/logger')->log($this, $message, $severity);
    }

}
