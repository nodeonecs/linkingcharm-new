<?php
/**
 * Iksula_Storemanager extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category       Iksula
 * @package        Iksula_Storemanager
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Store admin grid block
 *
 * @category    Iksula
 * @package     Iksula_Storemanager
 * @author      Ultimate Module Creator
 */
class Iksula_Storemanager_Block_Adminhtml_Store_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * constructor
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('storeGrid');
        $this->setDefaultSort('group_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * prepare collection
     *
     * @access protected
     * @return Iksula_Storemanager_Block_Adminhtml_Store_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('core/store_group')
            ->getCollection()->addFieldToFilter('default_store_id' , array('neq' => '1'));

            /*$collection->getSelect()->join('core_store_group', 'core_website.website_id = main_table.website_id',array('store_state','store_city'));*/

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * prepare grid collection
     *
     * @access protected
     * @return Iksula_Storemanager_Block_Adminhtml_Store_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'group_id',
            array(
                'header' => Mage::helper('iksula_storemanager')->__('Id'),
                'index'  => 'group_id',
                'type'   => 'number'
            )
        );

        $this->addColumn(
            'website_id',
            array(
                'header' => Mage::helper('iksula_storemanager')->__('Website Name'),
                'sortable' => true,
                'width' => '60',
                'index' => 'website_id',
                'renderer' => 'Iksula_Storemanager_Block_Adminhtml_Store_Edit_Renderer_Websitelabel',
            )
        );

        $this->addColumn(
            'store_code',
            array(
                'header' => Mage::helper('iksula_storemanager')->__('Store Code'),
                'sortable' => true,
                'width' => '60',
                'index' => 'store_code',
                //'renderer' => 'Iksula_Storemanager_Block_Adminhtml_Store_Edit_Renderer_Website',
            )
        );

        $this->addColumn(
            'name',
            array(
                'header' => Mage::helper('iksula_storemanager')->__('Store Name'),
                'index'  => 'name',
            )
        );



        // $this->addColumn(
        //     'store_city',
        //     array(
        //         'header' => Mage::helper('iksula_storemanager')->__('Store City'),
        //         'index'  => 'store_city',
        //         'renderer'  => 'Iksula_Storemanager_Block_Adminhtml_Renderer_Citylabelgrid',// THIS IS WHAT THIS POST IS ALL ABOUT
        //     )
        // );

        /*$fieldset->addType('label', 'Iksula_Storemanager_Block_Adminhtml_Renderer_Citylabel');
             $fieldset->addField('store_city', 'label', array(
                'label'     => Mage::helper('iksula_storemanager')->__('Store City'),
            ));
*/

        // $this->addColumn(
        //     'store_state',
        //     array(
        //         'header' => Mage::helper('iksula_storemanager')->__('Store State'),
        //         'index'  => 'store_state',
        //         'renderer'  => 'Iksula_Storemanager_Block_Adminhtml_Renderer_Statelabelgrid',// THIS IS WHAT THIS POST IS ALL ABOUT
        //     )
        // );


        // $this->addColumn(
        //     'store_pincode',
        //     array(
        //         'header' => Mage::helper('iksula_storemanager')->__('Store Pincode'),
        //         'index'  => 'store_pincode',
        //     )
        // );


        // $this->addColumn(
        //     'store_zone',
        //     array(
        //         'header' => Mage::helper('iksula_storemanager')->__('Store Zone'),
        //         'index'  => 'store_zone',

        //     )
        // );




        /*$this->addColumn(
            'store_status',
            array(
                'header' => Mage::helper('iksula_storemanager')->__('Status'),
                'sortable' => true,
                'width' => '60',
                'index' => 'store_status',
                'renderer' => 'Iksula_Storemanager_Block_Adminhtml_Store_Edit_Renderer_Statuslabel',
            )
        );*/

        /*$this->addColumn(
            'store_status',
            array(
                'header' => Mage::helper('iksula_storemanager')->__('Store Status'),
                'index'  => 'store_status',

            )
        );*/


            $this->addColumn("stores_assigned", array(
                'width'   => '100',
                "header" => Mage::helper("orderitemsstatestatus")->__("Stores Assigned"),
                "index" => "stores_assigned",
                'type' => 'text',
                'renderer' => 'Iksula_Storemanager_Block_Adminhtml_Store_Edit_Renderer_Storesassigned',
                ));


            $this->addColumn("store_definition", array(
                "header" => Mage::helper("iksula_storemanager")->__("Store Definition"),
                "index" => "store_definition",
                'type' => 'text',
                ));


            $this->addColumn("active_status", array(
                "header" => Mage::helper("iksula_storemanager")->__("Status"),
                "index" => "active_status",
                'type' => 'options',
                'options'=> array( '0' => 'Inactive', '1' => 'Active'),
                ));
            $this->addColumn("store_phone", array(
                "header" => Mage::helper("iksula_storemanager")->__("Store Phone for updates"),
                "index" => "store_phone"
                ));
            $this->addColumn("store_email", array(
                "header" => Mage::helper("iksula_storemanager")->__("Store Email for updates"),
                "index" => "store_email"
                ));

        $this->addColumn(
            'action',
            array(
                'header'  =>  Mage::helper('iksula_storemanager')->__('Action'),
                'width'   => '100',
                'type'    => 'action',
                'getter'  => 'getId',
                'actions' => array(
                    array(
                        'caption' => Mage::helper('iksula_storemanager')->__('Edit'),
                        'url'     => array('base'=> '*/*/edit'),
                        'field'   => 'id'
                    )
                ),
                'filter'    => false,
                'is_system' => true,
                'sortable'  => false,
            )
        );
        $this->addExportType('*/*/exportCsv', Mage::helper('iksula_storemanager')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('iksula_storemanager')->__('Excel'));
        $this->addExportType('*/*/exportXml', Mage::helper('iksula_storemanager')->__('XML'));
        return parent::_prepareColumns();
    }

    /**
     * prepare mass action
     *
     * @access protected
     * @return Iksula_Storemanager_Block_Adminhtml_Store_Grid
     * @author Ultimate Module Creator
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('store');
        $this->getMassactionBlock()->addItem(
            'delete',
            array(
                'label'=> Mage::helper('iksula_storemanager')->__('Delete'),
                'url'  => $this->getUrl('*/*/massDelete'),
                'confirm'  => Mage::helper('iksula_storemanager')->__('Are you sure?')
            )
        );
        $this->getMassactionBlock()->addItem(
            'status',
            array(
                'label'      => Mage::helper('iksula_storemanager')->__('Change status'),
                'url'        => $this->getUrl('*/*/massStatus', array('_current'=>true)),
                'additional' => array(
                    'status' => array(
                        'name'   => 'status',
                        'type'   => 'select',
                        'class'  => 'required-entry',
                        'label'  => Mage::helper('iksula_storemanager')->__('Status'),
                        'values' => array(
                            '1' => Mage::helper('iksula_storemanager')->__('Enabled'),
                            '0' => Mage::helper('iksula_storemanager')->__('Disabled'),
                        )
                    )
                )
            )
        );
        return $this;
    }

    /**
     * get the row url
     *
     * @access public
     * @param Iksula_Storemanager_Model_Store
     * @return string
     * @author Ultimate Module Creator
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    /**
     * get the grid url
     *
     * @access public
     * @return string
     * @author Ultimate Module Creator
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    /**
     * after collection load
     *
     * @access protected
     * @return Iksula_Storemanager_Block_Adminhtml_Store_Grid
     * @author Ultimate Module Creator
     */
    protected function _afterLoadCollection()
    {
        $this->getCollection()->walk('afterLoad');
        parent::_afterLoadCollection();
    }

    /**
     * filter store column
     *
     * @access protected
     * @param Iksula_Storemanager_Model_Resource_Store_Collection $collection
     * @param Mage_Adminhtml_Block_Widget_Grid_Column $column
     * @return Iksula_Storemanager_Block_Adminhtml_Store_Grid
     * @author Ultimate Module Creator
     */
    protected function _filterStoreCondition($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return;
        }
        $collection->addStoreFilter($value);
        return $this;
    }
}
