<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */


class Brainvire_Points_Model_Rule_Condition_Product_Found extends Brainvire_Points_Model_Rule_Condition_Product_Combine {

    public function __construct() {
        parent::__construct();
        $this->setType('points/rule_condition_product_found');
    }

    public function loadValueOptions() {
        $this->setValueOption(array(
            1 => 'FOUND',
            0 => 'NOT FOUND',
        ));
        return $this;
    }

    public function asHtml() {
        $html = $this->getTypeElement()->getHtml() .
                Mage::helper('points')->__('If an item is %s in the cart with %s of these conditions true:', $this->getValueElement()->getHtml(), $this->getAggregatorElement()->getHtml());
        if ($this->getId() != '1') {
            $html.= $this->getRemoveLinkHtml();
        }
        return $html;
    }

    
    public function validate(Varien_Object $object) {
        $all = $this->getAggregator() === 'all';
        $true = (bool) $this->getValue();
        $found = false;
        foreach ($object->getAllItems() as $item) {
            $found = $all ? true : false;
            foreach ($this->getConditions() as $cond) {
                $validated = $cond->validate($item);
                if ($all && !$validated) {
                    $found = false;
                    break;
                } elseif (!$all && $validated) {
                    $found = true;
                    break 2;
                }
            }
            if ($found && $true) {
                break;
            }
        }
        if ($found && $true) {
            

            return true;
        } elseif (!$found && !$true) {
           
            return true;
        }
        return false;
    }

}
