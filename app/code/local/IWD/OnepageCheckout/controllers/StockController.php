<?php
class IWD_OnepageCheckout_StockController extends Mage_Core_Controller_Front_Action
{

    /*
    * This controller gets product Id and returns its stock availability
    * and maximum qty available.
    */

    public function checkqtyAction(){

        $response = array();
        $productId = $this->getRequest()->getParam('productId');
        $requestedQty = $this->getRequest()->getParam('requestedQty');
        $model = Mage::getModel('catalog/product'); 
        $_product = $model->load($productId);
        if($_product && $_product->getName() && is_numeric($requestedQty)){
            $availableQty = (int)Mage::getModel('cataloginventory/stock_item')
                        ->loadByProduct($_product)->getQty();
            if($requestedQty > $availableQty){
                $response['status'] = 'success';
                $response['maxqty'] = $availableQty;
                $response['isavailable'] = 'No';
            }
            else{
                $response['status'] = 'success';
                $response['maxqty'] = $requestedQty;
                $response['isavailable'] = 'Yes';
            }
        }
        else{
            $response['status'] = 'failed';
        } 
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
        return;
    }
}