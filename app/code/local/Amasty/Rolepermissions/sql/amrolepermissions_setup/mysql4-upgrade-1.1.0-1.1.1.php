<?php
$installer = $this;

$installer->startSetup();

$installer->run("
ALTER TABLE amasty_amrolepermissions_rule
    ADD scope_hub varchar(255) ;
");

$installer->endSetup();
