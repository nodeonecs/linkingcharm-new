<?php
class Aster_Styler_IndexController extends Mage_Core_Controller_Front_Action{
    public function IndexAction() {
      
    $this->loadLayout();   
    $this->getLayout()->getBlock("head")->setTitle($this->__("Styler"));
          $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
      $breadcrumbs->addCrumb("home", array(
                "label" => $this->__("Home Page"),
                "title" => $this->__("Home Page"),
                "link"  => Mage::getBaseUrl()
       ));

      $breadcrumbs->addCrumb("styler", array(
                "label" => $this->__("Styler"),
                "title" => $this->__("Styler")
       ));

      $this->renderLayout(); 
    
    }

    public function get_itemsAction()
    {
      $charm_config = Mage::getStoreConfig('linkingcharm_config/styler_config/charm_list');
      $charm_list = explode(',',$charm_config);
      
      $chain_config = Mage::getStoreConfig('linkingcharm_config/styler_config/chain_list');
      $chain_list = explode(',',$chain_config);

      $allParams = Mage::app()->getRequest()->getParams();
      // print_r($allParams);exit;
      $collection = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect('*');
      $collection->addFieldToFilter('visibility', Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);
      $collection->setOrder('style_position','asc');

     if(isset($allParams['filter']['browse_by']) && !empty($allParams['filter']['browse_by'])){
        $collection->addFieldToFilter(array(array('attribute'=>'product_type','eq'=>$allParams['filter']['browse_by'])));
      }

      if(isset($allParams['filter']['type']) && !empty($allParams['filter']['type'])){
        $collection->addFieldToFilter(array(array('attribute'=>'product_theme','eq'=>$allParams['filter']['type'])));
      }

      if(isset($allParams['filter']['finish']) && !empty($allParams['filter']['finish'])){
        $collection->addFieldToFilter(array(array('attribute'=>'product_finish','eq'=>$allParams['filter']['finish'])));
      }

      if(isset($allParams['filter']['colour']) && !empty($allParams['filter']['colour'])){
        $collection->addFieldToFilter(array(array('attribute'=>'color','eq'=>$allParams['filter']['colour'])));
      }

      // print_r($allParams['filter']['price']);exit;
      if(isset($allParams['filter']['price']) && !empty($allParams['filter']['price'])){  
        // $price = explode('-',$allParams['filter']['price']);
        if($allParams['filter']['price']=='asc')
        {
           $collection->addAttributeToSort('price', 'asc');
        }

        if($allParams['filter']['price']=='desc')
        {
          $collection->addAttributeToSort('price', 'desc');
        }

        // echo $collection->getSelect();exit;

        // if(!empty($price[0]) && !empty($price[1])){
        //    $collection->addAttributeToFilter('price', array(array('gt' => $price[0],'lt' => $price[1])));
        // }

        // if(empty($price[1])){
        //    $collection->addAttributeToFilter('price', array(array('gt' => $price[0])));
        // }
     
      }


      $jsonData = array();
      $i = 0;
      foreach ($collection as $key => $product) {
        $_product = Mage::getModel('catalog/product')->load($product->getId());

        $jsonData['items'][$i]['product_id']  =  $product->getId();
        $filename = $default_filename = '';
        $jsonData['items'][$i]['style']  = $product->getName();
        foreach ($_product->getMediaGalleryImages() as $_image):
          if(strtolower($_image->getData('label_default'))=='config'){
            $filename = $_image->getUrl();
          }
          if(strtolower($_image->getData('label_default'))=='normal'){
            $default_filename = $_image->getUrl();
          }
        endforeach;
        
        // Big image which is used        
        $jsonData['items'][$i]['filename']  = $filename;
        // small thumbnail image
        $jsonData['items'][$i]['default_filename']  = $default_filename;
        $product_type = $product->getResource()->getAttribute('product_type')->getFrontend()->getValue($product);
        if(in_array(strtolower($product->getData('product_type')),$charm_list)){
          $jsonData['items'][$i]['x'] = $product->getData('x_cordinate');
          $jsonData['items'][$i]['y'] = $product->getData('y_cordinate');
          $jsonData['items'][$i]['full_width']  = $product->getData('full_width');
          $jsonData['items'][$i]['full_height']  = $product->getData('full_height');
          $jsonData['items'][$i]['chain_width']  = $product->getData('chain_width');
          $jsonData['items'][$i]['isStopper']  = $product->getData('is_stopper');
          $jsonData['items'][$i]['product_type']  = "Charm";
        }

        if(in_array(strtolower($product->getData('product_type')),$chain_list)){
          $control_point_array = $product->getData('control_point_array');
          $jsonData['items'][$i]['control_point_array'] = json_decode($control_point_array,true);
          $jsonData['items'][$i]['isBarrel']  = $product->getData('is_barrel');
          $jsonData['items'][$i]['product_type']  = "Chain";
          if($product->getTypeId()=='configurable')
          {
            $jsonData['items'][$i]['sizes'] = Mage::helper('styler')->getConfigOptionsByProduct($product);
          }
        }

        $jsonData['items'][$i]['actual_width']  = $product->getData('actual_width');

        // $jsonData['items'][$i]['product_type']  =  $product_type;
        $jsonData['items'][$i]['sku']  =  $product->getSku();
        $jsonData['items'][$i]['price']  =  $product->getPrice();
        $jsonData['items'][$i]['retailer_product_data']  = NULL;
        $jsonData['items'][$i]['original_price']  =  $product->getPrice();
        $jsonData['items'][$i]['product_sku']  =  $product->getSku();      
        $jsonData['items'][$i]['sylee_product_type']  =  $product->getTypeId();   

        $i++;
      }

        $this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
        $this->getResponse()->setBody(json_encode($jsonData));
    }

    public function get_profileAction()
    {
      $charm_config = Mage::getStoreConfig('linkingcharm_config/styler_config/charm_list');
      $charm_list = explode(',',$charm_config);

      $chain_config = Mage::getStoreConfig('linkingcharm_config/styler_config/chain_list');
      $chain_list = explode(',',$chain_config);

      $hash = Mage::app()->getRequest()->getParam('hash');
      if($hash){
        $hashIds = explode('/', $hash);
      }

      $profile_cookie = Mage::app()->getRequest()->getParam('profile');
      if($profile_cookie){
        $cookieData = Mage::getSingleton('core/cookie')->get($profile_cookie);
        $hashIds = explode('/', $cookieData);
      }

      $cookie = Mage::app()->getRequest()->getParam('cookie');
      if($cookie){
        $cookieData = Mage::getSingleton('core/cookie')->get($cookie);
        $cookieJson = json_decode($cookieData,true);
        // $hashIds = explode('/', $cookieData);
      }

      // print_r($hashIds);

      $counter = 1;
      $charmsProductId = array();

      $productIds[0]['id']   = array_shift($hashIds);
      foreach ($hashIds as $singleIds) {        
        if($counter%2==0){
          $arrayCounter = $counter;
          $charmsProductId[$counter]['id']   = $hashIds[$arrayCounter-2];
          $productIds[$counter]['id']   = $hashIds[$arrayCounter-2];
          $productIds[$counter]['left']   = $singleIds;
          $charmsProductId[$counter]['left'] = $singleIds;
        }
        $counter++;
      }

      // print_r($charmsProductId);
      
      $collection = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect('*');
      $collection->addFieldToFilter('visibility', Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);
      $collection->addFieldToFilter(array(array('attribute'=>'entity_id','in'=>$productIds)));
      // $collection->setOrder('style_position','asc');
      $jsonData = array();
      $i = 0;
      $j = 0;

      foreach ($productIds as $key => $productId) {
        
        $_product = Mage::getModel('catalog/product')->load($productId['id']);
        $product_type = $_product->getResource()->getAttribute('product_type')->getFrontend()->getValue($_product);
        $filename = $default_filename = '';
        foreach ($_product->getMediaGalleryImages() as $_image):
          if(strtolower($_image->getData('label_default'))=='config'){
            $filename = $_image->getUrl();
          }
          if(strtolower($_image->getData('label_default'))=='normal'){
            $default_filename = $_image->getUrl();
          }
        endforeach;

        if(in_array(strtolower($_product->getData('product_type')),$chain_list)){
          $jsonData['profile']['product_id']  =  $_product->getId();          
          $jsonData['profile']['style']  = $_product->getName();
          // Big image which is used        
          $jsonData['profile']['filename']  = $filename;
          // small thumbnail image
          $jsonData['profile']['default_filename']  = $default_filename;
          $control_point_array = $_product->getData('control_point_array');
          $jsonData['profile']['control_point_array'] = json_decode($control_point_array,true);
          $jsonData['profile']['isBarrel']  = $_product->getData('is_barrel');

          $jsonData['profile']['actual_width']  = $_product->getData('actual_width');

          // $jsonData['profile']['product_type']  =  $_product_type;
          $jsonData['profile']['product_type']  = "Chain";
          $jsonData['profile']['sku']  =  $_product->getSku();
          $jsonData['profile']['price']  =  $_product->getPrice();
          $jsonData['profile']['retailer_product_data']  = NULL;
          $jsonData['profile']['original_price']  =  $_product->getPrice();
          $jsonData['profile']['product_sku']  =  $_product->getSku();  
          $jsonData['profile']['sylee_product_type']  =  $_product->getTypeId();

          if($_product->getTypeId()=='configurable')
          {
            $jsonData['profile']['sizes'] = Mage::helper('styler')->getConfigOptionsByProduct($_product);
          }
          $i++;
        }

          if(in_array(strtolower($_product->getData('product_type')),$charm_list)){
            $jsonData['charms'][$j]['product_id']  =  $_product->getId();          
            $jsonData['charms'][$j]['style']  = $_product->getName();        
            $jsonData['charms'][$j]['left']  = $productId['left'];        

            // Big image which is used        
            $jsonData['charms'][$j]['filename']  = $filename;
            // small thumbnail image
            $jsonData['charms'][$j]['default_filename']  = $default_filename;

            $jsonData['charms'][$j]['x'] = $_product->getData('x_cordinate');
            $jsonData['charms'][$j]['y'] = $_product->getData('y_cordinate');
            $jsonData['charms'][$j]['full_width']  = $_product->getData('full_width');
            $jsonData['charms'][$j]['full_height']  = $_product->getData('full_height');
            $jsonData['charms'][$j]['chain_width']  = $_product->getData('chain_width');
            $jsonData['charms'][$j]['isStopper']  = $_product->getData('is_stopper');

            $jsonData['charms'][$j]['actual_width']  = $_product->getData('actual_width');

            $jsonData['charms'][$j]['product_type']  = "Charm";
            $jsonData['charms'][$j]['sku']  =  $_product->getSku();
            $jsonData['charms'][$j]['price']  =  $_product->getPrice();
            $jsonData['charms'][$j]['retailer_product_data']  = NULL;
            $jsonData['charms'][$j]['original_price']  =  $_product->getPrice();
            $jsonData['charms'][$j]['product_sku']  =  $_product->getSku();  
            $jsonData['charms'][$j]['sylee_product_type']  =  $_product->getTypeId();
            $j++;
          }
        }
        $this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
        $this->getResponse()->setBody(json_encode($jsonData));

    }


    public function ajax_add_to_cartAction()
    {
      $jsonData = array();
      $product_data[] = Mage::app()->getRequest()->getParam('data');
      // print_r($product_data);exit;
      $cart = Mage::getSingleton('checkout/cart');
      $response = array();
      try
      {
        if(count($product_data)>0)
        {
        foreach ($product_data as $product_id) 
        {
          $_product = Mage::getModel('catalog/product')->load($product_id);
          if($_product->getId())
          {
            if($_product->getTypeId()=='configurable')
            {
              
             // $configurableProduct = Mage::getModel('catalog/product')->loadByAttribute('sku','HC-32-BR');
             $buyRequest = Mage::helper('styler/data')->getConfigOptions($_product,$_product->getSku(),1);

            }
            else
            {
              $buyRequest = array('qty' => 1);
            }
            
            $cart->addProduct($_product, $buyRequest);
          }          
        }

        $quoteMessageQty = $cart->getQuote()->getMessages();

        if (!$cart->getQuote()->getHasError() || $quoteMessageQty['qty'])
        {
            $cart->save();
            $message = $this->__('%s was added to your shopping cart.', Mage::helper('core')->htmlEscape($_product->getName()));
            // Mage::getSingleton('checkout/session')->addSuccess($message);
            $cartItemsCount = Mage::helper('checkout/cart')->getSummaryCount();
            $response['count'] = $cartItemsCount;
            $response['status'] = 'SUCCESS';
            $response['message'] = $message;
        }
      }

      }catch (Mage_Core_Exception $e) {      
        $cartItemsCount = Mage::helper('checkout/cart')->getSummaryCount();
        $response['count'] = $cartItemsCount;
        $response['status'] = 'ERROR';
        $response['message'] = $e->getMessage();
        // Mage::getSingleton('checkout/session')->addError(Mage::helper('core')->escapeHtml($e->getMessage()));


      } catch (Exception $e) {
        $cartItemsCount = Mage::helper('checkout/cart')->getSummaryCount();
        $response['count'] = $cartItemsCount;
        $response['status'] = 'ERROR';
        $response['message'] = $this->__('Cannot add the item to shopping cart.');
        // Mage::getSingleton('checkout/session')->addError(Mage::helper('core')->escapeHtml($e->getMessage()));

        Mage::logException($e);
      }     

      $this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
      $this->getResponse()->setBody(json_encode($response));      
    }



    public function testAction()
    {
      echo "<pre>";
      $configurableProduct = Mage::getModel('catalog/product')->loadByAttribute('sku',$sku);

      $params = Mage::helper('styler/data')->getConfigOptions($configurableProduct,'HC-32-BR',1);
      
      $productCollection = Mage::getModel('catalog/product')->load($configurableProduct->getId());

      $cart->addProduct($productCollection, $params);
      // $cart->addProduct($_product, $params);
      $session = Mage::getSingleton('customer/session');
      $session->setCartWasUpdated(true);
      $cart->save();
    
    }


    public function ajax_stylerAction()
    {
      // simpleProduct
      
          $jsonData = array();
          $product_data= Mage::app()->getRequest()->getParam('product');
          
          foreach ($product_data as $product_id => $value) 
          {
            $cart = Mage::getModel('checkout/cart');
            $cartHelper = Mage::helper('checkout/cart');

            $items = $cartHelper->getCart()->getItems();
            foreach ($items as $item) {
              if ($item->getProduct()->getId() == $value['product_id']) {
                $itemId = $item->getItemId();
                $cartHelper->getCart()->removeItem($itemId)->save();
                break;
              }
            }
            $jsonData[] = Mage::helper('styler/data')->addToCartCustom($cart,$value);
            
          }      
      

      $this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
      $this->getResponse()->setBody(json_encode($jsonData));      
    }
}
