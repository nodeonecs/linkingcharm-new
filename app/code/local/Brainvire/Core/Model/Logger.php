<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   Brainvire
 * @package    Brainvire_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * @license    
 */


class Brainvire_Core_Model_Logger extends Mage_Core_Model_Abstract {
    
    const LOG_SEVERITY_NOTICE = 1;
    
    const LOG_SEVERITY_STRICT_NOTICE = 2;
    
    const LOG_SEVERITY_WARNING = 4;
    
    const LOG_SEVERITY_ERROR = 8;
   
    const LOG_SEVERITY_FATAL = 8;

    protected function _construct() {
        $this->_init('brainvirecore/logger');
    }

    
    public function _beforeSave() {
        if (!$this->getSeverity()) {
            $this->setSeverity(self::LOG_SEVERITY_NOTICE);
        }
        if (!$this->getDate()) {
            $this->setDate(now());
        }
        return parent::_beforeSave();
    }

    
    public function exorcise() {
        return Mage::helper('brainvirecore/logger')->exorcise();
    }

}