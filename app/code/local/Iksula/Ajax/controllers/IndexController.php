<?php
require_once 'Mage/Checkout/controllers/CartController.php';
class Iksula_Ajax_IndexController extends Mage_Checkout_CartController
{

	// public function indexAction(){
	// 	$this->loadLayout();
	// 	$this->renderLayout();
	// }

	public function addAction()
	{
		$cart   = $this->_getCart();
		$params = $this->getRequest()->getParams();
		
		if($params['isAjax'] == 1){

			$response = array();
			try {
				if (isset($params['qty'])) {
					$filter = new Zend_Filter_LocalizedToNormalized(
					array('locale' => Mage::app()->getLocale()->getLocaleCode())
					);
					$params['qty'] = $filter->filter($params['qty']);
				}

				$product = $this->_initProduct();
				$related = $this->getRequest()->getParam('related_product');

				/**
				 * Check product availability
				 */
				if (!$product) {
					$response['status'] = 'ERROR';
					$response['message'] = $this->__('Unable to find Product ID');
				}

				
				if($params['checkout_method'] == '') {
					$params['checkout_method'] = 'deliverable';
				}
				
				$itemCollection = Mage::getModel('sales/quote_item');
				$quote_id = $cart->getQuote()->getData('entity_id');
				$quoteItemCollection = $itemCollection->getCollection()->addFieldToFilter('quote_id',$quote_id)->addFieldToFilter('sku',$params['product_sku']);

				if($quoteItemCollection->getData()) {
					$response['status'] = 'ERROR';
					$response['message'] = $this->__('Product is already present in the shopping cart. Cannot Add product again');

				} else {
					$cart->addProduct($product, $params);
					if (!empty($related)) {
						$cart->addProductsByIds(explode(',', $related));
					}

					$cart->save();

					$items = $cart->getQuote()->getAllItems();
					foreach ($items as $value) {
						if ($value->getId() > $max) {
				            $max = $value->getId();
				            $lastItem = $value;
				        }
					}
					if ($lastItem){
							$store_viewid = Mage::getModel('core/store')->load($params['store_id'] , 'group_id')->getStoreId();
					        $lastItem->setCheckoutMethod($params['checkout_method']);
				            $lastItem->setStoreName($params['store_name']);
				            $lastItem->setStoreCode($params['store_code']);
				            $lastItem->setStorePickupid($params['store_id']);
				            $lastItem->setStorePickupviewid($store_viewid);
				            $lastItem->setStoreAddress($params['store_address']);
				            $lastItem->setStoreCity($params['store_cityname']);
				            $lastItem->setStorePincode($params['store_pincode']);
				            $lastItem->setStoreZone($params['store_zone']);
				            $lastItem->setStoreRegion($params['store_region']);
				            $lastItem->save();
				            

				            if($lastItem->getParentItemId()) {
				            	$parentId = $lastItem->getParentItemId();	
				            	$quoteItemCollection = $cart->getQuote()->getItemById($parentId);
					          
				            	if ($quoteItemCollection) {
					            	$quoteItemCollection->setCheckoutMethod($params['checkout_method']);
						            $quoteItemCollection->setStoreName($params['store_name']);
						            $quoteItemCollection->setStoreCode($params['store_code']);
						            $quoteItemCollection->setStorePickupid($params['store_id']);
						            $quoteItemCollection->setStorePickupviewid($store_viewid);
						            $quoteItemCollection->setStoreAddress($params['store_address']);
						            $quoteItemCollection->setStoreCity($params['store_cityname']);
						            $quoteItemCollection->setStorePincode($params['store_pincode']);
						            $quoteItemCollection->setStoreZone($params['store_zone']);
						            $quoteItemCollection->setStoreRegion($params['store_region']);
						            $quoteItemCollection->save();
						            $cart->save();
					            } else {
				            		// Mage::log($parentId,NULL,'quoteitem.log');
					            }
				            }
				    }
					$this->_getSession()->setCartWasUpdated(true);

					/**
					 * @todo remove wishlist observer processAddToCart
					 */
					Mage::dispatchEvent('checkout_cart_add_product_complete',
					array('product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse())
					);

					$quoteMessageQty = $cart->getQuote()->getMessages();

					if (!$cart->getQuote()->getHasError() || $quoteMessageQty['qty']){
						$message = $this->__('%s was added to your shopping cart.', Mage::helper('core')->htmlEscape($product->getName()));
						$response['status'] = 'SUCCESS';
						$response['message'] = $message;
						//New Code Here
						$this->loadLayout();
						$toplink = $this->getLayout()->getBlock('top.links')->toHtml();
						$sidebar_block = $this->getLayout()->getBlock('cart_sidebar');
						Mage::register('referrer_url', $this->_getRefererUrl());
						$sidebar = $sidebar_block->toHtml();
						$response['toplink'] = $toplink;
						$response['sidebar'] = $sidebar;
						$response['count'] = Mage::helper('checkout/cart')->getSummaryCount();
					}

					
				}
			} catch (Mage_Core_Exception $e) {
				$msg = "";
				if ($this->_getSession()->getUseNotice(true)) {
					$msg = $e->getMessage();
				} else {
					$messages = array_unique(explode("\n", $e->getMessage()));
					foreach ($messages as $message) {
						$msg .= $message.'<br/>';
					}
				}

				$response['status'] = 'ERROR';
				$response['message'] = $msg;
			} catch (Exception $e) {
				$response['status'] = 'ERROR';
				$response['message'] = $this->__('Cannot add the item to shopping cart.');
				Mage::logException($e);
			}

			$this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
			return;
		}else{
			return parent::addAction();
		}
	}

	/*public function deleteAction()
	{

		$id = (int) $this->getRequest()->getParam('id');

		if($this->getRequest()->isXmlHttpRequest()) {
			$id = (int) $this->getRequest()->getParam('id');
			if ($id) {
				try {
					$this->_getCart()->removeItem($id)->save();
			  		$this->_getSession()->setCartWasUpdated(true);
			  		$cart = $this->_getCart();
			  		$quoteMessageQty = $cart->getQuote()->getMessages();

	       			 if (!$cart->getQuote()->getHasError() || $quoteMessageQty['qty']){
							$response = $this->responseBody();
			  		}
				} catch (Exception $e) {
					$response['status'] = 'ERROR';
					$response['message'] = $this->__('Cannot be deleted from shopping cart.');
					Mage::logException($e);
				}
			}else{
				$response['status'] = 'ERROR';
				$response['message'] = $this->__('Unable to find Product in cart');
			}
			Mage::getSingleton('core/session')->addSuccess('Product deleted from shopping cart.');
			return $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
		} else {
			return parent::deleteAction();
		}
	}*/


	

    protected function responseBody()
	{
	//	$response = array();
		$cart = $this->_getCart();
		$response['status'] = 'SUCCESS';
		//New Code Here
		$this->loadLayout();
		$toplink = $this->getLayout()->getBlock('top.links')->toHtml();
		$sidebar_block = $this->getLayout()->getBlock('cart_sidebar');
		Mage::register('referrer_url', $this->_getRefererUrl());
		$sidebar = $sidebar_block->toHtml();
		$response['toplink'] = $toplink;
		$response['sidebar'] = $sidebar;

		$block = $this->getLayout()->createBlock('onepagecheckout/onepage_link')->setTemplate('onepagecheckout/onepage/link.phtml');
		$response['orderlink'] = $block->toHtml();

		$quoteData = $cart->getQuote()->getData();

		//print_r($quoteData);

		if($quoteData['items_count']==0 && $quoteData['items_qty']==0)
		{
			$block = $this->getLayout()->createBlock('checkout/cart')->setTemplate('checkout/cart/noItems.phtml');
			$response['noitemsblock'] = $block->toHtml();
		}

		$cartTotals = $this->getLayout()->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/totals.phtml')->toHtml();
		$response['carttotals'] = $cartTotals;

		$cartBlock = $this->getLayout()->createBlock('checkout/cart')->setCartTemplate('checkout/cart.phtml');
		$cartBlock->addItemRender('simple', 'checkout/cart_item_renderer', 'checkout/cart/item/default.phtml');
		$cartBlock->addItemRender('grouped', 'checkout/cart_item_renderer_grouped', 'checkout/cart/item/default.phtml');
		$cartBlock->addItemRender('configurable', 'checkout/cart_item_renderer_configurable', 'checkout/cart/item/default.phtml');

		$itemHtml = '';
		foreach($cartBlock->getItems() as $_item) {
			$itemHtml .= $cartBlock->getItemHtml($_item);
		}
		$response['cartitems'] = $itemHtml;

		return $response;
	}
	// created by Deepthi
	public function addToCartFromCatalogAction()
    {
    	$response = array();
    	$response['status'] = 'FAIL';
		if ($this->getRequest()->isXmlHttpRequest()) {

			$params = $this->getRequest()->getParams();
			
			if( !empty($params) && count($params) ) {
				
				if($params['checkoutmethod'] == 'pickable') {
					$id = Mage::getModel('catalog/product')->getResource()->getIdBySku($params['product_id']);
					$product = Mage::getModel('catalog/product')->load($id);
				} else {
					$product = Mage::getModel('catalog/product')->load($params['product_id']);
				}

				$options = array();
				$options['qty'] = 1;
				if($params['type'] == "configurable") {
					$catalogProduct = Mage::getModel('catalog/product');
			        $sizeAttribute = $catalogProduct->getResource()->getAttribute("size");
			        $sizeAttributeId = 0;
			        $sizeAttributeId = $sizeAttribute->getAttributeId();
			        $options['product'] = $params['product_id'];
			        $options['super_attribute'] = array($sizeAttributeId=>$params['attribute_id']);
		    	}
				
				$cart = Mage::getModel("checkout/cart");
				
				try {
					if($params['checkoutmethod'] == 'pickable') {
						$ids = Mage::getModel('catalog/product_type_configurable')->getChildrenIds($product->getId());
					} else {
						$ids = Mage::getModel('catalog/product_type_configurable')->getChildrenIds($params['product_id']);
					}

					if($params['type'] == 'configurable') {
				        $_subproducts = Mage::getModel('catalog/product')->getCollection()
				                        ->addAttributeToFilter('entity_id', $ids)
				                        ->addAttributeToFilter('size' , array('eq' => $params['attribute_id']));
						
				        foreach ($_subproducts as $value) {
				            $simple_productSku = $value->getData('sku');
				        }
					}else {
						$simple_productSku = $params['product_id'];
					}

					$itemCollection = Mage::getModel('sales/quote_item');
					$quote_id = $cart->getQuote()->getData('entity_id');
					$quoteItemCollection = $itemCollection->getCollection()->addFieldToFilter('quote_id',$quote_id)->addFieldToFilter('sku',$simple_productSku);

					if(!$quoteItemCollection->getData()) {
				        
						$cart->addProduct($product, $options);

						$cart->save();
						$items = $cart->getQuote()->getAllItems();
						foreach ($items as $value) {
							if ($value->getId() > $max) {
					            $max = $value->getId();
					            $lastItem = $value;
					        }
						}
						
						if ($lastItem){
					        
					        if($params['checkoutmethod'] == 'pickable') {
								$data = (array)json_decode($params['store_data']);
								
					        	$store_viewid = Mage::getModel('core/store')->load($data['group_id'] , 'group_id')->getStoreId();
					        	$lastItem->setCheckoutMethod($params['checkoutmethod']);
					            $lastItem->setStoreName($data['name']);
					            $lastItem->setStoreCode($data['store_code']);
					            $lastItem->setStorePickupid($data['group_id']);
					            $lastItem->setStorePickupviewid($store_viewid);
					            $lastItem->setStoreAddress($data['store_address']);
					            $lastItem->setStoreCity($data['cityname']);
					            $lastItem->setStorePincode($data['store_pincode']);
					            $lastItem->setStoreZone($data['store_zone']);
					            $lastItem->setStoreRegion($data['region']);
				            	$lastItem->save();
				            	

					            if($lastItem->getParentItemId()) {
					            	$parentId = $lastItem->getParentItemId();

					            	$quoteItemCollection = $cart->getQuote()->getItemById($parentId);
					            	if ($quoteItemCollection) {
						            	$store_viewid = Mage::getModel('core/store')->load($data['group_id'] , 'group_id')->getStoreId();
						            	$quoteItemCollection->setCheckoutMethod($params['checkoutmethod']);
							            $quoteItemCollection->setStoreName($data['name']);
							            $quoteItemCollection->setStoreCode($data['store_code']);
							            $quoteItemCollection->setStorePickupid($data['group_id']);
							            $quoteItemCollection->setStorePickupviewid($store_viewid);
							            $quoteItemCollection->setStoreAddress($data['store_address']);
							            $quoteItemCollection->setStoreCity($data['cityname']);
							            $quoteItemCollection->setStorePincode($data['store_pincode']);
							            $quoteItemCollection->setStoreZone($data['store_zone']);
							            $quoteItemCollection->setStoreRegion($data['region']);
						            	$quoteItemCollection->save();
						            	$cart->save();
						            } else {
					            		// Mage::log($parentId,NULL,'quoteitem.log');
						            }
					            }
					        }
					    }
					    
						Mage::getSingleton('checkout/session')->setCartWasUpdated(true);
						$response['status'] = 'SUCCESS';
						$response['message'] = 'Product successfully added to cart';
						$response['count'] = (int)Mage::getModel('checkout/cart')->getQuote()->getItemsQty();
					
					} else {
						$response['status'] = 'FAIL';
						$response['message'] = $this->__('Product is already present in the shopping cart. Cannot Add product again');
					}

				} catch(Exception $e) {
					$response['status'] = 'FAIL';
					$response['message'] = $e->getMessage();
				}
			}
		}
		$this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
		return;
    }

    public function couponPostAction()
    {

        /**
         * No reason continue with empty shopping cart
         */
        if(!isset($_POST['ajax'])) 
        { 
            parent::couponPostAction(); 
            return; 
        } 

        $response = array();

        if (!$this->_getCart()->getQuote()->getItemsCount()) {
            $this->_goBack();
            return;
        }

        $couponCode = (string) $this->getRequest()->getParam('coupon_code');
        if ($this->getRequest()->getParam('remove') == 1) {
            $couponCode = '';
        }
        $oldCouponCode = $this->_getQuote()->getCouponCode();

        if (!strlen($couponCode) && !strlen($oldCouponCode)) {
            $this->_goBack();
            return;
        }

        try {
            $codeLength = strlen($couponCode);
            $isCodeLengthValid = $codeLength && $codeLength <= Mage_Checkout_Helper_Cart::COUPON_CODE_MAX_LENGTH;

            $this->_getQuote()->getShippingAddress()->setCollectShippingRates(true);
            $this->_getQuote()->setCouponCode($isCodeLengthValid ? $couponCode : '')
            ->collectTotals()
            ->save();

            if ($codeLength) {
                if ($isCodeLengthValid && $couponCode == $this->_getQuote()->getCouponCode()) {
                    /*Mage::getSingleton('core/session')->addSuccess(
                        $this->__('Coupon code "%s" was applied.', Mage::helper('core')->escapeHtml($couponCode))
                        );*/
                $response['status'] = 'success';
                $response['txt'] = $this->__('Coupon code "%s" was applied.', Mage::helper('core')->escapeHtml($couponCode));
                } else {
                    /*Mage::getSingleton('core/session')->addError(
                        $this->__('Coupon code "%s" is not valid.', Mage::helper('core')->escapeHtml($couponCode))
                        );*/
            $response['status'] = 'error';
                $response['txt'] = $this->__('Coupon code "%s" is not valid.', Mage::helper('core')->htmlEscape($couponCode));
                }
            } else {
                /*Mage::getSingleton('core/session')->addSuccess($this->__('Coupon code was canceled.'));*/
                $response['status'] = 'success';
                $response['txt'] = $this->__('Coupon code was canceled.');
            }

        } catch (Mage_Core_Exception $e) {
            $response['status'] = 'error';
            $response['txt'] = $e->getMessage();
        } catch (Exception $e) {
            $response['status'] = 'error';
            $response['txt'] = $this->__('Cannot apply the coupon code.');
            Mage::logException($e);
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
        return;
    
    }
}

