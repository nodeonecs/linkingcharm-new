jQuery(document).ready(function($){

    $(".configurable_avail_sizes span").live("click",function() {
        $(".add-to-cart-loader-new").show();
        var attribute_id    =   $(this).attr("id");
        var thissku    =   $(this).attr("sku");
        var checkoutmethod    =   $(this).attr("checkoutmethod");

        attribute_id = attribute_id.replace("selected_size_","");

        var product_id      =   $(this).parent().find(".selected_product_id").text();
        $.ajax ({
          type: "POST",
          url: baseurl +""+'ajax/index/addToCartFromCatalog',
          data:"product_id="+product_id+"&checkoutmethod="+checkoutmethod+"&attribute_id="+attribute_id+"&type=configurable",
          success: function(response)
          {
                //updateHeaderCartPopupAfterProductIsAdded(thissku, 'cart');
                var result = $.parseJSON(response);
                var itemText = '';
                if( result['count'] <= 1) {
                    itemText = " <span class='suffix'> item</span>";
                } else {
                 itemText = " <span class='suffix'> items</span>";
             }
             jQuery('.cart_link_wrapper #headercartcount').html(result['count']);
             if( result['status'] == 'SUCCESS' ) {
                updateHeaderCartPopupAfterProductIsAdded(thissku, 'cart');
                    // $(".add-to-cart-message").text("Product added to cart successfuly");
                    $('.messages-custom li span').html(result['message']);
                    $('.messages-custom').show();
                    $('html, body').animate({
                        scrollTop: $('.header-cart-link').offset().top - 50
                    }, 500);
                    $(".add-to-cart-loader-new").hide();
                } else {
                    $('.messages-custom li span').first().html(result['message']);
                    $('.messages-custom').first().show();
                    $('html, body').animate({
                        scrollTop: $('.header-cart-link').offset().top - 50
                    }, 500);
                    $(".add-to-cart-loader-new").hide();
                }
                // var cartCount = jQuery('.cart_link_wrapper').find('span').html();
                var cartCount = result['count'];
                if( $(".select-config-buy-now").length >= 1 && cartCount > 0 ) {
                    window.location.href = $(".config-buy-now-checkout").attr("href");
                }
            }
        });
    });

    $(".simple-product-add-cart").live("click",function() {
        // window.location.href = $(this).attr('url');
        // return false;
        $(".add-to-cart-loader-new").show();
        var simpleSku = $(this).attr('sku');
        $(".simple-buy-now").removeClass('select-simple-buy-now');
        var product_id = $(this).attr('productid');
        $.ajax ({
            type: "POST",
            url: baseurl +""+'ajax/index/addToCartFromCatalog',
            data:"product_id="+product_id+"&type=simple",
            success: function(response)
            {
                $(".add-to-cart-loader-new").hide();
                var result = $.parseJSON(response);
                var itemText = '';
                if( result['count'] <= 1) {
                    itemText = " <span class='suffix'> item</span>";
                } else {
                    itemText = " <span class='suffix'> items</span>";
                }
                updateHeaderCartPopupAfterProductIsAdded(simpleSku, 'cart');
                jQuery('.cart_link_wrapper #headercartcount').html(result['count']);
                if( result['status'] == 'SUCCESS' ) {
                    // $(".add-to-cart-message").text("Product added to cart successfuly");
                    $('.messages-custom li span').first().html(result['message']);
                    $('.messages-custom').first().show();
                    $('html, body').animate({
                        scrollTop: $('.header-cart-link').offset().top - 50
                    }, 500);
                } else {
                    $('.messages-custom li span').html(result['message']);
                    $('.messages-custom').show();
                    $('html, body').animate({
                        scrollTop: $('.header-cart-link').offset().top - 50
                    }, 500);
                }

            }
        });
    });

});