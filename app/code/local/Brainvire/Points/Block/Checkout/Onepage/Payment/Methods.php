<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   Brainvire
 * @package    Brainvire_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * @license    
 */


class Brainvire_Points_Block_Checkout_Onepage_Payment_Methods extends Mage_Checkout_Block_Onepage_Payment_Methods {

    protected $_summaryForCustomer;

    protected function _toHtml() {
        $magentoVersionTag = Brainvire_Points_Helper_Data::MAGENTO_VERSION_14;
        if (Mage::helper('points')->magentoLess14())
            $magentoVersionTag = Brainvire_Points_Helper_Data::MAGENTO_VERSION_13;
        $this->setTemplate('brainvire_points/checkout/onepage/payment/' . $magentoVersionTag . '/methods.phtml');

        return parent::_toHtml();
    }

    public function getSummaryForCustomer() {
        if (!$this->_summaryForCustomer) {
            $this->_summaryForCustomer = Mage::getModel('points/summary')->loadByCustomer(Mage::getSingleton('customer/session')->getCustomer());
        }
        return $this->_summaryForCustomer;
    }

    public function getMoneyForPoints() {
        if (!$this->getData('money_for_points')) {
            try {
                $moneyForPoints = Mage::getModel('points/rate')
                        ->loadByDirection(Brainvire_Points_Model_Rate::POINTS_TO_CURRENCY)
                        ->exchange($this->getSummaryForCustomer()->getPoints());
                $this->setData('money_for_points', Mage::app()->getStore()->convertPrice($moneyForPoints, true));
            } catch (Exception $ex) {
                
            }
        }
        return $this->getData('money_for_points');
    }

    public function getNeededPoints() {
        return Mage::helper('points')->getNeededPoints($this->getQuote()->getData('base_subtotal_with_discount'));
    }

    public function getLimitedPoints() {

        $sum = $this->getQuote()->getData('base_subtotal_with_discount');

        return Mage::helper('points')->getLimitedPoints($sum);
    }

    public function getBaseGrandTotalInPoints() {
        return Mage::helper('points')->getNeededPoints($this->getQuote()->getBaseGrandTotal());
    }

    public function pointsSectionAvailable() {
        return
                $this->getSummaryForCustomer()->getPoints()
                && $this->getMoneyForPoints()
                && Mage::helper('points')->isAvailableToRedeem($this->getSummaryForCustomer()->getPoints())
                && $this->customerIsRegistered();
    }

    protected function customerIsRegistered() {
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        return $customer->getId() > 0;
    }

}
