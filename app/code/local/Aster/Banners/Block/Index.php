<?php   
class Aster_Banners_Block_Index extends Mage_Core_Block_Template{   

	public function getSliders()
	{
		$_response = array();

		$_bannerCollection = Mage::getModel('banners/banners')->getCollection();
		$_bannerCollection->addFieldToFilter('banner_status',1);
		$_bannerCollection->addFieldToFilter('banner_type',0);
		$_bannerCollection->setOrder('sort_order', 'asc');

		$mediaurl=Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
		$counter = 0;
		foreach ($_bannerCollection as $banner) {
			$_response[$counter]['banner_link'] = $banner->banner_link;
			$_response[$counter]['banner_image'] = $mediaurl.''.$banner->banner_image;
			$_response[$counter]['sort_order'] = $banner->sort_order;
			$_response[$counter]['banner_title'] = $banner->banner_title;
			$counter++;
		}

		return $_response;

	}

	public function getMobileSliders()
	{
		$_response = array();

		$_bannerCollection = Mage::getModel('banners/banners')->getCollection();
		$_bannerCollection->addFieldToFilter('banner_status',1);
		$_bannerCollection->addFieldToFilter('banner_type',1);
		$_bannerCollection->setOrder('sort_order', 'asc');

		$mediaurl=Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
		$counter = 0;
		foreach ($_bannerCollection as $banner) {
			$_response[$counter]['banner_link'] = $banner->banner_link;
			$_response[$counter]['banner_image'] = $mediaurl.''.$banner->banner_image;
			$_response[$counter]['sort_order'] = $banner->sort_order;
			$_response[$counter]['banner_title'] = $banner->banner_title;
			$counter++;
		}
		
		return $_response;

		
	}

}