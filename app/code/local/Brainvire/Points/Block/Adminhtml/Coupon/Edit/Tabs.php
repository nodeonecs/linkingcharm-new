<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   BV
 * @package    BV_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */


class Brainvire_Points_Block_Adminhtml_Coupon_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {

    public function __construct() {
        parent::__construct();
        $this->setId('coupon_id');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('points')->__('Reward Coupon'));
    }

    protected function _beforeToHtml() {
        $helper = Mage::helper('points');

        $this->addTab('main_section', array(
            'label' => $helper->__('Coupon Information'),
            'title' => $helper->__('Coupon Information'),
            'content' => $this->getLayout()->createBlock('points/adminhtml_coupon_edit_tab_main')->toHtml(),
            'active' => true
        ));

        
        if ($this->getRequest()->getParam('id')) {
            $this->addTab('form_section1', array(
                'label' => $helper->__('Transactions'),
                'title' => $helper->__('Transactions'),
                'url' => $this->getUrl('*/*/transactions', array('_current' => true)),
                'class' => 'ajax',
            ));
        }
        return parent::_beforeToHtml();
    }

}