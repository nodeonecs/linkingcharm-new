<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_Model_Resource_Giftcard_Invoice_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('aw_giftcard2/giftcard_invoice');
    }

    public function setFilterByOrderId($orderId)
    {
        $this->getSelect()->where('main_table.order_id = ?', $orderId);
        return $this;
    }

    public function setFilterByGiftCardId($giftCardId)
    {
        $this->getSelect()->where('main_table.giftcard_id = ?', $giftCardId);
        return $this;
    }

    public function setFilterByInvoiceId($invoiceId)
    {
        $this->getSelect()->where('main_table.invoice_id = ?', $invoiceId);
        return $this;
    }

    public function addGiftCardsData()
    {
        $this->getSelect()
            ->joinLeft(
                array(
                    'giftcard' => $this->getTable('aw_giftcard2/giftcard')
                ),
                'main_table.giftcard_id = giftcard.giftcard_id',
                array(
                    'code'         => 'giftcard.code',
                    'balance' => 'giftcard.balance'
                )
            )
        ;
        return $this;
    }

    public function addSumAmountsPerGiftCard()
    {
        $this->getSelect()
            ->reset(Zend_Db_Select::COLUMNS)
            ->columns(
                array(
                    'giftcard_id' => "giftcard_id",
                    'base_giftcard_amount' => "SUM(base_giftcard_amount)",
                    'giftcard_amount' => "SUM(giftcard_amount)",
                )
            )
            ->group("giftcard_id")
        ;
        return $this;
    }
}