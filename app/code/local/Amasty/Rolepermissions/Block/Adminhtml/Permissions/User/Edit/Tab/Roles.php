<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2013 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Amasty_Rolepermissions_Block_Adminhtml_Permissions_User_Edit_Tab_Roles extends Mage_Adminhtml_Block_Permissions_User_Edit_Tab_Roles
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('permissionsUserRolesGrid');
        $this->setDefaultSort('sort_order');
        $this->setDefaultDir('asc');
        //$this->setDefaultFilter(array('assigned_user_role'=>1));
        $this->setTitle(Mage::helper('adminhtml')->__('User Roles Information'));
        $this->setUseAjax(true);
    }

    protected function _addColumnFilterToCollection($column)
    {
        if ($column->getId() == 'assigned_user_role') {
            $userRoles = $this->_getSelectedRoles();
            if (empty($userRoles)) {
                $userRoles = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('role_id', array('in'=>$userRoles));
            }
            else {
                if($userRoles) {
                    $this->getCollection()->addFieldToFilter('role_id', array('nin'=>$userRoles));
                }
            }
        }
        else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }


    public function showRolepermission(){
        //echo 'I am done';

        $admin_user_session = Mage::getSingleton('admin/session');
        $adminuserId = $admin_user_session->getUser()->getUserId();
        $currentroleid = Mage::getModel('admin/user')->load($adminuserId)->getRole()->getId();


        $AdvancedCustomRole = Mage::getModel('amrolepermissions/rule')->load($currentroleid , 'role_id')->getCustomRole();



        return array($AdvancedCustomRole , $currentroleid) ;

    }

    protected function _prepareCollection()
    {


        list($role_custom , $role_id ) = $this->showRolepermission();
        if($role_custom){
             if($role_custom == 'W_HO'){

            $Ho_websiteId = Mage::getModel('amrolepermissions/rule')->load($role_id , 'role_id')->getScopeWebsites();

            $storesViewsCollections = Mage::getModel('amrolepermissions/rule')->getCollection()->addFieldToFilter('custom_role' , array('eq' => 'STORE_VIEW'))->addFieldToSelect(array('scope_storeviews' , 'role_id'));
            $aStoreRoleId = array();
            foreach($storesViewsCollections as $stores){

                $aStoresViews = explode(',' , $stores->getScopeStoreviews());

                $iStoreViewsWebsiteID = Mage::getModel('core/store')->load($aStoresViews[0])->getWebsiteId();

                if($iStoreViewsWebsiteID == $Ho_websiteId){
                    $aStoreRoleId [] = $stores->getRoleId();
                }

            }

            $CustomRoleId = Mage::getModel('amrolepermissions/rule')->getCollection()->addFieldToFilter('custom_role' , array('nin' => array('S_A' , 'W_HO')))->addFieldToFilter('role_id' , array('in' => $aStoreRoleId))->addFieldToSelect('role_id');

            foreach($CustomRoleId as $roleid){

                $aRoleId [] = $roleid->getRoleId();
            }

             $collection = Mage::getResourceModel('admin/role_collection')->addFieldToFilter('role_id' , array('in' => $aRoleId));

        }else{

             $collection = Mage::getResourceModel('admin/role_collection');

        }

        }else{
             $collection = Mage::getResourceModel('admin/role_collection');
        }



        $collection->setRolesFilter();
        $this->setCollection($collection);
        //return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {

        $this->addColumn('assigned_user_role', array(
            'header_css_class' => 'a-center',
            'header'    => Mage::helper('adminhtml')->__('Assigned'),
            'type'      => 'radio',
            'html_name' => 'roles[]',
            'values'    => $this->_getSelectedRoles(),
            'align'     => 'center',
            'index'     => 'role_id'
        ));

        /*$this->addColumn('role_id', array(
            'header'    =>Mage::helper('adminhtml')->__('Role ID'),
            'index'     =>'role_id',
            'align'     => 'right',
            'width'    => '50px'
        ));*/

        $this->addColumn('role_name', array(
            'header'    =>Mage::helper('adminhtml')->__('Role Name'),
            'index'     =>'role_name'
        ));

        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/rolesGrid', array('user_id' => Mage::registry('permissions_user')->getUserId()));
    }

    protected function _getSelectedRoles($json=false)
    {
        if ( $this->getRequest()->getParam('user_roles') != "" ) {
            return $this->getRequest()->getParam('user_roles');
        }
        /* @var $user Mage_Admin_Model_User */
        $user = Mage::registry('permissions_user');
        //checking if we have this data and we
        //don't need load it through resource model
        if ($user->hasData('roles')) {
            $uRoles = $user->getData('roles');
        } else {
            $uRoles = $user->getRoles();
        }

        if ($json) {
            $jsonRoles = Array();
            foreach($uRoles as $urid) $jsonRoles[$urid] = 0;
            return Mage::helper('core')->jsonEncode((object)$jsonRoles);
        } else {
            return $uRoles;
        }
    }

}
