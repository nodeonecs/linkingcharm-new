<?php
class Aster_Purchase_Adminhtml_ReportController extends Mage_Adminhtml_Controller_Action
{

	protected function _isAllowed()
	{
		return true;
	}

	public function indexAction()
    {
       $this->loadLayout();
	   $this->_title($this->__("Report"));
	   $this->renderLayout();
    }

    public function processAction()
	{
		$reportType = $this->getRequest()->getPost('report_type');
		$params = $this->getRequest()->getParams();
		$file = $reportType.'.csv';

		// write logic for the report generation
		if(empty($reportType) || (isset($reportType) && empty($reportType))) {
			Mage::getSingleton('adminhtml/session')->addError("Request Report not found");
			$this->_redirectReferer();
			return;
		}
		if($reportType=='gp_report'){
			$file = Mage::getModel('slandsbek_simpleorderexport/export_gpreport')->exportOrders($params);
		}

		if($reportType=='purchase_sales_report'){
			$file = Mage::getModel('slandsbek_simpleorderexport/export_purchasereport')->exportOrders($params);
		}

		if(empty($file))
		{
			Mage::getSingleton('adminhtml/session')->addError("No Report Found");
			$this->_redirectReferer();
			return;

		}
		$this->_prepareDownloadResponse($file, file_get_contents(Mage::getBaseDir('export').DS.$file));
   	}


}
