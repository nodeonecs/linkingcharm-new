<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_Block_Adminhtml_Widget_Grid_Column_Renderer_Recipient extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        try {
            $columnValue =  $row->getData($this->getColumn()->getIndex());
            $index = ($this->getColumn()->getIndex() == 'recipient_name')
                ? 'recipient_email'
                : 'sender_email';
            $customerEmail = $row->getData($index);
            $websuteId = $row->getData('website_id');
            $customer = Mage::getModel("customer/customer");
            $customer->setWebsiteId($websuteId);
            $customer->loadByEmail($customerEmail);
            if ($customer->getId()) {
                $customerLink = Mage::helper('adminhtml')->getUrl("adminhtml/customer/edit", array('id' => $customer->getId()));
                return  '<a href="' . $customerLink . '">' . $columnValue . '</a>';
            }
            return $columnValue;
        } catch (Exception $ex) {

        }
        return '';
    }

    public function renderExport(Varien_Object $row)
    {
        try {
            $index = ($this->getColumn()->getIndex() == 'recipient_name')
                ? 'recipient_email'
                : 'sender_email';
            $columnValue = $row->getData($this->getColumn()->getIndex()) . '|' . $row->getData($index);
        } catch (Exception $ex) {
            $columnValue = '';
        }
        return $columnValue;
    }
}