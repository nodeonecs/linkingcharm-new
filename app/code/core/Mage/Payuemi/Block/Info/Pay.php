<?php
class Mage_Payuemi_Block_Info_Pay extends Mage_Payment_Block_Info
{
    protected function _prepareSpecificInformation($transport = null)
    {
        if (null !== $this->_paymentSpecificInformation) {
            return $this->_paymentSpecificInformation;
        }
        $info = $this->getInfo();
        $transport = new Varien_Object();
        $transport = parent::_prepareSpecificInformation($transport);
        if($info->getBankName()&&$info->getEmiCode())
            $transport->addData(array(
                Mage::helper('payment')->__('Bank Name') => $info->getBankName(),
                Mage::helper('payment')->__('Emi Option') => $info->getEmiCode()
            ));
        return $transport;
    }
}

