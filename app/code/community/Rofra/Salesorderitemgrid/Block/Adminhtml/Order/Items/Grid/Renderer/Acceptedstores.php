<?php


class Rofra_Salesorderitemgrid_Block_Adminhtml_Order_Items_Grid_Renderer_Acceptedstores
extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract{


public function render(Varien_Object $row)
    {
        $rowItemId = $row->getId();

        if($rowItemId){
            $oSalesOrderItems = Mage::getModel('sales/order_item');
            $aOrderStatus = $oSalesOrderItems->load($rowItemId)->getItemStatus();
            $store_id = $oSalesOrderItems->load($rowItemId)->getStoreCode();


            $iLoginuserid = Mage::getSingleton('admin/session')->getUser()->getUserId();

            $LoginUserRole = Mage::getModel('admin/user')->load($iLoginuserid)->getRole()->getRoleId();

            $iRole_Collectionsid = Mage::getSingleton('amrolepermissions/rule')
            ->getCollection()->addFieldToFilter('role_id' , array('eq' => $LoginUserRole));



             foreach($iRole_Collectionsid as $Role)
             {

                    $RoleCustomRole = $Role->getCustomRole();
                    $storeviewid = $Role->getScopeStoreviews();
             }
            /****** If Autoallocation is active shows the 5 Stores which is allocated for the Order items **************/

            //$isAutoallocateActive = Mage::getStoreConfig('omniapis_section/custom_settings/allocations_of_orders'); // Check the configuration if its active or not.

            //if($isAutoallocateActive == '1'){
                $store_id_1 = $oSalesOrderItems->load($rowItemId)->getData('store_code_1'); // Store 1
                $store_id_2 = $oSalesOrderItems->load($rowItemId)->getData('store_code_2'); // Store 2
                $store_id_3 = $oSalesOrderItems->load($rowItemId)->getData('store_code_3'); // Store 3
                $store_id_4 = $oSalesOrderItems->load($rowItemId)->getData('store_code_4'); // Store 4
                $store_id_5 = $oSalesOrderItems->load($rowItemId)->getData('store_code_5'); // Store 5
                $storeviewidmodel = Mage::getModel('core/store');  // Store Id Model
                $storeidmodel = Mage::getModel('core/store_group'); // Store View Id Model
                $store_1_name = "";
                $store_2_name = "";
                $store_3_name = "";
                $store_4_name = "";
                $store_5_name = "";

                if($store_id_1 != 'NULL' && $store_id_1 !== NULL){
                    //$store_id_1 = $storeviewidmodel->load($store_id_1)->getGroupId();
                    $store_1_name = $storeidmodel->load($store_id_1)->getName();
                }

                if($store_id_2 != 'NULL' && $store_id_2 !== NULL){
                    //$store_id_2 = $storeviewidmodel->load($store_viewid_2)->getGroupId();
                    $store_2_name = $storeidmodel->load($store_id_2)->getName();
                }

                if($store_id_3 != 'NULL' && $store_id_3 !== NULL){
                    //$store_id_3 = $storeviewidmodel->load($store_viewid_3)->getGroupId();
                    $store_3_name = $storeidmodel->load($store_id_3)->getName();
                }

                if($store_id_4 != 'NULL' && $store_id_4 !== NULL){
                    //$store_id_4 = $storeviewidmodel->load($store_viewid_4)->getGroupId();
                    $store_4_name = $storeidmodel->load($store_id_4)->getName();
                }

                if($store_id_5 != 'NULL' && $store_id_5 !== NULL){
                    //$store_id_5 = $storeviewidmodel->load($store_viewid_5)->getGroupId();
                    $store_5_name = $storeidmodel->load($store_id_5)->getName();
                }

                $sStoreName = $store_1_name."|".$store_2_name . "|".$store_3_name . "|". $store_4_name . "|".$store_5_name;

                if($RoleCustomRole == 'STORE_VIEW'){ // Condition for Store
                    $store_id = $storeviewidmodel->load($storeviewid)->getGroupId();
                    $store_name = $storeidmodel->load($store_id)->getName();
                    $sStoreName = $store_name;
                }
           /* }else{

                $sStoreName = Mage::getModel('core/store_group')->load($store_id)->getName();  // Get the Store Name if its not autoallocation and allocated to more than one store.

            }*/

            /**************************************************************/


            $aAllocated = array('store_allocated' , 'store_pickup_allocated');
            $aUnallocated = array('store_unallocated');

            $aRejected = array('store_rejected');
            $aAccepted = array('store_accepted' , 'out_for_hub' , 'store_pickup_accepted');

            if(in_array($aOrderStatus , $aAccepted)){
                $StoreAction = "Accepted by ";

            }elseif(in_array($aOrderStatus , $aRejected)){
                $StoreAction = "Rejected ";
                $sStoreName = "";


            }elseif(in_array($aOrderStatus , $aAllocated)){
                $StoreAction = "Allocated to ";

            }

        }

        return $StoreAction.$sStoreName;
    }


}
