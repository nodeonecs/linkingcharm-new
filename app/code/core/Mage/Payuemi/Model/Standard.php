<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category   Mage
 * @package    Mage_Payuemi
 * @copyright  Copyright (c) 2008 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Payuemi Standard Model
 *
 * @category   Mage
 * @package    Mage_Payuemi
 * @name       Mage_Payuemi_Model_Standard
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Mage_Payuemi_Model_Standard extends Mage_Payment_Model_Method_Abstract
{
    protected $_code  = 'payuemi_standard';
    protected $_formBlockType = 'payuemi/standard_form';

    protected $_isGateway               = false;
    protected $_canAuthorize            = true;
    protected $_canCapture              = true;
    protected $_canCapturePartial       = false;
    protected $_canRefund               = false;
    protected $_canVoid                 = false;
    protected $_canUseInternal          = false;
    protected $_canUseCheckout          = true;
    protected $_canUseForMultishipping  = false;

    protected $_order = null;
    protected $_infoBlockType = 'payuemi/info_pay';


     

    /**
     * Get Config model
     *
     * @return object Mage_Payuemi_Model_Config
     */
    public function getConfig()
    {
        return Mage::getSingleton('payuemi/config');
    }


    public function assignData($data)
    {
         if (!($data instanceof Varien_Object)) {
            $data = new Varien_Object($data);
        }
 
        $dataArr = array();
        $dataArr = $data->getData();
        $session = Mage::getSingleton("core/session",  array("name"=>"frontend"));
        $session->setData("cc_type", $dataArr['cc_type']);
        $session->setData("cc_owner", $dataArr['cc_owner']);
        $session->setData("cc_number_enc", $dataArr['cc_number_enc']);
        $session->setData("cc_exp_month", $dataArr['cc_exp_month']);
        $session->setData("cc_exp_year", $dataArr['cc_exp_year']);
        $session->setData("cc_last4", $dataArr['cc_last4']);

        $info = $this->getInfoInstance();
        $info->setBankName($data->getEmiBank())
            ->setEmiCode($data->getEmiMode());
        return $this;
        
 
    }

     /**
     * Return true if the method can be used at this time
     *
     * @param Mage_Sales_Model_Quote|null $quote
     * @return bool
     */
   /* public function isAvailable($quote = null)
    {

        if (!parent::isAvailable($quote)) {
            return false;
        }
        if (!is_null($quote)) {

            $minOrderAmt = Mage::getStoreConfig('payment/payuemi_standard/min_order_amt');
            if($minOrderAmt>0):
                if($quote->getBaseSubtotalWithDiscount() <$minOrderAmt)
                    {
                      return false;
                    }                
                endif;
            return true;
        }


    }
*/

    /**
     * Payment validation
     *
     * @param   none
     * @return  Mage_Payuemi_Model_Standard
     */
    public function validate()
    {
        parent::validate();
        $paymentInfo = $this->getInfoInstance();
        if ($paymentInfo instanceof Mage_Sales_Model_Order_Payment) {
            $currency_code = $paymentInfo->getOrder()->getBaseCurrencyCode();
        } else {
            $currency_code = $paymentInfo->getQuote()->getBaseCurrencyCode();
        }
        /*
        $bankName = $paymentInfo->getBankName();
        $emimode =  $paymentInfo->getEmiCode();

        if(empty($bankName) || empty($emimode)){
            $errorCode = 'invalid_data';
            $errorMsg = $this->_getHelper()->__('Check No and Date are required fields');
        }
 
        if($errorMsg){
            Mage::throwException($errorMsg);
        }

        */
        if ($currency_code != $this->getConfig()->getCurrency()) {
          Mage::throwException(Mage::helper('payuemi')->__('Selected currency //code ('.$currency_code.') is not compatabile with Payuemi'));
        }
        return $this;
    }

    /**
     * Capture payment
     *
     * @param   Varien_Object $orderPayment
     * @return  Mage_Payment_Model_Abstract
     */
    public function capture (Varien_Object $payment, $amount)
    {
        $payment->setStatus(self::STATUS_APPROVED)
            ->setLastTransId($this->getTransactionId());

        return $this;
    }

    /**
     *  Returns Target URL
     *
     *  @return	  string Target URL
     */
    public function getPayuemiUrl ()
    {
        $mode=Mage::getSingleton('payuemi/config')->getTransactionMode();

        if($mode == '1')
        {
            return 'https://test.payu.in/_payment';
        }
        else
        {
            return 'https://secure.payu.in/_payment';
        }
        //return 'https://secure.ebs.in/pg/ma/sale/pay/';
    }

    /**
     *  Return URL for Payuemi success response
     *
     *  @return	  string URL
     */
    protected function getSuccessURL ()
    {
        return Mage::getUrl('payuemi/standard/success', array('_secure' => true));
    }

    /**
     *  Return URL for Payuemi notification
     *
     *  @return	  string Notification URL
     */
    protected function getNotificationURL ()
    {
        return Mage::getUrl('payuemi/standard/notify', array('_secure' => true));
    }

    /**
     *  Return URL for Payuemi failure response
     *
     *  @return	  string URL
     */
    protected function getFailureURL ()
    {
        return Mage::getUrl('payuemi/standard/failure', array('_secure' => true));
    }

    /**
     *  Form block description
     *
     *  @return	 object
     */
    public function createFormBlock($name)
    {
        $block = $this->getLayout()->createBlock('payuemi/form_standard', $name);
        $block->setMethod($this->_code);
        $block->setPayment($this->getPayment());
        return $block;
    }

    /**
     *  Return Order Place Redirect URL
     *
     *  @return	  string Order Redirect URL
     */
    public function getOrderPlaceRedirectUrl()
    {
        return Mage::getUrl('payuemi/standard/redirect');
    }

    /**
     *  Return Standard Checkout Form Fields for request to Payuemi
     *
     *  @return	  array Array of hidden form fields
     */
    public function getStandardCheckoutFormFields ()
    {
    
        $order   = $this->getOrder();
        $payment = $order->getPayment();

      

        if (!($order instanceof Mage_Sales_Model_Order)) {
            Mage::throwException($this->_getHelper()->__('Cannot retrieve order object'));
        }
        $billingAddress = $order->getBillingAddress();
	    $shippingAddress = $order->getShippingAddress();

        $streets = $billingAddress->getStreet();
        $street = isset($streets[0]) && $streets[0] != ''
                  ? $streets[0]
                  : (isset($streets[1]) && $streets[1] != '' ? $streets[1] : '');


	

        if ($this->getConfig()->getDescription()) {
            $transDescription = $this->getConfig()->getDescription();
        } else {
            $transDescription = Mage::helper('payuemi')->__('Order #%s', $order->getRealOrderId());
        }

        if ($order->getCustomerEmail()) {
            $email = $order->getCustomerEmail();
        } elseif ($billingAddress->getEmail()) {
            $email = $billingAddress->getEmail();
        } else {
            $email = '';
        }

	//$user_credentials = "C0Dr8m".":".$billingAddress->getEmail();
	    $session = Mage::getSingleton("core/session",  array("name"=>"frontend"));
        
        $fields = array(
                        'order_id'         => $order->getIncrementId(),
                        'product_desc'     => $transDescription,
                        'product_price'    => number_format($order->getBaseGrandTotal(), 2, '.', ''),
                        'f_name'           => $billingAddress->getFirstname(),
                        's_name'           => $billingAddress->getLastname(),
                        'phone'            => $billingAddress->getTelephone(),
                        'city'             => $billingAddress->getCity(),
                        'email'            => $email,
                        'cc_type'          => $session->getData("cc_type"),
                        'cc_name'          => $session->getData("cc_owner"),
                        'cc_num'           => $session->getData("cc_number_enc"),
                        'cc_exp_mon'       => $session->getData("cc_exp_month"),
                        'cc_exp_yr'        => $session->getData("cc_exp_year"),
                      	'cc_cvv'           => $session->getData("cc_last4"),
                        'bank_name'        => $payment->getBankName(),
                        'emi_code'         => $payment->getEmiCode(),
			
                    );

        $session->unsetData('cc_type');
        $session->unsetData('cc_owner');
        $session->unsetData('cc_number_enc');
        $session->unsetData('cc_exp_month');
        $session->unsetData('cc_exp_year');
        $session->unsetData('cc_last4');
 
        return $fields;
    }

}
