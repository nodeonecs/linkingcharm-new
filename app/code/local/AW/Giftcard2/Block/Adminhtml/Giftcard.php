<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_Block_Adminhtml_Giftcard extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_giftcard';
        $this->_blockGroup = 'aw_giftcard2';
        $this->_headerText = Mage::helper('aw_giftcard2')->__('Gift Card Codes');
        $this->_addButtonLabel = $this->__('Add Gift Card Code');
        return parent::__construct();
    }

    public function getButtonsHtml($area = null)
    {
        $out = parent::getButtonsHtml();
        $form = new Varien_Data_Form(
            array(
                'id'      => 'giftcard_import',
                'name'      => 'giftcard_import',
                'action'  => Mage::helper('adminhtml')->getUrl('*/*/import'),
                'method'  => 'post',
                'enctype' => 'multipart/form-data'
            )
        );
        $form->setUseContainer(true);

        $config = array(
            'name' => AW_Giftcard2_Model_Import::FILE_ID,
            'label' => $this->__('CSV file to import:')
        );
        $element = new Varien_Data_Form_Element_File($config);
        $element->setId(AW_Giftcard2_Model_Import::FILE_ID);
        $form->addElement($element);

        $button = new Mage_Adminhtml_Block_Widget_Button();
        $button->setOnClick('gc2ImportForm.submit()');
        $button->setLabel($this->__('Import'));

        $html = $form->toHtml() . $button->toHtml() . $out;
        $html .= '<script type="text/javascript">gc2ImportForm = new varienForm(\'giftcard_import\', \'\');';
        $html .= '$$(\'#giftcard_import .field-row\').first().setStyle(\'display: inline;\');</script>';

        return $html;
    }
}