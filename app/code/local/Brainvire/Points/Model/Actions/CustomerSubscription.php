<?php
/**
  * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */


class Brainvire_Points_Model_Actions_CustomerSubscription extends Brainvire_Points_Model_Actions_Abstract {

    protected $_action = 'customer_subscription';
    protected $_comment = 'Reward for signing up to newsletter';

}