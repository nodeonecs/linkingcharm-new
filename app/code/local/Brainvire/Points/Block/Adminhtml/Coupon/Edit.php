<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   BV
 * @package    BV_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */


class Brainvire_Points_Block_Adminhtml_Coupon_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

    public function __construct() {
        $this->_objectId = 'id';
        $this->_blockGroup = 'points';
        $this->_controller = 'adminhtml_coupon';
        parent::__construct();
    }

    public function getHeaderText() {

        $coupon = Mage::registry('points_coupon_data');
        return ($coupon->getId()) ? (Mage::helper('points')->__('Coupon # %s ', $coupon->getId())) : Mage::helper('points')->__('New coupon');
    }

}