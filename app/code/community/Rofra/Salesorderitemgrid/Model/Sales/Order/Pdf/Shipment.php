<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Sales
 * @copyright  Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Sales Order Shipment PDF model
 *
 * @category   Mage
 * @package    Mage_Sales
 * @author     Magento Core Team <core@magentocommerce.com>
 */

require_once('Tcpdf/config/lang/eng.php');
require_once('Tcpdf/tcpdf.php');
include_once 'app/code/core/Mage/Sales/Model/Order/Pdf/Shipment.php';

class Rofra_Salesorderitemgrid_Model_Sales_Order_Pdf_Shipment extends Mage_Sales_Model_Order_Pdf_Shipment
{
    /**
     * Draw table header for product items
     *
     * @param  Zend_Pdf_Page $page
     * @return void
     */
    protected function _drawHeader(Zend_Pdf_Page $page)
    {
        /* Add table head */
        $this->_setFontRegular($page, 10);
        $page->setFillColor(new Zend_Pdf_Color_RGB(0.93, 0.92, 0.92));
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.5));
        $page->setLineWidth(0.5);
        $page->drawRectangle(25, $this->y, 570, $this->y-15);
        $this->y -= 10;
        $page->setFillColor(new Zend_Pdf_Color_RGB(0, 0, 0));

        //columns headers
        $lines[0][] = array(
            'text' => Mage::helper('sales')->__('Products'),
            'feed' => 100,
            );

        $lines[0][] = array(
            'text'  => Mage::helper('sales')->__('Qty'),
            'feed'  => 35
            );

        $lines[0][] = array(
            'text'  => Mage::helper('sales')->__('SKU'),
            'feed'  => 565,
            'align' => 'right'
            );

        $lineBlock = array(
            'lines'  => $lines,
            'height' => 10
            );

        $this->drawLineBlocks($page, array($lineBlock), array('table_header' => true));
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
        $this->y -= 20;
    }

    /**
     * Return PDF document
     *
     * @param  array $shipments
     * @return Zend_Pdf
     */
    public function getPdf($shipments = array())
    {   
        $store_id = $shipments[0]['store_id'];
        $store_name = Mage::getModel('core/store')->load($store_id)->getName();
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false ,false ,false);//last parameter for  footer  if true then footer show. if false then footer not show
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetTitle('Shipment');
        $logoweb = Mage::getStoreConfig('sales/identity/logo', $store_id);
        $img_url = (string) Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . "sales/store/logo/" . $logoweb;
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $pdf->SetMargins(20,20,20,TRUE);
        $pdf->SetAutoPageBreak(TRUE, 15);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->setLanguageArray($l);
        $pdf->SetFont('dejavusans', 'B', 20);
        $pdf->SetFont('dejavusans', '', 8);
        $product = array();
         $CompanyAddress = Mage::getStoreConfig('general/store_information/address');
        $CompanyName = Mage::getStoreConfig('general/store_information/name')." ";
        foreach ($shipments as $shipment)
        {
            $shipment_itemcollection = $shipment->getItemsCollection()->getData();

            $total_qty = 0;
            $line_item_total = 0;
            $total_price = 0;
            foreach($shipment_itemcollection as $item)
            {
                $order_item_id = $item['order_item_id'];
                $order_item = Mage::getModel('sales/order_item')->load($order_item_id);
                $item_base_tax_amount = $order_item->getBaseTaxAmount();
                $item_base_discount_amount = $order_item->getBaseDiscountAmount();
                $total_qty += $item['qty'];
                $price_item = $item['price']-$item_base_discount_amount+$item_base_tax_amount;
                $line_item_total = $price_item*$item['qty'];
                $total_price += $line_item_total;
                $product[] = array('name'=>$item['name'],'brand'=>$store_name,'qty'=>$item['qty'],'rate'=>$price_item,'amount'=>$price_item*$item['qty']);
            }
            $pdf->AddPage();

            $order = $shipment->getOrder();
            $invoice_no = '&nbsp;';
            $invoice_Date = '&nbsp;';
            if ($order->hasInvoices()) {

                foreach ($order->getInvoiceCollection() as $_eachInvoice) {
                    $invoice_no = $_eachInvoice->getData('increment_id');
                    $invoice_Date =date('d/m/Y', time($_eachInvoice->getData('created_at')));

                }
            }


            $order_date = date('d/m/Y', time($order->getData('created_at')));

            $trackArr = $shipment->getTracksCollection()->getData();
            $track_title = $trackArr[0]['title']; 
            $track_number = $trackArr[0]['track_number']; 

            $shippingAdd        = $shipment->getShippingAddress()->getData();
            $cust_name                 = $shippingAdd['firstname'].' '.$shippingAdd['lastname'];
            $filename = $shippingAdd['firstname'].$shippingAdd['lastname'];
            $street                     = $shippingAdd['street'];
            $city                       = $shippingAdd['city'];
            $postcode           = $shippingAdd['postcode'];
            $region                     = $shippingAdd['region'];
            $telephone          = $shippingAdd['telephone'];
            $shippingNo             = $shipment->getIncrementId(); 

            $orderId                = $order->getData('increment_id');
            $threechar = '';
            
            $shippingDate       = date('d/m/Y', $shipment->getCreatedAtDate()->getTimestamp());

            $paymentMethod = $order->getPayment()->getMethodInstance()->getTitle();

                $basegrandtotal             = number_format($order->getData('base_grand_total'));//total value
                $pdf->SetFont('dejavusans', '', 8);
                
                $params_awb = $pdf->serializeTCPDFtagParameters(array($track_number, 'C39', '', '', 40, 10, 0.4, array('position'=>'S', 'border'=>false, 'padding'=>0, 'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>false, 'font'=>$fontname, 'fontsize'=>2, 'stretchtext'=>4), 'N'));
                $params_shipment_id = $pdf->serializeTCPDFtagParameters(array($shippingNo, 'C39', '', '', 40, 10, 0.4, array('position'=>'S', 'border'=>false, 'padding'=>0, 'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>false, 'font'=>$fontname, 'fontsize'=>2, 'stretchtext'=>4), 'N'));
                $HTML = '<table border="1" cellpadding="4" cellspacing="0" width="100%" style="border: 1px solid #000000; font-size: 30px; font-family: arial">
                <tr>
                <td width="50%" align="left" valign="top" style="padding: 2px; border-bottom: 1px solid #000000;"><img src="'.$img_url.'" style="width: 200px; height: auto"></td>
                <td width="50%" align="center" valign="top" style="padding: 2px; border-bottom: 1px solid #000000; text-align: center">
                <p>'.$track_title.' </p>
                <tcpdf method="write1DBarcode" params="'.$params_awb.'"/>
                <p>*'.$track_number.'*</p>
                </td>
                </tr>
                <tr>';
                if(strcmp($paymentMethod, 'Cash on Delivery')==0){

                    $HTML .='<td colspan="2" style="text-align: center; padding: 2px; background-color: #eee; border-bottom: 2px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000; font-size: 36px;" width="100%">COLLECT CASH ON DELIVERY RS. '.$total_price .' ONLY </td>';
                }
                else{
                    $HTML .='<td colspan="2" style="text-align: center; padding: 2px; background-color: #eee; border-bottom: 2px solid #000000; font-size: 36px; border-left: 1px solid #000000; border-right: 1px solid #000000;" width="100%">RECEIVED AMOUNT RS. '.$total_price .'</td>';
                }
                $HTML .='</tr>

                <tr style="font-size:30px;">';
                if(strcmp($paymentMethod, 'Cash on Delivery')==0){
                    $HTML .='<td width="50%" align="left" valign="top">Name & Delivery Address</td> <td width="50%" align="left" valign="top">Payment Mode: COD</td>';
                }
                else{
                    $HTML .='<td width="50%" align="left" valign="top">Name & Delivery Address</td><td width="50%" align="left" valign="top">Payment Mode: PREPAID</td>';
                }
                $HTML .= 
                '</tr>
                
                <tr>
                    <td colspan="2">
                        <table width="100%" cellpadding="4" cellspacing="4">
                        <tr>
                            <td rowspan="3">                
                            <p>'.$cust_name.'<br/>'.$street.'<br/>'.$city.','.$region.','.$postcode.'<br/>T:'.$telephone.'</p>
                            </td>
                            <td width="50%" align="left" valign="top">
                                Order No:'.$orderId.'               
                            </td>
                        </tr>
                        <tr>
                        <td>
                            Shipment Id 
                            <br>
                            <tcpdf method="write1DBarcode" params="'.$params_shipment_id.'" />
                            <br>
                            *'.$shippingNo.'*
                          </td>
                        </tr>
                        <tr>
                          <td>Invoice No:'.$invoice_no.'</td>
                        </tr>
                        </table>    
                    </td>           
                </tr>               

                <tr>
                <td colspan="2">
                <table width="100%" border="0" cellpadding="2" cellspacing="2">
                <tr>
                <th width="10%" style="border-bottom: 1px solid #000">Sr No.</th>
                <th width="40%" style="border-bottom: 1px solid #000">Description</th>
                <th width="20%" style="border-bottom: 1px solid #000">Brand</th>
                <th width="10%" style="border-bottom: 1px solid #000">Qty</th>
                <th width="10%" style="border-bottom: 1px solid #000">Rate</th>
                <th width="10%" style="border-bottom: 1px solid #000">Amount</th>
                </tr>';
                $count = 0;
                foreach ($product as $value) {
                    $count++;
                    $HTML .= '<tr>
                    <td style="border-bottom: 1px solid #000">'.$count.'</td>
                    <td style="border-bottom: 1px solid #000">'.$value["name"].'</td>
                    <td style="border-bottom: 1px solid #000">'.$value["brand"].'</td>
                    <td style="border-bottom: 1px solid #000">'.number_format($value["qty"]).'</td>
                    <td style="border-bottom: 1px solid #000">'.Mage::helper('core')->currency($value['rate'], true, false).'</td>
                    <td style="border-bottom: 1px solid #000">'.Mage::helper('core')->currency($value['amount'], true, false).'</td>
                    </tr>';
                }
                $HTML .= '<tr>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>Total</th>
                <th>'.$total_qty.'</th>
                <th align="right">&nbsp;</th>
                <th align="left">'.Mage::helper('core')->currency($total_price, true, false).'</th>
                </tr>
                </table>                </td>
                </tr>
                <tr><td colspan="2" style="width:100%">Prices are inclusive of all applicable taxes<br /></td></tr>
                <tr><td colspan="2" style="width:100%">If Undelivered, please return to :<br /></td></tr>
                <tr><td colspan="2" style="width:100%">'.$CompanyName.'<br>'.$CompanyAddress.'<br /></td></tr>
                </table>
                ';  
            
                $pdf->writeHTML($HTML, true, false, false, false, '');

            }

            ob_end_clean();
            $filename = $filename.".pdf";
            $fileName = $filename;
            $pdf->Output($fileName, 'D');
        }

    /**
     * Create new page and assign to PDF object
     *
     * @param  array $settings
     * @return Zend_Pdf_Page
     */
    public function newPage(array $settings = array())
    {
        /* Add new table head */
        $page = $this->_getPdf()->newPage(Zend_Pdf_Page::SIZE_A4);
        $this->_getPdf()->pages[] = $page;
        $this->y = 800;
        if (!empty($settings['table_header'])) {
            $this->_drawHeader($page);
        }
        return $page;
    }
}
