<?php


class Rofra_Salesorderitemgrid_Block_Adminhtml_Order_Items_Grid_Renderer_Orderpaymentmethod
extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract{


public function render(Varien_Object $row)
    {
        $rowItemId = $row->getId();

        $orderId = Mage::getModel('sales/order_item')->load($rowItemId)->getOrderId();		
		$order = Mage::getModel('sales/order')->load($orderId);	
			
		try{
			$payment = $order->getPayment();	
			$payment_method_name = $payment->getMethodInstance()->getTitle();	
		}catch(Exception $e){

			$payment_method_name = $e->getMessage();

		}
		
        return $payment_method_name;

    }


}
