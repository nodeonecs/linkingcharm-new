<?php
class Magecomp_GstCharge_Model_Source_Gstoptions 
{
	public function toOptionArray()
    {
		$helper = Mage::helper('gstcharge');
		
        return array(
            array('value' => 0, 'label'=>$helper->__('None')),
			array('value' => 3, 'label'=>$helper->__('3%')),
            array('value' => 5, 'label'=>$helper->__('5%')),
			array('value' => 12, 'label'=>$helper->__('12%')),
			array('value' => 18, 'label'=>$helper->__('18%')),
			array('value' => 24, 'label'=>$helper->__('24%')),
			array('value' => 28, 'label'=>$helper->__('28%')),
        );
    }
}
