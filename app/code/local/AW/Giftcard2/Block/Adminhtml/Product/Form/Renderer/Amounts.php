<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_Block_Adminhtml_Product_Form_Renderer_Amounts extends Mage_Adminhtml_Block_Widget
    implements Varien_Data_Form_Element_Renderer_Interface
{
    protected $_websites = null;
    protected $_amountsTableId = 'aw-giftcard2-amounts-table';
    protected $_addButtonId = 'aw-giftcard2-product-add-amount-btn';
    protected $_allowOpenAmountCheckboxId = 'aw_gc2_allow_open_amount_checkbox';

    public function _construct()
    {
        $this->setTemplate('aw_giftcard2/product/edit/amounts.phtml');
    }

    public function getAmountsTableId()
    {
        return $this->_amountsTableId;
    }

    public function getRowsContainerId()
    {
        return $this->_amountsTableId . '_container';
    }

    protected function _getRowTemplate()
    {
        $htmlClass = $this->getElement()->getClass();
        $htmlName = $this->getElement()->getName();
        $websiteOptions = '';
        foreach ($this->getWebsites() as $value => $info) {
            $label = sprintf(
                "%s [%s]",
                $this->escapeHtml($info['name']),
                !empty($info['currency']) ? $this->escapeHtml($info['currency']) : ''
            );
            $websiteOptions .= "<option value=\"{$value}\">{$label}</option>";
        }
        $deleteBtnLabel = $this->__('Delete');
        return <<<HTML
    <tr>
        <td>
            <select class="{$htmlClass} required-entry"
                name="{$htmlName}[#{data.index}][website_id]"
                id="aw_gc2_amount_row_#{data.index}_website">
                {$websiteOptions}
            </select>
        </td>
        <td>
            <input class="{$htmlClass} required-entry validate-zero-or-greater aw-gc2-amounts-duplicate"
                type="text"
                name="{$htmlName}[#{data.index}][price]"
                value="#{data.price}"
                id="aw_gc2_amount_row_#{data.index}_price"
            />
        </td>
        <td class="last">
            <input type="hidden" name="{$htmlName}[#{data.index}][delete]" class="delete"
                    value="" id="aw_gc2_amount_row_#{data.index}_delete" />
            <button title="{$deleteBtnLabel}" type="button" class="action- scalable delete icon-btn delete-product-option"
                    id="aw_gc2_amount_row_#{data.index}_delete_button"
                    onclick="awGCAmounts.deleteAmount('aw_gc2_amount_row_#{data.index}_delete');return false">
                <span>{$deleteBtnLabel}</span>
            </button>
        </td>
    </tr>
HTML;
    }

    public function getAmountsErrorId()
    {
        return $this->_amountsTableId . '_error_container';
    }

    public function getAddButtonHtml()
    {
        return $this->getLayout()->createBlock('adminhtml/widget_button')
            ->setData(
                array(
                    'label'     => $this->__('Add Amount'),
                    'onclick'   => "awGCAmounts.addAmount()",
                    'class'     => 'add'
                )
            )
            ->toHtml();
    }

    public function isMultiWebsites()
    {
        return !Mage::app()->isSingleStoreMode();
    }

    public function getWebsites()
    {
        if ($this->_websites === null) {
            $websites = array();
            $websites[0] = array(
                'name'      => $this->__('All Websites'),
                'currency'  => Mage::app()->getBaseCurrencyCode()
            );
            $productWebsiteIds = $this->getProduct()->getWebsiteIds();
            foreach (Mage::app()->getWebsites() as $website) {
                if (!in_array($website->getId(), $productWebsiteIds)) {
                    continue;
                }
                $websites[$website->getId()] = array(
                    'name'      => $website->getName(),
                    'currency'  => $website->getConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_BASE),
                );
            }
            $this->_websites = $websites;
        }
        return $this->_websites;
    }

    public function getDefaultWebsite()
    {
        return Mage::app()->getStore($this->getProduct()->getStoreId())->getWebsiteId();
    }

    public function getProduct()
    {
        return Mage::registry('product');
    }

    protected function _getAmountsValues()
    {
        $data = array();
        if ($values = $this->getElement()->getValue()) {
            foreach ($values as $value) {
                $data[] = array(
                    'website_id' => $value['website_id'],
                    'price' => $value['price']
                );
            }
        }
        return $data;
    }

    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        $this->setElement($element);
        return $this->toHtml();
    }
}