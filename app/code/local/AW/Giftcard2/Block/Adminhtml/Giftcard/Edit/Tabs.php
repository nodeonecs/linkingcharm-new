<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_Block_Adminhtml_Giftcard_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('aw_giftcard2_info_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle($this->__('Gift Card'));
    }

    protected function _beforeToHtml()
    {
        $this->addTab('info_tab', array(
            'label'     => $this->__('Information'),
            'content'   => $this->getLayout()
                ->createBlock('aw_giftcard2/adminhtml_giftcard_edit_tab_information')
                ->initForm()
                ->toHtml(),
            'active'    => true
        ));

        $giftcardModel = Mage::registry('current_giftcard');
        if ($giftcardModel->getId()) {
            $activeTab = $this->getRequest()->getParam('tab', null);
            $state = false;

            if ($activeTab == 'aw_giftcard2_info_tabs_history_tab') {
                $state = true;
            }

            $this->addTab('history_tab', array(
                'label'     => $this->__('History'),
                'content'   => $this->getLayout()
                    ->createBlock('aw_giftcard2/adminhtml_giftcard_edit_tab_history')
                    ->toHtml(),
                'active'    => $state
            ));
        }
        return parent::_beforeToHtml();
    }

    protected function _toHtml()
    {
        $html = parent::_toHtml();
        $script = "<script type=\"text/javascript\">
                    //<![CDATA[
                    Event.observe(window, 'load', initCurrency);
                    var currencies = {$this->_getCurrencyJson()}
                    function updateCurrency() {
                        var val = $('_info_website_id').options[$('_info_website_id').selectedIndex].value;
                        if (currencies[val]) {
                            $('balance_currency').innerHTML = '<b>[' + currencies[val] + ']</b>';
                        }
                    }

                    function initCurrency() {
                        if ($('_info_website_id') != null) {
                            Event.observe($('_info_website_id'), 'change', updateCurrency);
                            updateCurrency()
                        }
                    }
                    //]]>
                    </script>";
        return  $html.$script;
    }

    protected function _getCurrencyJson()
    {
        $result = array();
        $websites = Mage::getSingleton('adminhtml/system_store')->getWebsiteCollection();
        foreach ($websites as $id => $website) {
            $result[$id] = $website->getBaseCurrencyCode();
        }
        return Mage::helper('core')->jsonEncode($result);
    }
}