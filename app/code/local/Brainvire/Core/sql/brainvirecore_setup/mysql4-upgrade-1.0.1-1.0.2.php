<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */


$installer = $this;



$installer->startSetup();

$installer->run("
	ALTER TABLE {$this->getTable('brainvirecore/logger')} ADD `custom_field_4` VARCHAR( 255 ) NOT NULL AFTER `custom_field_3`;
	ALTER TABLE {$this->getTable('brainvirecore/logger')} ADD INDEX ( `custom_field_4` );
");
$installer->endSetup();