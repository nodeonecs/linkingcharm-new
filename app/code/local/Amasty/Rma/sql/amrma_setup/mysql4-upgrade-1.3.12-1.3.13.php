<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Rma
 */


$installer = new Mage_Eav_Model_Entity_Setup($this->_resourceName);
$installer->startSetup();

$installer->addAttribute('catalog_product', 'allow_for_rma', array(
    'type' => 'int',
    'backend' => '',
    'frontend' => '',
    'label' => 'Allow for RMA',
    'input' => 'boolean',
    'class' => '',
    'source' => 'eav/entity_attribute_source_boolean',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible' => true,
    'required' => false,
    'user_defined' => false,
    'default' => '1',
    'searchable' => false,
    'filterable' => false,
    'comparable' => false,
    'visible_on_front' => false,
    'unique' => false,
    'apply_to' => '',
    'is_configurable' => false,
));

$attributeId = $installer->getAttributeId('catalog_product', 'allow_for_rma');

foreach ($installer->getAllAttributeSetIds('catalog_product') as $attributeSetId) {
    try {
        $attributeGroupId = $installer->getAttributeGroupId('catalog_product', $attributeSetId, 'General');
    } catch (Exception $e) {
        $attributeGroupId = $installer->getDefaultAttributeGroupId('catalog_product', $attributeSetId);
    }
    $installer->addAttributeToSet('catalog_product', $attributeSetId, $attributeGroupId, $attributeId);
}

$attrData = array('allow_for_rma' => '1');
$storeId = 1;
$productIds = Mage::getModel('catalog/product')->getCollection()->getAllIds();
Mage::getModel("catalog/product_action")->updateAttributes($productIds, $attrData, $storeId);

$this->run("ALTER TABLE `{$this->getTable('amrma/request')}` ADD last_show TIMESTAMP NOT NULL after `updated`");

$installer->endSetup();
