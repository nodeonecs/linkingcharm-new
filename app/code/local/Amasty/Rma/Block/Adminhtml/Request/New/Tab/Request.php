<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2015 Amasty (https://www.amasty.com)
 * @package Amasty_Rma
 */ 
class Amasty_Rma_Block_Adminhtml_Request_New_Tab_Request extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $model = $this->getModel();
                
        $form = new Varien_Data_Form();
        $this->setForm($form);
        
        /* @var $hlp Amasty_Rma_Helper_Data */
        $hlp = Mage::helper('amrma');
    
        $fldInfo = $form->addFieldset('information', array('legend'=> $hlp->__('Add Return Request here...')));
        //       
        //  $fldInfo->addField('request_id', 'link', array(
        //   'label'     => $hlp->__('ID'),
        //   'name'      => 'request_id',
        // ));
         
        $fldInfo->addField('increment_id', 'text', array(
          'label'     => $hlp->__('Order #'),
          'href' => Mage::helper('adminhtml')->getUrl("adminhtml/sales_order/view", array(
              'order_id' => $model->getOrderId()
            )),
          'required'  => true,
          'name'      => 'increment_id',
          'value'     => $this->getRequest()->getParam('order_id'),
        ));

        $fldInfo->addField('request_created_by', 'hidden', array(
          'label'     => $hlp->__('Request Created By'),
          'name'      => 'request_created_by',
          'readonly'  => true,
          'value'     => Mage::getSingleton('admin/session')->getUser()->getUserId(),
        ));

        $fldInfo->addField('store_id', 'select', array(
           'label'     => $hlp->__('Select Website'),
           'name'      => 'store_id',
           'required'  => true,
           'options'   => $hlp->getWebsitesEnabled(),
           // 'value'     => $model->getIsShipped()
        ));
        
        $fldInfo->addField('buttonloader', 'button', array(
          // 'label'     => $hlp->__('Load Customer Data'),
          'name'      => 'buttonloader',
          'onclick'   => "loadDataUsingEmail(document.getElementById('increment_id').value,document.getElementById('store_id').value)",
          'value'      => ' Load Customer Data .. '
        ));

        $fldInfo->addField('email', 'text', array(
          'label'     => $hlp->__('Email'),
          'name'      => 'email',
          'required'  => true,
          // 'onclick'   => 'loadDataUsingEmail(this.value)'
          'readonly'  => true,
        ));

        $fldInfo->addField('customer_id', 'hidden', array(
          'label'     => $hlp->__('Customer Id'),
          'readonly' => true,
          'name'      => 'customer_id',
        ));
        $fldInfo->addField('customer_firstname', 'text', array(
          'label'     => $hlp->__('Customer First Name'),
          'name'      => 'customer_firstname',
          'required'  => true,
        ));
        $fldInfo->addField('customer_lastname', 'text', array(
          'label'     => $hlp->__('Customer Last Name'),
          'name'      => 'customer_lastname',
          'required'  => true,
        ));

        $fldInfo->addField('customer_phone', 'text', array(
          'label'     => $hlp->__('Customer Phone'),
          'name'      => 'customer_phone',
          'required'  => true,
          'readonly'  => true,
        ));

        $fldInfo->addField('itemselect', 'select', array(
          'label'     => $hlp->__('Select Product to be return'),
          'name'      => 'itemselect',
          'options'   => array(),
          'onclick'  => 'loadProductFields()',
          'required'  => true,
        ));
        $fldInfo->addField('order_id', 'text', array(
          'label'     => $hlp->__('Sub Order ID'),
          'readonly' => true,
          'name'      => 'order_id',
          'required'  => true,
        ));
        $fldInfo->addField('sku', 'text', array(
          'label'     => $hlp->__('SKU'),
          'name'      => 'sku',
          'readonly' => true,
          'required'  => true,
        ));
        $fldInfo->addField('name', 'text', array(
          'label'     => $hlp->__('Product Name'),
          'name'      => 'name',
          'readonly' => true,
          'required'  => true,
        ));
        $fldInfo->addField('product_id', 'text', array(
          'label'     => $hlp->__('Product ID'),
          'name'      => 'product_id',
          'readonly' => true,
          'required'  => true,
        ));
        $fldInfo->addField('sales_item_id', 'text', array(
          'label'     => $hlp->__('Sales Item ID'),
          'name'      => 'sales_item_id',
          'readonly' => true,
          'required'  => true,
        ));
        $fldInfo->addField('qty', 'select', array(
          'label'     => $hlp->__('Qty'),
          'name'      => 'qty',
          'options'   => array(),
          'required'  => true,
        ));
        $fldInfo->addField('status_id', 'select', array(
           'label'     => $hlp->__('Select Product Return Status'),
           'name'      => 'status_id',
           'required'  => true,
           'options'   => $hlp->getRequestStatusesForProducts(),
           // 'value'     => $model->getIsShipped()
        ));
        // $fldInfo->addField('reason', 'textarea', array(
        //   'label'     => $hlp->__('Reason'),
        //   'name'      => 'reason',
        // ));

        $fldInfo->addField('reason', 'select', array(
           'label'     => $hlp->__('Reason'),
           'name'      => 'reason',
           'required'  => true,
           'values'   => Amasty_Rma_Block_Adminhtml_Request_New_Tab_Request::getValueArrayReturnsReasons(),
           //'options'   => Mage::helper('amrma')->getReasons(),
         ));


        $fldInfo->addField('condition', 'select', array(
          'label'     => $hlp->__('Condition'),
          'name'      => 'condition',
           'values'   => Amasty_Rma_Block_Adminhtml_Request_New_Tab_Request::getValueArrayReturnsConditions(),
        ));
        $fldInfo->addField('resolution', 'select', array(
          'label'     => $hlp->__('Resolution'),
          'name'      => 'resolution',
          'values'   => Amasty_Rma_Block_Adminhtml_Request_New_Tab_Request::getValueArrayReturnsResolutions(),
        ));
        $fldInfo->addField('otp', 'hidden', array(
          // 'label'     => $hlp->__('Load Customer Data'),
          'name'      => 'otp',
          'value'      => rand(0000,9999)
        ));
        $fldInfo->addField('sendOtp', 'button', array(
          // 'label'     => $hlp->__('Load Customer Data'),
          'name'      => 'sendOtp',
          'onclick'   => "sendOtpSMS(document.getElementById('otp').value,document.getElementById('sku').value,document.getElementById('customer_phone').value)",
          'value'      => ' Send Otp '
        ));
        $fldInfo->addField('enterOtp', 'text', array(
          'label'     => $hlp->__('Enter OTP received by customer'),
          'name'      => 'enterOtp',
          'required'  => true,
        ));
        $fldInfo->addField('verifyOtp', 'button', array(
          // 'label'     => $hlp->__('Load Customer Data'),
          'name'      => 'verifyOtp',
          'onclick'   => "verifyOtpfn(document.getElementById('otp').value,document.getElementById('enterOtp').value)",
          'value'      => ' Verify Otp '
        ));
        $form->addValues($model); 
        
        if ($this->hasExtraFields()){
            $fldInfo = $form->addFieldset('extra', array('legend'=> $hlp->__($this->getExtraTitle())));
            
            for ($field = 1; $field <= 5; $field++){
                $title = $this->getExtraField($field);
                if (!empty($title)) {
                    $fldInfo->addField('field_'.$field, 'label', array(
                        'label'     => $hlp->__($title),
                        'name'      => 'field_'.$field,
                    ));
                }
            }
            
            $form->setValues($model); 
        }
        
        return parent::_prepareForm();
    }
    
    public function hasExtraFields(){
        return Mage::helper("amrma")->hasExtraFields();
    }

    public function getExtraField($field){
        return Mage::helper("amrma")->getExtraField($field);
    }

    public function getExtraTitle(){
        return Mage::helper("amrma")->getExtraTitle();
    }

    static public function getValueArrayReturnsReasons(){

      $Data_array=array();

      $aReturns = Mage::helper('amrma')->getReasons();

      foreach($aReturns as $key => $returnsValues){
          
            $Data_array [$returnsValues] = $returnsValues;
            // $Data_array [$key]= array('value' => $returnsValues , 'label' => $returnsValues);

      }
      return $Data_array;

    }

    static public function getValueArrayReturnsResolutions(){

      $Data_array=array();

      $aReturns = Mage::helper('amrma')->getResolutions();

      foreach($aReturns as $key => $returnsValues){
          
            $Data_array [$returnsValues] = $returnsValues;
            // $Data_array [$key]= array('value' => $returnsValues , 'label' => $returnsValues);

      }

      return $Data_array;

    }


    static public function getValueArrayReturnsConditions(){

      $Data_array=array();

      $aReturns = Mage::helper('amrma')->getConditions();
      foreach($aReturns as $key => $returnsValues){
          
            $Data_array [$returnsValues] = $returnsValues;

      }
      return $Data_array;

    }
    
}
?>