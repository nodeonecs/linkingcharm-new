<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_Model_Resource_Giftcard_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('aw_giftcard2/giftcard');
    }

    public function addCodeFilter($code)
    {
        $this->addFieldToFilter('code', $code);
        return $this;
    }

    public function addGiftcardFilter(array $giftcardIds)
    {
        $this->addFieldToFilter('giftcard_id',  array('in' => $giftcardIds));
        return $this;
    }

    public function addOrderIncrementId()
    {
        $this->getSelect()
            ->joinLeft(
                array(
                    'order' => $this->getTable('sales/order')
                ),
                'main_table.order_id = order.entity_id',
                array(
                    'order_increment_id' => 'order.increment_id',
                )
            )
        ;
        return $this;
    }

    public function addProductName()
    {
        $nameAttr = Mage::getModel('catalog/resource_eav_attribute')->loadByCode('catalog_product', 'name');
        $nameTable = $nameAttr->getBackendTable();

        $this->getSelect()
            ->joinLeft(
                array (
                    'name_def' => $nameTable
                ),
                "main_table.product_id = name_def.entity_id AND name_def.attribute_id = {$nameAttr->getId()} AND name_def.store_id = 0",
                array(
                    'product_name' => 'name_def.value'
                )
            )
        ;
        return $this;
    }

    public function addAvailabilityFilter($availability)
    {
        $this->addFieldToFilter('availability', $availability);
        return $this;
    }

    public function addActiveFilter()
    {
        $this->addAvailabilityFilter(AW_Giftcard2_Model_Source_Giftcard_Availability::ACTIVE_VALUE);
        return $this;
    }
}