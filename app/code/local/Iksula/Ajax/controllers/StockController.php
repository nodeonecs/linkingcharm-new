<?php
class Iksula_Ajax_StockController extends Mage_Core_Controller_Front_Action
{

    /*
    * This controller gets product Id and returns its stock availability
    * and maximum qty available.
    */

    public function checkqtyAction(){

        $response = array();
        $productId = $this->getRequest()->getParam('productId');
        $requestedQty = $this->getRequest()->getParam('requestedQty');
        $storecode = $this->getRequest()->getParam('storecode');
        $checkouttype = $this->getRequest()->getParam('checkouttype');
        $model = Mage::getModel('catalog/product'); 
        $_product = $model->load($productId);
        if($checkouttype == 'deliverable') {

            if($_product && $_product->getName() && is_numeric($requestedQty)){
                $availableQty = (int)Mage::getModel('cataloginventory/stock_item')
                            ->loadByProduct($_product)->getQty();
                if($requestedQty <= $availableQty){
                    $response['status'] = 'success';
                    $response['maxqty'] = $requestedQty;
                    $response['isavailable'] = 'Yes';
                }
                else{
                    $response['status'] = 'success';
                    $response['maxqty'] = $availableQty;
                    $response['isavailable'] = 'No';
                }
            }
            else{
                $response['status'] = 'failed';
            } 
        }else {
            $bufferValue = Mage::getStoreConfig('iksula_storemanager/store/bufferfield');
            if($bufferValue != ''){
                // do nothing
            }else{
                $bufferValue = 0;                
            }

            $product_sku = $_product->getSku();
            
            $collection = Mage::getModel('storeinventory/storeinventory')->getCollection();
            $collection->addFieldToFilter('sku',$product_sku);
            $collection->addFieldToFilter('store_code',array('in' => $storecode));
            $collection->addFieldToFilter('inventory',array('gt' => $bufferValue));
            $data = $collection->getData();
            
            $result = $data[0]['inventory'] - $bufferValue;
            if(($data[0]['inventory'] -($bufferValue + $requestedQty)) >= 0){
                $response['status'] = 'success';
                $response['maxqty'] = $requestedQty;
                $response['isavailable'] = 'Yes';
            }
            else{
                $response['status'] = 'success';
                $response['maxqty'] = $result;
                $response['isavailable'] = 'No';
            }
        }
        
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
        return;
    }
}