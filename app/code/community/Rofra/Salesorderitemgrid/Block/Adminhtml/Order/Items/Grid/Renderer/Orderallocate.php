<?php


class Rofra_Salesorderitemgrid_Block_Adminhtml_Order_Items_Grid_Renderer_Orderallocate
extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract{


public function render(Varien_Object $row)
    {

        $rowItemId = $row->getId();
        $aAllocateLink = array('store_unallocated' , 'store_rejected' /*, 'cod_verified' , 'pending'*/ , 'processing');
        $aUnAllocateLink = array('store_allocated', 'store_pickup_allocated');
        $aAcceptedText = array('store_accepted' , 'store_pickup_accepted');
        $aRejectedText = array('store_rejected');
        $invoicedonecheck = Mage::helper('salesorderitemgrid')->getInvoiceOfOrderItem($rowItemId);
        $shipmentdonecheck = Mage::helper('salesorderitemgrid')->getShipmentOfOrderItem($rowItemId);
        
        $iLoginuserid = Mage::getSingleton('admin/session')->getUser()->getUserId();


        $LoginUserRole = Mage::getModel('admin/user')->load($iLoginuserid)->getRole()->getRoleId();

        $iRole_Collectionsid = Mage::getSingleton('amrolepermissions/rule')
                                                ->getCollection()
                                                ->addFieldToFilter('role_id' , array('eq' => $LoginUserRole));

                 foreach($iRole_Collectionsid as $Role){

                        $RoleCustomRole = $Role->getCustomRole();
                 }

         $oSalesOrderItemsCollections = Mage::getSingleton('sales/order_item')->load($rowItemId);
         $iAssignedStatus = $oSalesOrderItemsCollections->getItemStatus(); // Order Items Status .
         $iTransfertoHub = $oSalesOrderItemsCollections->getIsTransfertohub();

         $isacceptrejectlink = Mage::getStoreConfig('omniapis_section/custom_settings/acceptrejectstores');
         //$isAutoallocateActive = Mage::getStoreConfig('omniapis_section/custom_settings/allocations_of_orders');

         $RoleWiseAdmin = array('S_A' , 'W_HO');
         if(!in_array($RoleCustomRole , $RoleWiseAdmin)){


            if($isacceptrejectlink == '0'){
                return $html = '';
            }
            if($iAssignedStatus == 'out_for_hub'){
                return $html = "Order is transfered to hub so cannot allocate and unallocate ";
            }
            //if(in_array($iAssignedStatus , $aUnAllocateLink)){
            
            //if( (count($invoicedonecheck) == 0 && count($shipmentdonecheck) == 0 )){
            if(in_array($iAssignedStatus , $aUnAllocateLink)){
                            //if($isAutoallocateActive == '1'){

                    if(strtolower($RoleCustomRole) == 'hub'){
                        return $html = "";
                    }
                    if( (count($invoicedonecheck) == 0 && count($shipmentdonecheck) == 0 ))
                       return $html = '<a onclick="storeActionFunc('.$rowItemId.', 1 , 0)">Accept</a>';
                    
                /*}else{
                   return $html = '<a onclick="storeActionFunc('.$rowItemId.', 1 , 0)">Accept</a>|<a onclick="storeActionFunc('.$rowItemId.', 0 ,1)">Reject</a>'; // Manual Allocation show the Accept and reject link.
                }*/
            }elseif(in_array($iAssignedStatus , $aRejectedText)){
                return $html = 'Rejected';
            }elseif(in_array($iAssignedStatus , $aAcceptedText))
                //return $html = 'Accepted';
                    if( (count($invoicedonecheck) == 0 && count($shipmentdonecheck) == 0 ))
                        return $html = '<a onclick="storeActionFunc('.$rowItemId.', 0 ,1)">Reject</a>';

         }else{
            //if($isAutoallocateActive == '0'){
                if($iAssignedStatus != 'out_for_hub'){
                    $orderAllocateStatus = Mage::getModel('sales/order_item')->load($rowItemId);

                    if(in_array($iAssignedStatus , $aAllocateLink)){
                    	$Buttontext = 'Allocate order';
                    }/*elseif(in_array($iAssignedStatus , $aUnAllocateLink)){
                    	$Buttontext = 'Unallocate order';
                    }*/
                    return $html ='<a onclick="storeActionFunc('.$rowItemId.' , 0 , 0 );">'.$Buttontext.'</a>';
                }else{

                   return $html = "Order is transfered to hub so cannot allocate and unallocate ";
                }
            //}
        }
    }
}
