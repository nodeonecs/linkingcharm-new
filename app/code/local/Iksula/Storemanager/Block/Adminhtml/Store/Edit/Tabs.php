<?php
/**
 * Iksula_Storemanager extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category       Iksula
 * @package        Iksula_Storemanager
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Store admin edit tabs
 *
 * @category    Iksula
 * @package     Iksula_Storemanager
 * @author      Ultimate Module Creator
 */
class Iksula_Storemanager_Block_Adminhtml_Store_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Initialize Tabs
     *
     * @access public
     * @author Ultimate Module Creator
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('store_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('iksula_storemanager')->__('Store'));
    }

    /**
     * before render html
     *
     * @access protected
     * @return Iksula_Storemanager_Block_Adminhtml_Store_Edit_Tabs
     * @author Ultimate Module Creator
     */
    protected function _beforeToHtml()
    {
        $this->addTab(
            'form_store',
            array(
                'label'   => Mage::helper('iksula_storemanager')->__('Store'),
                'title'   => Mage::helper('iksula_storemanager')->__('Store'),
                'content' => $this->getLayout()->createBlock(
                    'iksula_storemanager/adminhtml_store_edit_tab_form'
                )
                ->toHtml(),
            )
        );

        $actionNameRegister = Mage::registry('action-name');

        if(isset($actionNameRegister)){

            $actionname = Mage::registry('action-name');

        }else{

            $actionname = Mage::app()->getRequest()->getActionName();

        }

        if($actionname == 'new'){

            $this->addTab(
                'form_user_store',
                array(
                    'label'   => Mage::helper('iksula_storemanager')->__('User'),
                    'title'   => Mage::helper('iksula_storemanager')->__('User'),
                    'content' => $this->getLayout()->createBlock(
                        'iksula_storemanager/adminhtml_store_edit_tab_user'
                    )
                    ->toHtml(),
                )
            );

            /*$this->addTab(
                'form_scope_store',
                array(
                    'label'   => Mage::helper('iksula_storemanager')->__('Scope'),
                    'title'   => Mage::helper('iksula_storemanager')->__('Scope'),
                    'content' => $this->getLayout()->createBlock(
                        'amrolepermissions/adminhtml_tab_scope'
                    )
                    ->toHtml(),
                )
            );*/
             /*$this->addTab(
                'form_role_role',
                array(
                    'label'   => Mage::helper('iksula_storemanager')->__('Role'),
                    'title'   => Mage::helper('iksula_storemanager')->__('Role'),
                    'content' => $this->getLayout()->createBlock(
                        'iksula_storemanager/adminhtml_store_edit_tab_role'
                    )
                    ->toHtml(),
                )
            );*/


             $this->addTab(
                'form_role_resource',
                array(
                    'label'   => Mage::helper('iksula_storemanager')->__('Resource'),
                    'title'   => Mage::helper('iksula_storemanager')->__('Resource'),
                    'content' => $this->getLayout()->createBlock(
                        'adminhtml/permissions_tab_rolesedit'
                    )
                    ->toHtml(),
                )
            );
        }
        /*if (!Mage::app()->isSingleStoreMode()) {
            $this->addTab(
                'form_store_store',
                array(
                    'label'   => Mage::helper('iksula_storemanager')->__('Store views'),
                    'title'   => Mage::helper('iksula_storemanager')->__('Store views'),
                    'content' => $this->getLayout()->createBlock(
                        'iksula_storemanager/adminhtml_store_edit_tab_stores'
                    )
                    ->toHtml(),
                )
            );
        }*/
        return parent::_beforeToHtml();
    }

    /**
     * Retrieve store entity
     *
     * @access public
     * @return Iksula_Storemanager_Model_Store
     * @author Ultimate Module Creator
     */
    public function getStore()
    {
        return Mage::registry('current_store');
    }
}
