<?php
class Aster_Purchase_Adminhtml_PurchasebackendController extends Mage_Adminhtml_Controller_Action
{

	protected function _isAllowed()
	{
		//return Mage::getSingleton('admin/session')->isAllowed('purchase/purchasebackend');
		return true;
	}

	public function indexAction()
    {
       $this->loadLayout();
	   $this->_title($this->__("Purchase"));
	   $this->renderLayout();
    }

    public function storecsvAction()
	{
		if (isset($_FILES['purchase_csv']['name']) && $_FILES['purchase_csv']['name'] != '')
		{
			try{
				$importFolder = Mage::getBaseDir('var') . DS . 'import';
				$fileName       = $_FILES['purchase_csv']['name'];                       
				$fileExt        = strtolower(substr(strrchr($fileName, ".") ,1));                       
				$uploader       = new Varien_File_Uploader('purchase_csv');
				$fileNamewoe    = rtrim($fileName, $fileExt);

				$uploader->setAllowedExtensions(array('csv')); //add more file types you want to allow
				$uploader->setAllowRenameFiles(false);
				$uploader->setFilesDispersion(false);
				
				$path = Mage::getBaseDir('media') . DS . 'purchase' . DS .'purchase'.DS;
				$purchase_csv_file = preg_replace('/\s+/', '_', $_FILES['purchase_csv']['name']);
				$destFile = $path.$purchase_csv_file;
				$filename = $uploader->getNewFileName($destFile);
				$uploader->save($path, $filename);
				$post_data['purchase_csv']='purchase/purchase/'.$filename;
				$csv = new Varien_File_Csv();
				$dataCollection = $csv->getData($path.$filename); //path to csv
				array_shift($dataCollection);
				$purchaseModel = Mage::getModel("purchase/purchase");

				foreach ($dataCollection as $row) {
					$data = array();
					$currentDate = Mage::getModel('core/date')->timestamp();
					// var_dump(date('Y-m-d H:i:s',$row[4]));
					$timestamp = strtotime(str_replace('/', '-', $row[4])); 

					$inwardDate = isset($timestamp)?date('Y-m-d H:i:s',$timestamp):$currentDate;

					$data['po_number'] = $row[0];
					$data['sku'] = $row[1];
					$data['qty'] = $row[2];
					$data['avg_price'] = $row[3];
					$data['inward_date'] = $inwardDate;
					$data['created_at'] = $currentDate;
					$data['updated_at'] = $currentDate;
					$purchaseModel->setData($data);
					$purchaseModel->save();
					// write logic for update po_number,avg_price,qty in the Magento System
				}

				$this->_redirect('*/adminhtml_purchase/index');	

			}
			catch(Exception $e)
			{
					Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
					$this->_redirect('*/*/');	
					return;
			}
		}
   	}


}
