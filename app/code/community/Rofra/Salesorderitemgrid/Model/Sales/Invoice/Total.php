<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category   Phoenix
 * @package    Phoenix_CashOnDelivery
 * @copyright  Copyright (c) 2010 - 2013 PHOENIX MEDIA GmbH (http://www.phoenix-media.eu)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Rofra_Salesorderitemgrid_Model_Sales_Invoice_Total extends Phoenix_CashOnDelivery_Model_Sales_Invoice_Total
{
    public function collect(Mage_Sales_Model_Order_Invoice $invoice)
    {
        $order = $invoice->getOrder();

        if ($order->getPayment()->getMethodInstance()->getCode() != 'phoenix_cashondelivery' || !$order->getCodFee()) {
            return $this;
        }

        foreach ($invoice->getOrder()->getInvoiceCollection() as $previousInvoice) {
            if ($previousInvoice->getCodAmount() && !$previousInvoice->isCanceled()) {
                $includeCodTax = false;
            }
        }
//        $item_id = Mage::app()->getRequest()->getParam('sub_order_id');
        $item_id = Mage::registry('invoice_sub-order_id');
        $ItemObj = Mage::getModel('sales/order_item')->load($item_id);

        $baseCodFee         = $ItemObj->getItemBaseCodFee();
        $baseCodFeeInvoiced = $ItemObj->getItemBaseCodFeeInvoiced();
        $baseInvoiceTotal   = $invoice->getBaseGrandTotal();
        $codFee             = $ItemObj->getItemCodFee();
        $codFeeInvoiced     = $ItemObj->getItemCodFeeInvoiced();
        $invoiceTotal       = $invoice->getGrandTotal();

        if(floor($ItemObj->getQtyInvoiced()) != 0){
            return $this;
        }

        /*Mage::log($baseCodFee . "<===>" . $baseCodFeeInvoiced. "<====>" . $codFee ."<---->" . $codFeeInvoiced , null , 'cod_test.log');*/

        /*if (!$baseCodFee || $baseCodFeeInvoiced==$baseCodFee) {
            return $this;
        }*/

        $baseCodFeeToInvoice = $baseCodFee - $baseCodFeeInvoiced;
        $codFeeToInvoice     = $codFee     - $codFeeInvoiced;

        $baseInvoiceTotal = $baseInvoiceTotal + $baseCodFeeToInvoice;
        $invoiceTotal     = $invoiceTotal     + $codFeeToInvoice;

        $invoice->setBaseGrandTotal($baseInvoiceTotal);
        $invoice->setGrandTotal($invoiceTotal);

        $invoice->setBaseCodFee($baseCodFeeToInvoice);
        $invoice->setCodFee($codFeeToInvoice);

        $ItemObj->setItemBaseCodFeeInvoiced($baseCodFeeInvoiced + $baseCodFeeToInvoice);
        $ItemObj->setItemCodFeeInvoiced($codFeeInvoiced         + $codFeeToInvoice);

        return $this;
    }
}
