<?php
/**
* Brainvire Infotech Pvt. Ltd
  * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */


class Brainvire_Points_Model_Actions_AddedByAdmin extends Brainvire_Points_Model_Actions_Abstract {

    protected $_action = 'added_by_admin';

    public function getComment() {
        if (isset($this->_commentParams['comment']))
            return $this->_commentParams['comment'];
        return $this->_comment;
    }

    public function getCommentHtml($area = self::ADMIN) {
        return Mage::helper('points')->__($this->_transaction->getComment());
    }

}
