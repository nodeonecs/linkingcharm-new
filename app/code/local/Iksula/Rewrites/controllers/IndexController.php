<?php
class Iksula_Rewrites_IndexController extends Mage_Core_Controller_Front_Action{

	public function editsizeAction()
    {

        $productDetails = Mage::app()->getRequest()->getParam('product_details');
        $productDetailsArray = explode('_', $productDetails);
        $parentId = $productDetailsArray[1];
        $removeProductId = $productDetailsArray[2];
        $addProductId = $productDetailsArray[3];
        $cartHelper = Mage::helper('checkout/cart');
        $cart = Mage::getModel('checkout/cart');
        $items = $cartHelper->getCart()->getItems();
        
          try{
              $qty_of_product = Mage::getModel('sales/quote_item')->load($removeProductId)->getQty();
              $configproductId = $parentId;
              $_helper = Mage::helper('rewrites');
              $packsize_option_value = $_helper->getPackSizeByProductId($addProductId); 
              $_helper->getconfigAddtocart($configproductId,$packsize_option_value,$qty_of_product); 
              foreach ($items as $item) 
              {
                  $itemId = $removeProductId;
                  $qty = $item->getQty();
                  $cartHelper->getCart()->removeItem($itemId)->save();    
                  Mage::getSingleton('checkout/session')->setCartWasUpdated(true); 
              }
          }catch(exception $e){
              Mage::getSingleton('core/session')->addError($e->getMessage());

          }

    }
}