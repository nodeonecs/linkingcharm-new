<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_Model_Product_Attribute_Backend_Templates extends Mage_Eav_Model_Entity_Attribute_Backend_Abstract
{
    protected function _getResource()
    {
        return Mage::getResourceSingleton('aw_giftcard2/product_attribute_backend_templates');
    }

    /**
     * @param Mage_Catalog_Model_Product $object
     * @return $this|void
     * @throws Mage_Core_Exception
     */
    public function validate($object)
    {
        if ($this->_isPhysical($object)) {
            return $this;
        }

        $templatesData = $object->getData($this->getAttribute()->getName());
        $templatesCount = 0;

        if ($templatesData !== null) {

            $storeTemplates = array();
            foreach ($templatesData as $template) {
                if (!isset($template['delete'])) {
                    continue;
                }
                $_storeId = $template['store_id'];
                if (!isset($storeTemplates[$_storeId])) {
                    $storeTemplates[$_storeId] = array();
                }
                $storeTemplates[$_storeId][] = $template['template'];
                $templatesCount++;
            }
        }

        if ($templatesData === null || $templatesCount == 0) {
            Mage::throwException(Mage::helper('aw_giftcard2')->__('Gift Card template is not specified'));
        }

        foreach ($storeTemplates as $store => $templates) {
            $template = array_shift($templates);

            while (!is_null($template)) {
                if (in_array($template, $templates)) {
                    Mage::throwException(Mage::helper('aw_giftcard2')->__('Duplicate templates found.'));
                }
                $template = array_shift($templates);
            }
        }

        return $this;
    }

    public function afterLoad($object)
    {
        $data = $this->_getResource()->loadTemplatesData($object->getId());
        $object->setData($this->getAttribute()->getName(), $data);
        $object->setOrigData($this->getAttribute()->getName(), $data);
        return $this;
    }

    public function afterSave($object)
    {
        $origTemplatesData = $object->getOrigData($this->getAttribute()->getName());
        $templatesData = $object->getData($this->getAttribute()->getName());

        if ($templatesData === null) {
            return $this;
        }

        $old = $new = array();

        if ($origTemplatesData && is_array($origTemplatesData)) {
            foreach ($origTemplatesData as $data) {
                $key = join('-', array($data['store_id'], $data['template']));
                $old[$key] = $data;
            }
        }

        if ($templatesData && is_array($templatesData)) {
            foreach ($templatesData as $data) {
                if (!isset($data['delete'])) {
                    continue;
                }
                $key = join('-', array($data['store_id'], $data['template']));
                $new[$key] = array(
                    'store_id' => $data['store_id'],
                    'value' => $data['template'],
                    'image' => $data['image']
                );
            }
        }

        $delete = array_diff_key($old, $new);
        $insert = array_diff_key($new, $old);
        $update = array_intersect_key($new, $old);

        if (!empty($delete)) {
            foreach ($delete as $data) {
                $this->_getResource()->deleteTemplatesData($this->_getTemplateObject($data, $object->getId()));
            }
        }
        if (!empty($insert)) {
            foreach ($insert as $data) {
                $this->_getResource()->saveTemplatesData($this->_getTemplateObject($data, $object->getId()));
            }
        }
        if (!empty($update)) {
            foreach ($update as $k => $v) {
                if (
                    $old[$k]['template'] != $v['value']
                    || $old[$k]['store_id'] != $v['store_id']
                    || $old[$k]['image'] != $v['image']
                ) {
                    $obj = $this->_getTemplateObject(
                        array_merge($v, array('value_id' => $old[$k]['value_id'])),
                        $object->getId()
                    );
                    $this->_getResource()->saveTemplatesData($obj);
                }
            }
        }
        return $this;
    }

    protected function _getTemplateObject($data, $entityId)
    {
        $data['entity_id'] = $entityId;
        return new Varien_Object($data);
    }

    protected function _isPhysical($product)
    {
        return $product->getTypeInstance()->isPhysicalGCType($product);
    }
}