<?php 
require_once('app/Mage.php'); //Path to Magento
umask(0);
Mage::app();

$resource = Mage::getSingleton('core/resource');
$readConnection = $resource->getConnection('core_read');
$writeConnection = $resource->getConnection('core_write');

echo "<br/>";
echo "------------------------------ Order ---------------------------------------------";

$query = 'SELECT DISTINCT sku FROM `sales_flat_order_item` where cost_price is NULL';
$results = $readConnection->fetchAll($query);

foreach ($results as $key => $item) {
	$product = Mage::getModel('catalog/product');
	$product->load($product->getIdBySku($item['sku']));
	$costPrice = $product->getData('cost_price');

	echo $newquery = "UPDATE sales_flat_order_item SET cost_price = '{$costPrice}' WHERE sku = '".$item['sku']."'";
	echo "<br/>";
	$writeConnection->query($newquery);
}

echo "<br/>";
echo "------------------------------Invoice ---------------------------------------------";

$query2 = 'SELECT DISTINCT sku FROM `sales_flat_invoice_item` where cost_price is NULL';
$results2 = $readConnection->fetchAll($query2);

foreach ($results2 as $key => $item) {
	$product = Mage::getModel('catalog/product');
	$product->load($product->getIdBySku($item['sku']));
	$costPrice = $product->getData('cost_price');

	echo $newquery2 = "UPDATE sales_flat_invoice_item SET cost_price = '{$costPrice}' WHERE sku = '".$item['sku']."'";
	echo "<br/>";
	$writeConnection->query($newquery2);
}
echo "<br/>";
echo "------------------------------Credit Memo ---------------------------------------------";
$query3 = 'SELECT DISTINCT sku FROM `sales_flat_creditmemo_item` where cost_price is NULL';
$results3 = $readConnection->fetchAll($query3);

foreach ($results3 as $key => $item) {
	$product = Mage::getModel('catalog/product');
	$product->load($product->getIdBySku($item['sku']));
	$costPrice = $product->getData('cost_price');

	echo $newquery3 = "UPDATE sales_flat_creditmemo_item SET cost_price = '{$costPrice}' WHERE sku = '".$item['sku']."'";
	echo "<br/>";
	$writeConnection->query($newquery3);
}