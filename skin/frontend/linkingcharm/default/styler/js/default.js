window.addToCart = function(items) {
	console.log(items);
	var submitForm = $j('<form method="post" action="/cart_update.php"></form>');
	var formStr = "";
	if(typeof items['chain'] !== 'undefined' || typeof items['charms'] !== 'undefined') {
		if(cart_item) {
			formStr = '<input type="hidden" name="editBracelet" value="'+cart_item+'"/>';
		} else {
			formStr = '<input type="hidden" name="addBracelet" value="true"/>';
		}
		if(items.chain) {
			formStr += '<input type="hidden" name="chain" value="'+items.chain.product_id+'"/>';
		}
		if(items.charms) {
			for(var i in items.charms) {
				formStr += '<input type="hidden" name="charms['+items.charms[i].left+']" value="'+items.charms[i].product_id+'"/>';
			}
		}
	} else if(typeof items['product_id'] !== 'undefined') {
		formStr += '<input type="hidden" name="add" value="'+items.product_id+'"/>';
		formStr += '<input type="hidden" name="qty" value="'+(items.quantity?items.quantity:1)+'"/>';
	}
	formStr += '<input type="hidden" name="return" value="/build.php"/>';
	submitForm.append(formStr);
	$j('body').append(submitForm);
	submitForm.submit();
};

window.initProfile = function(){
	hash = window.location.hash.replace(/^#!/, '');
	if (hash && hash != window.location.hash && hash.match(/^\/build\/|^\/edit\/|^\/profile\//)) {
		if (hash.match(/^\/build\//)) {
			$j.getJSON(baseurl+'styler/index/get_profile?hash=' + escape(hash.replace(/^\/build\//, '')), loadProfile);
		} else if (hash.match(/^\/profile\//)) {
			$j.getJSON(baseurl+'styler/index/get_profile?profile=' + escape(hash.replace(/^\/profile\//, '')), loadProfile);
		} else {
			cookie_name = false;
			cart_item = hash.replace(/^\/edit\//, '');
			$j.getJSON(baseurl+'styler/index/get_profile?edit=' + escape(hash.replace(/^\/edit\//, '')), loadProfile);
		}
	} else {
		$j.getJSON(baseurl+'styler/index/get_profile?cookie=' + cookie_name, loadProfile);
	}
}

window.edited = function() {
	var hashStr = '#!/build/';
	var data = {charms: {}};
	if ($j('#canvas').data('product-info')) {
		data.chain = $j('#canvas').data('product-info').product_id;
		hashStr += data.chain;
	}
	$j('#canvas img').sort(sortLeftAsc).each(function(i) {
		var charmData = $j(this).data('product-info');
		data.charms[i] = {
			product_id: charmData.product_id,
			left: charmData.left
		};
		hashStr += '/' + data.charms[i].product_id + '/' + data.charms[i].left;
	});
	window.location.replace(window.location.href.replace(/#.*/, '') + hashStr);

	if (cookie_name) {
		$j.cookie(cookie_name, JSON.stringify(data), {expires: 30, path: cookie_path});
	}
	recalculateTotal();
}