<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */


class Brainvire_Points_Block_Adminhtml_Rate_Earn_Edit_Form extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {
        $helper = Mage::helper('points');

        $rate = Mage::registry('points_rate_data');

        $form = new Varien_Data_Form(array(
                    'id' => 'edit_form',
                    'action' => $this->getUrl('*/*/save', array('_current' => true)),
                    'method' => 'post'
                ));

        $fieldset = $form->addFieldset('base_fieldset', array(
            'legend' => $helper->__('Reward Exchange Rate Information')
                ));

        $fieldset->addField('website_ids', 'multiselect', array(
            'name' => 'website_ids',
            'title' => $helper->__('Website'),
            'label' => $helper->__('Website'),
            'values' => Mage::getSingleton('adminhtml/system_store')->getWebsiteValuesForForm(),
            'value' => $rate->getWebsiteIds(),
            'required' => true,
            'after_element_html' => $helper->addSelectAll('website_ids')
        ));

        $groups = Mage::getResourceModel('customer/group_collection')
                ->addFieldToFilter('customer_group_id', array('gt' => 0))
                ->load()
                ->toOptionArray();

        $fieldset->addField('customer_group_ids', 'multiselect', array(
            'name' => 'customer_group_ids',
            'title' => $helper->__('Customer Group'),
            'label' => $helper->__('Customer Group'),
            'values' => $groups,
            'value' => $rate->getCustomerGroupIds(),
            'required' => true,
            'after_element_html' => $helper->addSelectAll('customer_group_ids')
        ));

        $fieldset->addField('direction', 'hidden', array(
            'name' => 'direction',
            'value' => $this->_getDirection()
        ));

        $ratesRenderer = $this->getLayout()
                ->createBlock('points/adminhtml_rate_earn_edit_form_renderer_rate')
                ->setDirection($this->_getDirection())
                ->setRate($rate);

        $fieldset->addField('rate_to_currency', 'note', array(
            'title' => $helper->__('Rate'),
            'label' => $helper->__('Rate'),
        ))->setRenderer($ratesRenderer);

        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

    protected function _getDirection() {
        return Brainvire_Points_Model_Rate::CURRENCY_TO_POINTS;
    }

}