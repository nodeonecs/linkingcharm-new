<?php
class Iksula_Rewrites_NotificationsController extends Mage_Core_Controller_Front_Action
{
    public function showcartaddednotificationAction(){
        $sku = $this->getRequest()->getParam('sku');
        $type = $this->getRequest()->getParam('type');
        $product = Mage::getModel('catalog/product')->loadByAttribute('sku',$sku);
        if($product){
            $response['status'] = 'success';
            $response['name'] = $product->getName();
            $response['thumbnail'] = $product->getThumbnailUrl();
            if($type == 'cart'){
                $response['msg'] = 'has been added to your cart.';
                $response['link'] = 'View Cart';
            }
            elseif($type == 'wishlist'){
                $response['msg'] = 'has been added to your wishlist.';
                $response['link'] = 'View Wishlist';
            }
            else{
                $response['msg'] = 'has been added to your cart.';
                $response['link'] = 'View Cart';
            }
        }else{
            $response['status'] = 'failure';
        }
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
        return;
    }
}
