<?php
/**
 * Iksula_Storemanager extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 *
 * @category       Iksula
 * @package        Iksula_Storemanager
 * @copyright      Copyright (c) 2015
 * @license        http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Store admin controller
 *
 * @category    Iksula
 * @package     Iksula_Storemanager
 * @author      Ultimate Module Creator
 */
class Iksula_Storemanager_Adminhtml_Storemanager_StoreController extends Iksula_Storemanager_Controller_Adminhtml_Storemanager
{
    /**
     * init the store
     *
     * @access protected
     * @return Iksula_Storemanager_Model_Store
     */
    protected function _initStore()
    {
        $storeId  = (int) $this->getRequest()->getParam('id');
        $store    = Mage::getModel('core/store_group');
        if ($storeId) {
            $store->load($storeId);
        }
        Mage::register('current_store', $store);
        return $store;
    }

    /**
     * default action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function indexAction()
    {
        $this->loadLayout();

        $this->_title(Mage::helper('iksula_storemanager')->__('Store Manager'))
             ->_title(Mage::helper('iksula_storemanager')->__('Store'));
        $this->renderLayout();
    }


   /* public function setwebsiteAction(){

        $website_id = $this->getRequest()->getParam('website_id');

        Mage::getModel('iksula_storemanager/store_option_storeassigned')->getOptionArray($website_id);




    }*/

    /**
     * grid action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function gridAction()
    {
        $this->loadLayout()->renderLayout();
    }

    /**
     * edit store - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function editAction()
    {
        $storeId    = $this->getRequest()->getParam('id');
        $store      = $this->_initStore();
        if ($storeId && !$store->getId()) {
            $this->_getSession()->addError(
                Mage::helper('iksula_storemanager')->__('This store no longer exists.')
            );
            $this->_redirect('*/*/');
            return;
        }
        $data = Mage::getSingleton('adminhtml/session')->getStoreData(true);
        if (!empty($data)) {
            $store->setData($data);
        }
        Mage::register('store_data', $store);
        $this->loadLayout();
        $this->_title(Mage::helper('iksula_storemanager')->__('Store Manager'))
             ->_title(Mage::helper('iksula_storemanager')->__('Store'));
        if ($store->getId()) {
            $this->_title($store->getGroupId());
        } else {
            $this->_title(Mage::helper('iksula_storemanager')->__('Add store'));
        }
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
        $this->renderLayout();
    }

    /**
     * new store action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function newAction()
    {

        Mage::register('action-name', 'new');
        $this->_forward('edit');
    }

    /**
     * save store - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function saveAction()
    {
        $store = $this->_initStore();
        $storeid = $store->getGroupId();

         $aDataAll = $this->getRequest()->getParams();

         $aUserRoleResource = $aDataAll;


        if ($data = $this->getRequest()->getPost('store')) {
            try {

                foreach ($data as $key => $value)
                {
                        /*************  Save the Store Entry in the store_hub_relation ***********/

                        /***********************************************/

                    if($key == 'store_definition'){

                        if($value != 'hub'){

                        }else{


                        }

                    }elseif($key == 'store_assigned'){


                        continue;
                    }else{
                        if (is_array($value))
                        {

                            $data[$key] = implode(',',$value);
                        }
                    }
                }


                $store->addData($data);
                $store->save();


                $data = $this->getRequest()->getPost('store');

                $lastinsertedid = $store->getId();



                $oStoreCollections = Mage::getModel('core/store')->getCollection()->addFieldToFilter('group_id' , array('eq' => $lastinsertedid));
                $oStoreCollections->getSize();

                if(($oStoreCollections->getSize()) == 0){


                                $data_storehub = array('store_id'=> $lastinsertedid,'hub_id'=> 0 ,'website_id'=>$data['website_id']);
                                    $storehubrelation = Mage::getModel('iksula_storemanager/storehubrelation')->setData($data_storehub);
                                    try {
                                        $storehubrelationId = $storehubrelation->save()->getId();
                                        echo "Data successfully inserted. Insert ID: ".$storehubrelationId;
                                    } catch (Exception $e){
                                     echo $e->getMessage();
                                    }

                                    //$sViewCode = "views_".$data['website_id']."_".$lastinsertedid;

                    $storeviewdata = array('code'=> "views_".$data['website_id']."_".$lastinsertedid,'website_id'=>$data['website_id'], 'group_id' => $lastinsertedid , 'name' => "views_".$data['website_id']."_".$lastinsertedid , 'is_active' => '1');
                    $storeviewmodel = Mage::getModel('core/store')->setData($storeviewdata);





                    try {

                     $LastViewId = $storeviewmodel->save()->getId();


                    } catch (Exception $e){
                     echo $e->getMessage();
                     exit;
                    }
                }



                if($data['store_definition'] == 'hub'){

                                        $hub_data = array('is_hub'=> '1');

                                        $HubsetId = Mage::getModel('iksula_storemanager/storehubrelation')->load($lastinsertedid , 'store_id')->getId();


                                        $HubSet = Mage::getModel('iksula_storemanager/storehubrelation')->load($HubsetId)->addData($hub_data);

                                        try {

                                            $HubSet->setId($HubsetId)->save();



                                        } catch (Exception $e){
                                            echo $e->getMessage();
                                        }


                    $ModelUnassignedStores = Mage::getModel('iksula_storemanager/storehubrelation')->getCollection();
                    $ModelUnassignedStores->addFieldToFilter('hub_id',array('eq' => $lastinsertedid));

                    if($ModelUnassignedStores->getSize() != 0){
                            foreach($ModelUnassignedStores as $StoreEntityObj)
                                   $StoreEntityObj->setHubId('0')->save();
                    }

                    $storeHubAssigned = $data['store_assigned'];// Store selected for the Hub assignment in the core_store_group table.


                    /**** Changes in the Store Assigned to the Hub it will update in the  amasty_amrolepermissions_rule for the Hub Role only  ****************/

                    $cStoresAssignedCollection = Mage::getModel('core/store')->getCollection()->addFieldToFilter('group_id' , array('in' => $storeHubAssigned ));

                    foreach($cStoresAssignedCollection as $storekey => $storevalue){

                        $aStoreViewIdsAssignedUpdated [] =   $storevalue->getStoreId();

                    }

                    $sStoreViewIdsAssignedUpdated = implode(',' , $aStoreViewIdsAssignedUpdated);


                    $cAmastyCustomHubRole = Mage::getModel('amrolepermissions/rule')->getCollection()->addFieldToFilter('custom_role' , array('eq' , 'HUB'));

                    foreach($cAmastyCustomHubRole as $rolekey => $rolevalue){

                        $HubStoreId = Mage::getModel('core/store')->load($rolevalue->getScopeHub())->getGroupId();

                        if($HubStoreId == $lastinsertedid){

                            $HubdataAssigned = array('scope_storeviews'=> $sStoreViewIdsAssignedUpdated );


                                        $HubAssigned = Mage::getModel('amrolepermissions/rule')->load($rolevalue->getRuleId())->addData($HubdataAssigned);
                                        try {
                                            $HubAssigned->setId($rolevalue->getRuleId())->save();


                                        } catch (Exception $e){
                                            echo $e->getMessage();
                                        }

                        }



                    }

                    /**************************************************************/


                                foreach($storeHubAssigned as $storeidSelected){

                                        $data = array('hub_id'=> $lastinsertedid );

                                        $HubAssignedTableId = Mage::getModel('iksula_storemanager/storehubrelation')->load($storeidSelected , 'store_id')->getId();
                                        $HubAssigned = Mage::getModel('iksula_storemanager/storehubrelation')->load($HubAssignedTableId)->addData($data);
                                        try {
                                            $HubAssigned->setId($HubAssignedTableId)->save();


                                        } catch (Exception $e){
                                            echo $e->getMessage();
                                        }



                                }

                }

                Mage::helper('iksula_storemanager')->createUserRoleForStore($aUserRoleResource ,$LastViewId);

                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('iksula_storemanager')->__('Store was successfully saved')
                );
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $store->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setStoreData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            } catch (Exception $e) {
                Mage::logException($e);
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('iksula_storemanager')->__('There was a problem saving the store.')
                );
                Mage::getSingleton('adminhtml/session')->setStoreData($data);
                $this->_redirect('*/*/index');
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('iksula_storemanager')->__('Unable to find store to save.')
        );
        $this->_redirect('*/*/');
    }



    /**
     * delete store - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function deleteAction()
    {

        // if ( $store_id = $this->getRequest()->getParam('id') > 0) {
        //     try {


        //         $storeViewModel = Mage::getModel('core/store');
        //         $storeviewcollections = $storeViewModel->getCollection()->addFieldToFilter('group_id' , array('eq' => $store_id));

        //         foreach($storeviewcollections as $StoreViews){

        //             $storeviewsid = $StoreViews->getStoreId();

        //             $storeViewModel->setId($storeviewsid)->delete();

        //         }


        //         $store = Mage::getModel('core/store_group');
        //         $store->setId($store_id)->delete();



        //         Mage::getSingleton('adminhtml/session')->addSuccess(
        //             Mage::helper('iksula_storemanager')->__('Store was successfully deleted.')
        //         );
        //         $this->_redirect('*/*/');
        //         return;
        //     } catch (Mage_Core_Exception $e) {
        //         Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        //         $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
        //     } catch (Exception $e) {
        //         Mage::getSingleton('adminhtml/session')->addError(
        //             Mage::helper('iksula_storemanager')->__('There was an error deleting store.')
        //         );
        //         $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
        //         Mage::logException($e);
        //         return;
        //     }
        // }
        // Mage::getSingleton('adminhtml/session')->addError(
        //     Mage::helper('iksula_storemanager')->__('Could not find store to delete.')
        // );
        $this->_redirect('*/*/');

    }

    /**
     * mass delete store - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massDeleteAction()
    {


        $storeIds = $this->getRequest()->getParam('store');


        if (!is_array($storeIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('iksula_storemanager')->__('Please select store to delete.')
            );
        } else {
            try {
                foreach ($storeIds as $storeId) {
                    $store = Mage::getModel('core/store_group');
                    $store_code = $store->getStoreCode();

                    //Mage::helper('iksula_storemanager')->deleteUserRoleForstores($store_code , $store_code);
                    $StoreDataRelation = Mage::getModel('iksula_storemanager/storehubrelation')->getCollection()
                                ->addFieldToFilter('hub_id' , array('eq' => $storeId));


                                foreach($StoreDataRelation as $storerelation){

                                    $storeRelationid = $storerelation->getId();

                                    $hubassignement = array('hub_id' => '0');

                                    $HubSet = Mage::getModel('iksula_storemanager/storehubrelation')->load($storeRelationid)->addData($hubassignement);

                                        try {

                                            $HubSet->setId($storeRelationid)->save();



                                        } catch (Exception $e){
                                            echo $e->getMessage();
                                        }


                                }

                    $store->setId($storeId)->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('iksula_storemanager')->__('Total of %d store were successfully deleted.', count($storeIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('iksula_storemanager')->__('There was an error deleting store.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * mass status change - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function massStatusAction()
    {
        $storeIds = $this->getRequest()->getParam('store');
        if (!is_array($storeIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('iksula_storemanager')->__('Please select store.')
            );
        } else {
            try {
                foreach ($storeIds as $storeId) {
                $store = Mage::getSingleton('core/store_group')->load($storeId)
                            ->setStatus($this->getRequest()->getParam('status'))
                            ->setIsMassupdate(true)
                            ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d store were successfully updated.', count($storeIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('iksula_storemanager')->__('There was an error updating store.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    /**
     * export as csv - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportCsvAction()
    {
        $fileName   = 'store.csv';
        $content    = $this->getLayout()->createBlock('iksula_storemanager/adminhtml_store_grid')
            ->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as MsExcel - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportExcelAction()
    {
        $fileName   = 'store.xls';
        $content    = $this->getLayout()->createBlock('iksula_storemanager/adminhtml_store_grid')
            ->getExcelFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as xml - action
     *
     * @access public
     * @return void
     * @author Ultimate Module Creator
     */
    public function exportXmlAction()
    {
        $fileName   = 'store.xml';
        $content    = $this->getLayout()->createBlock('iksula_storemanager/adminhtml_store_grid')
            ->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Check if admin has permissions to visit related pages
     *
     * @access protected
     * @return boolean
     * @author Ultimate Module Creator
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('core/store_group');
    }



    public function stateAction() {
        //echo 'hshshh';
        $countrycode = $this->getRequest()->getParam('country');
        $state = "<option value=''>Please Select</option>";
        if ($countrycode != '') {
            $statearray = Mage::getModel('directory/region')->getResourceCollection()->addCountryFilter($countrycode)->load();
            /*echo '<pre>';
            print_r($statearray->getData());*/

            foreach ($statearray as $_state) {
                $state .= "<option value='" . $_state->getRegionId() . "'>" .  $_state->getDefaultName() . "</option>";
            }
        }
        echo $state;
    }


    public function cityAction() {
        //echo 'hshshh';
        $statecode = $this->getRequest()->getParam('state');
        $city = "<option value=''>Please Select</option>";
        if ($statecode != '') {
            $cityarray = Mage::getModel('romcity/romcity')->getCollection()->addFieldToFilter('region_id' , array('eq' => $statecode));




            foreach ($cityarray as $_city) {
                //$city .= "<option value='" . $_city->getCityId() . "'>" .  $_city->getCity() . "</option>";
                $aCityData [$_city->getCityId()]= $_city->getCityname();
            }

             $aCityData = array_unique($aCityData);

            foreach ($aCityData as $_cityid => $_cityname) {
                $city .= "<option value='" . $_cityid . "'>" .  $_cityname . "</option>";
                //$aCityData = array($_city->getCityId() => $_city->getCity());
            }
        }

        echo $city;

    }

    public function checkusernameAction(){

        $username = trim($this->getRequest()->getParam('username'));

            $usernameCollections = Mage::getSingleton('admin/user')->getCollection()
            ->addFieldToFilter('username' , array('eq' => $username));

            $usernamecountcheck = $usernameCollections->getData();

            if(count($usernamecountcheck) > 0){

                echo '1';

            }else{

                echo '0';
            }


    }
}
