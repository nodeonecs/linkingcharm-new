<?php

class Aster_Purchase_Block_Adminhtml_Report_Purchase_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		// public function __construct()
		// {
		// 		parent::__construct();
		// 		$this->setId("purchaseGrid");
		// 		// $this->setDefaultSort("purchase_id");
		// 		// $this->setDefaultDir("DESC");
		// 		// $this->setSaveParametersInSession(true);
		// }

		// protected function _prepareCollection()
		// {
		// 		// $collection = Mage::getModel("purchase/purchase")->getCollection();
		// 		// $this->setCollection($collection);
		// 		// return parent::_prepareCollection();

		// 		parent::_prepareCollection();
  //       		$this->getCollection()->initReport('purchase/report_purchase_collection');
		// }

		// protected function _prepareColumns()
		// {
		// 		$this->addColumn("purchase_id", array(
		// 		"header" => Mage::helper("purchase")->__("ID"),
		// 		"align" =>"right",
		// 		"width" => "50px",
		// 	    "type" => "number",
		// 		"index" => "purchase_id",
		// 		));
                
		// 		$this->addColumn("po_number", array(
		// 		"header" => Mage::helper("purchase")->__("PO Number"),
		// 		"index" => "po_number",
		// 		));
		// 		$this->addColumn("sku", array(
		// 		"header" => Mage::helper("purchase")->__("SKU"),
		// 		"index" => "sku",
		// 		));
		// 		$this->addColumn("avg_price", array(
		// 		"header" => Mage::helper("purchase")->__("Avg Price"),
		// 		"index" => "avg_price",
		// 		));
		// 		$this->addColumn("qty", array(
		// 		"header" => Mage::helper("purchase")->__("Qty"),
		// 		"index" => "qty",
		// 		));
		// 	$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV')); 
		// 	$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

		// 		return parent::_prepareColumns();
		// }

		// public function getRowUrl($row)
		// {
		// 	   return '#';
		// }


		
		// protected function _prepareMassaction()
		// {
		// 	$this->setMassactionIdField('purchase_id');
		// 	$this->getMassactionBlock()->setFormFieldName('purchase_ids');
		// 	$this->getMassactionBlock()->setUseSelectAll(true);
		// 	$this->getMassactionBlock()->addItem('remove_purchase', array(
		// 			 'label'=> Mage::helper('purchase')->__('Remove Purchase'),
		// 			 'url'  => $this->getUrl('*/adminhtml_purchase/massRemove'),
		// 			 'confirm' => Mage::helper('purchase')->__('Are you sure?')
		// 		));
		// 	return $this;
		// }
			

		protected $_columnGroupBy = 'period';

		public function __construct()
		{
			parent::__construct();
			$this->setCountTotals(true);
		}

		public function getResourceCollectionName()
		{
			return 'purchase/report_purchase_collection';
		}

		protected function _prepareColumns()
		{

			$this->addColumn('period', array(
				'header'        => Mage::helper('purchase')->__('Period'),
				'index'         => 'period',
				'width'         => 100,
				'sortable'      => false,
				'period_type'   => $this->getPeriodType(),
				'renderer'      => 'adminhtml/report_sales_grid_column_renderer_date',
				'totals_label'  => Mage::helper('adminhtml')->__('Total'),
				'html_decorators' => array('nobr'),
				));

			$this->addColumn('sku', array(
				'header'    =>Mage::helper('purchase')->__('sku'),
				'index'     =>'sku',
				'sortable'  => false
				));

			$currencyCode = $this->getCurrentCurrencyCode();

			$this->addColumn('qty', array(
				'header'    => Mage::helper('purchase')->__('qty'),
				'currency_code' => $currencyCode,
				'index'     =>'qty',
				'type'      => 'currency',
				'total'     => 'sum',
				'sortable'  => false
				));

			$this->addExportType('*/*/exportComplexCsv', Mage::helper('purchase')->__('CSV'));

			return parent::_prepareColumns();
		}

}