<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   BV
 * @package    BV_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */


/**
 *
 */
class Brainvire_Points_Test_Controller_RewardController extends EcomDev_PHPUnit_Test_Case_Controller {

    
    public function indexAction($testId, $enabled, $isLoggedIn, $subscribe) {
        $this->registerPointsConfigMockObject($enabled);
        $this->registerCustomerSessionMockObject($isLoggedIn);
        $expected = $this->expected('id' . $testId);

        if ($subscribe)
            $this->getRequest()->setQuery('subscribe', true);
        $this->dispatch('points/reward/index');
        if ($expected->getIsSubscribe()) {
            $this->assertRedirect();
        } elseif (!$expected->getEnabled()) {
            $this->assertResponseHeaderNotContains('Location', 'login');
        } else {
            if (!$expected->getIsLoggedIn()) {
                $this->assertResponseHeaderContains('Location', 'login');
            } else {
                $this->assertResponseBodyContains("<title>Reward Points</title>");
            }
        }
    }

    
    public function subscribeAction($testId, $subscribe) {
        $this->registerPointsConfigMockObject(1);
        $this->registerCustomerSessionMockObject(1);
        $expected = $this->expected('id' . $testId);

        if ($subscribe)
            $this->getRequest()->setQuery('is_subscribed', true);
        $this->dispatch('points/reward/subscribe');
        $this->assertRedirect();
        $items = Mage::getSingleton('customer/session')->getMessages(true)->getItems(null);
        $this->assertEquals(
                $expected->getMessageCode(), $items[0]->getCode()
        );
    }

    
    protected function registerPointsConfigMockObject($enabled) {
        $stub = $this->getHelperMock('points/config', array('isPointsEnabled'));
        $stub->expects($this->any())
                ->method('isPointsEnabled')
                ->will($this->returnValue($enabled));
        $this->replaceByMock('helper', 'points/config', $stub);
    }

    
    protected function registerCustomerSessionMockObject($isLoggedIn) {
        $stub = $this->getModelMock('customer/session', array('isLoggedIn'));
        $stub->expects($this->any())
                ->method('isLoggedIn')
                ->will($this->returnValue($isLoggedIn));
        $this->replaceByMock('singleton', 'customer/session', $stub);
    }

}