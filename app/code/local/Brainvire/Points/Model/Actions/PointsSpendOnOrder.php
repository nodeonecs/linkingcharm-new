<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   Brainvire
 * @package    Brainvire_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * @license    
 */


class Brainvire_Points_Model_Actions_PointsSpendOnOrder extends Brainvire_Points_Model_Actions_Abstract {

    protected $_action = 'spend_on_order';
    protected $_comment = 'Spent on order #%s';
    protected $_commentHtml = 'Spent on order %s#%s%s';

    public function getComment() {
        if (isset($this->_commentParams['order_increment_id'])) {
            return sprintf($this->_comment, $this->_commentParams['order_increment_id']);
        }
        return $this->_comment;
    }

    public function getCommentHtml($area = self::ADMIN) {
        if (!$this->_transaction)
            return;
        $orderIncrementId = substr($this->_transaction->getComment(), strpos($this->_transaction->getComment(), '#') + 1);
        if (!$orderIncrementId)
            return;
        $order = Mage::getModel('sales/order')->loadByIncrementId($orderIncrementId);
        if ($area == self::ADMIN) {
            $orderUrl = Mage::getModel('adminhtml/url')->getUrl('adminhtml/sales_order/view/', array('order_id' => $order->getId()));
        } else {
            $orderUrl = Mage::getUrl('sales/order/view/', array('order_id' => $order->getId()));
        }
        return Mage::helper('points')->__($this->_commentHtml, '<a href="' . $orderUrl . '">', $order->getIncrementId(), '</a>');
    }

    public function addTransaction($additionalData = array()) {
        $action = parent::addTransaction($additionalData);
        $this->getTransaction()->saveSpendOrderInfo($this->getObjectForAction());
        return $action;
    }

}
