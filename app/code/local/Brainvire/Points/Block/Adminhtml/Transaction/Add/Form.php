<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   BV
 * @package    BV_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */


class Brainvire_Points_Block_Adminhtml_Transaction_Add_Form extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {
        $helper = Mage::helper('points');

        $form = new Varien_Data_Form(array(
                    'id' => 'transaction_add_form',
                    'action' => $this->getUrl('*/*/save'),
                    'method' => 'post',
                    'enctype' => 'multipart/form-data'
                ));

        $fieldset = $form->addFieldset('main_group', array('legend' => Mage::helper('points')->__('Fields')));

        $fieldset->addField('comment', 'text', array(
            'label' => $helper->__('Comment'),
            'required' => true,
            'name' => 'comment'
        ));

        $fieldset->addField('balance_change', 'text', array(
            'label' => $helper->__('Points Balance Change'),
            'required' => true,
            'name' => 'balance_change',
            'class' => 'validate-number'
        ));

        $fieldset->addField('selected_customers', 'hidden', array(
            'name' => 'selected_customers',
        ));

        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

}