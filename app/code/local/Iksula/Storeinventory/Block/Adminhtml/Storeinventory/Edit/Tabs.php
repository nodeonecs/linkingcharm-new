<?php
class Iksula_Storeinventory_Block_Adminhtml_Storeinventory_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
		public function __construct()
		{
				parent::__construct();
				$this->setId("storeinventory_tabs");
				$this->setDestElementId("edit_form");
				$this->setTitle(Mage::helper("storeinventory")->__("Item Information"));
		}
		protected function _beforeToHtml()
		{
				$this->addTab("form_section", array(
				"label" => Mage::helper("storeinventory")->__("Item Information"),
				"title" => Mage::helper("storeinventory")->__("Item Information"),
				"content" => $this->getLayout()->createBlock("storeinventory/adminhtml_storeinventory_edit_tab_form")->toHtml(),
				));
				return parent::_beforeToHtml();
		}

}
