<?php

    class Webkul_RmaSystem_Adminhtml_RmaController extends Mage_Adminhtml_Controller_Action {

        protected function _initAction() {
            $this->loadLayout()->_setActiveMenu("rmasystem");
            $this->getLayout()->getBlock("head")->setTitle($this->__("RMA System"));
            return $this;
        }

        public function indexAction() {
            $this->_initAction()->renderLayout();
        }

        public function editAction() {
            $id = $this->getRequest()->getParam("id");
            $model = Mage::getModel("rmasystem/rma")->load($id);
            if($model->getId() || $id == 0) {
                Mage::register("rma_data", $model);
                $this->loadLayout();
                $this->getLayout()->getBlock("head")->setTitle($this->__("RMA Request"));
                $this->_addContent($this->getLayout()->createBlock("rmasystem/adminhtml_rma_edit"))
                    ->_addLeft($this->getLayout()->createBlock("rmasystem/adminhtml_rma_edit_tabs"));
                $this->renderLayout();
            }
            else {
                Mage::getSingleton("adminhtml/session")->addError($this->__("Item does not exist"));
                $this->_redirect("*/*/");
            }
        }

        public function updateAction() {
            $post = $this->getRequest()->getParams();
            $status_flag = false;$delivery_flag = false;
            if($post["message"] != ""){
                Mage::getModel("rmasystem/conversation")
                    ->setRmaId($post["rma_id"])
                    ->setMessage($post["message"])
                    ->setCreatedAt(time())
                    ->setSender(0)
                    ->save();
            }
            $is_rma_save_required = false;
            $rma = Mage::getModel("rmasystem/rma")->load($post["rma_id"]);
            if(isset($post["admin_delivery_status"]) && $rma->getAdminDeliveryStatus() != $post["admin_delivery_status"]){
                $rma->setAdminDeliveryStatus($post["admin_delivery_status"]);
                if(isset($post["admin_consignment_no"])){ $rma->setAdminConsignmentNo($post["admin_consignment_no"]); }
                $is_rma_save_required = true;
                $delivery_flag = true;
            }
            if(isset($post["shipping_label"]) && $rma->getShippingLabel() != $post["shipping_label"]){
                $rma->setShippingLabel($post["shipping_label"]);
                $is_rma_save_required = true;
            }
            if($rma->getStatus() != $post["status"] && $post["status"] != ""){
                $rma->setStatus($post["status"]);
                $is_rma_save_required = true;
                $status_flag = true;
            }
            if($is_rma_save_required == true)
                $rma->save();
            if($status_flag == true || $delivery_flag == true)
                Mage::getModel("rmasystem/mails")->RMAUpdateAdmin($post,$status_flag,$delivery_flag);
            else
                Mage::getModel("rmasystem/mails")->NewMessage($post,$rma->getGroup(),"admin");
            Mage::getSingleton("adminhtml/session")->addSuccess($this->__("RMA Updated Successfully"));
            if(isset($post["back"]))
                $this->_redirect("*/*/edit", array("id" => $rma->getId()));
            else
                $this->_redirect("*/*/");
        }

        protected function _sendUploadResponse($fileName, $content, $contentType="application/octet-stream") {
            $response = $this->getResponse();
            $response->setHeader("HTTP/1.1 200 OK", "");
            $response->setHeader("Pragma", "public", true);
            $response->setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0", true);
            $response->setHeader("Content-Disposition", "attachment; filename=" . $fileName);
            $response->setHeader("Last-Modified", date("r"));
            $response->setHeader("Accept-Ranges", "bytes");
            $response->setHeader("Content-Length", strlen($content));
            $response->setHeader("Content-type", $contentType);
            $response->setBody($content);
            $response->sendResponse();
            die;
        }

        public function exportCsvAction() {
            $fileName = "rma.csv";
            $content = $this->getLayout()->createBlock("rmasystem/adminhtml_rma_grid")->getCsv();
            $this->_sendUploadResponse($fileName, $content);
        }

        public function exportXmlAction() {
            $fileName = "rma.xml";
            $content = $this->getLayout()->createBlock("rmasystem/adminhtml_rma_grid")->getXml();
            $this->_sendUploadResponse($fileName, $content);
        }

        public function gridAction()    {
            $this->loadLayout();
            $this->getResponse()->setBody($this->getLayout()->createBlock("rmasystem/adminhtml_rma_grid")->toHtml());
        }

    }