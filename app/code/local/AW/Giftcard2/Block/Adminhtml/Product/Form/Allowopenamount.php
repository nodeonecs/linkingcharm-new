<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_Block_Adminhtml_Product_Form_AllowOpenAmount extends Varien_Data_Form_Element_Select
{
    public function getAfterElementHtml()
    {
        $html = parent::getAfterElementHtml();
        return $html."  <script type=\"text/javascript\">
                        var openAmountOptions = {
                            openAmountSelector: '".$this->getHtmlId()."',
                            openAmountMinSelector: '".AW_Giftcard2_Model_Product_Type_Giftcard::ATTRIBUTE_CODE_OPEN_AMOUNT_MIN."',
                            openAmountMaxSelector: '".AW_Giftcard2_Model_Product_Type_Giftcard::ATTRIBUTE_CODE_OPEN_AMOUNT_MAX."',
                        }
                        $( openAmountOptions.openAmountSelector).observe('change', function () {
                            checkOpenAmount(openAmountOptions);
                        });
                        document.observe('dom:loaded', function () {
                            checkOpenAmount(openAmountOptions);
                        });
                        checkOpenAmount = function(openAmountOptions) {
                            if ($(openAmountOptions.openAmountSelector).value == 1) {
                                enableOpenAmounts(openAmountOptions);
        				    } else {
                                disableOpenAmounts(openAmountOptions);
        				    }
                        }

                        var enableOpenAmounts = function (options) {
                                $(options.openAmountMinSelector).up('tr').style = 'opacity: 1;';
                                $(options.openAmountMaxSelector).up('tr').style = 'opacity: 1;';
                                $(options.openAmountMinSelector).enable();
        				        $(options.openAmountMaxSelector).enable();
                        }

                        var disableOpenAmounts = function (options) {
                                $(options.openAmountMinSelector).value = '';
        				        $(options.openAmountMaxSelector).value = '';
        				        $(options.openAmountMinSelector).disable();
        				        $(options.openAmountMaxSelector).disable();
        				        $(options.openAmountMinSelector).up('tr').style = 'opacity: 0.5;';
                                $(options.openAmountMaxSelector).up('tr').style = 'opacity: 0.5;';
                        }
        				</script>";
    }
}