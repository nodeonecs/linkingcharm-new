<?php
require_once "Mage/Captcha/controllers/RefreshController.php";  
class Iksula_Rewrites_Captcha_RefreshController extends Mage_Captcha_RefreshController{

	public function customrefreshAction()
	    {
	        $paymentMethod = $this->getRequest()->getPost('payment');
	        $formId = $this->getRequest()->getPost('formId');
	        $captchaModel = Mage::helper('captcha')->getCaptcha($formId.$paymentMethod);
	        $this->getLayout()->createBlock($captchaModel->getBlockName())->setFormId($formId)->setIsAjax(true)->toHtml();
	        $this->getResponse()->setBody(json_encode(array('imgSrc' => $captchaModel->getImgSrc())));
	        $this->setFlag('', self::FLAG_NO_POST_DISPATCH, true);
	        Mage::log($captchaModel->getWord(),null,'captcha.log',true);
	    }
}
				