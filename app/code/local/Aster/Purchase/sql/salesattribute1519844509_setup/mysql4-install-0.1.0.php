<?php
$installer = $this;
$installer->startSetup();

$installer->addAttribute("quote_item", "hsn_code", array("type"=>"varchar"));
$installer->addAttribute("quote_item", "cost_price", array("type"=>"varchar"));
$installer->addAttribute("quote_item", "base_category", array("type"=>"varchar"));
$installer->addAttribute("order_item", "hsn_code", array("type"=>"varchar"));
$installer->addAttribute("order_item", "cost_price", array("type"=>"varchar"));
$installer->addAttribute("order_item", "base_category", array("type"=>"varchar"));
$installer->endSetup();
	 