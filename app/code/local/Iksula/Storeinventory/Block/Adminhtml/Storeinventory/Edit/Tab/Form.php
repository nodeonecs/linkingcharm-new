<?php
class Iksula_Storeinventory_Block_Adminhtml_Storeinventory_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("storeinventory_form", array("legend"=>Mage::helper("storeinventory")->__("Item information")));


						$fieldset->addField("store_code", "text", array(
						"label" => Mage::helper("storeinventory")->__("Store Code"),
						"class" => "required-entry",
						"required" => true,
						"name" => "store_code",
						));

						$fieldset->addField("sku", "text", array(
						"label" => Mage::helper("storeinventory")->__("Sku"),
						"class" => "required-entry",
						"required" => true,
						"name" => "sku",
						));

						$fieldset->addField("inventory", "text", array(
						"label" => Mage::helper("storeinventory")->__("Inventory"),
						"class" => "required-entry",
						"required" => true,
						"name" => "inventory",
						));

						 $fieldset->addField('website_id', 'select', array(
						'label'     => Mage::helper('storeinventory')->__('Website'),
						'values'   => Iksula_Storeinventory_Block_Adminhtml_Storeinventory_Grid::getValueArray3(),
						'name' => 'website_id',
						"class" => "required-entry",
						"required" => true,
						));

				if (Mage::getSingleton("adminhtml/session")->getStoreinventoryData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getStoreinventoryData());
					Mage::getSingleton("adminhtml/session")->setStoreinventoryData(null);
				}
				elseif(Mage::registry("storeinventory_data")) {
				    $form->setValues(Mage::registry("storeinventory_data")->getData());
				}
				return parent::_prepareForm();
		}
}
