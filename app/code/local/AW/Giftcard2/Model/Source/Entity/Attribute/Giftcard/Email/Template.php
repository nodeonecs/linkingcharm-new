<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_Model_Source_Entity_Attribute_Giftcard_Email_Template extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
    implements AW_Giftcard2_Model_Source_SourceInterface
{
    public function toArray()
    {
        $options = $this->toOptionArray();
        $optArray = array();
        foreach ($options as $option) {
            $optArray[$option['value']] = $option['label'];
        }
        return $optArray;
    }

    public function getAllOptions()
    {
        return array();
    }

   public function toOptionArray()
    {
        if ($this->_options === null) {
            $this->_options = Mage::getModel('adminhtml/system_config_source_email_template')
                ->setPath(AW_Giftcard2_Model_Source_Giftcard_Email_Template::DEFAULT_EMAIL_TEMPLATE_PATH)
                ->toOptionArray()
            ;
        }
        return $this->_options;
    }

    public function getOptionText($value)
    {
        $options = $this->toOptionArray();
        foreach ($options as $option) {
            if ($option['value'] == $value) {
                return $option['label'];
            }
        }
        return null;
    }

}
