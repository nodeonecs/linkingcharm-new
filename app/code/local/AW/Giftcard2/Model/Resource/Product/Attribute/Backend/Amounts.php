<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_Model_Resource_Product_Attribute_Backend_Amounts
    extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init('aw_giftcard2/product_entity_amounts', 'value_id');
    }

    /**
     * @param int $entityId
     * @return array
     */
    public function loadAmountsData($entityId)
    {
        $adapter = $this->_getReadAdapter();
        $columns = array(
            'value_id' => $this->getIdFieldName(),
            'price' => 'value',
            'website_id' => 'website_id',
        );
        $select = $adapter->select()
                        ->from($this->getMainTable(), $columns)
                        ->where('entity_id=?', $entityId)
                        ->order(array('website_id ASC', 'value ASC'))
        ;
        return $adapter->fetchAll($select);
    }

    /**
     * @param Varien_Object $amountObject
     * @return int
     */
    public function deleteAmountsData(Varien_Object $amountObject)
    {
        $adapter = $this->_getWriteAdapter();
        $conditions = array (
            $adapter->quoteInto('entity_id = ?', $amountObject->getEntityId()),
            $adapter->quoteInto('value = ?', $amountObject->getPrice()),
        );
        if ($amountObject->getWebsiteId()) {
            $conditions[] = $adapter->quoteInto('website_id = ?', $amountObject->getWebsiteId());
        }
        $where = implode(' AND ', $conditions);
        return $adapter->delete($this->getMainTable(), $where);
    }

    /**
     * @param Varien_Object $amountObject
     * @return $this
     */
    public function saveAmountsData(Varien_Object $amountObject)
    {
        $adapter = $this->_getWriteAdapter();
        $data = $this->_prepareDataForTable($amountObject, $this->getMainTable());

        if (!empty($data[$this->getIdFieldName()])) {
            $where = $adapter->quoteInto($this->getIdFieldName() . ' = ?', $data[$this->getIdFieldName()]);
            unset($data[$this->getIdFieldName()]);
            $adapter->update($this->getMainTable(), $data, $where);
        } else {
            $adapter->insert($this->getMainTable(), $data);
        }
        return $this;
    }
}