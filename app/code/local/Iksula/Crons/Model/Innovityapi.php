<?php

class Iksula_Crons_Model_Innovityapi extends Mage_Core_Model_Abstract
{

    /**
     * Function returns Innovity Response by accessing their verify Payment API
     *
     * @param order details
     * @return status of API w.r.t. payment tried by customer
     */

    public function getApiResponse($orderData){

    	$orderObj        = Mage::getModel('sales/order');
        // $requestUrl      = 'https://unipaynet.innoviti.com/themobilestore/enqTrans ';
        $requestUrl      = Mage::getStoreConfig('crons_section/crons_group/innovityurl');
        // $merchantId      = '658425565512351';
        $merchantId      = Mage::getStoreConfig('crons_section/crons_group/innovitymerchantid');
        // $submerchantId   = 'tms9';
        $submerchantId      = Mage::getStoreConfig('crons_section/crons_group/innovitysubmerchantid');
        $type            = 'O';
        // $date            = $orderData->getCreatedAt();
        $date            = $orderData->getCreatedAtStoreDate()->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);
        $dateArray       = explode(" ", $date);
        $transactionDate = str_replace("-", "", $dateArray[0]);
        $orderIdvalue    = $orderData->getRealOrderId();
        $pram            = "merchantId=658425565512351&subMerchantId=tms9&type=O&value=" . $orderIdvalue . "&txnDate=" . $transactionDate;
        $ch              = curl_init();
        curl_setopt($ch, CURLOPT_URL, $requestUrl);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $pram);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $apiResponse = curl_exec($ch);
        curl_close($ch);
        $obj         = new SimpleXMLElement($apiResponse);
        // $status      = (string) $obj->resmsg;
        $status      = (string) $obj->resCode;
        return $status;
    }

}

