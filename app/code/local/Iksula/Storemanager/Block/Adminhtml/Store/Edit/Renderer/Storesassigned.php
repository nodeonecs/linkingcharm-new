

<?php


class Iksula_Storemanager_Block_Adminhtml_Store_Edit_Renderer_Storesassigned
extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract{


public function render(Varien_Object $row)
    {

        $store_id = $row->getId();

        $storehubrelationObj = Mage::getModel('iksula_storemanager/storehubrelation')->load($store_id , 'store_id');

        if($storehubrelationObj->getIsHub() == '1'){

        	$StoreAssignedCollections = Mage::getModel('iksula_storemanager/storehubrelation')->getCollection()->addFieldToSelect('store_id')->addFieldToFilter('hub_id' , array('eq' => $store_id));

        	foreach($StoreAssignedCollections as $stores){
        			$aStoresAssigned [] = $stores->getStoreId();
        	}

        	//return $aStoresAssigned;

        	$StoresNameCollections = Mage::getModel('core/store_group')->getCollection()->addFieldToSelect('name')->addFieldToFilter('group_id' , array('in' => $aStoresAssigned));

			foreach($StoresNameCollections as $storesDetails){
        			$aStoresName [] = $storesDetails->getName();
        	}

        	//return print_r($aStoresName);

        	$sStoresName = implode(',' , $aStoresName);

        }

        //echo $sStoreName;



        return $sStoresName;
    }


}
