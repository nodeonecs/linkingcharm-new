<?php
/**
 * NOTICE OF LICENSE
 *
 * The MIT License
 *
 * Copyright (c) 2009 S. Landsbek (slandsbek@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package    SLandsbek_SimpleOrderExport
 * @copyright  Copyright (c) 2009 S. Landsbek (slandsbek@gmail.com)
 * @license    http://opensource.org/licenses/mit-license.php  The MIT License
 */

/**
 * Exports orders to csv file. If an order contains multiple ordered items, each item gets
 * added on a separate row.
 */
class SLandsbek_SimpleOrderExport_Model_Export_Billwise extends SLandsbek_SimpleOrderExport_Model_Export_Abstract
{
    const ENCLOSURE = '"';
    const DELIMITER = ',';

    /**
     * Concrete implementation of abstract method to export given orders to csv file in var/export.
     *
     * @param $orders List of orders of type Mage_Sales_Model_Order or order ids to export.
     * @return String The name of the written csv file in var/export
     */
    public function exportOrders($orders) 
    {
        $fileName = 'billwise_'.date("Ymd_His").'.csv';
        $fp = fopen(Mage::getBaseDir('export').'/'.$fileName, 'w');

        $this->writeHeadRow($fp);
        foreach ($orders as $order) {
            $order = Mage::getModel('sales/order')->load($order);
            $this->writeOrder($order, $fp);
        }

        fclose($fp);

        return $fileName;
    }

    /**
	 * Writes the head row with the column names in the csv file.
	 * 
	 * @param $fp The file handle of the csv file
	 */
    protected function writeHeadRow($fp) 
    {
        fputcsv($fp, $this->getHeadRowValues(), self::DELIMITER, self::ENCLOSURE);
    }

    /**
	 * Writes the row(s) for the given order in the csv file.
	 * A row is added to the csv file for each ordered item. 
	 * 
	 * @param Mage_Sales_Model_Order $order The order to write csv of
	 * @param $fp The file handle of the csv file
	 */
    protected function writeOrder($order, $fp) 
    {
        $common = $this->getCommonOrderValues($order);

        $orderItems = $order->getItemsCollection();
        // $creditMemoItems = $order->getCreditmemosCollection();
        
        $itemInc = 0;
        foreach ($orderItems as $item)
        {
            if (!$item->isDummy()) {
                $record = array_merge($common, $this->getOrderItemValues($item, $order, ++$itemInc));
                fputcsv($fp, $record, self::DELIMITER, self::ENCLOSURE);
            }
        }

        $commonCreditMemo = $this->getCommonCreditMemoValues($order);
        $creditMemos = Mage::getResourceModel('sales/order_creditmemo_collection');
        $creditMemos->addFieldToFilter('order_id', $order->getId());
        $creditMemos->setOrder('created_at','asc');
        $creditMemos->load();

        foreach ($creditMemos as $creditmemo)
        {
            $creditmemoModel = Mage::getModel('sales/order_creditmemo')->load($creditmemo->getId());
            $commonCreditMemo[2] = $creditmemo->getIncrementId();
            $_items = $creditmemoModel->getAllItems();
            foreach ($_items as $cmitem) {
                if ($cmitem->getOrderItem()->getParentItem()) continue;
                // echo "<pre>";
                // print_r($item->getData());
                $record1 = array_merge($commonCreditMemo, $this->getCreditMemoItemValues($cmitem, $order, ++$itemInc));
                fputcsv($fp, $record1, self::DELIMITER, self::ENCLOSURE);
            }
            
        }
        // exit;
    }

    /**
	 * Returns the head column names.
	 * 
	 * @return Array The array containing all column names
	 */
    protected function getHeadRowValues() 
    {
        return array(
            'Date',
            'Invoice No',
            'Credit Note',
            'Order No',           
            'SKU',
            'HSN Code',
            'Description',
    		'Name',
            'Category',
        	'Cost price',
        	'Selling Price',
        	'Quantity',
            'Gross Sales',
            'Discount',
            'IGST',
            'CGST',
            'SGST',
            'Net Sales',
            'Cost of Sale',
            'Gross Margin',
            'GP%',
    	);
    }

    /**
	 * Returns the values which are identical for each row of the given order. These are
	 * all the values which are not item specific: order data, shipping address, billing
	 * address and order totals.
	 * 
	 * @param Mage_Sales_Model_Order $order The order to get values from
	 * @return Array The array containing the non item specific values
	 */
    protected function getCommonOrderValues($order) 
    {
        $shippingAddress = !$order->getIsVirtual() ? $order->getShippingAddress() : null;
        $billingAddress = $order->getBillingAddress();
        
        return array(
            Mage::helper('core')->formatDate($order->getCreatedAt(), 'short', false),
            $this->getItemInvoiceNumber($order), // invoice number
            '', // credit note number
            $order->getRealOrderId(), // order id    
        );
    } 

    protected function getCommonCreditMemoValues($order) 
    {
        $shippingAddress = !$order->getIsVirtual() ? $order->getShippingAddress() : null;
        $billingAddress = $order->getBillingAddress();
        
        return array(
            Mage::helper('core')->formatDate($order->getCreatedAt(), 'short', false),
            '', // invoice number
            $this->getItemCreditNoteNumber($order), // credit note number
            $order->getRealOrderId(), // order id    
        );
    }

    /**
	 * Returns the item specific values.
	 * 
	 * @param Mage_Sales_Model_Order_Item $item The item to get values from
	 * @param Mage_Sales_Model_Order $order The order the item belongs to
	 * @return Array The array containing the item specific values
	 */
    protected function getOrderItemValues($item, $order, $itemInc=1) 
    {
        $_product = Mage::getModel('catalog/product')->load($item->getProductId());
        $code_of_sale = $this->getItemTotal($item);
        $gross_margin = $this->getItemTotal($item);
        
        $costPrice = $this->formatPrice($item->getCostPrice(), $order); 
        $sellingPrice = $this->formatPrice($item->getPrice(), $order);
        $grossSales = $this->formatPrice($item->getRowTotal(), $order);
        $discountAmount = $this->formatPrice($this->getItemDiscountAmount($item), $order);
        $igst = $this->formatPrice($item->getData('igst_charge'), $order);
        $cgst = $this->formatPrice($item->getData('cgst_charge'), $order);
        $sgst = $this->formatPrice($item->getData('sgst_charge'), $order);
        $netSales = $grossSales-$discountAmount-$igst-$cgst-$sgst;
        $grossMargin = $netSales-$costPrice;
        $gp = ($grossMargin/$netSales);

        return array(
            $this->getItemSku($item), //sku
            '', // hsn code
            $_product->getDescription(), //description
            $item->getName(), // name
            $this->getItemCategories($_product), //category
            $costPrice, //cost price
            $sellingPrice, //selling price
            (int)$item->getQtyOrdered(), //qty
            $grossSales,//gross sales
            $discountAmount, //discount
            $igst, //igst
            $cgst, //cgst
            $sgst, //sgst
            $netSales, // net sales
            $costPrice, // cost of sales
            $grossMargin, // gross margin
            $gp // GP %
        );
    }


    protected function getCreditMemoItemValues($item, $order, $itemInc=1) 
    {
        $_product = Mage::getModel('catalog/product')->load($item->getProductId());
        $code_of_sale = $this->getItemTotal($item);
        $gross_margin = $this->getItemTotal($item);
        
        $costPrice = $this->formatPrice($item->getCostPrice(), $order); 
        $sellingPrice = $this->formatPrice($item->getPrice(), $order);
        $grossSales = $this->formatPrice($item->getRowTotal(), $order);
        $discountAmount = $this->formatPrice($this->getItemDiscountAmount($item), $order);
        $igst = $this->formatPrice($item->getData('igst_charge'), $order);
        $cgst = $this->formatPrice($item->getData('cgst_charge'), $order);
        $sgst = $this->formatPrice($item->getData('sgst_charge'), $order);
        $netSales = $grossSales-$discountAmount-$igst-$cgst-$sgst;
        $grossMargin = $netSales-$costPrice;
        $gp = ($grossMargin/$netSales);

        return array(
            $this->getItemSku($item), //sku
            '', // hsn code
            $_product->getDescription(), //description
            $item->getName(), // name
            $this->getItemCategories($_product), //category
            (-1)*$costPrice, //cost price
            (-1)*$sellingPrice, //selling price
            (int)(-1)*$item->getQtyOrdered(), //qty
            (-1)*$grossSales,//gross sales
            (-1)*$discountAmount, //discount
            (-1)*$igst, //igst
            (-1)*$cgst, //cgst
            (-1)*$sgst, //sgst
            (-1)*$netSales, // net sales
            (-1)*$costPrice, // cost of sales
            (-1)*$grossMargin, // gross margin
            (-1)*$gp // GP %
        );
    } 

    protected function getCreditMemoItemValues2($item, $order, $itemInc=1) 
    {
        $_product = Mage::getModel('catalog/product')->load($item->getProductId());
        $code_of_sale = $this->getItemTotal($item);
        $gross_margin = $this->getItemTotal($item);
        $gp = ($code_of_sale/$gross_margin)*100;
        return array(
            $this->getItemSku($item), //sku
            '', // hsn code
            $_product->getDescription(), //description
            $item->getName(), // name
            $this->getItemCategories($_product), //category
            $this->formatPrice((-1)*$item->getCostPrice(), $order), //cost price
            $this->formatPrice((-1)*$item->getPrice(), $order), //selling price
            (int)(-1)*$item->getQty(), //qty
            $this->formatPrice((-1)*$item->getPrice()*$item->getQty(), $order),//gross sales
            $this->formatPrice((-1)*$this->getItemDiscountAmount($item), $order), //discount
            $this->formatPrice((-1)*$item->getData('igst_charge'), $order), //igst
            $this->formatPrice((-1)*$item->getData('cgst_charge'), $order), //cgst
        	$this->formatPrice((-1)*$item->getData('sgst_charge'), $order), //sgst
            $this->formatPrice((-1)*$item->getRowTotal(), $order), // net sales = gross sales-discount-igst-csgt-sgst
            $this->formatPrice((-1)*$item->getCostPrice()*$item->getQty(), $order), // cost of sales = cost*qty
            $this->formatPrice((-1)*$this->getItemTotal($item), $order), // gross margin = Net Sales - Cost of Sale
            $gp // GP %
        );
    }
}
?>