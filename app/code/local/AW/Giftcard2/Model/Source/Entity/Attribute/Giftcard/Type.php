<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

class AW_Giftcard2_Model_Source_Entity_Attribute_Giftcard_Type extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
    implements AW_Giftcard2_Model_Source_SourceInterface
{
    /**
     * Option values
     */
    const VIRTUAL_VALUE = 1;
    const PHYSICAL_VALUE = 2;
    const COMBINED_VALUE = 3;

    /**
     * Option labels
     */
    const VIRTUAL_LABEL = 'Virtual';
    const PHYSICAL_LABEL = 'Physical';
    const COMBINED_LABEL = 'Combined';

    /**
     * @return array
     */
    public function toArray()
    {
        $helper = Mage::helper('aw_giftcard2');
        return array(
            self::VIRTUAL_VALUE           => $helper->__(self::VIRTUAL_LABEL),
            self::PHYSICAL_VALUE          => $helper->__(self::PHYSICAL_LABEL),
            self::COMBINED_VALUE          => $helper->__(self::COMBINED_LABEL),
        );
    }

    public function getAllOptions()
    {
        $result = array();
        foreach ($this->toArray() as $key => $value) {
            $result[] = array('value' => $key, 'label' => $value);
        }
        return $result;
    }

    public function getOptionText($value)
    {
        $options = $this->toArray();
        if (array_key_exists($value, $options)) {
            return $options[$value];
        }
        return null;
    }

    /**
     * Add sort by gift card type to products collection
     *
     * @param Mage_Eav_Model_Entity_Collection_Abstract $collection
     * @param string $dir
     * @return Mage_Eav_Model_Entity_Attribute_Source_Abstract $this
     */
    public function addValueSortToCollection($collection, $dir = 'asc')
    {
        $attributeCode  = $this->getAttribute()->getAttributeCode();
        $attributeId    = $this->getAttribute()->getId();
        $attributeTable = $this->getAttribute()->getBackend()->getTable();

        $tableName = $attributeCode . '_table';
        $collection->getSelect()
            ->joinLeft(
                array($tableName => $attributeTable),
                "e.entity_id={$tableName}.entity_id"
                . " AND {$tableName}.attribute_id='{$attributeId}'"
                . " AND {$tableName}.store_id='0'",
                array())
            ->order($tableName . '.value' . ' ' . $dir);
        ;
        return $this;
    }

}
