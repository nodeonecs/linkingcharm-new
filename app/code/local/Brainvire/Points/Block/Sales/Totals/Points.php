<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   Brainvire
 * @package    Brainvire_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * @license    
 */


class Brainvire_Points_Block_Sales_Totals_Points extends Mage_Core_Block_Template {

    public function getOrder() {
        return $this->getParentBlock()->getOrder();
    }

    public function getSource() {
        return $this->getParentBlock()->getSource();
    }

    public function initTotals() {
        if ($this->getOrder()->getMoneyForPoints()) {
            $source = $this->getSource();

            $this->getParentBlock()->addTotal(new Varien_Object(array(
                        'code' => 'points',
                        'strong' => false,
                        'label' => Mage::helper('points/config')->getPointUnitName(),
                        'value' => $source->getMoneyForPoints()
                    )), 'subtotal');
        }

        return $this;
    }

}