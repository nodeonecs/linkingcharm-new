<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @category   Brainvire
 * @package    Brainvire_Points
 * @version    1.5.1
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * @license    
 */


class Brainvire_Points_Model_Actions_OrderInvoicedByReferral extends Brainvire_Points_Model_Actions_Abstract {

    protected $_action = 'order_invoiced_by_referral';
    protected $_comment = 'Reward for invited user placed order';

}

?>
