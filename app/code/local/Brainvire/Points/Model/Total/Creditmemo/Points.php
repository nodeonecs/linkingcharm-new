<?php
/**
* Brainvire Infotech Pvt. Ltd
 * @copyright  Copyright (c) 2010-2012 Brainvire Infotech Pvt. Ltd (http://www.brainvire.com)
 * 
 */


class Brainvire_Points_Model_Total_Creditmemo_Points extends Mage_Sales_Model_Order_Creditmemo_Total_Abstract {

    public function collect(Mage_Sales_Model_Order_Creditmemo $creditmemo) {
        $order = $creditmemo->getOrder();
        if ($order->getBaseMoneyForPoints() && $order->getMoneyForPoints()) {
            $moneyBaseToReduce = $order->getBaseMoneyForPoints();
            $moneyToReduce = $order->getMoneyForPoints();

            if ($creditmemo->getBaseGrandTotal() + $moneyBaseToReduce < 0) {
                $creditmemo->setGrandTotal(0);
                $creditmemo->setBaseGrandTotal(0);
                $creditmemo->setMoneyForPoints($creditmemo->getGrandTotal());
                $creditmemo->setBaseMoneyForPoints($creditmemo->getBaseGrandTotal());
            } else {
                $creditmemo->setGrandTotal($creditmemo->getGrandTotal() + $moneyToReduce);
                $creditmemo->setBaseGrandTotal($creditmemo->getBaseGrandTotal() + $moneyBaseToReduce);
                $creditmemo->setMoneyForPoints($moneyToReduce);
                $creditmemo->setBaseMoneyForPoints($moneyBaseToReduce);
            }
        }
        return $this;
    }

}
