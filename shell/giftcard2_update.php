<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Giftcard2
 * @version    2.2.0
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

require '../app/Mage.php';

if (Mage::isInstalled()) {
    Mage::app('admin')->setUseSessionInUrl(false);
    try {
        Mage::getModel('aw_giftcard2/shell_migration')->process();
        Mage::log("Completed!", 1, AW_Giftcard2_Model_Shell_Migration::LOG_FILE, true);
    } catch (Exception $e) {
        Mage::printException($e);
    }
} else {
    Mage::log(
        "Application is not installed yet, please complete install wizard first.",
        1,
        AW_Giftcard2_Model_Shell_Migration::LOG_FILE,
        true
    );
}
