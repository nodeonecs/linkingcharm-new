<?php


class Iksula_Rewrites_Model_Sales_Quote_Address_Total_Subtotal extends Mage_Sales_Model_Quote_Address_Total_Subtotal
{
	public function fetch(Mage_Sales_Model_Quote_Address $address)
    {
        $address->addTotal(array(
            'code'  => $this->getCode(),
            'title' => Mage::helper('sales')->__('Total'),
            'value' => $address->getSubtotal()
        ));
        return $this;
    }


	public function getLabel()
    {
        return Mage::helper('sales')->__('Total');
    }


}



?>